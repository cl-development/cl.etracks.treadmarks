﻿using CL.TMS.DataContracts.RetailConnectionService;
using CL.TMS.ExceptionHandling;
using CL.TMS.ServiceContracts.RPMServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;

namespace CL.TMS.RetailConnectionServices.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/RetailConnection")]
    public class RetailConnectionController : ApiController
    {
        private IRetailConnectionService retailConnectionService;

        public RetailConnectionController(IRetailConnectionService retailConnectionService)
        {
            this.retailConnectionService = retailConnectionService;
        }

        [Route("Collectors")]
        [HttpGet]
        public IHttpActionResult AllCollectors()
        {
            try
            {
                var result = retailConnectionService.LoadActiveCollectors();
                if (result.Count == 0)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Categories")]
        [ResponseType(typeof(CategoryModel))]
        [HttpGet]
        public IHttpActionResult ProductCategories()
        {
            try
            {
                var result = retailConnectionService.LoadActiveCategory();
                if (result.Count==0)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Categories/{categoryId:int}/products")]
        [ResponseType(typeof(ProductListModel))]
        [HttpGet]
        public IHttpActionResult ProductList(int categoryId)
        {
            try
            {
                var result = retailConnectionService.LoadActiveProductList(categoryId);
                if (result.Count==0)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(result);
                }               
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("Products/{productId:int}")]
        [ResponseType(typeof(ProductModel))]
        [HttpGet]
        public IHttpActionResult Product(int productId)
        {
            try
            {
                var result = retailConnectionService.LoadActiveProductDetails(productId);
                if (result == null)
                {
                    return BadRequest("Inactive Product");
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}