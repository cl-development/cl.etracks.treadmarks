﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Announcement
{
    public class AnnouncementVM
    { 
        public int ID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        // for display order
        public DateTime CreateDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Steward { get; set; }
        public bool Collector { get; set; }
        public bool Hauler { get; set; }
        public bool Processor { get; set; }
        public bool RPM { get; set; }
        public bool Staff { get; set; }
    }
    public class AnnouncementListVM : BaseDTO<int>
    {      
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; } 
        public string ModifiedBy { get; set; }
    }
}
