﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel
{
    public class ClaimDetailVM
    {
        public int ClaimId { get; set; }
        public int TransactionId { get; set; }
    }
}
