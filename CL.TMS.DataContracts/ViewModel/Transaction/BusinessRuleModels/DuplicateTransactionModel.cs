﻿using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels
{
    public class DuplicateTransactionModel
    {
        public int TransactionId { get; set; }
        public string TransactionNumber { get; set; }
        public string TransactionTypeCode {get; set;}
        public int? IncomingId { get; set; }
        public int? OutgoingId { get; set; }
        public string IncomingRegistrationNumber { get; set; }
        public string OutgoingRegistrationNumber { get; set; }
        public string IncomingPeriodShortName { get; set; }
        public string OutgoingPeriodShortName { get; set; }
        public int IncomingVendorTypeInt { get; set; }
        public int OutgoingVendorTypeInt { get; set; }
        public string IncomingVendorType { get { return CommonModelHelper.GetRegistrantTypeName(IncomingVendorTypeInt); } }
        public string OutgoingVendorType { get { return CommonModelHelper.GetRegistrantTypeName(OutgoingVendorTypeInt); } }
    }


}
