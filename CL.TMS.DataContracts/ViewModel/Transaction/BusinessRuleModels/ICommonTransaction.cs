﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels
{
    public interface ICommonTransaction
    {
        CommonTransactionBusinessRules CommonBusinessRulesContainer { get; set; }
    }
}
