﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionListSecurityViewModel : PageSecurityViewModel
    {
        public bool AddNewTransactionBtnDisabled { get; set; }
        public bool UpdateTransactionDisabled { get; set; } //adjust, invalidate, void, approve
    }
}
