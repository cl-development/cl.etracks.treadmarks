﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using FluentValidation.Attributes;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class CollectorDOTTransactionViewModel : CollectorTransactionViewModel
    {
        public int? HaulerNumber { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public bool GenerateTires { get; set; }

        public CollectorDOTTransactionViewModel() : base()
        {            
        }

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.DOT;
            FormDate = new TransactionFormDateViewModel();   
            TireTypeList = new List<TransactionTireTypeViewModel>();            
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Paper Form", DocumentFormat = 3, Required = true },
                new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = "Scale Ticket", DocumentFormat = 3, Required = false }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();

            ScaleTicket = new TransactionScaleTicketViewModel() { WeightType = TreadMarksConstants.Kg, WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg } };
            GenerateTires = false;
        }
    }
}
