﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public interface IRPMTransactionViewModel 
    {
        int CurrentVendorId { get; set; }
        int ClaimId { get; set; }
        Guid TransactionId { get; set; }
        string FormType { get; set; }
        TransactionFormDateViewModel FormDate { get; set; }
        long? FormNumber { get; set; }
        VendorReference Inbound { get; set; }
        VendorReference Outbound { get; set; }        
        List<TransactionSupportingDocumentViewModel> SupportingDocumentList { get; set; }
        List<TransactionFileUploadModel> UploadDocumentList { get; set; }
        string InternalNote { get; set; }

        void initialize();
    }    
}
