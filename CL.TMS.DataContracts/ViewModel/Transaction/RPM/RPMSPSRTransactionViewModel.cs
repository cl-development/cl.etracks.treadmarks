﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using FluentValidation.Attributes;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class RPMSPSRTransactionViewModel : RPMTransactionViewModel
    {
        public string InvoiceNumber { get; set; }
        public double? TotalTDPWeight { get; set; }
        public int UnitType { get; set; }
        public RPMSPSRProductViewModel ProductDetail { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<TransactionProductViewModel> ProductList { get; set; }

        public List<string> CountryList { get; set; }

        public RPMSPSRTransactionViewModel() : base()
        {
            UnitType = TreadMarksConstants.Ton;
        }

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.SPSR;
            FormDate = new TransactionFormDateViewModel();
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Invoice", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();
            ProductDetail = new RPMSPSRProductViewModel();
            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "" };

            ProductList = new List<TransactionProductViewModel>();
            CountryList = new List<string>();
        }
    }
}
