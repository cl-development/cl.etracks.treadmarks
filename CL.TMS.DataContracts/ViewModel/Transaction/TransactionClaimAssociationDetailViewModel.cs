﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionClaimAssociationDetailViewModel
    {
        public int TransactionId { get; set; }
        public List<TransactionClaimAssociationViewModel> Associations { get; set; }        
    }

    public class TransactionClaimAssociationViewModel
    {
        public int ClaimId { get; set; }       
        public string ClaimPeriodName { get; set; }
        public TransactionPeriodViewModel Period { get; set; }
        public string Status { get; set; }
        public int ParticipantID { get; set; }
        public string ContactEmail { get; set; }
    }

    public class TransactionPeriodViewModel
    {        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }        
    }
}
