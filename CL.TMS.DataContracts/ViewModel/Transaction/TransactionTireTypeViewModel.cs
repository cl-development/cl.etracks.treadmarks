﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionTireTypeViewModel
    {
        public string TireType { get; set; }
        public int? Count { get; set; }
        public decimal? StandardWeight { get; set; }
        public int? OriginalCount { get; set; }
    }
}
