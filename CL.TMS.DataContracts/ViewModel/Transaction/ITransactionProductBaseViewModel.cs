﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public interface ITransactionProductBaseViewModel
    {
        int? ProductTypeId { get; set; }        
        Decimal? ProductRate { get; set; }
    }
}
