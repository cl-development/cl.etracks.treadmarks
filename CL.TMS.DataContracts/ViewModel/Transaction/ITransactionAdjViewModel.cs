﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public interface ITransactionAdjViewModel
    {
        bool Preview { get; set; }
        int TransactionId { get; set; }
        long TransactionNumber { get; set; }
        Guid TransactionGuid { get; set; }
        string TransactionType { get; set; }
        List<TransactionSupportingDocumentViewModel> SupportingDocumentList { get; set; }
        List<TransactionViewSupportingDocumentAttachmentViewModel> AttachmentList { get; set; }
        List<TransactionFileUploadModel> UploadDocumentList { get; set; }
        string AdjustmentNote { get; set; }
    }
}
