﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorRateDetailViewModel
    {
        public decimal? OnRoadRate { get; set; }
        public decimal? OffRoadRate { get; set; }
    }
}
