﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.Processor
{
    public class ProcessorCommonTransactionListViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        public ProcessorCommonTransactionListViewModel()
        {
            this.TransactionListSecurity = new TransactionListSecurityViewModel();
        }
        ///TCR
        public Guid TransactionId { get; set; }
        public string DeviceName { get; set; }
        public string Notes { get; set; }
        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.TransactionNotes)
                    foreach (string item in this.TransactionNotes)
                    {
                        if (null != item)
                        {
                            sSum += ("- " + item.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        //public List<TransactionNote> TransactionNotes { get; set; }
        public List<string> TransactionNotes { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TransactionFriendlyId { get; set; }//TCR #, DOT #, HIT #...
        public string ReviewStatus { get; set; }
        public string Adjustment
        {
            get
            {
                if (0 < IsAdjustByStaff.Count)
                {
                    if (IsAdjustByStaff.All(c => c == true))
                    {
                        return "Staff";
                    }
                    else if (IsAdjustByStaff.All(c => c == false))
                    {
                        return "Participant";
                    }
                    else if (IsAdjustByStaff.Any(c => c == true) && IsAdjustByStaff.Any(c => c == false))
                    {
                        return "Both";
                    }
                }

                return string.Empty;
            }
        }
        public string VendorNumber { get; set; }//Transaction receiver Vendor number
        public string VendorBusinessName { get; set; }//Transaction receiver Business name
        public string Zone { get; set; }
        public string PostalCode { get; set; }
        public string FSA
        {
            get
            {
                if (PostalCode != null)
                {
                    if (PostalCode.Length > 2)
                    {
                        return PostalCode.Substring(0, 3);
                    }
                    return PostalCode;
                }
                return string.Empty;
            }
        }
        public decimal OnRoadWeight { get; set; }
        public decimal OffRoadWeight { get; set; }
        public long CreatedUserId { get; set; }
        public bool IsGenerateTires { get; set; }
        [JsonIgnore]
        public List<bool> IsAdjustByStaff { get; set; }
        [JsonIgnore]
        public List<TransactionItem> TransactionItems { get; set; }
        [JsonIgnore]
        public List<TransactionItemListDetailViewModel> TireCountList { get; set; } 

        public int PLT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.PLT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        public int MT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int AGLS
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.AGLS);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int IND
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.IND);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int SOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.SOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int MOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int LOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.LOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int GOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.GOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        //OTSTM2-1064
        private bool flagQtyOverDOR;
        public bool FlagQtyOverDOR
        {
            get
            {
                return flagQtyOverDOR;
            }
        }

        public void SetFlagQtyOverDOR(int cBTotalTireCount, int totalTireCountDOR)
        {
            if (totalTireCountDOR < 0)
            {
                flagQtyOverDOR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountDOR)
                {
                    flagQtyOverDOR = true;
                }
                else
                {
                    flagQtyOverDOR = false;
                }
            }

        }

        //OTSTM2-1064
        private bool flagQtyOverPTR;
        public bool FlagQtyOverPTR
        {
            get
            {
                return flagQtyOverPTR;
            }
        }

        public void SetFlagQtyOverPTR(int cBTotalTireCount, int totalTireCountPTR)
        {
            if (totalTireCountPTR < 0)
            {
                flagQtyOverPTR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountPTR)
                {
                    flagQtyOverPTR = true;
                }
                else
                {
                    flagQtyOverPTR = false;
                }
            }

        }
       
        public string ProcessingStatus { get; set; }

        public bool MobileFormat { get; set; }
        ///DOT
        public decimal? ScaleWeight { get; set; }
        public decimal EstWeight { get; set; }

        ///UCR
        public string Sitename { get; set; }
        public string ReasonNotEligible { get; set; }//TransactionNotes

        ///HIT

        ///STC
        public string NameofGroupIndividual { get; set; }

        ///PTR
        public string ScaleTicketNbr { get; set; }
        public decimal TotalWeight { get; set; }
        public string Badge { get; set; }
        public decimal AmountDollar { get; set; }
        
        public decimal? Variance
        {
            get
            {
                if (0 != EstWeight)
                    return (((this.ScaleWeight - this.EstWeight) / (this.EstWeight)) * 100);
                else
                    return 0;
                //Variance = ((Scale Weight - Estimated Weight)/ Estimated Weight) *100
            }
            set { Variance = value; }
        }

        public List<ScaleTicket> ScaleTickets { get; set; }

        public int? IncomingId { get; set; }

        ///RTR
        public string MarketLocation { get; set; }
        public string TireMarketType { get; set; }
        public string BuyerName { get; set; }
        public string InvoiceNbr { get; set; }
        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }

        ///SPS
        public string Destination { get; set; }
        public string ProductType { get; set; }
        public string EligibleProductCode { get; set; }
        public string MeshRange { get; set; }
        public decimal? Rate { get; set; }
        public DateTime InvoiceDate { get; set; }

        ///DOR
        public int MaterialType { get; set; }
        public string MaterialTypeName 
        {
            get
            {
                switch (this.MaterialType)
                {
                    case 1:
                        return TreadMarksConstants.Trash;
                    case 2:
                        return TreadMarksConstants.Steel;
                    case 3:
                        return TreadMarksConstants.Fibre;
                    case 4:
                        return TreadMarksConstants.TireRims;
                    case 5:
                        return TreadMarksConstants.UsedTire;
                    default:
                        return "";
                }
            }
        }
        public string DispositionReason { get; set; }
        public Decimal? OnRoadRate { get; set; }
        public Decimal? OffRoadRate { get; set; }

        public int? OutgoingId { get; set; }
    }
}
