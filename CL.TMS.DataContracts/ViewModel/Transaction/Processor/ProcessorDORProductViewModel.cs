﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorDORProductViewModel
    {
        public int? MaterialTypeId { get; set; }
        public string PTRNumber { get; set; }
        public ProcessorPTRWeightDetailViewModel PTRWeightDetail { get; set; }
        public ProcessorScaleWeightViewModel OnRoadScaleWeight { get; set; }
        public ProcessorScaleWeightViewModel OffRoadScaleWeight { get; set; }
        public int? DispositionReasonId { get; set; }

        public decimal? OnRoadRate { get; set; }
        public decimal? OffRoadRate { get; set; }
    }
}
