﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;

using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorPITTransactionViewModel : ProcessorTransactionViewModel
    {
        public bool IsInbound { get; set; }
        public int? ProcessorNumber { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public ProcessorPITProductViewModel ProductDetail { get; set; }

        public List<TransactionProductViewModel> ProductList { get; set; }

        public ProcessorPITTransactionViewModel() : base()
        {            
        }   

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.PIT;
            FormDate = new TransactionFormDateViewModel();            
            TireTypeList = new List<TransactionTireTypeViewModel>();            
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Paper Form", DocumentFormat = 3, Required = true },
                new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = "Scale Ticket", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();

            ScaleTicket = new TransactionScaleTicketViewModel() { WeightType = TreadMarksConstants.Ton, WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg, TreadMarksConstants.Ton } };
            ProductDetail = new ProcessorPITProductViewModel();

            ProductList = new List<TransactionProductViewModel>();
        }
    }
}
