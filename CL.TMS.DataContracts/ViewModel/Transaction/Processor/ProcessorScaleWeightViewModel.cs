﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorScaleWeightViewModel
    {        
        public double? Weight { get; set; }
        public int WeightType { get; set; }
    }
}
