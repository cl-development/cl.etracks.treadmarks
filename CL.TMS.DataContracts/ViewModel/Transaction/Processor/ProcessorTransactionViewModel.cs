﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorTransactionViewModel : IProcessorTransactionViewModel,ICommonTransaction
    {
        public int CurrentVendorId { get; set; }
        public int ClaimId { get; set; }
        public Guid TransactionId { get; set; }
        public string FormType { get; set; }
        public TransactionFormDateViewModel FormDate { get; set; }
        public long? FormNumber { get; set; }
        public VendorReference Inbound { get; set; }
        public VendorReference Outbound { get; set; }
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }
        public List<TransactionSupportingDocumentViewModel> SupportingDocumentList { get; set; }
        public List<TransactionFileUploadModel> UploadDocumentList { get; set; }
        public string InternalNote { get; set; }
        public CommonTransactionBusinessRules CommonBusinessRulesContainer { get; set; }

        public ProcessorTransactionViewModel() { }

        public virtual void initialize() { }
    }
}
