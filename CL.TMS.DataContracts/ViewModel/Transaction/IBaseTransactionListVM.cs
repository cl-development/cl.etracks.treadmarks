﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public interface IBaseTransactionListVM
    {
        //todo:move all common property for transaction list VM here
        TransactionListSecurityViewModel TransactionListSecurity { get; set; }
        //List<TransactionItem> TransactionItems { get; set; }
        List<TransactionItemListDetailViewModel> TireCountList { get; set; } 
    }
}
