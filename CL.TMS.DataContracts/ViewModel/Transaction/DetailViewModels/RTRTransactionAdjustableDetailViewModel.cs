﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class RTRTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public TransactionViewParticipantViewModel InboundParticipant { get; set; }
        public RTRTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();
        }
    }
}
