﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels
{
    public class TransactionReviewToolBar
    {
        public bool IsOtherClaimApproved { get; set; }
        public bool hasPendingAdjustments { get; set; }
    }
}
