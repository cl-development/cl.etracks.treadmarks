﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class SPSTransactionDetailViewModel : TransactionDetailViewModel
    {             
        public string SoldTo { get; set; }
        public bool AsSIR { get; set; }
    }
}
