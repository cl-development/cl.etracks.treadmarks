﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class DORTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }
        public string InvoiceNumber { get; set; }
        public int? MaterialTypeId { get; set; }
        public int? DispositionReasonId { get; set; }
        public TransactionViewDORTireRimViewModel TireRimDetail { get; set; }
        public TransactionViewDORUsedTireSaleViewModel UsedTireSaleDetail { get; set; }
        public TransactionViewParticipantViewModel InboundParticipant { get; set; }

        public DORTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = null;
        }
    }
}
