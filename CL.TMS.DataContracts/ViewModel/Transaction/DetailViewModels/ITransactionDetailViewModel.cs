﻿using CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public interface ITransactionDetailViewModel
    {
        bool IsIncomplete { get; set; }

        int Id { get; set; }
        Guid TransactionId { get; set; }
        string Status { get; set; }
        string CurrentMode { get; set; }
        bool AdjustmentAllowedInClaim { get; set; }
        bool AdjustmentReviewAllowed { get; set; }
        bool AdjustmentRecallAllowed { get; set; }
        bool AdjustmentRejectAllowed { get; set; }
        bool ActiveAdjustmentExits { get; set; }
        bool PendingAdjustmentExits { get; set; }
        bool DisplayOriginal { get; set; }
        string TypeCode { get; set; }
        int Direction { get; set; }
        int? IncomingId { get; set; }
        int? OutgoingId { get; set; }
        string DisplayTypeCode { get; set; }
        DateTime TransactionDate { get; set; }
        DateTime? TransactionEndDate { get; set; }
        DateTime TransactionCreatedDate { get; set; }
        long FriendlyNumber { get; set; }
        bool IsMobile { get; set; }
        DateTime? MobileSyncDate { get; set; }
        string DeviceName { get; set; }
        string Badge { get; set; }
        decimal? RetailPrice { get; set; }
        TransactionViewParticipantViewModel InboundParticipant { get; set; }
        TransactionViewParticipantViewModel OutboundParticipant { get; set; }
        List<string> CommentList { get; set; }
        List<TransactionViewPhotoViewModel> PhotoList { get; set; }
        TransactionViewSignatureViewModel Signature { get; set; }
        TransactionManageInternalNoteViewModel InternalNote { get; set; }
        TransactionAdjStatusViewModel AdjustmentDetail { get; set; }

        ITransactionAdjViewModel AdjModel { get; set; }
        ITransactionAdjustableDetailViewModel Original { get; set; }
        ITransactionAdjustableDetailViewModel Adjusted { get; set; }

        string AdjustmentNote { get; set; }
        string OtherClaimStatus { get; set; }

        //OTSTM2-660
        TransactionReviewToolBar TransactionReviewToolBar { get; set; }
        string TransactionProcessingStatus { get; set; }
        List<MobileTransactionGPS> MobileTransactionGPS { get; set; }
    }
}
