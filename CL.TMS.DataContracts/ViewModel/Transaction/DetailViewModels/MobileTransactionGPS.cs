﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels
{
    public class MobileTransactionGPS
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime? GPSDateTime { get; set; }
        public string LocationName { get; set; }
        public string IconName { get; set; }
        public string AddressText { get; set; }
    }
}
