﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class PTRTransactionAdjViewModel : TransactionAdjViewModel
    {
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }
    }
}
