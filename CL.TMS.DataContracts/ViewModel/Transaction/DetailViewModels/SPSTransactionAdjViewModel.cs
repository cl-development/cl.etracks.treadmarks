﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class SPSTransactionAdjViewModel : TransactionAdjViewModel
    {
        public string SoldTo { get; set; }
        public string InvoiceNumber { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public ProcessorSPSProductViewModel ProductDetail { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<TransactionProductViewModel> ProductList { get; set; }
        public List<string> CountryList { get; set; }
    }
}
