﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class SPSTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public string InvoiceNumber { get; set; }
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }
        public TransactionViewProductViewModel ProductDetail { get; set; }
        public TransactionViewParticipantViewModel InboundParticipant { get; set; }

        public SPSTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = null;
        }
    }
}
