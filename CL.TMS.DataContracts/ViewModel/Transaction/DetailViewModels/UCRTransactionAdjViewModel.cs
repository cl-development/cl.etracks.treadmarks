﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class UCRTransactionAdjViewModel : TransactionAdjViewModel
    {
        public string CollectorNumber { get; set; }
        public string PaymentEligible { get; set; }
        public string PaymentEligibleOther { get; set; }
        public string PickedUpFrom { get; set; }
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }  
    }
}
