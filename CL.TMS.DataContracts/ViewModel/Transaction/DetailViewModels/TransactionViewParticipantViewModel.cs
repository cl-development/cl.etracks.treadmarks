﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewParticipantViewModel
    {
        public bool ValidInfo { get; set; }

        public string ParticipantLabel { get; set; }

        public string ParticipantNumber { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        public bool DisplayParticipantNumber { get; set; }
        public bool ShowMapMarker { get; set; }

        public string Gps { get; set; }
    }
}
