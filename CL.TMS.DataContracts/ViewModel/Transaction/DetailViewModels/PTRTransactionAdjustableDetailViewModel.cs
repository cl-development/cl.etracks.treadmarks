﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class PTRTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }
        public ProcessorCalculationSummaryViewModel CalculationSummary { get; set; }

        public PTRTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();
        }
    }
}
