﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorCalculationSummaryViewModel
    {              
        public Decimal? Amount { get; set; }
        public List<ProcessorCalculationItemViewModel> ItemList { get; set; }
    }

    public class ProcessorCalculationItemViewModel
    {
        public string Name { get; set; }
        public decimal EstWeight { get; set; }
        public decimal ScaleWeight { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }

    public class ProcessorCalculationDetailViewModel
    {
        public string Name { get; set; }
        public decimal EstWeight { get; set; }
        public decimal ScaleWeight { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }
}
