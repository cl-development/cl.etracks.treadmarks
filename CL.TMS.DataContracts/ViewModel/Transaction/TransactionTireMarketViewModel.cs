﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionTireMarketViewModel
    {
        public string Location { get; set; }
        public string LocationOther { get; set; }
        public int Type { get; set; }
        public string BillofLoading { get; set; }
    }

    public class TransactionTireMarketDetailViewModel
    {
        public string Location { get; set; }        
        public int Type { get; set; }
        public string BillofLoading { get; set; }
    }
}
