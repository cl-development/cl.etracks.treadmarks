﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class HaulerStaffCommonTransactionListViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        public HaulerStaffCommonTransactionListViewModel()
        {
            this.TransactionListSecurity = new TransactionListSecurityViewModel();
        }

        ///TCR
        public Guid TransactionId { get; set; }
        public string DeviceName { get; set; }
        public string Notes { get; set; }
        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.TransactionNotes)
                    foreach (TransactionNote item in this.TransactionNotes)
                    {
                        if (null != item)
                        {
                            sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        [JsonIgnore]
        public List<TransactionNote> TransactionNotes { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TransactionFriendlyId { get; set; }//TCR #, DOT #, HIT #...
        public string ReviewStatus { get; set; }
        public string Adjustment
        {
            get
            {
                if (0 < IsAdjustByStaff.Count)
                {
                    if (IsAdjustByStaff.All(c => c == true))
                    {
                        return "Staff";
                    }
                    else if (IsAdjustByStaff.All(c => c == false))
                    {
                        return "Participant";
                    }
                    else if (IsAdjustByStaff.Any(c => c == true) && IsAdjustByStaff.Any(c => c == false))
                    {
                        return "Both";
                    }
                }

                return string.Empty;
            }
        }
        public string VendorNumber { get; set; }//Transaction receiver Vendor number
        public int VendorId { get; set; }//to get rate group and group names
        public string VendorBusinessName { get; set; }//Transaction receiver Business name
        public string VendorGroupName { get; set; }
        public string VendorRateGroupName { get; set; }
        public string PostalCode { get; set; }

        public decimal OnRoadWeight { get; set; }
        public decimal OffRoadWeight { get; set; }
        public long CreatedUserId { get; set; }
        public bool IsGenerateTires { get; set; }
        [JsonIgnore]
        public List<bool> IsAdjustByStaff { get; set; }

        //public List<TransactionItem> TransactionItems { get; set; }
        [JsonIgnore]
        public List<TransactionItemListDetailViewModel> TireCountList { get; set; }

        public int PLT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.PLT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        public int MT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int AGLS
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.AGLS);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int IND
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.IND);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int SOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.SOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int MOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int LOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.LOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int GOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.GOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        #region OTSTM2-1064      
        private bool flagQtyOverDOT;
        public bool FlagQtyOverDOT
        {
            get
            {
                return flagQtyOverDOT;
            }
        }

        public void SetFlagQtyOverDOT(int cBTotalTireCount, int totalTireCountDOT)
        {
            if (totalTireCountDOT < 0)
            {
                flagQtyOverDOT = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (MOTR + LOTR + GOTR) >= totalTireCountDOT)
                {
                    flagQtyOverDOT = true;
                }
                else
                {
                    flagQtyOverDOT = false;
                }
            }
        }

        private bool flagQtyOverHIT;
        public bool FlagQtyOverHIT
        {
            get
            {
                return flagQtyOverHIT;
            }
        }

        public void SetFlagQtyOverHIT(int cBTotalTireCount, int totalTireCountHIT)
        {
            if (totalTireCountHIT < 0)
            {
                flagQtyOverHIT = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountHIT)
                {
                    flagQtyOverHIT = true;
                }
                else
                {
                    flagQtyOverHIT = false;
                }
            }
        }

        private bool flagQtyOverPTR;
        public bool FlagQtyOverPTR
        {
            get
            {
                return flagQtyOverPTR;
            }
        }

        public void SetFlagQtyOverPTR(int cBTotalTireCount, int totalTireCountPTR)
        {
            if (totalTireCountPTR < 0)
            {
                flagQtyOverPTR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountPTR)
                {
                    flagQtyOverPTR = true;
                }
                else
                {
                    flagQtyOverPTR = false;
                }
            }
        }

        private bool flagQtyOverRTR;
        public bool FlagQtyOverRTR
        {
            get
            {
                return flagQtyOverRTR;
            }
        }

        public void SetFlagQtyOverRTR(int cBTotalTireCount, int totalTireCountRTR)
        {
            if (totalTireCountRTR < 0)
            {
                flagQtyOverRTR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountRTR)
                {
                    flagQtyOverRTR = true;
                }
                else
                {
                    flagQtyOverRTR = false;
                }
            }
        }

        private bool flagQtyOverSTC;
        public bool FlagQtyOverSTC
        {
            get
            {
                return flagQtyOverSTC;
            }
        }

        public void SetFlagQtyOverSTC(int cBTotalTireCount, int totalTireCountSTC)
        {
            if (totalTireCountSTC < 0)
            {
                flagQtyOverSTC = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountSTC)
                {
                    flagQtyOverSTC = true;
                }
                else
                {
                    flagQtyOverSTC = false;
                }
            }
        }

        private bool flagQtyOverTCR;
        public bool FlagQtyOverTCR
        {
            get
            {
                return flagQtyOverTCR;
            }
        }

        public void SetFlagQtyOverTCR(int cBTotalTireCount, int totalTireCountTCR)
        {
            if (totalTireCountTCR < 0)
            {
                flagQtyOverTCR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountTCR)
                {
                    flagQtyOverTCR = true;
                }
                else
                {
                    flagQtyOverTCR = false;
                }
            }
        }

        private bool flagQtyOverUCR;
        public bool FlagQtyOverUCR
        {
            get
            {
                return flagQtyOverUCR;
            }
        }

        public void SetFlagQtyOverUCR(int cBTotalTireCount, int totalTireCountUCR)
        {
            if (totalTireCountUCR < 0)
            {
                flagQtyOverUCR = false;
            }
            else
            {
                if ((cBTotalTireCount == 1) && (PLT + MT + AGLS + IND + SOTR + MOTR + LOTR + GOTR) >= totalTireCountUCR)
                {
                    flagQtyOverUCR = true;
                }
                else
                {
                    flagQtyOverUCR = false;
                }
            }
        }
        #endregion

        public string ProcessingStatus { get; set; }

        public bool MobileFormat { get; set; }
        ///DOT
        public decimal? ScaleWeight { get; set; }
        public decimal EstWeight { get; set; }

        ///UCR

        ///HIT

        ///STC
        public string NameofGroupIndividual { get; set; }
        public string EventNumber { get; set; } //OTSTM2-1215

        ///PTR
        public string ScaleTicketNbr { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal? Variance
        {
            get
            {
                if (0 != EstWeight)
                    return (((this.ScaleWeight - this.EstWeight) / (this.EstWeight)) * 100);
                else
                    return 0;
            }
            set { Variance = value; }
        }



        ///RTR
        public string MarketLocation { get; set; }
        public string TireMarketType { get; set; }
        public string BuyerName { get; set; }
        public string InvoiceNbr { get; set; }
        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }
    }
}
