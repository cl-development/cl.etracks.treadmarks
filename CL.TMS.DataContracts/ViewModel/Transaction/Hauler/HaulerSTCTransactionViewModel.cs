﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using FluentValidation.Attributes;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class HaulerSTCTransactionViewModel : HaulerTransactionViewModel
    {
        public string EventNumber { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }   

        public HaulerSTCTransactionViewModel() : base()
        {            
        }

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.STC;
            FormDate = new TransactionFormDateViewModel();
            TireTypeList = new List<TransactionTireTypeViewModel>();
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Paper Form", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();

            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "Canada" };
        }
    }
}
