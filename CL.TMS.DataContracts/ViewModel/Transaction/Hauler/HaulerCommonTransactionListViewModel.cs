﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class HaulerCommonTransactionListViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        public HaulerCommonTransactionListViewModel()
        {
            this.TransactionListSecurity = new TransactionListSecurityViewModel();
        }

        ///TCR
        public Guid TransactionId { get; set; }
        public string DeviceName { get; set; }
        public string Badge { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TransactionFriendlyId { get; set; }//TCR #, DOT #, HIT #...
        public string ReviewStatus { get; set; }
        //public string Adjustment { get; set; }
        //OTSTM2-416
        public List<bool> IsAdjustByStaff { get; set; }
        public string Adjustment
        {
            get
            {
                if (0 < IsAdjustByStaff.Count)
                {
                    if (IsAdjustByStaff.All(c => c == true))
                    {
                        return "Staff";
                    }
                    else if (IsAdjustByStaff.All(c => c == false))
                    {
                        return "Participant";
                    }
                    else if (IsAdjustByStaff.Any(c => c == true) && IsAdjustByStaff.Any(c => c == false))
                    {
                        return "Both";
                    }
                }

                return string.Empty;
            }
        }
        public string VendorNumber { get; set; }//Transaction receiver Vendor number
        public string VendorBusinessName { get; set; }//Transaction receiver Business name
        public int VendorId { get; set; }
        public string VendorGroupName { get; set; }
        public string VendorRateGroupName { get; set; }
        public string PostalCode { get; set; }
        public decimal OnRoadWeight { get; set; }
        public decimal OffRoadWeight { get; set; }
        public long CreatedUserId { get; set; }
        public bool IsGenerateTires { get; set; }
        public bool MobileFormat { get; set; }

        ///DOT
        public decimal? ScaleWeight { get; set; }
        public decimal EstWeight { get; set; }

        ///UCR
        public string Sitename { get; set; }
        //public string ReasonNotEligible { get; set; }//TransactionNotes
        public int PaymentEligible { get; set; }
        public string PaymentEligibleOther { get; set; }
        public string ReasonNotEligible
        {
            get
            {
                switch (this.PaymentEligible)
                {
                    case 1:
                        return TreadMarksConstants.TransactionPaymentEligi1;
                    case 2:
                        return TreadMarksConstants.TransactionPaymentEligi2;
                    case 3:
                        return TreadMarksConstants.TransactionPaymentEligi3;
                    case 4:
                        return this.PaymentEligibleOther ?? string.Empty;
                    default:
                        break;
                }
                return string.Empty;
            }
        }

        ///HIT

        ///STC
        public string NameofGroupIndividual { get; set; }
        public string EventNumber { get; set; } //OTSTM2-1214

        ///PTR
        public string ScaleTicketNbr { get; set; }

        public Decimal Variance
        {
            get
            {
                if (0 != EstWeight)
                    return Convert.ToDecimal((((this.ScaleWeight - this.EstWeight) / (this.EstWeight)) * 100));
                else
                    return 0;
            }
            set { Variance = value; }
        }


        ///RTR
        public string MarketLocation { get; set; }
        public string TireMarketType { get; set; }
        public string BuyerName { get; set; }
        public string InvoiceNbr { get; set; }
        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }

        [JsonIgnore]
        public List<ScaleTicket> ScaleTickets { get; set; }
        //public List<TransactionItem> TransactionItems { get; set; }
        [JsonIgnore]
        public List<TransactionItemListDetailViewModel> TireCountList { get; set; }
        public int PLT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.PLT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        public int MT
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int AGLS
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.AGLS);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int IND
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.IND);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int SOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.SOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int MOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.MOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int LOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.LOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int GOTR
        {
            get
            {
                var pltItem = TireCountList.FirstOrDefault(c => c.ShortName == TreadMarksConstants.GOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
    }
}
