﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using FluentValidation.Attributes;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class HaulerRTRTransactionViewModel : HaulerTransactionViewModel
    {
        public TransactionTireMarketViewModel TireMarketInfo { get; set; }
        public string InvoiceNumber { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<string> CountryList { get; set; }
        
        public HaulerRTRTransactionViewModel() : base()
        {            
        }

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.RTR;
            FormDate = new TransactionFormDateViewModel();
            TireTypeList = new List<TransactionTireTypeViewModel>();
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Paper Form", DocumentFormat = 3, Required = true },
                new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = "Bill of Lading", DocumentFormat = 3, Required = false },
                new TransactionSupportingDocumentViewModel() { UniqueId = 3, DocumentName = "Invoice", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();

            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "Canada", Province = "Ontario" };

            CountryList = new List<string>();

            TireMarketInfo = new TransactionTireMarketViewModel() { Location = "Canada: Ontario" , Type = 1 };
        }
    }
}
