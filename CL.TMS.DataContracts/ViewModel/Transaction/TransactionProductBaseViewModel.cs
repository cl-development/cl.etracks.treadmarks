﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionProductBaseViewModel : ITransactionProductBaseViewModel
    { 
        public int? ProductTypeId { get; set; }        
        public Decimal? ProductRate { get; set; }
    }
}
