﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionAdjViewModel : ITransactionAdjViewModel
    {
        public bool Preview { get; set; }
        public int TransactionId { get; set; }
        public long TransactionNumber { get; set; }
        public Guid TransactionGuid { get; set; }
        public string TransactionType { get; set; }
        public List<TransactionSupportingDocumentViewModel> SupportingDocumentList { get; set; }
        public List<TransactionViewSupportingDocumentAttachmentViewModel> AttachmentList { get; set; }
        public List<TransactionFileUploadModel> UploadDocumentList { get; set; }
        public string AdjustmentNote { get; set; }        
    }
}
