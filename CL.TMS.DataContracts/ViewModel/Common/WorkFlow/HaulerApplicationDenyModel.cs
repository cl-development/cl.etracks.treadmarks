﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.WorkFlow
{
    public class HaulerApplicationDenyModel
    {
        public string applicationId { get; set; }
        public string[] validationErrorMessages { get; set; }
    }
}
