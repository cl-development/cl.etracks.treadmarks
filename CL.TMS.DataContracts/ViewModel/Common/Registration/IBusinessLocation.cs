﻿using CL.TMS.DataContracts.ViewModel.Hauler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.Registration
{
    public interface IBusinessLocation
    {
        BusinessRegistrationModel BusinessLocation { get; set; }
    }
}
