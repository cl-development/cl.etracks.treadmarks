﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ContactAddressModel : AddressModelBase
    {
        public override string AddressType()
        {
            return "Contact";

        }
    }
}
