﻿namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class CommonVendorGroupModel
    {
        public int RateGroupId { get; set; }
        public int GroupId { get; set; }
        public int VendorId { get; set; }
        public string RateGroupName { get; set; }
        public string GroupName { get; set; }
        public string RateGroupCategory { get; set; }
        public string GroupType { get; set; }

    }
}