﻿namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class StewardApplicationEmailModel
    {
        public string PrimaryContactFirstName { get; set; }
        public string PrimaryContactLastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }
        public string BusinessLegalName { get; set; }
        public string EMail { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string ProvinceState { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string InvitationToken { get; set; }
    }
}
