﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class BankingInformationRegistrationModel
    {      
        public int ID { get; set; }
        public string BankName { get; set; }
        public string TransitNumber { get; set; }
        public string BankNumber { get; set; }
        public string AccountNumber { get; set; }
        public string BankAddress { get; set; }
        public bool EmailPaymentTransferNotification { get; set; }
        public bool NotifyToPrimaryContactEmail { get; set; }
        public string Email { get; set; }
        public string CCEmail { get; set; }        
        public string Token { get; set; }
        public int CountOfUploadedDocuments { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
        public string SupportingDocumentsOption { get; set; }
        public int VendorId { get; set; }
        public string RegistrationNumber { get; set; }
        public string OperatingName { get; set; }
        public int applicationId { get; set; }
        public string ReviewStatus { get; set; }

        public string AccountNumberMask { get; set; }
        
    }
}
