﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Common.ApplicationPartialViewData
{
    public class CommonPartialView
    {
        public string prefix { get; set; }
        public string view { get; set; }        
        public IList<string> InvalidFormFieldList { get; set; }
        public bool? IsParticipant { get; set; }
        public ApplicationStatusEnum Status { get; set; }
        public ApplicationTypes Type { get; set; }
    }
}
