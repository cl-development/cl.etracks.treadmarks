﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class BusinessAddressModel : AddressModelBase
    {
        public override string AddressType()
        {
            return "Business";
        }
    }
}
