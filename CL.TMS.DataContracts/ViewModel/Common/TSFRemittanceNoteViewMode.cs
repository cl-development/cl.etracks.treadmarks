﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class TSFRemittanceNoteViewMode:BaseDTO<int>
    {
        public int ClaimId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserId { get; set; }

        public string AddedBy { get; set; }

        public string CreatedDateString
        {
            get { return CreatedDate.ToString("yyyy-MM-dd hh:mm tt"); }
        }
    }
}
