﻿using CL.TMS.Common;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class BaseItemModel
    {
        public int ItemValue { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public string SecurityResultType { get; set; }
        public int DisplayOrder { get; set; }

    }
}
