﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ActiveInactiveViewModel: RegistrationModelBase
    {
        public ActiveInactiveViewModel()
        {
            RegistrantActive = new VendorActiveHistory();
            RegistrantInactive = new VendorActiveHistory();
            CurrentActiveHistory = new VendorActiveHistory();
        }
        public VendorActiveHistory RegistrantActive { get; set; }
        public VendorActiveHistory RegistrantInactive { get; set; }
        public VendorActiveHistory CurrentActiveHistory { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
