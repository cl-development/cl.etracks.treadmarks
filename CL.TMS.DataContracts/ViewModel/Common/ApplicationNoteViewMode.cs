﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ApplicationNoteViewMode : InternalNotesBase
    {
        public int? ApplicationId { get; set; }
        public int? VendorId { get; set; }
        public int? CustomerId { get; set; }        
    }
}
