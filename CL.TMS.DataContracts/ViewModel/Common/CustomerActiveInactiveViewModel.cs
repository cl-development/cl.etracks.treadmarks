﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class CustomerActiveInactiveViewModel: RegistrationModelBase
    {
        public CustomerActiveInactiveViewModel()
        {
            RegistrantActive = new CustomerActiveHistory();
            RegistrantInactive = new CustomerActiveHistory();
            CurrentActiveHistory = new CustomerActiveHistory();
        }
        public CustomerActiveHistory RegistrantActive { get; set; }
        public CustomerActiveHistory RegistrantInactive { get; set; }
        public CustomerActiveHistory CurrentActiveHistory { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
