﻿using CL.TMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public abstract class AddressModel //: InvalidFormFieldModel, IAddress
    {

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        abstract public string AddressType();


        //public AddressModel()
        //{
        //    //Contacts = new List<IContact>();
        //    //VendorStorageSites = new List<IVendorStorageSite>();
        //}


        //public int ID { get; set; }
        //public AddressTypeModel AddressType { get; set; }
        //public string AddressTypeName { get; set; }
        //public int AddressTypeID { get; set; }
        //public string Address1 { get; set; }
        //public string Address2 { get; set; }
        //public string Address3 { get; set; }
        //public string City { get; set; }
        //public string Province { get; set; }
        //public string PostalCode { get; set; }
        //public string Country { get; set; }
        //public string Phone { get; set; }
        //public string Fax { get; set; }
        //public string Email { get; set; }
        //public string Ext { get; set; }
        ////public string AgreementText { get; set; }
        ////public string AgreementAcceptedByFullName { get; set; }
        ////public Nullable<bool> AgreementAcceptedCheck { get; set; }
        ////public string UserIP { get; set; }
        ////public string Status { get; set; }

        //public Nullable<decimal> GPSCoordinate1 { get; set; }
        //public Nullable<decimal> GPSCoordinate2 { get; set; }
        //public IList<IContact> Contacts { get; set; }
        //public IList<IVendorStorageSite> VendorStorageSites { get; set; }


    }
}
