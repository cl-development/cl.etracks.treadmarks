﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ActivityShowTypeModel
    {
        
        public string Resource { get; set; }

        
        public long EntityID { get; set; }

        
        public string EntityTable { get; set; }

        
        public string EntityName { get; set; }

        
        public long SubEntityID { get; set; }

        
        public string SubEntityTable { get; set; }

        
        public string SubEntityName { get; set; }

        
        public int ActivityLevelID { get; set; }

        
        public int ActivityLevelValue { get; set; }

        
        public string ActivityLevelCode { get; set; }

        
        public long UserID { get; set; }

        
        public string UserName { get; set; }

        //
        //public int MaxRows { get; set; }

        
        public int PageSize { get; set; }

        //zero based page number
        
        public int PageNumber { get; set; }

        
        public string SearchValue { get; set; }
    }
}
