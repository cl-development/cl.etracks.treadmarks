﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class TransactionFileUploadModel
    {
        public string TransactionId { get; set; }

        public int? AttachmentId { get; set; }
        public string Type { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
