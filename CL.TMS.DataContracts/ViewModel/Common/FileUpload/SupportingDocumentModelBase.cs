﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class SupportingDocumentModelBase
    {        
        public int ID { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Description { get; set; }
        public long Size { get; set; }
        public string UniqueName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public string ValidationMessage { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
    }
}
