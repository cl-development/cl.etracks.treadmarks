﻿using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class FileUploadServiceModel
    {
        public int TransactionID { get; set; }        
        public HttpPostedFileBase[] Files { get; set; }
        public FileUploadSectionName _fileUploadSectionName { get; set; }
        public bool bBankInfo { get; set; }

        public IEnumerable<AttachmentModel> SaveUploadedFilesAndCreateAttachmentModels(string uploadRepositoryPath, string createdBy)
        {
            var attachments = new List<AttachmentModel>();

            try
            {
                if (this.Files != null && this.Files.Length != 0)
                {
                    string uploadAbsolutePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath(uploadRepositoryPath, this._fileUploadSectionName, this.TransactionID.ToString());

                    foreach (var file in this.Files)
                    {
                        string uniqueFileName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                        CL.TMS.Common.Helper.FileUploadHandler.SaveUploadFile(file, uploadAbsolutePath, uniqueFileName, true);

                        AttachmentModel attachment = new AttachmentModel()
                        {
                            FileType = file.ContentType,
                            FileName = file.FileName,
                            Description = string.Empty,
                            Size = file.ContentLength,
                            UniqueName = uniqueFileName,
                            IsValid = true,
                            CreatedBy = createdBy,
                            CreatedDate = DateTime.Now,
                            FilePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadFileRelativePath(this._fileUploadSectionName, this.TransactionID.ToString(), uniqueFileName),
                            IsBankingRelated = this.bBankInfo,
                        };

                        attachments.Add(attachment);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return attachments;
        }        

        public static JQueryFileUploadModel GetUploadedFiles(Func<int, bool, IEnumerable<AttachmentModel>> getUploadFilesOperation, int transactionID, string fileUploadPreviewDomainName, string uploadRepositoryPath, bool bBankInfo)
        {
            JQueryFileUploadModel jQueryFileUploadModel = new JQueryFileUploadModel();

            if (getUploadFilesOperation != null)
            {
                IEnumerable<AttachmentModel> attachments = getUploadFilesOperation.Invoke(transactionID, bBankInfo);

                jQueryFileUploadModel = ComposeJQueryFileUploadResponseModelFromAttachments(attachments, transactionID, fileUploadPreviewDomainName, uploadRepositoryPath);
            }

            return jQueryFileUploadModel;
        }

        public static JQueryFileUploadModel DeleteUploadedFile(Action<int, string> deleteUploadFilesOperation, Func<int, bool, IEnumerable<AttachmentModel>> getUploadFilesOperation, int transactionID, string fileName, string fileUploadPreviewDomainName, string uploadRepositoryPath, bool bBankInfo, bool bTempFile = false)
        {
            JQueryFileUploadModel jQueryFileUploadModel = new JQueryFileUploadModel();

            if (deleteUploadFilesOperation != null)
            {
                deleteUploadFilesOperation.Invoke(transactionID, fileName);
                IEnumerable<AttachmentModel> attachments = getUploadFilesOperation.Invoke(transactionID, bBankInfo);

                jQueryFileUploadModel = ComposeJQueryFileUploadResponseModelFromAttachments(attachments, transactionID, fileUploadPreviewDomainName, uploadRepositoryPath);
            }

            return jQueryFileUploadModel;
        }

        public static JQueryFileUploadModel ComposeJQueryFileUploadResponseModelFromAttachments(IEnumerable<AttachmentModel> attachments, int transactionID, string fileUploadPreviewDomainName, string uploadRepositoryPath)
        {
            var jQueryFileUploadResponseModel = new JQueryFileUploadModel();

            foreach (var attachment in attachments)
            {
                var jQueryFileUploadFileModel = CreateJQueryFileUploadFileModelList(attachment, transactionID, fileUploadPreviewDomainName, uploadRepositoryPath);
                jQueryFileUploadResponseModel.files.Add(jQueryFileUploadFileModel);
            }

            return jQueryFileUploadResponseModel;
        }

        private static JQueryFileUploadFileModel CreateJQueryFileUploadFileModelList(AttachmentModel attachment, int transactionID, string fileUploadPreviewDomainName, string uploadRepositoryPath)
        {
            var jQueryFileUploadFile = new JQueryFileUploadFileModel()
            {
                id = attachment.ID,
                transactionId = transactionID,
                encodedFileName = Token.EncodeTo64(attachment.UniqueName),
                name = attachment.FileName,
                type = attachment.FileType,
                size = attachment.Size,
                isValid = (attachment.IsValid.HasValue ? attachment.IsValid.Value : true),
                validationMessage = attachment.ValidationMessage,
                createdOn = attachment.CreatedDate.ToString("yyyy-MM-dd HH:mm"),
                description = attachment.Description,
                thumbnail_url = Path.Combine(fileUploadPreviewDomainName, attachment.FilePath),
                url = Path.Combine(fileUploadPreviewDomainName, attachment.FilePath),
                delete_type = "GET",
            };

            return jQueryFileUploadFile;
        }        
    }
}
