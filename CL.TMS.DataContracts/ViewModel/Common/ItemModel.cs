﻿namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ItemModel<TKey>
    {
        public int? ItemID { get; set; }
        public string ItemName { get; set; }
        public TKey ItemValue { get; set; }
    }
}