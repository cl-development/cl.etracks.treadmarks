﻿using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class VendorClaimRateGroupInfo : CommonVendorGroupModel
    {
        public DateTime ClaimPeriod { get; set; }
        public string ClaimPeriodName
        {
            get
            {
                return ClaimPeriod.ToString("MMM, yyyy");
            }
        }
        public decimal ProcessorOnRoadRate { get; set; }
        public decimal ProcessorOffRoadRate { get; set; }
    }
}
