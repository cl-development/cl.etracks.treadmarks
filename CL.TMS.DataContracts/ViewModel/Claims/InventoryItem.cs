﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class InventoryItem
    {
        public int ItemId { get; set; }
        public bool IsEligible { get; set; }
        public int Qty { get; set; }
        public decimal Weight { get; set; }

        public decimal ActualWeight { get; set; }
        public bool Direction { get; set; }
        public bool IsAdjustedItem { get; set; }
    }
}
