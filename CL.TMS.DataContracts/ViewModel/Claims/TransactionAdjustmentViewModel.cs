﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class TransactionAdjustmentViewModel:BaseDTO<int>
    {
        public int TransactionId { get; set; }
        public int TransactionAdjustmentId { get; set; }
        public string TransactionType { get; set; }
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public string StartedBy { get; set; }
        public string Status { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public int CreatedVendorId { get; set; }
        public bool IsAdjustByStaff { get; set; }

        /// <summary>
        /// 1 - Recall
        /// 2 - Accept, Reject
        /// 3 - Reject
        /// 4 - Accept
        /// </summary>
        public int ActionMenu { get; set; }
    }
}
