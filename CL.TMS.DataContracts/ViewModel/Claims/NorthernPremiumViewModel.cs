﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class NorthernPremiumViewModel
    {
        #region N1 Row
        public decimal N1SturgeonFallWeightOnRoad { get; set; }
        public decimal N1SturgeonFallRateOnRoad { get; set; }
        public decimal N1SturgeonFallPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1SturgeonFallRateOnRoad * N1SturgeonFallWeightOnRoad, 2) : Math.Round(N1SturgeonFallRateOnRoad * N1SturgeonFallWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N1SturgeonFallWeightOffRoad { get; set; }
        public decimal N1SturgeonFallRateOffRoad { get; set; }
        public decimal N1SturgeonFallPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1SturgeonFallRateOffRoad * N1SturgeonFallWeightOffRoad, 2) : Math.Round(N1SturgeonFallRateOffRoad * N1SturgeonFallWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }
        #endregion Sturgeon Falls
        #region South
        public decimal N1SouthWeightOnRoad { get; set; }
        public decimal N1SouthRateOnRoad { get; set; }
        public decimal N1SouthPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1SouthRateOnRoad * N1SouthWeightOnRoad, 2) : Math.Round(N1SouthRateOnRoad * N1SouthWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N1SouthWeightOffRoad { get; set; }
        public decimal N1SouthRateOffRoad { get; set; }
        public decimal N1SouthPremiumOffRoad
        {
            get
            {
                return N1SouthRateOffRoad * N1SouthWeightOffRoad;
            }
        }
        #endregion South

        #region N1 Total Weight
        public decimal N1TotalWeightOnRoad
        {
            get
            {
                return N1SturgeonFallWeightOnRoad + N1SouthWeightOnRoad;
            }
        }

        public decimal N1TotalPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1SturgeonFallPremiumOnRoad + N1SouthPremiumOnRoad, 2) : Math.Round(N1SturgeonFallPremiumOnRoad + N1SouthPremiumOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N1ToalWeightOffRoad
        {
            get
            {
                return N1SturgeonFallWeightOffRoad + N1SouthWeightOffRoad;
            }
        }

        public decimal N1TotalPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1SturgeonFallPremiumOffRoad + N1SouthPremiumOffRoad, 2) : Math.Round(N1SturgeonFallPremiumOffRoad + N1SouthPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }
        #endregion Total Weight

        #region N2 Row
        public decimal N2SturgeonFallWeightOnRoad { get; set; }
        public decimal N2SturgeonFallRateOnRoad { get; set; }
        public decimal N2SturgeonFallPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SturgeonFallRateOnRoad * N2SturgeonFallWeightOnRoad, 2) : Math.Round(N2SturgeonFallRateOnRoad * N2SturgeonFallWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N2SturgeonFallWeightOffRoad { get; set; }
        public decimal N2SturgeonFallRateOffRoad { get; set; }
        public decimal N2SturgeonFallPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SturgeonFallRateOffRoad * N2SturgeonFallWeightOffRoad, 2) : Math.Round(N2SturgeonFallRateOffRoad * N2SturgeonFallWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N2SouthWeightOnRoad { get; set; }
        public decimal N2SouthRateOnRoad { get; set; }
        public decimal N2SouthPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SouthRateOnRoad * N2SouthWeightOnRoad, 2) : Math.Round(N2SouthRateOnRoad * N2SouthWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N2SouthWeightOffRoad { get; set; }
        public decimal N2SouthRateOffRoad { get; set; }
        public decimal N2SouthPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SouthRateOffRoad * N2SouthWeightOffRoad, 2) : Math.Round(N2SouthRateOffRoad * N2SouthWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }


        #endregion South

        #region Total N2
        public decimal N2TotalWeightOnRoad
        {
            get
            {
                return N2SturgeonFallWeightOnRoad + N2SouthWeightOnRoad;
            }
        }

        public decimal N2TotalPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SturgeonFallPremiumOnRoad + N2SouthPremiumOnRoad, 2) : Math.Round(N2SturgeonFallPremiumOnRoad + N2SouthPremiumOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N2ToalWeightOffRoad
        {
            get
            {
                return N2SturgeonFallWeightOffRoad + N2SouthWeightOffRoad;
            }
        }

        public decimal N2TotalPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2SturgeonFallPremiumOffRoad + N2SouthPremiumOffRoad, 2) : Math.Round(N2SturgeonFallPremiumOffRoad + N2SouthPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }
        #endregion N2


        #region N3 Row
        public decimal N3SturgeonFallWeightOnRoad { get; set; }
        public decimal N3SturgeonFallRateOnRoad { get; set; }
        public decimal N3SturgeonFallPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SturgeonFallRateOnRoad * N3SturgeonFallWeightOnRoad, 2) : Math.Round(N3SturgeonFallRateOnRoad * N3SturgeonFallWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N3SturgeonFallWeightOffRoad { get; set; }
        public decimal N3SturgeonFallRateOffRoad { get; set; }
        public decimal N3SturgeonFallPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SturgeonFallRateOffRoad * N3SturgeonFallWeightOffRoad, 2) : Math.Round(N3SturgeonFallRateOffRoad * N3SturgeonFallWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N3SouthWeightOnRoad { get; set; }
        public decimal N3SouthRateOnRoad { get; set; }
        public decimal N3SouthPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SouthRateOnRoad * N3SouthWeightOnRoad, 2) : Math.Round(N3SouthRateOnRoad * N3SouthWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N3SouthWeightOffRoad { get; set; }
        public decimal N3SouthRateOffRoad { get; set; }
        public decimal N3SouthPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SouthRateOffRoad * N3SouthWeightOffRoad, 2) : Math.Round(N3SouthRateOffRoad * N3SouthWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        #endregion

        #region N3 Total
        public decimal N3TotalWeightOnRoad
        {
            get
            {
                return N3SturgeonFallWeightOnRoad + N3SouthWeightOnRoad;
            }
        }

        public decimal N3TotalPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SturgeonFallPremiumOnRoad + N3SouthPremiumOnRoad, 2) : Math.Round(N3SturgeonFallPremiumOnRoad + N3SouthPremiumOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N3ToalWeightOffRoad
        {
            get
            {
                return N3SturgeonFallWeightOffRoad + N3SouthWeightOffRoad;
            }
        }

        public decimal N3TotalPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3SturgeonFallPremiumOffRoad + N3SouthPremiumOffRoad, 2) : Math.Round(N3SturgeonFallPremiumOffRoad + N3SouthPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        #endregion

        #region N4 Row
        public decimal N4SturgeonFallWeightOnRoad { get; set; }
        public decimal N4SturgeonFallRateOnRoad { get; set; }
        public decimal N4SturgeonFallPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SturgeonFallRateOnRoad * N4SturgeonFallWeightOnRoad, 2) : Math.Round(N4SturgeonFallRateOnRoad * N4SturgeonFallWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N4SturgeonFallWeightOffRoad { get; set; }
        public decimal N4SturgeonFallRateOffRoad { get; set; }
        public decimal N4SturgeonFallPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SturgeonFallRateOffRoad * N4SturgeonFallWeightOffRoad, 2) : Math.Round(N4SturgeonFallRateOffRoad * N4SturgeonFallWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N4SouthWeightOnRoad { get; set; }
        public decimal N4SouthRateOnRoad { get; set; }
        public decimal N4SouthPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SouthRateOnRoad * N4SouthWeightOnRoad, 2) : Math.Round(N4SouthRateOnRoad * N4SouthWeightOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N4SouthWeightOffRoad { get; set; }
        public decimal N4SouthRateOffRoad { get; set; }
        public decimal N4SouthPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SouthRateOffRoad * N4SouthWeightOffRoad, 2) : Math.Round(N4SouthRateOffRoad * N4SouthWeightOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        #endregion

        #region N4 Total
        public decimal N4TotalWeightOnRoad
        {
            get
            {
                return N4SturgeonFallWeightOnRoad + N4SouthWeightOnRoad;
            }
        }

        public decimal N4TotalPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SturgeonFallPremiumOnRoad + N4SouthPremiumOnRoad, 2) : Math.Round(N4SturgeonFallPremiumOnRoad + N4SouthPremiumOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N4ToalWeightOffRoad
        {
            get
            {
                return N4SturgeonFallWeightOffRoad + N4SouthWeightOffRoad;
            }
        }

        public decimal N4TotalPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4SturgeonFallPremiumOffRoad + N4SouthPremiumOffRoad, 2) : Math.Round(N4SturgeonFallPremiumOffRoad + N4SouthPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        #endregion

        public decimal TotalPremiumOnRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1TotalPremiumOnRoad + N2TotalPremiumOnRoad + N3TotalPremiumOnRoad + N4TotalPremiumOnRoad, 2) : Math.Round(N1TotalPremiumOnRoad + N2TotalPremiumOnRoad + N3TotalPremiumOnRoad + N4TotalPremiumOnRoad, 2, MidpointRounding.AwayFromZero);
            }
        }
        public decimal TotalPremiumOffRoad
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1TotalPremiumOffRoad + N2TotalPremiumOffRoad + N3TotalPremiumOffRoad + N4TotalPremiumOffRoad, 2) : Math.Round(N1TotalPremiumOffRoad + N2TotalPremiumOffRoad + N3TotalPremiumOffRoad + N4TotalPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalPremium
        {
            get
            {
                return UsingOldRounding ? Math.Round(TotalPremiumOnRoad + TotalPremiumOffRoad, 2) : Math.Round(TotalPremiumOnRoad + TotalPremiumOffRoad, 2, MidpointRounding.AwayFromZero);
            }
        }

        public bool UsingOldRounding { get; set; }
    }
}
