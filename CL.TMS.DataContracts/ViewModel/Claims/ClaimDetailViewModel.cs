﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimDetailViewModel
    {
        public bool Direction { get; set; }
        public string TransactionType { get; set; }
        public bool IsEligible { get; set; }

        public decimal OnRoad
        {
            get; set;
        }

        public decimal OffRoad
        {
            get; set;
        }

        public decimal ActualTotal
        {
            get { return OnRoad + OffRoad; }
        }
        public decimal EstimatedOnRoad
        {
            get; set;
        }

        public decimal EstimatedOffRoad
        {
            get; set;
        }

        public decimal EstimatedToal
        {
            get { return EstimatedOnRoad + EstimatedOffRoad; }
        }
        public List<TransactionItem> TransactionItems { get; set; }

        public DomainEntities.Transaction Transaction { get; set; }

        public List<ScaleTicket> ScaleTickets { get; set; }

        public decimal ExtraDORTotal
        {
            get
            {
                decimal weight = 0M;

                if (Transaction.TransactionTypeCode == "DOR" && Transaction.MaterialType == 4) weight = (Transaction.OffRoadWeight ?? 0M) + (Transaction.OnRoadWeight ?? 0M);

                if (Transaction.TransactionTypeCode == "DOR" && (Transaction.MaterialType == 1 || Transaction.MaterialType == 2 || Transaction.MaterialType == 3))
                {
                    var st = ScaleTickets.SingleOrDefault(i => i.TransactionId == Transaction.ID);
                    if (st != null) weight = ((st.InboundWeight ?? 0M) - (st.OutboundWeight ?? 0M));
                }

                return weight;
            }
        }

        public decimal OnroadExtraDORForTireRims
        {
            get
            {
                decimal weight = 0M;

                if (Transaction.TransactionTypeCode == "DOR" && Transaction.MaterialType == 4)
                    weight = (Transaction.OnRoadWeight ?? 0M);
                return weight;
            }
        }

        public decimal OffroadExtraDORForTireRims
        {
            get
            {
                decimal weight = 0M;

                if (Transaction.TransactionTypeCode == "DOR" && Transaction.MaterialType == 4)
                    weight = (Transaction.OffRoadWeight ?? 0M);
                return weight;
            }
        }

        public int ClaimId { get; set; }

        public List<TransactionAdjustment> TransactionAdjustments { get; set; }

        #region Item Level distribution
        public decimal PLTActualWeight
        {
            get; set;
        }

        public decimal PLTAverageWeight
        {
            get; set;
        }

        public int PLTQty
        {
            get; set;
        }

        public decimal MTActualWeight
        {
            get; set;
        }

        public decimal MTAverageWeight
        {
            get; set;
        }

        public int MTQty
        {
            get; set;
        }
        public decimal AGLSActualWeight
        {
            get; set;
        }

        public decimal AGLSAverageWeight
        {
            get; set;
        }

        public int AGLSQty
        {
            get; set;
        }

        public decimal INDActualWeight
        {
            get; set;
        }

        public decimal INDAverageWeight
        {
            get; set;
        }

        public int INDQty
        {
            get; set;
        }

        public decimal SOTRActualWeight
        {
            get; set;
        }

        public decimal SOTRAverageWeight
        {
            get; set;
        }

        public int SOTRQty
        {
            get; set;
        }

        public decimal MOTRActualWeight
        {
            get; set;
        }

        public decimal MOTRAverageWeight
        {
            get; set;
        }

        public int MOTRQty
        {
            get; set;
        }

        public decimal LOTRActualWeight
        {
            get; set;
        }

        public decimal LOTRAverageWeight
        {
            get; set;
        }

        public int LOTRQty
        {
            get; set;
        }

        public decimal GOTRActualWeight
        {
            get; set;
        }

        public decimal GOTRAverageWeight
        {
            get; set;
        }

        public int GOTRQty
        {
            get; set;
        }
        #endregion
    }
}