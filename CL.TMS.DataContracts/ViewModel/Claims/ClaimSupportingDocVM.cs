﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimSupportingDocVM
    {
        public int Id { get; set; }
        public int ClaimId { get; set; }
        public string Category { get; set; }
        public string TypeCode { get; set; }
        public int DefinitionValue { get; set; }
        public string SelectedOption { get; set; }
        public string TypeDescription { get; set; }
    }
}
