﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimAdjustmentModalResult
    {
        public ClaimAdjustmentModalResult()
        {
            SelectedItem = new ClaimAdjustmentType();
        }
        public ClaimAdjustmentType SelectedItem { get; set; }
        public string Direction { get; set; }
        public string Eligibility { get; set; }
        public decimal Onroad { get; set; }

        public decimal Offroad { get; set; }
        public int PLT { get; set; }
        public int MT { get; set; }
        public int AGLS { get; set; }
        public int IND { get; set; }
        public int SOTR { get; set; }
        public int MOTR { get; set; }
        public int LOTR { get; set; }
        public int GOTR { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public int DirectionValue
        {
            get
            {
                if (Direction == "inbound")
                    return 1;
                if (Direction == "outbound")
                    return 2;
                return 0;
            }
        }

        public bool IsEligible
        {
            get
            {
                return (Eligibility == "eligible");
            }
        }

        public bool IsAdd { get; set; }

        public int AdjustmentId { get; set; }

        public int UnitType { get; set; }

        //OTSTM2-270 Internal Note
        public string InternalNote { get; set; }

        //OTSTM2-489 changes for yard count internal
        //adjustment
        public int? PLTBeforeAdj { get; set; }
        public int? MTBeforeAdj { get; set; }
        public int? AGLSBeforeAdj { get; set; }
        public int? INDBeforeAdj { get; set; }
        public int? SOTRBeforeAdj { get; set; }
        public int? MOTRBeforeAdj { get; set; }
        public int? LOTRBeforeAdj { get; set; }
        public int? GOTRBeforeAdj { get; set; }

        public int? PLTAfterAdj { get; set; }
        public int? MTAfterAdj { get; set; }
        public int? AGLSAfterAdj { get; set; }
        public int? INDAfterAdj { get; set; }
        public int? SOTRAfterAdj { get; set; }
        public int? MOTRAfterAdj { get; set; }
        public int? LOTRAfterAdj { get; set; }
        public int? GOTRAfterAdj { get; set; }
        
    }
}
