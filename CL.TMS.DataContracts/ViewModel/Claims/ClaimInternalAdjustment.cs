﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.DomainEntities;
using Newtonsoft.Json;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimInternalAdjustment:BaseDTO<int>
    {
        public int InternalAdjustmentId { get; set; }
        public DateTime AdjustmentDate { get; set; }

        public string AdjustmentType
        {
            get { return EnumHelper.GetEnumDescription(InternalAdjustmentType); }
        }
        public string AdjustmentBy { get; set; }

        public InternalAdjustmentType InternalAdjustmentType { get; set; }

        //OTSTM2-668    
        public string Note { get; set; }
        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.InternalNotes)
                    foreach (ClaimInternalAdjustmentNote item in this.InternalNotes)
                    {
                        if (null != item)
                        {
                            //sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                            sSum += (item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        //OTSTM2-668       
        [JsonIgnore]        
        public List<ClaimInternalAdjustmentNote> InternalNotes { get; set; }
    }
}
