﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims.Export
{
    public class CommonPaymentAdjustExportViewModel
    {
        public string RowName{ get; set;}
        public string Rate { get; set; }
        public string QTY { get; set; }
        public string Payment { get; set; }
    }
}
