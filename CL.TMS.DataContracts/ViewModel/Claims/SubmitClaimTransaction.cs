﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class SubmitClaimTransaction
    {
        public int TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public int? IncomingVendorId { get; set; }
        public int? OutgoingVendorId { get; set; }
        public string ProcessingStatus { get; set; }
        public int IncomingVendorType { get; set; }
        public string IncomingVendorNumber { get; set; }
        public int OutgoingVendorType { get; set; }
        public string OutgoingVendorNumber { get; set; }
    }
}
