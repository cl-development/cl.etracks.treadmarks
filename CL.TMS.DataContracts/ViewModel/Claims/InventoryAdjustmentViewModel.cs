﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class InventoryAdjustmentViewModel
    {
 
        public decimal EligibleAdjustmentInboundOnRoad { get; set; }
        public decimal EligibleAdjustmentInboundOffRoad { get; set; }

        public decimal EligibleAdjustmentInboundTotal
        {
            get
            {
                return EligibleAdjustmentInboundOnRoad + EligibleAdjustmentInboundOffRoad;
            }
        }

        public decimal IneligibleAdjustmentInboundOnRoad { get; set; }
        public decimal IneligibleAdjustmentInboundOffRoad { get; set; }

        public decimal IneligibleAdjustmentInboundTotal
        {
            get
            {
                return IneligibleAdjustmentInboundOnRoad + IneligibleAdjustmentInboundOffRoad;
            }
        }

        public decimal EligibleAdjustmentOutboundOnRoad { get; set; }
        public decimal EligibleAdjustmentOutboundOffRoad { get; set; }

        public decimal EligibleAdjustmentOutboundTotal
        {
            get
            {
                return EligibleAdjustmentOutboundOnRoad + EligibleAdjustmentOutboundOffRoad;
            }
        }

        public decimal IneligibleAdjustmentOutboundOnRoad { get; set; }
        public decimal IneligibleAdjustmentOutboundOffRoad { get; set; }

        public decimal IneligibleAdjustmentOutboundTotal
        {
            get
            {
                return IneligibleAdjustmentOutboundOnRoad + IneligibleAdjustmentOutboundOffRoad;
            }
        }

        public decimal EligibleAdjustmentOverallOnRoad{ get; set;}
        public decimal EligibleAdjustmentOnRoad
        {
            get { return EligibleAdjustmentOverallOnRoad; }
        }

        public decimal EligibleAdjustmentOverallOffRoad { get; set; }
        public decimal EligibleAdjustmentOffRoad
        {
            get { return EligibleAdjustmentOverallOffRoad; }
        }

        public decimal EligibleAdjustmentTotal
        {
            get { return EligibleAdjustmentOnRoad + EligibleAdjustmentOffRoad; }
        }

        public decimal IneligibleAdjustmentOverallOnRoad { get; set; }
        public decimal IneligibleAdjustmentOnRoad
        {
            get { return IneligibleAdjustmentOverallOnRoad; }
        }
        public decimal IneligibleAdjustmentOverallOffRoad { get; set; }

        public decimal IneligibleAdjustmentOffRoad
        {
            get { return IneligibleAdjustmentOverallOffRoad; }
        }

        public decimal IneligibleAdjustmentTotal
        {
            get { return IneligibleAdjustmentOnRoad + IneligibleAdjustmentOffRoad; }
        }

   
        public decimal WeightVariance { get; set; }
        public decimal SortYandCapacity { get; set; }

        public ItemRow ItemSubmission { get; set; }
        public ItemRow TireCountBalance { get; set; }
        public ItemRow TireCountBalanceWithoutAdj { get; set; }
        //public ItemRow TireCountBeforeAdj { get; set; }
        //public ItemRow TireCountAfterAdj { get; set; }

    }
}
