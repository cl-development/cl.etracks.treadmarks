﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class HaulerSubmitClaimViewModel: SubmitClaimModelBase
    {            
        public decimal EligibleClosingTotal { get; set; }
        public decimal IneligibleClosingTotal { get; set; }

        public decimal TCROnRoad { get; set; }
        public decimal TCROffRoad { get; set; }
        //DOT
        public decimal DOTOnRoad { get; set; }
        public decimal DOTOffRoad { get; set; }
        //STC
        public decimal STCOnRoad { get; set; }
        public decimal STCOffRoad { get; set; }
        //UCR
        public decimal UCROnRoad { get; set; }
        public decimal UCROffRoad { get; set; }
        //HIT
        public decimal HITOnRoad { get; set; }
        public decimal HITOffRoad { get; set; }


        //Outbound
        //PTR
        public decimal PTROnRoad{ get; set; }
        public decimal PTROffRoad{ get; set; }
        //HIT
        public decimal HITOutboundOnRoad{ get; set; }
        public decimal HITOutboundOffRoad{ get; set; }
        //RTR
        public decimal RTROnRoad{ get; set; }
        public decimal RTROffRoad { get; set; }

        //Sort Yard Submission
        public decimal PLT { get; set; }
        public decimal MT { get; set; }
        public decimal AGLS { get; set; }
        public decimal IND { get; set; }
        public decimal SOTR { get; set; }
        public decimal MOTR { get; set; }
        public decimal LOTR { get; set; }
        public decimal GOTR { get; set; }
    }
}
