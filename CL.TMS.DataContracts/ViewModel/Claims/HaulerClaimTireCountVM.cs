﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class HaulerClaimTireCountVM
    {
        public int VendorId { get; set; }
        public string RegistrationNumber { get; set; }
        public int ClaimId { get; set; }
        public DateTime StartDate { get; set; }
        public string ClaimStatus { get; set; }
        public ItemRow ItemTireCount { get; set; }
    }
}
