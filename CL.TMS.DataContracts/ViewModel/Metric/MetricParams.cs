﻿using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Metric
{
    public class MetricParams 
    {
        public int? CategoryID { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
