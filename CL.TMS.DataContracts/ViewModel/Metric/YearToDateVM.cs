﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Metric
{
    public class YearToDateVM  
    {
        public int Year { get; set; }
        public decimal PLT { get; set; } //C1
        public decimal MT { get; set; }  //C2
        public decimal OTR { get; set; }  //C3-c18
    }
}
