﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Metric
{
    public class TSFStatusTimeClassVM  
    {
        public string TimeClass { get; set; }      
        public decimal Amount { get; set; }
    }
}
