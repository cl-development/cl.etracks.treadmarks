﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.Validation;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.EmailSettings
{
    [Validator(typeof(EmailSettingsGeneralModelValidator))]
    public class EmailSettingsGeneralVM
    {
        //Email
        public string Email_BccApplicationPath { get; set; }
        public string Email_StewardApplicationApproveBCCEmailAddr { get; set; }
        public string Email_defaultFrom { get; set; }
        public bool Email_CBCCEmailClaimAndApplication { get; set; }
        public bool Email_CBCCEmailStewardAndRemittance { get; set; }
        public bool Email_CBDefaultFromEmailAddr { get; set; }

        //URL
        public bool Email_CBHomepageURL { get; set; }
        public bool Email_CBFacebookURL { get; set; }
        public bool Email_CBTwitterURL { get; set; }
    }
}
