﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresBasedOnFSAReportVM
    {
        [Display(Name = "Region")]
        public string Region { get; set; }        
        [Display(Name = "Zone")]
        public string Zone { get; set; }       
        //public string PostalCode { get; set; }     
        [Display(Name = "PLT")]
        public int PLT { get; set; }
        [Display(Name = "MT")]
        public int MT { get; set; }
        [Display(Name = "AG/LS")]
        public int AGLS { get; set; }
        [Display(Name = "IND")]
        public int IND { get; set; }
        [Display(Name = "SOTR")]
        public int SOTR { get; set; }
        [Display(Name = "MOTR")]
        public int MOTR { get; set; }      
        [Display(Name = "LOTR")]
        public int LOTR { get; set; }
        [Display(Name = "GOTR")]
        public int GOTR { get; set; }
    }
}
