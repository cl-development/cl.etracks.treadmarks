﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class StewardRptRevenueSupplyOldTireVM : Reportbase
    {
        public string period { get; set; }
        [Display(Name = "reg no")]
        public string regNo { get; set; }
        [Display(Name = "legal name")]
        public string legalName { get; set; }
        [Display(Name = "web submission date")]
        public DateTime? webSubmissionDate { get; set; }
        [Display(Name = "submitted by")]
        public string submittedBy { get; set; }
        [Display(Name = "steward type")]
        public string stewardType
        {
            get
            {
                return (base.RegistrantSubTypeID == 1) ? "Original Equipment Manufacture (OEM)" : ((base.RegistrantSubTypeID == 2) ? "Tire Manufacture/Brand Owner" : "First Importer");
            }
        }
        [Display(Name = "OEM")]
        public string OEM
        {
            get
            {
                return (this.RegistrantSubTypeID == 1) ? "Yes" : "No";
            }
        }
        [Display(Name = "tsf status")]
        public string tsfStatus { get; set; }
        [Display(Name = "receipt date")]
        public DateTime? receiptDate { get; set; }
        [Display(Name = "deposit date")]
        public DateTime? DepositDate { get; set; }
        [Display(Name = "Cheque/EFT #")]
        public string ChequeEFT { get; set; }
        [Display(Name = "cheque amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal chequeAmount { get; set; }
        [Display(Name = "total receivable")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal totalReceiveable { get; set; }
        [Display(Name = "HST")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal ApplicableTaxesHst { get; set; }
        [Display(Name = "Payment Adjustments")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? AdjustmentTotal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal penalties { get; set; }
        public string interest { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal plt
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "PLT").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal mt
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MT").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal agls
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "AGLS").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal ind
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "IND").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal sotr
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "SOTR").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal motr
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MOTR").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal lotr
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "LOTR").Sum(i => i.TSFDue);
            }
        }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal gotr
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "GOTR").Sum(i => i.TSFDue);
            }
        }


        [Display(Name = "plt")]
        public int? pltCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "PLT").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "plt -ve adj")]
        public int? pltVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "PLT").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "plt net")]
        public int? pltNet
        {
            get
            {
                return this.pltCount - this.pltVeAdj;
            }
        }

        
        [Display(Name = "mt")]
        public int? mtCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MT").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "mt -ve adj")]
        public int? mtVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MT").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "mt net")]
        public int? mtNet
        {
            get
            {
                return this.mtCount - this.mtVeAdj;
            }
        }

        [Display(Name = "agls")]
        public int? aglsCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "AGLS").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "agls -ve adj")]
        public int? aglsVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "AGLS").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "agls net")]
        public int? aglsNet
        {
            get
            {
                return this.aglsCount - this.aglsVeAdj;
            }
        }

        [Display(Name = "ind")]
        public int? indCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "IND").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "ind -ve adj")]
        public int? indVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "IND").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "ind net")]
        public int? indNet
        {
            get
            {
                return this.indCount - this.indVeAdj;
            }
        }


        [Display(Name = "sotr")]
        public int? sotrCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "SOTR").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "sotr -ve adj")]
        public int? sotrVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "SOTR").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "sotr net")]
        public int? sotrNet
        {
            get
            {
                return this.sotrCount - this.sotrVeAdj;
            }
        }

        [Display(Name = "motr")]
        public int? motrCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MOTR").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "motr -ve adj")]
        public int? motrVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "MOTR").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "motr net")]
        public int? motrNet
        {
            get
            {
                return this.motrCount - this.motrVeAdj;
            }
        }


        [Display(Name = "lotr")]
        public int? lotrCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "LOTR").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "lotr -ve adj")]
        public int? lotrVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "LOTR").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "lotr net")]
        public int? lotrNet
        {
            get
            {
                return this.lotrCount - this.lotrVeAdj;
            }
        }


        [Display(Name = "gotr")]
        public int? gotrCount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "GOTR").Sum(i => i.TireSupplied);
            }
        }

        [Display(Name = "gotr -ve adj")]
        public int? gotrVeAdj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "GOTR").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "gotr net")]
        public int? gotrNet
        {
            get
            {
                return this.gotrCount - this.gotrVeAdj;
            }
        }
        public int? tires { get; set; }
        [Display(Name = "registrant status")]
        public string registrantStatus { get; set; }
        [Display(Name = "status change date")]
        public DateTime? statusChangeDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        [Display(Name = "remittance payable")]
        public decimal remittancePayable { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        [Display(Name = "payable variance")]
        public decimal payableVariance //refer to StewardRptRevenueSupplyNewTireVM.payableVariance
        {
            get
            {
                return base.BalanceDue;
            }
        }
    }
}
