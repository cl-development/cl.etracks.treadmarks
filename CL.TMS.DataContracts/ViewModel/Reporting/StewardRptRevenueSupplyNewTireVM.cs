﻿using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class StewardRptRevenueSupplyNewTireVM : Reportbase
    {
        [Display(Name = "period")]
        public string Period { get; set; }
        [Display(Name = "reg no")]
        public string regNo { get; set; }
        [Display(Name = "legal name")]
        public string legalName { get; set; }
        [Display(Name = "web submission date")]
        public DateTime? webSubmissionDate { get; set; }
        [Display(Name = "Submitted By")]
        public string submittedBy { get; set; }
        [Display(Name = "steward type")]
        public string stewardType { get {
            return (base.RegistrantSubTypeID == 1) ? "Original Equipment Manufacture (OEM)" : ((base.RegistrantSubTypeID == 2) ? "Tire Manufacture/Brand Owner" : "First Importer");
        } }
        [Display(Name = "OEM")]
        public string OEM { get {
            return (this.RegistrantSubTypeID == 1) ? "Yes" : "No";
        } }
        [Display(Name = "tsf status")]
        public string tsfStatus { get; set; }
        [Display(Name = "receipt date")]
        public DateTime? receiptDate { get; set; }
        [Display(Name = "deposit date")]
        public DateTime? depositDate { get; set; }
        [Display(Name = "Cheque/EFT #")]
        public string ChequeEFT { get; set; }
        [Display(Name = "cheque amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal chequeAmount { get; set; }
        [Display(Name = "total receivable")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal totalReceiveable { get; set; }
        [Display(Name = "Tire Counts $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal Subtotal { get; set; }
        [Display(Name = "penalties")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? penalties { get; set; }
        [Display(Name = "interest")]
        public string interest { get; set; }
        [Display(Name = "HST")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal ApplicableTaxesHst { get; set; }
        [Display(Name = "Payment Adjustments")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? AdjustmentTotal { get; set; }

        [Display(Name = "class1")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class1Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C1").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class2")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class2Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C2").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class3")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class3Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C3").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class4")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class4Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C4").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class5")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class5Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C5").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class6")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class6Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C6").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class7")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class7Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C7").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class8")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class8Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C8").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class9")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class9Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C9").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class10")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class10Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C10").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class11")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class11Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C11").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class12")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class12Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C12").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class13")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class13Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C13").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class14")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class14Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C14").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class15")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class15Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C15").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class16")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class16Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C16").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class17")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class17Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C17").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class18")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class18Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C18").Sum(i => i.TSFDue);
            }
        }
        [Display(Name = "class1")]       
        public int? class1
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C1").Sum(i => i.TireSupplied);
            }
        }

        [Display(Name = "class1 -ve adj")]
        public int? class1_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C1").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class1 net")]
        public int? class1_net
        {
            get
            {
                return this.class1 - this.class1_ve_adj;
            }
        }
        [Display(Name = "class2")]
        public int? class2
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C2").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class2 -ve adj")]
        public int? class2_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C2").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class2 net")]
        public int? class2_net
        {
            get
            {
                return this.class2 - this.class2_ve_adj;
            }
        }
        [Display(Name = "class3")]
        public int? class3
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C3").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class3 -ve adj")]
        public int? class3_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C3").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class3 net")]
        public int? class3_net
        {
            get
            {
                return this.class3 - this.class3_ve_adj;
            }
        }
        [Display(Name = "class4")]
        public int? class4
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C4").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class4 -ve adj")]
        public int? class4_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C4").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class4 net")]
        public int? class4_net
        {
            get
            {
                return this.class4 - this.class4_ve_adj;
            }
        }
        [Display(Name = "class5")]
        public int? class5
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C5").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class5 -ve adj")]
        public int? class5_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C5").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class5 net")]
        public int? class5_net
        {
            get
            {
                return this.class5 - this.class5_ve_adj;
            }
        }
        [Display(Name = "class6")]
        public int? class6
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C6").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class6 -ve adj")]
        public int? class6_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C6").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class6 net")]
        public int? class6_net
        {
            get
            {
                return this.class6 - this.class6_ve_adj;
            }
        }
        [Display(Name = "class7")]
        public int? class7
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C7").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class7 -ve adj")]
        public int? class7_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C7").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class7 net")]
        public int? class7_net
        {
            get
            {
                return this.class7 - this.class7_ve_adj;
            }
        }
        [Display(Name = "class8")]
        public int? class8
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C8").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class8 -ve adj")]
        public int? class8_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C8").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class8 net")]
        public int? class8_net
        {
            get
            {
                return this.class8 - this.class8_ve_adj;
            }
        }
        [Display(Name = "class9")]
        public int? class9
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C9").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class9 -ve adj")]
        public int? class9_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C9").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class9 net")]
        public int? class9_net
        {
            get
            {
                return this.class9 - this.class9_ve_adj;
            }
        }
        [Display(Name = "class10")]
        public int? class10
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C10").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class10 -ve adj")]
        public int? Class10_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C10").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class10 net")]
        public int? class10_net
        {
            get
            {
                return this.class10 - this.Class10_ve_adj;
            }
        }
        [Display(Name = "class11")]
        public int? class11
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C11").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class11 -ve adj")]
        public int? class11_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C11").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class11 net")]
        public int? class11_net
        {
            get
            {
                return this.class11 - this.class11_ve_adj;
            }
        }
        [Display(Name = "class12")]
        public int? class12
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C12").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class12 -ve adj")]
        public int? class12_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C12").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class12 net")]
        public int? class12_net
        {
            get
            {
                return this.class12 - this.class12_ve_adj;
            }
        }
        [Display(Name = "class13")]
        public int? class13
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C13").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class13 -ve adj")]
        public int? class13_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C13").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class13 net")]
        public int? class13_net
        {
            get
            {
                return this.class13 - this.class13_ve_adj;
            }
        }
        [Display(Name = "class14")]
        public int? class14
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C14").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class14 -ve adj")]
        public int? class14_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C14").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class14 net")]
        public int? class14_net
        {
            get
            {
                return this.class14 - this.class14_ve_adj;
            }
        }
        [Display(Name = "class15")]
        public int? class15
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C15").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class15 -ve adj")]
        public int? class15_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C15").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class15 net")]
        public int? class15_net
        {
            get
            {
                return this.class15 - this.class15_ve_adj;
            }
        }
        [Display(Name = "class16")]
        public int? class16
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C16").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class16 -ve adj")]
        public int? class16_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C16").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class16 net")]
        public int? class16_net
        {
            get
            {
                return this.class16 - this.class16_ve_adj;
            }
        }
        [Display(Name = "class17")]
        public int? class17
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C17").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class17 -ve adj")]
        public int? class17_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C17").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class17 net")]
        public int? class17_net
        {
            get
            {
                return this.class17 - this.class17_ve_adj;
            }
        }
        [Display(Name = "class18")]
        public int? class18
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C18").Sum(i => i.TireSupplied);
            }
        }
        [Display(Name = "class18 -ve adj")]
        public int? class18_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C18").Sum(i => i.NegativeAdjustment);
            }
        }
        [Display(Name = "class18 net")]
        public int? class18_net
        {
            get
            {
                return this.class18 - this.class18_ve_adj;
            }
        }
        public int? tires { get; set; }
        [Display(Name = "registrant status")]
        public string registrantStatus { get; set; }
        [Display(Name = "status change date")]
        public DateTime? statusChangeDate
        {
            get
            {
                if (null != base.statusChangeDatetime)
                {
                    DateTime dateValue;
                    DateTime.TryParse(base.statusChangeDatetime.ToString(), out dateValue);
                    //return (dateValue.Date.ToString("MM/dd/yyyy"));
                    return dateValue.Date;
                }
                else
                {
                    return null;
                }
            }
            set { }
        }
        [Display(Name = "remittance payable")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal remittancePayable { get { return (remittancePayableVal - creditPayableVal + (AdjustmentTotal ?? 0)); } }        

        [Display(Name = "payable variance")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal payableVariance
        {
            //Payable Variance = Cheque Amount - Remittance Payable where 
            //Remittance Payable = (TSF Due + Penalties) - Credit
            get
            {
                //return ((decimal)((decimal)this.chequeAmountVal - base.remittancePayableVal)).ToString("0.00");
                //return ((decimal)((decimal)this.chequeAmountVal - base.remittancePayableVal - base.penaltiesVal)).ToString("0.00");
                return base.BalanceDue;
            }
        }

    }
    public class Reportbase
    {
        private string ReportType { get; set; }
        public DateTime? statusChangeDatetime { get; set; }
        public decimal remittancePayableVal { get; set; }
        public decimal creditPayableVal { get; set; }
        [JsonIgnore]
        public IEnumerable<TSFClaimDetailsVM> TSFClaimDetail { get; set; }
        public int RegistrantSubTypeID { get; set; }
        public decimal? penaltiesVal { get; set; }
        public decimal totalReceiveableVal { get; set; }
        public decimal? chequeAmountVal { get; set; }
        public decimal BalanceDue { get; set; }
    }
    public class TSFClaimDetailsVM
    {
        //public int TSFClaimID { get; set; }
        //public int ItemID { get; set; }
        public int TireSupplied { get; set; }
        public int NegativeAdjustment { get; set; }
        
        //public int CountSupplied { get; set; }
        //public decimal TSFRate { get; set; }
        //public string OtherDesc { get; set; }
        public decimal TSFDue { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedUser { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public string ModifiedUser { get; set; }
        //public int AdjustmentReasonType { get; set; }
        public string ItemShortName { get; set; }
        public int CreditNegativeAdj { get; set; }
        public decimal CreditTSFDue { get; set; }
    }
}