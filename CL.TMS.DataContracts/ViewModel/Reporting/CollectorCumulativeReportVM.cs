﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectorCumulativeReportVM : baseVM
    {
        [Display(Name = "Registration Number")]
        public string RegistrationNumber { get; set; }
        [Display(Name = "Claim Period")]
        public string ClaimPeriod { get; set; }
        [Display(Name = "Date Submitted")]
        public DateTime? DateSubmitted { get; set; }
        [Display(Name = "Due Date")]
        public DateTime? DueDate { get; set; }        
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Display(Name = "Amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? Amount
        {
            get
            {
                return this.AmountWithHST - this.HST;
            }
        }
        [Display(Name = "HST")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? HST { get; set; }
        [Display(Name = "AmountWithHST")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? AmountWithHST { get; set; }
        [Display(Name = "Operating Name Web")]
        public string OperatingNameWeb { get; set; }
        [Display(Name = "Operating Name TM")]
        public string OperatingNameTM { get; set; }
        [Display(Name = "Mailed Date")]
        public DateTime? MailedDate { get; set; }
        [Display(Name = "Active State")]
        public string ActiveState { get; set; }
        [Display(Name = "Inactive Date")]
        public DateTime? InactiveDate { get; set; }
        [Display(Name = "User Email")]
        public string UserEmail { get; set; }
        [Display(Name = "Date Finalized")]
        public string DateFinalized
        {
            get
            {
                if (null == DateFinalizedVal)
                {
                    return "N/A";
                }
                else
                {
                    DateTime parseResult = DateTime.MinValue;
                    DateTime.TryParse(this.DateFinalizedVal.ToString(), out parseResult);
                    if (parseResult != DateTime.MinValue)
                    {
                        return parseResult.ToString(CL.TMS.Common.TreadMarksConstants.DateTimeFormat);
                    }
                    else
                    {
                        return "N/A";
                    }
                }
            }
        }
        [Display(Name = "Flag Finalized")]
        public string FlagFinalized { get; set; }
    }
    public class baseVM
    {
        public DateTime? DateFinalizedVal { get; set; }        
    }
}
