﻿using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class RpmData : RpmDataExtended
    {
        [Display(Name = "registration_number")]
        public string RegNumber { get; set; }

        [Display(Name = "rpm_claim_period_start")]
        public DateTime? ClaimPeriodStart { get; set; }

        [Display(Name = "rpm_claim_period_description")]
        public string ClaimPeriodDescription { get; set; }

        [Display(Name = "SPS#")]
        public string TransactionNumber { get; set; }
        [Display(Name = "approve")]
        public string Approve { get; set; }

        [Display(Name = "documents_received")]
        public bool? DocumentsReceived { get; set; }

        [Display(Name = "record_modified")]
        public bool RecordModified { get; set; }

        [Display(Name = "comment")]
        public string Comment { 
            get
            {
                string comment = string.Empty;
                if (this.TransactionNotes != null)
                    foreach (TransactionNote item in this.TransactionNotes)
                    {
                        if (item != null)
                        {
                            comment += ("- " + item.Note + " -");
                        }
                    }
                return comment;
            }
        }

        [Display(Name = "date_sold")]
        public DateTime DateSold { get; set; }

        [Display(Name = "end_use_market_desc")]
        public string EndUseMarketDesc { get; set; }

        [Display(Name = "product_other_description")]
        public string ProductOtherDesc { get; set; }

        [Display(Name = "sales_invoice_no")]
        public string SalesInvoiceNum { get; set; }

        [Display(Name = "purchaser")]
        public string Purchaser { get; set; }

        [Display(Name = "recycled_material_used")]
        public decimal? RecycledMaterialUsed
        {
            get
            {
                return Math.Round(DataConversion.ConvertKgToTon((double)RpmActualWeight), 4, MidpointRounding.AwayFromZero);
            }
        }

        [Display(Name = "rate($)")]
        public decimal? Rate 
        { 
            get 
            {
                return Math.Round(RpmRate ?? 0, 2, MidpointRounding.AwayFromZero);
            } 
        }

        [Display(Name = "incentive($)")]
        public decimal? Incentive
        {
            get
            {
                return Math.Round(((RpmRate ?? 0) * (RecycledMaterialUsed ?? 0)), 2, MidpointRounding.AwayFromZero);
            }
        }
    }

    public class RpmDataExtended {

        [JsonIgnore]
        public int TransactionId
        {
            get;
            set;
        }

        [JsonIgnore]
        public List<TransactionNote> TransactionNotes { get; set; }

        [JsonIgnore]
        public decimal? RpmRate
        {
            get;
            set;
        }
        [JsonIgnore]
        public decimal? RpmActualWeight
        {
            get;
            set;
        }
    }
}
