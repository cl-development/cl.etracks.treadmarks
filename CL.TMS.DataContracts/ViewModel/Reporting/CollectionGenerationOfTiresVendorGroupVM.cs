﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresVendorGroupVM
    {
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public List<int> VendorIds { get; set; }
    }
}
