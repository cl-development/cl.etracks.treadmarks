﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class StaffUserNameAndEmailViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
