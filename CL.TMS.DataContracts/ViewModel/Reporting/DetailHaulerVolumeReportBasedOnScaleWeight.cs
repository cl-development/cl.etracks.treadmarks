﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class DetailHaulerVolumeReportBasedOnScaleWeight
    {
        public DateTime ClaimPeriodStart { get; set; }
        public string RegistrationNumber { get; set; }
        public string HaulerState { get; set; }
        public string RecordState { get; set; }
        public string ReferenceTable { get; set; }
        public long PtrFormNumber { get; set; }
        public string ProcessorNumber { get; set; }
        public string ProcessorState { get; set; }
        public Nullable<decimal> Scaleweight { get; set; }
        public Nullable<decimal> OtrTotalScaleWeight { get; set; }
        public Nullable<decimal> PltScaleWeight { get; set; }
        public Nullable<decimal> MtScaleWeight { get; set; }
        public Nullable<decimal> AglsScaleWeight { get; set; }
        public Nullable<decimal> IndScaleWeight { get; set; }
        public Nullable<decimal> SotrScaleWeight { get; set; }
        public Nullable<decimal> MotrScaleWeight { get; set; }
        public Nullable<decimal> LotrScaleWeight { get; set; }
        public Nullable<decimal> GotrScaleWeight { get; set; }
        public Nullable<decimal> TotalTireWeight { get; set; }
        public Nullable<decimal> OtrTotalEstweight { get; set; }
        public Nullable<decimal> PltEstWeight { get; set; }
        public Nullable<decimal> MtEstWeight { get; set; }
        public Nullable<decimal> AglsEstWeight { get; set; }
        public Nullable<decimal> IndEstWeight { get; set; }
        public Nullable<decimal> SotrEstWeight { get; set; }
        public Nullable<decimal> MotrEstWeight { get; set; }
        public Nullable<decimal> LotrEstWeight { get; set; }
        public Nullable<decimal> GotrEstWeight { get; set; }
    }
}
