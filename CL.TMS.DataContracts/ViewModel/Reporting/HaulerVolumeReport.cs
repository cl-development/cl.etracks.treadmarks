﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class HaulerVolumeReport
    {
        [Display(Name = "Reporting Period")]
        public string ReportingPeriod { get; set; }
        [Display(Name = "Registration Number")]
        public decimal HaulerRegistrationNumber { get; set; }
        [Display(Name = "Hauler Status")]
        public string HaulerStatus { get; set; }
        [Display(Name = "Destination Type")]
        public string DestinationType { get; set; }
        [Display(Name = "Processor / Hauler Registration Number")]
        public Nullable<decimal> ProcessorHaulerRegistrationNumber { get; set; }
        [Display(Name = "Processor Status")]
        public string Status { get; set; }
        [Display(Name = "Record State")]
        public string RecordState { get; set; }
        [Display(Name = "Used Tire Destination")]
        public string UsedTireDestionation { get; set; }
        [Display(Name = "Total Actual Weight")]
        public Nullable<decimal> TotalActualWeight { get; set; }
        [Display(Name = "Total (Est. Weight)")]
        public Nullable<decimal> TotalEstWeight { get; set; }
        [Display(Name = "PLT (Est. Weight)")]
        public Nullable<decimal> PltEstWeight { get; set; }
        [Display(Name = "MT (Est. Weight)")]
        public Nullable<decimal> MtEstWeight { get; set; }
        [Display(Name = "AGLS (Est. Weight)")]
        public Nullable<decimal> AglsEstWeight { get; set; }
        [Display(Name = "IND (Est. Weight)")]
        public Nullable<decimal> IndEstWeight { get; set; }
        [Display(Name = "SOTR (Est. Weight)")]
        public Nullable<decimal> SotrEstWeight { get; set; }
        [Display(Name = "MOTR (Est. Weight)")]
        public Nullable<decimal> MotrEstWeight { get; set; }
        [Display(Name = "LOTR (Est. Weight)")]
        public Nullable<decimal> LotrEstWeight { get; set; }
        [Display(Name = "GOTR (Est. Weight)")]
        public Nullable<decimal> GotrEstWeight { get; set; }
        [Display(Name = "Total (Payment Earned)")]
        public Nullable<decimal> TotalPaymentEarned { get; set; }
        [Display(Name = "PLT (Payment Earned)")]
        public Nullable<decimal> PltPaymentEarned { get; set; }
        [Display(Name = "MT (Payment Earned)")]
        public Nullable<decimal> MtPaymentEarned { get; set; }
        [Display(Name = "AGLS (Payment Earned)")]
        public Nullable<decimal> AglsPaymentEarned { get; set; }
        [Display(Name = "IND (Payment Earned)")]
        public Nullable<decimal> IndPaymentEarned { get; set; }
        [Display(Name = "SOTR (Payment Earned)")]
        public Nullable<decimal> SotrPaymentEarned { get; set; }
        [Display(Name = "MOTR (Payment Earned)")]
        public Nullable<decimal> MotrPaymentEarned { get; set; }
        [Display(Name = "LOTR (Payment Earned)")]
        public Nullable<decimal> LotrPaymenEarned { get; set; }
        [Display(Name = "GOTR (Payment Earned)")]
        public Nullable<decimal> GotrPaymentEarned { get; set; }
        public Nullable<decimal> Hst { get; set; }
        [Display(Name = "Total (Payment earned with HST)")]
        public Nullable<decimal> TotalPaymentEarnedWithHst { get; set; }
    }
}
