﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectorTireOriginReport
    {
        [Display(Name = "Claim Period")]
        public string ClaimPeriod { get; set;}
        [Display(Name = "Reg #")]
        public string CollectorRegNumber { get; set;}
        [Display(Name = "Collector Company Name Legal")]
        public string CollectorLegalName { get; set; }
        [Display(Name = "Collector Company Name Operating")]
        public string CollectorOperatingName { get; set; }
        [Display(Name = "Claim Status")]
        public string Status { get; set; }
        [Display(Name = "Collector Status")]
        public string CollectorStatus { get; set; }
        [Display(Name = "Origin Description")]
        public string OriginDescription { get; set;}
        [Display(Name = "PLT")]
        public int? PLT { get; set; }
        [Display(Name = "MT")]
        public int? MT { get; set; }
        [Display(Name = "AGLS")]
        public int? AGLS { get; set; }
        [Display(Name = "IND")]
        public int? IND { get; set; }
        [Display(Name = "SOTR")]
        public int? SOTR { get; set; }
        [Display(Name = "MOTR")]
        public int? MOTR { get; set; }
        [Display(Name = "LOTR")]
        public int? LOTR { get; set; }
        [Display(Name = "GOTR")]
        public int? GOTR { get; set; }
        [Display(Name = "PLT (Est. Weight)")]
        public Nullable<decimal> PltEstWeight { get; set; }
        [Display(Name = "MT (Est. Weight)")]
        public Nullable<decimal> MtEstWeight { get; set; }
        [Display(Name = "AGLS (Est. Weight)")]
        public Nullable<decimal> AglsEstWeight { get; set; }
        [Display(Name = "IND (Est. Weight)")]
        public Nullable<decimal> IndEstWeight { get; set; }
        [Display(Name = "SOTR (Est. Weight)")]
        public Nullable<decimal> SotrEstWeight { get; set; }
        [Display(Name = "MOTR (Est. Weight)")]
        public Nullable<decimal> MotrEstWeight { get; set; }
        [Display(Name = "LOTR (Est. Weight)")]
        public Nullable<decimal> LotrEstWeight { get; set; }
        [Display(Name = "GOTR (Est. Weight)")]
        public Nullable<decimal> GotrEstWeight { get; set; }
       
    }
}
