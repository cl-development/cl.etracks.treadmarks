﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class TSFRptOnlinePaymentsOutstandingVM //: CommonRptBaseVM
    {
        [Display(Name = "Reporting Period")]
        public string ReportingPeriod { get; set; }
        [Display(Name = "Steward Registration Number")]
        public string RegistrationNbr { get; set; }
        [Display(Name = "Active status")]
        public string ActiveStatus { get; set; }
        [Display(Name = "Legal Name")]
        public string LegalName { get; set; }
        [Display(Name = "Submission Date")]
        public DateTime? SubmissionDate { get; set; }
        [Display(Name = "Submitted By")]
        public string SubmittedBy { get; set; }
        [Display(Name = "Primary contact first name")]
        public string PrimaryContactFname { get; set; }
        [Display(Name = "Primary contact last name")]
        public string PrimaryContactLname { get; set; }
        [Display(Name = "Primary contact Telephone #")]
        public string ContactTel { get; set; }
        [Display(Name = "Primary Contact EXT #")]
        public string ContactTelExt { get; set; }
        [Display(Name = "Balance Due")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal? BalanceDue { get; set; }
        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }
        [Display(Name = "Remittance Status")]
        public string RemittanceStatus { get; set; }
    }
}
