﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresBasedOnRateGroupProcessingVM
    {
        //public int VendorId { get; set; }
        public string ShortName { get; set; }
        public int GroupId { get; set; }
        public int RateGroupId { get; set; }
        public int Quantity { get; set; }
        public int transactionID { get; set; } //introduce this ID to avoid duplicated records in query result
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
    }

}
