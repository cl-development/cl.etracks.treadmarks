﻿using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class StewardRptRevenueSupplyNewTireCreditVM : Reportbase
    {
        [Display(Name = "period")]
        public string Period { get; set; }
        [Display(Name = "reg no")]
        public string regNo { get; set; }
        [Display(Name = "legal name")]
        public string legalName { get; set; }
        [Display(Name = "web submission date")]
        public DateTime? webSubmissionDate { get; set; }
        [Display(Name = "Submitted By")]
        public string submittedBy { get; set; }
        [Display(Name = "steward type")]
        public string stewardType
        {
            get
            {
                return (base.RegistrantSubTypeID == 1) ? "Original Equipment Manufacture (OEM)" : ((base.RegistrantSubTypeID == 2) ? "Tire Manufacture/Brand Owner" : "First Importer");
            }
        }
        [Display(Name = "OEM")]
        public string OEM
        {
            get
            {
                return (this.RegistrantSubTypeID == 1) ? "Yes" : "No";
            }
        }
        [Display(Name = "tsf status")]
        public string tsfStatus { get; set; }
        [Display(Name = "receipt date")]
        public DateTime? receiptDate { get; set; }
        [Display(Name = "deposit date")]
        public DateTime? depositDate { get; set; }
        [Display(Name = "Cheque/EFT #")]
        public string ChequeEFT { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        [Display(Name = "cheque amount")]
        public decimal chequeAmount { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        [Display(Name = "total receivable")]
        public decimal totalReceiveable { get; set; }
        [Display(Name = "total Credit")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal totalCredit { get; set; }
        [Display(Name = "taxes due")]
        public string taxesDue { get; set; }
        [Display(Name = "penalties")]
        public string penalties { get { return ((decimal)penaltiesVal).ToString("0.00"); } }
        [Display(Name = "interest")]
        public string interest { get; set; }
        [Display(Name = "Month/Yr -ve")]
        public DateTime? NegativeAdjDate { get; set; }


        [Display(Name = "class1 -ve $")]
        public decimal class1Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C1").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class2 -ve $")]
        public decimal class2Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C2").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class3 -ve $")]
        public decimal class3Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C3").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class4 -ve $")]
        public decimal class4Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C4").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class5 -ve $")]
        public decimal class5Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C5").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class6 -ve $")]
        public decimal class6Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C6").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class7 -ve $")]
        public decimal class7Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C7").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class8 -ve $")]
        public decimal class8Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C8").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class9 -ve $")]
        public decimal class9Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C9").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class10 -ve $")]
        public decimal class10Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C10").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class11 -ve $")]
        public decimal class11Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C11").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class12 -ve $")]
        public decimal class12Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C12").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class13 -ve $")]
        public decimal class13Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C13").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class14 -ve $")]
        public decimal class14Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C14").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class15 -ve $")]
        public decimal class15Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C15").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class16 -ve $")]
        public decimal class16Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C16").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class17 -ve $")]
        public decimal class17Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C17").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class18 -ve $")]
        public decimal class18Amount
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C18").Sum(i => i.CreditTSFDue);
            }
        }
        [Display(Name = "class1 -ve adj")]
        public int? class1_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C1").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }
        [Display(Name = "class2 -ve adj")]
        public int? class2_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C2").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class3 -ve adj")]
        public int? class3_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C3").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class4 -ve adj")]
        public int? class4_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C4").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class5 -ve adj")]
        public int? class5_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C5").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class6 -ve adj")]
        public int? class6_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C6").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class7 -ve adj")]
        public int? class7_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C7").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class8 -ve adj")]
        public int? class8_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C8").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class9 -ve adj")]
        public int? class9_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C9").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class10 -ve adj")]
        public int? Class10_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C10").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class11 -ve adj")]
        public int? class11_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C11").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class12 -ve adj")]
        public int? class12_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C12").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class13 -ve adj")]
        public int? class13_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C13").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class14 -ve adj")]
        public int? class14_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C14").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class15 -ve adj")]
        public int? class15_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C15").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class16 -ve adj")]
        public int? class16_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C16").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class17 -ve adj")]
        public int? class17_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C17").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        [Display(Name = "class18 -ve adj")]
        public int? class18_ve_adj
        {
            get
            {
                return this.TSFClaimDetail.Where(i => i.ItemShortName == "C18").Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment));
            }
        }

        public int? tires { get; set; }
        [Display(Name = "registrant status")]
        public string registrantStatus { get; set; }
        [Display(Name = "status change date")]
        public string statusChangeDate
        {
            get
            {
                if (null != base.statusChangeDatetime)
                {
                    DateTime dateValue;
                    DateTime.TryParse(base.statusChangeDatetime.ToString(), out dateValue);
                    //return (dateValue.Date.ToString("MM/dd/yyyy"));// + " 12:00:00 AM" //OTSTM2-425 #5
                    return (dateValue.Date.ToString("dd/MM/yyyy hh:mm:ss"));
                }
                else
                {
                    return string.Empty;
                }
            }
            set { }
        }
        [Display(Name = "Credit Payable")]
        public string remittancePayable { get { return creditPayableVal.ToString("0.00"); } }
    }
    public class StewardRptRevenueSupplyNewTireCreditVMsp
    {
        [Display(Name = "period")]
        public string Period { get; set; }
        [Display(Name = "reg no")]
        public string regNo { get; set; }
        [Display(Name = "legal name")]
        public string legalName { get; set; }
        [Display(Name = "web submission date")]
        public DateTime? webSubmissionDate { get; set; }
        [Display(Name = "Submitted By")]
        public string submittedBy { get; set; }
        [Display(Name = "steward type")]
        public string stewardType { get; set; }
        [Display(Name = "OEM")]
        public string OEM { get; set; }
        [Display(Name = "tsf status")]
        public string tsfStatus { get; set; }
        [Display(Name = "receipt date")]
        public DateTime? receiptDate { get; set; }
        [Display(Name = "deposit date")]
        public DateTime? depositDate { get; set; }
        [Display(Name = "Cheque/EFT #")]
        public string ChequeEFT { get; set; }
        [Display(Name = "cheque amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal chequeAmountVal { get; set; }
        [Display(Name = "total Credit")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal totalCredit { get; set; }
        [Display(Name = "taxes due")]
        public string taxesDue { get; set; }
        [Display(Name = "penalties")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        //public decimal penalties { get; set; }
        public string penalties { get { return "N/A"; } }
        [Display(Name = "interest")]
        public string interest { get; set; }
        [Display(Name = "Month/Yr -ve")]
        public string NegativeAdjDate { get; set; }
        [Display(Name = "class1 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class1Amount { get; set; }
        [Display(Name = "class2 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class2Amount { get; set; }
        [Display(Name = "class3 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class3Amount { get; set; }
        [Display(Name = "class4 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class4Amount { get; set; }
        [Display(Name = "class5 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class5Amount { get; set; }
        [Display(Name = "class6 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class6Amount { get; set; }
        [Display(Name = "class7 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class7Amount { get; set; }
        [Display(Name = "class8 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class8Amount { get; set; }
        [Display(Name = "class9 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class9Amount { get; set; }
        [Display(Name = "class10 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class10Amount { get; set; }
        [Display(Name = "class11 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class11Amount { get; set; }
        [Display(Name = "class12 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class12Amount { get; set; }
        [Display(Name = "class13 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class13Amount { get; set; }
        [Display(Name = "class14 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class14Amount { get; set; }
        [Display(Name = "class15 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class15Amount { get; set; }
        [Display(Name = "class16 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class16Amount { get; set; }
        [Display(Name = "class17 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class17Amount { get; set; }
        [Display(Name = "class18 -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal class18Amount { get; set; }
        [Display(Name = "class1 -ve adj")]
        public int? class1_ve_adj { get; set; }
        [Display(Name = "class2 -ve adj")]
        public int? class2_ve_adj { get; set; }

        [Display(Name = "class3 -ve adj")]
        public int? class3_ve_adj { get; set; }

        [Display(Name = "class4 -ve adj")]
        public int? class4_ve_adj { get; set; }

        [Display(Name = "class5 -ve adj")]
        public int? class5_ve_adj { get; set; }

        [Display(Name = "class6 -ve adj")]
        public int? class6_ve_adj { get; set; }

        [Display(Name = "class7 -ve adj")]
        public int? class7_ve_adj { get; set; }

        [Display(Name = "class8 -ve adj")]
        public int? class8_ve_adj { get; set; }

        [Display(Name = "class9 -ve adj")]
        public int? class9_ve_adj { get; set; }

        [Display(Name = "class10 -ve adj")]
        public int? Class10_ve_adj { get; set; }

        [Display(Name = "class11 -ve adj")]
        public int? class11_ve_adj { get; set; }

        [Display(Name = "class12 -ve adj")]
        public int? class12_ve_adj { get; set; }

        [Display(Name = "class13 -ve adj")]
        public int? class13_ve_adj { get; set; }

        [Display(Name = "class14 -ve adj")]
        public int? class14_ve_adj { get; set; }

        [Display(Name = "class15 -ve adj")]
        public int? class15_ve_adj { get; set; }

        [Display(Name = "class16 -ve adj")]
        public int? class16_ve_adj { get; set; }

        [Display(Name = "class17 -ve adj")]
        public int? class17_ve_adj { get; set; }

        [Display(Name = "class18 -ve adj")]
        public int? class18_ve_adj { get; set; }

        public int? tires { get; set; }
        [Display(Name = "registrant status")]
        public string registrantStatus { get; set; }
        [Display(Name = "status change date")]
        public DateTime? statusChangeDate { get; set; }
        [Display(Name = "Credit Payable")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal creditPayable { get; set; }
    }
    public class StewardRptRevenueSupplyOldTireCreditVMsp
    {
        [Display(Name = "period")]
        public string Period { get; set; }
        [Display(Name = "reg no")]
        public string regNo { get; set; }
        [Display(Name = "legal name")]
        public string legalName { get; set; }
        [Display(Name = "web submission date")]
        public DateTime? webSubmissionDate { get; set; }
        [Display(Name = "Submitted By")]
        public string submittedBy { get; set; }
        [Display(Name = "steward type")]
        public string stewardType { get; set; }
        [Display(Name = "OEM")]
        public string OEM { get; set; }
        [Display(Name = "tsf status")]
        public string tsfStatus { get; set; }
        [Display(Name = "receipt date")]
        public DateTime? receiptDate { get; set; }
        [Display(Name = "deposit date")]
        public DateTime? depositDate { get; set; }
        [Display(Name = "Cheque/EFT #")]
        public string ChequeEFT { get; set; }
        [Display(Name = "cheque amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal chequeAmountVal { get; set; }
        [Display(Name = "total Credit")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal totalCredit { get; set; }
        [Display(Name = "taxes due")]
        public string taxesDue { get; set; }
        [Display(Name = "penalties")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        //public decimal penalties { get; set; }
        public string penalties { get { return "N/A"; } }
        [Display(Name = "interest")]
        public string interest { get; set; }
        [Display(Name = "Month/Yr -ve")]
        public string NegativeAdjDate { get; set; }

        [Display(Name = "plt -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal pltAmount { get; set; }
        [Display(Name = "mt -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal mtAmount { get; set; }
        [Display(Name = "agls -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal aglsAmount { get; set; }
        [Display(Name = "ind -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal indAmount { get; set; }
        [Display(Name = "sotr -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal sotrAmount { get; set; }
        [Display(Name = "motr -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal motrAmount { get; set; }
        [Display(Name = "lotr -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal lotrAmount { get; set; }
        [Display(Name = "gotr -ve $")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal gotrAmount { get; set; }


        [Display(Name = "plt -ve adj")]
        public int? plt_ve_adj { get; set; }
        [Display(Name = "mt -ve adj")]
        public int? mt_ve_adj { get; set; }

        [Display(Name = "agls -ve adj")]
        public int? agls_ve_adj { get; set; }

        [Display(Name = "ind -ve adj")]
        public int? ind_ve_adj { get; set; }

        [Display(Name = "sotr -ve adj")]
        public int? sotr_ve_adj { get; set; }

        [Display(Name = "motr -ve adj")]
        public int? motr_ve_adj { get; set; }

        [Display(Name = "lotr -ve adj")]
        public int? lotr_ve_adj { get; set; }

        [Display(Name = "gotr -ve adj")]
        public int? gotr_ve_adj { get; set; }

        public int? tires { get; set; }
        [Display(Name = "registrant status")]
        public string registrantStatus { get; set; }
        [Display(Name = "status change date")]
        public DateTime? statusChangeDate { get; set; }
        [Display(Name = "Credit Payable")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal creditPayable { get; set; }
    }
    public class TSFClaimDetailsVMsp
    {
        public int TSFClaimID { get; set; }
        public int ItemID { get; set; }
        public int TireSupplied { get; set; }
        public int NegativeAdjustment { get; set; }

        //public int CountSupplied { get; set; }
        //public decimal TSFRate { get; set; }
        //public string OtherDesc { get; set; }
        public decimal TSFDue { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedUser { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public string ModifiedUser { get; set; }
        //public int AdjustmentReasonType { get; set; }
        public string ItemShortName { get; set; }
        public int CreditNegativeAdj { get; set; }
        public decimal CreditTSFDue { get; set; }
        public DateTime? NegativeAdjDate { get; set; }
    }
}