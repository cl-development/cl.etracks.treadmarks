﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class StewardNonFilers
    {
        [Display(Name = "Period")]
        public DateTime? Period { get; set; }
        [Display(Name = "Reg #.")]
        public string RegistrationNumber { get; set; }
        [Display(Name = "Reg. Type")]
        public string RegistrationType { get; set; }
        [Display(Name = "Legal Name")]
        public string LegalName { get; set; }
        [Display(Name = "Operating Name")]
        public string OperatingName { get; set; }
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
        [Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }
        [Display(Name = "Contact Position")]
        public string ContactPosition { get; set; }
        [Display(Name = "Bus. Post Code")]
        public string BusinessPostalCode { get; set; }
        [Display(Name = "Bus. Start Date")]
        public DateTime? BusinessStartDate { get; set; }
        [Display(Name = "Business Email")]
        public string BusinessEmail { get; set; }

        //OTSTM2-375 Add ReportingFrequency and remove GracePeriodStartDate, GracePeriodEndDate
        //[Display(Name = "Grace Period Start Date")]
        //public DateTime? GracePeriodStartDate { get; set; }
        //[Display(Name = "Grace Period End Date")]
        //public DateTime? GracePeriodEndDate { get; set; }        
        
        [Display(Name = "Reporting Frequency")]
        public string ReportingFrequency { get; set; }
        
        [Display(Name = "Registrant Status")]
        public string RegistrantStatus { get; set; }
        [Display(Name = "Status Change Date")]
        public DateTime? StatusChangeDate { get; set; }
    }
}
