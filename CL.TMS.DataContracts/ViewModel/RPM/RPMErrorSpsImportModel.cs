﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMErrorSpsImportModel
    {
        public int Row { get; set; }
        public string Column { get; set; }
        
    }
}
