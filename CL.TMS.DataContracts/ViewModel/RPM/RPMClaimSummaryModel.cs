﻿using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMClaimSummaryModel
    {

        public RPMClaimSummaryModel()
        {
            SupportingDocument = new SupportingDocumentModelBase();
            ClaimCommonModel = new ClaimCommonModel();
            RPMPayment = new RPMPaymentViewModel();
        }

        #region Common
        public ClaimCommonModel ClaimCommonModel { get; set; }
        #endregion

        #region Inbound
        public decimal TotalOpening { get; set; }
        public decimal SIR { get; set; }
        public decimal AdjustIn { get; set; }
        public decimal TotalInbound
        {
            get
            {
                return Math.Round(TotalOpening, 4, MidpointRounding.AwayFromZero) + Math.Round(SIR, 4, MidpointRounding.AwayFromZero) + Math.Round(AdjustIn, 4, MidpointRounding.AwayFromZero);
            }
        }
        #endregion

        #region Outbound
        public decimal SPS { get; set; }
        public decimal AdjustOut { get; set; }
        public decimal TotalOutbound
        {
            get
            {
                return Math.Round(SPS, 4, MidpointRounding.AwayFromZero) + Math.Round(AdjustOut, 4, MidpointRounding.AwayFromZero);
            }
        }
        #endregion

        #region Inventory Adjustments
        public decimal TotalAdjustment { get; set; }
        public decimal OverallAdjustments { get; set; }
        public decimal ClosingInventory
        {
            get
            {
                return Math.Round(TotalInbound, 4, MidpointRounding.AwayFromZero) - Math.Round(TotalOutbound, 4, MidpointRounding.AwayFromZero) + Math.Round(OverallAdjustments, 4, MidpointRounding.AwayFromZero);
            }
        }
        public decimal TotalCapacity { get; set; }
        #endregion

        #region Support Documents
        public SupportingDocumentModelBase SupportingDocument { get; set; }

        #endregion

        #region Payment and Adjustment
        public RPMPaymentViewModel RPMPayment { get; set; }
        #endregion

        #region Staff Specific
        public ClaimStatusViewModel ClaimStatus { get; set; }
        public ClaimWorkflowViewModel ClaimWorkflow { get; set; }
        #endregion
    }
}
