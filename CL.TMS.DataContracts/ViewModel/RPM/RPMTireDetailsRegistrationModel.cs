﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;


namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMTireDetailsRegistrationModel : RegistrationModelBase
    {
        public IList<RPMTireDetailsItemTypeModel> TireDetailsItemType { get; set; }           
        public string OtherRegistrantSubType { get; set; }
        public string OtherProcessProduct { get; set; }
        public IList<RPMTireDetailsItemTypeModel> ProductsProduceType { get; set; }
    }
}
