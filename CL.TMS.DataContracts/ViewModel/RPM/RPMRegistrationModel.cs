﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMRegistrationModel : RegistrationModelBase, IBusinessLocation, IContactInformationListModel, ITermsAndConditions
    {
        public RPMRegistrationModel()
        {
            InitializeModel(0);
        }

        public RPMRegistrationModel(int applicationId)
        {
            InitializeModel(applicationId);
        }

        private void InitializeModel(int applicationId)
        {
            this.ID = applicationId;
            this.InvalidFormFields = new List<string>();
            this.TireDetailsItemTypeString = new List<string>();
            this.ProductsProduceTypeString = new List<string>();
            this.BusinessLocation = new BusinessRegistrationModel();
            this.ContactInformationList = new List<ContactRegistrationModel>() { };
            this.SortYardDetails = new List<RPMSortYardDetailsRegistrationModel>() { };
            //this.TireDetails = new TireDetailsRegistrationModel();
            this.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.TermsAndConditions = new TermsAndConditionsRegistrationModel();
            this.BankingInformation = new BankingInformationRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.RPMDetails = new RPMDetailsRegistrationModel();
            this.RegistrantInActiveStatus = new RegistrantStatusChange();

            ActiveInactive = new ActiveInactiveViewModel();
        }

        public int ID { get; set; }
        public int RegistrationNumber { get; set; }
        public int VendorID { get; set; }
        public ActivityStatusEnum ActivityStatus { get; set; }
        public bool IsInternal { get; set; }
        public string FormObject { get; set; }

        #region RPM/Collector Application models

        public BusinessRegistrationModel BusinessLocation { get; set; }
        public IList<ContactRegistrationModel> ContactInformationList { get; set; }
        public IList<RPMSortYardDetailsRegistrationModel> SortYardDetails { get; set; }
        public RPMTireDetailsRegistrationModel TireDetails { get; set; }        
        public RPMDetailsRegistrationModel RPMDetails { get; set; }
        public SupportingDocumentsRegistrationModel SupportingDocuments { get; set; }
        public TermsAndConditionsRegistrationModel TermsAndConditions { get; set; }
        public BankingInformationRegistrationModel BankingInformation { get; set; }
        public RemitanceDetails RemitanceDetail { get; set; }

        #endregion RPM/Collector Application models
        
        public IList<string> InvalidFormFields { get; set; }
        public RegistrantStatusChange RegistrantActiveStatus { get; set; }
        public RegistrantStatusChange RegistrantInActiveStatus { get; set; }
        public List<string> TireDetailsItemTypeString { get; set; }
        public List<string> ProductsProduceTypeString { get; set; }
        public string UserIPAddress { get; set; }
        public int ServerDateTimeOffset { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public ActiveInactiveViewModel ActiveInactive { get; set; } 
        public string GetClientStatus()
        {
            string status = "Open";
            switch (this.Status)
            {
                case ApplicationStatusEnum.Open:
                case ApplicationStatusEnum.BackToApplicant:
                    status = "Open";
                    break;

                case ApplicationStatusEnum.Submitted:
                    status = "Submitted";
                    break;

                case ApplicationStatusEnum.Assigned:
                case ApplicationStatusEnum.ReAssignToRep:
                case ApplicationStatusEnum.OffHold:
                case ApplicationStatusEnum.OnHold:
                    status = "Under Review";
                    break;

                case ApplicationStatusEnum.Approved:
                case ApplicationStatusEnum.Resend:
                case ApplicationStatusEnum.BankInformationSubmitted:
                case ApplicationStatusEnum.BankInformationApproved:
                    status = "Approved";
                    break;

                case ApplicationStatusEnum.Denied:
                    status = "Rejected";
                    break;

                case ApplicationStatusEnum.Completed:
                    status = "Completed";
                    break;
            }

            return status;
        }
        public DateTime CreatedDate { get; set; }
    }
}