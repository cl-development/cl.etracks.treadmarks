﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.Validation;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.CompanyBranding
{
    [Validator(typeof(CompanyInformationModelValidator))]
    public class CompanyInformationVM
    {
        public CompanyInformationVM()
        {           
            CountryList = new List<string>();
        }
        public string CompanyName { get; set; }       
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }       
        public string PhoneNumber { get; set; }
        public string Ext { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public List<string> CountryList { get; set; }
    }
}
