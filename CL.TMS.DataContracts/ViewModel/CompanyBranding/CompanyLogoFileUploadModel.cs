﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.CompanyBranding
{
    public class CompanyLogoFileUploadModel
    {
        public CompanyLogoFileUploadModel()
        {
            IsDefaultImage = false;
            IsOTSImage = true;
        }
        //public string LogoId { get; set; }
        public int? AttachmentId { get; set; }
        public string Type { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsOTSImage { get; set; }
        public bool IsDefaultImage { get; set; }
    }
}
