﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class StewardActivityHandlingModel
    {
        public bool IsSubmitted { get; set; }
        public bool IsReassigned { get; set; }
        public bool IsApproved { get; set; }
        public bool IsUnassigned { get; set; }
        public bool IsBackToParticipant { get; set; }
        public bool IsAssigned { get; set; }
        public bool IsAssignToMe { get; set; }
    }
}
