﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFRemittanceModel : BaseDTO<int>
    {
        public TSFRemittanceModel()
        {
            InitializeModel(0);
        }
        private void InitializeModel(int applicationId)
        {
            this.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Remittances };
            this.TSFRemittance = new TSFRemittanceItemModel();
            this.Status = TMS.Common.Enum.ClaimStatus.Unknown;
            ClaimCommonModel = new ClaimCommonModel();
            this.InvalidFormFields = new List<string>();
            this.ClaimStatus = new ClaimStatusViewModel();
            this.IsTaxApplicable = true; //OTSTM2-1224  Enable by default the HST calculation 
        }
        #region Common
        public ClaimCommonModel ClaimCommonModel { get; set; }
        #endregion
        public bool? Istaff { get; set; }
        public bool IsCreatedByStaff { get; set; } //OTSTM2-56
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }
        public DateTime? Submitted { get; set; }
        public string SubmittedBy { get; set; }
        public long SubmittedByUserId { get; set; }
        public DateTime? PeriodDate { get; set; }
        public string PayPeriod { get; set; }
        public DateTime? PayReceiptDate { get; set; }
        public DateTime? PayDepositDate { get; set; }
        public SelectList TSFRateDateList { get; set; } //add for 2x
        public int PeriodID { get; set; }
        public string ChequeReferenceNumber { get; set; }
        public string Account { get; set; }
        public int CustomerID { get; set; }
        public string PreparedBy { get; set; }
        public decimal? TotalTSFDue { get; set; }
        public decimal? ApplicableTaxesGst { get; set; }
        public decimal? ApplicableTaxesHst { get; set; }
        public bool IsTaxApplicable { get; set; }
        public decimal? TSFDuePostHst { get; set; }
        public decimal? TSFDuePreHst { get; set; }
        public decimal? Penalties { get; set; }
        public bool IsPenaltyOverride { get; set; }
        public decimal? PenaltiesManually { get; set; }
        public string PenaltyTooltipText { get; set; }
        public decimal PenaltyRate { get; set; }
        public decimal? Interest { get; set; }
        public decimal? TotalRemittancePayable { get; set; }
        public string PaymentType { get; set; }
        public string CurrencyType { get; set; }
        public string PaymentReferenceNumber { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool? SRemitsPerYears { get; set; }
        public DateTime? RemitsPerYearSwitchDate { get; set; }
        //public bool? SAudits { get; set; } //OTSTM2-1110 comment out
        public string RecordState { get; set; }
        public IReadOnlyList<TSFRemittanceItemModel> LineItems { get; set; }
        public SelectList Tires { get; set; }
        public SelectList CreditTires { get; set; }
        public decimal? Credit { get; set; }
        public int CategoryId { get; set; }
        public int ApplicationID { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ChequeDue { get; set; }
        //OTSTM2-1110 comment out
        //public DateTime? Onhold { get; set; }
        //public DateTime? Offhold { get; set; }
        public DateTime? Assigned { get; set; }
        public DateTime? StartedDate { get; set; }
        //public int OnholdDays { get; set; } //OTSTM2-1110 comment out
        public IList<SelectListItem> AdjustmentReasonTypeList { get; set; }
        public int AdjustmentReason { get; set; }
        public int TSFClaimID { get; set; }
        public IList<string> InvalidFormFields { get; set; }
        public int BatchNumber { get; set; }

        public decimal? InternalPaymentAdjustment { get; set; } //OTSTM2-485
        public int InternalPaymentAdjustmentCount { get; set; } //OTSTM2-485

        #region Panel attributes
        public SupportingDocumentsRegistrationModel SupportingDocuments { get; set; }
        public string SupportingDocOption { get; set; }
        public TSFRemittanceItemModel TSFRemittance { get; set; }
        public ClaimStatus Status { get; set; }
        public string StatusName { get; set; }
        public ActivityStatusEnum ActivityStatus { get; set; }
        public DateTime? ActivityApprovedDate { get; set; }
        public int ServerDateTimeOffset { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public Address Address { get; set; }
        public Contact PrimaryContact { get; set; }
        public string PayPeriodDesc { get; set; }
        public bool IsActive { get; set; }
        public long? AssignToUser { get; set; }
        #endregion

        #region Staff Specific
        public ClaimStatusViewModel ClaimStatus { get; set; }
        public ClaimWorkflowViewModel ClaimWorkflow { get; set; }
        public bool IsDeleteVisible { get; set; }
        #endregion

    }
}
public class BusinessLocation
{
    public Vendor BusinessLocationVendor { get; set; }
    public Address BusinessLocationAddress { get; set; }
    public bool MailAddressSameAsBusinessAddress { get; set; }
    public Address BusinessMailingAddress { get; set; }

}
public class ContactInformation
{
    public Address ContactInformationAddress { get; set; }
    public Contact ContactInformationContact { get; set; }
    public bool ContactAddressSameAsBusinessAddress { get; set; }
}



