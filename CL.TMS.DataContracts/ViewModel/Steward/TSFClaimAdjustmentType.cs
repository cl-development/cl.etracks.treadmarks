﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFClaimAdjustmentType
    {
        public string Name { get; set; }
        public int Id { get; set;}
    }
}
