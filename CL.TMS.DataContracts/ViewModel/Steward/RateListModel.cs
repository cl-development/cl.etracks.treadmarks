﻿using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class RateListModel : BaseDTO<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal ItemRate { get; set; }
    }
}
