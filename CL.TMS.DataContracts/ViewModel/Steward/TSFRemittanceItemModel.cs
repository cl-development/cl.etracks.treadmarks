﻿using CL.TMS.Framework.DTO;
using System;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFRemittanceItemModel //: BaseDTO<long>
    {
        public int Quantity { get; set; }
        public int NegativeAdjustment { get; set; }
        public decimal Rate { get; set; }
        public decimal Total { get; set; }
        public string Category { get; set; }
        public int CategoryId { get; set; }
        public int ClaimID { get; set; }
        public int DetailID { get; set; }
        public int Net { get; set; }
        public string OtherDesc { get; set; }
        public int AdjustmentReasonType { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? NegativeAdjDate { get; set; }
        public string strNegativeAdjDate
        {
            get
            {
                DateTime dt = NegativeAdjDate ?? DateTime.MinValue;
                string val = (DateTime.MinValue == dt ? string.Empty : dt.ToString("MMM yyyy"));
                return val;
            }
        }
        public int CreditItemID { get; set; }
        public int CreditNegativeAdj { get; set; }
        public decimal CreditTSFRate { get; set; }
        public decimal CreditTSFDue { get; set; }
        public DateTime? TSFRateDate { get; set; } //add for 2x
        public string TSFRateDateShortName { get; set; } //add for 2x
    }
}
