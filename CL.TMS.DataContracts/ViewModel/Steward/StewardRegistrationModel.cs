﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class StewardRegistrationModel : BaseDTO<long>, IBusinessLocation, IContactInformationListModel, ITermsAndConditions, ITireDetails
    {
        public StewardRegistrationModel()
        {
            InitializeModel(0);
        }

        public StewardRegistrationModel(int applicationId)
        {
            InitializeModel(applicationId);
        }

        private void InitializeModel(int applicationId)
        {
            this.ID = applicationId;
            InvalidFormFields = new List<string>();
            this.BusinessLocation = new BusinessRegistrationModel();
            this.ContactInformationList = new List<ContactRegistrationModel>();
            this.ContactInformation = new List<ContactInformation>();
            this.StewardDetails = new StewardDetailsRegistrationModel();
            this.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.TermsAndConditions = new TermsAndConditionsRegistrationModel();
            TireDetailsItemTypeString = new List<string>();
            BusinessActivities = new List<BusinessActivity>();
            this.ApplicationInvitationChecks = new ApplicationInvitationChecks();
            this.BankingInformation = new BankingInformationRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.RegistrantActiveStatus = new RegistrantStatusChange();
            this.RegistrantInActiveStatus = new RegistrantStatusChange();
            this.RemitanceDetail = new RemitanceDetails();

            ActiveInactive = new CustomerActiveInactiveViewModel();
        }

        public int ID { get; set; }
        public int RegistrationNumber { get; set; }
        public int VendorID { get; set; }
        public ActivityStatusEnum ActivityStatus { get; set; }
        public bool IsInternal { get; set; }
        public string FormObject { get; set; }

        #region Panel attributes

        public BusinessRegistrationModel BusinessLocation { get; set; }

        public IList<ContactRegistrationModel> ContactInformationList { get; set; }

        public StewardTireDetailsRegistrationModel TireDetails { get; set; }

        public StewardDetailsRegistrationModel StewardDetails { get; set; }

        public SupportingDocumentsRegistrationModel SupportingDocuments { get; set; }

        public TermsAndConditionsRegistrationModel TermsAndConditions { get; set; }

        #endregion Panel attributes

        public int ServerDateTimeOffset { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public RemitanceDetails RemitanceDetail { get; set; }
        public SRemitsPerYear SRemitsPerYears { get; set; }
        //public SAudit SAudits { get; set; } //OTSTM2-1110 comment out
        public SMOE SMOES { get; set; }
        public DateTime? SRemitsPerYearSwitchDate { get; set; }
        //public DateTime? SAuditSwitchDate { get; set; } //OTSTM2-1110 comment out
        public DateTime? SMOESwitchDate { get; set; }
        public ApplicationInvitationChecks ApplicationInvitationChecks { get; set; }
        public IList<string> InvalidFormFields { get; set; }
        public BankingInformationRegistrationModel BankingInformation { get; set; }
        public List<ContactInformation> ContactInformation { get; set; }
        public ApplicationStatusEnum Status { get; set; }
        public RegistrantStatusChange RegistrantActiveStatus { get; set; }
        public RegistrantStatusChange RegistrantInActiveStatus { get; set; }
        public List<string> TireDetailsItemTypeString { get; set; }
        public List<BusinessActivity> BusinessActivities { get; set; }
        public string UserIPAddress { get; set; }
        public CustomerActiveInactiveViewModel ActiveInactive { get; set; } 
        public string GetClientStatus()
        {
            string status = "Open";

            switch (this.Status)
            {
                case ApplicationStatusEnum.Open:
                case ApplicationStatusEnum.BackToApplicant:
                    status = "Open";
                    break;

                case ApplicationStatusEnum.Submitted:
                    status = "Submitted";
                    break;

                case ApplicationStatusEnum.Assigned:
                case ApplicationStatusEnum.ReAssignToRep:
                case ApplicationStatusEnum.OffHold:
                case ApplicationStatusEnum.OnHold:
                    status = "Under Review";
                    break;

                case ApplicationStatusEnum.Approved:
                case ApplicationStatusEnum.Resend:
                    status = "Approved";
                    break;

                case ApplicationStatusEnum.Denied:
                    status = "Rejected";
                    break;

                case ApplicationStatusEnum.Completed:
                    status = "Completed";
                    break;
            }

            return status;
        }

        //Added
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        public DateTime CreatedDate { get; set; }
        public long? AssignToUserId { get; set; }
    }

    public class StewardDetails
    {
        public Vendor StewardDetailsVendor { get; set; }
    }

    public class BusinessLocation
    {
        public Vendor BusinessLocationVendor { get; set; }
        public Address BusinessLocationAddress { get; set; }
        public bool MailAddressSameAsBusinessAddress { get; set; }
        public Address BusinessMailingAddress { get; set; }
    }

    public class ContactInformation
    {
        public Address ContactInformationAddress { get; set; }
        public Contact ContactInformationContact { get; set; }
        public bool ContactAddressSameAsBusinessAddress { get; set; }
    }

    public class SortYardDetails
    {
        public VendorStorageSite SortYardDetailsVendorStorageSiteModel { get; set; }

        public Address SortYardDetailsAddress { get; set; }
    }

    public class TireDetails
    {
        public List<Item> TireDetailsItemType { get; set; }
        public Vendor TireDetailsVendor { get; set; }
    }

    public class BusinessActivityDetails
    {
        public List<BusinessActivity> BusinessActivities { get; set; }
    }

    public class HaulerDetails
    {
        public Vendor HaulerDetailsVendor { get; set; }
    }

    public class BankingInformation
    {
        public BankInformation BankInformation { get; set; }
    }

    public class SupportingDocuments
    {
        public Attachment Attachment { get; set; }
    }

    public class TermsAndConditions
    {
        public Application TermsAndConditionsApplication { get; set; }
    }

    public class ApplicationInvitationChecks
    {
        public ApplicationInvitation ApplicationInvitationCheck { get; set; }
    }
}