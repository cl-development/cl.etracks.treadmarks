﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFRemittanceBriefModel : BaseDTO<long>
    {
        public string Period { get; set; }
        public DateTime? PeriodDate { get; set; }
        public string Status { get; set; }
        public decimal Due { get; set; }
        public decimal? Payments { get; set; }
        public decimal BalanceDue { get; set; }
        public DateTime? Submitted { get; set; }
        public string SubmittedBy { get; set; }
        public string RegNo { get; set; }
        public string Company { get; set; }
        public string AssignedTo { get; set; }
        public long? AssignedToUserId { get; set; }
        public Address Address { get; set; }
        public Contact PrimaryContact { get; set; }
        //OTSTM2-188
        public string ReportingFrequency { get; set; }
        public DateTime? DepositDate { get; set; }
        public decimal TotalReceivable { get; set; }
        public decimal ChequeAmount { get; set; }
        public UserReference CurrentUser { get; set; }
    }
}
