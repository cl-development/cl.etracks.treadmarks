﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.GroupRate;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.GroupRate;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class RateDetailsVM
    {
        public RateDetailsVM()
        {
            notes = new List<InternalNoteViewModel>();
            isSpecific = false;
        }
        public int RateTransactionID { get; set; }
        public string effectiveDate { get; set; }
        public int category { get; set; }
        public DateTime effectiveStartDate { get; set; }
        public DateTime effectiveEndDate { get; set; }
        public bool isSpecific { get; set; }
        public DateTime previousEffectiveStartDate { get; set; }//during edit Rate, use this value.AddDays(-1), to locate previous Rates 
        public string note { get; set; }
        public int decimalsize { get; set; }
        
        public List<InternalNoteViewModel> notes { get; set; }

        // Transportation Incentive Rates
        public TransportationIncentiveViewModel tiVM { get; set; }
        public NorthernPremiumRateVM northernPremiumRate { get; set; }
        public DOTPremiumRateVM DOTPremiumRate { get; set; }
        public IneligibleInventoryPaymentRateVM ineligibleInventoryPaymentRate { get; set; }

        // Collector Allowance Rates
        public CollectorAllowanceRateVM collectorAllowanceRate { get; set; }
        //  Processing Incentive SPS Rates (Tire Stewardship Fee Rates)
        public ProcessingIncentiveRateVM processingIncentiveRate { get; set; }
        public PISpecificRateVM piSpecificRate { get; set; }
        // Manufacturing Incentive Rates
        public ManufacturingIncentiveRateVM manufacturingIncentiveRate { get; set; }
        // Tire Stewardship Fees Rates
        public TireStewardshipFeesRateVM tireStewardshipFeesRate { get; set; }
        // Estimated Weights Rates
        public EstimatedWeightsRateVM estimatedWeightsRate { get; set; }
        // Penalty Rates
        public PenaltyRateVM penaltyRate { get; set; }
    }
}
