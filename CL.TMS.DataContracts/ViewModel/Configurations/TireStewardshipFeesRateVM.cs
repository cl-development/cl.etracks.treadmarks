﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class TireStewardshipFeesRateVM
    {
        public TireStewardshipFeesRateVM() { }
        public TireStewardshipFeesRateVM(TireStewardshipFeeParam param) {
            C1Name = param.cn1  ;
            C2Name  = param.cn2  ;
            C3Name  = param.cn3  ;
            C4Name  = param.cn4  ;
            C5Name  = param.cn5  ;
            C6Name  = param.cn6  ;
            C7Name  = param.cn7  ;
            C8Name  = param.cn8  ;
            C9Name  = param.cn9  ;
            C10Name = param.cn10 ;
            C11Name = param.cn11 ;
            C12Name = param.cn12 ;
            C13Name = param.cn13 ;
            C14Name = param.cn14 ;
            C15Name = param.cn15 ;
            C16Name = param.cn16 ;
            C17Name = param.cn17 ;
            C18Name = param.cn18 ;
        }
        public decimal C1 { get; set; }
        public decimal C2 { get; set; }
        public decimal C3 { get; set; }
        public decimal C4 { get; set; }
        public decimal C5 { get; set; }
        public decimal C6 { get; set; }
        public decimal C7 { get; set; }
        public decimal C8 { get; set; }
        public decimal C9 { get; set; }
        public decimal C10 { get; set; }
        public decimal C11 { get; set; }
        public decimal C12 { get; set; }
        public decimal C13 { get; set; }
        public decimal C14 { get; set; }
        public decimal C15 { get; set; }
        public decimal C16 { get; set; }
        public decimal C17 { get; set; }
        public decimal C18 { get; set; }

        public string C1Name { get; set; }
        public string C2Name { get; set; }
        public string C3Name { get; set; }
        public string C4Name { get; set; }
        public string C5Name { get; set; }
        public string C6Name { get; set; }
        public string C7Name { get; set; }
        public string C8Name { get; set; }
        public string C9Name { get; set; }
        public string C10Name{ get; set; }
        public string C11Name{ get; set; }
        public string C12Name{ get; set; }
        public string C13Name{ get; set; }
        public string C14Name{ get; set; }
        public string C15Name{ get; set; }
        public string C16Name{ get; set; }
        public string C17Name{ get; set; }
        public string C18Name{ get; set; }
    }
}
