﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class RecoveryEstimatedWeightListViewModel : BaseDTO<int>
    {
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateModified { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Notes { get; set; }
        public string NotesAllText
        {
            get
            {
                if (this.Category != 1)
                {
                    string sSum = string.Empty;
                    if (null != this.WeightNotes)
                        foreach (WeightTransactionNote item in this.WeightNotes)
                        {
                            if (null != item)
                            {
                                sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                            }
                        }
                    return sSum;
                }
                return null;
            }
        }
        public int Category { get; set; }
        [JsonIgnore]
        public List<WeightTransactionNote> WeightNotes { get; set; }
    }
}
