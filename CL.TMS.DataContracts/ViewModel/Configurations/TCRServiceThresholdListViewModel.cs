﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class TCRServiceThresholdListViewModel
    {
        public TCRServiceThresholdListViewModel()
        {
            trcServiceThresholds = new List<TCRListViewModel>();
            withoutThresholdVendors = new List<WithoutThesholdVendor>();
        }
        public List<TCRListViewModel> trcServiceThresholds { get; set; }
        public List<WithoutThesholdVendor> withoutThresholdVendors { get; set; }
        public int withThresholds { get; set; }
        public int reachedThresholds { get; set; }
        public int withoutThresholds { get; set; }
    }
    public class TCRListViewModel
    {
        public int ID { get; set; }
        public int VendorID { get; set; }
        public string RegNumber { get; set; }
        public string BusinessName { get; set; }
        public string Description { get; set; }
        public int ThresholdDays { get; set; }
        public int? DaysSinceLastTCR { get; set; }
        public int DaysLeft { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsActive { get; set; }

    }
    public class WithoutThesholdVendor
    {
        public int ID { get; set; }
        public int VendorID { get; set; }
        public string RegNumber { get; set; }
        public string BusinessName { get; set; }
        public string Description { get; set; }
        public int ThresholdDays { get; set; }
        public bool IsActive { get; set; }
    }
}


