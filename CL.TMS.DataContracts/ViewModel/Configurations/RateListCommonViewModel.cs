﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class RateListViewModel : BaseDTO<int>
    {
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateModified { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Notes { get; set; }
        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.RateNotes)
                    foreach (RateTransactionNote item in this.RateNotes)
                    {
                        if (null != item)
                        {
                            sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        public int Category { get; set; }
        [JsonIgnore]
        public List<RateTransactionNote> RateNotes { get; set; }
    }
    public class TIRateListViewModel : RateListViewModel
    {
        public TIRateListViewModel()
        {
            pickupRateGroups = new List<ItemModel<string>>();
            deliveryRateGroups = new List<ItemModel<string>>();
        }
        [JsonIgnore]
        public List<ItemModel<string>> pickupRateGroups { get; set; }
        [JsonIgnore]
        public List<ItemModel<string>> deliveryRateGroups { get; set; }
    }
    public class PIRateListViewModel : RateListViewModel
    {
        public bool PIType { get; set; }
        public string AssociatedVendorsText
        {
            get
            {
                string sSum = string.Empty;
                if (this.AssociatedVendorsList != null)
                    foreach (string item in this.AssociatedVendorsList)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            sSum += (item.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        /// <summary>
        /// When the processor rate is specific we list the associate vendorid first.
        /// </summary>
        [JsonIgnore]

        public IEnumerable<int> AssociateVendorIDs { get; set; }
        [JsonIgnore]
        public List<string> AssociatedVendorsList { get; set; }
    }
}
