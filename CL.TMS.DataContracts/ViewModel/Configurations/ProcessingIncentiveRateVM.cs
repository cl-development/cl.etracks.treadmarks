﻿using CL.TMS.DataContracts.ViewModel.Common;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class ProcessingIncentiveRateVM
    {
        #region //SPS OnRoad

        public decimal TDP1OnRoad { get; set; }
        public decimal TDP1FeedstockOnRoad { get; set; }
        public decimal TDP2OnRoad { get; set; }
        public decimal TDP2FeedstockOnRoad { get; set; }
        public decimal TDP3OnRoad { get; set; }
        public decimal TDP3FeedstockOnRoad { get; set; }
        public decimal TDP4OnRoad { get; set; }
        public decimal TDP5OnRoad { get; set; }

        #endregion //SPS OnRoad

        #region //SPS OffRoad

        public decimal TDP1OffRoad { get; set; }
        public decimal TDP1FeedstockOffRoad { get; set; }
        public decimal TDP2OffRoad { get; set; }
        public decimal TDP2FeedstockOffRoad { get; set; }
        public decimal TDP3OffRoad { get; set; }
        public decimal TDP3FeedstockOffRoad { get; set; }
        public decimal TDP4OffRoad { get; set; }
        public decimal TDP5OffRoad { get; set; }

        #endregion //SPS OffRoad

        #region //PIT

        public decimal TDP1 { get; set; }
        public decimal TDP2 { get; set; }
        public decimal TDP3 { get; set; }
        public decimal TDP4FF { get; set; }
        public decimal TDP4FFNoPI { get; set; }
        public decimal TDP5FT { get; set; }
        public decimal TDP5NT { get; set; }
        public decimal TDP6NP { get; set; }
        public decimal TDP6FP { get; set; }
        public decimal TDP7NT { get; set; }
        public decimal TDP7FT { get; set; }
        public decimal TransferFibreRubber { get; set; } //Transfer-Fibre/Rubber

        #endregion //PIT
    }

    public class PISpecificRateVM
    {
        public PISpecificRateVM()
        {
            AssignedProcessIDs = new List<ItemModel<string>>();
            AvailabledProcessIDs = new List<ItemModel<string>>();
        }

        public List<ItemModel<string>> AssignedProcessIDs { get; set; }
        public List<ItemModel<string>> AvailabledProcessIDs { get; set; }
    }
}