﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class RateParamsDTO
    {
        public RateParamsDTO() {         
        }
        public long userId { get; set; }
        public string categoryName { get; set; } 
        // Transportation Incentive 
        public TransportationIncentiveParam TI { get; set; }
        // Collector Allowance 
        public CollectorAllowanceParam CA { get; set; }
        //  Processing Incentive 
        public ProcessingIncentiveParam PI { get; set; }
        // Manufacturing Incentive 
        public ManufacturingIncentiveParam MI { get; set; }
        // Tire Stewardship Fees 
        public TireStewardshipFeeParam TFS { get; set; }
        // Estimated Weights 
        public EstimatedWeightsParam SW { get; set; }
        public PenaltyParam PE { get; set; }
    }
    public class TransportationIncentiveParam
    {
        public int n1ZoneId { get; set; }
        public int n2ZoneId { get; set; }
        public int n3ZoneId { get; set; }
        public int n4ZoneId { get; set; }
        public int southZoneId { get; set; }
        public int mooseCreekZoneId { get; set; }
        public int gtaZoneId { get; set; }
        public int wtcZoneId { get; set; }
        public int sturgeonFallsZoneId { get; set; }
        // itemtype
        public int itemTypeOnRoad { get; set; }
        public int itemTypeOffRoad { get; set; }
        //payment type id
        public int paymentTypeNorth { get; set; }
        public int paymentTypeDOT { get; set; }
        public int paymentTypeProcessorTI { get; set; }

        public int claimTypeHauler { get; set; }
        public int claimTypeProcessor { get; set; }
        public string pickupRateGroup { get; set; }
        public string deliveryRateGroup { get; set; }
    }
    public class CollectorAllowanceParam
    {

    }
    public class ProcessingIncentiveParam
    {
        public int claimType { get; set; }
        #region //ItemParams
        public int pitItemCategory { get; set; }
        public int spsItemCategory { get; set; }
        public int onRoad { get; set; }
        public int offRoad { get; set; }
        public int spsPaymentType { get; set; }
        public int pitPaymentType { get; set; }  
        public IEnumerable<Item> spsOnRoad { get; set; }
        public IEnumerable<Item> spsOffRoad { get; set; }
        public IEnumerable<Item> pit { get; set; }
        public int[] items { get; set; }
        #endregion

        #region // SPS On-Road
        public int tdp1OnRoad { get; set; }
        public int tdp1FeedstockOnRoad { get; set; }
        public int tdp2OnRoad { get; set; }
        public int tdp2FeedstockOnRoad { get; set; }
        public int tdp3OnRoad { get; set; }
        public int tdp3FeedstockOnRoad { get; set; }
        public int tdp4OnRoad { get; set; }
        public int tdp5OnRoad { get; set; }
        #endregion

        #region //SPS OffRoad
        public int tdp1OffRoad { get; set; }
        public int tdp1FeedstockOffRoad { get; set; }
        public int tdp2OffRoad { get; set; }
        public int tdp2FeedstockOffRoad { get; set; }
        public int tdp3OffRoad { get; set; }
        public int tdp3FeedstockOffRoad { get; set; }
        public int tdp4OffRoad { get; set; }
        public int tdp5OffRoad { get; set; }
        #endregion

        #region //PIT
        public int tdp1 { get; set; }
        public int tdp2 { get; set; }
        public int tdp3 { get; set; }
        public int tdp4FF { get; set; }
        public int tdp4FFNoPI { get; set; }
        public int tdp5FT { get; set; }
        public int tdp5NT { get; set; }
        public int tdp6NP { get; set; }
        public int tdp6FP { get; set; }
        public int tdp7NT { get; set; }
        public int tdp7FT { get; set; }
        public int transferFibreRubber { get; set; }
        #endregion
    }
    public class ManufacturingIncentiveParam
    {
        public int claimType { get; set; }
        public int calendaredId { get; set; }
        public int extrudedId { get; set; }
        public int moldedId { get; set; }
        public int[] items { get { return new int[] { calendaredId, extrudedId, moldedId }; } }
    }
    public class TireStewardshipFeeParam
    {
        public int claimType { get; set; }        
        public int c1 { get; set; }
        public int c2 { get; set; }
        public int c3 { get; set; }
        public int c4 { get; set; }
        public int c5 { get; set; }
        public int c6 { get; set; }
        public int c7 { get; set; }
        public int c8 { get; set; }
        public int c9 { get; set; }
        public int c10 { get; set; }
        public int c11 { get; set; }
        public int c12 { get; set; }
        public int c13 { get; set; }
        public int c14 { get; set; }
        public int c15 { get; set; }
        public int c16 { get; set; }
        public int c17 { get; set; }
        public int c18 { get; set; }
        public string cn1 { get; set; }
        public string cn2 { get; set; }
        public string cn3 { get; set; }
        public string cn4 { get; set; }
        public string cn5 { get; set; }
        public string cn6 { get; set; }
        public string cn7 { get; set; }
        public string cn8 { get; set; }
        public string cn9 { get; set; }
        public string cn10 { get; set; }
        public string cn11 { get; set; }
        public string cn12 { get; set; }
        public string cn13 { get; set; }
        public string cn14 { get; set; }
        public string cn15 { get; set; }
        public string cn16 { get; set; }
        public string cn17 { get; set; }
        public string cn18 { get; set; }
        public int[] items { get { return new int[] { c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18 }; } }
    }
    public class EstimatedWeightsParam
    {
        public int claimType { get; set; }
        public string userName { get; set; }
        public int PLT { get; set; }
        public int MT { get; set; }
        public int AGLS { get; set; }
        public int IND { get; set; }
        public int SOTR { get; set; }
        public int MOTR { get; set; }
        public int LOTR { get; set; }
        public int GOTR { get; set; }
        public int[] items { get { return new int[] { PLT, MT, AGLS, IND, SOTR, MOTR, LOTR, GOTR }; } }
    }
    public class PenaltyParam
    {
        public int claimType { get; set; }
        public int ratePaymentType { get; set; }
    }
}
