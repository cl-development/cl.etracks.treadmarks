﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class TCRServiceThresholdViewModel : BaseDTO<int>
    {
        public int VendorID { get; set; }
        public string RegNumber { get; set; }
        public string BusinessName { get; set; }
        public int ThresholdDays { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreatedByID { get; set; }
        public long? ModifiedByID { get; set; }
    }
}
