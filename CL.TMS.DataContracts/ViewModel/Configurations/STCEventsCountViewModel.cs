﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class STCEventsCountViewModel : BaseDTO<int>
    {
        public int Pending { get; set; }
        public int Complete { get; set; }
        public int Expired { get; set; }
    }
}
