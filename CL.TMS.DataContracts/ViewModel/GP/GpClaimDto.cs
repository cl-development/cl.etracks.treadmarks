﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpClaimDto
    {
        public int ClaimId { get; set; }
        public string RegistrationNumber { get; set; }
        public int ParticipantId { get; set; }
        public decimal TotalClaimAmount { get; set; }
        public Period Period { get; set; }
        //public GpiBatch GpiBatch { get; set; }
        //public GpiBatchEntry GpiBatchEntry { get; set; }
        public int GpiBatchId { get; set; }
        public int GpiBatchEntryId { get; set; }
        public bool? IsTaxApplicable { get; set; } //OTSTM2-1294
    }
}
