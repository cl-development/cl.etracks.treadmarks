﻿using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GPModalListViewModal : BaseDTO<int>
    {
        public string GPTransactionNum { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string GpiStatusId { get; set; }
        public string GpiBatchEntryId { get; set; }
    }
}
