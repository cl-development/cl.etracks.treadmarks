﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpParticipantUpdate
    {
        public GpParticipantUpdate()
        {
            
        }

        public GpParticipantUpdate(object participant)
        {
            var vendor = participant as Vendor;
            if (vendor != null)
            {
                var oldMailingAddress = (vendor.VendorAddresses.FirstOrDefault(a => a.AddressType == (int)AddressTypeEnum.Mail) ??
                                                            vendor.VendorAddresses.FirstOrDefault(a => a.AddressType == (int)AddressTypeEnum.Business)) ?? new Address();
                //get the first primary contact
                var oldContact = vendor.VendorAddresses.Where(a => a.Contacts.Any()).Select(i => new
                {
                    contact = i.Contacts.FirstOrDefault(c => c.IsPrimary ?? false)
                }).Select(i => i.contact).FirstOrDefault() ?? new Contact();

                this.MailingAddress1 = oldMailingAddress.Address1 ?? string.Empty;
                this.MailingAddress2 = oldMailingAddress.Address2 ?? string.Empty;
                this.MailingCity = oldMailingAddress.City ?? string.Empty;
                this.MailingPostalCode = oldMailingAddress.PostalCode ?? string.Empty;
                this.MailingCountry = oldMailingAddress.Country ?? string.Empty;
                this.MailingProvince = oldMailingAddress.Province ?? string.Empty;
                this.PrimaryContactName = string.Format("{0} {1}", oldContact.FirstName ?? string.Empty, oldContact.LastName ?? string.Empty);
                this.PrimaryContactPhone = oldContact.PhoneNumber ?? string.Empty;
                this.IsTaxExempt = vendor.IsTaxExempt;
                this.IsActive = vendor.IsActive;
            }

            var customer = participant as Customer;
            if(customer != null)
            {
                var oldMailingAddress = (customer.CustomerAddresses.FirstOrDefault(a => a.AddressType ==  (int)AddressTypeEnum.Mail) ??
                                                            customer.CustomerAddresses.FirstOrDefault(a => a.AddressType == (int)AddressTypeEnum.Business)) ?? new Address();
                //get the first primary contact
                var oldContact = customer.CustomerAddresses.Where(a => a.Contacts.Any()).Select(i => new
                {
                    contact = i.Contacts.FirstOrDefault(c => c.IsPrimary ?? false)
                }).Select(i => i.contact).FirstOrDefault() ?? new Contact();

                this.MailingAddress1 = oldMailingAddress.Address1 ?? string.Empty;
                this.MailingAddress2 = oldMailingAddress.Address2 ?? string.Empty;
                this.MailingCity = oldMailingAddress.City ?? string.Empty;
                this.MailingPostalCode = oldMailingAddress.PostalCode ?? string.Empty;
                this.MailingCountry = oldMailingAddress.Country ?? string.Empty;
                this.MailingProvince = oldMailingAddress.Province ?? string.Empty;
                this.PrimaryContactName = string.Format("{0} {1}", oldContact.FirstName ?? string.Empty, oldContact.LastName ?? string.Empty);
                this.PrimaryContactPhone = oldContact.PhoneNumber ?? string.Empty;
                this.IsTaxExempt = customer.IsTaxExempt;
                this.IsActive = customer.IsActive;                 
            }
        }


        public bool Equals(GpParticipantUpdate obj)
        {
            return (string.Equals((this.MailingAddress1 ?? string.Empty).Trim(), (obj.MailingAddress1 ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.MailingAddress2 ?? string.Empty).Trim(), (obj.MailingAddress2 ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                //string.Equals(this.MailingAddress3, obj.MailingAddress3, StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.MailingCity ?? string.Empty).Trim(), (obj.MailingCity ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.MailingPostalCode ?? string.Empty).Trim(), (obj.MailingPostalCode ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.MailingCountry ?? string.Empty).Trim(), (obj.MailingCountry ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.MailingProvince ?? string.Empty).Trim(), (obj.MailingProvince ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.PrimaryContactName ?? string.Empty).Trim(), (obj.PrimaryContactName ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                string.Equals((this.PrimaryContactPhone ?? string.Empty).Trim(), (obj.PrimaryContactPhone ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                //string.Equals(this.PrimaryContactFax, obj.PrimaryContactFax, StringComparison.OrdinalIgnoreCase) &&
                (this.IsTaxExempt == obj.IsTaxExempt) && (this.IsActive == obj.IsActive));
        }

        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        //public string MailingAddress3 { get; set; } //Not Available
        public string MailingCity { get; set; }
        public string MailingPostalCode { get; set; }
        public string MailingCountry { get; set; }
        public string MailingProvince { get; set; }
        public string PrimaryContactName { get; set; }
        public string PrimaryContactPhone { get; set; }
        //public string PrimaryContactFax { get; set; }  //Not Available
        public bool IsTaxExempt { get; set; }
        public bool IsActive { get; set; }

    }
}
