﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpRrOtsRmTransDistributionsDto
    {
        public string DOCNUMBR { get; set; }    
        public string CUSTNMBR { get; set; }
        public int DISTTYPE { get; set; }
        public string ACTNUMST { get; set; }
        public decimal DEBITAMT { get; set; }
        public decimal CRDTAMNT { get; set; }
        public string PAYMENTTYPE { get; set; }    
        public Int16? RMDTYPAL { get; set; }
    }
}
