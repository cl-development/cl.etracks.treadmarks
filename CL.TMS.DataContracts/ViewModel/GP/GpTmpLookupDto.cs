﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpTmpLookupDto
    {
        public string RegisterNumber { get; set; }
        public int ClaimId { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public string SubmissionNumber { get; set; }
        public string DataReceived { get; set; } 
    }
}
