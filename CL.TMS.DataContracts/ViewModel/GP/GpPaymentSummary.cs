﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpPaymentSummary
    {
        public string TransactionNumber { get; set; }
        public string ChequeNumber { get; set; }
        public decimal AmountPaid { get; set; }
        public DateTime PaidDate { get; set; }
    }
}
