﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpProcessorOtsPmTransactionDto
    {
        public string VCHNUMWK { get; set; }
        public int DOCTYPE { get; set; }
        public string VENDORID { get; set; }
        public int DISTTYPE { get; set; }
        public string ACTNUMST { get; set; }
        public decimal DEBITAMT { get; set; }
        public decimal CRDTAMNT { get; set; }
        public decimal EstWeight { get; set; }
        public string INTERID { get; set; }
        public int GpBatchEntryId { get; set; }
        public int GpBatchId { get; set; }
        public bool IsTipi { get; set; }
        public decimal TiClaimAmount { get; set; }
    }
}
