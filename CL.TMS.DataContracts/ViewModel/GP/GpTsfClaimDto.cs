﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpTsfClaimDto
    {
        public int ClaimId { get; set; }
        public string RegistrationNumber { get; set; }
        public Period Period { get; set; }
        public GpiBatch GpiBatch { get; set; }
        public GpiBatchEntry GpiBatchEntry { get; set; }
        public string GpCheckBookName { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? DepositDate { get; set; }
        public DateTime? ReceiptDate{ get; set; }
        public bool? IsTaxExempt { get; set; }
        public decimal ApplicableTaxesHst { get; set; }
        public decimal TotalTsfDue { get; set; }
        public decimal Credit { get; set; }
        public decimal Penalties { get; set; }
        public decimal Interest { get; set; }
        public decimal TotalRemittancePayable { get; set; }
        public decimal PaymentAmount { get; set; }
        public string CurrencyType { get; set; }
        public decimal InternalPaymentAdjustment { get; set; } //OTSTM2-652
    }
}
