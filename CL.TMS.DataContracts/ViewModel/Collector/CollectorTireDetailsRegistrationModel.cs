﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorTireDetailsRegistrationModel : RegistrationModelBase
    {
        public IList<TireDetailsItemTypeModel> TireDetailsItemType { get; set; }

        public string PLT { get; set; }
        public string MT { get; set; }
        public string AGLS { get; set; }
        public string IND { get; set; }
        public string SOTR { get; set; }
        public string MOTR { get; set; }
        public string LOTR { get; set; }
        public string GOTR { get; set; }

    }
}
