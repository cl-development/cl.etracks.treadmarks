﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorClaimInboundReuseTireModel
    {
        public CollectorClaimInboundReuseTireModel()
        {
            InboundList = new List<TireOriginItemRow>();
            ReuseTire = new ReuseTiresItemRow();
            TotalInbound = new ItemRow();
        }
        #region Inbound
        public List<TireOriginItemRow> InboundList { get; set; }
        public ItemRow TotalInbound { get; set; }
        #endregion

        #region Reuse Tires
        public ReuseTiresItemRow ReuseTire { get; set; }        
        #endregion
    }
}
