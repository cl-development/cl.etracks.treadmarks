﻿using CL.TMS.DataContracts.ViewModel.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    [JsonObject]
    public class ReuseTiresItemRow:ItemRow
    {
        public string ReuseTires { get; set; }
    }
}
