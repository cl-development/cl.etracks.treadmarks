﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorAllowancePaymentDetailVM
    {
        public decimal PLTRate { get; set; }
        public int PLTTireCountQty { get; set; }
        public decimal PLTPayment
        {
            get { return UsingOldRounding ? Math.Round(PLTRate * PLTTireCountQty, 2) : Math.Round(PLTRate * PLTTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }
        public decimal MTRate { get; set; }
        public int MTTireCountQty { get; set; }
        public decimal MTPayment
        {
            get { return UsingOldRounding ? Math.Round(MTRate * MTTireCountQty, 2) : Math.Round(MTRate * MTTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal AGLSRate { get; set; }
        public int AGLSTireCountQty { get; set; }
        public decimal AGLSPayment
        {
            get { return UsingOldRounding ? Math.Round(AGLSRate * AGLSTireCountQty, 2) : Math.Round(AGLSRate * AGLSTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal INDRate { get; set; }
        public int INDTireCountQty { get; set; }
        public decimal INDPayment
        {
            get { return UsingOldRounding ? Math.Round(INDRate * INDTireCountQty, 2) : Math.Round(INDRate * INDTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal SOTRRate { get; set; }
        public int SOTRTireCountQty { get; set; }
        public decimal SOTRPayment
        {
            get { return UsingOldRounding ? Math.Round(SOTRRate * SOTRTireCountQty, 2) : Math.Round(SOTRRate * SOTRTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal MOTRRate { get; set; }
        public int MOTRTireCountQty { get; set; }
        public decimal MOTRPayment
        {
            get { return UsingOldRounding ? Math.Round(MOTRRate * MOTRTireCountQty, 2) : Math.Round(MOTRRate * MOTRTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal LOTRRate { get; set; }
        public int LOTRTireCountQty { get; set; }
        public decimal LOTRPayment
        {
            get { return UsingOldRounding ? Math.Round(LOTRRate * LOTRTireCountQty, 2) : Math.Round(LOTRRate * LOTRTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal GOTRRate { get; set; }
        public int GOTRTireCountQty { get; set; }
        public decimal GOTRPayment
        {
            get { return UsingOldRounding ? Math.Round(GOTRRate * GOTRTireCountQty, 2) : Math.Round(GOTRRate * GOTRTireCountQty, 2, MidpointRounding.AwayFromZero); }
        }
        public decimal PaymentTotal
        {
            get { return UsingOldRounding ? Math.Round(PLTPayment + MTPayment + AGLSPayment + INDPayment + SOTRPayment + MOTRPayment + LOTRPayment + GOTRPayment, 2) : Math.Round(PLTPayment + MTPayment + AGLSPayment + INDPayment + SOTRPayment + MOTRPayment + LOTRPayment + GOTRPayment, 2, MidpointRounding.AwayFromZero); }
        }
        public bool UsingOldRounding { get; set; }
    }
}
