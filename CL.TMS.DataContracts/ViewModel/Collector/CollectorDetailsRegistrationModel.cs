﻿using System;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.DomainEntities;
using System.Web.Mvc;
using System.Collections.Generic;
namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorDetailsRegistrationModel : RegistrationModelBase
    {
        public DateTime? BusinessStartDate { get; set; }
        public DateTime? GeneratorDate { get; set; }
        public bool GeneratorStatus { get; set; }
        public DateTime? SubCollectorDate { get; set; } //OTSTM2-953
        public bool SubCollectorStatus { get; set; } //OTSTM2-953
        public string BusinessNumber { get; set; }
        public string CommercialLiabHstNumber { get; set; }
        public bool IsTaxExempt { get; set; }

        public string BusinessActivity { get; set; }

        public string CommercialLiabInsurerName { get; set; }
        public DateTime? CommercialLiabInsurerExpDate { get; set; }

        public bool? HasMoreThanOneEmp { get; set; }

        public string WsibNumber { get; set; }

        public IList<SelectListItem> BusinessActivites { get; set; }
    }
}
