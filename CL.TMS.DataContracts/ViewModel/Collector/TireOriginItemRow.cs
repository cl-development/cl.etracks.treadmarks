﻿using CL.TMS.DataContracts.ViewModel.Common;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    [JsonObject]
    public class TireOriginItemRow:ItemRow
    {
        public string TireOrigin { get; set; }
        public int TireOriginValue { get; set; }
        public int DisplayOrder { get; set; }
    }
}
