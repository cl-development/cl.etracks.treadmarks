﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System.Diagnostics;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class StaffAllClaimsModel : BaseDTO<Guid>
    {
        public int ClaimId { get; set; }
        public string RegNumber { get; set; }
        public ClaimType Type { get; set; }
        public string Company { get; set; }
        public string Period { get; set; }
        public DateTime? Submitted { get; set; }
        public DateTime SubmittedSerachVal
        {
            get
            {
                return Submitted ?? DateTime.MinValue;
            }
        }
        public string Status { get; set; }
        public string AssignedTo { get; set; }
        public long AssignedToUserId { get; set; }
        public string ClaimsActions { get; set; }
        public int RoutingCount { get; set; }
        public DateTime ClaimStartDate { get; set; }
        public DateTime? ClaimOnholdDate { get; set; }
        public DateTime? ChequeDueDate { get; set; }
        public DateTime? PaymentDue
        {
            get
            {
                //return ChequeDueDate + OnHoldDays;
                return ChequeDueDate; //OTSTM2-194
            }
        }
        public TimeSpan ClaimOnHoldDays
        {
            get
            {
                bool onhold = ClaimOnhold ?? false;
                if ((null == ClaimOnholdDate) || (!onhold))
                {
                    return TimeSpan.Zero;
                }
                DateTime parseResult = DateTime.MinValue;
                DateTime.TryParse(this.ClaimOnholdDate.ToString(), out parseResult);
                return DateTime.UtcNow.Date - parseResult.Date;
            }
        }
        public Decimal? TotalClaimAmount { get; set; }
        public string HoldStatus { get; set; }
        public string AuditHoldStatus { get; set; }
        public bool? ClaimOnhold { get; set; }
        public bool? AuditOnhold { get; set; }
    }
}
