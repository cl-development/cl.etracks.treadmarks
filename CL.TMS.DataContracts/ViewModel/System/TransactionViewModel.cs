﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class TransactionViewModel:BaseDTO<Guid>
    {
        public string TransactionStatusTypeNameKey { get; set; }
        public long FriendlyId { get; set; }
        public string DeviceName { get; set; }
        public string TransactionTypeShortNameKey { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public decimal IncomingRegistrationNumber { get; set; }
        public decimal OutgoingRegistrationNumber { get; set; }
        public long IncomingUserId { get; set; }

        public long TransactionTypeId { get; set; }

        public string BadgeId { get; set; }

        public CommonSupportingDocumentsModel supportDocs { get; set; }
    }
}
