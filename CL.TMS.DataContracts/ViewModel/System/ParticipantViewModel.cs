﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class ParticipantViewModel
    {
        public string Number { get; set; }

        //[Required(ErrorMessage = "Business Name is required.")]
        public string BusinessName { get; set; }
        public Address AddressInfo { get; set; }

        public ParticpantType ParticipantType { get; set; }

        public string AddressInfoAddress1
        {
            get
            {
                string result = string.Empty;
                if (AddressInfo != null)
                    result = AddressInfo.Address1;
                return result;
            }
        }

        public int NumberOfIpads { get; set; }
        public int NumberOfBadges { get; set; }
        public bool Status { get; set; }


    }
}
