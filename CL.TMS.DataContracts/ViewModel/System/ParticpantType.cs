﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class ParticpantType
    {
        public string Type { get; set; }
        public int ID { get; set; }
    }
}
