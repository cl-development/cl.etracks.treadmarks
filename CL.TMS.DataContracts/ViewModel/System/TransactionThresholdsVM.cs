﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.Validation;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [Validator(typeof(TransactionThresholdsModelValidator))]
    public class TransactionThresholdsVM
    {
        //Individual Tire Counts
        public int? Threshold_IndividualTireCountPLT { get; set; }
        public int? Threshold_IndividualTireCountMT { get; set; }
        public int? Threshold_IndividualTireCountAGLS { get; set; }
        public int? Threshold_IndividualTireCountIND { get; set; }
        public int? Threshold_IndividualTireCountSOTR { get; set; }
        public int? Threshold_IndividualTireCountMOTR { get; set; }
        public int? Threshold_IndividualTireCountLOTR { get; set; }
        public int? Threshold_IndividualTireCountGOTR { get; set; }
        public bool Threshold_CBIndividualTireCount { get; set; } //check box

        //Total Tire Counts
        public int? Threshold_TotalTireCountDOR { get; set; }
        public int? Threshold_TotalTireCountDOT { get; set; }
        public int? Threshold_TotalTireCountHIT { get; set; }
        public int? Threshold_TotalTireCountPTR { get; set; }
        public int? Threshold_TotalTireCountRTR { get; set; }
        public int? Threshold_TotalTireCountSTC { get; set; }
        public int? Threshold_TotalTireCountTCR { get; set; }
        public int? Threshold_TotalTireCountUCR { get; set; }
        public bool Threshold_CBTotalTireCount { get; set; } //check box
        public bool Threshold_CBAutoAppDOT { get; set; } //check box
        public bool Threshold_CBAutoAppHIT { get; set; } //check box
        public bool Threshold_CBAutoAppPIT { get; set; } //check box
        public bool Threshold_CBAutoAppPTR { get; set; } //check box
        public bool Threshold_CBAutoAppSTC { get; set; } //check box
        public bool Threshold_CBAutoAppTCR { get; set; } //check box
        public bool Threshold_CBAutoAppUCR { get; set; } //check box

    }
}
