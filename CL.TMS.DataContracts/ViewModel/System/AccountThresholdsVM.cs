﻿using CL.TMS.DataContracts.ViewModel.Common;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class AccountThresholdsVM
    {
        public string applicationTypeID = "Threshold";
        public int decimalsize = 0;
        public string note { get; set; }
        public IEnumerable<ItemModel<string>> Items { get; set; }
    }
}