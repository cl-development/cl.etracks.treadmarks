﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class MaterialCompositionsVM : BaseDTO<int>
    {
        public int? ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemShortName { get; set; }    
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }
        public List<ItemModel<string>> recoverableMaterials { get; set; }
        public List<ItemRecoverableMaterialVM> ItemMaterialList { get; set; }
    }
  
}