﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class StaffCategoryViewModel:BaseDTO<int>
    {
        public DateTime AddedDate { get; set; }
        public string Status { get; set; }
        public string CategoryName { get; set; }
        public string[] StaffCategoryNotesAllText 
        {
            get
            {
                var notes = new List<string>();
                if (this.StaffCategoryNotes != null)
                    foreach (CategoryNote item in this.StaffCategoryNotes)
                    {
                        if (item != null)
                        {
                            notes.Add(string.Format("- {0} <hr>", item.Note));
                        }
                    }
                return notes.ToArray();
            }        
        }

        [JsonIgnore]
        public List<CategoryNote> StaffCategoryNotes { get; set; }
    }
}
