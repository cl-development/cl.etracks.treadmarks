﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection.Export
{
    public class ProductCategoryExportModel
    {
        [Display(Name = "Category Id")]
        public string CategoryId { get; set; }
        [Display(Name = "Added Date")]
        public DateTime AddedDate { get; set; }
        public string Status { get; set; }
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        public string Notes { get; set; }
    }
}
