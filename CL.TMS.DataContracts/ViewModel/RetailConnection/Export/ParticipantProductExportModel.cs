﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection.Export
{
    public class ParticipantProductExportModel
    {
        //View Model
        [Display(Name = "Date Added")]
        public DateTime? ApprovedDate { get; set; }
        public string Status { get; set; }
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        [Display(Name = "Brand Name")]
        public string BrandName { get; set; }
        [Display(Name = "Added By")]
        public string AddedBy { get; set; }

        //Form Model
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; } 
        public string Description { get; set; }
        [Display(Name = "Retailer Name And Url")]
        public string RetailerNameAndUrl { get; set; }

    }
}
