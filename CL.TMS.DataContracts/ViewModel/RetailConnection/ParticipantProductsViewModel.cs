﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class ParticipantProductsViewModel : BaseDTO<int>
    {
        public DateTime? ApprovedDate { get; set; }
        public string Status { get; set; }
        public string ProductName { get; set; }
        public string BrandName { get; set; }
        public string AddedBy { get; set; }
    }
}
