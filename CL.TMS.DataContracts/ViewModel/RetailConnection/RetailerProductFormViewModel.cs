﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class RetailerProductFormViewModel
    {
        public int RetailerId { get; set; }
        public string ProductUrl { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public string RetailerName { get; set; }
    }
}
