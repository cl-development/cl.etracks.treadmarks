﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class ProductsFormViewModel
    {
        public ProductsFormViewModel()
        {
            this.UploadDocument = new RcProductFileUploadModel();
            this.RetailerProducts = new List<RetailerProductFormViewModel>();
            this.ProductId = Guid.NewGuid();
        }

        public int Id { get; set; }
        public Guid ProductId { get; set; }
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public string BrandName { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public int VendorId { get; set; }
        public RcProductFileUploadModel UploadDocument { get; set; }
        public List<RetailerProductFormViewModel> RetailerProducts { get; set; }
    }
}
