﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection.Email
{
    public class ProductRetailerApproveEmailModel
    {
        public string EmailAddress { get; set; }
        public string Name { get; set; }
    }
}
