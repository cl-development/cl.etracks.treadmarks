﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Processor
{
    public class ProcessorRegistrationModel : RegistrationModelBase, IBusinessLocation, IContactInformationListModel, ITermsAndConditions, ITireDetails
    {

        public ProcessorRegistrationModel()
        {
            InitializeModel(0);
        }

        public ProcessorRegistrationModel(int applicationId)
        {
            InitializeModel(applicationId);
        }

        private void InitializeModel(int applicationId)
        {
            this.ID = applicationId;
            this.InvalidFormFields = new List<string>();
            this.TireDetailsItemTypeString = new List<string>();
            this.ProductsProduceTypeString = new List<string>();
            this.BusinessLocation = new BusinessRegistrationModel();
            this.ContactInformationList = new List<ContactRegistrationModel>() { };
            this.SortYardDetails = new List<ProcessorSortYardDetailsRegistrationModel>() { };
            //this.TireDetails = new TireDetailsRegistrationModel();
            this.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.TermsAndConditions = new TermsAndConditionsRegistrationModel();
            this.BankingInformation = new BankingInformationRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.ProcessorDetails = new ProcessorDetailsRegistrationModel();
            this.RegistrantInActiveStatus = new RegistrantStatusChange();

            ActiveInactive = new ActiveInactiveViewModel();
        }

        public int ID { get; set; }
        public int RegistrationNumber { get; set; }
        public int VendorID { get; set; }
        public ActivityStatusEnum ActivityStatus { get; set; }
        public bool IsInternal { get; set; }
        public string FormObject { get; set; }
        public string GroupName { get; set; }

        #region Processor/Collector Application models

        public BusinessRegistrationModel BusinessLocation { get; set; }
        public IList<ContactRegistrationModel> ContactInformationList { get; set; }
        public IList<ProcessorSortYardDetailsRegistrationModel> SortYardDetails { get; set; }
        public ProcessorTireDetailsRegistrationModel TireDetails { get; set; }
        public ProcessorDetailsRegistrationModel ProcessorDetails { get; set; }
        public SupportingDocumentsRegistrationModel SupportingDocuments { get; set; }
        public TermsAndConditionsRegistrationModel TermsAndConditions { get; set; }
        public BankingInformationRegistrationModel BankingInformation { get; set; }
        public RemitanceDetails RemitanceDetail { get; set; }

        #endregion Processor/Collector Application models

        public IList<string> InvalidFormFields { get; set; }
        public RegistrantStatusChange RegistrantActiveStatus { get; set; }
        public RegistrantStatusChange RegistrantInActiveStatus { get; set; }
        public List<string> TireDetailsItemTypeString { get; set; }
        public List<string> ProductsProduceTypeString { get; set; }
        public string UserIPAddress { get; set; }
        public int ServerDateTimeOffset { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public ActiveInactiveViewModel ActiveInactive { get; set; }
        public string GetClientStatus()
        {
            string status = "Open";
            switch (this.Status)
            {
                case ApplicationStatusEnum.Open:
                case ApplicationStatusEnum.BackToApplicant:
                    status = "Open";
                    break;

                case ApplicationStatusEnum.Submitted:
                    status = "Submitted";
                    break;

                case ApplicationStatusEnum.Assigned:
                case ApplicationStatusEnum.ReAssignToRep:
                case ApplicationStatusEnum.OffHold:
                case ApplicationStatusEnum.OnHold:
                    status = "Under Review";
                    break;

                case ApplicationStatusEnum.Approved:
                case ApplicationStatusEnum.Resend:
                case ApplicationStatusEnum.BankInformationSubmitted:
                case ApplicationStatusEnum.BankInformationApproved:
                    status = "Approved";
                    break;

                case ApplicationStatusEnum.Denied:
                    status = "Rejected";
                    break;

                case ApplicationStatusEnum.Completed:
                    status = "Completed";
                    break;
            }

            return status;
        }
        public DateTime CreatedDate { get; set; }
        public int SelectedVendorGroupId { get; set; }
        public int rateGroupId;
        public long approvedByUserId;
    }
}