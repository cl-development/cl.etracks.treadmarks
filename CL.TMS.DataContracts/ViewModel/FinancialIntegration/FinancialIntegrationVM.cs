﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.DataContracts.ViewModel.FinancialIntegration
{
    public class FinancialIntegrationVM
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string AccountLabel { get; set; }
        public string AccountNumber { get; set; }
        public int DisplayOrder { get; set; }
    }
}
