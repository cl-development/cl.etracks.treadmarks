﻿using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class RoleListViewModel : BaseDTO<int>
    {
        public string RoleName { get; set; }
        public DateTime UpdateDate { get; set; }

        public int AssociatedUsers { get; set; }
        public string AllAssociatedUsersText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.AllAssociatedUsersList)
                    foreach (string item in this.AllAssociatedUsersList)
                    {
                        if (null != item)
                        {
                            //sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                            sSum += (item.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        [JsonIgnore]
        public List<string> AllAssociatedUsersList { get; set; }

    }
}
