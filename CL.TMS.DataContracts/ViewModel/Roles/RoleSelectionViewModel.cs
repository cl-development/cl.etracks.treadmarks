﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class RoleSelectionViewModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (!(obj is RoleSelectionViewModel))
                throw new ArgumentException("obj is not an RoleSelectionViewModel");
            var role = obj as RoleSelectionViewModel;
            if (role == null)
                return false;
            return this.Id.Equals(role.Id);
        }
    }
}
