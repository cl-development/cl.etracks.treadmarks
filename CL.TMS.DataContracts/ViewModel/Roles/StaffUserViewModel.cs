﻿using CL.TMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class StaffUserViewModel
    {
        
        public StaffUserViewModel()
        {
            StewardClaimsWorkflow = new ClaimsWorkflowViewModel(TreadMarksConstants.StewardAccount);
            CollectorClaimsWorkflow = new ClaimsWorkflowViewModel(TreadMarksConstants.CollectorAccount);
            HaulerClaimsWorkflow = new ClaimsWorkflowViewModel(TreadMarksConstants.HaulerAccount);
            ProcessorClaimsWorkflow = new ClaimsWorkflowViewModel(TreadMarksConstants.ProcessorAccount);
            RPMClaimsWorkflow = new ClaimsWorkflowViewModel(TreadMarksConstants.RPMAccount);
            StewardApplicationsWorkflow = new ApplicationsWorkflowViewModel(TreadMarksConstants.StewardAccount);
            CollectorApplicationsWorkflow = new ApplicationsWorkflowViewModel(TreadMarksConstants.CollectorAccount);
            HaulerApplicationsWorkflow = new ApplicationsWorkflowViewModel(TreadMarksConstants.HaulerAccount);
            ProcessorApplicationsWorkflow = new ApplicationsWorkflowViewModel(TreadMarksConstants.ProcessorAccount);
            RPMApplicationsWorkflow = new ApplicationsWorkflowViewModel(TreadMarksConstants.RPMAccount);
        }
        /// <summary>
        /// Invitation Id or User Id
        /// </summary>
        public long Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<RoleSelectionViewModel> AvailableRoles { get; set; }
        public List<RoleSelectionViewModel> AssignedRoles { get; set; }
        
        public ClaimsWorkflowViewModel StewardClaimsWorkflow { get; set; }
        public ClaimsWorkflowViewModel CollectorClaimsWorkflow { get; set; }
        public ClaimsWorkflowViewModel HaulerClaimsWorkflow { get; set; }
        public ClaimsWorkflowViewModel ProcessorClaimsWorkflow { get; set; }
        public ClaimsWorkflowViewModel RPMClaimsWorkflow { get; set; }
        public ApplicationsWorkflowViewModel StewardApplicationsWorkflow { get; set; }

        public ApplicationsWorkflowViewModel CollectorApplicationsWorkflow { get; set; }
        public ApplicationsWorkflowViewModel HaulerApplicationsWorkflow { get; set; }
        public ApplicationsWorkflowViewModel ProcessorApplicationsWorkflow { get; set; }
        public ApplicationsWorkflowViewModel RPMApplicationsWorkflow { get; set; }
        public List<string> WorkflowList { get; set; }
        public List<string> StewardWorkflowList { get; set; }
    }
}
