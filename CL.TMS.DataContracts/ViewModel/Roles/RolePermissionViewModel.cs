﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class RolePermissionViewModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Menu { get; set; }
        public string Screen { get; set; }
        public string Panel { get; set; }

        public int ResourceLevel { get; set; }
        public int DisplayOrder { get; set; }

        public bool IsNoAccess { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsEditSave { get; set; }
        public bool IsCustom { get; set; }

        public bool IsScreen
        {
            get
            {
                return ResourceLevel == 1;
            }
        }
        public bool IsPanel
        {
            get
            {
                return ResourceLevel == 2;
            }
        }

        public string ResourceMenu
        {
            get;
            set;
        }

        public bool HideRow
        {
            get
            {
                return ResourceLevel != 0;
            }
        }

        //ApplicableLevel
        public string ApplicableLevel { get; set; }
        public bool ShowNoAccessRadio
        {
            get
            {
                return ApplicableLevel.IndexOf("1") == 0;
            }
        }
        public bool ShowReadOnlyRadio
        {
            get
            {
                var middleString = ApplicableLevel.Substring(1, 1);
                return middleString.IndexOf("1") == 0;
            }
        }
        public bool ShowEditSaveRadio
        {
            get
            {
                return ApplicableLevel.LastIndexOf("1") == (ApplicableLevel.Length - 1);
            }
        }
        public bool ShowCustomRadio
        {
            get
            {
                return ResourceLevel == 1 || ResourceLevel == 0;
            }
        }
    }
}
