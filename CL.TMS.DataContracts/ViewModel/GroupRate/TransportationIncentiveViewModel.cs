﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    
    public class TransportationIncentiveViewModel
    {
        public TransportationIncentiveViewModel() {
            TransportationPremium = new TransportationPremiumViewModel();
            DOTPremium = new DOTPremiumRateViewModel();
            ProcessorTI = new ProcessorTIViewModel();
        }
        public int category { get; set; }
        public int RateTransactionId { get; set; }
        public string InternalNote { get; set; }
        public DateTime EffectiveDate { get; set; }

        //using for update previous rates
        public DateTime previousEffectiveDate { get; set; }
        public int PickupRateGroupId { get; set; }
        public int DeliveryRateGroupId { get; set; }
        // for built UI table 
        public int pickupGroupNumber { get; set; }
        public int deliveryGroupNumber { get; set; }

        public TransportationPremiumViewModel TransportationPremium { get; set; }
        public DOTPremiumRateViewModel DOTPremium { get; set; }
        public ProcessorTIViewModel ProcessorTI { get; set; }
        public TIRateGroupList TIRateGroupList { get; set; }
    }

    public class TIRateGroupList
    {
        public TIRateGroupList()
        {
            pickupRateGroups = new List<ItemModel<string>>();
            deliveryRateGroups = new List<ItemModel<string>>();
        }
        public List<ItemModel<string>> pickupRateGroups { get; set; }
        public List<ItemModel<string>> deliveryRateGroups { get; set; }
    }
}
