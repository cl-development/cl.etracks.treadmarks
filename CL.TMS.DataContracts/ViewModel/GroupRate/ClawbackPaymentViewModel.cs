﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class ClawbackPaymentViewModel
    {
        public decimal TotalOnRoadPreminum { get; set; }
        public decimal TotalOffRoadPremium { get; set; }
        public decimal TotalPremium { get; set; }
        public List<ClawbackPaymentItem> RowDatas { get; set; }
    }

    public class ClawbackPaymentItem
    {
        public int DeliveryGroupId { get; set; }
        public string DeliveryGroupName { get; set; }
        public decimal OnRoadRate { get; set; }
        public decimal OffRoadRate { get; set; }
        public decimal OnRoadWeight { get; set; }
        public decimal OffRoadWeight { get; set; }
        public decimal OnRoadPremium { get; set; }
        public decimal OffRoadPremium { get; set; }
    }
}
