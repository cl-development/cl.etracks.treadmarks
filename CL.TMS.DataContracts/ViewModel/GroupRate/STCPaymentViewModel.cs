﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class STCPremiumViewModel
    {
        public decimal TotalOnRoadPreminum { get; set; }
        public decimal TotalOffRoadPremium { get; set; }
        public decimal TotalPremium { get; set; }

        public List<STCPaymentItemViewModel> RowDatas { get; set; }
        public int RowCount
        {
            get { return RowDatas.Count; }
        }
    }

    public class STCPaymentItemViewModel
    {
        public long STCNumber { get; set; }
        public string EventNumber { get; set; }
        
        public decimal OnRoadRate { get; set; }
        public decimal OffRoadRate { get; set; }

        public decimal OnRoadWeight { get; set; }
        public decimal OffRoadWeight { get; set; }

        public decimal OnRoadPremium { get; set; }
        public decimal OffRoadPremium { get; set; }

    }
}
