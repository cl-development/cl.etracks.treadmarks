﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class RateGroupViewModel
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string RateGroupName { get; set; }
        public GroupViewModel SourceGroup { get; set; }
        public GroupViewModel DestinationGroup { get; set; }
        public List<ItemModel<string>> VendorsMovedFromSourceToDestination { get; set; }
        public List<ItemModel<string>> VendorsMovedFromDestinationToSource { get; set; }

        public string logContent { get; set; }

    }
}
