﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class GroupViewModel
    {
        public GroupViewModel()
        {
            VendorIds = new List<int>();
        }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<int> VendorIds { get; set; }
    }
}
