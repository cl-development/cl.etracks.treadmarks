﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class QRCodeExcelModel
    {
        public int UID { get; set; }
        public string QRCodeNumber { get; set; }
        public bool Activated { get; set; }
    }
}