﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class IPadFleetExcelModel
    {
        public string IPadNumber { get; set; }
        public string AssignedTo { get; set; }
        public bool Activated { get; set; }
    }
}