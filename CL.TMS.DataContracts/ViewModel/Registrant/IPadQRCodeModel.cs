﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class IPadQRCodeModel
    {
        public IPadQRCodeModel()
        {
            this.AssignedIPads = new Grid();
            this.UnAssignedIPads = new Grid();
            this.QRCodes = new Grid();
            this.Activities = new Grid();
        }

        public Grid AssignedIPads { get; set; }
        public Grid UnAssignedIPads { get; set; }
        public Grid QRCodes { get; set; }
        public Grid Activities { get; set; }

        public static List<Grid.Row> AssignedIPadDataSample()
        {

            var list = new List<Grid.Row>();
            list.AddRange(new Grid.Row[] { new Grid.Row() { Value = "OTSIPAD120" }, new Grid.Row() { Value = "OTSIPAD119" }, new Grid.Row() { Value = "OTSIPAD109" }, new Grid.Row() { Value = "OTSIPAD103" }, new Grid.Row() { Value = "OTSIPAD120" }, new Grid.Row() { Value = "OTSIPAD119" }, new Grid.Row() { Value = "OTSIPAD109" }, new Grid.Row() { Value = "OTSIPAD103" } });
            
            return list;
        }

        public class Grid
        {
            public IQueryable<Row> Rows { get; set; }
            public int TotalRowsInCollection { get; set; }

            public class Row
            {
                public string Value { get; set; }
            }
        }
    }
}