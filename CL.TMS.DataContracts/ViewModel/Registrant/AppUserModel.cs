﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class AppUserModel
    {
        public int ID { get; set; }
        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
        public int VendorAppUserID { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string EMail { get; set; }
        public int AccessTypeID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string Metadata { get; set; }
        public Nullable<DateTime> LastAccessOn { get; set; }
        public Nullable<DateTime> SyncOn { get; set; }
        public bool Active { get; set; }
        public IReadOnlyList<Address> Addresses { get; set; }
    }
}
