﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.GroupRate;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class HaulerPaymentViewModel
    {
        #region Payment Adjustment
        public HaulerPaymentViewModel()
        {
            HSTBase = 0.13m;
        }
        public decimal NorthernPremium { get; set; }
        public decimal DOTPremium { get; set; }
        public decimal STCPremium { get; set; } //OTSTM2-1215
        public decimal TransportationPremiumBatch //OTSTM2-1214 only for batch calculation
        {
            get { return UsingOldRounding ? Math.Round(NorthernPremium + STCPremium + PaymentAdjustment, 2) : Math.Round(NorthernPremium + STCPremium + PaymentAdjustment, 2, MidpointRounding.AwayFromZero); }
        }
        public decimal PaymentAdjustment { get; set; }

        public decimal SubTotal
        {
            get { return UsingOldRounding ? Math.Round(NorthernPremium + DOTPremium + STCPremium + PaymentAdjustment, 2) : Math.Round(NorthernPremium + DOTPremium + STCPremium + PaymentAdjustment, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal HSTBase { get; set; }
        public decimal HST
        {
            get { return UsingOldRounding ? Math.Round(SubTotal * HSTBase, 2) : Math.Round(SubTotal * HSTBase, 2, MidpointRounding.AwayFromZero); }
        }

        public bool IsTaxExempt { get; set; }

        public decimal PaymentTotal
        {
            get { return SubTotal + HST; }
        }

        public decimal IneligibleInventoryPayment { get; set; }
        public decimal IneligibleInventoryPaymentAdjust { get; set; }

        public decimal GrandTotal
        {
            get { return PaymentTotal + IneligibleInventoryPayment + IneligibleInventoryPaymentAdjust; }
        }

        public DateTime? PaymentDueDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public DateTime? MailedDate { get; set; }
        public string BatchNumber { get; set; }
        public bool? isStaff { get; set; }
        public bool UsingOldRounding { get; set; }
        #endregion

        #region Premium
        public TransportationPremium TransportationPremium { get; set; }

        public STCPremiumViewModel STCPremiumViewModel { get; set; } //OTSTM2-1215
        public DOTPaymentViewModel DOTPaymentViewModel { get; set; }

        public ClawbackPaymentViewModel ClawbackPaymentViewModel { get; set; }
        #endregion

    }
}
