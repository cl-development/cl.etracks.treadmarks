﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class HaulerClaimSummaryModel
    {
        public HaulerClaimSummaryModel()
        {
            SupportingDocument = new SupportingDocumentModelBase();
            InventoryAdjustment = new InventoryAdjustmentViewModel();
            ClaimCommonModel = new ClaimCommonModel();
        }
        #region Common
        public ClaimCommonModel ClaimCommonModel { get; set; }
        #endregion

        #region Inbound Panel
        public decimal TotalEligibleOpeningOnRoad { get; set; }
        public decimal TotalEligibleOpeningOffRoad { get; set; }

        public decimal TotalEligibleOpeningTotal
        {
            get { return TotalEligibleOpeningOnRoad + TotalEligibleOpeningOffRoad; }
        }
        public decimal TCROnRoad { get; set; }
        public decimal TCROffRoad { get; set; }

        public decimal TCRTotal
        {
            get { return TCROnRoad + TCROffRoad; }
        }
        public decimal DOTOnRoad { get; set; }
        public decimal DOTOffRoad { get; set; }

        public decimal DOTTotal
        {
            get { return DOTOnRoad + DOTOffRoad; }
        }
        public decimal STCOnRoad { get; set; }
        public decimal STCOffRoad { get; set; }

        public decimal STCTotal
        {
            get { return STCOnRoad + STCOffRoad; }
        }

        public decimal TotalEligibleOnRoad
        {
            get { return TotalEligibleOpeningOnRoad + TCROnRoad + DOTOnRoad + STCOnRoad + InventoryAdjustment.EligibleAdjustmentInboundOnRoad; }
        }

        public decimal TotalEligibleOffRoad
        {
            get { return TotalEligibleOpeningOffRoad + TCROffRoad + DOTOffRoad + STCOffRoad + InventoryAdjustment.EligibleAdjustmentInboundOffRoad; }
        }

        public decimal TotalEligibleTotal
        {
            get { return TotalEligibleOnRoad + TotalEligibleOffRoad; }
        }

        public decimal TotalIneligibleOpeningOnRoad { get; set; }
        public decimal TotalIneligibleOpeningOffRoad { get; set; }

        public decimal TotalIneligibleOpeningTotal { get { return TotalIneligibleOpeningOnRoad + TotalIneligibleOpeningOffRoad; } }

        public decimal UCROnRoad { get; set; }
        public decimal UCROffRoad { get; set; }
        public decimal UCRTotal { get { return UCROnRoad + UCROffRoad; } }

        public decimal HITOnRoad { get; set; }
        public decimal HITOffRoad { get; set; }
        public decimal HITTotal { get { return HITOnRoad + HITOffRoad; } }


        public decimal TotalIneligibleOnRoad
        {
            get { return TotalIneligibleOpeningOnRoad + UCROnRoad + HITOnRoad + InventoryAdjustment.IneligibleAdjustmentInboundOnRoad; }
        }

        public decimal TotalIneligibleOffRoad
        {
            get { return TotalIneligibleOpeningOffRoad + UCROffRoad + HITOffRoad + InventoryAdjustment.IneligibleAdjustmentInboundOffRoad; }
        }

        public decimal TotalIneligibleTotal
        {
            get { return TotalIneligibleOnRoad + TotalIneligibleOffRoad; }
        }

        public decimal TotalInboundOnRoad
        {
            get { return TotalEligibleOnRoad + TotalIneligibleOnRoad; }
        }

        public decimal TotalInboundOffRoad
        {
            get { return TotalEligibleOffRoad + TotalIneligibleOffRoad; }
        }

        public decimal TotalInboundTotal
        {
            get { return TotalInboundOnRoad + TotalInboundOffRoad; }
        }
        #endregion

        #region Intermediate Values Required for Calculation
        public decimal EligibleOnRoadPercentage
        {
            get
            {
                return (TotalInboundOnRoad > 0) ? TotalEligibleOnRoad / TotalInboundOnRoad : 0;
            }
        }
        public decimal EligibleOffRoadPercentage
        {
            get
            {
                return (TotalInboundOffRoad > 0) ? TotalEligibleOffRoad / TotalInboundOffRoad : 0;
            }
        }
        public decimal TotalEligiblePercentage
        {
            get
            {
                return EligibleOnRoadPercentage + EligibleOffRoadPercentage;
            }
        }
        public decimal IneligibleOnRoadPercentage
        {
            get
            {
                return (TotalInboundOnRoad > 0) ? TotalIneligibleOnRoad / TotalInboundOnRoad : 0;
            }
        }
        public decimal IneligibleOffRoadPercentage
        {
            get
            {
                return (TotalInboundOffRoad > 0) ? TotalIneligibleOffRoad / TotalInboundOffRoad : 0;
            }
        }

        public decimal TotalIneligiblePercentage
        {
            get
            {
                return IneligibleOnRoadPercentage + IneligibleOnRoadPercentage;
            }
        }
        #endregion

        #region Outbound Panel
        public decimal PTROnRoad { get; set; }
        public decimal PTROffRoad { get; set; }

        public decimal PTRTotal
        {
            get { return PTROnRoad + PTROffRoad; }
        }
        public decimal HITOutboundOnRoad { get; set; }
        public decimal HITOutboundOffRoad { get; set; }

        public decimal HITOutboundTotal
        {
            get { return HITOutboundOnRoad + HITOutboundOffRoad; }
        }

        public decimal RTROnRoad { get; set; }
        public decimal RTROffRoad { get; set; }

        public decimal RTRTotal
        {
            get { return RTROnRoad + RTROffRoad; }
        }

        public decimal TotalEligibleOutboundOnRoad
        {
            get
            {
                return Math.Round(TotalOutboundOnRoad * EligibleOnRoadPercentage,4, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalEligibleOutboundOffRoad
        {
            get
            {
                return Math.Round(TotalOutboudnOffRoad * EligibleOffRoadPercentage,4, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalEligibleOutboundTotal
        {
            get { return TotalEligibleOutboundOnRoad + TotalEligibleOutboundOffRoad; }
        }

        public decimal TotalIneligibleOutboundOnRoad
        {
            get
            {
                if (TotalInboundOnRoad == 0)
                    return Math.Round(TotalOutboundOnRoad, 4, MidpointRounding.AwayFromZero);
                else
                    return Math.Round(TotalOutboundOnRoad * IneligibleOnRoadPercentage,4, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalIneligibleOutboudnOffRoad
        {
            get
            {
                if (TotalInboundOffRoad == 0)
                    return Math.Round(TotalOutboudnOffRoad, 4, MidpointRounding.AwayFromZero);
                else
                    return Math.Round(TotalOutboudnOffRoad * IneligibleOffRoadPercentage,4, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalIneligibleOutboundTotal
        {
            get { return TotalIneligibleOutboundOnRoad + TotalIneligibleOutboudnOffRoad; }
        }

        public decimal TotalOutboundOnRoad
        {
            get { return PTROnRoad + HITOutboundOnRoad + RTROnRoad + InventoryAdjustment.EligibleAdjustmentOutboundOnRoad + InventoryAdjustment.IneligibleAdjustmentOutboundOnRoad; }
        }

        public decimal TotalOutboudnOffRoad
        {
            get { return PTROffRoad + HITOutboundOffRoad + RTROffRoad + InventoryAdjustment.EligibleAdjustmentOutboundOffRoad + InventoryAdjustment.IneligibleAdjustmentOutboundOffRoad; }
        }

        public decimal TotalOutboundTotal
        {
            get { return TotalOutboundOnRoad + TotalOutboudnOffRoad; }
        }
        #endregion

        #region InventoryAdjustment

        public decimal EligibleClosingOnRoad
        {
            get { return TotalEligibleOnRoad - TotalEligibleOutboundOnRoad + InventoryAdjustment.EligibleAdjustmentOverallOnRoad; }
        }

        public decimal EligibleClosingOffRoad
        {
            get { return TotalEligibleOffRoad - TotalEligibleOutboundOffRoad + InventoryAdjustment.EligibleAdjustmentOverallOffRoad; }
        }

        public decimal EligibleClosingTotal
        {
            get { return EligibleClosingOnRoad + EligibleClosingOffRoad; }
        }

        public decimal IneligibleClosingOnRoad
        {
            get { return TotalIneligibleOnRoad - TotalIneligibleOutboundOnRoad + InventoryAdjustment.IneligibleAdjustmentOverallOnRoad; }
        }

        public decimal IneligibleClosingOffRoad
        {
            get { return TotalIneligibleOffRoad - TotalIneligibleOutboudnOffRoad + InventoryAdjustment.IneligibleAdjustmentOverallOffRoad; }
        }

        public decimal IneligibleClosingTotal
        {
            get { return IneligibleClosingOnRoad + IneligibleClosingOffRoad; }
        }

        //OTSTM2-388
        public decimal TotalClosingOnRoad
        {
            get { return EligibleClosingOnRoad + IneligibleClosingOnRoad; }
        }
        //OTSTM2-388
        public decimal TotalClosingOffRoad
        {
            get { return EligibleClosingOffRoad + IneligibleClosingOffRoad; }
        }
        //OTSTM2-388
        public decimal TotalClosingTotal
        {
            get { return EligibleClosingTotal + IneligibleClosingTotal; }
        }

        public InventoryAdjustmentViewModel InventoryAdjustment { get; set; }
        #endregion

        #region Support Documents
        public SupportingDocumentModelBase SupportingDocument { get; set; }

        #endregion

        #region Staff Specific
        public ClaimStatusViewModel ClaimStatus { get; set; }
        public ClaimWorkflowViewModel ClaimWorkflow { get; set; }
        #endregion
    }
}
