﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class BusinessRegistrationModel : RegistrationModelBase
    {
        public BusinessRegistrationModel()
        {
            this.BusinessAddress = new BusinessAddressModel();
            this.MailingAddress = new MailingAddressModel();
        }
        public string BusinessName { get; set; }
        public string OperatingName { get; set; }

        public BusinessAddressModel BusinessAddress { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Email { get; set; }
        public bool MailingAddressSameAsBusiness { get; set; }
        public MailingAddressModel MailingAddress { get; set; }


    }
}
