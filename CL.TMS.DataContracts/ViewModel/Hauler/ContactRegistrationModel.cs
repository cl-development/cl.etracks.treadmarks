﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class ContactRegistrationModel : RegistrationModelBase
    {
        public ContactRegistrationModel()
        {
            this.ContactAddress = new ContactAddressModel();
            this.ContactInformation = new ContactModel();
        }
        public ContactModel ContactInformation { get; set; }
        public bool ContactAddressSameAsBusinessAddress { get; set; }
        public ContactAddressModel ContactAddress { get; set; }
        public bool isEmptyContact { get; set; }
    }
}
