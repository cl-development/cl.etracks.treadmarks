﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public partial class TransactionAdjustment
    {
        [NotMapped]
        public Decimal EstimatedWeight
        {
            get { return (this.EstimatedOffRoadWeight ?? 0.0000M) + (this.EstimatedOnRoadWeight ?? 0.0000M); }
        }
    }
}
