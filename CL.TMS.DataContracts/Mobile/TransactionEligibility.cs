﻿﻿using System;
﻿using System.ComponentModel.DataAnnotations.Schema;
﻿using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("Transaction_Eligibility")]
    public class TransactionEligibility:BaseDTO<Guid>
    {
        public DateTime? SyncDate { get; set; }
        public bool Value { get; set; }
        public long EligibilityId { get; set; }
        public Guid TransactionId { get; set; }
    }
}
