
using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{

    [Table("Comment")]
    public class Comment:BaseDTO<Guid>
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public string Text { get; set; }
        public Guid TransactionId { get; set; }
        public long UserId { get; set; }

        public virtual Transaction Transaction { get; set; }


    }
}
