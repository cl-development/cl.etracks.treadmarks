﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("Transaction")]
    public class Transaction:BaseDTO<Guid>
    {
        public long FriendlyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TransactionTypeId { get; set; }
        public long OutgoingUserId { get; set; }
        public Guid OutgoingGpsLogId { get; set; }
        public long IncomingUserId { get; set; }
        public Guid? IncomingGpsLogId { get; set; }
        public decimal IncomingRegistrationNumber { get; set; }
        public long TransactionStatusTypeId { get; set; }
        public long TransactionSyncStatusTypeId { get; set; }
        public string TrailerNumber { get; set; }
        public long? TrailerLocationId { get; set; }
        public string OutgoingSignatureName { get; set; }
        public string IncomingSignatureName { get; set; }
        public long CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public Guid? IncomingSignaturePhotoId { get; set; }
        public Guid? OutgoingSignaturePhotoId { get; set; }
        public string ResponseComments { get; set; }
        public string PostalCode1 { get; set; }
        public string PostalCode2 { get; set; }
        public string DeviceName { get; set; }
        public decimal OutgoingRegistrationNumber { get; set; }
        public string VersionBuild { get; set; }
        public string DeviceId { get; set; }
       
        public virtual TransactionType TransactionType { get; set; }
        public virtual TransactionStatusType TransactionStatusType { get; set; }
        public virtual ICollection<TransactionTireType> TransactionTireTypes { get; set; }
        public virtual ICollection<TransactionMaterialType> TransactionMaterialTypes { get; set; }        
        public virtual ICollection<Comment> Comments { get; set;}

        public virtual ICollection<Photo> Photos { get; set; }

        public virtual ICollection<ScaleTicket> ScaleTickets { get; set; }
       
        public virtual ICollection<TransactionEligibility> TransactionEligibilities { get; set; }
        public virtual ICollection<Authorization> Authorizations { get; set; }
    
    }
}


