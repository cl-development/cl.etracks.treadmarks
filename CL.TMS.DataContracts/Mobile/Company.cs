using System;
using CL.TMS.Framework.DTO;
using System.ComponentModel.DataAnnotations.Schema;
namespace CL.TMS.DataContracts.Mobile
{
    [Table("Company")]
    public class Company : BaseDTO<int>
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public System.Guid TransactionId { get; set; }
    }
}