
using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("GpsLog")]   
    public class GpsLog:BaseDTO<Guid>
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime? SyncDate { get; set; }
    }
}
