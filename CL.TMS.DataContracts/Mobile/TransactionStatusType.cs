﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("TransactionStatusType")]
    public class TransactionStatusType:BaseDTO<long>
    {
        public string NameKey { get; set; }
        public string FileName { get; set; }
        public DateTime? SyncDate { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}
