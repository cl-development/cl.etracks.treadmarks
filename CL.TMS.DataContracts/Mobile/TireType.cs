
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("TireType")]
    public partial class TireType:BaseDTO<long>
    {
        public string NameKey { get; set; }
        public long SortIndex { get; set; }
        public decimal EstimatedWeight { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public string ShortNameKey { get; set; }

        public virtual ICollection<TransactionTireType> TransactionTireType { get; set; }


    }
}
