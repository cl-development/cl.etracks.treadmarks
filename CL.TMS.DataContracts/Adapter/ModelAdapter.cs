﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.GpStaging;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;

namespace CL.TMS.DataContracts.Adapter
{
    public static class ModelAdapter
    {
        public static Invitation ToInvitation(CreateInvitationViewModel createInvitationViewModel, string userIp, DateTime expireDate, string siteUrl)
        {
            return new Invitation
            {
                FirstName = createInvitationViewModel.FirstName,
                LastName = createInvitationViewModel.LastName,
                UserName = createInvitationViewModel.EmailAddress,
                CreatedBy = TMSContext.TMSPrincipal.Identity.Name,
                UserIP = userIp,
                InviteDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                InviteGUID = Guid.NewGuid(),
                SiteUrl = siteUrl,
                EmailAddress = createInvitationViewModel.EmailAddress,
                Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Invited)
            };
        }

        public static Invitation ToStaffUserInvitation(string firstName, string lastName, string emailAddress, string userIp, DateTime expireDate, string siteUrl)
        {
            return new Invitation
            {
                FirstName = firstName,
                LastName = lastName,
                UserName = emailAddress,
                CreatedBy = TMSContext.TMSPrincipal.Identity.Name,
                UserIP = userIp,
                InviteDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                InviteGUID = Guid.NewGuid(),
                SiteUrl = siteUrl,
                EmailAddress = emailAddress,
                Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Invited),
                UpdateDate = DateTime.Now
            };
        }

        public static Invitation ToInvitation(string firstName, string lastName, string emailAddress, string userIp, DateTime expireDate, string siteUrl, string RedirectUrl, int vendorId, int applicationId)
        {
            return new Invitation
            {
                FirstName = firstName,
                LastName = lastName,
                UserName = emailAddress,
                CreatedBy = TMSContext.TMSPrincipal.Identity.Name,
                UserIP = userIp,
                InviteDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                InviteGUID = Guid.NewGuid(),
                SiteUrl = siteUrl,
                EmailAddress = emailAddress,
                Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Invited),
                RedirectUrl = RedirectUrl,
                VendorId = vendorId,
                ApplicationId = applicationId
            };
        }
        public static Invitation ToCustomerInvitation(string firstName, string lastName, string emailAddress, string userIp, DateTime expireDate, string siteUrl, string RedirectUrl, int customerId, int applicationId)
        {
            return new Invitation
            {
                FirstName = firstName,
                LastName = lastName,
                UserName = emailAddress,
                CreatedBy = TMSContext.TMSPrincipal.Identity.Name,
                UserIP = userIp,
                InviteDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                InviteGUID = Guid.NewGuid(),
                SiteUrl = siteUrl,
                EmailAddress = emailAddress,
                Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Invited),
                RedirectUrl = RedirectUrl,
                CustomerId = customerId,
                ApplicationId = applicationId
            };
        }
        public static GpBatchDto ToGpBatchDto(GpiBatch gpiBatch)
        {
            return new GpBatchDto()
            {
                ID = gpiBatch.ID,
                GpiBatchCount = gpiBatch.GpiBatchCount,
                GpiTypeID = gpiBatch.GpiTypeID,
                LastUpdatedDate = gpiBatch.LastUpdatedDate,
                GpiStatusId = gpiBatch.GpiStatusId,
                Message = gpiBatch.Message
            };
        }
        public static GpBatchEntryDto ToGpBatchEntryDto(GpiBatchEntry gpiBatchEntry)
        {
            return new GpBatchEntryDto()
            {
                ID = gpiBatchEntry.ID,
                GpiTxnNumber = gpiBatchEntry.GpiTxnNumber,
                GpiStatusID = gpiBatchEntry.GpiStatusID,
                Message = gpiBatchEntry.Message,
            };
        }
        public static RR_OTS_RM_CUSTOMERS ToRrOtsRmCustomers(RrOtsRmCustomer customer)
        {
            return new RR_OTS_RM_CUSTOMERS()
            {
                CUSTNMBR = customer.CUSTNMBR,
                ADRSCODE = customer.ADRSCODE,
                INTERID = customer.INTERID,
                CUSTNAME = customer.CUSTNAME,
                CUSTCLAS = customer.CUSTCLAS,
                CNTCPRSN = customer.CNTCPRSN,
                TAXSCHID = customer.TAXSCHID,
                ADDRESS1 = customer.ADDRESS1,
                ADDRESS2 = customer.ADDRESS2,
                ADDRESS3 = customer.ADDRESS3,
                COUNTRY = customer.COUNTRY,
                CITY = customer.CITY,
                STATE = customer.STATE,
                ZIP = customer.ZIP,
                PHNUMBR1 = customer.PHNUMBR1,
                FAX = customer.FAX,
                CURNCYID = customer.CURNCYID,
                PYMTRMID = customer.PYMTRMID,
                USERDEF1 = customer.USERDEF1,
                USERDEF2 = customer.USERDEF2,
                INACTIVE = customer.INACTIVE,
                INTSTATUS = customer.INTSTATUS,
                INTDATE = customer.INTDATE,
                ERRORCODE = customer.ERRORCODE,
            };
        }
        public static RR_OTS_PM_VENDORS ToRrOtsPmVendors(RrOtsPmVendor vendor)
        {
            return new RR_OTS_PM_VENDORS()
            {
                VENDORID = vendor.VENDORID,
                VADDCDPR = vendor.VADDCDPR,
                INTERID = vendor.INTERID,
                VENDNAME = vendor.VENDNAME,
                VNDCLSID = vendor.VNDCLSID,
                VNDCNTCT = vendor.VNDCNTCT,
                ADDRESS1 = vendor.ADDRESS1,
                ADDRESS2 = vendor.ADDRESS2,
                ADDRESS3 = vendor.ADDRESS3,
                CITY = vendor.CITY,
                STATE = vendor.STATE,
                ZIPCODE = vendor.ZIPCODE,
                COUNTRY = vendor.COUNTRY,
                PHNUMBR1 = vendor.PHNUMBR1,
                FAXNUMBR = vendor.FAXNUMBR,
                CURNCYID = vendor.CURNCYID,
                TAXSCHID = vendor.TAXSCHID,
                PYMTRMID = vendor.PYMTRMID,
                USERDEF1 = vendor.USERDEF1,
                USERDEF2 = vendor.USERDEF2,
                VENDSTTS = vendor.VENDSTTS,
                INTSTATUS = vendor.INTSTATUS,
                INTDATE = vendor.INTDATE,
                ERRORCODE = vendor.ERRORCODE,
                BANKNAME = vendor.BANKNAME,
                EmailToAddress = string.IsNullOrWhiteSpace(vendor.EmailToAddress) ? string.Empty : vendor.EmailToAddress,
                EmailCcAddress = string.IsNullOrWhiteSpace(vendor.EmailCcAddress) ? string.Empty : vendor.EmailCcAddress,
                EFTBankAcct = string.IsNullOrWhiteSpace(vendor.EFTBankAcct) ? string.Empty : Token.Decrypt(vendor.EFTBankAcct, vendor.Key),
                EFTBankNumber = string.IsNullOrWhiteSpace(vendor.EFTBankNumber) ? string.Empty : Token.Decrypt(vendor.EFTBankNumber, vendor.Key),
                EFTTransitNumber = string.IsNullOrWhiteSpace(vendor.EFTTransitNumber) ? string.Empty : Token.Decrypt(vendor.EFTTransitNumber, vendor.Key),
            };
        }
        public static RR_OTS_RM_TRANSACTIONS ToRrOtsRmTransactions(RrOtsRmTransaction rrOtsRmTransaction)
        {
            var defaultDate = new DateTime(DateTime.Now.Year, 1, 1);
            return new RR_OTS_RM_TRANSACTIONS()
            {
                RMDTYPAL = rrOtsRmTransaction.RMDTYPAL,
                RMDNUMWK = rrOtsRmTransaction.RMDNUMWK,
                DOCNUMBR = rrOtsRmTransaction.DOCNUMBR,
                DOCDESCR = rrOtsRmTransaction.DOCDESCR,
                DOCDATE = rrOtsRmTransaction.DOCDATE ?? defaultDate,
                BACHNUMB = rrOtsRmTransaction.BACHNUMB,
                CUSTNMBR = rrOtsRmTransaction.CUSTNMBR,
                ADRSCODE = rrOtsRmTransaction.ADRSCODE,
                CSTPONBR = rrOtsRmTransaction.CSTPONBR,
                TAXSCHID = rrOtsRmTransaction.TAXSCHID,
                TAXAMNT = rrOtsRmTransaction.TAXAMNT,
                SLSAMNT = rrOtsRmTransaction.SLSAMNT,
                DOCAMNT = rrOtsRmTransaction.DOCAMNT,
                PYMTRMID = rrOtsRmTransaction.PYMTRMID,
                USERDEF1 = rrOtsRmTransaction.USERDEF1,
                USERDEF2 = rrOtsRmTransaction.USERDEF2,
                INTERID = rrOtsRmTransaction.INTERID,
                INTSTATUS = rrOtsRmTransaction.INTSTATUS,
                INTDATE = rrOtsRmTransaction.INTDATE,
                ERRORCODE = rrOtsRmTransaction.ERRORCODE,
            };
        }
        public static RR_OTS_PM_TRANSACTIONS ToRrOtsPmTransactions(RrOtsPmTransaction rrOtsPmTransaction)
        {
            return new RR_OTS_PM_TRANSACTIONS()
            {
                BACHNUMB = rrOtsPmTransaction.BACHNUMB,
                VCHNUMWK = rrOtsPmTransaction.VCHNUMWK,
                VENDORID = rrOtsPmTransaction.VENDORID,
                DOCNUMBR = rrOtsPmTransaction.DOCNUMBR,
                DOCTYPE = rrOtsPmTransaction.DOCTYPE,
                DOCAMNT = rrOtsPmTransaction.DOCAMNT,
                DOCDATE = rrOtsPmTransaction.DOCDATE,
                PSTGDATE = rrOtsPmTransaction.PSTGDATE,
                VADDCDPR = rrOtsPmTransaction.VADDCDPR,
                PYMTRMID = rrOtsPmTransaction.PYMTRMID,
                TAXSCHID = rrOtsPmTransaction.TAXSCHID,
                DUEDATE = rrOtsPmTransaction.DUEDATE,
                PRCHAMNT = rrOtsPmTransaction.PRCHAMNT,
                TAXAMNT = rrOtsPmTransaction.TAXAMNT,
                PORDNMBR = rrOtsPmTransaction.PORDNMBR,
                USERDEF1 = rrOtsPmTransaction.USERDEF1,
                USERDEF2 = rrOtsPmTransaction.USERDEF2,
                INTERID = rrOtsPmTransaction.INTERID,
                INTSTATUS = rrOtsPmTransaction.INTSTATUS,
                INTDATE = rrOtsPmTransaction.INTDATE,
                ERRORCODE = rrOtsPmTransaction.ERRORCODE,
                //DEX_ROW_ID = rrOtsPmTransaction,                
            };
        }
        public static RR_OTS_RM_TRANS_DISTRIBUTIONS ToRrOtsRmTransDistributions(RrOtsRmTransDistribution rrOtsRmTransDistributions)
        {
            return new RR_OTS_RM_TRANS_DISTRIBUTIONS()
            {
                RMDTYPAL = rrOtsRmTransDistributions.RMDTYPAL,
                DOCNUMBR = rrOtsRmTransDistributions.DOCNUMBR,
                CUSTNMBR = rrOtsRmTransDistributions.CUSTNMBR,
                SEQNUMBR = rrOtsRmTransDistributions.SEQNUMBR,
                DISTTYPE = rrOtsRmTransDistributions.DISTTYPE,
                DistRef = rrOtsRmTransDistributions.DistRef,
                ACTNUMST = rrOtsRmTransDistributions.ACTNUMST,
                DEBITAMT = rrOtsRmTransDistributions.DEBITAMT,
                CRDTAMNT = rrOtsRmTransDistributions.CRDTAMNT,
                USERDEF1 = rrOtsRmTransDistributions.USERDEF1,
                USERDEF2 = rrOtsRmTransDistributions.USERDEF2,
                INTERID = rrOtsRmTransDistributions.INTERID,
                INTSTATUS = rrOtsRmTransDistributions.INTSTATUS,
                INTDATE = rrOtsRmTransDistributions.INTDATE,
                ERRORCODE = rrOtsRmTransDistributions.ERRORCODE,
            };
        }
        public static RR_OTS_PM_TRANS_DISTRIBUTIONS ToRrOtsPmTransDistributions(RrOtsPmTransDistribution rrOtsPmTransDistributions)
        {
            return new RR_OTS_PM_TRANS_DISTRIBUTIONS()
            {
                VCHNUMWK = rrOtsPmTransDistributions.VCHNUMWK,
                DOCTYPE = rrOtsPmTransDistributions.DOCTYPE,
                VENDORID = rrOtsPmTransDistributions.VENDORID,
                DSTSQNUM = rrOtsPmTransDistributions.DSTSQNUM,
                DISTTYPE = rrOtsPmTransDistributions.DISTTYPE,
                DistRef = rrOtsPmTransDistributions.DistRef,
                ACTNUMST = rrOtsPmTransDistributions.ACTNUMST,
                DEBITAMT = rrOtsPmTransDistributions.DEBITAMT,
                CRDTAMNT = rrOtsPmTransDistributions.CRDTAMNT,
                USERDEF1 = rrOtsPmTransDistributions.USERDEF1,
                USERDEF2 = rrOtsPmTransDistributions.USERDEF2,
                INTERID = rrOtsPmTransDistributions.INTERID,
                INTSTATUS = rrOtsPmTransDistributions.INTSTATUS,
                INTDATE = rrOtsPmTransDistributions.INTDATE,
                ERRORCODE = rrOtsPmTransDistributions.ERRORCODE,
            };
        }
        public static RR_OTS_RM_CASH_RECEIPT ToRrOtsRmCashReceipt(RrOtsRmCashReceipt cashReceipt)
        {
            return new RR_OTS_RM_CASH_RECEIPT()
            {
                BACHNUMB = cashReceipt.BACHNUMB,
                DOCNUMBR = cashReceipt.DOCNUMBR,
                CUSTNMBR = cashReceipt.CUSTNMBR,
                RMDTYPAL = cashReceipt.RMDTYPAL,
                DOCDATE = cashReceipt.DOCDATE.Value,
                ORTRXAMT = cashReceipt.ORTRXAMT,
                CSHRCTYP = (Int16)cashReceipt.CSHRCTYP,
                CHEKNMBR = cashReceipt.CHEKNMBR,
                CHEKBKID = "",
                //Not required for successor
                /*cashReceipt.CHEKBKID,*/
                CRCARDID = cashReceipt.CRCARDID,
                CURNCYID = cashReceipt.CURNCYID,
                TRXDSCRN = cashReceipt.TRXDSCRN,
                GLPOSTDT = cashReceipt.GLPOSTDT.Value,
                USERDEF1 = cashReceipt.USERDEF1,
                USERDEF2 = cashReceipt.USERDEF2,
                INTERID = cashReceipt.INTERID,
                INTSTATUS = cashReceipt.INTSTATUS,
                INTDATE = cashReceipt.INTDATE,
                ERRORCODE = cashReceipt.ERRORCODE,
            };

        }
        public static RR_OTS_RM_APPLY ToRrOtsRmApply(RrOtsRmApply apply)
        {
            return new RR_OTS_RM_APPLY()
            {
                APTODCNM = apply.APTODCNM,
                APFRDCNM = apply.APTODCNM,
                APPTOAMT = apply.APPTOAMT,
                APFRDCTY = apply.APFRDCTY,
                APTODCTY = apply.APTODCTY,
                APPLYDATE = apply.APPLYDATE,
                GLPOSTDT = apply.GLPOSTDT,
                USERDEF1 = apply.USERDEF1,
                USERDEF2 = apply.USERDEF2,
                INTERID = apply.INTERID,
                INTSTATUS = apply.INTSTATUS,
                INTDATE = apply.INTDATE,
                ERRORCODE = apply.ERRORCODE,
                //DEX_ROW_ID = apply,                
            };
        }
        public static RegistrantWeeklyReport ToRegistrantWeeklyReport(SpRptRegistrantWeeklyReport registrant)
        {
            return new RegistrantWeeklyReport()
            {
                BusinessName = registrant.BusinessName,
                BusinessOperatingName = registrant.OperatingName,
                DisplayName = !string.IsNullOrEmpty(registrant.BusinessName) ? registrant.BusinessName : registrant.OperatingName,
                RegistrantTypeDesc = GetVendorTypeName(registrant.RegistrantType),
                BusinessDescription = registrant.PrimaryBusinessActivity,
                PrimaryContactName = registrant.Name,
                PrimaryContactEmail = registrant.Email,
                PrimaryContactPosition = registrant.Position,
                PrimaryContactAddress1 = registrant.PrimaryContactAddress1,
                PrimaryContactAddress2 = registrant.PrimaryContactAddress2,
                PrimaryContactAddress3 = registrant.PrimaryContactAddress3,
                PrimaryContactCity = registrant.PrimaryContactCity,
                PrimaryContactProvince = registrant.PrimaryContactProvince,
                PrimaryContactPostalCode = registrant.PrimaryContactPostalCode,
                PrimaryContactCountry = registrant.PrimaryContactCountry,
                PrimaryContactPhone = registrant.PrimaryContactPhone,
                MailingAddress1 = registrant.MailingAddress1,
                MailingAddress2 = registrant.MailingAddress2,
                MailingAddress3 = registrant.MailingAddress3,
                MailingCity = registrant.MailingCity,
                MailingProvince = registrant.MailingProvince,
                MailingPostalCode = registrant.MailingPostalCode,
                MailingCountry = registrant.MailingCountry,
                LocEmail = registrant.BusinessEmail,
                LocAddress1 = registrant.BusinessAddress1,
                LocAddress2 = registrant.BusinessAddress2,
                LocAddress3 = registrant.BusinessAddress3,
                LocCity = registrant.BusinessCity,
                LocProvince = registrant.BusinessProvince,
                LocPostalCode = registrant.BusinessPostalCode,
                LocCountry = registrant.BusinessCountry,
                LocPhone = registrant.BusinessPhone,
                LTT = registrant.PLT > 0 ? "X" : "",
                MTT = registrant.MT > 0 ? "X" : "",
                LST = registrant.AGLS > 0 ? "X" : "",
                SLT = registrant.IND > 0 ? "X" : "",
                SOTR = registrant.SOTR > 0 ? "X" : "",
                MOTR = registrant.MOTR > 0 ? "X" : "",
                LOTR = registrant.LOTR > 0 ? "X" : "",
                GOTR = registrant.GOTR > 0 ? "X" : "",
                RegNumber = registrant.RegistrationNumber,
                DateReceived = registrant.DateReceived,
                CreatedDate = registrant.CreatedDate,
                ActivateDate = registrant.ActivationDate,
                ConfirmationMailDate = registrant.ConfirmationMailedDate,
                RegStatus = registrant.IsActive ? "Active" : "Inactive",
                EffectiveInactiveDate = registrant.IsActive ? null : registrant.ActiveStateChangeDate,
                Reason = registrant.InactivationReason,
                NewRegNumber = registrant.NewRegNumber,
            };
        }
        public static HaulerVolumeReport ToHaulerVolumeReport(SpRptHaulerVolumeReport i)
        {
            var tmp = -1;
            var tmp2 = -1;
            return new HaulerVolumeReport()
            {
                ReportingPeriod = i.ReportingPeriod,
                HaulerRegistrationNumber = int.TryParse(i.HaulerRegistrationNumber, out tmp) ? tmp : -1,
                HaulerStatus = i.HaulerStatus ? "Active" : "Inactive",
                DestinationType = i.DestinationType,
                ProcessorHaulerRegistrationNumber = int.TryParse(i.ProcessorHaulerRegistrationNumber, out tmp2) ? tmp2 : (decimal?)null,
                Status = string.IsNullOrEmpty(i.ProcessorHaulerRegistrationNumber) ? string.Empty : (i.Status ? "Active" : "Inactive"),
                RecordState = i.RecordState,
                UsedTireDestionation = i.UsedTireDestionation,
                TotalActualWeight = i.TotalActualWeight,
                TotalEstWeight = i.TotalEstWeight,
                PltEstWeight = i.PltEstWeight,
                MtEstWeight = i.MtEstWeight,
                AglsEstWeight = i.AglsEstWeight,
                IndEstWeight = i.IndEstWeight,
                SotrEstWeight = i.SotrEstWeight,
                MotrEstWeight = i.MotrEstWeight,
                LotrEstWeight = i.LotrEstWeight,
                GotrEstWeight = i.GotrEstWeight,
                TotalPaymentEarned = i.TotalPaymentEarned,
                PltPaymentEarned = i.PltPaymentEarned,
                MtPaymentEarned = i.MtPaymentEarned,
                AglsPaymentEarned = i.AglsPaymentEarned,
                IndPaymentEarned = i.IndPaymentEarned,
                SotrPaymentEarned = i.SotrPaymentEarned,
                MotrPaymentEarned = i.MotrPaymentEarned,
                LotrPaymenEarned = i.LotrPaymentEarned,
                GotrPaymentEarned = i.GotrPaymentEarned,
                //Hst = i.Hst,
                TotalPaymentEarnedWithHst = i.TotalPaymentEarnedWithHst,
            };
        }


        public static void UpdateContact(ref Contact contact, ContactRegistrationModel contactRegistrationModel)
        {
            contact.FirstName = contactRegistrationModel.ContactInformation.FirstName;
            contact.LastName = contactRegistrationModel.ContactInformation.LastName;
            contact.Position = contactRegistrationModel.ContactInformation.Position;
            contact.PhoneNumber = contactRegistrationModel.ContactInformation.PhoneNumber;
            contact.Ext = contactRegistrationModel.ContactInformation.Ext;
            contact.AlternatePhoneNumber = contactRegistrationModel.ContactInformation.AlternatePhoneNumber;
            contact.Email = contactRegistrationModel.ContactInformation.Email;
            return;
        }
        public static void UpdateAddress(ref Address address, ContactRegistrationModel contactRegistrationModel)
        {
            address.AddressType = (int)AddressTypeEnum.Contact;
            address.Address1 = contactRegistrationModel.ContactAddress.AddressLine1;
            address.Address2 = contactRegistrationModel.ContactAddress.AddressLine2;
            address.City = contactRegistrationModel.ContactAddress.City;
            address.PostalCode = contactRegistrationModel.ContactAddress.Postal;
            address.Province = contactRegistrationModel.ContactAddress.Province;
            address.Country = string.IsNullOrWhiteSpace(contactRegistrationModel.ContactAddress.Country) ? "Canada" : contactRegistrationModel.ContactAddress.Country;
            return;
        }

        #region Private Methods
        //TODO refactor this into a helper class
        private static string GetVendorTypeName(int Id)
        {
            switch (Id)
            {
                case 1:
                    return "Steward";
                case 2:
                    return "Collector";
                case 3:
                    return "Hauler";
                case 4:
                    return "Processor";
                case 5:
                    return "RPM";
                default:
                    return string.Empty;
            }
        }
        #endregion

    }
}
