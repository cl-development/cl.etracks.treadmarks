﻿using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.Validation
{
    public class RequestAccountValidator: AbstractValidator<RequestAccountViewModel>
    {
        public RequestAccountValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
                default:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
            }
        }

        private void BuildOTSValidationRules()
        {
            RuleFor(c => c.PrimaryEmail)
                .Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$")
                .WithMessage(MessageResource.NotValidEmail).NotNull().WithMessage(MessageResource.EmailIsRequired);
            RuleFor(c => c.RegistrationNumber)
                .Matches(@"^([0-9]{7})$").WithMessage("Registration number must be 7 digit")
                .NotNull().WithMessage("Registration number is required");
        }
    }
}
