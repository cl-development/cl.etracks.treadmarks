﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Resources;
using FluentValidation;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.DataContracts.Validation
{
    public class UserPasswordResetModelValidator : AbstractValidator<UserPasswordResetModel>
    {
        public UserPasswordResetModelValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
                default:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
            }
        }

        private void BuildOTSValidationRules()
        {
            RuleFor(c => c.UserName)
              .Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$")
              .WithMessage(MessageResource.UserNameError)
              .NotNull().WithMessage(MessageResource.UserNameIsNotNull);
            RuleFor(c => c.Password)
                .Matches(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^+=-]).*$").WithMessage(MessageResource.PasswordError)
                .NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.ConfirmPassword).Equal(c => c.Password).WithMessage(MessageResource.PasswordNotMatched);
        }
    }
}
