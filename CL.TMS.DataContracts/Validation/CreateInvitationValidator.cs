﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using FluentValidation;

namespace CL.TMS.DataContracts.Validation
{
    public class CreateInvitationValidator : AbstractValidator<CreateInvitationViewModel>
    {
        public CreateInvitationValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                {
                    BuildOTSValidationRules();
                    break;
                }
                default:
                {
                    BuildOTSValidationRules();
                    break;
                }
            }
        }

        private void BuildOTSValidationRules()
        {
            RuleFor(c => c.EmailAddress)
                .Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$")
                .WithMessage(MessageResource.NotValidEmail).NotNull().WithMessage(MessageResource.EmailIsRequired);
            RuleFor(c => c.FirstName).NotNull().WithMessage(MessageResource.FirstNameIsRequired);
            RuleFor(c => c.LastName).NotNull().WithMessage(MessageResource.LastNameIsRequired);
        }
    }
}
