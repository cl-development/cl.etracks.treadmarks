﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CL.TMS.DataContracts.Communication
{
    /// <summary>
    /// Generic communication contract defining commom returned data by web service response.
    /// </summary>
    public interface IResponseMessage<T>
    {
        IEnumerable<T> ResponseContent { get; }
        Int32 ResponseStatus { get; } //1 OK, 2 Error, 3 Warning
        List<String> ResponseMessages { get;  }
        T SingleEntity { get; }
    }
}
