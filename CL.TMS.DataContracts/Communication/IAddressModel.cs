﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.Communication
{
    public interface IAddressModel
    {
        int ID { get; set; }
        int AddressTypeID { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string City { get; set; }
        string Province { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        Nullable<decimal> GPSCoordinate1 { get; set; }
        Nullable<decimal> GPSCoordinate2 { get; set; }
    }
}
