﻿using System;

namespace CL.TMS.DataContracts.Communication
{
    /// <summary>
    /// Communication contract for IRegistrantMsg
    /// * Defines the applicable actions and returned data collection of a web service call.
    /// </summary>
    public interface IAppUserMsg : IResponseMessage<IAppUserModel>
    {        
        void GetAllUpdatedAppUsers(DateTime syncDate);
    }
}
