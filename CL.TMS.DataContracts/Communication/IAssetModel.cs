﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.Communication
{
    public interface IAssetModel
    {
        int ID { get; set; }
        string AssetTag { get; set; }
        Nullable<int> VendorID { get; set; }
        int AssetTypeUID { get; set; }
        int AssetTypeID { get; set; }
        bool Active { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        Nullable<DateTime> UpdatedOn { get; set; }
        string UpdatedBy { get; set; }
    }
}
