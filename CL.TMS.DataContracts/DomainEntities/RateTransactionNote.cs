﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RateTransactionNote")]
    public class RateTransactionNote : BaseDTO<int>
    {
        public int RateTransactionID { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }
        [ForeignKey("RateTransactionID")]
        public virtual RateTransaction RateTransaction { get; set; }
    }
}
