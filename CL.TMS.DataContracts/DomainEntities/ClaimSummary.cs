using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimSummary")]
    public class ClaimSummary : BaseDTO<int>
    {
        public decimal? EligibleOpeningOnRoad { get; set; }
        public decimal? EligibleOpeningOffRoad { get; set; }
        public decimal? IneligibleOpeningOnRoad { get; set; }
        public decimal? IneligibleOpeningOffRoad { get; set; }

        public decimal? EligibleClosingOnRoad { get; set; }
        public decimal? EligibleClosingOffRoad { get; set; }
        public decimal? IneligibleClosingOnRoad { get; set; }
        public decimal? IneligibleClosingOffRoad { get; set; }

        public decimal? TotalOpening { get; set; }
        public decimal? TotalClosing { get; set; }

        public decimal? EligibleOpeningOnRoadEstimated { get; set; }
        public decimal? EligibleOpeningOffRoadEstimated { get; set; }
        public decimal? IneligibleOpeningOnRoadEstimated { get; set; }
        public decimal? IneligibleOpeningOffRoadEstimated { get; set; }

        public decimal? EligibleClosingOnRoadEstimated { get; set; }
        public decimal? EligibleClosingOffRoadEstimated { get; set; }
        public decimal? IneligibleClosingOnRoadEstimated { get; set; }
        public decimal? IneligibleClosingOffRoadEstimated { get; set; }

        public decimal? TotalOpeningEstimated { get; set; }
        public decimal? TotalClosingEstimated { get; set; }

        public DateTime UpdatedDate { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
