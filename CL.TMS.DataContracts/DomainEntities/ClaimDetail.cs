using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimDetail")]
    public class ClaimDetail : BaseDTO<int>
    {
        [ForeignKey("Claim")]
        public int ClaimId { get; set; }
        [ForeignKey("Transaction")]
        public int TransactionId { get; set; }
        public bool Direction { get; set; }
        public virtual Transaction Transaction { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
