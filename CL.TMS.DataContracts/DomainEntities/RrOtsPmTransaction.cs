﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsPmTransaction")]
    public class RrOtsPmTransaction
    {
        public string BACHNUMB { get; set; }
        [Key, Column(Order = 0)]
        public string VCHNUMWK { get; set; }
        public string VENDORID { get; set; }
        public string DOCNUMBR { get; set; }
        [Key, Column(Order = 1)]
        public int DOCTYPE { get; set; }
        public decimal DOCAMNT { get; set; }
        public DateTime DOCDATE { get; set; }
        public DateTime? PSTGDATE { get; set; }
        public string VADDCDPR { get; set; }
        public string PYMTRMID { get; set; }
        public string TAXSCHID { get; set; }
        public DateTime? DUEDATE { get; set; }
        public decimal PRCHAMNT { get; set; }
        public decimal? TAXAMNT { get; set; }
        public string PORDNMBR { get; set; }
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 2)]
        public string INTERID { get; set; }
        public int? INTSTATUS { get; set; }
        public DateTime? INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DEX_ROW_ID { get; set; }
        [Display(Name = "fiBatchEntryId")] //OTSTM2-1124
        public int GpiBatchEntryId { get; set; }
        [NotMapped]
        public int ClaimId { get; set; }

    }
}
