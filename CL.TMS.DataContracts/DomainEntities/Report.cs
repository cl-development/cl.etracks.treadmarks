﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Report")]
    public class Report
    {
        public int ID { get; set; }
        public string ReportName { get; set; }
        public int ReportCategoryID { get; set; }
        //OTSTM2-615 Tool Tip text for each report.
        public string Description { get; set; } 
        //public virtual ReportCategory ReportCategory { get; set; }
    }
}
