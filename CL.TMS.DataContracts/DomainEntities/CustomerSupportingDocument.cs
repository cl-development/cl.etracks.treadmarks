﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("CustomerSupportingDocument")]
    public class CustomerSupportingDocument : BaseDTO<int>
    {
        public int CustomerID { get; set; }
        public int TypeID { get; set; }
        public string Option { get; set; }

        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

    }
}
