﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("UserRole")]
    public class UserRole : BaseDTO<int>
    {
        public int UserID { get; set; }
        public long RoleID { get; set; }

    }
}
