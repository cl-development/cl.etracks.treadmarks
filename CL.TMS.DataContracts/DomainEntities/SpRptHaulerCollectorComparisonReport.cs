﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptHaulerCollectorComparisonReport
    {


        public string RegistrationNumberH { get; set; }
        public string ClaimPeriodStartH { get; set; }
        public string StatusH { get; set; }
        public long FormNumberH { get; set; }
        public string TransactionStatusH { get; set; }
        public string CollectorNumberH { get; set; }
        public Nullable<int> pltH { get; set; }
        public Nullable<int> mtH { get; set; }
        public Nullable<int> aglsH { get; set; }
        public Nullable<int> indH { get; set; }
        public Nullable<int> sotrH { get; set; }
        public Nullable<int> motrH { get; set; }
        public Nullable<int> lotrH { get; set; }
        public Nullable<int> gotrH { get; set; }
        public Nullable<int> TotalH { get; set; }
        public string RegistrationNumberC { get; set; }
        public string ClaimPeriodStartC { get; set; }
        public string StatusC { get; set; }
        public Nullable<long> FormNumberC { get; set; }
        public string TransactionStatusC { get; set; }
        public Nullable<int> pltC { get; set; }
        public Nullable<int> mtC { get; set; }
        public Nullable<int> aglsC { get; set; }
        public Nullable<int> indC { get; set; }
        public Nullable<int> sotrC { get; set; }
        public Nullable<int> motrC { get; set; }
        public Nullable<int> lotrC { get; set; }
        public Nullable<int> gotrC { get; set; }
        public Nullable<int> TotalC { get; set; }
        public Nullable<int> Difference { get; set; }
    }
}
