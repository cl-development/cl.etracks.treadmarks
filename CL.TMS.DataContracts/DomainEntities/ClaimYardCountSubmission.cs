﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimYardCountSubmission")]
    public class ClaimYardCountSubmission:BaseDTO<int>
    {
        public int ClaimId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public decimal Weight { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
