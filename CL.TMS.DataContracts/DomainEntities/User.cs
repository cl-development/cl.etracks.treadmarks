﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("User")]
    public class User : BaseDTO<long>
    {
        public User()
        {
            UserPasswordArchive = new List<UserPasswordArchive>();
            Roles = new List<Role>();

            UserClaimsWorkflows = new List<UserClaimsWorkflow>(); //new added
            UserApplicationsWorkflows = new List<UserApplicationsWorkflow>(); //new added
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<DateTime> LastLoginDate { get; set; }
        public string Email { get; set; }
        public bool IsLocked { get; set; }
        public bool Inactive { get; set; }
        public int InvalidLoginAttempts { get; set; }
        public bool PasswordReset { get; set; }
        public Nullable<DateTime> PasswordExpirationDate { get; set; }
        public Nullable<DateTime> LastPasswordResetDate { get; set; }
        public bool AgreedToTermsOfUse { get; set; }
        public string PasswordRestToken { get; set; }
        public string PasswordRestSecretKey { get; set; }
        public DateTime CreateDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<bool> RedirectUserAfterLogin { get; set; }
        public string RedirectUserAfterLoginUrl { get; set; }

        public virtual ICollection<UserPasswordArchive> UserPasswordArchive { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<UserClaimsWorkflow> UserClaimsWorkflows { get; set; }  //new added
        public virtual ICollection<UserApplicationsWorkflow> UserApplicationsWorkflows { get; set; }  //new added

    }
}
