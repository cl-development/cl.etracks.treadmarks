﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimProcess")]
    public class ClaimProcess : BaseDTO<int>
    {
        /// long? :User ID, reviewed by
        /// DateTime?:reviewed on Date
        public long? RepresentativeReview { get; set; }//Reviewed by Representative, user ID
        public DateTime? RepresentativeReviewDate { get; set; }//Representative Reviewed on Date
        public long? LeadReview { get; set; }
        public DateTime? LeadReviewDate { get; set; }
        public long? SupervisorReview { get; set; }
        public DateTime? SupervisorReviewDate { get; set; }
        public long? Approver1 { get; set; }
        public DateTime? Approver1Date { get; set; }
        public long? Approver2 { get; set; }
        public DateTime? Approver2Date { get; set; }

        public virtual Claim Claim { get; set; }
    }
}
