﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;


namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Customer")]
    public class Customer : InvalidFormFieldModel
    {
        public Customer()
        {
            CustomerAddresses = new List<Address>();
            BusinessActivities = new List<BusinessActivity>();
            CustomerActiveHistories = new List<CustomerActiveHistory>();
            Items = new List<Item>();
            CustomerSupportingDocuments = new List<CustomerSupportingDocument>();
            CustomerSettingHistoryList = new List<CustomerSettingHistory>(); //OTSTM2-21
        }

        public string FormCompletedByFirstName { get; set; }
        public string FormCompletedByLastName { get; set; }
        public string FormCompletedByPhone { get; set; }
        public string SigningAuthorityFirstName { get; set; }
        public string SigningAuthorityLastName { get; set; }
        public string SigningAuthorityPosition { get; set; }
        public string AgreementAcceptedByFullName { get; set; }
        public Nullable<bool> AgreementAcceptedCheck { get; set; }

        public int CustomerType { get; set; }
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }
        public string FranchiseName { get; set; }
        public string OperatingName { get; set; }
        public string BusinessNumber { get; set; }
        public string TerritoryCode { get; set; }
        public string GlobalDimension1Code { get; set; }
        public string GlobalDimension2Code { get; set; }
        public Nullable<int> CreditLimit { get; set; }
        public string TelexAnswerBack { get; set; }
        public string TaxRegistrationNo { get; set; }
        public string TaxRegistrationNo2 { get; set; }
        public string HomePage { get; set; }
        public string BusinessGroupCode { get; set; }
        public string CustomerGroupCode { get; set; }
        public string DepartmentCode { get; set; }
        public string ProjectCode { get; set; }
        public string PurchaserCode { get; set; }
        public string SalesCampaignCode { get; set; }
        public string SalesPersonCode { get; set; }
        public string AuthSigComplete { get; set; }
        public Nullable<DateTime> ReceivedDate { get; set; }
        public Nullable<DateTime> GracePeriodStartDate { get; set; }
        public Nullable<DateTime> GracePeriodEndDate { get; set; }
        public string CommercialLiabInsurerName { get; set; }
        public Nullable<DateTime> CommercialLiabInsurerExpDate { get; set; }

        public string WorkerHealthSafetyCertNo { get; set; }
        public string PermitsCertDescription { get; set; }
        public Nullable<bool> AmendedAgreement { get; set; }
        public Nullable<DateTime> AmendedDate { get; set; }
        public bool IsTaxExempt { get; set; }
        public string UpdateToFinancialSoftware { get; set; }
        public string PrimaryBusinessActivity { get; set; }
        public Nullable<bool> MailingAddressSameAsBusiness { get; set; }
        public int BusinessActivityID { get; set; }
        public string GSTNumber { get; set; }
        public string AuthorizedSigComplete { get; set; }
        public string LocationEmailAddress { get; set; }
        public Nullable<DateTime> DateReceived { get; set; }
        public Nullable<DateTime> ActivationDate { get; set; }
        public Nullable<DateTime> ConfirmationMailedDate { get; set; }
        public Nullable<int> PreferredCommunication { get; set; }
        public int RegistrantTypeID { get; set; }
        public int RegistrantSubTypeID { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<DateTime> LastUpdatedDate { get; set; }
        public Nullable<int> CheckoutNum { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool InBatch { get; set; }
        public bool IsActive { get; set; }
        public Nullable<DateTime> ActiveStateChangeDate { get; set; }

        public Nullable<int> ApplicationID { get; set; }
        public Nullable<bool> PayOTSFee { get; set; }

        public string BusinessDesc { get; set; }
        public Nullable<DateTime> MOESwitchDate { get; set; }
        //OTSTM2-1110 comment out
        //public Nullable<bool> Audit { get; set; }
        //public Nullable<DateTime> AuditSwitchDate { get; set; }
        public Nullable<bool> RemitsPerYear { get; set; }//true:12X false:2x

        public Nullable<DateTime> RemitsPerYearSwitchDate { get; set; }

        public Nullable<bool> MOE { get; set; }
        public string WSIBNumber { get; set; }
        public Nullable<bool> HasMoreThanOneEmp { get; set; }
        public string CommercialLiabHSTNumber { get; set; }
        public Nullable<DateTime> BusinessStartDate { get; set; }
        public Nullable<bool> GpUpdate { get; set; }
        public string RegistrantStatus { get; set; }

        public string ContactInfo { get; set; }
        public string KeyWords { get; set; }
        public virtual ICollection<Address> CustomerAddresses { get; set; }

        public virtual ICollection<BusinessActivity> BusinessActivities { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual Application Application { get; set; }

        public virtual ICollection<CustomerActiveHistory> CustomerActiveHistories { get; set; }

        public virtual ICollection<CustomerSupportingDocument> CustomerSupportingDocuments { get; set; }
        public virtual ICollection<CustomerSettingHistory> CustomerSettingHistoryList { get; set; } //OTSTM2-21

        [NotMapped]
        public List<int> SubTypeList { get; set; }

    }
}
