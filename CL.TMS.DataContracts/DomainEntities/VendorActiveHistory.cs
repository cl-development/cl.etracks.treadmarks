﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorActiveHistory")]
    public class VendorActiveHistory : BaseDTO<int>
    {
        public VendorActiveHistory()
        {
            IsValid = true;
        }
        public bool ActiveState { get; set; }
        public DateTime ActiveStateChangeDate { get; set; }
        public string Reason { get; set; }
        public int VendorID { get; set; }
        public string Description { get; set; }
        public string OtherReason { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsValid { get; set; }
        public string CreatedBy { get; set; }

        [NotMapped]
        public bool ChangeToActive { get; set; }
    }
}
