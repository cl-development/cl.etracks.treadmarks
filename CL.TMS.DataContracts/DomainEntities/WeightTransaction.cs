﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("WeightTransaction")]
    public class WeightTransaction : BaseDTO<int>
    {
        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }
        public int Category { get; set; }

        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }

        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }

        public virtual List<WeightTransactionNote> WeightTransactionNotes { get; set; }

        public virtual List<ItemWeight> ItemWeights { get; set;}
    }
}
