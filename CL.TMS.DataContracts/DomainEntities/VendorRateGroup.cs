﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorRateGroup")]
    public class VendorRateGroup
    {
        [Key]
        public int Id { get; set; }
        public int? RateGroupId { get; set; }
        public int GroupId { get; set; }
        public int? VendorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }
        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }
        [ForeignKey("RateGroupId")]
        public virtual RateGroup RateGroup { get; set; }
    }
}
