﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpGetRegistrantSpotlightsPaginate : BaseDTO<int>
    {
        //public int ID { get; set; }
        //public string Number { get; set; }
        //public string LegalName { get; set; }
        //public string OperatingName { get; set; }
        public int vendorID { get; set; }
        public string PlaceHolder1 { get; set; }
        public string PlaceHolder2 { get; set; }
        public string PlaceHolder3 { get; set; }
        public string PlaceHolder4 { get; set; }
        public string PlaceHolder5 { get; set; }
        public Nullable<int> ModuleType { get; set; }
        //public bool Active { get; set; }
        public int TotalCount { get; set; }
        public string Section { get; set; }
        public string Status { get; set; }
        public DateTime? PeriodStartDate { get; set; }
    }
}
