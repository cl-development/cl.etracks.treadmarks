﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Inventory")]
    public class Inventory:BaseDTO<int>
    {
        public int VendorId { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public bool IsEligible { get; set; }
        public int Qty { get; set; }
        public decimal Weight { get; set; }
        public decimal ActualWeight { get; set; }        
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        public bool IsValidForClaim { get; set; }
        public virtual Item Item { get; set; }
    }
}
