﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorStorageSite")]
    public class VendorStorageSite : BaseDTO<int>
    {
        public VendorStorageSite()
        {
            UnitType = 3;
        }
        public int VendorID { get; set; }
        public int AddressID { get; set; }
        public int StorageSiteType { get; set; }
        public Double MaxStorageCapacity { get; set; }
        public string CertificateOfApprovalNumber { get; set; }

        public int UnitType { get; set; }
        [ForeignKey("VendorID")]
        public virtual Vendor Vendor { get; set; }

        [ForeignKey("AddressID")]
        public virtual Address Address { get; set; }
    }
}