﻿using CL.TMS.Framework.DTO;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ApplicationSupportingDocument")]
    public class ApplicationSupportingDocument : BaseDTO<int>
    {
        public int ApplicationID { get; set; }
        public int TypeID { get; set; }
        public string Option { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual Application Application { get; set; }
    }
}
