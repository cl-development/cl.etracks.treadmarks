using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimPayment")]
    public class ClaimPayment : BaseDTO<int>
    {
        public int ClaimID { get; set; }
        public string SourceZone{ get; set; }
        public string DeliveryZone { get; set; }
        public decimal Weight { get; set; }
        public decimal Rate { get; set; }
        public int ItemType { get; set; }
        public int PaymentType { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
