﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;


namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Attachment")]
    public class Attachment : BaseDTO<int>
    {
        public Attachment()
        {
            this.Applications = new HashSet<Application>();
            this.BanksInformation = new HashSet<BankInformation>();
            this.Claims = new HashSet<Claim>();
        }

        public string FileType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Description { get; set; }
        public long Size { get; set; }
        public string UniqueName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public string ValidationMessage { get; set; }

        public bool IsBankingRelated { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<BankInformation> BanksInformation { get; set; }

        public virtual ICollection<Claim> Claims { get; set; }
        public virtual ICollection<TSFClaim> tsfClaims { get; set; }

    }
}
