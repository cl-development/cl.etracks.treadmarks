﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using System.ComponentModel.DataAnnotations;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("BankInformation")]
    public class BankInformation : BaseDTO<int>
    {
        public BankInformation()
        {
            Attachments = new HashSet<Attachment>();
        }

        public string BankName { get; set; }
        public string TransitNumber { get; set; }
        public string BankNumber { get; set; }
        public string AccountNumber { get; set; }
        public string BankAddress { get; set; }

        public bool EmailPaymentTransferNotification { get; set; }
        public bool NotifyToPrimaryContactEmail { get; set; }        
		public string Email { get; set; }
        public string CCEmail { get; set; }
        public string Key { get; set; }
        public string ReviewStatus { get; set; }
        public int VendorId { get; set; }
        [NotMapped]
        public string SupportDocOption { get; set; }
        public DateTime? SubmittedOnDate { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }

        public bool Equals(BankInformation obj)
        {
            return string.Equals((this.BankName ?? string.Empty).Trim(), (obj.BankName ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals((this.TransitNumber ?? string.Empty).Trim(), (obj.TransitNumber ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals((this.BankNumber ?? string.Empty).Trim(), (obj.BankNumber ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals((this.AccountNumber ?? string.Empty).Trim(), (obj.AccountNumber ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals((this.Email ?? string.Empty).Trim(), (obj.Email ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals((this.CCEmail ?? string.Empty).Trim(), (obj.CCEmail ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase) &&
                   string.Equals(this.ReviewStatus, obj.ReviewStatus, StringComparison.OrdinalIgnoreCase);
        }

    }
}
