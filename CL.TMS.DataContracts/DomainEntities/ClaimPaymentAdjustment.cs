using System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimPaymentAdjustment")]
    public class ClaimPaymentAdjustment : BaseDTO<int>
    {
        //public int ID { get; set; }
        public int ClaimAdjustmentDetailsID { get; set; }
        public Nullable<int> SourceZoneID { get; set; }
        public Nullable<int> DeliveryZoneID { get; set; }
        public decimal Weight { get; set; }
        public decimal Rate { get; set; }
        public int ItemType { get; set; }
        public int PaymentType { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public virtual ClaimAdjustmentDetail ClaimAdjustmentDetail { get; set; }
    }
}
