﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsRmCashReceipt")]
    public class RrOtsRmCashReceipt
    {
        public string BACHNUMB { get; set; }
        [Key, Column(Order = 0)]    
        public string DOCNUMBR { get; set; }    
        public string CUSTNMBR { get; set; }
        [Key, Column(Order = 1)]    
        public short RMDTYPAL { get; set; }    
        public DateTime? DOCDATE { get; set; }    
        public decimal ORTRXAMT { get; set; }    
        public int CSHRCTYP { get; set; }    
        public string CHEKNMBR { get; set; }    
        public string CHEKBKID { get; set; }    
        public string CRCARDID { get; set; }    
        public string CURNCYID { get; set; }    
        public string TRXDSCRN { get; set; }    
        public DateTime? GLPOSTDT { get; set; }    
        public string USERDEF1 { get; set; }    
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 2)]    
        public string INTERID { get; set; }    
        public int? INTSTATUS { get; set; }    
        public DateTime? INTDATE { get; set; }    
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int DEX_ROW_ID { get; set; }    
        [Display(Name = "fibatchentry_id")] //OTSTM2-1124
        public long gpibatchentry_id { get; set; }    
    }

}
