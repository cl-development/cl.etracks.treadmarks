﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("AppUser")]
    public class AppUser : BaseDTO<int>
    {
        public int VendorID { get; set; }
        public int VendorAppUserID { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string EMail { get; set; }
        public int AccessTypeID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string Metadata { get; set; }
        public Nullable<DateTime> LastAccessOn { get; set; }
        public Nullable<DateTime> SyncOn { get; set; }
        public bool Active { get; set; }

        [ForeignKey("VendorID")]
        public virtual Vendor Vendor { get; set; }
    }
}
