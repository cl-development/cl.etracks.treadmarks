﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Contact")]

    public class Contact : InvalidFormFieldModel
    {
        public int AddressID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsPrimary { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Ext { get; set; }
        public string FaxNumber { get; set; }
        public string PreferredContactMethod { get; set; }
        public Nullable<bool> ContactAddressSameAsBusiness { get; set; }
        [ForeignKey("AddressID")]
        public virtual Address Address { get; set; }
    }
}


