﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TermsAndConditions")]
    public class TermsAndConditions : BaseDTO<int>
    {     
        public int ApplicationTypeID { get; set; }
        public string Content {get; set; }
        public long? CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? ModifiedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }   
    }
}
