﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("SpecialItemRateList")]
    public class SpecialItemRateList : BaseDTO<int>
    {
        public string EventNumber { get; set; }
        public string Description { get; set; }
        public decimal OnRoadRate { get; set; }
        public decimal OffRoadRate { get; set; }
        public int? TransactionID { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }

        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }

        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }

        [ForeignKey("TransactionID")]
        public virtual Transaction Transaction { get; set;}
    }
}
