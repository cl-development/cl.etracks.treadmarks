﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
     [Table("ApplicationInvitation")]
    public class ApplicationInvitation : BaseDTO<int>
    {                  
        public string TokenID { get; set; }          
        public int ParticipantTypeID { get; set; }
        public string Email { get; set; }
        public int? ApplicationID{ get; set; }
        public bool IsRegistered { get; set; }
        public string ParticipantTypeName { get; set; }
        public DateTime? InvitationDate { get; set; }
        [ForeignKey("ParticipantTypeID")]
        public virtual ParticipantType ParticipantType { get; set; }
         
    }
}
