﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("FIAccount")]
    public class FIAccount : BaseDTO<int>
    {      
        public int AdminFICategoryID { get; set; }
        public string AccountLabel { get; set; }
        public string AccountNumber { get; set; }
        public int DisplayOrder { get; set; }
        public string AccountDescription { get; set; }
        public string MappingNote { get; set; }
        public long?  ModifiedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? distribution_type_id { get; set; } //OTSTM2-1124
        public string Name { get; set; } //OTSTM2-1124
    }
}
