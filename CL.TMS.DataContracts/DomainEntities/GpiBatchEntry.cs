﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpiBatchEntry")]
    public class GpiBatchEntry : BaseDTO<int>
    {
        public int GpiBatchID { get; set; }
        public string GpiTxnNumber { get; set; }
        public string GpiBatchEntryDataKey { get; set; }
        public string GpiStatusID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string Message { get; set; }
        public long IntStatus { get; set; }
        public DateTime? IntDate { get; set; }
        public virtual GpiBatch GpiBatch { get; set; }
        public int? ClaimId { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
