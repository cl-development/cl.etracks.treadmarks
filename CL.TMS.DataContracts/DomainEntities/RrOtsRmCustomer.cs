﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsRmCustomer")]
    public class RrOtsRmCustomer
    {
        [Key, DatabaseGeneratedAttribute(DatabaseGeneratedOption.None), Column(Order = 0)]
        public string CUSTNMBR { get; set; }
        public string CUSTNAME { get; set; }
        public string CUSTCLAS { get; set; }
        public string CNTCPRSN { get; set; }
        [Key, DatabaseGeneratedAttribute(DatabaseGeneratedOption.None), Column(Order = 1)]
        public string ADRSCODE { get; set; }
        public string TAXSCHID { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string COUNTRY { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string PHNUMBR1 { get; set; }
        public string FAX { get; set; }
        public string CURNCYID { get; set; }
        public string PYMTRMID { get; set; }
        [Key, DatabaseGeneratedAttribute(DatabaseGeneratedOption.None), Column(Order = 2)]
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        public int? INACTIVE { get; set; }
        [Key, DatabaseGeneratedAttribute(DatabaseGeneratedOption.None), Column(Order = 3)]
        public string INTERID { get; set; }
        public int? INTSTATUS { get; set; }
        public DateTime? INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DEX_ROW_ID { get; set; }
        [Display(Name = "fibatchentry_id")] //OTSTM2-1124
        public long gpibatchentry_id { get; set; }
    }
}
