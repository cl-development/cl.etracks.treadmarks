﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimPaymentSummary")]
    public class ClaimPaymentSummary : BaseDTO<int>
    {
        public decimal? AmountPaid { get; set; }
        public DateTime CreateDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public DateTime? MailDate { get; set; }
        public string BatchNumber { get; set; }

        public virtual Claim Claim { get; set; }
    }
}
