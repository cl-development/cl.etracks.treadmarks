﻿using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    //OTSTM2-270
    [Table("ClaimInternalAdjustmentNote")]
    public class ClaimInternalAdjustmentNote : BaseDTO<int>
    {
        public Nullable<int> ClaimInternalAdjustmentId { get; set; }
        public Nullable<int> ClaimInternalPaymentAdjustId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User AddedBy { get; set; }
        [ForeignKey("ClaimInternalAdjustmentId")]
        public virtual ClaimInventoryAdjustment ClaimInventoryAdjustment { get; set; }
        [ForeignKey("ClaimInternalPaymentAdjustId")]
        public virtual ClaimInternalPaymentAdjust ClaimInternalPaymentAdjust { get; set; }
    }
}
