﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Email")]
    public class Email : BaseDTO<int>
    {     
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }

        public string HeaderSection { get; set; }

        public string Body { get; set; }

        public string ButtonSection { get; set; }

        public string ButtonLabel { get; set; }

        public bool IsEnabled { get; set; }

        public int SortOrder { get; set; }

        public DateTime CreatedDate { get; set; }

        public long? CreatedByID { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public long? ModifiedByID { get; set; }        
    }
}

