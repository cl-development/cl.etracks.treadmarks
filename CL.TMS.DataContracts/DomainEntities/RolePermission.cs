﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RolePermission")]
    public class RolePermission : BaseDTO<int>
    {
        public int RoleId { get; set; }
        public int AppResourceId { get; set; }
        public int AppPermissionId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
        [ForeignKey("AppResourceId")]
        public virtual AppResource AppResource { get; set; }
        [ForeignKey("AppPermissionId")]
        public virtual AppPermission AppPermission { get; set; }
    }
}
