﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
     [Table("CustomerUser")]
    public class CustomerUser:BaseDTO<int>
    {
        public int CustomerId { get; set; }
        public long UserId { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
    }
}
