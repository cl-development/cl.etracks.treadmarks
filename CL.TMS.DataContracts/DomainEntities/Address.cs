﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Address")]
    public class Address : InvalidFormFieldModel
    {
        public Address()
        {
            this.Vendors = new List<Vendor>();
            this.Contacts = new List<Contact>();
            this.VendorStorageSites = new List<VendorStorageSite>();
            this.Customers = new List<Customer>();
        }
        public int AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Ext { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> GPSCoordinate1 { get; set; }
        public Nullable<decimal> GPSCoordinate2 { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<VendorStorageSite> VendorStorageSites { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
