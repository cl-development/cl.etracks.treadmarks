﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimInventory")]
    public class ClaimInventory:BaseDTO<int>
    {
        public int ClaimId { get; set; }
        public int VendorId { get; set; }
        public int ItemId { get; set; }
        public bool IsEligible { get; set; }
        public int Qty { get; set; }
        public decimal Weight { get; set; }
        public decimal ActualWeight { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
