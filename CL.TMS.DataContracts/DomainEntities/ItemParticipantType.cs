﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
   [Table("ItemParticipantType")]
  public  class ItemParticipantType: BaseDTO<int>
   {
       public int ParticipantTypeId { get;set; }
       public int ItemId { get; set; }
       public virtual Item Item { get; set; }
    }
}
