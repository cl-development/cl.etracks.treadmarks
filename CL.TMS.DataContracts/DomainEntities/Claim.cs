﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Claim")]
    public class Claim : BaseDTO<int>
    {
        public Claim()
        {
            ClaimDetails = new List<ClaimDetail>();
            ClaimNotes = new List<ClaimNote>();
            ClaimPayments = new List<ClaimPayment>();
            Attachments = new List<Attachment>();
            ClaimInternalPaymentAdjusts = new List<ClaimInternalPaymentAdjust>();
            ClaimInventoryAdjustments = new List<ClaimInventoryAdjustment>();
            ClaimSupportingDocuments = new List<ClaimSupportingDocument>();

        }

        public int ClaimPeriodId { get; set; }


        public int ParticipantId { get; set; }

        public int ParticipantAddressID { get; set; }

        public decimal? TotalTax { get; set; }
        public bool? IsTaxApplicable { get; set; } //OTSTM2-1294
        public Nullable<decimal> AdjustmentTotal { get; set; }
        public Nullable<decimal> ClaimsAmountTotal { get; set; }
        public Nullable<decimal> ClaimsAmountCredit { get; set; }
        public string Status { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public DateTime? ChequeDueDate { get; set; }
        public long? AssignToUserId { get; set; }
        public DateTime? AssignDate { get; set; }
        public int ClaimType { get; set; }

        public long? SubmittedBy { get; set; }
        public long? ApprovedBy { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public long? ReviewedBy { set; get; }
        public DateTime? ReviewDueDate { get; set; }
        public DateTime? ReviewStartDate { get; set; }
        public DateTime? ReviewEndDate { get; set; }
        public bool? ClaimOnhold { get; set; }
        public bool? AuditOnhold { get; set; }
        public DateTime? ClaimOnHoldDate { get; set; }
        public DateTime? ClaimOffHoldDate { get; set; }
        public DateTime? AuditOnHoldDate { get; set; }
        public DateTime? AuditOffHoldDate { get; set; }

        public long? ClaimOnHoldBy { get; set; }
        public long? AuditOnHoldBy { get; set; }

        public int ClaimOnHoldDays { get; set; }
        public int AuditOnHoldDays { get; set; }
        public bool? InBatch { get; set; }

        [ForeignKey("ParticipantAddressID")]
        public virtual Address ParticipantAddress { get; set; }

        [ForeignKey("ParticipantId")]
        public virtual Vendor Participant { get; set; }
        public virtual ClaimPaymentSummary ClaimPaymentSummary { get; set; }

        public virtual ClaimSummary ClaimSummary { get; set; }
        public virtual ClaimProcess ClaimProcess { get; set; }

        [ForeignKey("SubmittedBy")]
        public virtual User SubmittedUser { get; set; }
        [ForeignKey("ReviewedBy")]
        public virtual User ReviewedUser { get; set; }
        [ForeignKey("AssignToUserId")]
        public virtual User AssignedToUser { get; set; }

        [ForeignKey("ClaimPeriodId")]
        public virtual Period ClaimPeriod { get; set; }

        public virtual ICollection<ClaimDetail> ClaimDetails { get; set; }
        public virtual ICollection<ClaimNote> ClaimNotes { get; set; }
        public virtual ICollection<ClaimPayment> ClaimPayments { get; set; }
        public virtual ICollection<ClaimInternalPaymentAdjust> ClaimInternalPaymentAdjusts { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<ClaimInventoryAdjustment> ClaimInventoryAdjustments { get; set; }

        public virtual ICollection<ClaimSupportingDocument> ClaimSupportingDocuments { get; set; }
    }
}