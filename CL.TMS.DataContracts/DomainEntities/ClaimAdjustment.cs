using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimAdjustment")]
    public class ClaimAdjustment : BaseDTO<int>
    {
        public int ClaimID { get; set; }
        public string ParticipantUnifier { get; set; }
        public Nullable<System.DateTime> AdjustmentDate { get; set; }
        public Nullable<decimal> AdjustmentAmount { get; set; }
        public string AdjustmentReason { get; set; }
        public int Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
