using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimSummaryAdjustment")]
    public class ClaimSummaryAdjustment : BaseDTO<int>
    {
     
        //public int ID { get; set; }
        public int ClaimAdjustmentDetailsID { get; set; }
        public decimal TotalEligible { get; set; }
        public decimal OpeningEligible { get; set; }
        public decimal TotalIneligible { get; set; }
        public decimal OpeningIneligible { get; set; }
        public decimal TotalEligibleDelivered { get; set; }
        public decimal TotalIneligibleDelivered { get; set; }
        public decimal ClosingEligible { get; set; }
        public decimal ClosingIneligible { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
    
        public virtual ClaimAdjustmentDetail ClaimAdjustmentDetail { get; set; }
    }
}
