﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptProcessorTIPIReport
    {
        public string Processor { get; set; }
        public string RateType { get; set; } //OTSTM2-1225
        public string ClaimPeriod { get; set; }
        public int SubmissionNumber { get; set; }
        public string RecordState { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> TIPI { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> PI { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> Adjustments { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> HST { get; set; }
        public Nullable<decimal> Payment { get; set; }

    }
}
