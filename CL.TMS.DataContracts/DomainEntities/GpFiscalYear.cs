﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpFiscalYear")]
    public class GpFiscalYear
    {
        public int ID { get; set; }    
        public DateTime MinimumDocDate { get; set; }    
        public DateTime EffectiveStartDate { get; set; }    
        public DateTime EffectiveEndDate { get; set; }    
    }
}
