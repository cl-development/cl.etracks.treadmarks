﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Region")]
    public class Region : BaseDTO<int>
    {
        public string Name { get; set; }
        public virtual List<RegionDetail> RegionDetails { get; set; }
    }
}
