﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Application")]
    public class Application : InvalidFormFieldModel
    {
        public Application()
        {
            this.Attachments = new HashSet<Attachment>();
            this.Vendors = new HashSet<Vendor>();
            this.ApplicationSupportingDocuments = new HashSet<ApplicationSupportingDocument>();
        }

        public int RegistrantID { get; set; }
        public string FormCompletedByFirstName { get; set; }
        public string FormCompletedByLastName { get; set; }
        public string FormCompletedByPhone { get; set; }
        public string SigningAuthorityFirstName { get; set; }
        public string SigningAuthorityLastName { get; set; }
        public string SigningAuthorityPosition { get; set; }
        public DateTime? CompletedDate { get; set; }        
        public string CompletedByUser { get; set; }
        public string AgreementText { get; set; }
        public string AgreementAcceptedByFullName { get; set; }
        public bool? AgreementAcceptedCheck { get; set; }
        public string UserIP { get; set; }
        public string Status { get; set; }     
        public string FormObject { get; set; }        
        public string DenyReasons { get; set; }
        public long? AssignToUser { get; set; }
        
        public string Company { get; set; }
        public string Contact { get; set; }
        public DateTime? ExpireDate { get; set; }

        public int StatusIndex { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<ApplicationSupportingDocument> ApplicationSupportingDocuments { get; set; }
        public bool? BankPolicyAgreementAcceptedCheck { get; set; }

        public int? TermsAndConditionsID { get; set; }

        [ForeignKey("AssignToUser")]
        public virtual User AssignUser { get; set; }
    }
}
