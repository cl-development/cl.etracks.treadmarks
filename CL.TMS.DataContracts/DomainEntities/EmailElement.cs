﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("EmailElement")]
    public class EmailElement : BaseDTO<int>
    {    
        public string ElementType { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

    }    
}

