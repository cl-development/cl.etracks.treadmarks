﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Announcement")]
    public class Announcement //: BaseDTO<int> don't need track the version.
    {
        public int ID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Steward { get; set; }
        public bool Collector { get; set; }
        public bool Hauler { get; set; }
        public bool Processor { get; set; }
        public bool RPM { get; set; }
        public bool Staff { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }
        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }
    }
}
