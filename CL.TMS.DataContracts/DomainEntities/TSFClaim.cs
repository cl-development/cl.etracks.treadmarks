﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TSFClaim")]
    public class TSFClaim : BaseDTO<int>
    {
        public TSFClaim()
        {
            this.TSFClaimDetails = new HashSet<TSFClaimDetail>();
            this.TSFRemittanceNotes = new HashSet<TSFRemittanceNote>();
            this.TSFClaimInternalPaymentAdjustments = new HashSet<TSFClaimInternalPaymentAdjustment>();
        }

        //public int ID { get; set; }
        public int CustomerID { get; set; }
        public string RegistrationNumber { get; set; }
        public int PeriodID { get; set; }
        public string PreparedBy { get; set; }
        public decimal TotalTSFDue { get; set; }
        public decimal ApplicableTaxesGst { get; set; }
        public decimal ApplicableTaxesHst { get; set; }
        public decimal TSFDuePostHst { get; set; }
        public decimal TSFDuePreHst { get; set; }
        public decimal Penalties { get; set; }
        public decimal? PenaltiesManually { get; set; }
        public decimal Interest { get; set; }
        public decimal TotalRemittancePayable { get; set; }
        public string PaymentType { get; set; }
        public string PaymentReferenceNumber { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public Nullable<System.DateTime> ReceiptDate { get; set; }
        public Nullable<System.DateTime> DepositDate { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public Nullable<System.DateTime> StartedDate { get; set; }
        public string RecordState { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public Nullable<bool> InBatch { get; set; }
        public string UniqueNumber { get; set; }
        public Nullable<System.DateTime> SubmissionDate { get; set; }
        public bool IsCreatedInTm { get; set; }
        public string ChequeReferenceNumber { get; set; }
        public string Account { get; set; }
        public byte IsPenaltyOverride { get; set; }
        public byte IsTaxApplicable { get; set; }
        public Nullable<long> AssignToUser { get; set; }
        public string SupportingDocOption { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool? IsActive { get; set; }
        public decimal? Credit { get; set; }
        public bool IsCreatedByStaff { get; set; } //OTSTM2-56
        public string CurrencyType { get; set; }//OTSTM2-370
        public Nullable<decimal> AdjustmentTotal { get; set; } //OTSTM2-485

        public virtual Customer Customer { get; set; }       
        public virtual Period Period { get; set; }
        public virtual ICollection<TSFClaimDetail> TSFClaimDetails { get; set; }
        public virtual ICollection<TSFRemittanceNote> TSFRemittanceNotes { get; set; }        
        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual ICollection<TSFClaimInternalPaymentAdjustment> TSFClaimInternalPaymentAdjustments { get; set; }  //OTSTM2-485
    }
}
