﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("InvitationRole")]
    public class InvitationRole:BaseDTO<int>
    {
        public long InvitationId { get; set; }
        public int Role { get; set; }
        public virtual Invitation Invitation { get; set; }
    }
}
