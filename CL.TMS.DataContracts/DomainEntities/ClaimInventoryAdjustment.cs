﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimInventoryAdjustment")]
    public class ClaimInventoryAdjustment : BaseDTO<int>
    {
        public ClaimInventoryAdjustment()
        {
            ClaimInventoryAdjustItems = new List<ClaimInventoryAdjustItem>();
            ClaimInternalAdjustmentNotes = new List<ClaimInternalAdjustmentNote>();
        }
        [ForeignKey("Claim")]
        public int ClaimId { get; set; }
        public int AdjustmentType { get; set; }
        public bool IsEligible { get; set; }
        public decimal AdjustmentWeightOnroad { get; set; }
        public decimal AdjustmentWeightOffroad { get; set; }
        public int Direction { get; set; }
        [ForeignKey("AdjustUser")]
        public long AdjustBy { get; set; }
        public DateTime AdjustDate { get; set; }
        public int UnitType { get; set; }
        public virtual User AdjustUser { get; set; }
        public virtual Claim Claim { get; set; }
        public virtual ICollection<ClaimInventoryAdjustItem> ClaimInventoryAdjustItems { get; set; }
        //OTSTM2-668
        public virtual ICollection<ClaimInternalAdjustmentNote> ClaimInternalAdjustmentNotes { get; set; }
    }
}
