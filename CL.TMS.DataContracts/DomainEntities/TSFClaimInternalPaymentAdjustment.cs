﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TSFClaimInternalPaymentAdjustment")]
    public class TSFClaimInternalPaymentAdjustment : BaseDTO<int>
    {
        public int TSFClaimId { get; set; }
        public int PaymentType { get; set; }
        public string Status { get; set; }
        public decimal AdjustmentAmount { get; set; }

        [ForeignKey("AdjustUser")]
        public long AdjustBy { get; set; }
        public DateTime AdjustDate { get; set; }

        public virtual User AdjustUser { get; set; }

        [ForeignKey("TSFClaimId")]
        public virtual TSFClaim TSFClaim { get; set; }

        public virtual ICollection<TSFClaimInternalAdjustmentNote> TSFClaimInternalAdjustmentNotes { get; set; }

        public TSFClaimInternalPaymentAdjustment()
        {
            TSFClaimInternalAdjustmentNotes = new List<TSFClaimInternalAdjustmentNote>();
        }
    }
}
