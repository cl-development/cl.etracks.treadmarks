﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionItem")]
    public class TransactionItem : BaseDTO<int>
    {
        public int ItemID { get; set; }
        public int? Quantity { get; set; }
        public int UnitType { get; set; }
        public decimal AverageWeight { get; set; }
        public decimal ActualWeight { get; set; }
        public decimal ScheduledIn { get; set; }
        public decimal ScheduledOut { get; set; }
        
        public int TransactionId { get; set; }        
        public int? TransactionAdjustmentId { get; set; }
        public int? MeshRangeStart { get; set; }
        public int? MeshRangeEnd { get; set; }
        public bool? IsFreeOfSteel { get; set; }
        public string ItemDescription { get; set; }
        public string ItemCode { get; set; }
        public decimal? Rate { get; set; }        
        public decimal? RecyclablePercentage { get; set; }
        [ForeignKey("ItemID")]
        public virtual Item Item { get; set; }        
    }
}
