﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionAdjustment")]
    public partial class TransactionAdjustment : BaseDTO<int>
    {
        public long StartedBy { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public string Status { get; set; }       
        public int OriginalTransactionId { get; set; }
        public string ResponseComments { get; set; }
        public long? ApprovalUser { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public long? RejectedBy { get; set; }
        public DateTime? RejectedDate { get; set; }
        public string RejectedReason { get; set; }
        public long? RecallBy { get; set; }
        public DateTime? RecallDate { get; set; }
        public bool IsAdjustByStaff { get; set; }
        public bool IsEligible { get; set; }
        public bool IsGenerateTires { get; set; }
        public decimal? OnRoadWeight { get; set; }
        public decimal? OffRoadWeight { get; set; }
        public decimal? EstimatedOnRoadWeight { get; set; }
        public decimal? EstimatedOffRoadWeight { get; set; }
        public string MarketLocation { get; set; }
        public int? TireMarketType { get; set; }
        public string BillofLoading { get; set; }
        public string InvoiceNumber { get; set; }
        public string EventNumber { get; set; }        
        public int? PaymentEligible { get; set; }
        public string PaymentEligibleOther { get; set; }
        public int? MaterialType { get; set; }     
        public string PTRNumber { get; set; }
        public int? DispositionReason { get; set; }
        public int? CreatedVendorId { get; set; }


        [ForeignKey("StartedBy")]
        public virtual User StartUser { get; set; }
        [ForeignKey("OriginalTransactionId")]
        public virtual Transaction Transaction { get; set; }        

        public int? CompanyInfoId { get; set; }
        [ForeignKey("CompanyInfoId")]
        public virtual CompanyInfo CompanyInfo { get; set; }

        public int? VendorInfoId { get; set; }
        [ForeignKey("VendorInfoId")]
        public virtual Vendor VendorInfo { get; set; }
        
        public virtual List<TransactionItem> TransactionItems { get; set; }        
        public virtual List<TransactionPhoto> TransactionPhotos { get; set; }
        public virtual List<ScaleTicket> ScaleTickets { get; set; }
        public virtual List<TransactionAttachment> TransactionAttachments { get; set; }
        public virtual List<TransactionSupportingDocument> TransactionSupportingDocuments { get; set; }


        public TransactionAdjustment()
        {
            TransactionItems = new List<TransactionItem>();            
            TransactionPhotos = new List<TransactionPhoto>();
            TransactionAttachments = new List<TransactionAttachment>();
            ScaleTickets = new List<ScaleTicket>();
            TransactionSupportingDocuments = new List<TransactionSupportingDocument>();
        }
    }
}
