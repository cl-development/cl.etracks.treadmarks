﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;


namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Item")]
    public class Item : BaseDTO<int>
    {
        public Item()
        {
            Vendors = new List<Vendor>();
            Customers = new List<Customer>();
            ItemWeights = new List<ItemWeight>();
        }
        public int ItemType { get; set; }
        public string ShortName { get; set; }
        public string Name {get;set;}
        public string Description { get; set; }
        public string TaxCode { get; set; }
        public int UnitType { get; set; }
        public bool IsActive { get; set; }

        public int ItemCategory { get; set; }
        public DateTime? EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }

        public Decimal? AvailableRangeMin { get; set; }
        public Decimal? AvailableRangeMax { get; set; }

        public virtual ICollection<ItemParticipantType> ItemParticipantTypes {get;set;}
        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }

        public virtual ICollection<ItemWeight> ItemWeights { get; set; }

    }
}
