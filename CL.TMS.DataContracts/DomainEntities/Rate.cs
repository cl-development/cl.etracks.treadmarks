﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Rate")]
    public class Rate : BaseDTO<int>
    {
        public int? ItemID { get; set; }
        public int? PaymentType { get; set; }
        public int ClaimType { get; set; }
        public int? PeriodID { get; set; }
        public decimal ItemRate { get; set; }
        public DateTime? EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }
        public int? SourceZoneID { get; set; }
        public int? DeliveryZoneID { get; set; }
        public string GreaterZone { get; set; }

        public string RateDescription { get; set; }

        public int? ItemType { get; set; }

        public int? RateTransactionID { get; set; }
        public bool? IsSpecificRate { get; set; }
        
        [ForeignKey("RateTransactionID")]
        public virtual RateTransaction RateTransaction { get; set; }

        [ForeignKey("ItemID")]
        public virtual Item item { get; set; }
        [ForeignKey("PeriodID")]
        public virtual Period period { get; set; }
    }

}
