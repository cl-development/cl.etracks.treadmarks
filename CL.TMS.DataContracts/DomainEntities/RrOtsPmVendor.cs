﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsPmVendor")]
    public class RrOtsPmVendor
    {
        [Key, Column(Order = 0)]
        public string VENDORID { get; set; }
        public string VENDNAME { get; set; }
        [Key, Column(Order = 1)]
        public string VADDCDPR { get; set; }
        public string VNDCLSID { get; set; }
        public string VNDCNTCT { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string COUNTRY { get; set; }
        public string PHNUMBR1 { get; set; }
        public string FAXNUMBR { get; set; }
        public string CURNCYID { get; set; }
        public string TAXSCHID { get; set; }
        public string PYMTRMID { get; set; }
        [Key, Column(Order = 2)]
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        public int? VENDSTTS { get; set; }
        [Key, Column(Order = 3)]
        public string INTERID { get; set; }
        public int? INTSTATUS { get; set; }
        public DateTime? INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DEX_ROW_ID { get; set; }
        [Display(Name = "fibatchentry_id")] //OTSTM2-1124
        public long gpibatchentry_id { get; set; }
        public string EmailToAddress { get; set; }
        public string EmailCcAddress { get; set; }
        public string BANKNAME { get; set; }
        public string EFTTransitNumber { get; set; }
        public string EFTBankNumber { get; set; }
        public string EFTBankAcct { get; set; }
        public string Key { get; set; }    
    }
}
