﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionComment")]
    public class TransactionComment : BaseDTO<int>
    {
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TransactionId { get; set; }        
    }
}
