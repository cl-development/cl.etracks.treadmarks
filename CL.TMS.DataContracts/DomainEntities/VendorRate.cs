﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorRate")]
    public class VendorRate  
    {
        public int ID { get; set; }
        public int RateTransactionID { get; set; }
        public int VendorID { get; set; }

        [ForeignKey("RateTransactionID")]
        public virtual RateTransaction RateTransaction { get; set; }
    }
}
