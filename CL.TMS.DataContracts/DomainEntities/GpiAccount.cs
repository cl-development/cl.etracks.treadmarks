﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpiAccount")]
    public class GpiAccount
    {
        [Key]
        public long id { get; set; }
        public string account_number { get; set; }
        public string description { get; set; }
        public long distribution_type_id { get; set; }
        public long? tire_type_id { get; set; }
        public string registrant_type_ind { get; set; }
        public string ShortName { get; set; }
    }
}
