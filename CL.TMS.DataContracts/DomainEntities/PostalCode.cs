﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("PostalCode")]
    public class PostalCode : BaseDTO<int>
    {
        public int ZoneID { get; set; }
        public string PostalCodePrefix { get; set; }
        public string City { get; set; }
        public DateTime? EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }

        public int ZoneType { get; set; }

        [ForeignKey("ZoneID")]
        public virtual Zone Zone { get; set; }
    }
}
