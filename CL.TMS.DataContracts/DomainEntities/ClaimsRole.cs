using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{
  
    [Table("ClaimsRole")]
    public class ClaimsRole : BaseDTO<int>  
    {
        //public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
