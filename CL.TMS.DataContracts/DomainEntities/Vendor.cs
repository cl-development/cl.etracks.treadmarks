﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;


namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Vendor")]
    public class Vendor : InvalidFormFieldModel
    {
        public Vendor()
        {
            this.Assets = new HashSet<Asset>();
            this.AppUsers = new HashSet<AppUser>();
            this.BankInformations = new HashSet<BankInformation>();
            this.VendorAddresses = new HashSet<Address>();
            this.BusinessActivities = new HashSet<BusinessActivity>();
            this.VendorStorageSites = new HashSet<VendorStorageSite>();
            this.VendorInventories = new HashSet<Inventory>();
            this.Items = new HashSet<Item>();
            this.VendorActiveHistories = new HashSet<VendorActiveHistory>();
            this.VendorSupportingDocuments = new List<VendorSupportingDocument>();
        }

        public string FormCompletedByFirstName { get; set; }
        public string FormCompletedByLastName { get; set; }
        public string FormCompletedByPhone { get; set; }
        public string SigningAuthorityFirstName { get; set; }
        public string SigningAuthorityLastName { get; set; }
        public string SigningAuthorityPosition { get; set; }
        public string AgreementAcceptedByFullName { get; set; }
        public Nullable<bool> AgreementAcceptedCheck { get; set; }

        public int VendorType { get; set; }
        public string Number { get; set; }
        public string BusinessName { get; set; }
        public string FranchiseName { get; set; }
        public string OperatingName { get; set; }
        public string BusinessNumber { get; set; }
        public string TerritoryCode { get; set; }
        public string GlobalDimension1Code { get; set; }
        public string GlobalDimension2Code { get; set; }
        public Nullable<int> CreditLimit { get; set; }
        public string TelexAnswerBack { get; set; }
        public string TaxRegistrationNo { get; set; }
        public string TaxRegistrationNo2 { get; set; }
        public string HomePage { get; set; }
        public string BusinessGroupCode { get; set; }
        public string CustomerGroupCode { get; set; }
        public string DepartmentCode { get; set; }
        public string ProjectCode { get; set; }
        public string PurchaserCode { get; set; }
        public string SalesCampaignCode { get; set; }
        public string SalesPersonCode { get; set; }
        public string AuthSigComplete { get; set; }
        public Nullable<DateTime> ReceivedDate { get; set; }
        public Nullable<DateTime> GracePeriodStartDate { get; set; }
        public Nullable<DateTime> GracePeriodEndDate { get; set; }
        public string CommercialLiabInsurerName { get; set; }
        public Nullable<DateTime> CommercialLiabInsurerExpDate { get; set; }
        public string CommercialLiabHSTNumber { get; set; }
        public string WorkerHealthSafetyCertNo { get; set; }
        public string PermitsCertDescription { get; set; }
        public Nullable<bool> AmendedAgreement { get; set; }
        public Nullable<DateTime> AmendedDate { get; set; }
        public bool IsTaxExempt { get; set; }
        public string UpdateToFinancialSoftware { get; set; }
        public string PrimaryBusinessActivity { get; set; }
        public Nullable<bool> MailingAddressSameAsBusiness { get; set; }
        public int BusinessActivityID { get; set; }
        public string GSTNumber { get; set; }
        public string AuthorizedSigComplete { get; set; }
        public string LocationEmailAddress { get; set; }
        public Nullable<DateTime> DateReceived { get; set; }
        public Nullable<DateTime> ActivationDate { get; set; }
        public Nullable<DateTime> ConfirmationMailedDate { get; set; }
        public Nullable<int> PreferredCommunication { get; set; }
        public int RegistrantTypeID { get; set; }
        public int RegistrantSubTypeID { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<DateTime> LastUpdatedDate { get; set; }
        public Nullable<int> CheckoutNum { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool InBatch { get; set; }
        public bool IsActive { get; set; }
        public Nullable<DateTime> ActiveStateChangeDate { get; set; }
        public string FacilityDescription { get; set; }
        public string TaxClass { get; set; }
        public Nullable<int> PrimaryBusinessAddrStorageCap { get; set; }
        public Nullable<int> NumberOfStorageSites { get; set; }
        public Nullable<int> MaxStorageSitesCapacity { get; set; }
        public string StorageSiteID { get; set; }
        public string CertificateOfApprovalNum { get; set; }
        public Nullable<DateTime> CompanyYearEndDate { get; set; }
        public string ProcProductsProduced { get; set; }
        public string ProcProductsProducedDesc { get; set; }
        public string HPrimaryBusAddrSortYards { get; set; }
        public string HNumberOfSortYards { get; set; }
        public Nullable<int> HSortYardsStorageCap { get; set; }
        public Nullable<int> HSortYardID { get; set; }
        public Nullable<DateTime> HYardCountDate { get; set; }
        public Nullable<DateTime> HYardCountDate2 { get; set; }
        public Nullable<int> HPrimaryBusAddrStorageCap { get; set; }
        public string CPointsAdditional { get; set; }
        public string CPointsType { get; set; }
        public string CPointsGarage { get; set; }
        public string CPointsUsedVehicle { get; set; }
        public string CPointsOther { get; set; }
        public string CPointsOtherDesc { get; set; }
        public Nullable<bool> CHasClaimInLegacy { get; set; }
        public string HasLocalInventory { get; set; }
        public Nullable<bool> CIsGenerator { get; set; }
        public Nullable<DateTime> CGeneratorDate { get; set; }
        public Nullable<bool> IsSubCollector { get; set; } //OTSTM2-953
        public Nullable<DateTime> SubCollectorDate { get; set; } //OTSTM2-953
        public string EndUseMarket { get; set; }
        public string EndUseMarketDesc { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<bool> HHasRelatioshipWithProcessor { get; set; }
        public Nullable<int> HRelatedProcessor { get; set; }
        public Nullable<bool> HIsGVWR { get; set; }
        public string CVORNumber { get; set; }
        public Nullable<DateTime> CVORExpiryDate { get; set; }
        public Nullable<bool> HasMoreThanOneEmp { get; set; }
        public string WSIBNumber { get; set; }
        public string InvalidFormFields { get; set; }
        public Nullable<bool> ContactAddressSameAsBusiness { get; set; }
        public Nullable<DateTime> BusinessStartDate { get; set; }
        public string RegistrantStatus { get; set; }
        public string ContactInfo { get; set; }
        public Nullable<bool> GpUpdate { get; set; }
        public string KeyWords { get; set; }
        public bool InactiveClaimValid { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AppUser> AppUsers { get; set; }
        public virtual ICollection<BankInformation> BankInformations { get; set; }

        public virtual ICollection<Address> VendorAddresses { get; set; }
        public virtual ICollection<BusinessActivity> BusinessActivities { get; set; }
        public virtual ICollection<VendorStorageSite> VendorStorageSites { get; set; }
        public virtual ICollection<Inventory> VendorInventories { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual Application Application { get; set; }
        public virtual ICollection<VendorActiveHistory> VendorActiveHistories { get; set; }

        public virtual ICollection<VendorSupportingDocument> VendorSupportingDocuments { get; set; }

        public string OtherRegistrantSubType { get; set; }
        public string OtherProcessProduct { get; set; }

    }
}
