﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptHaulerVolumeReport
    {
        public string ReportingPeriod { get; set; }
        public string HaulerRegistrationNumber { get; set; }
        public bool HaulerStatus { get; set; }
        public string DestinationType { get; set; }
        public string ProcessorHaulerRegistrationNumber { get; set; }
        public bool Status { get; set; }
        public string RecordState { get; set; }
        public string UsedTireDestionation { get; set; }
        public Nullable<decimal> TotalActualWeight { get; set; }
        public Nullable<decimal> TotalEstWeight { get; set; }
        public Nullable<decimal> PltEstWeight { get; set; }
        public Nullable<decimal> MtEstWeight { get; set; }
        public Nullable<decimal> AglsEstWeight { get; set; }
        public Nullable<decimal> IndEstWeight { get; set; }
        public Nullable<decimal> SotrEstWeight { get; set; }
        public Nullable<decimal> MotrEstWeight { get; set; }
        public Nullable<decimal> LotrEstWeight { get; set; }
        public Nullable<decimal> GotrEstWeight { get; set; }
        public Nullable<decimal> TotalPaymentEarned { get; set; }
        public Nullable<decimal> PltPaymentEarned { get; set; }
        public Nullable<decimal> MtPaymentEarned { get; set; }
        public Nullable<decimal> AglsPaymentEarned { get; set; }
        public Nullable<decimal> IndPaymentEarned { get; set; }
        public Nullable<decimal> SotrPaymentEarned { get; set; }
        public Nullable<decimal> MotrPaymentEarned { get; set; }
        public Nullable<decimal> LotrPaymentEarned { get; set; }
        public Nullable<decimal> GotrPaymentEarned { get; set; }
        public Nullable<decimal> Hst { get; set; }
        public Nullable<decimal> TotalPaymentEarnedWithHst { get; set; }
    }
}
