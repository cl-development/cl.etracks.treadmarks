﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("BusinessActivity")]
    public class BusinessActivity : BaseDTO<int>
    {
        public BusinessActivity()
        {
            Vendors = new List<Vendor>();
            Customers = new List<Customer>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }

        public int BusinessActivityType { get; set; }
    }
}
