﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.RetailConnectionService
{
    public class RetailerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}