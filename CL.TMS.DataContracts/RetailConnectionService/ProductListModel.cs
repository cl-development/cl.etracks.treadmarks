﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.RetailConnectionService
{
    public class ProductListModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Brand { get; set; }
        public string ImageUrl { get; set; }
    }
}