//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.GpStaging
{
    using System;
    using System.Collections.Generic;
    
    public partial class RR_OTS_RM_APPLY
    {
        [Key, Column(Order = 0)]
        public string APTODCNM { get; set; }
        [Key, Column(Order = 1)]
        public string APFRDCNM { get; set; }
        public decimal APPTOAMT { get; set; }
        [Key, Column(Order = 2)]
        public int APFRDCTY { get; set; }
        [Key, Column(Order = 3)]
        public int APTODCTY { get; set; }
        public Nullable<System.DateTime> APPLYDATE { get; set; }
        public Nullable<System.DateTime> GLPOSTDT { get; set; }
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 4)]
        public string INTERID { get; set; }
        public Nullable<int> INTSTATUS { get; set; }
        public Nullable<System.DateTime> INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DEX_ROW_ID { get; set; }
    }
}
