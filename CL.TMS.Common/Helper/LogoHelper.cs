﻿using System.Net.Mail;
using System.Net.Mime;

namespace CL.TMS.Common.Helper
{
    public static class LogoHelper
    {
        public static LinkedResource GetCompanyLogo(string logoURL, string uploadRepositoryPath)
        {
            //OTSTM2-1003            
            var extension = System.IO.Path.GetExtension(logoURL);
            var fileType = string.Empty;
            switch (extension)
            {
                case ".jpg":
                    fileType = MediaTypeNames.Image.Jpeg;
                    break;
                case ".gif":
                    fileType = MediaTypeNames.Image.Gif;
                    break;
                case ".png":
                    fileType = "image/png";
                    break;
                default:
                    fileType = MediaTypeNames.Image.Jpeg;
                    break;
            }
            var otsEmailLogo = new LinkedResource(System.IO.Path.Combine(uploadRepositoryPath, logoURL), fileType);
            return otsEmailLogo;
        }
    }
}
