﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Helper
{
    public class Base64
    {
        private const string Plus = "+";
        private const string Minus = "-";
        private const string Slash = "/";
        private const string Underscore = "_";
        private const string EqualSign = "=";
        private const string Pipe = "|";
        private static IDictionary<string, string> _mapper = new Dictionary<string, string> { { Plus, Minus }, { Slash, Underscore }, { EqualSign, Pipe } };


        public static string EncodeString(string input)
        {
            if (input == null)
                return null;
            byte[] plaintextBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return System.Convert.ToBase64String(plaintextBytes);
        }

        public static string DecodeString(string input)
        {
            if (input == null)
                return null;

            byte[] base64EncodedBytes = System.Convert.FromBase64String(input);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


        /// <summary>
        /// Encode the base64 to safeUrl64
        /// </summary>
        /// <param name="base64Str"></param>
        /// <returns></returns>
        public static string EncodeBase64Url(string base64Str)
        {
            if (string.IsNullOrEmpty(base64Str))
                return base64Str;

            foreach (var pair in _mapper)
            {
                base64Str = base64Str.Replace(pair.Key, pair.Value);
            }

            return base64Str;
        }


        /// <summary>
        /// Decode the Url back to original base64
        /// </summary>
        /// <param name="safe64Url"></param>
        /// <returns></returns>
        public static string DecodeBase64Url(string safe64Url)
        {

            if (string.IsNullOrEmpty(safe64Url))
                return safe64Url;

            foreach (var pair in _mapper)
            {
                safe64Url = safe64Url.Replace(pair.Value, pair.Key);
            }

            return safe64Url;
        }

    }
}
