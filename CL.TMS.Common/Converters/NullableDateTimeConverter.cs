﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CL.TMS.Common.Converters
{
    /// <summary>
    /// Nullable datetime converter used by AutoMapper
    /// </summary>
    public class NullableDateTimeConverter : ITypeConverter<DateTime?, DateTime?>
    {
        public DateTime? Convert(ResolutionContext context)
        {
            var sourceDate = context.SourceValue as DateTime?;
            if (sourceDate.HasValue)
                return sourceDate.Value;
            else
                return default(DateTime?);
        }
    }
}
