﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CL.TMS.Common.Enum;

namespace CL.TMS.Common.Converters
{
    /// <summary>
    /// To convert one type to another for central repository 
    /// </summary>
    public class EnumTypeConverter
    {
        /// <summary>
        /// Takes ProductTypeEnumCR as input and gives back ProductTypeEnum
        /// </summary>
        public static int ConvertToProductType(int productTypeEnumCR)
        {
            switch (productTypeEnumCR)
            {
                case (int)ProductTypeEnumCR.TD6FP:
                    return (int)ProductTypeEnum.TD6FP;
                case (int)ProductTypeEnumCR.TD6NP:
                    return (int)ProductTypeEnum.TD6NP;
                case (int)ProductTypeEnumCR.TDP1:
                    return (int)ProductTypeEnum.TDP1;
                case (int)ProductTypeEnumCR.TDP2:
                    return (int)ProductTypeEnum.TDP2;
                case (int)ProductTypeEnumCR.TDP3:
                    return (int)ProductTypeEnum.TDP3;
                case (int)ProductTypeEnumCR.TDP4FF:
                    return (int)ProductTypeEnum.TDP4FF;
                case (int)ProductTypeEnumCR.TDP4FF_NoPI:
                    return (int)ProductTypeEnum.TDP4FF_NoPI;
                case (int)ProductTypeEnumCR.TDP5FT:
                    return (int)ProductTypeEnum.TDP5FT;
                case (int)ProductTypeEnumCR.TDP5NT:
                    return (int)ProductTypeEnum.TDP5NT;
                case (int)ProductTypeEnumCR.TDP7FT:
                    return (int)ProductTypeEnum.TDP7FT;
                case (int)ProductTypeEnumCR.TDP7NT:
                    return (int)ProductTypeEnum.TDP7NT;
                case (int)ProductTypeEnumCR.Transfer_Fiber_Rubber:
                    return (int)ProductTypeEnum.Transfer_Fiber_Rubber;                
            }
            return 0;
        }
    }
}
