﻿using System;
using System.Security.Claims;
using System.Web;

namespace CL.TMS.Common
{
    /// <summary>
    /// TreadMarks applicatin context for storing user principal
    /// </summary>
    public static class TMSContext
    {
        #region Properties
        [ThreadStatic]
        private static bool isLocal;

        public static bool IsLocal
        {
            get { return isLocal; }
            set { isLocal = value; }
        }
        [ThreadStatic]
        private static ClaimsPrincipal tMSPrincipal;

        public static ClaimsPrincipal TMSPrincipal
        {
            get { return GetContext(); }
            set { SetContext(value); }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get context from container
        /// </summary>
        /// <returns></returns>
        private static ClaimsPrincipal GetContext()
        {
            if (isLocal)
            {
                return tMSPrincipal;
            }
            else
            {
                if (HttpContext.Current == null)
                {
                    return new ClaimsPrincipal(new ClaimsIdentity());
                }
                else
                {
                    return HttpContext.Current.User as ClaimsPrincipal;
                }
            }
        }

        /// <summary>
        /// Push context to container
        /// </summary>
        /// <param name="principal"></param>
        private static void SetContext(ClaimsPrincipal principal)
        {
            if (IsLocal)
            {
                tMSPrincipal = principal;
            }
            else
            {
                HttpContext.Current.User = principal;
            }
        }
        #endregion
    }
}
