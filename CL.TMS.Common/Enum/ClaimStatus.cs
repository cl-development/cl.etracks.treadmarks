﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ClaimStatus
    {
        [Description("Unknown")]
        Unknown = -1,
        [Description("Open")]
        Open = 0,
        [Description("Submitted")]
        Submitted, //1
        [Description("Under Review")]
        UnderReview, //2
        [Description("Hold")]
        Onhold, //3
        [Description("Audit Review")]
        AuditReview, //4
        [Description("Audit Hold")]
        AuditHold, //5
        [Description("Approved")]
        Approved, //6
        [Description("Receive Payment")]
        ReceivePayment       //7
    }
}
