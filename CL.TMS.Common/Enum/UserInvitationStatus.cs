﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum UserInvitationStatus
    {
        [Description("Invite Sent")]
        Invited = 1,
        [Description("Invite Accepted")]
        Accepted = 2,
        [Description("Invite Resent")]
        InviteResent = 3,
        InviteAlredyExists = 4,
        Edited = 5,
        UserCreated = 6,
        ErrorOccured = 7
    }
}
