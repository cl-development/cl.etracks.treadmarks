﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ClaimWorkflowStatus
    {
        None = 0,
        Representative, //1
        Lead,      //2
        Supervisor,//3
        Approver1, //4
        Approver2, //5
        ApprovePay //6
    }
}
