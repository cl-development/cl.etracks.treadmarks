﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ReportCategoryEnum : byte
    {
        [Description("Steward")]
        Steward = 1,

        [Description("Collector")]
        Collector = 2,

        [Description("Hauler")]
        Hauler = 3,

        [Description("Processor")]
        Processor = 4,

        [Description("RPM")]
        RPM = 5,

        [Description("Registrant")]
        Registrant = 6,

    }
}

