﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ClaimPaymentType
    {
        //Hauler
        [Description("Transportation Premium")]
        NorthernPremium = 1,
        [Description("DOT Premium")]
        DOTPremium,  //2
        [Description("Ineligible Inventory")] //in rate section, it using as Processor TI
        IneligibleInventoryPayment, //3

        //Collector
        PLT, //4
        MTGOTR,  //5

        //Processor
        [Description("PTR")]
        PTR,  //6
        [Description("SPS")]
        SPS, //7
        [Description("Outbound PIT")]
        PITOutbound, //8
        [Description("DOR")]
        DOR, //9

        //RPM
        [Description("Outbound SPS")]
        RPMSPS, //10

        //Overall -- Other paymentadjustment
        [Description("Overall")]
        Overall, //11

        //Collector Payment Changes OTSTM2- 961
        MT, //12
        AGLS, //13
        IND, //14
        SOTR, //15
        MOTR, //16
        LOTR, //17 
        GOTR,  //18
        [Description("STC Premium")]
        STCPremium, //19 OTSTM2-1215
    }
}
