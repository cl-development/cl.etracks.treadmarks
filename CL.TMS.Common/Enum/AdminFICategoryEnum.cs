﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum AdminFICategoryEnum : byte
    {
    
        Steward = 1,
        Collector = 2,
        Hauler = 3,
        Processor = 4,
        RPM = 5,
    }
}

