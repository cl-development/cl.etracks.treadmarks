﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum GlobalSearchSection
    {
        Applications = 1,
        Claims,
        Transactions,
        Remittance,
        Users,
        Reports,
        FinancialIntegration,
        RetailConnection
    }
}
