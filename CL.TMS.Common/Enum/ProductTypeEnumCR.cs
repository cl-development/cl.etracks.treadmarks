﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    //ProductTypeEnumCR for central repository
    public enum ProductTypeEnumCR
    {
        TDP5FT = 1,
        TDP5NT = 2,
        TD6NP = 3,
        TD6FP = 4,
        TDP7NT = 5,
        TDP7FT = 6,
        Transfer_Fiber_Rubber = 7,
        TDP1 = 8,
        TDP2 = 9,
        TDP3 = 10,
        TDP4FF = 11,
        TDP4FF_NoPI = 12,
    }
}

