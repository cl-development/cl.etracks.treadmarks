﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum FileUploadSectionName
    {
        Applications = 0,
        Claims = 1,
        Transactions = 2,
        Remittances = 3,
        Registrant = 4,
        Customer = 5,
        Product = 6,
        CompanyLogo = 7
    }
}
