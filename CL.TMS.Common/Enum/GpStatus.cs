﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum GpStatus
    {
        [Description("Queued")]
        PostedQueued,
        [Description("Posted")]
        PostedImported,
        [Description("Failed Staging")]
        FailedStaging,
        [Description("Failed")]
        FailedImport,
        [Description("On Hold")]
        Hold,
        [Description("Extracted")]
        Extract,
        [Description("Unknown")]
        StatusUnknown
    }
}
