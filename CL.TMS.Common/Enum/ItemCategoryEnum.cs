﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ItemCategoryEnum
    {
        TireType = 1,
        ProductsProduced = 2,
        RPMType = 3,
        StewardTireType = 4,
        CollectorTireType = 5,//?
        ProcessorTireType = 6,//?
        ProcessorTireType2 = 7,//?
        RPMTireType = 8,//?
    }
}
