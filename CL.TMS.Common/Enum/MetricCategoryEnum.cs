﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum MetricCategoryEnum
    {       
        [Description("Year to Date TSF Dollar")]
        YearToDateTSFDollar =101,
        [Description("Year to Date TSF Units")]
        YearToDateTSFUnits =102,
        [Description("Steward TSF Status Overview")]
        StewardTSFStatusOverview =120,
        [Description("Top Ten Stewards")]
        TopTenStewards = 130,
        [Description("Steward Payment Methods")]
        StewardPaymentMethods =140
    }
}

