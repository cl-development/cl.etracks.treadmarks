﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.DTO
{
    /// <summary>
    /// Order by Information
    /// </summary>
    public class OrderByInfo
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OrderByInfo()
        {
            OrderDirection = OrderDirection.ASC;
        }

        /// <summary>
        /// Order By field name
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// Order by direction
        /// </summary>
        public OrderDirection OrderDirection { get; set; }
    }

    public enum OrderDirection
    {
        ASC,
        DESC
    }
}
