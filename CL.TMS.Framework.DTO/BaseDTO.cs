﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.DTO
{
    /// <summary>
    /// Base DTO
    /// </summary>
    public class BaseDTO<TKey> : IObjectWithState
    {
        [Key]
        public TKey ID { get; set; }

        [Timestamp]
        public virtual Byte[] Rowversion { get; set; }

        [NotMapped]
        public bool HasRowVersion
        {
            get { return Rowversion != null; }
        }
        
        [NotMapped]
        public State State { get; set;}
    }
}
