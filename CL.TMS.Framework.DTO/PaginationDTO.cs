﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.DTO
{
    /// <summary>
    /// Pagination Data Transfer Object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class PaginationDTO<T, TKey>:BaseDTO<TKey>
        where T: BaseDTO<TKey>
    {
        #region Fields
        private List<T> dtoCollection = new List<T>();
        #endregion 

        #region Properties

        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Current page number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Total number of records in the database
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// The begining row number of the current page
        /// </summary>
        public int RowFrom
        {
            get
            {
                return ((PageNumber - 1) * PageSize) + 1;
            }
        }

        /// <summary>
        /// The ending row number of the current page
        /// </summary>
        public int RowTo
        {
            get
            {
                var count = (PageNumber * PageSize);

                return (count > TotalRecords) ? TotalRecords : count;
            }
        }

        /// <summary>
        /// Contains the total number of pages in the database
        /// </summary>
        public int TotalPages
        {
            get
            {
                //Returns one page by default
                if (PageSize < 1)
                    return 1;
                //Calculate number of pages
                return (0 == (TotalRecords % PageSize)) ? (TotalRecords / PageSize) : (TotalRecords / PageSize) + 1;
            }
        }

        /// <summary>
        /// Contains a DTO Collection
        /// </summary>
        public List<T> DTOCollection
        {
            get
            {
                return dtoCollection;
            }
            set
            {
                dtoCollection = value;
            }
        }

        public List<OrderByInfo> OrderByInfos { get; set; }
        /// <summary>
        /// Adds an item to DTO Collection
        /// </summary>
        /// <param name="t"></param>
        public void Add(T t)
        {
            if (DTOCollection == null)
                DTOCollection = new List<T>();

            DTOCollection.Add(t);
        }

        /// <summary>
        /// Removes an item from DTOCollection
        /// </summary>
        /// <param name="t"></param>
        public void Remove(T t)
        {
            if (DTOCollection != null)
            {
                DTOCollection.Remove(t);
            }
        }

        /// <summary>
        /// Clears DTOCollection
        /// </summary>
        public void Clear()
        {
            if (DTOCollection != null)
            {
                DTOCollection.Clear();
            }
        }

        #endregion
    }
}
