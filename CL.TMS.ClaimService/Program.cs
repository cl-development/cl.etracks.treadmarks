﻿using CL.TMS.Repository.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Check if it is 0:00am to 1:00am
            var now = DateTime.Now;
            if ((now.Day == 1) && IsTimeOfDayBetween(now, new TimeSpan(0, 0, 0), new TimeSpan(1, 0, 0)))
            {
                OpenClaimsForActiveVendors();
            }
        }

        private static void OpenClaimsForActiveVendors()
        {
            var claimsRepository = new ClaimsRepository();
            claimsRepository.InitializeClaimForActiveVendors();
        }

        private static bool IsTimeOfDayBetween(DateTime time, TimeSpan startTime, TimeSpan endTime)
        {
            if (endTime == startTime)
            {
                return true;
            }
            else if (endTime < startTime)
            {
                return time.TimeOfDay <= endTime ||
                    time.TimeOfDay >= startTime;
            }
            else
            {
                return time.TimeOfDay >= startTime &&
                    time.TimeOfDay <= endTime;
            }

        }
    }
}
