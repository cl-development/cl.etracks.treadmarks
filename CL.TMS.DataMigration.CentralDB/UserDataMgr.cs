﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration.CentralDB
{
    public class UserDataMgr
    {
        private LegacyCentralDBEntities dbContext;
        public UserDataMgr()
        {
            dbContext = new LegacyCentralDBEntities();
        }

        public IEnumerable<User> GetAllByRegNo(decimal registrationNumber)
        {
            return this.dbContext.Users.Where(r => r.registrationNumber == registrationNumber).OrderByDescending(r => r.userId);                         
        }
    }
}


