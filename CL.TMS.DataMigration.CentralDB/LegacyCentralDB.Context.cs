﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration.CentralDB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class LegacyCentralDBEntities : DbContext
    {
        public LegacyCentralDBEntities()
            : base("name=LegacyCentralDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Authorization> Authorizations { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Eligibility> Eligibilities { get; set; }
        public DbSet<GpsLog> GpsLogs { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<PhotoPreSelectComment> PhotoPreSelectComments { get; set; }
        public DbSet<PhotoType> PhotoTypes { get; set; }
        public DbSet<Registrant> Registrants { get; set; }
        public DbSet<RegistrantType> RegistrantTypes { get; set; }
        public DbSet<ScaleTicket> ScaleTickets { get; set; }
        public DbSet<ScaleTicketType> ScaleTicketTypes { get; set; }
        public DbSet<STCAuthorization> STCAuthorizations { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<TireType> TireTypes { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Transaction_Eligibility> Transaction_Eligibility { get; set; }
        public DbSet<Transaction_TireType> Transaction_TireType { get; set; }
        public DbSet<TransactionStatusType> TransactionStatusTypes { get; set; }
        public DbSet<TransactionSyncStatusType> TransactionSyncStatusTypes { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<TransactionType_Module> TransactionType_Module { get; set; }
        public DbSet<TransactionType_RegistrantType> TransactionType_RegistrantType { get; set; }
        public DbSet<TransactionType_TireType> TransactionType_TireType { get; set; }
        public DbSet<UnitType> UnitTypes { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
