﻿using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;

namespace CL.TMS.Repository.Common
{
    public class ItemRepository : BaseRepository<RegistrantBoundedContext, Item, int>, IItemRepository
    {
        public EntityStore<BusinessActivity, int> Activity;

        public ItemRepository()
        {
            Activity = new EntityStore<BusinessActivity, int>(context);          
        }

        public IList<Item> GetAllItems()        
        {
            var allItemsList = entityStore.All.Where(x => x.ItemParticipantTypes.Any(y => y.ParticipantTypeId == 2)).ToList();
            return AutoMapper.Mapper.Map<IList<Item>>(allItemsList);
        }
        public IList<Item> GetAllItems(int ParticipantType)
        {
            var allItemsList = entityStore.All.Where(x => x.ItemParticipantTypes.Any(y => y.ParticipantTypeId == ParticipantType)).ToList();
            return AutoMapper.Mapper.Map<IList<Item>>(allItemsList);
        }
        public IList<Item> GetAllProduct(int ItemCategory)
        {
            var allItemsList = entityStore.All.Where(x => x.ItemCategory == ItemCategory).ToList();
            return AutoMapper.Mapper.Map<IList<Item>>(allItemsList);
        }
        public IList<BusinessActivity> GetBusinessActivities(int Type)
        {
            var ac = Activity.All.Where(x => x.BusinessActivityType == Type).ToList();
            return AutoMapper.Mapper.Map<List<BusinessActivity>>(ac);
        }

    }
}
