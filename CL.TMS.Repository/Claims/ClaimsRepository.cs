﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.GroupRate;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.System;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Repository.Claims
{
    public class ClaimsRepository : BaseRepository<ClaimsBoundedContext, Claim, int>, IClaimsRepository
    {
        #region Common

        public bool HasAnyTransactionInClaim(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.ClaimDetails.Where(c => c.ClaimId == claimId).Select(c => c.TransactionId);
            return query.Count() > 0;
        }
        public PaginationDTO<StaffAllClaimsModel, Guid> LoadAllClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int routingCount, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claim in dbContext.Claims.AsNoTracking()
                        select new StaffAllClaimsModel()
                        {
                            ClaimId = claim.ID,
                            RegNumber = claim.Participant.Number,
                            Company = claim.Participant.BusinessName,
                            Period = claim.ClaimPeriod.ShortName,
                            Submitted = claim.SubmissionDate,
                            Status = claim.Status,
                            AssignedTo = claim.AssignedToUser != null ? claim.AssignedToUser.FirstName.Trim() + " " + claim.AssignedToUser.LastName.Trim() : "",
                            AssignedToUserId = claim.AssignedToUser != null ? claim.AssignedToUser.ID : 0,
                            Type = (ClaimType)claim.ClaimType,
                            RoutingCount = routingCount,
                            ClaimStartDate = claim.ClaimPeriod.StartDate,

                            ClaimOnholdDate = claim.ClaimOnHoldDate,
                            ChequeDueDate = claim.ChequeDueDate,
                            TotalClaimAmount = claim.ClaimsAmountTotal,
                            HoldStatus = (true == claim.ClaimOnhold) ? "On Hold" : (true == claim.AuditOnhold) ? "On Hold Audit" : "",
                            ClaimOnhold = claim.ClaimOnhold,
                            AuditOnhold = claim.AuditOnhold,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (((DbFunctions.TruncateTime(c.Submitted) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.Submitted) <= DbFunctions.TruncateTime(nextMonth)))
                            //|| ((DbFunctions.TruncateTime(c.PaymentDue) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.PaymentDue) <= DbFunctions.TruncateTime(nextMonth)))
                            ));
                    }
                    else
                    {
                        query = query.Where(c => ((DbFunctions.TruncateTime(c.Submitted) == DbFunctions.TruncateTime(dateValue))
                            //|| (DbFunctions.TruncateTime(c.PaymentDue) == DbFunctions.TruncateTime(dateValue))
                            ));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.RegNumber.ToString().Contains(searchText)
                                            || c.Type.ToString().ToLower().Contains(searchText)
                                            || c.Company.ToString().ToLower().Contains(searchText)
                                            || c.Period.ToString().ToLower().Contains(searchText)
                                            || SqlFunctions.StringConvert(c.TotalClaimAmount, 18, 2).ToLower().Contains(searchText) //OTSTM2-904
                                            || c.HoldStatus.ToString().ToLower().Contains(searchText)
                                            || c.AssignedTo.ToString().ToLower().Contains(searchText));
                }
            }

            //OTSTM2-904
            var columnSearchTextRegNumber = columnSearchText["RegNumber"];
            var columnSearchTextType = columnSearchText["Type"];
            var columnSearchTextCompany = columnSearchText["Company"];
            var columnSearchTextPeriod = columnSearchText["Period"];
            var columnSearchTextSubmitted = columnSearchText["Submitted"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextTotalClaimAmount = columnSearchText["TotalClaimAmount"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextHoldStatus = columnSearchText["HoldStatus"];
            var columnSearchTextAssignedTo = columnSearchText["AssignedTo"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextRegNumber))
            {
                query = query.Where(c => c.RegNumber.ToString().Contains(columnSearchTextRegNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextType))
            {
                query = query.Where(c => c.Type.ToString().Contains(columnSearchTextType));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
            {
                query = query.Where(c => c.Company.ToString().Contains(columnSearchTextCompany));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                query = query.Where(c => c.Period.ToString().Contains(columnSearchTextPeriod));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                query = query.Where(c => (c.Submitted != null ? (SqlFunctions.DatePart("yyyy", c.Submitted.Value) + "-" + DbFunctions.Right("00" + c.Submitted.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.Submitted.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextSubmitted));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextTotalClaimAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.TotalClaimAmount, 18, 2).Contains(columnSearchTextTotalClaimAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextHoldStatus))
            {
                query = query.Where(c => c.HoldStatus.ToString().Contains(columnSearchTextHoldStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextAssignedTo));
            }


            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "RegNumber" : orderBy;
            orderBy = orderBy == "Period" ? "ClaimStartDate" : orderBy;


            if (sortDirection == "asc")
                query = query.AsQueryable<StaffAllClaimsModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<StaffAllClaimsModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<StaffAllClaimsModel, Guid>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public void UpdateClaim(Claim claim)
        {
            if (claim == null) { throw new ArgumentNullException("claim"); }
            entityStore.Update(claim);
        }

        public void UpdateClaimForCalculation(Claim claim)
        {
            var dbContext = context as ClaimsBoundedContext;
            var dbClaim = dbContext.Claims.FirstOrDefault(c => c.ID == claim.ID);
            if (dbClaim != null)
            {
                dbClaim.ClaimsAmountTotal = claim.ClaimsAmountTotal;
                dbClaim.AdjustmentTotal = claim.AdjustmentTotal;
                dbClaim.TotalTax = claim.TotalTax;
                dbContext.SaveChanges();
            }
        }
        public bool IsFirstClaim(int vendorId, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var firstClaimId = dbContext.Claims.Where(c => c.ParticipantId == vendorId).OrderBy(c => c.ClaimPeriod.StartDate).Select(c => c.ID).FirstOrDefault();
            var hasSubmittedInLegacy = dbContext.Vendors.Any(c => c.ID == vendorId && (c.CHasClaimInLegacy ?? false));
            return (firstClaimId == claimId) && !hasSubmittedInLegacy;
        }

        public List<SubmitClaimTransaction> LoadClaimTransactions(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimDetails.Where(c => c.ClaimId == claimId).Select(c =>
                new SubmitClaimTransaction
                {
                    TransactionId = c.TransactionId,
                    TransactionDate = c.Transaction.TransactionDate,
                    IncomingVendorId = c.Transaction.IncomingId,
                    OutgoingVendorId = c.Transaction.OutgoingId,
                    ProcessingStatus = c.Transaction.ProcessingStatus,
                    IncomingVendorType = c.Transaction.IncomingVendor != null ? c.Transaction.IncomingVendor.VendorType : 0,
                    IncomingVendorNumber = c.Transaction.IncomingVendor != null ? c.Transaction.IncomingVendor.Number : "",
                    OutgoingVendorType = c.Transaction.OutgoingVendor != null ? c.Transaction.OutgoingVendor.VendorType : 0,
                    OutgoingVendorNumber = c.Transaction.OutgoingVendor != null ? c.Transaction.OutgoingVendor.Number : ""
                }
            ).ToList();
        }
        public Claim FindClaimByClaimId(int claimId)
        {
            if (claimId < 0) { throw new ArgumentOutOfRangeException("claimId"); }
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.Include(c => c.ClaimPeriod).Include(c => c.Participant).Include(c => c.ClaimSummary).FirstOrDefault(c => c.ID == claimId);
        }

        public Claim FindClaimByClaimIdForReport(int claimId)
        {
            if (claimId < 0) { throw new ArgumentOutOfRangeException("claimId"); }
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.AsNoTracking().Include(c => c.ClaimPeriod).Include(c => c.Participant).FirstOrDefault(c => c.ID == claimId);
        }

        public ClaimSummary GetClaimSummaryByClaimId(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimSummaries.FirstOrDefault(c => c.Claim.ID == claimId);
        }
        public StaffAllClaimsModel GetTopClaimForMe(long userId)
        {
            var claimTypeIds = new List<int>();
            if ((SecurityContextHelper.CollectorClaimsWorkflow != null) && (SecurityContextHelper.CollectorClaimsWorkflow.Workflow == TreadMarksConstants.RepresentativeWorkflow))
            {
                claimTypeIds.Add(2);
            }

            if ((SecurityContextHelper.HaulerClaimsWorkflow != null) && (SecurityContextHelper.HaulerClaimsWorkflow.Workflow == TreadMarksConstants.RepresentativeWorkflow))
            {
                claimTypeIds.Add(3);
            }

            if ((SecurityContextHelper.ProcessorClaimsWorkflow != null) && (SecurityContextHelper.ProcessorClaimsWorkflow.Workflow == TreadMarksConstants.RepresentativeWorkflow))
            {
                claimTypeIds.Add(4);
            }

            if ((SecurityContextHelper.RPMClaimsWorkflow != null) && (SecurityContextHelper.RPMClaimsWorkflow.Workflow == TreadMarksConstants.RepresentativeWorkflow))
            {
                claimTypeIds.Add(5);
            }

            //Based on user claims workflow which belongs to user
            var dbContext = context as ClaimsBoundedContext;
            string claimStatus = EnumHelper.GetEnumDescription(ClaimStatus.Submitted);
            var query = dbContext.Claims.AsNoTracking()
                .Where(c => c.Status == claimStatus && claimTypeIds.Contains(c.ClaimType))
                .OrderBy(c => c.SubmissionDate)
                .Select(c => new StaffAllClaimsModel
                {
                    ClaimId = c.ID,
                    RegNumber = c.Participant.Number,
                    Company = c.Participant.BusinessName,
                    Period = c.ClaimPeriod.ShortName,
                    Submitted = c.SubmissionDate,
                    Status = c.Status,
                    Type = (ClaimType)c.ClaimType
                });
            return query.FirstOrDefault();
        }

        public StaffAllClaimsModel FindClaimById(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.Claims.Where(c => c.ID == claimId).Select(
                    c => new StaffAllClaimsModel
                    {
                        ClaimId = c.ID,
                        RegNumber = c.Participant.Number,
                        Company = c.Participant.BusinessName,
                        Period = c.ClaimPeriod.ShortName,
                        Status = c.Status,
                        AssignedTo = c.AssignedToUser != null ? c.AssignedToUser.FirstName + " " + c.AssignedToUser.LastName : "",
                        Type = (ClaimType)c.ClaimType
                    }
                );
            return query.FirstOrDefault();
        }
        public List<StaffAllClaimsModel> GetExportDetails(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claims in dbContext.Claims.AsNoTracking()
                        join usr in dbContext.Users on claims.AssignToUserId equals usr.ID into claims_usr
                        from c_user in claims_usr.DefaultIfEmpty()
                        join vdr in dbContext.Vendors on claims.ParticipantId equals vdr.ID
                        join prd in dbContext.Periods on claims.ClaimPeriodId equals prd.ID
                        select new StaffAllClaimsModel()
                        {
                            ClaimId = claims.ID,
                            RegNumber = vdr.Number.Trim(),
                            Company = vdr.BusinessName.Trim(),
                            Period = prd.ShortName.Trim(),
                            Submitted = claims.SubmissionDate,
                            Status = claims.Status.Trim(),
                            AssignedTo = c_user.FirstName.Trim() + " " + c_user.LastName.Trim(),
                            Type = (ClaimType)claims.ClaimType,
                            RoutingCount = 1,
                            ClaimStartDate = prd.StartDate,

                            ClaimOnholdDate = claims.ClaimOnHoldDate,
                            ChequeDueDate = claims.ChequeDueDate,
                            TotalClaimAmount = claims.ClaimsAmountTotal,
                            HoldStatus = (true == claims.ClaimOnhold) ? "On Hold" : (true == claims.AuditOnhold) ? "On Hold Audit" : "",
                            ClaimOnhold = claims.ClaimOnhold,
                            AuditOnhold = claims.AuditOnhold,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (((DbFunctions.TruncateTime(c.Submitted) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.Submitted) <= DbFunctions.TruncateTime(nextMonth)))
                            //|| ((DbFunctions.TruncateTime(c.PaymentDue) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.PaymentDue) <= DbFunctions.TruncateTime(nextMonth)))
                            ));
                    }
                    else
                    {
                        query = query.Where(c => ((DbFunctions.TruncateTime(c.Submitted) == DbFunctions.TruncateTime(dateValue))
                            //|| (DbFunctions.TruncateTime(c.PaymentDue) == DbFunctions.TruncateTime(dateValue))
                            ));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.RegNumber.ToString().Contains(searchText)
                                            || c.Company.ToString().ToLower().Contains(searchText)
                                            || c.Period.ToString().ToLower().Contains(searchText)
                                            || SqlFunctions.StringConvert(c.TotalClaimAmount, 18, 2).ToLower().Contains(searchText) //OTSTM2-904
                                            || c.Status.ToString().ToLower().Contains(searchText)
                                            || c.HoldStatus.ToString().ToLower().Contains(searchText)
                                            || c.AssignedTo.ToString().ToLower().Contains(searchText)
                                            || c.Type.ToString().ToLower().Contains(searchText));
                }
            }

            //OTSTM2-904
            var columnSearchTextRegNumber = columnSearchText["RegNumber"];
            var columnSearchTextType = columnSearchText["Type"];
            var columnSearchTextCompany = columnSearchText["Company"];
            var columnSearchTextPeriod = columnSearchText["Period"];
            var columnSearchTextSubmitted = columnSearchText["Submitted"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextTotalClaimAmount = columnSearchText["TotalClaimAmount"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextHoldStatus = columnSearchText["HoldStatus"];
            var columnSearchTextAssignedTo = columnSearchText["AssignedTo"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextRegNumber))
            {
                query = query.Where(c => c.RegNumber.ToString().Contains(columnSearchTextRegNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextType))
            {
                query = query.Where(c => c.Type.ToString().Contains(columnSearchTextType));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
            {
                query = query.Where(c => c.Company.ToString().Contains(columnSearchTextCompany));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                query = query.Where(c => c.Period.ToString().Contains(columnSearchTextPeriod));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                query = query.Where(c => (c.Submitted != null ? (SqlFunctions.DatePart("yyyy", c.Submitted.Value) + "-" + DbFunctions.Right("00" + c.Submitted.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.Submitted.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextSubmitted));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextTotalClaimAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.TotalClaimAmount, 18, 2).Contains(columnSearchTextTotalClaimAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextHoldStatus))
            {
                query = query.Where(c => c.HoldStatus.ToString().Contains(columnSearchTextHoldStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextAssignedTo));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "RegNumber" : orderBy;

            if (orderBy == "Period")
                orderBy = "ClaimStartDate";

            //OTSTM2-736
            if (orderBy == "PaymentDue")
                orderBy = "ChequeDueDate";

            if (sortDirection == "asc")
                query = query.AsQueryable<StaffAllClaimsModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<StaffAllClaimsModel>().OrderBy(orderBy + " descending");

            return query.ToList();
        }

        public void InitializeClaimForActiveVendors()
        {
            using (var conn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_AddClaimForVendors";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@periodDate", DateTime.Now.Date));

                    //Set command time out to 5 minutes to open claims for all active vendors
                    cmd.CommandTimeout = 300;

                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void InitializeClaimForVendor(int vendorId)
        {
            var claimContext = context as ClaimsBoundedContext;
            var vendor = claimContext.Vendors.FirstOrDefault(c => c.ID == vendorId && c.IsActive);
            if (vendor != null)
            {
                //Get date portion only
                var today = DateTime.Now.Date;
                var claimPeriodId = claimContext.Periods.FirstOrDefault(a =>
                                          a.StartDate <= today && a.EndDate >= today &&
                                          a.PeriodType == vendor.VendorType).ID;
                var dbClaim = claimContext.Claims.FirstOrDefault(c => c.ParticipantId == vendorId && c.ClaimPeriodId == claimPeriodId);
                if (dbClaim == null)
                {
                    var primaryAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                    if (primaryAddress != null)
                    {
                        var claim = new Claim
                        {
                            ClaimPeriodId = claimPeriodId,
                            ParticipantId = vendorId,
                            ParticipantAddressID = primaryAddress.ID,
                            Status = ClaimStatus.Open.ToString(),
                            ClaimType = vendor.VendorType,
                            IsTaxApplicable = true
                        };
                        claimContext.Claims.Add(claim);
                        claimContext.SaveChanges();
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Initialize3ClaimsForProcessorRMP(int vendorId, DateTime activeStateChangeDate)//created for OTSTM2-628
        {
            var claimContext = context as ClaimsBoundedContext;
            var vendor = claimContext.Vendors.FirstOrDefault(c => c.ID == vendorId);

            if (vendor != null)
            {
                //Get date portion only
                //var today = DateTime.Now.Date;
                DateTime activeStateChange90Days = activeStateChangeDate.AddDays(90);
                DateTime activeStateChange3Month = activeStateChangeDate.AddMonths(3);
                List<Claim> newClaims = new List<Claim>();
                while (activeStateChangeDate <= activeStateChange3Month)
                {
                    var claimPeriodId = claimContext.Periods.FirstOrDefault(a =>
                                              a.StartDate <= activeStateChangeDate && a.EndDate >= activeStateChangeDate &&
                                              a.PeriodType == vendor.VendorType).ID;
                    var dbClaim = claimContext.Claims.FirstOrDefault(c => c.ParticipantId == vendorId && c.ClaimPeriodId == claimPeriodId);
                    if (dbClaim == null)
                    {
                        var primaryAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                        if (primaryAddress != null)
                        {
                            var claim = new Claim
                            {
                                ClaimPeriodId = claimPeriodId,
                                ParticipantId = vendorId,
                                ParticipantAddressID = primaryAddress.ID,
                                Status = ClaimStatus.Open.ToString(),
                                ClaimType = vendor.VendorType,
                                IsTaxApplicable = true
                            };
                            newClaims.Add(claim);
                        }
                    }
                    activeStateChangeDate = activeStateChangeDate.AddMonths(1);
                }

                if (newClaims.Count > 0)
                {
                    claimContext.Claims.AddRange(newClaims);
                    claimContext.SaveChanges();
                }
            }
        }

        public Claim GetClaimForTransaction(int vendorId, DateTime transactionDate, bool forStaff)
        {
            var claimContext = context as ClaimsBoundedContext;
            var query = from claim in claimContext.Claims
                        join claimPeriod in claimContext.Periods on claim.ClaimPeriodId equals claimPeriod.ID
                        where claim.ParticipantId == vendorId &&
                              ((forStaff && (claim.Status == "Open" || claim.Status == "Submitted" || claim.Status == "Under Review")) || (!forStaff && claim.Status == "Open")) &&
                             DbFunctions.TruncateTime(claimPeriod.StartDate) <= transactionDate.Date && DbFunctions.TruncateTime(claimPeriod.EndDate) >= transactionDate.Date
                        select claim;
            return query.Include(c => c.ClaimPeriod).OrderBy(c => c.ClaimPeriod.StartDate).FirstOrDefault();
        }

        //OTSTM2-1313
        public Claim GetClaimForDORTransaction(int transactionId)
        {
            var claimContext = context as ClaimsBoundedContext;
            var query = from claimDetail in claimContext.ClaimDetails
                        join claim in claimContext.Claims on claimDetail.ClaimId equals claim.ID
                        join transaction in claimContext.Transactions on claimDetail.TransactionId equals transaction.ID
                        where transaction.ID == transactionId
                        select claim;
            return query.Include(c => c.ClaimPeriod).OrderBy(c => c.ClaimPeriod.StartDate).FirstOrDefault();
        }

        public Claim GetCurrentClaim(int vendorId, DateTime transactionDate)
        {
            var claimContext = context as ClaimsBoundedContext;
            var query = from claim in claimContext.Claims
                        join claimPeriod in claimContext.Periods on claim.ClaimPeriodId equals claimPeriod.ID
                        where claim.ParticipantId == vendorId && DbFunctions.TruncateTime(claimPeriod.StartDate) <= transactionDate.Date && DbFunctions.TruncateTime(claimPeriod.EndDate) >= transactionDate.Date
                        select claim;
            return query.Include(c => c.ClaimPeriod).FirstOrDefault();
        }

        public Claim GetNextAvailableOpenClaimForTransaction(int vendorId, DateTime transactionDate, bool forStaff)
        {
            var claimContext = context as ClaimsBoundedContext;
            var query = from claim in claimContext.Claims
                        join claimPeriod in claimContext.Periods on claim.ClaimPeriodId equals claimPeriod.ID
                        where claim.ParticipantId == vendorId && DbFunctions.TruncateTime(claimPeriod.StartDate) > transactionDate.Date &&
                        ((forStaff && (claim.Status == "Open" || claim.Status == "Submitted" || claim.Status == "Under Review")) || (!forStaff && claim.Status == "Open"))
                        orderby claimPeriod.StartDate ascending
                        select claim;
            return query.Include(c => c.ClaimPeriod).OrderBy(c => c.ClaimPeriod.StartDate).FirstOrDefault();
        }

        public void AddClaimDetail(ClaimDetail claimDetail)
        {
            var claimContext = context as ClaimsBoundedContext;
            claimContext.ClaimDetails.Add(claimDetail);
            claimContext.SaveChanges();
        }

        public List<int> RemoveClaimDetail(int transactionId)
        {
            var claimContext = context as ClaimsBoundedContext;

            var claimDetails = claimContext.ClaimDetails.Where(c => c.TransactionId == transactionId).ToList();
            if (claimDetails.Count > 0)
            {
                claimContext.ClaimDetails.RemoveRange(claimDetails);
                claimContext.SaveChanges();
            }

            return claimDetails.Select(c => c.ClaimId).ToList();
        }

        //OTSTM2-81
        public void AddClaimDetailRange(List<ClaimDetail> claimDetailList)
        {
            var claimContext = context as ClaimsBoundedContext;
            claimContext.ClaimDetails.AddRange(claimDetailList);
            claimContext.SaveChanges();
        }

        public Period GetClaimPeriod(int claimId)
        {
            var claimContext = context as ClaimsBoundedContext;
            var query = from period in claimContext.Periods
                        join claim in claimContext.Claims on period.ID equals claim.ClaimPeriodId
                        where claim.ID == claimId
                        select period;
            return query.FirstOrDefault();
        }

        public bool IsTransactionInSameClaim(int claimId, int transactionId)
        {
            return (context as ClaimsBoundedContext).ClaimDetails.Any(c => c.ClaimId == claimId && c.TransactionId == transactionId);
        }

        public bool IsClaimOpen(int claimId)
        {
            return (context as ClaimsBoundedContext).Claims.Any(c => c.ID == claimId && c.Status == "Open");
        }
        public bool IsClaimAllowAddNewTransaction(int claimId)
        {
            return (context as ClaimsBoundedContext).Claims.Any(c => c.ID == claimId && ((c.Status == "Open")
                || (c.Status == "Submitted") || (c.Status == "Under Review") || (c.Status == "Hold") || (c.Status == "Audit Review")
                || (c.Status == "Audit Hold")));
        }
        public TransactionClaimAssociationDetailViewModel GetTransactionClaimAssociationDetailDataModel(int transactionId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var associationList = dbContext.ClaimDetails.Where(i => i.TransactionId == transactionId).Select(i => new TransactionClaimAssociationViewModel()
            {
                ClaimId = i.ClaimId,
                ClaimPeriodName = i.Claim.ClaimPeriod.ShortName,
                Period = new TransactionPeriodViewModel() { StartDate = i.Claim.ClaimPeriod.StartDate, EndDate = i.Claim.ClaimPeriod.EndDate },
                Status = i.Claim.Status,
                ParticipantID = i.Claim.ParticipantId,
                ContactEmail = i.Claim.Participant.VendorAddresses.FirstOrDefault(add => add.AddressType == 3).Email
            }).ToList();

            associationList.ForEach(i =>
            {
                var addressList = dbContext.ClaimDetails.Where(cd => cd.ClaimId == i.ClaimId).Select(cd => cd.Claim.Participant.VendorAddresses).First();
                var primaryContact = RepositoryHelper.FindPrimaryContact(addressList.ToList());
                i.ContactEmail = primaryContact.Email;
            });

            var associationDataModel = new TransactionClaimAssociationDetailViewModel()
            {
                TransactionId = transactionId,
                Associations = new List<TransactionClaimAssociationViewModel>()
            };
            associationDataModel.Associations.AddRange(associationList);

            return associationDataModel;
        }

        public List<TransactionSearchListViewModel> GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType, int direction, int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claimDetail in dbContext.ClaimDetails
                        join transaction in dbContext.Transactions on claimDetail.TransactionId equals transaction.ID
                        where claimDetail.ClaimId == claimId
                        select transaction;

            if (direction == 1) query = query.Where(c => c.IncomingId == vendorId);
            if (direction == 2) query = query.Where(c => c.OutgoingId == vendorId);

            if (!string.IsNullOrWhiteSpace(transactionFormat) && transactionFormat != "Both")
            {
                if (transactionFormat == "Mobile") query = query.Where(c => c.MobileFormat == true);
                if (transactionFormat == "Paper") query = query.Where(c => c.MobileFormat == false);
            }

            if (!string.IsNullOrWhiteSpace(processingStatus) && processingStatus != "All")
            {
                //query = query.Where(c => c.ProcessingStatus == processingStatus);
                if (processingStatus == "Adjusted")
                {
                    query = query.Where(c => c.TransactionAdjustments.Any());
                }
                else
                {
                    query = query.Where(c => c.ProcessingStatus == processingStatus);
                }
            }

            if (!string.IsNullOrWhiteSpace(transactionType) && transactionType != "All")
            {
                query = query.Where(c => c.TransactionTypeCode == transactionType);
            }

            return query.Select(c => new TransactionSearchListViewModel()
            {
                Id = c.ID,
                TransactionId = c.TransactionId,
                TypeCode = c.TransactionTypeCode,
                FriendlyNumber = c.FriendlyId,
                ProcessingStatus = string.IsNullOrEmpty(c.ProcessingStatus) ? "Unknown" : c.ProcessingStatus
            }).ToList();
        }

        public string GetClaimStatus(int claimId)
        {
            return entityStore.All.Where(c => c.ID == claimId).Select(c => c.Status).FirstOrDefault();
        }
        public string GetClaimStatusByTransactionId(int transactionId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails
                        where claimdetail.TransactionId == transactionId
                        select claimdetail.Claim.Status;
            return query.FirstOrDefault();
        }
        public Dictionary<int, string> GetClaimStatusforTransaction()
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails
                        select new { TransactionId = claimdetail.TransactionId, status = claimdetail.Claim.Status };

            return query.GroupBy(a => a.TransactionId).Select(g => g.FirstOrDefault()).ToDictionary(item => item.TransactionId, item => item.status);
        }

        public List<InventoryItem> LoadPreviousApprovedClaimInventories(int vendorId, DateTime claimEndDate)
        {
            var dbContext = context as ClaimsBoundedContext;

            var inventoryList = new List<InventoryItem>();
            var previousApprovedClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && c.ClaimPeriod.EndDate < claimEndDate && (c.Status == "Approved" || c.Status == "Receive Payment")).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousApprovedClaim != null)
            {
                //Load inventory item from previous approved claim inventory
                var claimInvenotryQuery = from claimInventory in dbContext.ClaimInventories.AsNoTracking()
                                          where claimInventory.ClaimId == previousApprovedClaim.ID
                                          select new InventoryItem
                                          {
                                              ItemId = claimInventory.ItemId,
                                              Direction = true,
                                              Qty = claimInventory.Qty,
                                              Weight = claimInventory.Weight,
                                              ActualWeight = claimInventory.ActualWeight
                                          };
                inventoryList.AddRange(claimInvenotryQuery.ToList());
            }
            return inventoryList;
        }

        public List<InventoryItem> LoadClaimInventoryItems(int vendorId, DateTime claimEndDate)
        {
            var dbContext = context as ClaimsBoundedContext;

            var inventoryList = new List<InventoryItem>();
            //Load previous approved claim including the current claim if it is approved
            var previousApprovedClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimEndDate.Date && (c.Status == "Approved" || c.Status == "Receive Payment")).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousApprovedClaim != null)
            {
                //Load inventory item from previous approved claim inventory
                var claimInvenotryQuery = from claimInventory in dbContext.ClaimInventories.AsNoTracking()
                                          where claimInventory.ClaimId == previousApprovedClaim.ID
                                          select new InventoryItem
                                          {
                                              ItemId = claimInventory.ItemId,
                                              Direction = true,
                                              Qty = claimInventory.Qty,
                                              Weight = claimInventory.Weight,
                                              ActualWeight = claimInventory.ActualWeight
                                          };
                inventoryList.AddRange(claimInvenotryQuery.ToList());
            }

            //Load all open claims up to the current claim
            var transactionQuery = from claimDetail in dbContext.ClaimDetails.AsNoTracking()
                                   join transaction in dbContext.Transactions on claimDetail.TransactionId equals transaction.ID
                                   where claimDetail.Claim.ParticipantId == vendorId && DbFunctions.TruncateTime(claimDetail.Claim.ClaimPeriod.StartDate) < claimEndDate.Date && claimDetail.Claim.Status != "Approved" && claimDetail.Claim.Status != "Receive Payment"
                                   && claimDetail.Transaction.ProcessingStatus != "Invalidated" && claimDetail.Transaction.Status != "Pending"
                                   select new
                                   {
                                       Direction = claimDetail.Direction,
                                       TransationItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null),
                                       Transaction = transaction,
                                       TransactionAdjustments = transaction.TransactionAdjustments
                                   };
            transactionQuery.ToList().ForEach(c =>
            {
                //Apply transaction adjustment
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    transactionAdjustment.TransactionItems.ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var item = new InventoryItem();
                            item.ItemId = i.ItemID;
                            item.Direction = c.Direction;
                            item.Qty = (int)i.Quantity;
                            item.Weight = i.AverageWeight;
                            item.ActualWeight = i.ActualWeight;
                            inventoryList.Add(item);
                        }
                    });
                }
                else
                {
                    c.TransationItems.ToList().ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var item = new InventoryItem();
                            item.ItemId = i.ItemID;
                            item.Direction = c.Direction;
                            item.Qty = (int)i.Quantity;
                            item.Weight = i.AverageWeight;
                            item.ActualWeight = i.ActualWeight;
                            inventoryList.Add(item);
                        }
                    });
                }
            });

            //Load tire count adjustment for all open claims up to the current claim
            var claimInventoryAdjustQuery = from claimInventoryAdjust in dbContext.ClaimInventoryAdjustments
                                            where claimInventoryAdjust.Claim.ParticipantId == vendorId
                                                 && claimInventoryAdjust.Claim.ClaimPeriod.StartDate < claimEndDate
                                                 && claimInventoryAdjust.Claim.Status != "Approved"
                                                 && claimInventoryAdjust.Claim.Status != "Receive Payment"
                                                 && (claimInventoryAdjust.AdjustmentType == 2 || claimInventoryAdjust.AdjustmentType == 3)
                                            select new
                                            {
                                                Direction = claimInventoryAdjust.Direction,
                                                InventoryAdjustItems = claimInventoryAdjust.ClaimInventoryAdjustItems
                                            };
            claimInventoryAdjustQuery.ToList().ForEach(c =>
            {
                c.InventoryAdjustItems.ToList().ForEach(i =>
                {
                    var item = new InventoryItem();
                    item.ItemId = i.ItemId;
                    item.Direction = c.Direction != 2;
                    item.Qty = i.QtyAdjustment;
                    item.Weight = i.WeightAdjustment;
                    item.ActualWeight = i.WeightAdjustment;
                    item.IsAdjustedItem = true;
                    inventoryList.Add(item);
                });
            });

            return inventoryList;

        }

        public List<InventoryItem> LoadAllClaimInventoryItems(int vendorId, DateTime claimEndDate)
        {
            var inventoryList = new List<InventoryItem>();

            var dbContext = context as ClaimsBoundedContext;

            //Load all claims up to the current claim
            var transactionQuery = from claimDetail in dbContext.ClaimDetails.AsNoTracking()
                                   join transaction in dbContext.Transactions on claimDetail.TransactionId equals transaction.ID
                                   where claimDetail.Claim.ParticipantId == vendorId && DbFunctions.TruncateTime(claimDetail.Claim.ClaimPeriod.StartDate) < claimEndDate.Date
                                   && claimDetail.Transaction.ProcessingStatus != "Invalidated" && claimDetail.Transaction.Status != "Pending"
                                   select new
                                   {
                                       Direction = claimDetail.Direction,
                                       TransationItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null),
                                       Transaction = transaction,
                                       TransactionAdjustments = transaction.TransactionAdjustments
                                   };
            transactionQuery.ToList().ForEach(c =>
            {
                //Apply transaction adjustment
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    transactionAdjustment.TransactionItems.ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var item = new InventoryItem();
                            item.ItemId = i.ItemID;
                            item.Direction = c.Direction;
                            item.Qty = (int)i.Quantity;
                            item.Weight = i.AverageWeight;
                            item.ActualWeight = i.ActualWeight;
                            inventoryList.Add(item);
                        }
                    });
                }
                else
                {
                    c.TransationItems.ToList().ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var item = new InventoryItem();
                            item.ItemId = i.ItemID;
                            item.Direction = c.Direction;
                            item.Qty = (int)i.Quantity;
                            item.Weight = i.AverageWeight;
                            item.ActualWeight = i.ActualWeight;
                            inventoryList.Add(item);
                        }
                    });
                }
            });

            //Load tire count adjustment for all open claims up to the current claim
            var claimInventoryAdjustQuery = from claimInventoryAdjust in dbContext.ClaimInventoryAdjustments
                                            where claimInventoryAdjust.Claim.ParticipantId == vendorId
                                                 && DbFunctions.TruncateTime(claimInventoryAdjust.Claim.ClaimPeriod.StartDate) < claimEndDate.Date
                                                 && (claimInventoryAdjust.AdjustmentType == 2 || claimInventoryAdjust.AdjustmentType == 3)
                                            select new
                                            {
                                                Direction = claimInventoryAdjust.Direction,
                                                InventoryAdjustItems = claimInventoryAdjust.ClaimInventoryAdjustItems
                                            };
            claimInventoryAdjustQuery.ToList().ForEach(c =>
            {
                c.InventoryAdjustItems.ToList().ForEach(i =>
                {
                    var item = new InventoryItem();
                    item.ItemId = i.ItemId;
                    item.Direction = c.Direction != 2;
                    item.Qty = i.QtyAdjustment;
                    item.Weight = i.WeightAdjustment;
                    item.ActualWeight = i.WeightAdjustment;
                    inventoryList.Add(item);
                });
            });

            return inventoryList;
        }

        public List<InventoryItem> LoadClaimItems(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            //Load claim inventory adjustment item
            var inventoryAdjustQuery = from inventoryAdjustItem in dbContext.ClaimInventoryAdjustItems
                                       where inventoryAdjustItem.ClaimInventoryAdjustment.ClaimId == claimId
                                       select new InventoryItem
                                       {
                                           ItemId = inventoryAdjustItem.ItemId,
                                           IsEligible = inventoryAdjustItem.ClaimInventoryAdjustment.IsEligible,
                                           Direction = (inventoryAdjustItem.ClaimInventoryAdjustment.Direction == 0 || inventoryAdjustItem.ClaimInventoryAdjustment.Direction == 1),
                                           Qty = inventoryAdjustItem.QtyAdjustment,
                                           Weight = inventoryAdjustItem.WeightAdjustment,
                                           ActualWeight = inventoryAdjustItem.WeightAdjustment
                                       };
            var inventoryAdjustList = inventoryAdjustQuery.ToList();

            //Load claim items 
            var claimQuery = from claimDetail in dbContext.ClaimDetails
                             where claimDetail.Claim.ID == claimId && claimDetail.Transaction.ProcessingStatus != "Invalidated" && claimDetail.Transaction.Status != "Pending"
                             select new
                             {
                                 Direction = claimDetail.Direction,
                                 Transaction = claimDetail.Transaction,
                                 TransactionAdjustments = claimDetail.Transaction.TransactionAdjustments
                             };

            claimQuery.ToList().ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    transactionAdjustment.TransactionItems.ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var inventoryItem = new InventoryItem
                            {
                                ItemId = i.ItemID,
                                IsEligible = transactionAdjustment.IsEligible,
                                Direction = c.Direction,
                                Qty = (int)i.Quantity,
                                Weight = i.AverageWeight,
                                ActualWeight = i.ActualWeight

                            };
                            inventoryAdjustList.Add(inventoryItem);
                        }
                    });
                }
                else
                {
                    c.Transaction.TransactionItems.Where(q => q.TransactionAdjustmentId == null).ToList().ForEach(i =>
                    {
                        if ((i.Quantity != null) && (i.Quantity > 0))
                        {
                            var inventoryItem = new InventoryItem
                            {
                                ItemId = i.ItemID,
                                IsEligible = c.Transaction.IsEligible,
                                Direction = c.Direction,
                                Qty = (int)i.Quantity,
                                Weight = i.AverageWeight,
                                ActualWeight = i.ActualWeight

                            };
                            inventoryAdjustList.Add(inventoryItem);
                        }
                    });
                }
            });

            return inventoryAdjustList;
        }

        public List<InventoryItem> LoadHaulerClaimItems(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            //Load claim inventory adjustment item
            var inventoryAdjustQuery = from inventoryAdjustItem in dbContext.ClaimInventoryAdjustItems.AsNoTracking()
                                       where inventoryAdjustItem.ClaimInventoryAdjustment.ClaimId == claimId
                                       select new InventoryItem
                                       {
                                           ItemId = inventoryAdjustItem.ItemId,
                                           IsEligible = inventoryAdjustItem.ClaimInventoryAdjustment.IsEligible,
                                           Direction = (inventoryAdjustItem.ClaimInventoryAdjustment.Direction == 0 || inventoryAdjustItem.ClaimInventoryAdjustment.Direction == 1),
                                           Qty = inventoryAdjustItem.QtyAdjustment,
                                           Weight = inventoryAdjustItem.WeightAdjustment,
                                           ActualWeight = inventoryAdjustItem.WeightAdjustment
                                       };
            var inventoryAdjustList = inventoryAdjustQuery.ToList();

            //Load claim items 
            var claimQuery = from claimDetail in dbContext.ClaimDetails.AsNoTracking()
                             where claimDetail.Claim.ID == claimId && claimDetail.Transaction.ProcessingStatus != "Invalidated" && claimDetail.Transaction.Status != "Pending"
                             select new
                             {
                                 Direction = claimDetail.Direction,
                                 Transaction = claimDetail.Transaction,
                                 TransactionItems = claimDetail.Transaction.TransactionItems,
                                 TransactionAdjustments = claimDetail.Transaction.TransactionAdjustments
                             };

            claimQuery.ToList().ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    transactionAdjustment.TransactionItems.ForEach(i =>
                    {
                        var inventoryItem = new InventoryItem
                        {
                            ItemId = i.ItemID,
                            IsEligible = (transactionAdjustment.Transaction.TransactionTypeCode == "TCR" || transactionAdjustment.Transaction.TransactionTypeCode == "DOT" || transactionAdjustment.Transaction.TransactionTypeCode == "STC" || transactionAdjustment.Transaction.TransactionTypeCode == "PTR"),
                            Direction = c.Direction,
                            Qty = i.Quantity != null ? (int)i.Quantity : 0,
                            Weight = i.AverageWeight,
                            ActualWeight = i.ActualWeight

                        };
                        inventoryAdjustList.Add(inventoryItem);
                    });
                }
                else
                {
                    c.TransactionItems.Where(q => q.TransactionAdjustmentId == null).ToList().ForEach(i =>
                    {
                        var inventoryItem = new InventoryItem
                        {
                            ItemId = i.ItemID,
                            IsEligible = (c.Transaction.TransactionTypeCode == "TCR" || c.Transaction.TransactionTypeCode == "DOT" || c.Transaction.TransactionTypeCode == "STC" || c.Transaction.TransactionTypeCode == "PTR"),
                            Direction = c.Direction,
                            Qty = i.Quantity != null ? (int)i.Quantity : 0,
                            Weight = i.AverageWeight,
                            ActualWeight = i.ActualWeight

                        };
                        inventoryAdjustList.Add(inventoryItem);
                    });
                }
            });

            return inventoryAdjustList;
        }

        public void UpdateVendorInventory(List<Inventory> inventoryItems, int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var inventories = dbContext.Inventories.Where(c => c.VendorId == vendorId).ToList();
            inventoryItems.ForEach(c =>
            {
                var dbItem = inventoryItems.FirstOrDefault(i => i.ItemId == c.ItemId && i.IsEligible == c.IsEligible);
                if (dbItem != null)
                {
                    //Upddate
                    dbItem.Qty = dbItem.Qty + c.Qty;
                    dbItem.Weight = dbItem.Weight + c.Weight;
                    dbItem.ActualWeight = dbItem.ActualWeight + c.ActualWeight;
                    dbItem.UpdatedBy = c.UpdatedBy;
                    dbItem.UpdatedDate = c.UpdatedDate;
                }
                else
                {
                    //insert
                    inventories.Add(c);
                }
            });
            dbContext.SaveChanges();
        }

        public void RecalculateClaimInventory(List<ClaimInventory> claimInventoryItems, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var dbClaimInventories = dbContext.ClaimInventories.Where(c => c.ClaimId == claimId).ToList();
            dbContext.ClaimInventories.RemoveRange(dbClaimInventories);
            dbContext.ClaimInventories.AddRange(claimInventoryItems);
            dbContext.SaveChanges();
        }

        public void RollbackVendorInventory(List<Inventory> inventoryItems, int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var inventories = dbContext.Inventories.Where(c => c.VendorId == vendorId).ToList();
            inventoryItems.ForEach(c =>
            {
                var dbItem = inventoryItems.FirstOrDefault(i => i.ItemId == c.ItemId && i.IsEligible == c.IsEligible);
                if (dbItem != null)
                {
                    //Upddate
                    dbItem.Qty = dbItem.Qty - c.Qty;
                    dbItem.Weight = dbItem.Weight - c.Weight;
                    dbItem.ActualWeight = dbItem.ActualWeight - c.ActualWeight;
                    dbItem.UpdatedBy = c.UpdatedBy;
                    dbItem.UpdatedDate = c.UpdatedDate;
                }
            });
            dbContext.SaveChanges();
        }
        public void AddInventoryAdjustment(ClaimInventoryAdjustment inventoryAdjustment)
        {
            var dbContext = context as ClaimsBoundedContext;
            dbContext.ClaimInventoryAdjustments.Add(inventoryAdjustment);
            dbContext.SaveChanges();
        }
        //OTSTM2-270
        public void AddInternalAdjustmentNote(ClaimInternalAdjustmentNote note)
        {
            var dbContext = context as ClaimsBoundedContext;
            dbContext.ClaimInternalAdjustmentNotes.Add(note);
            dbContext.SaveChanges();
        }
        //OTSTM2-270 Load Notes for Inventory
        public List<ClaimInternalAdjustmentNote> LoadClaimInternalNotesForInventory(int ClaimInternalAdjustmentId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.ClaimInternalAdjustmentNotes.AsNoTracking().Include(c => c.AddedBy).Where(cl => cl.ClaimInternalAdjustmentId == ClaimInternalAdjustmentId);
            return query.ToList();
        }
        //OTSTM2-270 Load Notes for Inventory
        public List<ClaimInternalAdjustmentNote> LoadClaimInternalNotesForPayment(int ClaimInternalPaymentAdjustId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.ClaimInternalAdjustmentNotes.AsNoTracking().Include(c => c.AddedBy).Where(cl => cl.ClaimInternalPaymentAdjustId == ClaimInternalPaymentAdjustId);
            return query.ToList();
        }

        //OTSTM2-270 Export Notes for Inventory
        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            var dbContext = context as ClaimsBoundedContext;

            IQueryable<InternalNoteViewModel> query = null;

            if (ClaimInternalAdjustmentId.HasValue)
            {
                //internal adjustment
                query = dbContext.ClaimInternalAdjustmentNotes.AsNoTracking().Where(cl => cl.ClaimInternalAdjustmentId == ClaimInternalAdjustmentId)
                            .Join(dbContext.Users.AsNoTracking(), notes => notes.UserId, user => user.ID, (notes, user) => new InternalNoteViewModel
                            {
                                Note = notes.Note,
                                AddedOn = notes.CreatedDate,
                                AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                            });
            }
            else if (ClaimInternalPaymentAdjustId.HasValue)
            {
                //internal adjustment
                query = dbContext.ClaimInternalAdjustmentNotes.AsNoTracking().Where(cl => cl.ClaimInternalPaymentAdjustId == ClaimInternalPaymentAdjustId)
                            .Join(dbContext.Users.AsNoTracking(), notes => notes.UserId, user => user.ID, (notes, user) => new InternalNoteViewModel
                            {
                                Note = notes.Note,
                                AddedOn = notes.CreatedDate,
                                AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                            });
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    query = query.Where(c => c.Note.ToLower().Contains(searchText.ToLower()) || c.AddedBy.ToLower().Contains(searchText.ToLower()));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;

            if (!sortReverse)
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn);
            }
            else
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn + " descending");
            }

            return query.ToList();
        }

        public void AddInternalPaymentAdjustment(ClaimInternalPaymentAdjust claimInternalPaymentAdjust)
        {
            var dbContext = context as ClaimsBoundedContext;
            dbContext.ClaimInternalPaymentAdjusts.Add(claimInternalPaymentAdjust);
            dbContext.SaveChanges();
        }

        public void RemoveInternalAdjustment(int adjustmentType, int internalAdjustmentId)
        {
            var dbContext = context as ClaimsBoundedContext;
            if (adjustmentType == 4)
            {
                //Payment adjustment
                var paymentAdjust = dbContext.ClaimInternalPaymentAdjusts.Where(c => c.ID == internalAdjustmentId).FirstOrDefault();

                //OTSTM2-270 remove all associated internal notes
                var allInternalNotes = dbContext.ClaimInternalAdjustmentNotes.Where(c => c.ClaimInternalPaymentAdjustId == paymentAdjust.ID).ToList();
                if (allInternalNotes.Count > 0)
                {
                    dbContext.ClaimInternalAdjustmentNotes.RemoveRange(allInternalNotes);
                }

                if (paymentAdjust != null)
                {
                    dbContext.ClaimInternalPaymentAdjusts.Remove(paymentAdjust);
                    dbContext.SaveChanges();
                }
            }
            else
            {
                var adjustItems = dbContext.ClaimInventoryAdjustItems.Where(c => c.ClaimInventoryAdjustmentId == internalAdjustmentId).ToList();
                if (adjustItems.Count > 0)
                {
                    dbContext.ClaimInventoryAdjustItems.RemoveRange(adjustItems);
                }

                //OTSTM2-270 remove all associated internal notes
                var allInternalNotes = dbContext.ClaimInternalAdjustmentNotes.Where(c => c.ClaimInternalAdjustmentId == internalAdjustmentId).ToList();
                if (allInternalNotes.Count > 0)
                {
                    dbContext.ClaimInternalAdjustmentNotes.RemoveRange(allInternalNotes);
                }

                var inventoryAdjust = dbContext.ClaimInventoryAdjustments.Where(c => c.ID == internalAdjustmentId).FirstOrDefault();
                if (inventoryAdjust != null)
                {
                    dbContext.ClaimInventoryAdjustments.Remove(inventoryAdjust);
                    dbContext.SaveChanges();
                }
            }
        }

        public ClaimInventoryAdjustment GetClaimInventoryAdjustment(int claimInternalAdjustmentId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimInventoryAdjustments.Where(c => c.ID == claimInternalAdjustmentId).Include(c => c.ClaimInventoryAdjustItems).FirstOrDefault();
        }
        public ClaimInternalPaymentAdjust GetClaimInternalPaymentAdjust(int claimInternalPaymentAdjustId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimInternalPaymentAdjusts.FirstOrDefault(c => c.ID == claimInternalPaymentAdjustId);
        }

        public void UpdateInternalPaymentAdjust(int claimInternalAdjustId, ClaimAdjustmentModalResult modalResult, long editUserId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var editAdjust = dbContext.ClaimInternalPaymentAdjusts.FirstOrDefault(c => c.ID == claimInternalAdjustId);
            editAdjust.AdjustBy = editUserId;
            editAdjust.AdjustDate = DateTime.UtcNow;
            editAdjust.AdjustmentAmount = modalResult.Amount;
            editAdjust.PaymentType = (int)EnumHelper.ToEnum<ClaimPaymentType>(modalResult.PaymentType);
            dbContext.SaveChanges();
        }
        public void UpdateInventoryPaymentAdjust(int claimInternalAdjustId, ClaimInventoryAdjustment editAdjust)
        {
            var dbContext = context as ClaimsBoundedContext;
            var dbAdjust = dbContext.ClaimInventoryAdjustments.Where(c => c.ID == claimInternalAdjustId).Include(c => c.ClaimInventoryAdjustItems).FirstOrDefault();
            if (dbAdjust.ClaimInventoryAdjustItems.Count > 0)
            {
                dbContext.ClaimInventoryAdjustItems.RemoveRange(dbAdjust.ClaimInventoryAdjustItems);
            }
            dbAdjust.IsEligible = editAdjust.IsEligible;
            dbAdjust.Direction = editAdjust.Direction;
            dbAdjust.AdjustBy = editAdjust.AdjustBy;
            dbAdjust.AdjustDate = editAdjust.AdjustDate;
            dbAdjust.AdjustmentWeightOnroad = editAdjust.AdjustmentWeightOnroad;
            dbAdjust.AdjustmentWeightOffroad = editAdjust.AdjustmentWeightOffroad;
            editAdjust.ClaimInventoryAdjustItems.ToList().ForEach(c =>
            {
                dbAdjust.ClaimInventoryAdjustItems.Add(c);
            });

            dbContext.SaveChanges();
        }

        public void InitializeReviewStartDate(int claimId, DateTime reviewStartDate)
        {
            var claim = entityStore.FindById(claimId);
            claim.ReviewStartDate = reviewStartDate;
            entityStore.Update(claim, c => c.ReviewStartDate);
        }

        public ClaimStatusViewModel LoadClaimStatusViewModel(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        where claim.ID == claimId
                        select new ClaimStatusViewModel
                        {
                            ClaimId = claimId,
                            AssignToUserId = claim.AssignToUserId,
                            ReviewedBy = claim.ReviewedUser != null ? claim.ReviewedUser.FirstName + " " + claim.ReviewedUser.LastName : string.Empty,
                            StatusString = claim.Status,
                            Submitted = claim.SubmissionDate,
                            Assigned = claim.AssignDate,
                            Started = claim.ReviewStartDate,
                            ReviewDue = claim.ReviewDueDate,
                            Reviewed = claim.ReviewEndDate,
                            Approved = claim.ApprovalDate,
                            ClaimOnhold = claim.ClaimOnhold != null ? (bool)claim.ClaimOnhold : false,
                            ClaimOnholdDate = claim.ClaimOnHoldDate,
                            ClaimOffholdDate = claim.ClaimOffHoldDate,
                            ClaimOnholdDays = claim.ClaimOnHoldDays,
                            AuditOnhold = claim.AuditOnhold != null ? (bool)claim.AuditOnhold : false,
                            AuditOnholdDate = claim.AuditOnHoldDate,
                            AuditOffholdDate = claim.AuditOffHoldDate,
                            AuditOnholdDays = claim.AuditOnHoldDays,
                            AllTransactionDetails = claim.ClaimDetails.Select(c => new TransactionDetail
                            {
                                TransactionType = c.Transaction.TransactionTypeCode,
                                ProcessingStatus = c.Transaction.ProcessingStatus,
                                Direction = c.Direction,
                                MobileFormat = c.Transaction.MobileFormat,
                                IsAdjusted = c.Transaction.TransactionAdjustments.Any(),
                                TransactionSupportDocumentCountList = c.Transaction.TransactionSupportingDocuments.GroupBy(t => new { t.DocumentName }).Select(t => new TransactionSupportDocumentCount
                                {
                                    //OTSMT2-31 only take Accepted as adjustment, otherwise, take original
                                    DocumentFormat = c.Transaction.TransactionAdjustments.OrderByDescending(y => y.ID).FirstOrDefault().Status == "Accepted" ? t.OrderByDescending(x => x.ID).FirstOrDefault().DocumentFormat : t.FirstOrDefault().DocumentFormat
                                }).ToList(),
                            }).ToList()
                        };
            var result = query.FirstOrDefault();
            if (result != null)
            {
                //Calcualte onholddays if the current status is onhold
                if (result.ClaimOnhold)
                {
                    var span = DateTime.UtcNow.Date.Subtract(((DateTime)result.ClaimOnholdDate).Date);
                    result.ClaimOnholdDays = result.ClaimOnholdDays + span.Days;
                    if (result.ReviewDue.HasValue)
                        result.ReviewDue = result.ReviewDue.Value.AddDays(span.Days);
                }
                if (result.AuditOnhold)
                {
                    var span = DateTime.UtcNow.Date.Subtract(((DateTime)result.AuditOnholdDate).Date);
                    result.AuditOnholdDays = result.AuditOnholdDays + span.Days;
                    if (result.ReviewDue.HasValue)
                        result.ReviewDue = result.ReviewDue.Value.AddDays(span.Days);
                }
            }
            //Calculate columns
            result.TotalTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            result.TotalTransactionDetailsOut = result.AllTransactionDetails.Where(c => !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.PaperTransactionDetailsIn = result.AllTransactionDetails.Where(c => !c.MobileFormat && c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            //OTSTM2-31
            result.UploadCount = result.AllTransactionDetails.Where(c => !c.MobileFormat).Sum(c => c.TransactionSupportDocumentCountList.Count(t => t.DocumentFormat == 3));
            result.MailCount = result.AllTransactionDetails.Where(c => !c.MobileFormat).Sum(c => c.TransactionSupportDocumentCountList.Count(t => t.DocumentFormat == 2));
            result.FaxCount = result.AllTransactionDetails.Where(c => !c.MobileFormat).Sum(c => c.TransactionSupportDocumentCountList.Count(t => t.DocumentFormat == 1));

            result.PaperTransactionDetailsOut = result.AllTransactionDetails.Where(c => !c.MobileFormat && !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.MobileTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.MobileFormat & c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            result.MobileTransactionDetailsOut = result.AllTransactionDetails.Where(c => c.MobileFormat & !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.UnreviewedTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString() & c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            result.UnreviewedTransactionDetailsOut = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString() & !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.AdjustedTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.IsAdjusted && c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            result.AdjustedTransactionDetailsOut = result.AllTransactionDetails.Where(c => c.IsAdjusted && !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.ApprovedTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() && c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();

            result.ApprovedTransactionDetailsOut = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() && !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.ReviewedTransactionDetailsIn = result.AllTransactionDetails.Where(c => (c.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || c.ProcessingStatus == TransactionProcessingStatus.Invalidated.ToString()) && c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();
            result.ReviewedTransactionDetailsOut = result.AllTransactionDetails.Where(c => (c.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || c.ProcessingStatus == TransactionProcessingStatus.Invalidated.ToString()) && !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();

            result.InvalidatedTransactionDetailsIn = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Invalidated.ToString() & c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = true,
                TransactionCount = c.Count()
            }).ToList();
            result.InvalidatedTransactionDetailsOut = result.AllTransactionDetails.Where(c => c.ProcessingStatus == TransactionProcessingStatus.Invalidated.ToString() & !c.Direction).GroupBy(c => new { c.TransactionType }).Select(c => new TransactionDetail
            {
                TransactionType = c.Key.TransactionType,
                Direction = false,
                TransactionCount = c.Count()
            }).ToList();
            return result;
        }

        public DateTime? UpdateClaimOnhold(int claimId, bool isOnhold, long userId, long systemUserId)
        {
            //OTSTM2-499
            var dbContext = context as ClaimsBoundedContext;
            var claim = dbContext.Claims.Where(c => c.ID == claimId).FirstOrDefault();
            if ((ClaimType)claim.ClaimType == ClaimType.Collector)
            {
                var oldStatus = claim.ClaimOnhold;

                if (oldStatus == true)
                {
                    var oldOnholdDate = claim.ClaimOnHoldDate;
                    var span = (DateTime.UtcNow.Date - ((DateTime)oldOnholdDate).Date).TotalDays;
                    var addedDays = Convert.ToInt32(span);
                    claim.ClaimOnHoldDays = claim.ClaimOnHoldDays + Convert.ToInt32(span);
                    //Recalculate ReviewDueDate and ChequeDueDate
                    if (claim.ReviewDueDate != null)
                        claim.ReviewDueDate = ((DateTime)claim.ReviewDueDate).AddDays(addedDays);
                    if (claim.ChequeDueDate != null)
                        claim.ChequeDueDate = ((DateTime)claim.ChequeDueDate).AddDays(addedDays);
                }

                claim.ClaimOnhold = isOnhold;
                if (isOnhold)
                {
                    claim.ClaimOnHoldDate = DateTime.UtcNow;
                    claim.ClaimOnHoldBy = userId;
                }
                else
                {
                    claim.ClaimOffHoldDate = DateTime.UtcNow;
                }
            }
            else
            {
                if (isOnhold)
                {
                    var claimList = dbContext.Claims.Where(c => c.ParticipantId == claim.ParticipantId && c.ClaimPeriod.StartDate >= claim.ClaimPeriod.StartDate && (c.Status == "Under Review" || c.Status == "Submitted") && c.ClaimOnhold != true).ToList();
                    claimList.ForEach(c =>
                    {
                        if (c.ClaimPeriod.StartDate == claim.ClaimPeriod.StartDate)
                        {
                            c.ClaimOnhold = isOnhold;
                            c.ClaimOnHoldDate = DateTime.UtcNow;
                            c.ClaimOnHoldBy = userId;
                        }
                        else
                        {
                            c.ClaimOnhold = isOnhold;
                            c.ClaimOnHoldDate = DateTime.UtcNow;
                            c.ClaimOnHoldBy = systemUserId;
                        }
                    });
                }
                else
                {
                    var oldOnholdDate = claim.ClaimOnHoldDate;
                    var span = (DateTime.UtcNow.Date - ((DateTime)oldOnholdDate).Date).TotalDays;
                    var addedDays = Convert.ToInt32(span);
                    claim.ClaimOnHoldDays = claim.ClaimOnHoldDays + Convert.ToInt32(span);
                    //Recalculate ReviewDueDate and ChequeDueDate
                    if (claim.ReviewDueDate != null)
                        claim.ReviewDueDate = ((DateTime)claim.ReviewDueDate).AddDays(addedDays);
                    if (claim.ChequeDueDate != null)
                        claim.ChequeDueDate = ((DateTime)claim.ChequeDueDate).AddDays(addedDays);

                    claim.ClaimOnhold = isOnhold;
                    claim.ClaimOffHoldDate = DateTime.UtcNow;
                }
            }

            dbContext.SaveChanges();
            return claim.ReviewDueDate;
        }


        public DateTime? UpdateAuditOnhold(int claimId, bool isOnhold, long userId, long systemUserId)
        {
            //OTSTM2-499
            var dbContext = context as ClaimsBoundedContext;
            var claim = dbContext.Claims.Where(c => c.ID == claimId).FirstOrDefault();

            if ((ClaimType)claim.ClaimType == ClaimType.Collector)
            {
                var oldStatus = claim.AuditOnhold;

                if (oldStatus == true)
                {
                    var oldOnholdDate = claim.AuditOnHoldDate;
                    var span = (DateTime.UtcNow.Date - ((DateTime)oldOnholdDate).Date).TotalDays;
                    var addedDays = Convert.ToInt32(span);
                    claim.AuditOnHoldDays = claim.AuditOnHoldDays + Convert.ToInt32(span);
                    //Recalculate ReviewDueDate and ChequeDueDate
                    if (claim.ReviewDueDate != null)
                        claim.ReviewDueDate = ((DateTime)claim.ReviewDueDate).AddDays(addedDays);
                    if (claim.ChequeDueDate != null)
                        claim.ChequeDueDate = ((DateTime)claim.ChequeDueDate).AddDays(addedDays);
                }

                claim.AuditOnhold = isOnhold;
                if (isOnhold)
                {
                    claim.AuditOnHoldDate = DateTime.UtcNow;
                    claim.AuditOnHoldBy = userId;
                }
                else
                {
                    claim.AuditOffHoldDate = DateTime.UtcNow;
                }
            }

            else
            {
                if (isOnhold)
                {
                    var claimList = dbContext.Claims.Where(c => c.ParticipantId == claim.ParticipantId && c.ClaimPeriod.StartDate >= claim.ClaimPeriod.StartDate && (c.Status == "Under Review" || c.Status == "Submitted") && c.AuditOnhold != true).ToList();
                    claimList.ForEach(c =>
                    {
                        if (c.ClaimPeriod.StartDate == claim.ClaimPeriod.StartDate)
                        {
                            c.AuditOnhold = isOnhold;
                            c.AuditOnHoldDate = DateTime.UtcNow;
                            c.AuditOnHoldBy = userId;
                        }
                        else
                        {
                            c.AuditOnhold = isOnhold;
                            c.AuditOnHoldDate = DateTime.UtcNow;
                            c.AuditOnHoldBy = systemUserId;
                        }
                    });
                }
                else
                {
                    var oldOnholdDate = claim.AuditOnHoldDate;
                    var span = (DateTime.UtcNow.Date - ((DateTime)oldOnholdDate).Date).TotalDays;
                    var addedDays = Convert.ToInt32(span);
                    claim.AuditOnHoldDays = claim.AuditOnHoldDays + Convert.ToInt32(span);
                    //Recalculate ReviewDueDate and ChequeDueDate
                    if (claim.ReviewDueDate != null)
                        claim.ReviewDueDate = ((DateTime)claim.ReviewDueDate).AddDays(addedDays);
                    if (claim.ChequeDueDate != null)
                        claim.ChequeDueDate = ((DateTime)claim.ChequeDueDate).AddDays(addedDays);

                    claim.AuditOnhold = isOnhold;
                    claim.AuditOffHoldDate = DateTime.UtcNow;
                }
            }

            dbContext.SaveChanges();
            return claim.ReviewDueDate;
        }
        public ClaimProcess GetClaimProcess(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimProcesses.FirstOrDefault(c => c.Claim.ID == claimId);
        }

        public void UpdateClaimStatus(ClaimProcess claimProcess, bool isAdd)
        {
            var dbContext = context as ClaimsBoundedContext;
            if (isAdd)
                dbContext.Entry(claimProcess).State = EntityState.Added;
            else
                dbContext.Entry(claimProcess).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void UpdateHST(int claimId, decimal totalTax, bool isTaxApplicable = true)
        {
            var dbContext = context as ClaimsBoundedContext;

            var claim = dbContext.Claims.Where(x => x.ID == claimId).FirstOrDefault();
            var claimPayments = LoadClaimPayments(claimId);
            var claimPaymentAdjustments = LoadClaimPaymentInternalAdjusts(claimId);
            var taxRate = 0.13m;
            claim.TotalTax = 0;
            claim.IsTaxApplicable = isTaxApplicable;
            if (claim.ClaimType == (int)ClaimType.Collector) //2
            {
                if (claim.Participant.IsTaxExempt)
                {
                    //No HST
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                }
                else
                {
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            else if (claim.ClaimType == (int)ClaimType.Hauler) //3)
            {
                if (claim.Participant.IsTaxExempt)
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);
                    var paymentAmount = paymentAmountPositive - paymentAmountNegative;
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;

                }
                else
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);
                    var noTaxAdjustmentAmount = claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => c.AdjustmentAmount);
                    var taxAdjustment = adjustmentAmount - noTaxAdjustmentAmount;
                    var taxAmount = Math.Round((paymentAmountPositive + taxAdjustment) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmountPositive - paymentAmountNegative + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            else if (claim.ClaimType == (int)ClaimType.Processor)//4)
            {
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var pitAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var dorAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var paymentAmount = ptrAmount + spsAmount + pitAmount + dorAmount;
                var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);

                if (isTaxApplicable)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                }
            }
            else if (claim.ClaimType == (int)ClaimType.RPM) //5)
            {
                var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var adjustmentAmount = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount);

                if (isTaxApplicable)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                }
            }
            dbContext.SaveChanges();
        }
        public void ClaimBackToParticipant(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claim = dbContext.Claims.FirstOrDefault(c => c.ID == claimId);
            claim.ReviewedBy = null;
            claim.AssignToUserId = null;
            claim.AssignDate = null;
            claim.SubmittedBy = null;
            claim.SubmissionDate = null;
            claim.ReviewStartDate = null;
            claim.ReviewDueDate = null;
            claim.ChequeDueDate = null;
            claim.ClaimOnhold = null;
            claim.AuditOnhold = null;
            claim.ClaimOnHoldBy = null;
            claim.AuditOnHoldBy = null;
            claim.ClaimOnHoldDays = 0;
            claim.AuditOnHoldDays = 0;
            claim.ClaimOnHoldDate = null;
            claim.ClaimOffHoldDate = null;
            claim.AuditOnHoldDate = null;
            claim.AuditOffHoldDate = null;
            claim.Status = EnumHelper.GetEnumDescription(ClaimStatus.Open);
            var claimProcess = dbContext.ClaimProcesses.FirstOrDefault(c => c.Claim.ID == claimId);
            if (claimProcess != null)
            {
                dbContext.ClaimProcesses.Remove(claimProcess);
            }
            var claimPaymentSummary = dbContext.ClaimPaymentSummaries.FirstOrDefault(c => c.Claim.ID == claimId);

            dbContext.SaveChanges();
        }

        public Claim GetClaimWithSummaryPeriod(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims
                .Include(c => c.ClaimPeriod)
                .Include(c => c.ClaimSummary)
                .FirstOrDefault(c => c.ID == claimId);
        }

        public List<int> GetClaimsByTransactionId(int transactionId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimDetails.Where(c => c.TransactionId == transactionId).Select(c => c.ClaimId).ToList();
        }

        public bool TransactionExistsInClaim(Guid transactionId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimDetails.Where(c => c.Transaction.TransactionId == transactionId).Any();
        }

        public void UpdateClaimMailDate(int claimId, DateTime mailDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimPaymentSummary = dbContext.ClaimPaymentSummaries.FirstOrDefault(c => c.Claim.ID == claimId);
            if (claimPaymentSummary != null)
            {
                claimPaymentSummary.MailDate = mailDate;
                dbContext.SaveChanges();
            }
        }
        #endregion Common

        #region participant claims

        public PaginationDTO<ClaimViewModel, int> LoadVendorClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims.AsNoTracking()
                        join prd in dbContext.Periods.AsNoTracking() on claim.ClaimPeriodId equals prd.ID
                        join gpbe in dbContext.GpBatchEntries.AsNoTracking() on claim.ID equals gpbe.ClaimId into gjcg //OTSTM2-484
                        from subGpbe in gjcg.DefaultIfEmpty()  //OTSTM2-484
                        where claim.ParticipantId == vendorId
                        select new ClaimViewModel()
                        {
                            ID = claim.ID,
                            Status = claim.Status,
                            Amount = claim.ClaimsAmountTotal ?? 0,
                            SubmittedDate = claim.SubmissionDate,
                            ReviewDueDate = claim.ReviewDueDate,
                            ChequeDueDate = claim.ChequeDueDate,
                            ClaimOnholdDate = claim.ClaimOnHoldDate,
                            AuditOnholdDate = claim.AuditOnHoldDate,
                            ClaimOnhold = claim.ClaimOnhold ?? false,
                            AuditOnhold = claim.AuditOnhold ?? false,
                            EftNumber = claim.ClaimPaymentSummary != null ? (claim.ClaimPaymentSummary.ChequeNumber != null ? claim.ClaimPaymentSummary.ChequeNumber : string.Empty) : string.Empty,
                            PaymentDate = claim.ClaimPaymentSummary != null ? claim.ClaimPaymentSummary.PaidDate : null,
                            AssignedDate = claim.AssignDate,
                            SubmittedBy = claim.SubmittedUser != null ? claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName : string.Empty,
                            ClaimType = (ClaimType)claim.ClaimType,
                            PeriodId = prd.ID,
                            PeriodName = prd.ShortName,
                            VendorId = vendorId,
                            PeriodStartDate = prd.StartDate,
                            IsClaimPost = subGpbe != null ? (subGpbe.GpiStatusID.Trim() == "P2" ? true : false) : false //OTSTM2-484
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.SubmittedDate) == DbFunctions.TruncateTime(dateValue)
                                            //|| DbFunctions.TruncateTime(c.ChequeDueDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.PaymentDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.AssignedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Status.ToLower().Contains(searchText.ToLower())
                                             || c.PeriodName.Contains(searchText.ToLower())
                                             || SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(searchText) //OTSTM2-988
                                             || c.EftNumber.Contains(searchText.ToLower())
                                             || c.SubmittedBy.Contains(searchText.ToLower()));
                }
            }

            //OTSTM2-988
            var columnSearchTextPeriodName = columnSearchText["PeriodName"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAmount = columnSearchText["Amount"];
            var columnSearchTextSubmittedDate = columnSearchText["SubmittedDate"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextEftNumber = columnSearchText["EftNumber"];
            var columnSearchTextPaymentDate = columnSearchText["PaymentDate"];
            var columnSearchTextAssignedDate = columnSearchText["AssignedDate"];
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriodName))
            {
                query = query.Where(c => c.PeriodName.ToString().Contains(columnSearchTextPeriodName));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(columnSearchTextAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextEftNumber))
            {
                query = query.Where(c => c.EftNumber.ToString().Contains(columnSearchTextEftNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPaymentDate))
            {
                query = query.Where(c => (c.PaymentDate != null ? (SqlFunctions.DatePart("yyyy", c.PaymentDate.Value) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextPaymentDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                query = query.Where(c => c.SubmittedBy.ToString().Contains(columnSearchTextSubmittedBy));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "SubmittedDate" : orderBy;

            if (orderBy == "PeriodName")
                orderBy = "PeriodStartDate";

            if (sortDirection == "asc")
                query = query.AsQueryable().OrderBy(orderBy);
            else
                query = query.AsQueryable().OrderBy(orderBy + " descending"); ;

            var result = query.ToList();

            //OTSTM2-988 convert the followed 2 date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.SubmittedDate = (i.SubmittedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.SubmittedDate.Value, TimeZoneInfo.Local) : i.SubmittedDate;
                i.AssignedDate = (i.AssignedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.AssignedDate.Value, TimeZoneInfo.Local) : i.AssignedDate;
            });

            //Both SubmittedDate and AssignedDate are UTC time, has to be converted to local time (as UI)
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedDate))
            {
                result = result.Where(c => (c.SubmittedDate != null ? c.SubmittedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmittedDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedDate))
            {
                result = result.Where(c => (c.AssignedDate != null ? c.AssignedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextAssignedDate)).ToList();
            }

            var totalRecords = result.Count();
            var returnValue = result.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ClaimViewModel, int>
            {
                DTOCollection = returnValue,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<ClaimViewModel> GetVendorParticipantClaimsList(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claim in dbContext.Claims.AsNoTracking()
                        join prd in dbContext.Periods on claim.ClaimPeriodId equals prd.ID
                        where claim.ParticipantId == vendorId
                        select new ClaimViewModel()
                        {
                            ID = claim.ID,
                            Status = claim.Status,
                            Amount = claim.ClaimsAmountTotal ?? 0,
                            SubmittedDate = claim.SubmissionDate,
                            ReviewDueDate = claim.ReviewDueDate,
                            ChequeDueDate = claim.ChequeDueDate,
                            ClaimOnholdDate = claim.ClaimOnHoldDate,
                            AuditOnholdDate = claim.AuditOnHoldDate,
                            ClaimOnhold = claim.ClaimOnhold ?? false,
                            AuditOnhold = claim.AuditOnhold ?? false,
                            EftNumber = claim.ClaimPaymentSummary != null ? claim.ClaimPaymentSummary.ChequeNumber : string.Empty,
                            PaymentDate = claim.ClaimPaymentSummary != null ? claim.ClaimPaymentSummary.PaidDate : null,
                            AssignedDate = claim.AssignDate,
                            SubmittedBy = claim.SubmittedUser != null ? claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName : string.Empty,
                            ClaimType = (ClaimType)claim.ClaimType,
                            PeriodId = prd.ID,
                            PeriodName = prd.ShortName,
                            VendorId = vendorId,
                            PeriodStartDate = prd.StartDate,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.SubmittedDate) == DbFunctions.TruncateTime(dateValue)
                                            //|| DbFunctions.TruncateTime(c.ChequeDueDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.PaymentDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.AssignedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Status.ToLower().Contains(searchText.ToLower())
                                             || c.PeriodName.Contains(searchText.ToLower())
                                             || SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(searchText) //OTSTM2-988
                                             || c.EftNumber.Contains(searchText.ToLower())
                                             || c.SubmittedBy.Contains(searchText.ToLower()));
                }
            }

            //OTSTM2-988
            var columnSearchTextPeriodName = columnSearchText["PeriodName"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAmount = columnSearchText["Amount"];
            var columnSearchTextSubmittedDate = columnSearchText["SubmittedDate"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextEftNumber = columnSearchText["EftNumber"];
            var columnSearchTextPaymentDate = columnSearchText["PaymentDate"];
            var columnSearchTextAssignedDate = columnSearchText["AssignedDate"];
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriodName))
            {
                query = query.Where(c => c.PeriodName.ToString().Contains(columnSearchTextPeriodName));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(columnSearchTextAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextEftNumber))
            {
                query = query.Where(c => c.EftNumber.ToString().Contains(columnSearchTextEftNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPaymentDate))
            {
                query = query.Where(c => (c.PaymentDate != null ? (SqlFunctions.DatePart("yyyy", c.PaymentDate.Value) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextPaymentDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                query = query.Where(c => c.SubmittedBy.ToString().Contains(columnSearchTextSubmittedBy));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "SubmittedDate" : orderBy;

            if (orderBy == "PeriodName")
                orderBy = "PeriodStartDate";

            //OTSTM2-736
            if (orderBy == "PaymentDue")
                orderBy = "ChequeDueDate";

            if (sortDirection == "asc")
                query = query.AsQueryable().OrderBy(orderBy);
            else
                query = query.AsQueryable().OrderBy(orderBy + " descending"); ;

            var result = query.ToList();

            //OTSTM2-988 convert the followed 2 date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.SubmittedDate = (i.SubmittedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.SubmittedDate.Value, TimeZoneInfo.Local) : i.SubmittedDate;
                i.AssignedDate = (i.AssignedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.AssignedDate.Value, TimeZoneInfo.Local) : i.AssignedDate;
            });

            //Both SubmittedDate and AssignedDate are UTC time, has to be converted to local time (as UI)
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedDate))
            {
                result = result.Where(c => (c.SubmittedDate != null ? c.SubmittedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmittedDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedDate))
            {
                result = result.Where(c => (c.AssignedDate != null ? c.AssignedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextAssignedDate)).ToList();
            }

            return result;
        }

        public IList GetClaimPeriodForBreadCrumb(int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claim in dbContext.Claims.AsNoTracking()
                        join prd in dbContext.Periods on claim.ClaimPeriodId equals prd.ID
                        where claim.ParticipantId == vendorId
                        //select prd;
                        select new { shortName = prd.ShortName, claimId = claim.ID };

            return query.ToList();
        }

        #endregion participant claims

        #region staff claims
        public PaginationDTO<ClaimViewModel, int> LoadVendorStaffClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claim in dbContext.Claims.AsNoTracking()
                        join prd in dbContext.Periods on claim.ClaimPeriodId equals prd.ID
                        join usr in dbContext.Users on claim.AssignToUserId equals usr.ID into gj
                        join gpbe in dbContext.GpBatchEntries.AsNoTracking() on claim.ID equals gpbe.ClaimId into gjcg //OTSTM2-484
                        from subUsr in gj.DefaultIfEmpty()
                        from subGpbe in gjcg.DefaultIfEmpty()  //OTSTM2-484
                        where claim.ParticipantId == vendorId
                        select new ClaimViewModel()
                        {
                            ID = claim.ID,
                            Status = claim.Status,
                            Amount = claim.ClaimsAmountTotal ?? 0,
                            SubmittedDate = claim.SubmissionDate,
                            ChequeDueDate = claim.ChequeDueDate != null ? claim.ChequeDueDate : null,
                            ClaimOnholdDate = claim.ClaimOnHoldDate != null ? claim.ClaimOnHoldDate : null,
                            EftNumber = claim.ClaimPaymentSummary != null ? (claim.ClaimPaymentSummary.ChequeNumber != null ? claim.ClaimPaymentSummary.ChequeNumber : string.Empty) : string.Empty,
                            PaymentDate = claim.ClaimPaymentSummary != null ? (claim.ClaimPaymentSummary.PaidDate != null ? claim.ClaimPaymentSummary.PaidDate : null) : null,
                            AssignedDate = claim.AssignDate,
                            SubmittedBy = claim.SubmittedUser != null ? claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName : string.Empty,
                            ClaimType = (ClaimType)claim.ClaimType,
                            PeriodId = prd.ID,
                            PeriodName = prd.ShortName,
                            VendorId = vendorId,
                            AssignedTo = (subUsr == null ? string.Empty : subUsr.FirstName + " " + subUsr.LastName),
                            //ReviewDate = claim.ReviewDueDate,
                            ReviewDueDate = claim.ReviewDueDate,
                            ClaimOnhold = claim.ClaimOnhold ?? false,
                            AuditOnhold = claim.AuditOnhold ?? false,
                            AuditOnholdDate = claim.AuditOnHoldDate,
                            PeriodStartDate = prd.StartDate,
                            IsClaimPost = subGpbe != null ? (subGpbe.GpiStatusID.Trim() == "P2" ? true : false) : false //OTSTM2-484
                        };


            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.SubmittedDate) == DbFunctions.TruncateTime(dateValue)
                                            //|| DbFunctions.TruncateTime(c.ChequeDueDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.PaymentDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.AssignedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Status.ToLower().Contains(searchText.ToLower())
                                             || c.PeriodName.Contains(searchText.ToLower())
                                             || SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(searchText) //OTSTM2-986
                                             || c.EftNumber.Contains(searchText.ToLower())
                                             || c.SubmittedBy.Contains(searchText.ToLower()));
                }
            }

            //OTSTM2-986
            var columnSearchTextPeriodName = columnSearchText["PeriodName"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAmount = columnSearchText["Amount"];
            var columnSearchTextSubmittedDate = columnSearchText["SubmittedDate"];
            var columnSearchTextReviewDate = columnSearchText["ReviewDate"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextEftNumber = columnSearchText["EftNumber"];
            var columnSearchTextPaymentDate = columnSearchText["PaymentDate"];
            var columnSearchTextAssignedDate = columnSearchText["AssignedDate"];
            var columnSearchTextHoldAssignedTo = columnSearchText["AssignedTo"];
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriodName))
            {
                query = query.Where(c => c.PeriodName.ToString().Contains(columnSearchTextPeriodName));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(columnSearchTextAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextEftNumber))
            {
                query = query.Where(c => c.EftNumber.ToString().Contains(columnSearchTextEftNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPaymentDate))
            {
                query = query.Where(c => (c.PaymentDate != null ? (SqlFunctions.DatePart("yyyy", c.PaymentDate.Value) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextPaymentDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextHoldAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextHoldAssignedTo));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                query = query.Where(c => c.SubmittedBy.ToString().Contains(columnSearchTextSubmittedBy));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "SubmittedDate" : orderBy;

            if (orderBy == "PeriodName")
                orderBy = "PeriodStartDate";

            //OTSTM2-736
            if (orderBy == "ReviewDate")
                orderBy = "ReviewDueDate";

            if (sortDirection == "asc")
                query = query.AsQueryable().OrderBy(orderBy);
            else
                query = query.AsQueryable().OrderBy(orderBy + " descending"); ;

            var result = query.ToList();

            //OTSTM2-986 convert the followed 2 date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.SubmittedDate = (i.SubmittedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.SubmittedDate.Value, TimeZoneInfo.Local) : i.SubmittedDate;
                i.AssignedDate = (i.AssignedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.AssignedDate.Value, TimeZoneInfo.Local) : i.AssignedDate;
            });

            //ReviewDueDate is UTC time, has to be convert to local time (as UI). Linq to entities doesn't recognize System.DateTime ConvertTimeFromUtc() method, has to be converted to list firstly.
            //Besides, both onhold days has to be added to ReviewDueDate, so using ReviewDate which already covers time converting and onhold days in ViewModel
            if (!string.IsNullOrWhiteSpace(columnSearchTextReviewDate))
            {
                result = result.Where(c => (c.ReviewDate != null ? c.ReviewDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextReviewDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedDate))
            {
                result = result.Where(c => (c.SubmittedDate != null ? c.SubmittedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmittedDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedDate))
            {
                result = result.Where(c => (c.AssignedDate != null ? c.AssignedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextAssignedDate)).ToList();
            }

            var totalRecords = result.Count();
            var returnValue = result.Skip(pageIndex)
                    .Take(pageSize).ToList();

            return new PaginationDTO<ClaimViewModel, int>
            {
                DTOCollection = returnValue,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<ClaimViewModel> GetVendorStaffClaimsList(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claim in dbContext.Claims.AsNoTracking()
                        join prd in dbContext.Periods on claim.ClaimPeriodId equals prd.ID
                        join usr in dbContext.Users on claim.AssignToUserId equals usr.ID into gj
                        from subUsr in gj.DefaultIfEmpty()
                        where claim.ParticipantId == vendorId
                        select new ClaimViewModel()
                        {
                            ID = claim.ID,
                            Status = claim.Status,
                            Amount = claim.ClaimsAmountTotal ?? 0,
                            SubmittedDate = claim.SubmissionDate,
                            ChequeDueDate = claim.ChequeDueDate != null ? claim.ChequeDueDate : null,
                            ClaimOnholdDate = claim.ClaimOnHoldDate != null ? claim.ClaimOnHoldDate : null,
                            EftNumber = claim.ClaimPaymentSummary != null ? (claim.ClaimPaymentSummary.ChequeNumber != null ? claim.ClaimPaymentSummary.ChequeNumber : string.Empty) : string.Empty,
                            PaymentDate = claim.ClaimPaymentSummary != null ? (claim.ClaimPaymentSummary.PaidDate != null ? claim.ClaimPaymentSummary.PaidDate : null) : null,
                            AssignedDate = claim.AssignDate,
                            SubmittedBy = claim.SubmittedUser != null ? claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName : string.Empty,
                            ClaimType = (ClaimType)claim.ClaimType,
                            PeriodId = prd.ID,
                            PeriodName = prd.ShortName,
                            VendorId = vendorId,
                            AssignedTo = (subUsr == null ? string.Empty : subUsr.FirstName + " " + subUsr.LastName),
                            //ReviewDate = claim.ReviewDueDate,
                            ReviewDueDate = claim.ReviewDueDate,
                            ClaimOnhold = claim.ClaimOnhold ?? false,
                            AuditOnhold = claim.AuditOnhold ?? false,
                            AuditOnholdDate = claim.AuditOnHoldDate,
                            PeriodStartDate = prd.StartDate
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.SubmittedDate) == DbFunctions.TruncateTime(dateValue)
                                            //|| DbFunctions.TruncateTime(c.ChequeDueDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.PaymentDate) == DbFunctions.TruncateTime(dateValue)
                                            || DbFunctions.TruncateTime(c.AssignedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Status.ToLower().Contains(searchText.ToLower())
                                             || c.PeriodName.Contains(searchText.ToLower())
                                             || SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(searchText) //OTSTM2-986
                                             || c.EftNumber.Contains(searchText.ToLower())
                                             || c.SubmittedBy.Contains(searchText.ToLower()));
                }
            }

            //OTSTM2-986
            var columnSearchTextPeriodName = columnSearchText["PeriodName"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAmount = columnSearchText["Amount"];
            var columnSearchTextSubmittedDate = columnSearchText["SubmittedDate"];
            var columnSearchTextReviewDate = columnSearchText["ReviewDate"];
            var columnSearchTextChequeDueDate = columnSearchText["ChequeDueDate"];
            var columnSearchTextEftNumber = columnSearchText["EftNumber"];
            var columnSearchTextPaymentDate = columnSearchText["PaymentDate"];
            var columnSearchTextAssignedDate = columnSearchText["AssignedDate"];
            var columnSearchTextHoldAssignedTo = columnSearchText["AssignedTo"];
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriodName))
            {
                query = query.Where(c => c.PeriodName.ToString().Contains(columnSearchTextPeriodName));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.Amount, 18, 2).Contains(columnSearchTextAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeDueDate))
            {
                query = query.Where(c => (c.ChequeDueDate != null ? (SqlFunctions.DatePart("yyyy", c.ChequeDueDate.Value) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.ChequeDueDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextChequeDueDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextEftNumber))
            {
                query = query.Where(c => c.EftNumber.ToString().Contains(columnSearchTextEftNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPaymentDate))
            {
                query = query.Where(c => (c.PaymentDate != null ? (SqlFunctions.DatePart("yyyy", c.PaymentDate.Value) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.PaymentDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextPaymentDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextHoldAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextHoldAssignedTo));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                query = query.Where(c => c.SubmittedBy.ToString().Contains(columnSearchTextSubmittedBy));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "SubmittedDate" : orderBy;

            if (orderBy == "PeriodName")
                orderBy = "PeriodStartDate";

            //OTSTM2-736
            if (orderBy == "PaymentDue")
                orderBy = "ChequeDueDate";
            if (orderBy == "ReviewDate")
                orderBy = "ReviewDueDate";

            if (sortDirection == "asc")
                query = query.AsQueryable().OrderBy(orderBy);
            else
                query = query.AsQueryable().OrderBy(orderBy + " descending"); ;

            var result = query.ToList();

            //OTSTM2-986 convert the followed 2 dates to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.SubmittedDate = (i.SubmittedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.SubmittedDate.Value, TimeZoneInfo.Local) : i.SubmittedDate;
                i.AssignedDate = (i.AssignedDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.AssignedDate.Value, TimeZoneInfo.Local) : i.AssignedDate;
            });

            if (!string.IsNullOrWhiteSpace(columnSearchTextReviewDate))
            {
                result = result.Where(c => (c.ReviewDate != null ? c.ReviewDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextReviewDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedDate))
            {
                result = result.Where(c => (c.SubmittedDate != null ? c.SubmittedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmittedDate)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedDate))
            {
                result = result.Where(c => (c.AssignedDate != null ? c.AssignedDate.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextAssignedDate)).ToList();
            }

            return result;
        }

        public void ApproveAllAssociatedTransactions(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails
                        join transaction in dbContext.Transactions on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.ProcessingStatus == "Unreviewed"
                        select transaction;
            query.ToList().ForEach(c =>
            {
                c.ProcessingStatus = TransactionProcessingStatus.Approved.ToString();
            });
            dbContext.SaveChanges();
        }

        public void ApproveMobileTransactions(int claimId, string TransactionType)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails
                        join transaction in dbContext.Transactions on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.MobileFormat && transaction.ProcessingStatus == "Unreviewed"
                        && transaction.TransactionTypeCode == TransactionType
                        select transaction;
            query.ToList().ForEach(c =>
            {
                if (!c.TransactionNotes.Any(i => i.NoteType == "Error" || i.NoteType == "Warning"))
                {
                    c.ProcessingStatus = TransactionProcessingStatus.Approved.ToString();
                }
            });
            dbContext.SaveChanges();
        }

        public int? GetTransactionIdByFriendlyId(long friendlyId, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails
                        join transaction in dbContext.Transactions on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.FriendlyId == friendlyId
                        select claimdetail;
            //#1172 for collector will filter UCR transactions.
            var collectorClaim = dbContext.Claims.Find(claimId);
            if (collectorClaim != null && collectorClaim.ClaimType == (int)ClaimType.Collector)
            {
                query = query.Where(x => x.Transaction != null && x.Transaction.TransactionTypeCode != "UCR");
            }

            var claimDetail = query.FirstOrDefault();
            if (claimDetail != null) return claimDetail.TransactionId;

            return null;
        }

        public VendorReference GetVendorReference(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        where claim.ID == claimId
                        select new VendorReference
                        {
                            VendorId = claim.Participant.ID,
                            RegistrationNumber = claim.Participant.Number,
                            BusinessName = claim.Participant.BusinessName,
                            VendorType = claim.Participant.VendorType,
                            IsActive = claim.Participant.IsActive,
                            Status = claim.Participant.RegistrantStatus
                        };
            return query.FirstOrDefault();
        }

        #endregion

        #region Claims Summary

        /// <summary>
        /// query scope:transaction belong to current Claim
        /// </summary>
        /// <param name="claimId">The claim identifier.</param>
        /// <returns></returns>
        public bool HasPendingTransactionAndAdjust(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claim = dbContext.Claims.FirstOrDefault(c => c.ID == claimId);

            //OTSTM2-419
            var queryTransaction = from detail in claim.ClaimDetails
                                   where (detail.Transaction.Status == TransactionStatus.Pending.ToString() || detail.Transaction.Status == TransactionStatus.Incomplete.ToString())
                                   select detail.ID;

            var queryAdjust = from transactionAdjustment in dbContext.TransactionAdjustments
                              join claimDetail in dbContext.ClaimDetails on transactionAdjustment.OriginalTransactionId equals
                                  claimDetail.TransactionId
                              where claimDetail.ClaimId == claimId && transactionAdjustment.Status == TransactionStatus.Pending.ToString()
                              select transactionAdjustment.ID;
            return ((queryTransaction.Count() > 0) || (queryAdjust.Count() > 0));
        }

        /// <summary>
        /// query scope:transaction belong to current Claim PERIOD
        /// </summary>
        /// <param name="claimId">The claim identifier.</param>
        /// <returns></returns>
        public bool HasPendingTransactionAndAdjustForThisClaimPeriod(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claim = dbContext.Claims.FirstOrDefault(c => c.ID == claimId);

            var queryTransaction = from transaction in dbContext.Transactions
                                   where (transaction.IncomingId == claim.ParticipantId || transaction.OutgoingId == claim.ParticipantId)
                                   && DbFunctions.TruncateTime(transaction.TransactionDate) >= claim.ClaimPeriod.StartDate.Date
                                   && DbFunctions.TruncateTime(transaction.TransactionDate) <= claim.ClaimPeriod.EndDate.Date
                                   && (transaction.Status == TransactionStatus.Pending.ToString() || transaction.Status == TransactionStatus.Incomplete.ToString())
                                   select transaction.ID;

            var queryAdjust = from transactionAdjustment in dbContext.TransactionAdjustments
                              join claimDetail in dbContext.ClaimDetails on transactionAdjustment.OriginalTransactionId equals
                                  claimDetail.TransactionId
                              where claimDetail.ClaimId == claimId && transactionAdjustment.Status == TransactionStatus.Pending.ToString()
                              select transactionAdjustment.ID;
            return ((queryTransaction.Count() > 0) || (queryAdjust.Count() > 0));
        }

        public InventoryOpeningSummary LoadInventoryOpeningSummary(int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.Inventories.AsNoTracking()
                .Where(c => c.VendorId == vendorId)
                .GroupBy(c => new { c.IsEligible, c.Item.ItemType })
                .Select(c => new { Name = c.Key, Sum = c.Sum(i => i.ActualWeight), SumEstimated = c.Sum(i => i.Weight) });
            var result = query.ToList();
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            result.ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.ItemType == 1)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoad = c.Sum;
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = c.SumEstimated;
                }
                if (!c.Name.IsEligible && c.Name.ItemType == 1)
                {
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = c.Sum;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = c.SumEstimated;
                }
                if (c.Name.IsEligible && c.Name.ItemType == 2)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoad = c.Sum;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = c.SumEstimated;
                }
                if (!c.Name.IsEligible && c.Name.ItemType == 2)
                {
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = c.Sum;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = c.SumEstimated;
                }
            });

            inventoryOpeningSummary.TotalOpening = inventoryOpeningSummary.TotalEligibleOpeningOnRoad + inventoryOpeningSummary.TotalIneligibleOpeningOnRoad + inventoryOpeningSummary.TotalEligibleOpeningOffRoad + inventoryOpeningSummary.TotalIneligibleOpeningOffRoad;
            inventoryOpeningSummary.TotalOpeningEstimated = inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated + inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated + inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated + inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated;
            return inventoryOpeningSummary;
        }

        public ClaimSummary GetPreviousClaimSummary(int vendorId, DateTime claimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            //Get previous approve claim
            var previousClaim = dbContext.Claims.Include(c => c.ClaimSummary).Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date && (c.Status == "Approved" || c.Status == "Receive Payment")).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousClaim != null)
            {
                return previousClaim.ClaimSummary;
            }
            return null;
        }

        public bool IsPreviousClaimApproved(int vendorId, DateTime claimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            var previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousClaim == null)
            {
                //First claim
                return true;
            }
            else
            {
                if ((previousClaim.Status == "Approved") || (previousClaim.Status == "Receive Payment"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsPreviousClaimPosted(int vendorId, DateTime currentClaimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            var previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < currentClaimPeriodDate.Date).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousClaim == null)
            {
                return true;
            }
            else
            {
                //Fixed OTSTM2-364
                if (previousClaim.ClaimsAmountTotal == 0)
                {
                    return true;
                }
                else
                {
                    return IsClaimPosted(previousClaim.ID);
                }
            }
        }

        public bool IsPreviousClaimOnHold(int vendorId, DateTime claimPeriodDate, string vendorType = "")
        {
            var dbContext = context as ClaimsBoundedContext;

            Claim previousClaim = null;

            if (vendorType == TreadMarksConstants.Collector)
                previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && c.ClaimOnhold == true && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date).FirstOrDefault();
            else
                previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            if (previousClaim == null)
            {
                //First claim
                return false;
            }
            else
            {
                if ((previousClaim.ClaimOnhold != null) && ((bool)previousClaim.ClaimOnhold))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public bool IsPreviousAuditOnHold(int vendorId, DateTime claimPeriodDate, string vendorType = "")
        {
            var dbContext = context as ClaimsBoundedContext;

            Claim previousClaim = null;
            if (vendorType == TreadMarksConstants.Collector)
                previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && c.AuditOnhold == true && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date).FirstOrDefault();
            else
                previousClaim = dbContext.Claims.Where(c => c.ParticipantId == vendorId && DbFunctions.TruncateTime(c.ClaimPeriod.StartDate) < claimPeriodDate.Date).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();

            if (previousClaim == null)
                //First claim
                return false;
            else
            {
                if ((previousClaim.AuditOnhold != null) && ((bool)previousClaim.AuditOnhold))
                    return true;
                else
                    return false;
            }
        }
        public bool IsFutureClaimOnHold(int vendorId, DateTime claimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;

            var claimList = dbContext.Claims.Where(c => c.ParticipantId == vendorId && c.ClaimPeriod.StartDate > claimPeriodDate && (c.Status == "Under Review" || c.Status == "Submitted") && c.ClaimOnhold == true);

            return claimList.Any();
        }

        public bool IsFutureAuditOnHold(int vendorId, DateTime claimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;

            var claimList = dbContext.Claims.Where(c => c.ParticipantId == vendorId && c.ClaimPeriod.StartDate > claimPeriodDate && (c.Status == "Under Review" || c.Status == "Submitted") && c.AuditOnhold == true);

            return claimList.Any();
        }

        public List<int> GetTransactionItemIds(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transactionItem in dbContext.TransactionItems.AsNoTracking()
                        on claimdetail.TransactionId equals transactionItem.TransactionId
                        where claimdetail.ClaimId == claimId
                        select transactionItem.ItemID;
            return query.Distinct().ToList();
        }

        public IEnumerable<string> GetTransactionItemShortName(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transactionItem in dbContext.TransactionItems.AsNoTracking()
                        on claimdetail.TransactionId equals transactionItem.TransactionId
                        join item in dbContext.Items.AsNoTracking()
                        on transactionItem.ItemID equals item.ID
                        where claimdetail.ClaimId == claimId && transactionItem.Quantity > 0
                        select item.ShortName;
            return query.ToList().Distinct();
        }


        public bool HasUnreviewedTransactions(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.ProcessingStatus == "Unreviewed"
                        select transaction.ID;
            return query.Any();
        }

        public bool TireOriginCheckForAutoApproval(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimTireOrigins = dbContext.ClaimTireOrigins.Where(c => c.ClaimId == claimId).ToList();
            //Tires dropped  off by a consumer/ resident or farmer 'NEEDS LOG'
            //Tires picked up from another business 'NEEDS LOG'
            //Tires dropped off by another business 'NEEDS LOG'
            //Tires removed from a purchased vehicle / equipment recycling(i.e Auto Dismantlers) 'NEEDS LOG'


            var hasTiresGenerated = claimTireOrigins.Any(c => c.TireOriginValue == 1);
            var hasTiresFromUnknownSource = claimTireOrigins.Any(c => c.TireOriginValue == 2);
            var hasTiresTiresWereCollectedFromApprovedOtsSubCollectors = claimTireOrigins.Any(c => c.TireOriginValue == 3);

            var hasTiresDroppedOffByConsumer = claimTireOrigins.Any(c => c.TireOriginValue == 4);
            var hasTiresDroppedOffByAnother = claimTireOrigins.Any(c => c.TireOriginValue == 5);
            var hasTiresPickedupFromAnother = claimTireOrigins.Any(c => c.TireOriginValue == 6);
            var invalidAutoDismanterTireTypes = new string[] { "MT", "IND", "SOTR", "MOTR", "LOTR", "GOTR" };
            var hasTiresRemovedFromPurchased = claimTireOrigins.Any(c => c.TireOriginValue == 9 && Array.IndexOf(invalidAutoDismanterTireTypes, c.Item.ShortName) > -1);
            var hasHostedCollection = claimTireOrigins.Any(c => c.TireOriginValue == 10);

            //OTSTM2-441
            var hasTiresRemovedOwnedOrLeasedVehicles = claimTireOrigins.Any(c => c.TireOriginValue == 7);
            //OTSTM2-441
            if (hasTiresGenerated || hasTiresFromUnknownSource || hasTiresTiresWereCollectedFromApprovedOtsSubCollectors || hasTiresDroppedOffByConsumer || hasTiresDroppedOffByAnother || hasTiresPickedupFromAnother || hasTiresRemovedFromPurchased || hasHostedCollection || hasTiresRemovedOwnedOrLeasedVehicles)
                return false;

            var tireGeneratedQty = claimTireOrigins.Where(c => c.TireOriginValue == 1).Sum(c => c.Quantity);
            var tireUnknownQty = claimTireOrigins.Where(c => c.TireOriginValue == 2).Sum(c => c.Quantity);
            var totalQty = tireGeneratedQty + tireUnknownQty;
            if (totalQty > 0)
            {
                var query = from claimdetail in dbContext.ClaimDetails
                            join transaction in dbContext.Transactions
                            on claimdetail.TransactionId equals transaction.ID
                            where claimdetail.ClaimId == claimId && transaction.IsGenerateTires
                            select transaction;
                int totalOutBoundQty = 0;
                query.ToList().ForEach(c =>
                {
                    totalOutBoundQty = totalOutBoundQty + c.TransactionItems.Sum(i => (int)i.Quantity);
                });
                if (totalQty != totalOutBoundQty)
                {
                    return false;
                }
            }
            return true;
        }
        public bool AllTransactionIsReviewed(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.ProcessingStatus == "Unreviewed"
                        select transaction.ID;
            return !query.Any();
        }

        public bool AllTransactionIsApproved(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && (transaction.ProcessingStatus == "Unreviewed")
                        select transaction.ID;
            return !query.Any();
        }

        public void Detach(object entity)
        {
            (((IObjectContextAdapter)context).ObjectContext).Detach(entity);
        }

        public List<ClaimDetailViewModel> LoadClaimDetailsForRPMGP(List<int> claimIds, List<string> transactionTypes, List<Item> items)
        {
            //Pull all PTR transactions from outgoing vendor
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimIds.Contains(claimdetail.ClaimId) && transaction.ProcessingStatus != "Invalidated" && transactionTypes.Contains(transaction.TransactionTypeCode) && transaction.OutgoingId != null
                        select new
                        {
                            OutgoingId = transaction.OutgoingId,
                            ClaimPeriodStartDate = claimdetail.Claim.ClaimPeriod.StartDate
                        };
            var result = new List<ClaimDetailViewModel>();
            query.Distinct().ToList().ForEach(c =>
            {
                var secondQuery = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                                  join transaction in dbContext.Transactions.AsNoTracking()
                                  on claimdetail.TransactionId equals transaction.ID
                                  where claimdetail.Claim.Status == "Approved" && claimdetail.Claim.ParticipantId == c.OutgoingId && DbFunctions.TruncateTime(claimdetail.Claim.ClaimPeriod.StartDate) == c.ClaimPeriodStartDate.Date && transaction.ProcessingStatus != "Invalidated" && transaction.TransactionTypeCode == "PTR"
                                  select new ClaimDetailViewModel
                                  {
                                      Direction = claimdetail.Direction,
                                      TransactionType = transaction.TransactionTypeCode,
                                      IsEligible = transaction.IsEligible,
                                      TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                                      Transaction = transaction,
                                      TransactionAdjustments = transaction.TransactionAdjustments,
                                      ClaimId = claimdetail.ClaimId
                                  };
                var queryResult = secondQuery.ToList();
                if (queryResult.Count == 0)
                {
                    var previousClaimStartDate = c.ClaimPeriodStartDate.AddMonths(-1);
                    var thirdQuery = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                                     join transaction in dbContext.Transactions.AsNoTracking()
                                     on claimdetail.TransactionId equals transaction.ID
                                     where claimdetail.Claim.Status == "Approved" && claimdetail.Claim.ParticipantId == c.OutgoingId && DbFunctions.TruncateTime(claimdetail.Claim.ClaimPeriod.StartDate) == previousClaimStartDate.Date && transaction.ProcessingStatus != "Invalidated" && transaction.TransactionTypeCode == "PTR"
                                     select new ClaimDetailViewModel
                                     {
                                         Direction = claimdetail.Direction,
                                         TransactionType = transaction.TransactionTypeCode,
                                         IsEligible = transaction.IsEligible,
                                         TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                                         Transaction = transaction,
                                         TransactionAdjustments = transaction.TransactionAdjustments,
                                         ClaimId = claimdetail.ClaimId
                                     };
                    queryResult = thirdQuery.ToList();
                }

                //Apply transaction adjustment
                queryResult.ForEach(q =>
                {
                    var transactionAdjustment = q.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                    if (transactionAdjustment != null)
                    {
                        q.TransactionItems = transactionAdjustment.TransactionItems;
                        q.IsEligible = transactionAdjustment.IsEligible;
                    }

                    CalculateTransactionItems(items, q);
                });
                result.AddRange(queryResult);
            });
            return result;
        }

        public List<ClaimDetailViewModel> LoadClaimDetailsForGP(List<int> claimIds, List<string> transactionTypes, List<Item> items)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimIds.Contains(claimdetail.ClaimId) && transaction.ProcessingStatus != "Invalidated" && transactionTypes.Contains(transaction.TransactionTypeCode)
                        select new ClaimDetailViewModel
                        {
                            Direction = claimdetail.Direction,
                            TransactionType = transaction.TransactionTypeCode,
                            IsEligible = transaction.IsEligible,
                            TransactionItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null).ToList(),
                            Transaction = transaction,
                            TransactionAdjustments = transaction.TransactionAdjustments,
                            ClaimId = claimdetail.ClaimId
                        };

            var result = query.ToList();
            //Apply transaction adjustment
            result.ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    c.TransactionItems = transactionAdjustment.TransactionItems;
                    c.IsEligible = transactionAdjustment.IsEligible;
                }

                CalculateTransactionItems(items, c);
            });

            return result;
        }

        public List<ClaimDetailViewModel> LoadClaimDetailsForCalculation(int claimId, List<Item> items)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.ProcessingStatus != "Invalidated"
                        select new ClaimDetailViewModel
                        {
                            Direction = claimdetail.Direction,
                            TransactionType = transaction.TransactionTypeCode,
                            IsEligible = transaction.IsEligible,
                            TransactionItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null).ToList(),
                            Transaction = transaction,
                            TransactionAdjustments = transaction.TransactionAdjustments,
                            ScaleTickets = transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).ToList()
                        };

            var result = query.ToList();

            result.ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    c.TransactionItems = transactionAdjustment.TransactionItems;
                    c.Transaction.OnRoadWeight = transactionAdjustment.OnRoadWeight;
                    c.Transaction.OffRoadWeight = transactionAdjustment.OffRoadWeight;
                    c.Transaction.EstimatedOnRoadWeight = transactionAdjustment.EstimatedOnRoadWeight;
                    c.Transaction.EstimatedOffRoadWeight = transactionAdjustment.EstimatedOffRoadWeight;
                    c.ScaleTickets = transactionAdjustment.ScaleTickets;
                    c.Transaction.IsGenerateTires = transactionAdjustment.IsGenerateTires;
                    c.Transaction.IsEligible = transactionAdjustment.IsEligible;
                    c.Transaction.MaterialType = transactionAdjustment.MaterialType;
                    c.Transaction.PaymentEligible = transactionAdjustment.PaymentEligible;
                    c.Transaction.PaymentEligibleOther = transactionAdjustment.PaymentEligibleOther;
                    c.Transaction.TireMarketType = transactionAdjustment.TireMarketType;
                    c.IsEligible = transactionAdjustment.IsEligible;
                }

                //Calculating transaction items here
                CalculateTransactionItemsForCalculation(items, c);
            });

            return result;
        }

        public List<ClaimDetailViewModel> LoadClaimDetailsForReport(int claimId, List<Item> items)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId
                        select new ClaimDetailViewModel
                        {
                            Direction = claimdetail.Direction,
                            TransactionType = transaction.TransactionTypeCode,
                            IsEligible = transaction.IsEligible,
                            TransactionItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null).ToList(),
                            Transaction = transaction,
                            TransactionAdjustments = transaction.TransactionAdjustments,
                            ScaleTickets = transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).ToList()
                        };

            var result = query.ToList();

            result.ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    c.TransactionItems = transactionAdjustment.TransactionItems;
                    c.Transaction.OnRoadWeight = transactionAdjustment.OnRoadWeight;
                    c.Transaction.OffRoadWeight = transactionAdjustment.OffRoadWeight;
                    c.Transaction.EstimatedOnRoadWeight = transactionAdjustment.EstimatedOnRoadWeight;
                    c.Transaction.EstimatedOffRoadWeight = transactionAdjustment.EstimatedOffRoadWeight;
                    c.ScaleTickets = transactionAdjustment.ScaleTickets;
                    c.Transaction.IsGenerateTires = transactionAdjustment.IsGenerateTires;
                    c.Transaction.IsEligible = transactionAdjustment.IsEligible;
                    c.Transaction.MaterialType = transactionAdjustment.MaterialType;
                    c.Transaction.PaymentEligible = transactionAdjustment.PaymentEligible;
                    c.Transaction.PaymentEligibleOther = transactionAdjustment.PaymentEligibleOther;
                    c.Transaction.TireMarketType = transactionAdjustment.TireMarketType;
                    c.IsEligible = transactionAdjustment.IsEligible;
                }

                //Calculating transaction items here
                CalculateTransactionItemsForCalculation(items, c);
            });

            return result;
        }

        public List<ClaimDetailViewModel> LoadClaimDetailsForReport(int claimId, int transactionId, List<Item> items)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && claimdetail.TransactionId == transactionId
                        select new ClaimDetailViewModel
                        {
                            Direction = claimdetail.Direction,
                            TransactionType = transaction.TransactionTypeCode,
                            IsEligible = transaction.IsEligible,
                            TransactionItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null).ToList(),
                            Transaction = transaction,
                            TransactionAdjustments = transaction.TransactionAdjustments,
                            ScaleTickets = transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).ToList()
                        };

            var result = query.ToList();

            result.ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    c.TransactionItems = transactionAdjustment.TransactionItems;
                    c.Transaction.OnRoadWeight = transactionAdjustment.OnRoadWeight;
                    c.Transaction.OffRoadWeight = transactionAdjustment.OffRoadWeight;
                    c.Transaction.EstimatedOnRoadWeight = transactionAdjustment.EstimatedOnRoadWeight;
                    c.Transaction.EstimatedOffRoadWeight = transactionAdjustment.EstimatedOffRoadWeight;
                    c.ScaleTickets = transactionAdjustment.ScaleTickets;
                    c.Transaction.IsGenerateTires = transactionAdjustment.IsGenerateTires;
                    c.Transaction.IsEligible = transactionAdjustment.IsEligible;
                    c.Transaction.MaterialType = transactionAdjustment.MaterialType;
                    c.Transaction.PaymentEligible = transactionAdjustment.PaymentEligible;
                    c.Transaction.PaymentEligibleOther = transactionAdjustment.PaymentEligibleOther;
                    c.Transaction.TireMarketType = transactionAdjustment.TireMarketType;
                    c.IsEligible = transactionAdjustment.IsEligible;
                }

                //Calculating transaction items here
                CalculateTransactionItemsForCalculation(items, c);
            });

            return result;
        }
        /// <summary>
        /// Load Claim Details
        /// </summary>
        /// <param name="claimId"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<ClaimDetailViewModel> LoadClaimDetails(int claimId, List<Item> items)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking()
                        on claimdetail.TransactionId equals transaction.ID
                        where claimdetail.ClaimId == claimId && transaction.ProcessingStatus != "Invalidated"
                        select new ClaimDetailViewModel
                        {
                            Direction = claimdetail.Direction,
                            TransactionType = transaction.TransactionTypeCode,
                            IsEligible = transaction.IsEligible,
                            TransactionItems = transaction.TransactionItems.Where(c => c.TransactionAdjustmentId == null).ToList(),
                            Transaction = transaction,
                            TransactionAdjustments = transaction.TransactionAdjustments,
                            ScaleTickets = transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).ToList()
                        };

            var result = query.ToList();

            result.ForEach(c =>
            {
                var transactionAdjustment = c.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString());
                if (transactionAdjustment != null)
                {
                    c.TransactionItems = transactionAdjustment.TransactionItems;
                    c.Transaction.OnRoadWeight = transactionAdjustment.OnRoadWeight;
                    c.Transaction.OffRoadWeight = transactionAdjustment.OffRoadWeight;
                    c.Transaction.EstimatedOnRoadWeight = transactionAdjustment.EstimatedOnRoadWeight;
                    c.Transaction.EstimatedOffRoadWeight = transactionAdjustment.EstimatedOffRoadWeight;
                    c.ScaleTickets = transactionAdjustment.ScaleTickets;
                    c.Transaction.IsGenerateTires = transactionAdjustment.IsGenerateTires;
                    c.Transaction.IsEligible = transactionAdjustment.IsEligible;
                    c.Transaction.MaterialType = transactionAdjustment.MaterialType;
                    c.Transaction.PaymentEligible = transactionAdjustment.PaymentEligible;
                    c.Transaction.PaymentEligibleOther = transactionAdjustment.PaymentEligibleOther;
                    c.Transaction.TireMarketType = transactionAdjustment.TireMarketType;
                    c.IsEligible = transactionAdjustment.IsEligible;
                }

                //Calculating transaction items here
                CalculateTransactionItems(items, c);
            });

            return result;

        }

        private void CalculateTransactionItemsForCalculation(List<Item> items, ClaimDetailViewModel claimDetail)
        {
            claimDetail.EstimatedOnRoad = 0;
            claimDetail.EstimatedOffRoad = 0;
            claimDetail.OnRoad = 0;
            claimDetail.OffRoad = 0;
            claimDetail.TransactionItems.ForEach(c =>
            {
                var item = items.FirstOrDefault(i => i.ID == c.ItemID);
                if (item.ItemType == 1)
                {
                    claimDetail.EstimatedOnRoad += c.AverageWeight;
                    claimDetail.OnRoad += c.ActualWeight;
                }
                if (item.ItemType == 2)
                {
                    claimDetail.EstimatedOffRoad += c.AverageWeight;
                    claimDetail.OffRoad += c.ActualWeight;
                }

                //Item Level Distribution
                if (item.ShortName == TreadMarksConstants.PLT)
                {
                    claimDetail.PLTActualWeight = c.ActualWeight;
                    claimDetail.PLTAverageWeight = c.AverageWeight;
                    claimDetail.PLTQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.MT)
                {
                    claimDetail.MTActualWeight = c.ActualWeight;
                    claimDetail.MTAverageWeight = c.AverageWeight;
                    claimDetail.MTQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.AGLS)
                {
                    claimDetail.AGLSActualWeight = c.ActualWeight;
                    claimDetail.AGLSAverageWeight = c.AverageWeight;
                    claimDetail.AGLSQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.IND)
                {
                    claimDetail.INDActualWeight = c.ActualWeight;
                    claimDetail.INDAverageWeight = c.AverageWeight;
                    claimDetail.INDQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.SOTR)
                {
                    claimDetail.SOTRActualWeight = c.ActualWeight;
                    claimDetail.SOTRAverageWeight = c.AverageWeight;
                    claimDetail.SOTRQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.MOTR)
                {
                    claimDetail.MOTRActualWeight = c.ActualWeight;
                    claimDetail.MOTRAverageWeight = c.AverageWeight;
                    claimDetail.MOTRQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.LOTR)
                {
                    claimDetail.LOTRActualWeight = c.ActualWeight;
                    claimDetail.LOTRAverageWeight = c.AverageWeight;
                    claimDetail.LOTRQty = c.Quantity ?? 0;
                }

                if (item.ShortName == TreadMarksConstants.GOTR)
                {
                    claimDetail.GOTRActualWeight = c.ActualWeight;
                    claimDetail.GOTRAverageWeight = c.AverageWeight;
                    claimDetail.GOTRQty = c.Quantity ?? 0;
                }
            });

        }

        private void CalculateTransactionItems(List<Item> items, ClaimDetailViewModel claimDetail)
        {
            claimDetail.EstimatedOnRoad = 0;
            claimDetail.EstimatedOffRoad = 0;
            claimDetail.OnRoad = 0;
            claimDetail.OffRoad = 0;
            claimDetail.TransactionItems.ForEach(c =>
            {
                var item = items.FirstOrDefault(i => i.ID == c.ItemID);
                if (item.ItemType == 1)
                {
                    claimDetail.EstimatedOnRoad += c.AverageWeight;
                    claimDetail.OnRoad += c.ActualWeight;
                }
                if (item.ItemType == 2)
                {
                    claimDetail.EstimatedOffRoad += c.AverageWeight;
                    claimDetail.OffRoad += c.ActualWeight;
                }
            });
        }

        public bool IsGenerateTires(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claimdtail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking() on claimdtail.TransactionId equals transaction.ID
                        where claimdtail.ClaimId == claimId && transaction.IsGenerateTires == true
                        select transaction;

            return query.Any();
        }

        public bool IncompleteTransaction(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = from claimdtail in dbContext.ClaimDetails.AsNoTracking()
                        join transaction in dbContext.Transactions.AsNoTracking() on claimdtail.TransactionId equals transaction.ID
                        where claimdtail.ClaimId == claimId && transaction.Status == TransactionStatus.Incomplete.ToString()
                        select transaction;

            return query.Any();
        }

        public List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimInventoryAdjustments.AsNoTracking().Include(c => c.ClaimInventoryAdjustItems).Where(c => c.ClaimId == claimId).ToList();
        }

        public List<ClaimYardCountSubmission> LoadYardCountSubmission(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.ClaimYardCountSubmissions.Where(c => c.ClaimId == claimId).ToList();
        }

        public decimal LoadVendorSortYardCapacity(int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var vendorStorageSites = dbContext.VendorStorageSites.AsNoTracking().Where(c => c.VendorID == vendorId).ToList();
            var totalCapacity = 0m;
            vendorStorageSites.ForEach(c =>
            {
                if (c.UnitType == TreadMarksConstants.Ton)
                {
                    totalCapacity += DataConversion.ConvertTonToKg(c.MaxStorageCapacity);
                }
                else
                {
                    if (c.UnitType == TreadMarksConstants.Lb)
                        totalCapacity += DataConversion.ConvertLbToKg(c.MaxStorageCapacity);
                    else
                        totalCapacity += Convert.ToDecimal(c.MaxStorageCapacity);
                }
            });
            return totalCapacity;
        }

        public bool HasAnyInternalAdjustment(int claimId)
        {
            //check two tables ClaimInventoryAdjustments and ClaimInternalPaymentAdjusts
            var dbContext = context as ClaimsBoundedContext;
            var queryClaimInventoryAdjustment = from inventoryAdjustment in dbContext.ClaimInventoryAdjustments.AsNoTracking()
                                                where inventoryAdjustment.ClaimId == claimId
                                                select inventoryAdjustment.ID;

            var queryClaimInternalPaymentAdjust = from internalPaymentAdjust in dbContext.ClaimInternalPaymentAdjusts.AsNoTracking()
                                                  where internalPaymentAdjust.ClaimId == claimId
                                                  select internalPaymentAdjust.ID;
            return (queryClaimInventoryAdjustment.Any() || queryClaimInternalPaymentAdjust.Any());
        }

        public PaginationDTO<ClaimInternalAdjustment, int> LoadClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            var dbContext = context as ClaimsBoundedContext;
            var queryInventoryAdjustment = from inventoryAdjustment in dbContext.ClaimInventoryAdjustments.AsNoTracking()
                                           where inventoryAdjustment.ClaimId == claimId
                                           select new ClaimInternalAdjustment
                                           {
                                               InternalAdjustmentId = inventoryAdjustment.ID,
                                               AdjustmentDate = inventoryAdjustment.AdjustDate,
                                               AdjustmentBy = inventoryAdjustment.AdjustUser.FirstName + " " + inventoryAdjustment.AdjustUser.LastName,
                                               InternalAdjustmentType = (InternalAdjustmentType)inventoryAdjustment.AdjustmentType,
                                               //OTSTM2-668
                                               Note = inventoryAdjustment.ClaimInternalAdjustmentNotes.FirstOrDefault() != null ? inventoryAdjustment.ClaimInternalAdjustmentNotes.FirstOrDefault().Note : string.Empty,
                                               InternalNotes = inventoryAdjustment.ClaimInternalAdjustmentNotes.OrderByDescending(c => c.CreatedDate).ToList()
                                           };
            var queryPaymentAdjustment = from claimInternalPaymentAdjust in dbContext.ClaimInternalPaymentAdjusts.AsNoTracking()
                                         where claimInternalPaymentAdjust.ClaimId == claimId
                                         select new ClaimInternalAdjustment
                                         {
                                             InternalAdjustmentId = claimInternalPaymentAdjust.ID,
                                             AdjustmentDate = claimInternalPaymentAdjust.AdjustDate,
                                             AdjustmentBy = claimInternalPaymentAdjust.AdjustUser.FirstName + " " + claimInternalPaymentAdjust.AdjustUser.LastName,
                                             InternalAdjustmentType = InternalAdjustmentType.Payment,
                                             //OTSTM2-668
                                             Note = claimInternalPaymentAdjust.ClaimPaymentAdjustmentNotes.FirstOrDefault() != null ? claimInternalPaymentAdjust.ClaimPaymentAdjustmentNotes.FirstOrDefault().Note : string.Empty,
                                             InternalNotes = claimInternalPaymentAdjust.ClaimPaymentAdjustmentNotes.OrderByDescending(c => c.CreatedDate).ToList()
                                         };
            var query = queryInventoryAdjustment.ToList().Union(queryPaymentAdjustment);

            List<ClaimInternalAdjustment> returnVal = query.ToList();
            if (!isStaff)
            {
                //Change to System
                returnVal.ForEach(c =>
                {
                    c.AdjustmentBy = "STAFF";
                });
            }

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                returnVal = returnVal.Where(c => c.InternalAdjustmentType.ToString().ToLower().Contains(searchText.ToLower()) || c.AdjustmentBy.ToLower().Contains(searchText.ToLower()) || c.AdjustmentDate.ToString(TreadMarksConstants.DateFormat).Contains(searchText)).ToList();
            }

            //Applying sorting
            returnVal = sortDirection == "asc" ? returnVal.AsQueryable<ClaimInternalAdjustment>().OrderBy(orderBy).ToList() : returnVal.AsQueryable<ClaimInternalAdjustment>().OrderBy(orderBy + " descending").ToList();

            var totalRecords = returnVal.Count();
            var result = returnVal.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ClaimInternalAdjustment, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public bool HasTransactionAdjustment(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from transactionAdjustment in dbContext.TransactionAdjustments
                        join claimDetail in dbContext.ClaimDetails on transactionAdjustment.OriginalTransactionId equals claimDetail.TransactionId
                        where claimDetail.ClaimId == claimId
                        select transactionAdjustment.ID;
            return query.Any();
        }
        public PaginationDTO<TransactionAdjustmentViewModel, int> LoadClaimTransactionAdjustment(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from transactionAdjustment in dbContext.TransactionAdjustments.AsNoTracking()
                        join claimDetail in dbContext.ClaimDetails on transactionAdjustment.OriginalTransactionId equals claimDetail.TransactionId
                        where claimDetail.ClaimId == claimId
                        select new TransactionAdjustmentViewModel
                        {
                            TransactionId = claimDetail.TransactionId,
                            TransactionAdjustmentId = transactionAdjustment.ID,
                            AdjustmentDate = transactionAdjustment.AdjustmentDate,
                            StartedBy = transactionAdjustment.StartUser.FirstName + " " + transactionAdjustment.StartUser.LastName,
                            Status = transactionAdjustment.Status,
                            TransactionDate = transactionAdjustment.Transaction.TransactionDate,
                            TransactionNumber = transactionAdjustment.Transaction.FriendlyId.ToString(),
                            TransactionType = transactionAdjustment.Transaction.TransactionTypeCode,
                            CreatedVendorId = transactionAdjustment.CreatedVendorId ?? 0,
                            IsAdjustByStaff = transactionAdjustment.IsAdjustByStaff,
                        };

            //OTSTM2-1172 Collector claims should not list UCR transactions
            var claim = dbContext.Claims.Find(claimId);
            if (claim != null && claim.ClaimType == (int)ClaimType.Collector)
            {
                query = query.Where(x => x.TransactionType != "UCR");
            }

            //Applying search text
            List<TransactionAdjustmentViewModel> returnVal = query.ToList();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                returnVal = returnVal.Where(c => c.StartedBy.ToLower().Contains(searchText.ToLower())
                    || c.TransactionDate.ToString(TreadMarksConstants.DateFormat).Contains(searchText.ToLower())
                    || c.AdjustmentDate.ToString(TreadMarksConstants.DateFormat).Contains(searchText.ToLower())
                    || c.Status.ToLower().Contains(searchText.ToLower())
                    || c.TransactionNumber.Contains(searchText.ToLower())
                    || c.TransactionType.ToLower().Contains(searchText.ToLower())).ToList();
            }

            //Applying sorting
            returnVal = sortDirection == "asc" ? returnVal.AsQueryable().OrderBy<TransactionAdjustmentViewModel>(orderBy).ToList() : returnVal.AsQueryable().OrderBy<TransactionAdjustmentViewModel>(orderBy + " descending").ToList();

            var totalRecords = returnVal.Count();
            var result = returnVal.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<TransactionAdjustmentViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public bool IsTaxExempt(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from vendor in dbContext.Vendors
                        join claim in dbContext.Claims on vendor.ID equals claim.ParticipantId
                        where claim.ID == claimId
                        select vendor.IsTaxExempt;
            return query.FirstOrDefault();
        }

        public ClaimPaymentSummary LoadClaimPaymentSummary(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimPaymentSummaries.FirstOrDefault(c => c.Claim.ID == claimId);
        }

        public List<ClaimPayment> LoadClaimPayments(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimPayments.Where(c => c.ClaimID == claimId).ToList();
        }

        public List<ClaimInternalPaymentAdjust> LoadClaimPaymentInternalAdjusts(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimInternalPaymentAdjusts.Where(c => c.ClaimId == claimId).ToList();
        }

        public void SaveContext()
        {
            context.SaveChanges();
        }

        public void AddClaimYardCountSubmission(List<ClaimYardCountSubmission> yardCountSubmissions)
        {
            var dbContext = context as ClaimsBoundedContext;
            yardCountSubmissions.ForEach(c =>
            {
                dbContext.ClaimYardCountSubmissions.Add(c);
            });
            dbContext.SaveChanges();
        }

        public void UpdateClaimSummary(ClaimSummary claimSummary)
        {
            var dbContext = context as ClaimsBoundedContext;
            var dbClaimSummary = dbContext.ClaimSummaries.FirstOrDefault(c => c.Claim.ID == claimSummary.Claim.ID);
            if (dbClaimSummary != null)
            {
                dbClaimSummary.EligibleClosingOffRoad = claimSummary.EligibleClosingOffRoad;
                dbClaimSummary.EligibleClosingOnRoad = claimSummary.EligibleClosingOnRoad;
                dbClaimSummary.EligibleOpeningOffRoad = claimSummary.EligibleOpeningOffRoad;
                dbClaimSummary.EligibleOpeningOnRoad = claimSummary.EligibleOpeningOnRoad;
                dbClaimSummary.IneligibleClosingOffRoad = claimSummary.IneligibleClosingOffRoad;
                dbClaimSummary.IneligibleClosingOnRoad = claimSummary.IneligibleClosingOnRoad;
                dbClaimSummary.IneligibleOpeningOffRoad = claimSummary.IneligibleOpeningOffRoad;
                dbClaimSummary.IneligibleOpeningOnRoad = claimSummary.IneligibleOpeningOnRoad;
                dbClaimSummary.TotalClosing = claimSummary.TotalClosing;
                dbClaimSummary.TotalOpening = claimSummary.TotalOpening;
                dbClaimSummary.EligibleClosingOffRoadEstimated = claimSummary.EligibleClosingOffRoadEstimated;
                dbClaimSummary.EligibleClosingOnRoadEstimated = claimSummary.EligibleClosingOnRoadEstimated;
                dbClaimSummary.EligibleOpeningOffRoadEstimated = claimSummary.EligibleOpeningOffRoadEstimated;
                dbClaimSummary.EligibleOpeningOnRoadEstimated = claimSummary.EligibleOpeningOnRoadEstimated;
                dbClaimSummary.IneligibleClosingOffRoadEstimated = claimSummary.IneligibleClosingOffRoadEstimated;
                dbClaimSummary.IneligibleClosingOnRoadEstimated = claimSummary.IneligibleClosingOnRoadEstimated;
                dbClaimSummary.IneligibleOpeningOffRoadEstimated = claimSummary.IneligibleOpeningOffRoadEstimated;
                dbClaimSummary.IneligibleOpeningOnRoadEstimated = claimSummary.IneligibleOpeningOnRoadEstimated;
                dbClaimSummary.TotalClosingEstimated = claimSummary.TotalClosingEstimated;
                dbClaimSummary.TotalOpeningEstimated = claimSummary.TotalOpeningEstimated;

                dbClaimSummary.UpdatedDate = DateTime.UtcNow;
            }
            else
            {
                dbContext.ClaimSummaries.Add(claimSummary);
            }
            dbContext.SaveChanges();
        }

        public void UpdateClaimPayments(int claimId, List<ClaimPayment> claimPayments)
        {
            var dbContext = context as ClaimsBoundedContext;
            //delete all of current claim payments
            dbContext.ClaimPayments.RemoveRange(dbContext.ClaimPayments.Where(c => c.ClaimID == claimId));
            //Insert new claim payments
            claimPayments.ForEach(i =>
            {
                i.Weight = Math.Round(i.Weight, 4, MidpointRounding.AwayFromZero);//rounding for: modelBuilder.Entity<ClaimPayment>().Property(c =>c.Weight).HasPrecision(18, 4);
            });
            dbContext.ClaimPayments.AddRange(claimPayments);
            dbContext.SaveChanges();
        }

        public void UpdateClaimPaymentDetails(int claimId, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var dbContext = context as ClaimsBoundedContext;
            //delete all of current claim payments
            dbContext.ClaimPaymentDetails.RemoveRange(dbContext.ClaimPaymentDetails.Where(c => c.ClaimId == claimId));

            //Insert new claim payments
            dbContext.ClaimPaymentDetails.AddRange(claimPaymentDetails);
            dbContext.SaveChanges();
        }

        public void UpdateClaimInventoryDetails(int claimId, List<ClaimInventoryDetail> claimInventoryDetails)
        {
            var dbContext = context as ClaimsBoundedContext;
            //delete all of current claim inventory details
            dbContext.ClaimInventoryDetails.RemoveRange(dbContext.ClaimInventoryDetails.Where(c => c.ClaimId == claimId));
            if (claimInventoryDetails.Count > 0)
                dbContext.ClaimInventoryDetails.AddRange(claimInventoryDetails);
            dbContext.SaveChanges();
        }

        public void UpdateClaimInventoryDetails(int claimId, int transactionId, List<ClaimInventoryDetail> claimInventoryDetails)
        {
            using (var dbContext = new ClaimsBoundedContext())
            {
                //delete all of current claim inventory details
                dbContext.ClaimInventoryDetails.RemoveRange(dbContext.ClaimInventoryDetails.Where(c => c.ClaimId == claimId && c.TransactionId == transactionId));
                if (claimInventoryDetails.Count > 0)
                    dbContext.ClaimInventoryDetails.AddRange(claimInventoryDetails);
                dbContext.SaveChanges();
            }
        }

        #endregion Claims Summary

        #region Claims Period

        public Claim GetFirstOpenClaimByStartDate(int vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims.AsNoTracking()
                        where (claim.ParticipantId == vendorId) && (claim.Status == "Open")
                        orderby claim.ClaimPeriod.StartDate ascending
                        select claim;
            return query.FirstOrDefault();
        }

        #endregion Claims Period

        #region Claim Attachment

        public void AddClaimAttachments(int claimId, IEnumerable<AttachmentModel> attachments)
        {
            ConditionCheck.Bounds(claimId, "Argument is out of bounds", int.MaxValue, 0);
            var claim = entityStore.FindById(claimId);

            foreach (var attachmentModel in attachments)
            {
                var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
                claim.Attachments.Add(attachment);
            }

            entityStore.Update(claim);
        }

        public IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo = false)
        {
            ConditionCheck.Bounds(claimId, "Argument is out of bounds", int.MaxValue, 0);

            var attachments = entityStore.FindById(claimId).Attachments;
            var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachments);
            return attachmentModelList;
        }

        public AttachmentModel GetClaimAttachment(int claimId, string fileUniqueName)
        {
            ConditionCheck.Bounds(claimId, "Argument is out of bounds", int.MaxValue, 0);

            var attachment = entityStore.FindById(claimId).Attachments.Single(r => r.UniqueName == fileUniqueName);

            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveClaimAttachment(int claimId, string fileUniqueName)
        {
            ConditionCheck.Bounds(claimId, "Argument is out of bounds", int.MaxValue, 0);

            var currentContext = (context as ClaimsBoundedContext);
            var claim = currentContext.Claims.FirstOrDefault(a => a.ID == claimId);
            var attachment = claim.Attachments.FirstOrDefault(a => a.UniqueName == fileUniqueName);

            if (attachment != null)
            {
                claim.Attachments.Remove(attachment);
                //currentContext.Attachments.Remove(attachment);
                attachment.FileName = "DELETE";
                currentContext.Attachments.Attach(attachment);
                var attachmentEntry = currentContext.Entry(attachment);
                attachmentEntry.State = EntityState.Modified;
                attachmentEntry.Property(a => a.FileName).IsModified = true;

                currentContext.SaveChanges();
            }
        }

        #endregion Claim Attachment

        #region Collector Claims

        public List<ClaimTireOrigin> LoadClaimTireOrigins(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimTireOrigins.AsNoTracking().Where(c => c.ClaimId == claimId).ToList();
        }

        public List<ClaimSupportingDocument> LoadClaimSupportingDocuments(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimSupportingDocuments.AsNoTracking().Where(c => c.ClaimId == claimId).ToList();
        }

        public List<ClaimReuseTires> LoadClaimReuseTires(int claimId)
        {
            return (context as ClaimsBoundedContext).ClaimReuseTires.AsNoTracking().Where(c => c.ClaimId == claimId).ToList();
        }

        public void AddTireOrigin(int claimId, int tireOriginValue, List<ClaimTireOrigin> claimTireOrigins)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var dbClaimTireOrigins = dbContext.ClaimTireOrigins.Where(c => c.TireOriginValue == tireOriginValue && c.ClaimId == claimId).ToList();
            if (dbClaimTireOrigins.Count > 0)
            {
                //Delete the existing records
                dbContext.ClaimTireOrigins.RemoveRange(dbClaimTireOrigins);
            }
            //Add new records
            claimTireOrigins.ForEach(c =>
            {
                dbContext.ClaimTireOrigins.Add(c);
            });
            dbContext.SaveChanges();

        }
        public ClaimSupportingDocument AddClaimSupportingDocument(ClaimSupportingDocument ClaimSupportingDocument)
        {
            var dbContext = context as ClaimsBoundedContext;

            var oldSupportingDoc = dbContext.ClaimSupportingDocuments.Where(c => c.ClaimId == ClaimSupportingDocument.ClaimId && c.Category == ClaimSupportingDocument.Category && c.TypeCode == ClaimSupportingDocument.TypeCode).FirstOrDefault();
            if (oldSupportingDoc != null)
            {
                dbContext.ClaimSupportingDocuments.Remove(oldSupportingDoc);
            }
            dbContext.ClaimSupportingDocuments.Add(ClaimSupportingDocument);
            dbContext.SaveChanges();
            return ClaimSupportingDocument;

        }
        public void RemoveClaimSupportingDocument(int claimId, int tireOriginValue)
        {
            var dbContext = context as ClaimsBoundedContext;

            var oldSupportingDoc = dbContext.ClaimSupportingDocuments.Where(c => c.ClaimId == claimId && c.DefinitionValue == tireOriginValue).FirstOrDefault();
            if (oldSupportingDoc != null)
            {
                dbContext.ClaimSupportingDocuments.Remove(oldSupportingDoc);
            }
            dbContext.SaveChanges();
        }
        public void UpdateSupportingDocOption(int claimId, int defValue, string option)
        {
            var dbContext = context as ClaimsBoundedContext;

            var oldSupportingDoc = dbContext.ClaimSupportingDocuments.Where(c => c.ClaimId == claimId && c.DefinitionValue == defValue).FirstOrDefault();
            if (oldSupportingDoc != null)
            {
                oldSupportingDoc.SelectedOption = option;
            }
            dbContext.SaveChanges();
        }

        public void RemoveTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var dbClaimTireOrigins = dbContext.ClaimTireOrigins.Where(c => c.TireOriginValue == tireOriginItemRow.TireOriginValue && c.ClaimId == claimId).ToList();
            if (dbClaimTireOrigins.Count > 0)
            {
                //Delete the existing records
                dbContext.ClaimTireOrigins.RemoveRange(dbClaimTireOrigins);
                dbContext.SaveChanges();
            }
        }

        public void RemoveClaimProgress(int claimId)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var removeEntry = dbContext.ClaimProcesses.FirstOrDefault(i => i.Claim.ID == claimId);
            if (removeEntry != null)
            {
                dbContext.ClaimProcesses.Remove(removeEntry);
                dbContext.SaveChanges();
            }
        }

        public void RemoveClaimPaymentSummary(ClaimPaymentSummary claimPaymentSummary)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var removeEntry = dbContext.ClaimPaymentSummaries.FirstOrDefault(i => i.ID == claimPaymentSummary.ID);
            if (removeEntry != null)
            {
                dbContext.ClaimPaymentSummaries.Remove(removeEntry);
                dbContext.SaveChanges();
            }
        }

        public void AddClaimReuseTires(int claimId, List<ClaimReuseTires> claimReuseTires)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var dbClaimReuseTires = dbContext.ClaimReuseTires.Where(c => c.ClaimId == claimId).ToList();
            if (dbClaimReuseTires.Count > 0)
            {
                //Delete the existing records
                dbContext.ClaimReuseTires.RemoveRange(dbClaimReuseTires);
            }
            //Add new records
            claimReuseTires.ForEach(c =>
            {
                dbContext.ClaimReuseTires.Add(c);
            });

            dbContext.SaveChanges();
        }

        public void RemoveClaimReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            var dbContext = (context as ClaimsBoundedContext);
            var dbClaimReuseTires = dbContext.ClaimReuseTires.Where(c => c.ClaimId == claimId).ToList();
            if (dbClaimReuseTires.Count > 0)
            {
                //Delete the existing records
                dbContext.ClaimReuseTires.RemoveRange(dbClaimReuseTires);
                dbContext.SaveChanges();
            }
        }

        #endregion Collector Claims

        #region Claim Internal Notes
        public List<ClaimNote> LoadClaimInternalNotes(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = dbContext.ClaimNotes.AsNoTracking().Include(c => c.User).Where(cl => cl.ClaimId == claimId);

            return query.ToList();

        }

        public ClaimNote AddClaimNote(ClaimNote claimNote)
        {
            var dbContext = context as ClaimsBoundedContext;
            dbContext.ClaimNotes.Add(claimNote);
            dbContext.SaveChanges();
            return claimNote;
        }

        public List<InternalNoteViewModel> LoadClaimNotesForExport(int claimId, string searchText, string sortcolumn, bool sortReverse)
        {
            var dbContext = context as ClaimsBoundedContext;

            var query = dbContext.ClaimNotes.AsNoTracking().Where(cl => cl.ClaimId == claimId)
                .Join(dbContext.Users.AsNoTracking(), notes => notes.UserId, user => user.ID, (notes, user) => new InternalNoteViewModel
                {
                    Note = notes.Note,
                    AddedOn = notes.CreatedDate,
                    AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                });

            if (!string.IsNullOrEmpty(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    query = query.Where(c => c.Note.ToLower().Contains(searchText.ToLower()) || c.AddedBy.ToLower().Contains(searchText.ToLower()));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;

            if (!sortReverse)
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn);
            }
            else
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn + " descending");
            }

            return query.ToList();
        }

        public void AddClaimNotes(List<ClaimNote> claimNotes)
        {
            var dbContext = context as ClaimsBoundedContext;
            dbContext.ClaimNotes.AddRange(claimNotes);
            dbContext.SaveChanges();
        }

        public List<Claim> LoadApprovedWithoutBatch(int claimType, bool includeNegativeAmounts)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.Claims.AsNoTracking()
                .Where(c => c.ClaimType == claimType && c.Status.ToLower() == "approved" &&
                    !(c.InBatch ?? false) &&
                    ((includeNegativeAmounts && (c.ClaimsAmountTotal.HasValue && c.ClaimsAmountTotal != 0)) || (!includeNegativeAmounts && (c.ClaimsAmountTotal.HasValue && c.ClaimsAmountTotal > 0))));
            return query.ToList();
        }


        public Claim GetPreviousApprovedClaim(int claimType, DateTime claimPeriodDate, int participantId = -1)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.Include(c => c.ClaimSummary).Where(c => c.ClaimPeriod.StartDate < claimPeriodDate && c.ClaimType == claimType && c.Status.ToLower() == "approved" && (c.ParticipantId == participantId || participantId == -1)).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
        }

        #endregion

        public bool IsClaimPosted(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var gpBatchEnrty = dbContext.GpBatchEntries.FirstOrDefault(c => c.ClaimId == claimId);
            if (gpBatchEnrty != null)
            {
                if (gpBatchEnrty.GpiStatusID.Trim() == "P2")
                    return true;
            }
            return false;
        }

        public void UpdateClaimPaymentSummary(GpPaymentSummary gpPaymentSummary, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimPaymentSummary = dbContext.ClaimPaymentSummaries.FirstOrDefault(c => c.Claim.ID == claimId);
            if (claimPaymentSummary != null)
            {
                if (claimPaymentSummary.ChequeNumber == null)
                {
                    claimPaymentSummary.AmountPaid = Math.Round(gpPaymentSummary.AmountPaid, 3, MidpointRounding.AwayFromZero);
                    claimPaymentSummary.ChequeNumber = gpPaymentSummary.ChequeNumber;
                    claimPaymentSummary.PaidDate = gpPaymentSummary.PaidDate;
                    dbContext.SaveChanges();

                }
                else
                {
                    if (claimPaymentSummary.ChequeNumber.Trim().ToLower() != gpPaymentSummary.ChequeNumber.Trim().ToLower())
                    {
                        claimPaymentSummary.AmountPaid = Math.Round(gpPaymentSummary.AmountPaid, 3, MidpointRounding.AwayFromZero);
                        claimPaymentSummary.ChequeNumber = gpPaymentSummary.ChequeNumber;
                        claimPaymentSummary.PaidDate = gpPaymentSummary.PaidDate;
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        //OTSTM2-484
        public ClaimPaymentSummary UpdateAndReturnClaimPaymentSummary(GpPaymentSummary gpPaymentSummary, int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimPaymentSummary = dbContext.ClaimPaymentSummaries.FirstOrDefault(c => c.Claim.ID == claimId);
            if (claimPaymentSummary != null)
            {
                if (claimPaymentSummary.ChequeNumber == null)
                {
                    claimPaymentSummary.AmountPaid = Math.Round(gpPaymentSummary.AmountPaid, 3, MidpointRounding.AwayFromZero);
                    claimPaymentSummary.ChequeNumber = gpPaymentSummary.ChequeNumber;
                    claimPaymentSummary.PaidDate = gpPaymentSummary.PaidDate;
                    dbContext.SaveChanges();

                }
                else
                {
                    if (claimPaymentSummary.ChequeNumber.Trim().ToLower() != gpPaymentSummary.ChequeNumber.Trim().ToLower())
                    {
                        claimPaymentSummary.AmountPaid = Math.Round(gpPaymentSummary.AmountPaid, 3, MidpointRounding.AwayFromZero);
                        claimPaymentSummary.ChequeNumber = gpPaymentSummary.ChequeNumber;
                        claimPaymentSummary.PaidDate = gpPaymentSummary.PaidDate;
                        dbContext.SaveChanges();
                    }
                }
            }
            return claimPaymentSummary;
        }

        public void UpdateClaimsForGPBatch(List<int> claimIds, string batchNumber)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        where claimIds.Contains(claim.ID)
                        select claim;
            query.ToList().ForEach(c =>
            {
                c.InBatch = true;
                if (c.ClaimPaymentSummary != null)
                {
                    c.ClaimPaymentSummary.BatchNumber = batchNumber;
                }
            });
            dbContext.SaveChanges();
        }

        public List<Claim> FindApprovedBankClaimIds(List<int> claimIds)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        where claimIds.Contains(claim.ID) /*&& (claim.Participant.BankInformations.FirstOrDefault() != null)
                        && (claim.Participant.BankInformations.FirstOrDefault().ReviewStatus == "Approved")*/
                        select claim;
            return query.ToList();
        }

        public List<int> GetNextClaimsIds(int vendorId, DateTime claimStartDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        where claim.ParticipantId == vendorId && DbFunctions.TruncateTime(claim.ClaimPeriod.StartDate) > claimStartDate.Date
                        orderby claim.ClaimPeriod.StartDate
                        select claim.ID;
            return query.ToList();
        }

        public IEnumerable<int> GetAllClaimsToReCalculate(int? vendorType = null)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimIds = new List<int>();
            if (vendorType != null)
            {
                claimIds = dbContext.Claims.Where(i => i.Participant.VendorType == vendorType).OrderBy(i => i.ID).Select(i => i.ID).ToList();
            }
            else
            {
                claimIds = dbContext.Claims.OrderBy(i => i.ID).Select(i => i.ID).ToList();
            }
            return claimIds;
        }
        public IEnumerable<int> GetAllClaimsToReCalculateByStatus(int? vendorType = null, string status = "")
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimIds = new List<int>();
            if (vendorType != null)
            {
                if (string.IsNullOrEmpty(status))
                {
                    claimIds = dbContext.Claims.Where(i => i.Participant.VendorType == vendorType).OrderBy(i => i.ID).Select(i => i.ID).ToList();
                }
                else
                {
                    claimIds = dbContext.Claims.Where(i => i.Participant.VendorType == vendorType && i.Status == status).OrderBy(i => i.ID).Select(i => i.ID).ToList();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(status))
                {
                    claimIds = dbContext.Claims.OrderBy(i => i.ID).Select(i => i.ID).ToList();
                }
                else
                {
                    claimIds = dbContext.Claims.Where(i => i.Status == status).OrderBy(i => i.ID).Select(i => i.ID).ToList();
                }
            }
            return claimIds;
        }

        public List<int> getAllVenderByType(int vType)
        {
            var dbContext = context as ClaimsBoundedContext;
            var result = dbContext.Vendors.Where(i => i.VendorType == vType).Select(i => i.ID).ToList();
            return result;
        }



        public List<int> GetMissingCollectorClaimIds()
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.Where(c => c.ClaimType == 2 && c.ClaimPeriodId == 310 && c.ClaimDetails.Count > 0).Select(c => c.ID).ToList();
        }

        //Notification Load test only
        public IEnumerable<int> GetAllUnderReviewClaimsIds()
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.Where(c => c.Status == "Under Review" && c.AssignToUserId != null).Select(c => c.ID).ToList();
        }

        public List<int> GetAllApprovedClaimIdsForInventoryCal(int? vendorId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var claimIds = new List<int>();
            if (vendorId != null)
            {
                claimIds = dbContext.Claims.Where(i => i.ClaimType != 2 && i.ParticipantId == vendorId && (i.Status == "Approved" || i.Status == "Receive Payment")).OrderBy(i => i.ID).Select(i => i.ID).ToList();
            }
            else
            {
                claimIds = dbContext.Claims.Where(i => i.ClaimType != 2 && (i.Status == "Approved" || i.Status == "Receive Payment")).OrderBy(i => i.ID).Select(i => i.ID).ToList();
            }
            return claimIds;

        }
        public bool IsReadyForApproval(int vendorId, DateTime claimPeriodDate)
        {
            var dbContext = context as ClaimsBoundedContext;
            var previousClaim = dbContext.Claims.Where(c => c.ClaimPeriod.StartDate < claimPeriodDate && c.ParticipantId == vendorId).OrderByDescending(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            //First claim
            if (previousClaim == null)
            {
                return true;
            }
            else
            {
                //previous claim has a GP Batch number that is POSTED or previous claim is a zero dollar claim and previous claim status is Approved
                if ((previousClaim.Status == "Approved") || (previousClaim.Status == "Receive Payment"))
                {
                    if (IsClaimPosted(previousClaim.ID))
                    {
                        return true;
                    }
                    if (previousClaim.ClaimsAmountTotal == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public string checkOtherClaimStatus(int claimId, int transactionId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var otherClaim = from claim in dbContext.Claims
                             join detail in dbContext.ClaimDetails on claim.ID equals detail.ClaimId into claim_detail
                             from cd in claim_detail
                             join transaction in dbContext.Transactions on cd.TransactionId equals transaction.ID
                             where claim.ID != claimId && transaction.ID == transactionId
                             select claim;
            if (otherClaim.AsQueryable().Count() == 1)
            {
                return otherClaim.FirstOrDefault().Status;
            }
            else
            {
                return string.Empty;
            }
        }

        //OTSTM2-400      
        public Claim GetNextClaimByVendorId(int participantId, Period currentPeriod)
        {
            var dbContext = context as ClaimsBoundedContext;
            var nextClaim = dbContext.Claims.Where(c => c.ParticipantId == participantId && c.ClaimPeriod.StartDate > currentPeriod.StartDate).OrderBy(c => c.ClaimPeriod.StartDate).FirstOrDefault();
            return nextClaim;
        }

        public Claim GetClaimByNumberAndPeriod(string registrationNumber, int claimPeriodId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Claims.Where(c => c.Participant.Number == registrationNumber && c.ClaimPeriodId == claimPeriodId).FirstOrDefault();
        }

        public List<int> GetMissingReportDataClaimIds()
        {
            //select distinct a.id from claim a, vendor b where a.ParticipantID=b.id and (a.id not in (select claimid from RptClaimInventoryDetail)) and b.VendorType=2 and a.ClaimsAmountTotal>0;
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.Claims.Where(c => !dbContext.ClaimInventoryDetails.Any(q => q.ClaimId == c.ID) && c.ClaimsAmountTotal > 0).Select(c => c.ID);
            return query.ToList();
        }

        public List<ClaimDetailVM> GetMissingReportDataTransactionIds()
        {
            using (var dbContext = new ClaimsBoundedContext())
            {
                var invClaimDetailVMs = dbContext.ClaimInventoryDetails.Select(c => new ClaimDetailVM
                {
                    ClaimId = c.ClaimId,
                    TransactionId = c.TransactionId
                }).Distinct().ToList();

                var claimDetailVMs = dbContext.ClaimDetails.Where(c => c.Transaction.TransactionItems.Count > 0).Select(c => new ClaimDetailVM
                {
                    ClaimId = c.ClaimId,
                    TransactionId = c.TransactionId
                }).ToList();

                return claimDetailVMs.Except(invClaimDetailVMs, new ClaimDetailVMComparer()).ToList();
            }
        }

        public List<ClaimDetailVM> GetMissingReportData(ClaimType claimType, string registrationNumber)
        {
            using (var dbContext = new ClaimsBoundedContext())
            {
                var intClaimType = (int)claimType;

                if (string.IsNullOrWhiteSpace(registrationNumber))
                {
                    var invClaimDetailVMs = dbContext.ClaimInventoryDetails.Where(c => c.Claim.ClaimType == intClaimType).Select(c => new ClaimDetailVM
                    {
                        ClaimId = c.ClaimId,
                        TransactionId = c.TransactionId
                    }).Distinct().ToList();

                    var claimDetailVMs = dbContext.ClaimDetails.Where(c => c.Transaction.TransactionItems.Count > 0 && c.Claim.ClaimType == intClaimType).Select(c => new ClaimDetailVM
                    {
                        ClaimId = c.ClaimId,
                        TransactionId = c.TransactionId
                    }).ToList();

                    return claimDetailVMs.Except(invClaimDetailVMs, new ClaimDetailVMComparer()).ToList();

                }
                else
                {
                    var invClaimDetailVMs = dbContext.ClaimInventoryDetails.Where(c => c.Claim.ClaimType == intClaimType && c.Claim.Participant.Number == registrationNumber).Select(c => new ClaimDetailVM
                    {
                        ClaimId = c.ClaimId,
                        TransactionId = c.TransactionId
                    }).Distinct().ToList();

                    var claimDetailVMs = dbContext.ClaimDetails.Where(c => c.Transaction.TransactionItems.Count > 0 && c.Claim.ClaimType == intClaimType && c.Claim.Participant.Number == registrationNumber).Select(c => new ClaimDetailVM
                    {
                        ClaimId = c.ClaimId,
                        TransactionId = c.TransactionId
                    }).ToList();

                    return claimDetailVMs.Except(invClaimDetailVMs, new ClaimDetailVMComparer()).ToList();
                }
            }
        }

        public List<int> LoadAllClaimIdsByVendorTypeAndStatus(int vendorType = 2, CL.TMS.Common.Enum.ClaimStatus claimStatus = CL.TMS.Common.Enum.ClaimStatus.Approved)
        {
            var dbContext = context as ClaimsBoundedContext;
            var idQuery = dbContext.Claims.Where(c => (c.Participant.Number.ToString().StartsWith(vendorType.ToString()) && (c.Status == claimStatus.ToString()))).Select(c => c.ID);
            return idQuery.ToList();
        }

        public User FindUserById(long userId)
        {
            var dbContext = context as ClaimsBoundedContext;
            return dbContext.Users.Where(c => c.ID == userId).FirstOrDefault();
        }

        public List<int> LoadAllClaimIdsByVendorID(int vendorID)
        {
            var dbContext = context as ClaimsBoundedContext;
            var idQuery = dbContext.Claims.Where(c => (c.Participant.ID == vendorID)).Select(c => c.ID);
            return idQuery.ToList();
        }

        public int GetAssociatedRateTrnsactionId(DateTime startDate, DateTime endDate, int vendorId)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var dbContext = context as ClaimsBoundedContext;
            var query = dbContext.VendorRates.FirstOrDefault(c => c.RateTransaction.EffectiveStartDate <= startDate && c.RateTransaction.EffectiveEndDate >= endDate && c.VendorID == vendorId);
            return query != null ? query.RateTransactionID : 0;
        }
        public List<Rate> LoadRateTransactionByID(int rateTransactionID)
        {
            var dbContext = context as ClaimsBoundedContext;

            return dbContext.Rates.Where(c => c.RateTransactionID == rateTransactionID).ToList();

        }
        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            var dbContext = context as ClaimsBoundedContext;

            return dbContext.RateTransactionNotes.Where(c => c.RateTransactionID == rateTransactionID).Select(i => new InternalNoteViewModel
            {
                AddedOn = i.CreatedDate,
                Note = i.Note,
                AddedBy = i.User.FirstName + " " + i.User.LastName
            }).OrderByDescending(i => i.AddedOn).ToList();
        }

        #region OTSTM2-1215 STC Premium
        public List<HaulerSTCTransactionBriefViewModel> GetStcTransactionBriefListByClaimId(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var query = from claim in dbContext.Claims
                        join claimDetail in dbContext.ClaimDetails on claim.ID equals claimDetail.ClaimId
                        join transaction in dbContext.Transactions on claimDetail.TransactionId equals transaction.ID
                        where transaction.TransactionTypeCode == TreadMarksConstants.STC && claimDetail.Direction == true && claim.ID == claimId
                        && claimDetail.Transaction.ProcessingStatus != "Invalidated"
                        select new HaulerSTCTransactionBriefViewModel
                        {
                            transactionId = transaction.ID,
                            friendlyId = transaction.FriendlyId
                        };
            var result = query.ToList();
            return result;
        }

        public List<ClaimPaymentDetail> LoadClaimPaymentDetails(int claimId)
        {
            var dbContext = context as ClaimsBoundedContext;
            var result = dbContext.ClaimPaymentDetails.Where(c => c.ClaimId == claimId).ToList();
            return result;
        }
        #endregion
    }

    public class ClaimDetailVMComparer : IEqualityComparer<ClaimDetailVM>
    {
        public bool Equals(ClaimDetailVM x, ClaimDetailVM y)
        {
            if ((x.ClaimId == y.ClaimId) && (x.TransactionId == y.TransactionId))
                return true;
            else
                return false;
        }

        public int GetHashCode(ClaimDetailVM obj)
        {
            int hashClaimId = obj.ClaimId.GetHashCode();
            int hashTransactionId = obj.TransactionId.GetHashCode();
            return hashClaimId ^ hashTransactionId;
        }
    }
}