﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common;
using CL.TMS.Common.Extension;
using CL.TMS.DataContracts.Mobile;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;

namespace CL.TMS.Repository.Claims
{
    public class MobileTransactionRepository : BaseRepository<MobileTransactionBoundedContext, Transaction, Guid>, IMobileTransactionRepository
    {
        public Transaction GetTransaction(Guid transactionId)
        {
            return entityStore.AllIncluding(c => c.Photos, c => c.ScaleTickets, c => c.TransactionTireTypes, c => c.TransactionMaterialTypes, c => c.Comments, c => c.TransactionEligibilities, c => c.Authorizations).FirstOrDefault(c => c.ID == transactionId);
        }

        public Location GetLocationByAuthorizationId(Guid authorizationId)
        {
            return
                (context as MobileTransactionBoundedContext).Authorizations.FirstOrDefault(c => c.ID == authorizationId)
                    .Locations.FirstOrDefault();
        }

        public GpsLog GetGps(Guid gpsLogId)
        {
            return (context as MobileTransactionBoundedContext).GpsLogs.FirstOrDefault(c => c.ID == gpsLogId);
        }

        public MobileRegistrant GetRegistrant(decimal registrationNumber)
        {
            return (context as MobileTransactionBoundedContext).MobileRegistrants.FirstOrDefault(c => c.ID == registrationNumber);
        }

        public Company GetCompany(Guid transactionId)
        {
            return (context as MobileTransactionBoundedContext).Companies.FirstOrDefault(c => c.TransactionId == transactionId);
        }
    }
}
