﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common;
using CL.TMS.Common.Extension;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.Adapter;
using System.Web.Script.Serialization;
using System.Diagnostics;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;
using System.Diagnostics;
using System.Text.RegularExpressions;
namespace CL.TMS.Repository.Claims
{
    public class TransactionRepository : BaseRepository<TransactionBoundedContext, Transaction, int>, ITransactionRepository
    {
        private const string STAFF = "staff";
        private const string PARTICIPANT = "participant";
        private const string BOTH = "both";
        private int totalRecords = 0;

        #region Staff / Vendor Transaction List
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.TCR && claimdetail.Direction == true
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (claimdetail.Transaction.Badge != null) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            VendorId = claimdetail.Transaction.OutgoingVendor != null ? claimdetail.Transaction.OutgoingVendor.ID : 0,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number ?? string.Empty,
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.ReviewStatus.ToLower().Contains(searchText)
                                                           || c.Badge.Contains(searchText)
                                                           || c.TransactionDate.ToString().Contains(searchText)
                                                           || c.TransactionFriendlyId.ToString().Contains(searchText)
                                                           || c.VendorNumber.Contains(searchText)
                                                           || c.VendorBusinessName.ToLower().Contains(searchText)
                                                           || c.DeviceName.Contains(searchText)
                                                           || c.OnRoadWeight.ToString().Contains(searchText)
                                                           || c.OffRoadWeight.ToString().Contains(searchText)
                                                           || c.PostalCode.ToLower().Contains(searchText)
                                                           || (
                                                                 (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                                                 (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                                                 (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                                                              ) //OTSTM2-416
                                                           );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);
            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOT && claimdetail.Direction == true
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),

                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.ReviewStatus.ToLower().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.VendorNumber.ToString().Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.DeviceName.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        || c.PostalCode.ToString().ToLower().Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || (
                                (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           ) //OTSTM2-416
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.UCR && claimdetail.Direction == true
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = (null != claimdetail.Transaction.FromPostalCode) ? claimdetail.Transaction.FromPostalCode
                            : ((null != claimdetail.Transaction.CompanyInfo.Address.PostalCode) ? claimdetail.Transaction.CompanyInfo.Address.PostalCode : " "),
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.VendorInfo.BusinessName ?? string.Empty,
                            VendorNumber = (null != claimdetail.Transaction.VendorInfo.Number) ? claimdetail.Transaction.VendorInfo.Number : ((null != claimdetail.Transaction.CompanyInfo.CompanyName) ? claimdetail.Transaction.CompanyInfo.CompanyName : " "),
                            VendorId = (null != claimdetail.Transaction.VendorInfo.Number) ? claimdetail.Transaction.VendorInfo.ID : 0,
                            PaymentEligibleOther = claimdetail.Transaction.PaymentEligibleOther ?? string.Empty,
                            PaymentEligible = claimdetail.Transaction.PaymentEligible ?? 4,
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.ReviewStatus.ToLower().Contains(searchText)
                            || c.Badge.ToString().Contains(searchText)
                            || c.TransactionDate.ToString().Contains(searchText)
                            || c.TransactionFriendlyId.ToString().Contains(searchText)
                            || c.VendorNumber.ToString().Contains(searchText)
                            || c.VendorBusinessName.ToLower().Contains(searchText)
                            || c.DeviceName.ToLower().Contains(searchText)
                            || c.OnRoadWeight.ToString().Contains(searchText)
                            || c.OffRoadWeight.ToString().Contains(searchText)
                            || c.PostalCode.ToString().ToLower().Contains(searchText)
                            //|| c.ReasonNotEligible.ToString().ToLower().Contains(searchText)
                            //|| c.Zone.ToLower().Contains(searchText)
                            || (
                                    (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                    (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                    (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                                ) //OTSTM2-416
                            );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);
            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where (claimdetail.Transaction.IncomingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.HIT && claimdetail.Direction == true
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };
            if (!(dataSubType.ToLower().Contains("inbound")))
            {
                query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where (claimdetail.Transaction.OutgoingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.HIT && claimdetail.Direction == false //false==outBound
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToString().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        //|| c.NameofGroupIndividual.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToString().ToLower().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText) //OTSTM2-416
                        || (
                                (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                            ) //OTSTM2-416
                    );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.STC && claimdetail.Direction == true
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.CompanyInfo.Address.PostalCode,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,

                            NameofGroupIndividual = (null != claimdetail.Transaction.OutgoingVendor.BusinessName) ? claimdetail.Transaction.OutgoingVendor.BusinessName : claimdetail.Transaction.CompanyInfo.CompanyName,
                            VendorId = (null != claimdetail.Transaction.OutgoingVendor.BusinessName) ? claimdetail.Transaction.OutgoingVendor.ID : 0,

                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            EventNumber = claimdetail.Transaction.EventNumber, //OTSTM2-1215
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToString().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.NameofGroupIndividual.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToString().ToLower().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText) //OTSTM2-416
                        || (
                                (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                            ) //OTSTM2-416
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PTR && claimdetail.Direction == false //false==outBound
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            //OnRoadWeight = transaction.EstimatedOnRoadWeight ?? 0,
                            //OffRoadWeight = transaction.EstimatedOffRoadWeight ?? 0,
                            OnRoadWeight = claimdetail.Transaction.OnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.OffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.ToPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number,
                            VendorId = claimdetail.Transaction.IncomingVendor.ID,
                            ScaleTickets = claimdetail.Transaction.ScaleTickets.OrderBy(a => a.ScaleTicketType).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.VendorNumber.ToString().Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        //|| c.ScaleTicketNbr.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        //|| c.Variance.ToString().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.ReviewStatus.ToString().Contains(searchText) //OTSTM2-416
                        || (
                                (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                            ) //OTSTM2-416
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);

            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });
            result.ForEach(c => { c.ScaleTicketNbr = string.Join("/", c.ScaleTickets.Select(i => i.TicketNumber)); });

            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()////.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.RTR && claimdetail.Direction == false //false==outBound
                        select new HaulerCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(), //OTSTM2-416
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //BuyerName = (null == s2) ? string.Empty : (s1.CompanyName??string.Empty),
                            BuyerName = claimdetail.Transaction.CompanyInfo.CompanyName ?? string.Empty,
                            //VendorNumber = s2.Number,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            MarketLocation = (null != claimdetail.Transaction.MarketLocation) ? claimdetail.Transaction.MarketLocation : string.Empty,
                            InvoiceNbr = claimdetail.Transaction.InvoiceNumber,
                            TireMarketType = (null == claimdetail.Transaction.TireMarketType) ? "Unknow Type" : (claimdetail.Transaction.TireMarketType == 1) ? "For Sale" : "Retread",// (DB 1 for sale , 2 for Retread)
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToString().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.VendorNumber.ToString().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.BuyerName.ToString().Contains(searchText)
                        || c.InvoiceNbr.ToString().Contains(searchText)
                        || c.MarketLocation.ToString().ToLower().Contains(searchText)
                        || c.TireMarketType.ToString().ToLower().Contains(searchText)
                        || c.ReviewStatus.ToString().Contains(searchText) //OTSTM2-416
                        || (
                                (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                            ) //OTSTM2-416
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction.TransactionItems).Include(c => c.Transaction.TransactionAdjustments)//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.TCR && claimdetail.Direction == true
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            //IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Where(c => (c.Status == TransactionAdjustmentStatus.Accepted.ToString()) || (c.Status == TransactionAdjustmentStatus.Pending.ToString())).Select(c => c.IsAdjustByStaff).ToList(),
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)//not able to search
                        || c.PostalCode.ToLower().StartsWith(searchText)//|| c.FSA.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );

                }

            }
            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOT && claimdetail.Direction == true
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToLower().StartsWith(searchText) //|| c.FSA.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.UCR && claimdetail.Direction == true
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = (null != claimdetail.Transaction.VendorInfo.Number) ? claimdetail.Transaction.VendorInfo.Number :
                            ((null != claimdetail.Transaction.CompanyInfo.CompanyName) ? claimdetail.Transaction.CompanyInfo.CompanyName : " "),
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = (null != claimdetail.Transaction.FromPostalCode) ? claimdetail.Transaction.FromPostalCode
                            : ((null != claimdetail.Transaction.CompanyInfo.Address.PostalCode) ? claimdetail.Transaction.CompanyInfo.Address.PostalCode : " "),
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.VendorInfo.BusinessName ?? string.Empty,
                            VendorId = claimdetail.Transaction.VendorInfo != null ? claimdetail.Transaction.VendorInfo.ID : 0,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.Contains(searchText)
                        || c.PostalCode.ToLower().StartsWith(searchText)//|| c.FSA.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.IncomingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.HIT && claimdetail.Direction == true
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };
            if (!(dataSubType.ToLower().Contains("inbound")))
            {
                query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.OutgoingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.HIT && claimdetail.Direction == false //false==outBound
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number,
                            VendorId = claimdetail.Transaction.IncomingVendor.ID,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.STC && claimdetail.Direction == true
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor != null ? claimdetail.Transaction.OutgoingVendor.ID : 0,
                            NameofGroupIndividual = claimdetail.Transaction.CompanyInfo.CompanyName ?? string.Empty,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            //PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            PostalCode = claimdetail.Transaction.CompanyInfo.Address.PostalCode,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                            EventNumber = claimdetail.Transaction.EventNumber, //OTSTM2-1215
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.NameofGroupIndividual.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToLower().StartsWith(searchText)//|| c.FSA.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking()//.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PTR && claimdetail.Direction == false //false==outBound
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName ?? string.Empty,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number,
                            VendorId = claimdetail.Transaction.IncomingVendor.ID,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.OnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.OffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.ToPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleTicketNbr = claimdetail.Transaction.ScaleTickets.FirstOrDefault().TicketNumber ?? string.Empty,
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),

                            VendorGroupName = string.Empty,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToLower().StartsWith(searchText)//|| c.FSA.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        //|| c.Variance.ToString().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.RTR && claimdetail.Direction == false //false==outBound
                        select new HaulerStaffCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorId = claimdetail.Transaction.OutgoingVendor.ID,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            OnRoadWeight = claimdetail.Transaction.EstimatedOnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.EstimatedOffRoadWeight ?? 0,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            MarketLocation = (null != claimdetail.Transaction.MarketLocation) ? claimdetail.Transaction.MarketLocation : string.Empty,
                            InvoiceNbr = claimdetail.Transaction.InvoiceNumber,
                            TireMarketType = (null == claimdetail.Transaction.TireMarketType) ? "Unknow Type" : (claimdetail.Transaction.TireMarketType == 1) ? "For Sale" : "Retread",
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                            BuyerName = claimdetail.Transaction.CompanyInfo.CompanyName ?? string.Empty,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.MarketLocation.ToLower().Contains(searchText)
                        || c.BuyerName.ToLower().Contains(searchText)
                        || c.TireMarketType.ToLower().Contains(searchText)
                        || c.InvoiceNbr.ToString().Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.OnRoadWeight.ToString().Contains(searchText)
                        || c.OffRoadWeight.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            if ("FSA" == orderBy) orderBy = "PostalCode";
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOT && claimdetail.Direction == false //false==outBound
                        select new CollectorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName ?? string.Empty,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus ?? string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number ?? string.Empty,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { Quantity = i.Quantity, ShortName = i.Item.ShortName }).ToList(),
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.ReviewStatus.ToLower().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.VendorNumber.ToString().Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.DeviceName.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            //foreach (CollectorCommonTransactionListViewModel transactionRecord in result)
            //{
            //    if (null != transactionRecord.IsAdjustByStaff) break;//if there is adjustment, skip curretn transation item.
            //    var transItems = from transItem in dbContext.TransactionItems
            //                     where transactionRecord.ID == transItem.TransactionId
            //                     select new { ActualWeight = transItem.ActualWeight };
            //    decimal totalWeigt = 0;
            //    if (null != transItems)
            //    {
            //        foreach (var t in transItems)
            //        {
            //            totalWeigt = totalWeigt + t.ActualWeight;
            //        }
            //    }
            //    transactionRecord.ScaleWeight = totalWeigt;
            //}

            return new PaginationDTO<CollectorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.TCR && claimdetail.Direction == false //false==outBound
                        select new CollectorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            DeviceName = claimdetail.Transaction.DeviceName ?? string.Empty,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            //OnRoadWeight = (null != transaction.EstimatedOnRoadWeight) ? (double)transaction.EstimatedOnRoadWeight : 0,
                            //OffRoadWeight = (null != transaction.EstimatedOffRoadWeight) ? (double)transaction.EstimatedOffRoadWeight : 0,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number ?? string.Empty,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList()
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { Quantity = i.Quantity, ShortName = i.Item.ShortName }).ToList(),
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList()
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.ReviewStatus.ToLower().Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.VendorNumber.ToString().Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.DeviceName.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )

                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<CollectorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOT && claimdetail.Direction == false //false==outBound
                        select new CollectorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number ?? string.Empty,
                            VendorId = claimdetail.Transaction.IncomingVendor != null ? claimdetail.Transaction.IncomingVendor.ID : 0,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = ((null != claimdetail.Transaction.OutgoingVendor) ? claimdetail.Transaction.OutgoingVendor.BusinessName : string.Empty), //(null == s2) ? string.Empty : s2.BusinessName,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                         c.DeviceName.ToLower().Contains(searchText)
                                || c.TransactionFriendlyId.ToString().Contains(searchText)
                                || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                                || c.Notes.ToLower().Contains(searchText)
                                || c.TransactionDate.ToString().Contains(searchText)
                                || c.VendorNumber.Contains(searchText)
                                //|| c.Zone.ToLower().Contains(searchText)
                                || c.PostalCode.ToLower().StartsWith(searchText)//|| c.FSA.ToLower().Contains(searchText)
                                || c.ScaleWeight.ToString().Contains(searchText)
                                || c.EstWeight.ToString().Contains(searchText)
                                || c.TireCountList.Any(i => i.Quantity == searchVal)
                                );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<CollectorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.TCR && claimdetail.Direction == false //false==outBound
                        select new CollectorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName ?? string.Empty,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            IsGenerateTires = claimdetail.Transaction.IsGenerateTires,
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName ?? string.Empty,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number ?? string.Empty,
                            VendorId = claimdetail.Transaction.IncomingVendor != null ? claimdetail.Transaction.IncomingVendor.ID : 0,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        //|| c.Zone.ToLower().Contains(searchText)
                        || c.PostalCode.ToLower().StartsWith(searchText) //|| c.FSA.ToLower().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result);

            return new PaginationDTO<CollectorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PTR && claimdetail.Direction == true
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            PostalCode = claimdetail.Transaction.ToPostalCode ?? string.Empty,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            ScaleTickets = claimdetail.Transaction.ScaleTickets.OrderBy(a => a.ScaleTicketType).ToList(),
                            IncomingId=claimdetail.Transaction.IncomingId
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.ScaleTicketNbr.ToString().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        //|| c.AGLS.ToString().Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        //|| c.Variance.ToString().Contains(searchText)
                        //|| c.AmountDollar.ToString().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.TireCountList.Any(i => i.Quantity == searchVal));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();
            TransactionItemAdjustment(result, TreadMarksConstants.PTR);

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, List<TypeDefinition> DispositionReasonList = null)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOR && claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus ?? string.Empty,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.VendorInfo.Number ?? string.Empty,

                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.VendorInfo.Number ?? (claimdetail.Transaction.CompanyInfo != null ? claimdetail.Transaction.CompanyInfo.CompanyName : string.Empty),
                            Destination = (null != claimdetail.Transaction.CompanyInfo.Address.Country) ? string.Concat(claimdetail.Transaction.CompanyInfo.Address.City, ", ", claimdetail.Transaction.CompanyInfo.Address.Province, ", ", claimdetail.Transaction.CompanyInfo.Address.Country)
                                            : string.Concat(claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City, ", ",
                                                                    claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Province, ", ",
                                                                    claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Country),
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            MaterialType = claimdetail.Transaction.MaterialType ?? 0,
                            DispositionReason = claimdetail.Transaction.DispositionReason.ToString() ?? string.Empty,
                            //TransactionItems = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            OnRoadWeight = claimdetail.Transaction.OnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.OffRoadWeight ?? 0,
                            OutgoingId=claimdetail.Transaction.OutgoingId
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();
            }
            int iMaterialType = -1;
            if (TreadMarksConstants.Trash.ToLower().Contains(searchText))
            {
                iMaterialType = 1;
            }
            else if (TreadMarksConstants.Steel.ToLower().Contains(searchText))
            {
                iMaterialType = 2;
            }
            else if (TreadMarksConstants.Fibre.ToLower().Contains(searchText))
            {
                iMaterialType = 3;
            }
            else if (TreadMarksConstants.TireRims.ToLower().Contains(searchText))
            {
                iMaterialType = 4;
            }
            else if (TreadMarksConstants.UsedTire.ToLower().Contains(searchText))
            {
                iMaterialType = 5;
            }

            int iDispositionReason = -1;
            if (TreadMarksConstants.ReuseResale.ToLower().Contains(searchText))
            {
                iDispositionReason = 1;
            }
            else if (TreadMarksConstants.Recycling.ToLower().Contains(searchText))
            {
                iDispositionReason = 2;
            }
            else if (TreadMarksConstants.Energy.ToLower().Contains(searchText))
            {
                iDispositionReason = 3;
            }
            else if (TreadMarksConstants.Landfill.ToLower().Contains(searchText))
            {
                iDispositionReason = 4;
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.Destination.ToLower().Contains(searchText)
                        || c.MaterialType == iMaterialType
                        || c.DispositionReason == iDispositionReason.ToString()
                        || c.ScaleTicketNbr.ToString().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        //|| c.AmountDollar.ToString().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            result.ForEach(c =>
            {
                c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : (c.OnRoadWeight + c.OffRoadWeight);
                c.ScaleWeight = DataConversion.ConvertKgToTon((double)c.ScaleWeight);
            });

            TransactionItemAdjustment(result, TreadMarksConstants.DOR);

            //load DispositionReason.
            if (null != DispositionReasonList)
            {
                result.ForEach(c =>
                {
                    c.DispositionReason = (DispositionReasonList.AsQueryable().FirstOrDefault(t => string.Compare(t.DefinitionValue.ToString(), c.DispositionReason, StringComparison.OrdinalIgnoreCase) == 0)).Code.ToString();
                });
            }

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.IncomingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PIT && claimdetail.Direction == true
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName,
                            //TransactionItems = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            ProductType = claimdetail.Transaction.TransactionItems.FirstOrDefault(i => i.TransactionAdjustmentId == null).ItemID.ToString(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };
            if (!(dataSubType.ToLower().Contains("inbound")))//outbound PIT 
            {
                query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.OutgoingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PIT && claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number,
                            Badge = (!((null == claimdetail.Transaction.Badge) || (!claimdetail.Transaction.MobileFormat))) ? claimdetail.Transaction.Badge : string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName,
                            //TransactionItems = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            Rate = claimdetail.Transaction.TransactionItems.FirstOrDefault().Rate,
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            ProductType = claimdetail.Transaction.TransactionItems.FirstOrDefault(i => i.TransactionAdjustmentId == null).ItemID.ToString(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.Badge.ToString().Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.ScaleTicketNbr.ToString().Contains(searchText));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            //TransactionItemAdjustment(result, TreadMarksConstants.PIT);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; c.ProductType = ("" != c.ProductType) ? c.ProductType : "0"; });

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            dbContext.Configuration.LazyLoadingEnabled = true;
            dbContext.Configuration.ProxyCreationEnabled = true;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.SPS && claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            InvoiceDate = claimdetail.Transaction.TransactionDate,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            VendorBusinessName = (null != claimdetail.Transaction.IncomingVendor.BusinessName) ? claimdetail.Transaction.IncomingVendor.BusinessName : claimdetail.Transaction.CompanyInfo.CompanyName,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number ?? string.Empty,
                            Destination = (null != claimdetail.Transaction.CompanyInfoId) ? string.Concat(claimdetail.Transaction.CompanyInfo.Address.City, ", ", claimdetail.Transaction.CompanyInfo.Address.Province, ", ", claimdetail.Transaction.CompanyInfo.Address.Country) :
                                    string.Concat(claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().City, ", ", claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().Province, ", ", claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().Country),
                            TransactionItems = claimdetail.Transaction.TransactionItems,
                            ProductType = claimdetail.Transaction.TransactionItems.FirstOrDefault().Item.ShortName,
                            MeshRange = string.Concat(claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeStart,
                            (null != claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeStart ? " to " : string.Empty), claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeEnd),//enable this field for search result
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            Rate = claimdetail.Transaction.TransactionItems.FirstOrDefault().Rate,//enable this field for search result
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            //TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            //AmountDollar = claimdetail.Transaction.TransactionItems.FirstOrDefault().Rate.Value * (claimdetail.Transaction.TransactionItems.FirstOrDefault().ActualWeight / 1000),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            InvoiceNbr = (claimdetail.Transaction.TransactionAdjustmentID == null) ? (claimdetail.Transaction.InvoiceNumber ?? string.Empty) :
                            (claimdetail.Transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).InvoiceNumber),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !IsDigitsOnly(searchText))//if pure digits or decimal, not searching date column
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.InvoiceDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.InvoiceDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.InvoiceDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    double parsedDigit = 0;
                    bool digitSearch = false;
                    if (IsDigitsOnly(searchText))
                    {
                        double.TryParse(searchText, out parsedDigit);
                        digitSearch = true;
                    }
                    query = query.Where(c =>
                        c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.InvoiceDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.Destination.ToLower().Contains(searchText)
                        || c.ProductType.ToLower().Contains(searchText)
                        || c.MeshRange.ToLower().Contains(searchText)
                        || Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4).ToString().Contains(searchText)//ConvertKgToTon, rounding
                        || ((digitSearch) && (Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) == parsedDigit))//ConvertKgToTon, rounding
                        || c.Rate.ToString().Contains(searchText)
                        || ((digitSearch) && (((double)(c.Rate ?? 0)) == parsedDigit))
                        || c.InvoiceNbr.Contains(searchText)
                        || Math.Round((Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) * (double)(c.Rate ?? 0)), 2).ToString().Contains(searchText)//search for AmountDollar
                        || ((digitSearch) && (Math.Round((Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) * (double)(c.Rate ?? 0)), 2) == parsedDigit))//search for AmountDollar NUMBER
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           ));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result, TreadMarksConstants.SPS);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PTR && claimdetail.Direction == true
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            EstWeight = (claimdetail.Transaction.EstimatedOnRoadWeight ?? 0) + (claimdetail.Transaction.EstimatedOffRoadWeight ?? 0),
                            PostalCode = claimdetail.Transaction.ToPostalCode ?? string.Empty,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            IncomingId=claimdetail.Transaction.IncomingId
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.EstWeight.ToString().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        //|| c.AmountDollar.ToString().Contains(searchText)
                        || c.TireCountList.Any(i => i.Quantity == searchVal)
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            TransactionItemAdjustment(result, TreadMarksConstants.PTR);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; });

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, List<TypeDefinition> DispositionReasonList = null)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.DOR && claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus ?? string.Empty,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.VendorInfo.Number ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            VendorBusinessName = claimdetail.Transaction.VendorInfo.Number ?? (claimdetail.Transaction.CompanyInfo != null ? claimdetail.Transaction.CompanyInfo.CompanyName : string.Empty),
                            Destination = (null != claimdetail.Transaction.CompanyInfo.Address.Country) ? string.Concat(claimdetail.Transaction.CompanyInfo.Address.City, ", ", claimdetail.Transaction.CompanyInfo.Address.Province, ", ", claimdetail.Transaction.CompanyInfo.Address.Country)
                                            : string.Concat(claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City, ", ",
                                                                    claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Province, ", ",
                                                                    claimdetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Country),
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            MaterialType = claimdetail.Transaction.MaterialType ?? 0,
                            DispositionReason = claimdetail.Transaction.DispositionReason.ToString(),
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            PostalCode = claimdetail.Transaction.FromPostalCode ?? string.Empty,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            OnRoadWeight = claimdetail.Transaction.OnRoadWeight ?? 0,
                            OffRoadWeight = claimdetail.Transaction.OffRoadWeight ?? 0,
                            OutgoingId=claimdetail.Transaction.OutgoingId
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();
            }
            int iMaterialType = -1;
            if (TreadMarksConstants.Trash.ToLower().Contains(searchText))
            {
                iMaterialType = 1;
            }
            else if (TreadMarksConstants.Steel.ToLower().Contains(searchText))
            {
                iMaterialType = 2;
            }
            else if (TreadMarksConstants.Fibre.ToLower().Contains(searchText))
            {
                iMaterialType = 3;
            }
            else if (TreadMarksConstants.TireRims.ToLower().Contains(searchText))
            {
                iMaterialType = 4;
            }
            else if (TreadMarksConstants.UsedTire.ToLower().Contains(searchText))
            {
                iMaterialType = 5;
            }

            int iDispositionReason = -1;
            if (TreadMarksConstants.ReuseResale.ToLower().Contains(searchText))
            {
                iDispositionReason = 1;
            }
            else if (TreadMarksConstants.Recycling.ToLower().Contains(searchText))
            {
                iDispositionReason = 2;
            }
            else if (TreadMarksConstants.Energy.ToLower().Contains(searchText))
            {
                iDispositionReason = 3;
            }
            else if (TreadMarksConstants.Landfill.ToLower().Contains(searchText))
            {
                iDispositionReason = 4;
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.Destination.ToLower().Contains(searchText)
                        || c.MaterialType == iMaterialType
                        || c.DispositionReason == iDispositionReason.ToString()
                        || c.ScaleWeight.ToString().Contains(searchText)
                        //|| c.AmountDollar.ToString().Contains(searchText)
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;

            orderBy = ("MaterialTypeName" == orderBy) ? "MaterialType" : orderBy;

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            result.ForEach(c =>
            {
                c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : (c.OnRoadWeight + c.OffRoadWeight);
                c.ScaleWeight = DataConversion.ConvertKgToTon((double)c.ScaleWeight);
            });

            TransactionItemAdjustment(result, TreadMarksConstants.DOR);
            //load DispositionReason.
            if (null != DispositionReasonList)
            {
                result.ForEach(c =>
                {
                    c.DispositionReason = (DispositionReasonList.AsQueryable().FirstOrDefault(t => string.Compare(t.DefinitionValue.ToString(), c.DispositionReason, StringComparison.OrdinalIgnoreCase) == 0)).Code.ToString();
                });
            }

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.IncomingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PIT && claimdetail.Direction == true
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.OutgoingVendor.Number,
                            VendorBusinessName = claimdetail.Transaction.OutgoingVendor.BusinessName ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            ProductType = claimdetail.Transaction.TransactionItems.FirstOrDefault(i => i.TransactionAdjustmentId == null).ItemID.ToString(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };
            if (!(dataSubType.ToLower().Contains("inbound")))//outbound PIT 
            {
                query = from claimdetail in dbContext.ClaimDetails.AsNoTracking().Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where (claimdetail.Transaction.OutgoingId == vendorId) && claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.PIT && claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            DeviceName = claimdetail.Transaction.DeviceName,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus,
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionDate = claimdetail.Transaction.TransactionDate,
                            VendorNumber = claimdetail.Transaction.IncomingVendor.Number,
                            VendorBusinessName = claimdetail.Transaction.IncomingVendor.BusinessName ?? string.Empty,
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TransactionItems = transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            Rate = claimdetail.Transaction.TransactionItems.FirstOrDefault().Rate,
                            ScaleTicketNbr = (claimdetail.Transaction.ScaleTickets.Count > 0) ? (claimdetail.Transaction.ScaleTickets.FirstOrDefault()).TicketNumber : string.Empty,
                            ProductType = claimdetail.Transaction.TransactionItems.FirstOrDefault(i => i.TransactionAdjustmentId == null).ItemID.ToString(),
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                        };
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.TransactionDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.TransactionDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.TransactionDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    int searchVal;
                    if (!(Int32.TryParse(searchText, out searchVal)))
                    {
                        searchVal = -1;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                        c.DeviceName.ToLower().Contains(searchText)
                        || c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        //|| c.Adjustment.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.Notes.ToLower().Contains(searchText)
                        || c.TransactionDate.ToString().Contains(searchText)
                        || c.VendorNumber.Contains(searchText)
                        || c.VendorBusinessName.ToLower().Contains(searchText)
                        || c.ScaleWeight.ToString().Contains(searchText)
                        || c.ProductType.ToLower().Contains(searchText)
                        || c.ScaleTicketNbr.ToString().Contains(searchText));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            //TransactionItemAdjustment(result, TreadMarksConstants.PIT);
            result.ForEach(c => { c.ScaleWeight = (null != c.ScaleWeight) ? c.ScaleWeight : 0; c.ProductType = ("" != c.ProductType) ? c.ProductType : "0"; });

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as TransactionBoundedContext;
            var query = from claimdetail in dbContext.ClaimDetails.AsNoTracking() //.Include(c => c.Transaction).Include(c => c.Transaction.OutgoingVendor)
                        where claimdetail.ClaimId == claimId && claimdetail.Transaction.TransactionTypeCode == TreadMarksConstants.SPS //&& claimdetail.Direction == false //false==outBound
                        select new ProcessorCommonTransactionListViewModel()
                        {
                            ID = claimdetail.Transaction.ID,
                            MobileFormat = claimdetail.Transaction.MobileFormat,
                            InvoiceDate = claimdetail.Transaction.TransactionDate,
                            TransactionFriendlyId = claimdetail.Transaction.FriendlyId,
                            ReviewStatus = claimdetail.Transaction.ProcessingStatus ?? string.Empty,
                            VendorBusinessName = (null != claimdetail.Transaction.IncomingVendor) ? claimdetail.Transaction.IncomingVendor.BusinessName : (claimdetail.Transaction.CompanyInfo.CompanyName ?? string.Empty),
                            IsAdjustByStaff = claimdetail.Transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            //Notes = claimdetail.Transaction.TransactionNotes.FirstOrDefault() != null ? claimdetail.Transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            VendorNumber = claimdetail.Transaction.IncomingVendor != null ? claimdetail.Transaction.IncomingVendor.Number : string.Empty,
                            Destination = (null != claimdetail.Transaction.CompanyInfoId) ? string.Concat(claimdetail.Transaction.CompanyInfo.Address.City, ", ", claimdetail.Transaction.CompanyInfo.Address.Province, ", ", claimdetail.Transaction.CompanyInfo.Address.Country) :
                                    string.Concat(claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().City, ", ", claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().Province, ", ", claimdetail.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault().Country),
                            //Destination ="",
                            TransactionItems = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(),
                            ProductType = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().Item.ShortName ?? string.Empty,
                            MeshRange = string.Concat(claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeStart,
                            (null != claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeStart ? " to " : string.Empty), claimdetail.Transaction.TransactionItems.FirstOrDefault().MeshRangeEnd),//enable this field for search result
                            ScaleWeight = claimdetail.Transaction.ScaleTickets.Where(c => c.TransactionAdjustmentId == null).Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))),
                            Rate = claimdetail.Transaction.TransactionItems.FirstOrDefault().Rate,//enable this field for search result
                            TransactionId = claimdetail.Transaction.TransactionId,
                            //TireCountList = claimdetail.Transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList(),
                            //TotalWeight = claimdetail.Transaction.TransactionItems.FirstOrDefault().ActualWeight,
                            TransactionNotes = claimdetail.Transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).Select(i => i.Note).ToList(),
                            InvoiceNbr = (claimdetail.Transaction.TransactionAdjustmentID == null) ? (claimdetail.Transaction.InvoiceNumber ?? string.Empty) :
                            (claimdetail.Transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).InvoiceNumber),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !IsDigitsOnly(searchText))//if pure digits or decimal, not searching date column
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.InvoiceDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.InvoiceDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.InvoiceDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    double parsedDigit = 0;
                    bool digitSearch = false;
                    if (IsDigitsOnly(searchText))
                    {
                        double.TryParse(searchText, out parsedDigit);
                        digitSearch = true;
                    }
                    query = query.Where(c =>
                        c.TransactionFriendlyId.ToString().Contains(searchText)
                                || c.ReviewStatus.ToLower().Contains(searchText)
                                //|| c.Adjustment.ToLower().Contains(searchText)
                                || (
                                     (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                     (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                     (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                                   )
                                //|| c.Notes.ToLower().Contains(searchText)
                                || c.InvoiceDate.ToString().Contains(searchText)
                                || c.VendorNumber.Contains(searchText)
                                || c.VendorBusinessName.ToLower().Contains(searchText)
                                || c.Destination.ToLower().Contains(searchText)
                                || c.ProductType.ToLower().Contains(searchText)//OTSTM2-472
                                || c.MeshRange.ToLower().Contains(searchText)
                                || Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4).ToString().Contains(searchText)//ConvertKgToTon, rounding
                                || ((digitSearch) && (Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) == parsedDigit))//ConvertKgToTon, rounding
                                || c.Rate.ToString().Contains(searchText)
                                || ((digitSearch) && (((double)(c.Rate ?? 0)) == parsedDigit))
                                //|| c.AmountDollar.ToString().Contains(searchText)
                                || Math.Round((Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) * (double)(c.Rate ?? 0)), 2).ToString().Contains(searchText)//search for AmountDollar
                                || ((digitSearch) && (Math.Round((Math.Round(((double)(c.ScaleWeight ?? 0) * 0.001), 4) * (double)(c.Rate ?? 0)), 2) == parsedDigit))//search for AmountDollar NUMBER
                                || c.InvoiceNbr.Contains(searchText)
                        );
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex).Take(pageSize).ToList();

            //TransactionItemAdjustment(result, TreadMarksConstants.SPS);

            return new PaginationDTO<ProcessorCommonTransactionListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

        }

        public PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMStaffTransactionList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType)
        {
            List<RPMCommonTransactionListViewModel> query = new List<RPMCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.SPSR:
                    {
                        query = this.GetRPMSPSTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType, pageIndex, pageSize);
                        break;
                    }
                case TreadMarksConstants.SPS:
                    {
                        query = this.GetRPMSIRTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType, pageIndex, pageSize);
                        break;
                    }
                default:
                    break;
            }

            //    TransactionItemAdjustment(query);

            //    query.ForEach(c =>
            //    {
            //        c.TotalWeight = Math.Round(DataConversion.ConvertKgToTon((double)c.TotalWeight), 4, MidpointRounding.AwayFromZero);
            //    });

            //var totalRecords = query.Count();
            //var result = query.Skip(pageIndex)
            //        .Take(pageSize)
            //        .ToList();

            return new PaginationDTO<RPMCommonTransactionListViewModel, int>
            {
                DTOCollection = query,
                TotalRecords = this.totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<RPMCommonTransactionListViewModel> GetRPMSIRTransactionList(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType, int pageIndex = 0, int pageSize = 0)
        {
            var dbContext = context as TransactionBoundedContext;

            var query = from transaction in dbContext.Transactions.Include(c => c.TransactionItems)
                        join claimDetail in dbContext.ClaimDetails on transaction.ID equals claimDetail.TransactionId
                        where claimId == claimDetail.ClaimId && (TreadMarksConstants.SPS == transaction.TransactionTypeCode) //SPSR => SPS
                        select new RPMCommonTransactionListViewModel()
                        {
                            ID = transaction.ID,
                            DeviceName = transaction.DeviceName ?? string.Empty,
                            TransactionFriendlyId = transaction.FriendlyId,
                            ReviewStatus = transaction.ProcessingStatus ?? string.Empty,
                            IsAdjustByStaff = transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            InvoiceDate = transaction.TransactionDate,
                            VendorNumber = transaction.OutgoingVendor.Number,
                            BusinessName = transaction.OutgoingVendor.BusinessName,
                            ProductType = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().Item.ShortName ?? string.Empty) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().Item.ShortName ?? string.Empty),
                            ProductCode = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().ItemCode ?? string.Empty) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().ItemCode ?? string.Empty),
                            TotalWeight = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().ActualWeight * 0.001m) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().ActualWeight * 0.001m),
                            TransactionId = transaction.ID,
                            MeshRange = (transaction.TransactionAdjustmentID == null) ? string.Concat(transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().MeshRangeStart,
                                (null == transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().MeshRangeStart) ? "" : " to ",
                                transaction.TransactionItems.Where(i => i.TransactionAdjustmentId == null).FirstOrDefault().MeshRangeEnd) :
                                string.Concat(transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().MeshRangeStart,
                                (null == transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().MeshRangeStart) ? "" : " to ",
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(i => i.TransactionAdjustmentId == transaction.TransactionAdjustmentID).FirstOrDefault().MeshRangeEnd),
                            Notes = transaction.TransactionNotes.FirstOrDefault() != null ? transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionNotes = transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                            InvoiceNbr = (transaction.TransactionAdjustmentID == null) ? (transaction.InvoiceNumber ?? string.Empty) :
                            (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).InvoiceNumber ?? string.Empty),
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((!Regex.IsMatch(searchText, @"^\d+(.\d+){0,1}$")) && (DateTime.TryParse(searchText, out dateValue)))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.InvoiceDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.InvoiceDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.InvoiceDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                            c.TransactionFriendlyId.ToString().Contains(searchText)
                            || c.ReviewStatus.ToLower().Contains(searchText)
                            || (
                                 (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                                 (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                                 (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                               )
                            || c.Notes.ToLower().Contains(searchText)
                            || c.VendorNumber.Contains(searchText)
                            || c.InvoiceDate.ToString().Contains(searchText)
                            || c.BusinessName.ToLower().Contains(searchText)
                            || c.ProductType.ToLower().Contains(searchText)
                            || c.ProductCode.ToLower().Contains(searchText)
                            || c.TotalWeight.ToString().Contains(searchText)
                            || c.MeshRange.ToString().Contains(searchText)
                            || c.InvoiceNbr.Contains(searchText)
                            );
                }
            }
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            if (0 == pageSize)
            {
                return query.ToList();
            }
            else
            {
                this.totalRecords = query.Count();
                return query.Skip(pageIndex).Take(pageSize).ToList();
            }
        }
        public PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMTransactionList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType)
        {
            List<RPMCommonTransactionListViewModel> query = new List<RPMCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.SPSR:
                    {
                        query = this.GetRPMSPSTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType, pageIndex, pageSize);
                        break;
                    }
                case TreadMarksConstants.SPS:
                    {
                        query = this.GetRPMSIRTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType, pageIndex, pageSize);
                        break;
                    }

                default:
                    break;
            }

            //    TransactionItemAdjustment(query);

            //    query.ForEach(c =>
            //    {
            //        c.TotalWeight = Math.Round(DataConversion.ConvertKgToTon((double)c.TotalWeight), 4, MidpointRounding.AwayFromZero);
            //    });

            //var totalRecords = query.Count();
            //var result = query.Skip(pageIndex)
            //        .Take(pageSize)
            //        .ToList();

            return new PaginationDTO<RPMCommonTransactionListViewModel, int>
            {
                DTOCollection = query,
                TotalRecords = this.totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<RPMCommonTransactionListViewModel> GetRPMSPSTransactionList(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType, int pageIndex = 0, int pageSize = 0)
        {
            var dbContext = context as TransactionBoundedContext;

            ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;

            var query = from transaction in dbContext.Transactions.Include(c => c.TransactionItems)
                        join claimDetail in dbContext.ClaimDetails on transaction.ID equals claimDetail.TransactionId
                        where claimId == claimDetail.ClaimId && (transactionType == transaction.TransactionTypeCode) && (TransactionStatus.Voided.ToString() != transaction.Status) //SPSR => SPS
                        select new RPMCommonTransactionListViewModel()
                        {
                            ID = transaction.ID,
                            DeviceName = transaction.DeviceName,
                            TransactionFriendlyId = transaction.FriendlyId,
                            ReviewStatus = transaction.ProcessingStatus,
                            IsAdjustByStaff = transaction.TransactionAdjustments.Select(c => c.IsAdjustByStaff).ToList(),
                            InvoiceDate = transaction.TransactionDate,
                            BusinessName = (transaction.TransactionAdjustmentID == null) ? ((null != transaction.CompanyInfo) ? (transaction.CompanyInfo.CompanyName ?? string.Empty) : string.Empty) :
                                ((transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo != null) ? (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo.CompanyName) : (transaction.CompanyInfo.CompanyName ?? string.Empty)),
                            Description = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.FirstOrDefault().ItemDescription ?? string.Empty) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().ItemDescription ?? string.Empty),
                            Destination = (transaction.TransactionAdjustmentID == null) ? ((null != transaction.CompanyInfo) ? (string.Concat(transaction.CompanyInfo.Address.City, ", ", transaction.CompanyInfo.Address.Province, ", ", transaction.CompanyInfo.Address.Country)) : string.Empty) :
                                ((transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo != null) ? string.Concat(
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo.Address.City, ", ", transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo.Address.Province, ", ", transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).CompanyInfo.Address.Country) : string.Empty),
                            ProductType = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.FirstOrDefault().Item.ShortName) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().Item.ShortName),
                            ProductCode = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.FirstOrDefault().ItemCode ?? "0") :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().ItemCode ?? "0"),
                            TotalWeight = (transaction.TransactionAdjustmentID == null) ? transaction.TransactionItems.FirstOrDefault().ActualWeight * 0.001m :
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().ActualWeight * 0.001m,
                            Percentage = (transaction.TransactionAdjustmentID == null) ? (transaction.TransactionItems.FirstOrDefault().RecyclablePercentage ?? 0) :
                                (transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().RecyclablePercentage ?? 0),
                            Rate = (transaction.TransactionAdjustmentID == null) ? transaction.TransactionItems.FirstOrDefault().Rate :
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault().Rate,
                            TransactionId = transaction.ID,
                            Notes = transaction.TransactionNotes.FirstOrDefault() != null ? transaction.TransactionNotes.FirstOrDefault().Note : string.Empty,
                            TransactionNotes = transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                            RetailPrice = transaction.MobileFormat ? (decimal?)null : (transaction.RetailPrice ?? 0),
                            MobileFormat = transaction.MobileFormat,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((!Regex.IsMatch(searchText, @"^\d+(.\d+){0,1}$")) && (DateTime.TryParse(searchText, out dateValue)))//not a valid dcimal i.e 2.131
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.InvoiceDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.InvoiceDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.InvoiceDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    bool bSerachNumber = false;
                    if (Regex.IsMatch(searchText, @"^\d+(.\d+){0,1}$"))
                    {
                        bSerachNumber = true;
                    }
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                            c.TransactionFriendlyId.ToString().Contains(searchText)
                        || c.ReviewStatus.ToLower().Contains(searchText)
                        || (
                             (STAFF.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == true) && c.IsAdjustByStaff.Any()) ||
                             (PARTICIPANT.Contains(searchText) && c.IsAdjustByStaff.All(cd => cd == false) && c.IsAdjustByStaff.Any()) ||
                             (BOTH.Contains(searchText) && (c.IsAdjustByStaff.Any(cd => cd == true) && c.IsAdjustByStaff.Any(cd => cd == false)))
                           )
                        || c.InvoiceDate.ToString().Contains(searchText)
                        || c.BusinessName.ToLower().Contains(searchText)
                        || c.Description.ToLower().Contains(searchText)
                        || c.Destination.ToLower().Contains(searchText)
                        || c.ProductType.ToLower().Contains(searchText)
                        || c.ProductCode.ToLower().Contains(searchText)
                        || Math.Round(c.TotalWeight, 4).ToString().Contains(searchText)
                        || c.Percentage.ToString().Contains(searchText)
                        || c.Rate.ToString().Contains(searchText)
                        || c.RetailPrice.ToString().Contains(searchText)
                        || (bSerachNumber && (Math.Round(((c.Rate ?? 0m) * (c.TotalWeight)), 2)).ToString().Contains(searchText))
                            );
                }
            }
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "TransactionDate" : orderBy;
            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            if (0 == pageSize)
            {
                return query.ToList();
            }
            else
            {
                this.totalRecords = query.Count();
                return query.Skip(pageIndex).Take(pageSize).ToList();
            }
        }
        #endregion

        public void AddTransaction(Transaction transaction)
        {
            entityStore.Insert(transaction);
        }

        //OTSTM2-81
        public void AddTransactionRange(List<Transaction> transactionList)
        {
            var dbContext = context as TransactionBoundedContext;
            dbContext.Transactions.AddRange(transactionList);
            dbContext.SaveChanges();
        }

        public Transaction GetTransactionById(int transactionId)
        {
            var dbContext = context as TransactionBoundedContext;

            return dbContext.Transactions.AsNoTracking()
                //.Include(c => c.IncomingVendor).Include(c => c.IncomingVendor.VendorAddresses)
                //.Include(c => c.OutgoingVendor).Include(c => c.OutgoingVendor.VendorAddresses)
                //.Include(c => c.CompanyInfo).Include(c => c.CompanyInfo.Address)
                //.Include(c => c.VendorInfo).Include(c => c.VendorInfo.VendorAddresses)
                //.Include(c => c.TransactionItems)
                //.Include(c => c.TransactionPhotos).Include(c => c.TransactionPhotos.Select(i => i.Photo))
                //.Include(c => c.TransactionAttachments).Include(c => c.TransactionAttachments.Select(i => i.Attachemnt))
                //.Include(c => c.TransactionNotes).Include(c => c.TransactionNotes.Select(i => i.User))
                //.Include(c => c.ScaleTickets)
                //.Include(c => c.TransactionComments)
                //.Include(c => c.TransactionAdjustments)
                //.Include(c => c.TransactionSupportingDocuments)
                .Where(i => i.ID == transactionId).SingleOrDefault();
        }

        public TransactionAdjustment GetTransactionAdjustmentById(int adjustmentId)
        {
            var dbContext = context as TransactionBoundedContext;

            return dbContext.TransactionAdjustments.AsNoTracking()
                //.Include(c => c.CompanyInfo).Include(c => c.CompanyInfo.Address)
                //.Include(c => c.VendorInfo).Include(c => c.VendorInfo.VendorAddresses)
                //.Include(c => c.TransactionItems)
                //.Include(c => c.TransactionPhotos).Include(c => c.TransactionPhotos.Select(i => i.Photo))
                //.Include(c => c.ScaleTickets)
                //.Include(c => c.TransactionAttachments).Include(c => c.TransactionAttachments.Select(i => i.Attachemnt))
                //.Include(c => c.TransactionSupportingDocuments)
                .Where(i => i.ID == adjustmentId).SingleOrDefault();
        }

        public Transaction GetTransactionById(Guid transactionId)
        {
            return entityStore.AllIncluding(t => t.TransactionPhotos, t => t.TransactionItems, t => t.TransactionNotes, t => t.ScaleTickets, t => t.TransactionComments).Where(t => t.TransactionId == transactionId).FirstOrDefault();
        }

        #region Transaction Attachment

        public void AddTransactionAttachments(List<TransactionAttachment> transactionAttachments)
        {
            var dbContext = context as TransactionBoundedContext;
            transactionAttachments.ForEach(c =>
            {
                dbContext.TransactionAttachments.Add(c);
            });

            dbContext.SaveChanges();
        }

        public Transaction GetTransactionByFriendlyID(long friendlyId)
        {
            return entityStore.All.FirstOrDefault(c => c.FriendlyId == friendlyId && c.Status != "Voided" && c.Status != "Rejected");
        }
        #endregion


        public CompanyInfo FindCompanyByName(string companyName)
        {
            var dbContext = context as TransactionBoundedContext;
            return dbContext.CompanyInfos.FirstOrDefault(c => string.Compare(c.CompanyName, companyName) == 0);
        }

        #region All Transactions
        public PaginationDTO<AllTransactionsViewModel, int> LoadAllTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt = "")
        {
            if (vendorId != 0)
            {
                //Load vendor specific transactions
                return LoadVendorTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, queryFilter, sExtendSearchTxt);
            }
            else
            {
                //Load all transactions
                return LoadTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, sExtendSearchTxt);
            }
        }

        private PaginationDTO<AllTransactionsViewModel, int> LoadVendorTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt)
        {
            using (var dbContext = new TransactionBoundedContext())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;
                searchText = searchText.ToLower();
                var query = from transaction in dbContext.Transactions
                            where ((transaction.IncomingId == vendorId) || (transaction.OutgoingId == vendorId)) && transaction.TransactionTypeCode != queryFilter
                            select new AllTransactionsViewModel()
                            {
                                ID = transaction.ID,
                                TransactionId = transaction.ID.ToString(),
                                Status = transaction.Status,
                                iPadForm = transaction.DeviceName,
                                TransNum = transaction.FriendlyId.ToString(),
                                Type = transaction.TransactionTypeCode,
                                Start = (!transaction.MobileFormat) ? transaction.TransactionDate : transaction.CreatedDate,
                                End = (!transaction.MobileFormat) ? DateTime.MinValue : transaction.TransactionEndDate.Value,
                                SyncAdd = (!transaction.MobileFormat) ? transaction.CreatedDate : transaction.SyncDate, //paper transaction:transaction.CreatedDate(the date user input data to new TM system)
                                IncRegNum = transaction.IncomingId != null ? transaction.IncomingVendor.Number : string.Empty,
                                OutRegNum = transaction.OutgoingId != null ? transaction.OutgoingVendor.Number : string.Empty,
                                BadgeUser = transaction.Badge == null ? transaction.CreatedUser.FirstName + " " + transaction.CreatedUser.LastName : transaction.Badge,
                                BadgeUserID = transaction.CreatedUserId != null ? transaction.CreatedUser.ID.ToString() : string.Empty,
                                MobileFormat = transaction.MobileFormat,
                                CreatedUserId = transaction.CreatedUserId ?? 0,
                                CreatedVendorId = transaction.CreatedVendorId ?? 0,
                            };
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = query.Where(c => c.Status.ToString().Contains(searchText.ToLower())
                                            || c.iPadForm.ToString().Contains(searchText.ToLower())
                                            || c.TransNum.ToString().Contains(searchText.ToLower())
                                            || (("" == sExtendSearchTxt) ? c.Type.ToLower().Contains(searchText.ToLower()) : false)
                                            || (c.Type.ToLower() == sExtendSearchTxt)
                                            || c.Start.ToString().Contains(searchText.ToLower())
                                            || c.End.ToString().Contains(searchText.ToLower())
                                            || c.SyncAdd.ToString().Contains(searchText.ToLower())
                                            || c.IncRegNum.ToString().Contains(searchText.ToLower())
                                            || c.OutRegNum.ToString().Contains(searchText.ToLower())
                                            || c.BadgeUser.ToString().Contains(searchText.ToLower())
                                            );
                }

                orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                var totalRecords = query.Count();
                var result = query.Skip(pageIndex).Take(pageSize).ToList();
                TransactionItemAdjustment(result);
                return new PaginationDTO<AllTransactionsViewModel, int>
                {
                    DTOCollection = result,
                    TotalRecords = totalRecords,
                    PageNumber = pageIndex + 1,
                    PageSize = pageSize
                };
            }
        }
        private PaginationDTO<AllTransactionsViewModel, int> LoadTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string sExtendSearchTxt)
        {
            using (var dbContext = new TransactionBoundedContext())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;
                searchText = searchText.ToLower();
                var query = from transaction in dbContext.Transactions
                            select new AllTransactionsViewModel()
                            {
                                ID = transaction.ID,
                                TransactionId = transaction.ID.ToString(),
                                Status = transaction.Status,
                                iPadForm = transaction.DeviceName,
                                TransNum = transaction.FriendlyId.ToString(),
                                Type = transaction.TransactionTypeCode,
                                Start = (!transaction.MobileFormat) ? transaction.TransactionDate : transaction.CreatedDate,
                                End = (!transaction.MobileFormat) ? DateTime.MinValue : transaction.TransactionEndDate.Value,
                                SyncAdd = (!transaction.MobileFormat) ? transaction.CreatedDate : transaction.SyncDate,
                                IncRegNum = transaction.IncomingId != null ? transaction.IncomingVendor.Number : string.Empty,
                                OutRegNum = transaction.OutgoingId != null ? transaction.OutgoingVendor.Number : string.Empty,
                                BadgeUser = transaction.Badge == null ? transaction.CreatedUser.FirstName + " " + transaction.CreatedUser.LastName : transaction.Badge,
                                BadgeUserID = transaction.CreatedUserId != null ? transaction.CreatedUser.ID.ToString() : string.Empty,
                                MobileFormat = transaction.MobileFormat,
                                CreatedUserId = transaction.CreatedUserId ?? 0,
                                CreatedVendorId = transaction.CreatedVendorId ?? 0,
                            };
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = query.Where(c => c.Status.ToString().Contains(searchText.ToLower())
                                            || c.iPadForm.ToString().Contains(searchText.ToLower())
                                            || c.TransNum.ToString().Contains(searchText.ToLower())
                                            || (("" == sExtendSearchTxt) ? c.Type.ToLower().Contains(searchText.ToLower()) : false)
                                            || (c.Type.ToLower() == sExtendSearchTxt)
                                            || c.Start.ToString().Contains(searchText.ToLower())
                                            || c.End.ToString().Contains(searchText.ToLower())
                                            || c.SyncAdd.ToString().Contains(searchText.ToLower())
                                            || c.IncRegNum.ToString().Contains(searchText.ToLower())
                                            || c.OutRegNum.ToString().Contains(searchText.ToLower())
                                            || c.BadgeUser.ToString().Contains(searchText.ToLower())
                                            );
                }

                orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                var totalRecords = query.Count();
                var result = query.Skip(pageIndex).Take(pageSize).ToList();
                TransactionItemAdjustment(result);
                return new PaginationDTO<AllTransactionsViewModel, int>
                {
                    DTOCollection = result,
                    TotalRecords = totalRecords,
                    PageNumber = pageIndex + 1,
                    PageSize = pageSize
                };
            }
        }

        public List<AllTransactionsViewModel> LoadAllTransactionsList(string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter)
        {
            using (var dbContext = new TransactionBoundedContext())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;
                searchText = searchText.ToLower();
                if (vendorId != 0)
                {
                    var query = from transaction in dbContext.Transactions
                                where ((transaction.IncomingId == vendorId) || (transaction.OutgoingId == vendorId)) && transaction.TransactionTypeCode != queryFilter
                                select new AllTransactionsViewModel()
                                {
                                    ID = transaction.ID,
                                    TransactionId = transaction.ID.ToString(),
                                    Status = transaction.Status,
                                    iPadForm = transaction.DeviceName,
                                    TransNum = transaction.FriendlyId.ToString(),
                                    Type = transaction.TransactionTypeCode,
                                    Start = (!transaction.MobileFormat) ? transaction.TransactionDate : transaction.CreatedDate,
                                    End = (!transaction.MobileFormat) ? DateTime.MinValue : transaction.TransactionEndDate.Value,
                                    SyncAdd = (!transaction.MobileFormat) ? transaction.CreatedDate : transaction.SyncDate, //paper transaction:transaction.CreatedDate(the date user input data to new TM system)
                                    IncRegNum = transaction.IncomingId != null ? transaction.IncomingVendor.Number : string.Empty,
                                    OutRegNum = transaction.OutgoingId != null ? transaction.OutgoingVendor.Number : string.Empty,
                                    BadgeUser = transaction.Badge == null ? transaction.CreatedUser.FirstName + " " + transaction.CreatedUser.LastName : transaction.Badge,
                                    BadgeUserID = transaction.CreatedUserId != null ? transaction.CreatedUser.ID.ToString() : string.Empty,
                                    MobileFormat = transaction.MobileFormat,
                                    CreatedUserId = transaction.CreatedUserId ?? 0,
                                    CreatedVendorId = transaction.CreatedVendorId ?? 0,
                                };
                    if (!string.IsNullOrWhiteSpace(searchText))
                    {
                        query = query.Where(c => c.Status.ToString().Contains(searchText.ToLower())
                                                || c.iPadForm.ToString().Contains(searchText.ToLower())
                                                || c.TransNum.ToString().Contains(searchText.ToLower())
                                                || c.Type.ToLower().Contains(searchText.ToLower())
                                                || c.Start.ToString().Contains(searchText.ToLower())
                                                || c.End.ToString().Contains(searchText.ToLower())
                                                || c.SyncAdd.ToString().Contains(searchText.ToLower())
                                                || c.IncRegNum.ToString().Contains(searchText.ToLower())
                                                || c.OutRegNum.ToString().Contains(searchText.ToLower())
                                                );
                    }

                    orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;
                    query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                    return query.ToList();
                }
                else
                {
                    var query = from transaction in dbContext.Transactions
                                select new AllTransactionsViewModel()
                                {
                                    TransactionId = transaction.ID.ToString(),
                                    Status = transaction.Status,
                                    iPadForm = transaction.DeviceName,
                                    TransNum = transaction.FriendlyId.ToString(),
                                    Type = transaction.TransactionTypeCode,
                                    Start = (!transaction.MobileFormat) ? transaction.TransactionDate : transaction.CreatedDate,
                                    End = (!transaction.MobileFormat) ? DateTime.MinValue : transaction.TransactionEndDate.Value,
                                    SyncAdd = (!transaction.MobileFormat) ? transaction.CreatedDate : transaction.SyncDate, //paper transaction:transaction.CreatedDate(the date user input data to new TM system)
                                    IncRegNum = transaction.IncomingId != null ? transaction.IncomingVendor.Number : string.Empty,
                                    OutRegNum = transaction.OutgoingId != null ? transaction.OutgoingVendor.Number : string.Empty,
                                    BadgeUser = transaction.Badge == null ? transaction.CreatedUser.FirstName + " " + transaction.CreatedUser.LastName : transaction.Badge,
                                    BadgeUserID = transaction.CreatedUserId != null ? transaction.CreatedUser.ID.ToString() : string.Empty,
                                    MobileFormat = transaction.MobileFormat,
                                    CreatedUserId = transaction.CreatedUserId ?? 0,
                                    CreatedVendorId = transaction.CreatedVendorId ?? 0,
                                };
                    if (!string.IsNullOrWhiteSpace(searchText))
                    {
                        query = query.Where(c => c.Status.ToString().Contains(searchText.ToLower())
                                                || c.iPadForm.ToString().Contains(searchText.ToLower())
                                                || c.TransNum.ToString().Contains(searchText.ToLower())
                                                || c.Type.ToLower().Contains(searchText.ToLower())
                                                || c.Start.ToString().Contains(searchText.ToLower())
                                                || c.End.ToString().Contains(searchText.ToLower())
                                                || c.SyncAdd.ToString().Contains(searchText.ToLower())
                                                || c.IncRegNum.ToString().Contains(searchText.ToLower())
                                                || c.OutRegNum.ToString().Contains(searchText.ToLower())
                                                );
                    }

                    orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;
                    query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                    return query.ToList();
                }
            }
        }

        public bool UpdateTransactionStatus(int transactionId, string oldStatus, string newStatus)
        {
            var transaction = entityStore.FindById(transactionId);
            if ((string.Compare(transaction.Status, oldStatus, StringComparison.OrdinalIgnoreCase) == 0)
                || (transaction.MobileFormat && (string.Compare(transaction.Status, TransactionStatus.Incomplete.ToString(), StringComparison.OrdinalIgnoreCase) == 0)))//Void a mobile transaction
            {
                //update status
                transaction.Status = newStatus;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateSPSRTransactionStatus(int transactionId, string oldStatus, string newStatus)
        {
            var transaction = entityStore.FindById(transactionId);
            if (string.Compare(transaction.ProcessingStatus, oldStatus, StringComparison.OrdinalIgnoreCase) == 0)
            {
                //update status
                transaction.Status = newStatus;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public void UdpateIncompleteTransaction(Transaction transaction)
        {
            var original = GetTransactionById(transaction.TransactionId);
            if (original != null)
            {
                transaction.ID = original.ID;
                context.Entry(original).CurrentValues.SetValues(transaction);
                context.SaveChanges();
            }
            else
                entityStore.Insert(transaction);
            //entityStore.Update(transaction);
        }

        public void UpdateCompletedTransaction(Transaction newTransaction)
        {
            var dbContext = context as TransactionBoundedContext;
            var dbTransaction = dbContext.Transactions.FirstOrDefault(c => c.TransactionId == newTransaction.TransactionId);
            if (dbTransaction != null)
            {
                dbContext.Transactions.Remove(dbTransaction);
            }
            dbContext.Transactions.Add(newTransaction);
            dbContext.SaveChanges();
        }

        public bool UdpateTransactioProcessingStatus(int transactionId, TransactionProcessingStatus status)
        {
            using (var dbContext = new TransactionBoundedContext())
            {
                var transaction = dbContext.Transactions.FirstOrDefault(c => c.ID == transactionId);
                transaction.ProcessingStatus = status.ToString();
                dbContext.SaveChanges();
                return true;
            }
        }
        public bool AddTransactionRetailPrice(int transactionId, decimal retailPrice)
        {
            var transaction = entityStore.FindById(transactionId);

            transaction.RetailPrice = retailPrice;
            context.SaveChanges();

            return true;
        }

        #endregion

        public void AddTransactionAttachments(List<AttachmentModel> attachmentModelList)
        {
            var dbContext = context as TransactionBoundedContext;

            var attachmentList = new List<Attachment>();
            attachmentModelList.ForEach(c => { attachmentList.Add(AutoMapper.Mapper.Map<Attachment>(c)); });

            dbContext.Attachments.AddRange(attachmentList);
            dbContext.SaveChanges();

            foreach (var attachment in attachmentList)
            {
                var attachmentModel = attachmentModelList.Find(a => a.UniqueName == attachment.UniqueName);
                attachmentModel.ID = attachment.ID;
            }
        }

        public void UpdateAttachmentDescription(List<TransactionFileUploadModel> list)
        {
            var dbContext = (context as TransactionBoundedContext);

            var attachmentIdList = list.Select(i => i.AttachmentId).ToList();
            var attachmentList = dbContext.Attachments.Where(a => attachmentIdList.Contains(a.ID));
            foreach (var attachment in attachmentList)
            {
                var attachmentDoc = list.FirstOrDefault(doc => doc.AttachmentId == attachment.ID);
                if (attachmentDoc != null) attachment.Description = attachmentDoc.Description;
            }

            dbContext.SaveChanges();
        }

        public AttachmentModel GetTransactionAttachment(int attachmentId)
        {
            var dbContext = context as TransactionBoundedContext;

            var attachment = dbContext.Attachments.FirstOrDefault(a => a.ID == attachmentId);
            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveTemporaryTransactionAttachment(int attachmentId)
        {
            var dbContext = (context as TransactionBoundedContext);

            var attachment = dbContext.Attachments.FirstOrDefault(a => a.ID == attachmentId);
            if (attachment != null)
            {
                dbContext.Attachments.Remove(attachment);
                dbContext.SaveChanges();
            }
        }

        public void RemoveTemporaryTransactionAttachments(List<int> attachmentIdList)
        {
            var dbContext = (context as TransactionBoundedContext);

            var attachmentList = dbContext.Attachments.Where(a => attachmentIdList.Contains(a.ID)).ToList();
            if (attachmentList.Any())
            {
                dbContext.Attachments.RemoveRange(attachmentList);
                dbContext.SaveChanges();
            }
        }

        public TransactionNote AddTransactionInternalNote(TransactionNote note)
        {
            using (var dbContext = new TransactionBoundedContext())
            {
                dbContext.Notes.Add(note);
                dbContext.SaveChanges();
                return note;
            }
        }

        //OTSTM2-765
        public List<TransactionNote> GetAllTransactionInternalNote(int transactionId)
        {
            var dbContext = context as TransactionBoundedContext;

            return dbContext.Notes.Where(c => c.TransactionId == transactionId).Select(c => c).ToList();
        }

        public long GetNextAutoPaperFormFriendlyID(long initialValue, string[] typeCodeList)
        {
            var dbContext = context as TransactionBoundedContext;

            if (dbContext.Transactions.Any(i => typeCodeList.Contains(i.TransactionTypeCode))) return dbContext.Transactions.Where(i => typeCodeList.Contains(i.TransactionTypeCode)).Max(i => i.FriendlyId) + 1;

            return initialValue;
        }

        public TransactionAdjustment AddSTCTransactionAdjustment(TransactionAdjustment adjustment, string transactionFromPostalCode)
        {
            var dbContext = context as TransactionBoundedContext;

            // mark existing accepted/pending adjustments as "SystemRejected" (NOTE: There should be only one Accepted adjustment will be found here, but just in case rejecting all)
            var adjustments = dbContext.TransactionAdjustments.Where(i => i.OriginalTransactionId == adjustment.OriginalTransactionId && (i.Status == TransactionAdjustmentStatus.Accepted.ToString() || i.Status == TransactionAdjustmentStatus.Pending.ToString())).ToList();

            adjustments.ForEach(c =>
            {
                c.Status = TransactionAdjustmentStatus.SystemRejected.ToString();
            });

            // add new adjustment
            dbContext.TransactionAdjustments.Add(adjustment);

            //for single party transaction, adjustment added with "Accepted", then update transaction.TransactionAdjustmentID with ID of newly added transaction adj.
            var transaction = dbContext.Transactions.SingleOrDefault(i => i.ID == adjustment.OriginalTransactionId);
            transaction.TransactionAdjustmentID = null;
            if (adjustment.Status == TransactionAdjustmentStatus.Accepted.ToString())
            {
                var newAdjustment = dbContext.TransactionAdjustments.Where(i => ((i.OriginalTransactionId == adjustment.OriginalTransactionId) && (i.Status == TransactionAdjustmentStatus.Accepted.ToString()))).FirstOrDefault();
                transaction.TransactionAdjustmentID = (newAdjustment != null) ? ((int?)(newAdjustment.ID)) : null;
            }

            transaction.FromPostalCode = transactionFromPostalCode;
            dbContext.SaveChanges();

            return adjustment;
        }

        public TransactionAdjustment AddTransactionAdjustment(TransactionAdjustment adjustment)
        {
            var dbContext = context as TransactionBoundedContext;

            // mark existing accepted/pending adjustments as "SystemRejected" (NOTE: There should be only one Accepted adjustment will be found here, but just in case rejecting all)
            var adjustments = dbContext.TransactionAdjustments.Where(i => i.OriginalTransactionId == adjustment.OriginalTransactionId && (i.Status == TransactionAdjustmentStatus.Accepted.ToString() || i.Status == TransactionAdjustmentStatus.Pending.ToString())).ToList();

            adjustments.ForEach(c =>
            {
                c.Status = TransactionAdjustmentStatus.SystemRejected.ToString();
            });

            // add new adjustment
            dbContext.TransactionAdjustments.Add(adjustment);
            dbContext.SaveChanges(); //OTSTM2-1012

            //for single party transaction, adjustment added with "Accepted", then update transaction.TransactionAdjustmentID with ID of newly added transaction adj.
            var transaction = dbContext.Transactions.SingleOrDefault(i => i.ID == adjustment.OriginalTransactionId);
            transaction.TransactionAdjustmentID = null;
            if (adjustment.Status == TransactionAdjustmentStatus.Accepted.ToString())
            {
                var newAdjustment = dbContext.TransactionAdjustments.Where(i => ((i.OriginalTransactionId == adjustment.OriginalTransactionId) && (i.Status == TransactionAdjustmentStatus.Accepted.ToString()))).FirstOrDefault();
                transaction.TransactionAdjustmentID = (newAdjustment != null) ? ((int?)(newAdjustment.ID)) : null;
            }
            dbContext.SaveChanges();

            return adjustment;
        }

        public void ChangeTransactionAdjustmentStatus(int adjustmentId, TransactionAdjustmentStatus reviewStatus, long changedById, int transactionId)
        {
            var dbContext = context as TransactionBoundedContext;

            var adjustment = dbContext.TransactionAdjustments.SingleOrDefault(i => i.ID == adjustmentId);
            var transaction = dbContext.Transactions.SingleOrDefault(i => i.ID == transactionId);

            if (adjustment != null)
            {
                adjustment.Status = reviewStatus.ToString();
                if (reviewStatus == TransactionAdjustmentStatus.Accepted)
                {
                    adjustment.ApprovalUser = changedById;
                    adjustment.ApprovalDate = DateTime.UtcNow;

                    transaction.TransactionAdjustmentID = adjustmentId;
                }

                if (reviewStatus == TransactionAdjustmentStatus.Rejected)
                {
                    adjustment.RejectedBy = changedById;
                    adjustment.RejectedDate = DateTime.UtcNow;

                    transaction.TransactionAdjustmentID = null;
                }

                if (reviewStatus == TransactionAdjustmentStatus.Recalled)
                {
                    adjustment.RecallBy = changedById;
                    adjustment.RecallDate = DateTime.UtcNow;
                    transaction.TransactionAdjustmentID = null;
                }

                dbContext.SaveChanges();
            }
        }

        public void TransactionItemAdjustment<T>(List<T> returnVal, string transactionType = "")
        {
            var dbContext = context as TransactionBoundedContext;
            Type listType = typeof(T);
            //Hauler
            if (listType == typeof(HaulerStaffCommonTransactionListViewModel))
            {

                var transIdList = returnVal.OfType<HaulerStaffCommonTransactionListViewModel>().Select(i => i.ID).ToList();
                List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();
                //acceptedAdjustmentList = dbContext.TransactionAdjustments.Where(i => transIdList.Contains(i.OriginalTransactionId) && (i.Status == TransactionAdjustmentStatus.Accepted.ToString() || i.Status == TransactionAdjustmentStatus.Pending.ToString())).ToList();

                acceptedAdjustmentList.ForEach(adj =>
                {
                    var transaction = returnVal.OfType<HaulerStaffCommonTransactionListViewModel>().SingleOrDefault(i => i.ID == adj.OriginalTransactionId);

                    if (transaction != null)
                    {
                        transaction.TireCountList = adj.TransactionItems.Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                        transaction.IsGenerateTires = adj.IsGenerateTires;
                        transaction.OnRoadWeight = adj.OnRoadWeight ?? 0;
                        transaction.OffRoadWeight = adj.OffRoadWeight ?? 0;
                        transaction.ScaleWeight = adj.ScaleTickets.Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0)));
                        transaction.EstWeight = (adj.EstimatedOnRoadWeight ?? 0) + (adj.EstimatedOffRoadWeight ?? 0);
                        //STC
                        transaction.PostalCode = (null != adj.CompanyInfo) ? ((null != adj.CompanyInfo.Address) ? adj.CompanyInfo.Address.PostalCode : transaction.PostalCode) : transaction.PostalCode;
                        transaction.NameofGroupIndividual = (null != adj.CompanyInfo) ? adj.CompanyInfo.CompanyName : transaction.NameofGroupIndividual;
                        //RTR
                        transaction.BuyerName = (null != adj.CompanyInfo) ? adj.CompanyInfo.CompanyName : transaction.BuyerName;
                    }
                });
            }

            if (listType == typeof(HaulerCommonTransactionListViewModel))
            {
                var transIdList = returnVal.OfType<HaulerCommonTransactionListViewModel>().Select(i => i.ID).ToList();
                List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Include(c => c.TransactionItems).Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();
                acceptedAdjustmentList.ForEach(adj =>
                {
                    var transaction = returnVal.OfType<HaulerCommonTransactionListViewModel>().SingleOrDefault(i => i.ID == adj.OriginalTransactionId);

                    if (transaction != null)
                    {
                        transaction.TireCountList = adj.TransactionItems.Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                        transaction.IsGenerateTires = adj.IsGenerateTires;
                        transaction.OnRoadWeight = adj.OnRoadWeight ?? 0;
                        transaction.OffRoadWeight = adj.OffRoadWeight ?? 0;
                        transaction.ScaleWeight = adj.ScaleTickets.Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0)));
                        transaction.EstWeight = (adj.EstimatedOnRoadWeight ?? 0) + (adj.EstimatedOffRoadWeight ?? 0);
                        //STC
                        transaction.PostalCode = (null != adj.CompanyInfo) ? ((null != adj.CompanyInfo.Address) ? adj.CompanyInfo.Address.PostalCode : transaction.PostalCode) : transaction.PostalCode;
                        transaction.NameofGroupIndividual = (null != adj.CompanyInfo) ? adj.CompanyInfo.CompanyName : transaction.NameofGroupIndividual;
                        //RTR
                        transaction.BuyerName = (null != adj.CompanyInfo) ? adj.CompanyInfo.CompanyName : transaction.BuyerName;
                    }
                });
            }

            //Collector
            if (listType == typeof(CollectorCommonTransactionListViewModel))
            {
                var transIdList = returnVal.OfType<CollectorCommonTransactionListViewModel>().Select(i => i.ID).ToList();
                List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Include(c => c.TransactionItems).Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();
                acceptedAdjustmentList.ForEach(adj =>
                {
                    var transaction = returnVal.OfType<CollectorCommonTransactionListViewModel>().SingleOrDefault(i => i.ID == adj.OriginalTransactionId);

                    if (transaction != null)
                    {
                        transaction.IsGenerateTires = adj.IsGenerateTires;
                        transaction.TireCountList = adj.TransactionItems.Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();

                        //DOT
                        transaction.ScaleWeight = adj.ScaleTickets.Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0)));
                        transaction.EstWeight = (adj.EstimatedOnRoadWeight ?? 0) + (adj.EstimatedOffRoadWeight ?? 0);
                    }
                });
            }

            //Processor
            if (listType == typeof(ProcessorCommonTransactionListViewModel))
            {
                var transIdList = returnVal.OfType<ProcessorCommonTransactionListViewModel>().Select(i => i.ID).ToList();
                List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Include(c => c.TransactionItems).Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();
                acceptedAdjustmentList.ForEach(adj =>
                {
                    var transaction = returnVal.OfType<ProcessorCommonTransactionListViewModel>().SingleOrDefault(i => i.ID == adj.OriginalTransactionId);

                    if (transaction != null)
                    {
                        transaction.TireCountList = adj.TransactionItems.Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                        transaction.OnRoadWeight = adj.OnRoadWeight ?? 0;
                        transaction.OffRoadWeight = adj.OffRoadWeight ?? 0;
                        if (transactionType.ToLower() == TreadMarksConstants.PTR.ToString().ToLower())
                        {
                            transaction.ScaleWeight = adj.ScaleTickets.Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0)));
                            transaction.EstWeight = ((adj.EstimatedOnRoadWeight ?? 0) + (adj.EstimatedOffRoadWeight ?? 0));
                        }
                        else
                        {
                            transaction.ScaleWeight = DataConversion.ConvertKgToTon((double)adj.ScaleTickets.Sum(c => ((2 == c.ScaleTicketType) ? 0 : (c.InboundWeight ?? 0)) - ((1 == c.ScaleTicketType) ? 0 : (c.OutboundWeight ?? 0))));
                            transaction.EstWeight = DataConversion.ConvertKgToTon((double)((adj.EstimatedOnRoadWeight ?? 0) + (adj.EstimatedOffRoadWeight ?? 0)));
                        }

                        //PIT
                        transaction.Rate = (adj.TransactionItems.FirstOrDefault() != null) ? adj.TransactionItems.FirstOrDefault().Rate : 0;
                        if (transactionType.ToLower() == TreadMarksConstants.PIT.ToString().ToLower())
                        {
                            transaction.ProductType = (adj.TransactionItems.FirstOrDefault() != null) ? adj.TransactionItems.FirstOrDefault(i => i.TransactionAdjustmentId != null).ItemID.ToString() : transaction.ProductType;
                            transaction.AmountDollar = Math.Round((transaction.ScaleWeight ?? 0) * (transaction.Rate ?? 0), 2, MidpointRounding.AwayFromZero);
                        }

                        //DOR
                        transaction.MaterialType = adj.MaterialType ?? 0;
                        transaction.DispositionReason = adj.DispositionReason.ToString();
                        transaction.ScaleTicketNbr = (adj.ScaleTickets.Count > 0) ? (adj.ScaleTickets.FirstOrDefault()).TicketNumber : transaction.ScaleTicketNbr;
                        if (transactionType.ToLower() == TreadMarksConstants.DOR.ToString().ToLower())
                        {
                            transaction.VendorBusinessName = ((null != adj.VendorInfo) && (null != adj.VendorInfo.Number)) ? adj.VendorInfo.Number : (adj.CompanyInfo != null ? adj.CompanyInfo.CompanyName : transaction.VendorBusinessName);
                            if ((null != adj.CompanyInfo) && (null != adj.CompanyInfo.Address)) //broken data on mxotspso3.ontariots.ad
                            {
                                transaction.Destination = (null != adj.CompanyInfo.Address.Country) ? string.Concat(adj.CompanyInfo.Address.City, ", ", adj.CompanyInfo.Address.Province, ", ", adj.CompanyInfo.Address.Country)
                                                    : string.Concat(adj.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City, ", ",
                                                    adj.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Province, ", ",
                                                    adj.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Country);
                            }
                            if (transaction.MaterialTypeName == TreadMarksConstants.TireRims.ToString())
                            {
                                transaction.ScaleWeight = DataConversion.ConvertKgToTon((double)((adj.OnRoadWeight ?? 0) + (adj.OffRoadWeight ?? 0)));
                            }
                        }

                        //sps
                        if (transactionType.ToLower() == TreadMarksConstants.SPS.ToString().ToLower())
                        {
                            transaction.TransactionItems = adj.TransactionItems ?? transaction.TransactionItems;
                            transaction.AmountDollar = (adj.TransactionItems.FirstOrDefault() != null) ? Math.Round((adj.TransactionItems.FirstOrDefault().Rate.Value * (Math.Round(adj.TransactionItems.FirstOrDefault().ActualWeight / 1000, 4, MidpointRounding.AwayFromZero))), 2, MidpointRounding.AwayFromZero) : 0;
                            transaction.InvoiceNbr = adj.InvoiceNumber;
                            transaction.MeshRange = string.Concat(transaction.TransactionItems.FirstOrDefault().MeshRangeStart,
                                (null != transaction.TransactionItems.FirstOrDefault().MeshRangeStart ? " to " : string.Empty), transaction.TransactionItems.FirstOrDefault().MeshRangeEnd);
                            transaction.ProductType = transaction.TransactionItems.FirstOrDefault().Item.ShortName ?? string.Empty;
                            if (adj.CompanyInfo != null)
                            {
                                transaction.VendorBusinessName = adj.CompanyInfo.CompanyName ?? string.Empty;
                                transaction.Destination = string.Concat(adj.CompanyInfo.Address.City, ", ", adj.CompanyInfo.Address.Province, ", ", adj.CompanyInfo.Address.Country);
                            }
                        }
                    }
                });
            }

            //RPM
            if (listType == typeof(RPMCommonTransactionListViewModel))
            {
                var transIdList = returnVal.OfType<RPMCommonTransactionListViewModel>().Select(i => i.ID).ToList();
                List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Include(c => c.TransactionItems).Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();
                acceptedAdjustmentList.ForEach(adj =>
                {
                    var transaction = returnVal.OfType<RPMCommonTransactionListViewModel>().SingleOrDefault(i => i.ID == adj.OriginalTransactionId);

                    if (transaction != null)
                    {
                        //SPS
                        transaction.TireCountList = adj.TransactionItems.Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                        transaction.Rate = adj.TransactionItems.FirstOrDefault().Rate ?? 0;
                        transaction.TotalWeight = adj.TransactionItems.FirstOrDefault().ActualWeight;
                        transaction.BusinessName = (null != adj.CompanyInfo) ? (adj.CompanyInfo.CompanyName ?? transaction.BusinessName) : transaction.BusinessName;
                        transaction.Description = adj.Transaction.TransactionItems.FirstOrDefault().ItemDescription ?? string.Empty;
                        transaction.Destination = (null != adj.CompanyInfo) ? (string.Concat(adj.Transaction.CompanyInfo.Address.City, ", ", adj.Transaction.CompanyInfo.Address.Province, ", ", adj.Transaction.CompanyInfo.Address.Country)) : string.Empty;
                        transaction.ProductType = adj.TransactionItems.FirstOrDefault().Item.ShortName;
                        transaction.ProductCode = adj.TransactionItems.FirstOrDefault().ItemCode ?? "0";
                        transaction.Percentage = adj.TransactionItems.FirstOrDefault().RecyclablePercentage ?? 0;

                        //SIR
                        transaction.MeshRange = string.Concat(adj.TransactionItems.FirstOrDefault().MeshRangeStart, (null == adj.TransactionItems.FirstOrDefault().MeshRangeStart) ? "" : " to ", adj.TransactionItems.FirstOrDefault().MeshRangeEnd);
                        transaction.InvoiceNbr = adj.InvoiceNumber;
                    }
                });
            }
        }

        private static void PopulateItemsForAdjustment(TransactionBoundedContext dbContext,
            List<TransactionAdjustment> acceptedAdjustmentList)
        {
            var transactionItems = dbContext.Items.ToList();
            acceptedAdjustmentList.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    i.Item = transactionItems.FirstOrDefault(q => q.ID == i.ItemID);
                });
            });
        }

        public List<Transaction> GetTransactionsByClaimId(int claimId, int vendorId, string transactionType = "")
        {
            var dbContext = context as TransactionBoundedContext;
            var query = from transaction in dbContext.Transactions.Include(c => c.TransactionItems)
                        join claimDetail in dbContext.ClaimDetails on transaction.ID equals claimDetail.TransactionId
                        where (transaction.IncomingId == vendorId) && claimId == claimDetail.ClaimId &&
                        (transactionType == transaction.TransactionTypeCode || transactionType == string.Empty) &&
                       (transaction.ProcessingStatus != "Invalidated")
                        select transaction;
            return query.Cast<Transaction>().ToList();
        }

        public List<Transaction> GetTransactionsByPeriod(Period period, int vendorId, string transactionType)
        {
            var dbContext = context as TransactionBoundedContext;
            var query = from claim in dbContext.Claims
                        join claimDetail in dbContext.ClaimDetails on claim.ID equals claimDetail.ClaimId
                        join transaction in dbContext.Transactions on claimDetail.TransactionId equals transaction.ID
                        where claim.ClaimPeriodId == period.ID && claim.ParticipantId == vendorId &&
                              (transactionType == "" || transaction.TransactionTypeCode == transactionType)
                        select transaction;
            return query.ToList();
        }

        public Period GetPreviousPeriod(Period period)
        {
            var dbContext = context as TransactionBoundedContext;
            return
                dbContext.Periods.Where(
                    p =>
                        p.PeriodType == period.PeriodType && DbFunctions.TruncateTime(p.StartDate) < period.StartDate.Date &&
                        DbFunctions.TruncateTime(p.EndDate) < period.EndDate.Date).OrderByDescending(i => i.ID).FirstOrDefault();
        }

        //OTSTM2-620
        public TransactionAdjustment GetMostRecentTransactionAdjustment(int claimId, int transactionId)
        {
            var dbContext = context as TransactionBoundedContext;

            var queryAdjust = from transactionAdjustment in dbContext.TransactionAdjustments
                              join claimDetail in dbContext.ClaimDetails on transactionAdjustment.OriginalTransactionId equals
                                  claimDetail.TransactionId
                              where claimDetail.ClaimId == claimId && transactionAdjustment.OriginalTransactionId == transactionId
                              orderby transactionAdjustment.AdjustmentDate descending
                              select transactionAdjustment;

            return (queryAdjust.FirstOrDefault());
        }

        #region BusinessRules

        public List<PreviousScaleTicketResult> GetDuplicateScaleTicket(string transactionType, int Id, string ticketNumber, int? periodType, bool is_Reg_Transaction = true, bool is_SPS = false)
        {
            var dbContext = context as TransactionBoundedContext;

            var allFiltered = from scaleTicket in dbContext.ScaleTickets.AsNoTracking()
                              join trans in dbContext.Transactions on scaleTicket.TransactionId equals trans.ID
                              join claimDetail in dbContext.ClaimDetails on trans.ID equals claimDetail.TransactionId
                              join TAdj in dbContext.TransactionAdjustments on scaleTicket.TransactionAdjustmentId equals TAdj.ID into tAdjstment
                              from transAdj in tAdjstment.DefaultIfEmpty()
                              where trans.TransactionTypeCode == transactionType &&
                              claimDetail.Claim.ClaimPeriod.PeriodType == periodType
                              && (transAdj.Status == "Accepted" || transAdj.Status == null)
                              select new PreviousScaleTicketResult()
                              {
                                  TransactionId = scaleTicket.TransactionId.ToString(),
                                  TicketNumber = scaleTicket.TicketNumber,
                                  TransactionTypeCode = trans.TransactionTypeCode,
                                  IncomingId = trans.IncomingId.ToString(),
                                  OutgoingId = trans.OutgoingId.ToString(),
                                  PeriodShortName = claimDetail.Claim.ClaimPeriod.ShortName,
                                  TransactionNumber = trans.FriendlyId.ToString(),
                                  OutgoingVendor = trans.OutgoingVendor,
                                  IncomingVendor = trans.IncomingVendor,
                                  Status = transAdj.Status
                              };

            var all = new List<PreviousScaleTicketResult>();

            if (is_Reg_Transaction)
            {
                all = allFiltered.Where(c => c.IncomingId == Id.ToString()).ToList();
            }
            else if (is_SPS)
            {
                all = allFiltered.Where(c => c.OutgoingId == Id.ToString()).ToList();
            }

            var allAccepted = all.Where(c => c.Status == "Accepted");

            var temp = allAccepted.Select(i => i.TransactionId).ToArray();

            var allOriginal = all.ToList().Where(c => !temp.Contains(c.TransactionId));

            var result = allAccepted.Union(allOriginal).Where(val => val.TicketNumber.ToLower() == ticketNumber.ToLower()).ToList();

            return result;
        }

        //OTSTM2-414, simplified version
        public bool HasAnyDuplicatePaperTransaction(int friendlyId)
        {
            var dbContext = context as TransactionBoundedContext;
            var query = from trans in dbContext.Transactions
                        where trans.FriendlyId == friendlyId &&
                                      trans.MobileFormat == false &&
                                        trans.Status != "Voided"
                        select trans;
            return query.Any();
        }
        public List<DuplicateTransactionModel> GetDuplicatePaperTransactions(int friendlyId)
        {
            var dbContext = context as TransactionBoundedContext;

            //OTSTM2-414 First version of implementation used reg# and the claim periods, later changed to include all transactions not only completed ones
            var query = from trans in dbContext.Transactions
                        join claimDetailIn in dbContext.ClaimDetails on new { tID = trans.ID, cdDir = true } equals new { tID = claimDetailIn.TransactionId, cdDir = claimDetailIn.Direction }
                        join claimDetail in dbContext.ClaimDetails on new { tID = trans.ID, cdDir = false } equals new { tID = claimDetail.TransactionId, cdDir = claimDetail.Direction } into claimDetailO
                        from claimDetailOut in claimDetailO.DefaultIfEmpty()
                        where trans.FriendlyId == friendlyId &&
                                      trans.MobileFormat == false
                        select new DuplicateTransactionModel()
                        {
                            TransactionId = trans.ID,
                            TransactionTypeCode = trans.TransactionTypeCode,
                            IncomingId = trans.IncomingId,
                            OutgoingId = trans.OutgoingId,
                            IncomingRegistrationNumber = claimDetailIn.Claim.Participant.Number,
                            OutgoingRegistrationNumber = claimDetailOut != null ? claimDetailOut.Claim.Participant.Number : "",
                            IncomingPeriodShortName = claimDetailIn != null ? claimDetailIn.Claim.ClaimPeriod.ShortName : "",
                            OutgoingPeriodShortName = claimDetailOut != null ? claimDetailOut.Claim.ClaimPeriod.ShortName : "",
                            TransactionNumber = trans.FriendlyId.ToString(),
                            OutgoingVendorTypeInt = trans.OutgoingVendor != null ? trans.OutgoingVendor.VendorType : 0,
                            IncomingVendorTypeInt = trans.IncomingVendor != null ? trans.IncomingVendor.VendorType : 0
                        };
            return query.ToList();
        }
        #endregion

        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (!((c >= '0' && c <= '9') || c == '.'))
                    return false;
            }
            return true;
        }

        #region STC Premium Implementation
        public STCEventNumberValidationStatus EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate)
        {
            var dbContext = context as TransactionBoundedContext;
            var eventTmp = dbContext.SpecialItemRateList.FirstOrDefault(c => c.EventNumber == eventNumber);
            if (eventTmp == null)
            {
                return STCEventNumberValidationStatus.NotExisting;
            }
            else
            {
                if (eventTmp.TransactionID != null)
                {
                    return STCEventNumberValidationStatus.IsUsed;
                }
                else
                {
                    if (transactionStartDate.Date > eventTmp.EffectiveEndDate.Date || transactionStartDate.Date < eventTmp.EffectiveStartDate.Date)
                    {
                        return STCEventNumberValidationStatus.NotInDateRange;
                    }
                    else
                    {
                        return STCEventNumberValidationStatus.Pass;
                    }
                }
            }
        }

        public void AddTransactionIdIntoEvent(int transactionId, string eventNumber)
        {
            var dbContext = context as TransactionBoundedContext;
            var eventTmp = dbContext.SpecialItemRateList.FirstOrDefault(c => c.EventNumber == eventNumber);
            eventTmp.TransactionID = transactionId;
            dbContext.SaveChanges();
        }
        #endregion

    }
}