﻿using System.Collections.Generic;
using System.Linq;
using CL.Framework.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.TSFSteward;
using System;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Framework.DTO;
using System.Diagnostics;
using CL.TMS.Common.Converters;
using System.Data.Entity;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Enum;
using System.Data.Entity.SqlServer;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Threading.Tasks;
using System.Collections;

namespace CL.TMS.Repository.System
{
    public class TSFRemittanceRepository : BaseRepository<TSFRemittanceBoundedContext, TSFClaim, int>, ITSFRemittanceRepository
    {
        private EntityStore<TSFClaimDetail, int> tsfClaimDetailStore;
        private EntityStore<Rate, int> rateStore;

        public TSFRemittanceRepository()
        {
            tsfClaimDetailStore = new EntityStore<TSFClaimDetail, int>(context);
            rateStore = new EntityStore<Rate, int>(context);
        }
        public IReadOnlyList<TSFClaim> GetAllTSFClaims()
        {
            return entityStore.All.ToList();
        }

        public IQueryable<TSFClaim> GetAllTsfClaimsAsQueryable()
        {
            return entityStore.All;
        }

        public void AddTSFClaim(TSFClaim tsfClaim)
        {
            entityStore.Insert(tsfClaim);
        }

        public bool RemoveTSFClaim(int tsfClaimId)
        {
            using (var dbContext = context as TSFRemittanceBoundedContext)
            {
                var claim = dbContext.TSFClaims.Where(m => m.ID == tsfClaimId).FirstOrDefault();
                if (claim != null)
                {
                    if (claim.TSFClaimInternalPaymentAdjustments.Count > 0)
                    {
                        foreach (var paymentAdjustment in claim.TSFClaimInternalPaymentAdjustments.ToList())
                        {
                            if (paymentAdjustment.TSFClaimInternalAdjustmentNotes.Count > 0)
                            {
                                var adjustmentNotes = paymentAdjustment.TSFClaimInternalAdjustmentNotes.ToList();
                                dbContext.TSFClaimInternalAdjustmentNote.RemoveRange(adjustmentNotes);
                            }
                            dbContext.TSFClaimInternalPaymentAdjustment.Remove(paymentAdjustment);
                        }
                    }

                    var tsfRemittanceNotes = claim.TSFRemittanceNotes.ToList();
                    if (tsfRemittanceNotes.Count > 0)
                    {
                        dbContext.TSFRemittanceNotes.RemoveRange(tsfRemittanceNotes);
                    }

                    dbContext.TSFClaims.Remove(claim);
                    context.SaveChanges();
                    return true;
                }
            }
            return false;
        }
        public TSFClaim GetTSFRemittance(int periodID, int customerID)
        {
            return entityStore.All.Where(t => t.PeriodID == periodID & t.CustomerID == customerID).FirstOrDefault();
        }
        public IQueryable<Period> GetAllTSFClaimPeriods()
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            return dbContext.Periods.Where(p => p.PeriodType == 1);
        }
        public TSFClaimDetail GetTSFClaimDetailsByClaimID(int tsfClaimID)
        {
            ConditionCheck.Bounds(tsfClaimID, "TSFClaimID", int.MaxValue);

            return tsfClaimDetailStore.All.Where(c => c.TSFClaimID == tsfClaimID)
                    .OrderByDescending(c => c.ID)
                    .FirstOrDefault();
        }
        public TSFRemittanceBriefModel GetFirstSubmitRemittance()
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var query = from remittance in dbContext.TSFClaims
                        where (remittance.RecordState == ClaimStatus.Submitted.ToString() && (remittance.SubmissionDate != null))
                        select new TSFRemittanceBriefModel
                        {
                            ID = remittance.ID,
                            Company = remittance.Customer.BusinessName,
                            Submitted = remittance.SubmissionDate,
                            RegNo = remittance.Customer.RegistrationNumber,
                            Period = remittance.Period.ShortName,
                        };
            return query.OrderBy(a => a.Submitted).FirstOrDefault();
        }
        public decimal GetRemittanceRateByItemId(int itemID)
        {
            decimal remittanceRate = 0;
            var dbContext = context as TSFRemittanceBoundedContext;

            Rate rate = rateStore.All.Where(r => r.ItemID == itemID).FirstOrDefault();
            if (rate == null)
            {

                throw new NullReferenceException();
            }
            decimal itemRate = rate.ItemRate;
            return remittanceRate = itemRate;
        }

        public User GetByUserId(long? userId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            return dbContext.Users.Find(userId);
        }
        public PaginationDTO<TSFRemittanceBriefModel, long> LoadAllTSFRemittance(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            string sSeparator = string.Empty;
            var dbContext = context as TSFRemittanceBoundedContext;
            DateTime dtbefore = Convert.ToDateTime("2016-01-01"); //OTSTM2-188

            var query = from claim in dbContext.TSFClaims
                        join user in dbContext.Users on claim.AssignToUser equals user.ID into claim_user
                        join customer in dbContext.Customers on claim.CustomerID equals customer.ID into claim_customer //OTSTM2-188
                        from set1 in claim_user.DefaultIfEmpty()
                        from set2 in claim_customer.DefaultIfEmpty() //OTSTM2-188
                        select new TSFRemittanceBriefModel
                        {
                            ID = claim.ID,
                            RegNo = claim.Customer.RegistrationNumber,
                            Company = claim.Customer.BusinessName,
                            Period = claim.Period.ShortName,
                            PeriodDate = claim.Period.StartDate,
                            Status = claim.RecordState,
                            Submitted = claim.SubmissionDate,
                            AssignedToUserId = claim.AssignToUser,
                            AssignedTo = set1.FirstName + " " + set1.LastName,
                            //OTSTM2-188
                            ReportingFrequency = claim.Period.StartDate >= dtbefore ?
                                                    (set2.CustomerSettingHistoryList.Count > 0 ?
                                                        (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == claim.Period.StartDate.Year).OrderByDescending(c => c.ChangeDate).FirstOrDefault() != null ?
                                                            set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == claim.Period.StartDate.Year).OrderByDescending(c => c.ChangeDate).FirstOrDefault().NewValue :
                                                            (claim.Period.StartDate.Year < DateTime.UtcNow.Year ?
                                                                (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == SqlFunctions.DateAdd("year", 1, claim.Period.StartDate).Value.Year).FirstOrDefault() != null ?
                                                                    (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == SqlFunctions.DateAdd("year", 1, claim.Period.StartDate).Value.Year).FirstOrDefault().OldValue)
                                                                    : "12x"
                                                                )
                                                                : "12x"
                                                            )
                                                        )
                                                        : (claim.Period.StartDate.Year != DateTime.UtcNow.Year ? "12x" : ((bool)set2.RemitsPerYear ? "12x" : "2x"))
                                                    )
                                                : "12x",
                            DepositDate = claim.DepositDate,
                            TotalReceivable = claim.TotalTSFDue,// - (claim.Credit ?? 0) + (claim.AdjustmentTotal ?? 0),
                            ChequeAmount = claim.PaymentAmount ?? 0,
                            BalanceDue = claim.BalanceDue ?? 0
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.Submitted) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.Submitted) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.Submitted) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                            c.RegNo.ToString().Contains(searchText)
                            || c.Company.ToLower().Contains(searchText)
                            || c.Period.ToLower().Contains(searchText)
                            || c.Status.Contains(searchText)
                            || c.AssignedTo.Contains(searchText)
                            //OTSTM2-188
                            || c.ReportingFrequency.Contains(searchText)
                            || SqlFunctions.StringConvert(c.TotalReceivable, 18, 2).Contains(searchText) //OTSTM2-929
                            || SqlFunctions.StringConvert(c.ChequeAmount, 18, 2).Contains(searchText) //OTSTM2-929
                            || SqlFunctions.StringConvert(c.BalanceDue, 18, 2).Contains(searchText) //OTSTM2-929
                            );
                }
            }

            //OTSTM2-929
            var columnSearchTextRegNumber = columnSearchText["RegNumber"];
            var columnSearchTextCompany = columnSearchText["Company"];
            var columnSearchTextReportingFrequency = columnSearchText["ReportingFrequency"];
            var columnSearchTextPeriod = columnSearchText["Period"];
            var columnSearchTextSubmitted = columnSearchText["Submitted"];
            var columnSearchTextDepositDate = columnSearchText["DepositDate"];
            var columnSearchTextTotalReceivable = columnSearchText["TotalReceivable"];
            var columnSearchTextChequeAmount = columnSearchText["ChequeAmount"];
            var columnSearchTextBalanceDue = columnSearchText["BalanceDue"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAssignedTo = columnSearchText["AssignedTo"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextRegNumber))
            {
                query = query.Where(c => c.RegNo.ToString().Contains(columnSearchTextRegNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
            {
                query = query.Where(c => c.Company.ToString().Contains(columnSearchTextCompany));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextReportingFrequency))
            {
                query = query.Where(c => c.ReportingFrequency.ToString().Contains(columnSearchTextReportingFrequency));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                query = query.Where(c => c.Period.ToString().Contains(columnSearchTextPeriod));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextDepositDate))
            {
                query = query.Where(c => (c.DepositDate != null ? (SqlFunctions.DatePart("yyyy", c.DepositDate.Value) + "-" + DbFunctions.Right("00" + c.DepositDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.DepositDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextDepositDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextTotalReceivable))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.TotalReceivable, 18, 2).Contains(columnSearchTextTotalReceivable));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.ChequeAmount, 18, 2).Contains(columnSearchTextChequeAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextBalanceDue))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.BalanceDue, 18, 2).Contains(columnSearchTextBalanceDue));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextAssignedTo));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Submitted" : orderBy;
            orderBy = orderBy == "Period" ? "PeriodDate" : orderBy;

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            var result = query.ToList();

            //OTSTM2-929 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.Submitted = (i.Submitted != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.Submitted.Value, TimeZoneInfo.Local) : i.Submitted;
            });

            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                result = result.Where(c => (c.Submitted != null ? c.Submitted.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmitted)).ToList();
            }

            var totalRecords = result.Count();
            var returnval = result.Skip(pageIndex).Take(pageSize).ToList();

            return new PaginationDTO<TSFRemittanceBriefModel, long>
            {
                DTOCollection = returnval,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<TSFRemittanceBriefModel, long> LoadAllTSFRemittanceByCustomer(string RegistertionNo, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            string sSeparator = string.Empty;
            var dbContext = context as TSFRemittanceBoundedContext;


            var query = from claim in dbContext.TSFClaims.AsEnumerable()
                        join prd in dbContext.Periods on claim.PeriodID equals prd.ID

                        where (claim.RegistrationNumber == RegistertionNo)
                        select new TSFRemittanceBriefModel
                        {
                            ID = claim.ID,
                            Period = prd.ShortName,
                            PeriodDate = prd.StartDate,
                            Status = claim.RecordState,
                            Due = claim.TotalTSFDue, //()
                            BalanceDue = (claim.BalanceDue != null) ? Convert.ToDecimal(claim.BalanceDue) : 0,
                            Payments = claim.PaymentAmount,
                            Submitted = claim.SubmissionDate,
                            SubmittedBy = (claim.CreatedUser != null) ? claim.CreatedUser : "",
                            // Subtotal :  TireCount(TotalemittancePayable)+PaymentAdjust(AdjustmentTotal)-Credit
                            TotalReceivable =  claim.TotalRemittancePayable//+(claim.AdjustmentTotal??0)-(claim.Credit??0),
                        };
            switch (orderBy.ToLower())
            {
                case "period":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PeriodDate);
                    else
                        query = query.OrderByDescending(a => a.PeriodDate);
                    break;
                case "status":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Status);
                    else
                        query = query.OrderByDescending(a => a.Status);
                    break;
                case "due":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Due);
                    else
                        query = query.OrderByDescending(a => a.Due);
                    break;
                case "balancedue":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.BalanceDue);
                    else
                        query = query.OrderByDescending(a => a.BalanceDue);
                    break;

                case "payments":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Payments);
                    else
                        query = query.OrderByDescending(a => a.Payments);
                    break;
                case "totalreceivable":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.TotalReceivable);
                    else
                        query = query.OrderByDescending(a => a.TotalReceivable);
                    break;
                case "submitted":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Submitted);
                    else
                        query = query.OrderByDescending(a => a.Submitted);
                    break;
                case "submittedby":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.SubmittedBy);
                    else
                        query = query.OrderByDescending(a => a.SubmittedBy);
                    break;

                default:
                    query = query.OrderByDescending(a => a.Submitted);
                    break;
            }
            List<TSFRemittanceBriefModel> returnValue = new List<TSFRemittanceBriefModel>();

            foreach (var app in query)
            {
                returnValue.Add(app);
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();
                //DateTime dateValue;
                //if (DateTime.TryParse(searchText, out dateValue))
                //{
                //    returnValue = returnValue.AsQueryable().Where(c => ((c.Submitted != null) ? Convert.ToDateTime(c.Submitted).ToString("yyyy-MM-dd") : "").ToLower().Contains(searchText)
                //        || c.Period.ToLower().Contains(searchText)
                //        || DateTimeConverter.TruncateTime(c.Submitted) == DateTimeConverter.TruncateTime(dateValue)
                //        || ((c.SubmittedBy != null) ? c.SubmittedBy : "").ToString().ToLower().Contains(searchText)).ToList<TSFRemittanceBriefModel>();
                //}
                //else
                //{
                returnValue = returnValue.AsEnumerable().Where(c => ((c.Submitted != null) ? c.Submitted.Value.ToString("yyyy-MM-dd") : "").ToLower().Contains(searchText)
                    || c.Period.ToLower().Contains(searchText) || c.Status.ToLower().ToString().Contains(searchText)
                    || c.Due.ToString("0.00").Contains(searchText) || c.BalanceDue.ToString("0.00").Contains(searchText) //OTSTM2-984
                    || ((c.SubmittedBy != null) ? c.SubmittedBy : "").ToString().ToLower().Contains(searchText)
                    || (c.Payments != null ? c.Payments.Value.ToString("0.00") : "0.00").ToLower().Contains(searchText) //OTSTM2-984
                    || c.TotalReceivable.ToString("0.00").Contains(searchText))
                    .ToList<TSFRemittanceBriefModel>();
                //}
            }

            //OTSTM2-984 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            returnValue.ForEach(i =>
            {
                i.Submitted = (i.Submitted != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.Submitted.Value, TimeZoneInfo.Local) : i.Submitted;
            });

            //OTSTM2-984
            var columnSearchTextPeriod = columnSearchText["Period"].ToLower();
            var columnSearchTextStatus = columnSearchText["Status"].ToLower();
            var columnSearchTextDue = columnSearchText["Due"].ToLower();
            var columnSearchTotalReceivable = columnSearchText["TotalReceivable"].ToLower();
            var columnSearchTextPayment = columnSearchText["Payment"].ToLower();
            var columnSearchTextBalanceDue = columnSearchText["BalanceDue"].ToLower();
            var columnSearchTextSubmitted = columnSearchText["Submitted"].ToLower();
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"].ToLower();

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                returnValue = returnValue.Where(c => c.Period.ToString().ToLower().Contains(columnSearchTextPeriod)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                returnValue = returnValue.Where(c => c.Status.ToString().ToLower().Contains(columnSearchTextStatus)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextDue))
            {
                returnValue = returnValue.Where(c => c.Due.ToString("0.00").ToLower().Contains(columnSearchTextDue)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTotalReceivable))
            {
                returnValue = returnValue.Where(c => c.TotalReceivable.ToString("0.00").ToLower().Contains(columnSearchTotalReceivable)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPayment))
            {
                returnValue = returnValue.Where(c => (c.Payments != null ? c.Payments.Value.ToString("0.00") : "0.00").ToLower().Contains(columnSearchTextPayment)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextBalanceDue))
            {
                returnValue = returnValue.Where(c => c.BalanceDue.ToString("0.00").ToLower().Contains(columnSearchTextBalanceDue)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                returnValue = returnValue.Where(c => ((c.Submitted != null) ? c.Submitted.Value.ToString("yyyy-MM-dd") : string.Empty).ToLower().Contains(columnSearchTextSubmitted)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                returnValue = returnValue.Where(c => ((c.SubmittedBy != null) ? c.SubmittedBy.ToString() : string.Empty).ToLower().Contains(columnSearchTextSubmittedBy)).ToList();
            }


            var totalRecords = returnValue.Count();

            return new PaginationDTO<TSFRemittanceBriefModel, long>
            {
                DTOCollection = returnValue,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var query = from r in dbContext.Rates
                        join i in dbContext.Items on r.ItemID equals i.ID into rateItem
                        from i in rateItem.DefaultIfEmpty()

                        where (r.EffectiveStartDate <= reportingPeriod && r.EffectiveEndDate >= reportingPeriod) && r.ClaimType == 1 && r.ItemID != null // for penalty, doesn't have ItemId, join will fail
                        orderby r.ItemID
                        select new RateListModel
                        {
                            ID = i.ID,
                            Name = i.Name,
                            Description = i.Description,
                            ItemRate = r.ItemRate
                        };
            return query.ToList();
        }

        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod, int ItemID)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var query = from r in dbContext.Rates
                        join i in dbContext.Items on r.ItemID equals i.ID into rateItem
                        from i in rateItem.DefaultIfEmpty()

                        where (r.EffectiveStartDate <= reportingPeriod && r.EffectiveEndDate >= reportingPeriod) && r.ClaimType == 1 && r.ItemID == ItemID
                        orderby r.ItemID
                        select new RateListModel
                        {
                            ID = r.ID,
                            ItemRate = r.ItemRate
                        };
            return query.ToList();
        }

        public RateListModel GetRate(DateTime reportingPeriod, int ItemID)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var rate = dbContext.Rates.Where(r => (r.EffectiveStartDate <= reportingPeriod && r.EffectiveEndDate >= reportingPeriod) && r.ClaimType == 1 && r.ItemID == ItemID).FirstOrDefault();
            var result = new RateListModel { ID = rate.ID, ItemRate = rate.ItemRate };
            return result;
        }

        public TSFClaim GetTSFClaimById(int claimID)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            return dbContext.TSFClaims.Where(m => m.ID == claimID).FirstOrDefault();
        }

        public IList GetTSFClaimPeriodForBreadCrumb(int customerId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var query = from claim in dbContext.TSFClaims.AsNoTracking()
                        join prd in dbContext.Periods on claim.PeriodID equals prd.ID
                        where claim.CustomerID == customerId
                        select new { shortName = prd.ShortName, claimId = claim.ID };

            return query.ToList();
        }
        public IEnumerable GetTSFClaimPeriods(int periodTypeID)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var query = from claim in dbContext.TSFClaims.AsNoTracking()
                        join prd in dbContext.Periods on claim.PeriodID equals prd.ID
                        where prd.PeriodType==periodTypeID
                        select new { shortName = prd.ShortName, periodID = prd.ID };
            return query.Distinct().OrderByDescending(x=>x.periodID);
        }
        public VendorReference GetVendorReference(int tsfClaimId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var query = from claim in dbContext.TSFClaims
                        where claim.ID == tsfClaimId
                        select new VendorReference
                        {
                            VendorId = claim.Customer.ID,
                            RegistrationNumber = claim.Customer.RegistrationNumber,
                            BusinessName = claim.Customer.BusinessName,
                            VendorType = 1,
                            IsActive = claim.Customer.IsActive,
                            Status = claim.Customer.RegistrantStatus
                        };
            return query.FirstOrDefault(); ;
        }

        public TSFRemittanceModel GetTSFRemittanceByCustomerId(Customer customer)
        {
            //var dbContext = context as TSFRemittanceBoundedContext;

            var mod = new TSFRemittanceModel
            {
                Address = customer.CustomerAddresses.FirstOrDefault(),
                //PrimaryContact = customer.CustomerAddresses.FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary),
                PrimaryContact = RepositoryHelper.FindPrimaryContact(customer.CustomerAddresses.ToList()),
                RegistrationNumber = customer.RegistrationNumber,
                BusinessName = customer.BusinessName,
                ApplicationID = customer.ApplicationID != null ? customer.ApplicationID.Value : 0,
                SRemitsPerYears = customer.RemitsPerYear,
                RemitsPerYearSwitchDate = customer.RemitsPerYearSwitchDate.HasValue ? customer.RemitsPerYearSwitchDate : null,
                //SAudits = customer.Audit, //OTSTM2-1110 comment out
                CustomerID = customer.ID,
                ActivityApprovedDate = customer.ActiveStateChangeDate,
                IsActive=customer.IsActive,                          
            };
            return mod;
        }

        public bool HasData(int TSFClaimId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var tsfClaim = dbContext.TSFClaims
               .Include(a => a.TSFClaimDetails)
               .Include(t => t.TSFClaimInternalPaymentAdjustments)
               .FirstOrDefault(a => a.ID == TSFClaimId);
            if (TSFClaimId == 0)
            {
                return false;
            }
            return tsfClaim.TSFClaimDetails.Any() || tsfClaim.TSFClaimInternalPaymentAdjustments.Any(tip => tip.Status != "Voided") || tsfClaim.Attachments.Any();
        }

        public TSFRemittanceModel PopulateTSFModelByTSFClaimId(int TSFClaimId, TSFRemittanceModel mod)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var items = new List<TSFRemittanceItemModel>();

            var cl = dbContext.TSFClaims
                .Include(a => a.TSFClaimDetails)
                .Include(b => b.TSFClaimDetails.Select(a => a.Item))
                .Include(p => p.Period)
                .Include(c => c.Customer) //OTSTM2-21
                .Include(t => t.TSFClaimInternalPaymentAdjustments) //OTSTM2-485
                .FirstOrDefault(a => a.ID == TSFClaimId);
            if (cl != null)
            {
                mod.ID = cl.ID;
                mod.Submitted = cl.SubmissionDate;
                mod.PeriodDate = cl.Period.StartDate;
                mod.PayPeriod = cl.Period.ShortName;
                mod.PaymentType = cl.PaymentType;
                mod.PayPeriodDesc = cl.Period.PeriodDesc;
                mod.PayDepositDate = cl.DepositDate;
                mod.PayReceiptDate = cl.ReceiptDate;
                mod.RecordState = cl.RecordState;
                mod.TotalRemittancePayable = cl.TotalRemittancePayable;
                mod.TotalTSFDue = cl.TotalTSFDue;
                mod.SubmittedDate = cl.SubmissionDate;
                mod.BalanceDue = cl.BalanceDue;
                mod.ChequeReferenceNumber = cl.ChequeReferenceNumber;
                mod.PaymentAmount = cl.PaymentAmount;
                mod.StatusName = cl.RecordState;
                mod.IsPenaltyOverride = (cl.IsPenaltyOverride == 1);
                mod.IsTaxApplicable = (cl.IsTaxApplicable == 1);
                mod.Penalties = (cl.IsPenaltyOverride == 1) ? 0 : cl.Penalties;//if PenaltyOverride then penalty = 0;
                mod.ApplicableTaxesHst = (cl.IsTaxApplicable == 1) ? cl.ApplicableTaxesHst : 0;
                mod.ChequeDue = cl.DepositDate;
                mod.Assigned = cl.AssignedDate;
                mod.ApprovedDate = cl.ApprovedDate;
                mod.StartedDate = cl.StartedDate;
                //mod.IsActive = (cl.IsActive != null) ? cl.IsActive.Value : false;
                mod.TotalTSFDue = cl.TotalTSFDue;
                mod.SupportingDocOption = cl.SupportingDocOption;
                //mod.PeriodID = cl.PeriodID; //Add for 2X
                mod.Credit = cl.Credit;
                mod.IsCreatedByStaff = cl.IsCreatedByStaff; //OTSTM2-56
                mod.CurrencyType = cl.CurrencyType;
                //mod.InternalPaymentAdjustment = cl.TSFClaimInternalPaymentAdjustments.Sum(t => t.AdjustmentAmount); //OTSTM2-485
                mod.InternalPaymentAdjustment = cl.AdjustmentTotal; //OTSTM2-485
                mod.InternalPaymentAdjustmentCount = cl.TSFClaimInternalPaymentAdjustments.Where(t => t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count(); //OTSTM2-485

                //OTSTM2-21 2x/12x displaying for new remittance
                DateTime dtbefore = Convert.ToDateTime("2016-01-01");
                if (cl.Period.StartDate >= dtbefore)
                {
                    if (cl.Customer.CustomerSettingHistoryList.Count > 0)
                    {
                        var setting = cl.Customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == cl.Period.StartDate.Year).OrderByDescending(c => c.ChangeDate).FirstOrDefault();
                        if (setting != null)
                        {
                            mod.SRemitsPerYears = setting.NewValue == "2x" ? false : true;
                        }
                        else
                        {
                            if (cl.Period.StartDate.Year < DateTime.UtcNow.Year) //for any earlier year
                            {
                                var oldSetting = cl.Customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == cl.Period.StartDate.AddYears(1).Year).FirstOrDefault();
                                if (oldSetting != null)
                                {
                                    mod.SRemitsPerYears = oldSetting.OldValue == "2x" ? false : true;
                                }
                                //else  //for 2015 and earlier
                                //{
                                //    mod.SRemitsPerYears = true;
                                //}
                            }
                            else  //for any future year
                            {
                                mod.SRemitsPerYears = true;
                            }

                        }
                    }
                    else  //no data in new table, if current year, no change(keep the value of SRemitsPerYears getting from GetTSFRemittanceByCustomerId()), the year other than current year, 12x
                    {
                        if (cl.Period.StartDate.Year != DateTime.UtcNow.Year)
                        {
                            mod.SRemitsPerYears = true;
                        }

                    }
                }
                else
                {
                    mod.SRemitsPerYears = true;
                }

                foreach (var item in cl.TSFClaimDetails)
                {
                    items.Add(new TSFRemittanceItemModel
                    {
                        DetailID = item.ID,
                        Quantity = item.TireSupplied,
                        NegativeAdjustment = item.NegativeAdjustment,
                        AdjustmentReasonType = item.AdjustmentReasonType,
                        OtherDesc = item.OtherDesc,
                        Rate = item.TSFRate,
                        Category = item.Item.Name,
                        CategoryId = item.ItemID,
                        Total = item.TSFDue,
                        Net = item.CountSupplied,
                        NegativeAdjDate = item.NegativeAdjDate ?? mod.PeriodDate,
                        CreditItemID = item.CreditItemID,
                        CreditNegativeAdj = item.CreditNegativeAdj,
                        CreditTSFRate = item.CreditTSFRate,
                        CreditTSFDue = item.CreditTSFDue,
                        TSFRateDate = item.TSFRateDate ?? mod.PeriodDate, //add for 2x
                        TSFRateDateShortName = item.TSFRateDate != null ? item.TSFRateDate.Value.ToString("MMM yyyy") : mod.PeriodDate.Value.ToString("MMM yyyy"), //add for 2x
                    });
                }
                mod.LineItems = items;

                var Rates = dbContext.Rates;
                Rate penaltyRate = Rates.Where(i => i.ClaimType == (int)ClaimType.Steward && i.PaymentType == 6 && i.EffectiveStartDate <= mod.PeriodDate && i.EffectiveEndDate > mod.PeriodDate).FirstOrDefault();
                mod.PenaltyRate = (null != penaltyRate) ? penaltyRate.ItemRate : (decimal)10.0;
            }

            return mod;
        }

        //------------------------------------------------------------------------------------

        public int AddTSFClaim(TSFRemittanceModel model, ref bool isCreated)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            DateTime dt = Convert.ToDateTime(model.PeriodDate).Date;
            var payPeriod = dbContext.Periods.FirstOrDefault(p => p.StartDate <= dt && p.EndDate >= dt && p.PeriodType == 1);
            if (payPeriod != null)
            {
                model.PeriodID = payPeriod.ID;
            }
            if (GetCustomerClaim(model.PeriodID, model.CustomerID) > 0)
            {
                var tsfclaim = dbContext.TSFClaims.FirstOrDefault(p => p.PeriodID == model.PeriodID && p.CustomerID == model.CustomerID);
                return tsfclaim.ID;
            }

            if (model.ID > 0)
            {

                TSFClaim objTSFClaim = dbContext.TSFClaims.Include(c => c.TSFClaimDetails).Where(x => x.ID == model.ID).FirstOrDefault();

                if (objTSFClaim != null)
                {
                    // model.TotalTSFDue = objTSFClaim.TSFClaimDetails.Sum(a => a.TSFDue);
                    objTSFClaim.TotalTSFDue = model.TotalTSFDue.Value;
                    objTSFClaim.Penalties = model.Penalties.Value;
                    objTSFClaim.TotalRemittancePayable = model.TotalRemittancePayable.Value;
                    if (model.Istaff == true)
                    {
                        // objTSFClaim.ReceiptDate = model.PayReceiptDate;
                        objTSFClaim.SubmissionDate = model.SubmittedDate;
                        //objTSFClaim.DepositDate = model.PayDepositDate.Value;

                    }
                    objTSFClaim.ChequeReferenceNumber = model.ChequeReferenceNumber;
                    objTSFClaim.PaymentAmount = model.PaymentAmount;
                    objTSFClaim.RecordState = model.StatusName;
                    objTSFClaim.CustomerID = model.CustomerID;
                    objTSFClaim.RegistrationNumber = model.RegistrationNumber;
                    objTSFClaim.PeriodID = model.PeriodID;
                    objTSFClaim.PreparedBy = model.PreparedBy;
                    objTSFClaim.CurrencyType = model.CurrencyType;
                    dbContext.SaveChanges();
                }
                return objTSFClaim.ID;
            }
            else //create new TSFClaim
            {
                TSFClaim obj = new TSFClaim();

                obj.TotalTSFDue = 0;
                obj.Penalties = 0;
                obj.TotalRemittancePayable = 0;
                obj.CreatedDate = DateTime.UtcNow;
                obj.ModifiedDate = obj.CreatedDate;
                obj.ChequeReferenceNumber = string.Empty;
                obj.PaymentAmount = 0;
                obj.RecordState = (ClaimStatus.UnderReview == model.Status) ? "Under Review" : model.Status.ToString();
                obj.CustomerID = model.CustomerID;
                obj.RegistrationNumber = model.RegistrationNumber;
                obj.PeriodID = model.PeriodID;
                obj.PreparedBy = string.Empty;
                obj.IsCreatedByStaff = model.IsCreatedByStaff; ////OTSTM2-56
                if (model.Istaff == true)
                {
                    obj.SubmissionDate = obj.CreatedDate;
                    obj.CreatedUser = model.SubmittedBy;
                    obj.AssignToUser = model.SubmittedByUserId;//if TSFClaim is created by a staff user, set status == "Under Review", also assign to this user. -OTSTM2-182
                    obj.AssignedDate = obj.CreatedDate;
                    obj.StartedDate = obj.CreatedDate;

                    isCreated = true; //OTSTM2-551
                }
                obj.CurrencyType = CL.TMS.Common.Enum.CurrencyType.CAD.ToString();
                obj.IsTaxApplicable = 1;
                dbContext.TSFClaims.Add(obj);
                dbContext.SaveChanges();
                return obj.ID;
            }
        }

        private int GetCustomerClaim(int PeriodID, int CustomerID)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var Claim = dbContext.TSFClaims.FirstOrDefault(p => p.PeriodID == PeriodID && p.CustomerID == CustomerID);
            if (Claim != null)
            {
                return Claim.ID;
            }
            else
            {
                return 0;
            }
        }
        public void AddRemittanceDetail(TSFRemittanceItemModel model)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            if (model.DetailID > 0) // update existing Remittance Details
            {
                TSFClaimDetail objTSFClaimDetail = dbContext.TSFClaimDetails.Where(x => x.ID == model.DetailID).FirstOrDefault();

                if (objTSFClaimDetail != null)
                {
                    if (model.CategoryId > 0)
                        objTSFClaimDetail.ItemID = model.CategoryId;
                    objTSFClaimDetail.TireSupplied = model.Quantity;
                    objTSFClaimDetail.NegativeAdjustment = model.NegativeAdjustment;
                    objTSFClaimDetail.AdjustmentReasonType = model.AdjustmentReasonType;
                    objTSFClaimDetail.OtherDesc = model.OtherDesc;
                    objTSFClaimDetail.TSFRate = model.Rate;
                    objTSFClaimDetail.CountSupplied = model.Net;
                    objTSFClaimDetail.TSFDue = model.Total;
                    objTSFClaimDetail.ModifiedUser = model.ModifiedBy;
                    objTSFClaimDetail.ModifiedDate = DateTime.UtcNow;
                    if (model.TSFRateDate != null)
                        objTSFClaimDetail.TSFRateDate = model.TSFRateDate;

                    objTSFClaimDetail.NegativeAdjDate = model.NegativeAdjDate;
                    objTSFClaimDetail.CreditItemID = model.CreditItemID;
                    objTSFClaimDetail.CreditTSFRate = model.CreditTSFRate;
                    objTSFClaimDetail.CreditTSFDue = model.CreditTSFDue;
                    objTSFClaimDetail.CreditNegativeAdj = model.CreditNegativeAdj;

                    dbContext.SaveChanges();
                }
            }
            else //Create New Remittance Details
            {
                dbContext.TSFClaimDetails.Add(new TSFClaimDetail
                {
                    ItemID = model.CategoryId,
                    TireSupplied = model.Quantity,
                    NegativeAdjustment = model.NegativeAdjustment,
                    AdjustmentReasonType = model.AdjustmentReasonType,
                    OtherDesc = model.OtherDesc,
                    TSFRate = model.Rate,
                    CountSupplied = model.Net,
                    TSFDue = model.Total,
                    TSFClaimID = model.ClaimID,
                    CreatedDate = DateTime.UtcNow,
                    ModifiedDate = DateTime.UtcNow,
                    CreatedUser = model.CreatedBy,
                    ModifiedUser = "",
                    TSFRateDate = model.TSFRateDate,
                    
                    NegativeAdjDate = model.NegativeAdjDate,
                    CreditItemID = model.CreditItemID,
                    CreditTSFRate = model.CreditTSFRate,
                    CreditTSFDue = model.CreditTSFDue,
                    CreditNegativeAdj = model.CreditNegativeAdj,
                });
                dbContext.SaveChanges();
            }
            UpdateClaimCalcs(model.ClaimID);
        }

        public void UpdatePenaltiesManually(int claimID, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            if (claimID > 0) // update existing Remittance Details
            {
                TSFClaim objTSFClaim = dbContext.TSFClaims.Where(x => x.ID == claimID).FirstOrDefault();

                if (objTSFClaim != null)
                {
                    objTSFClaim.ModifiedUser = modifiedBy;
                    objTSFClaim.ModifiedDate = DateTime.UtcNow;
                    objTSFClaim.IsPenaltyOverride = (true == IsPenaltyOverride) ? (byte)1 : (byte)0;
                    if (objTSFClaim.IsPenaltyOverride == 1)
                    {
                        objTSFClaim.PenaltiesManually = 0;
                    }
                    else
                    {
                        objTSFClaim.PenaltiesManually = objTSFClaim.Penalties;
                    }

                    dbContext.SaveChanges();
                }
            }
            UpdateClaimCalcs(claimID);
        }
        public void UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            if (tsfClaimId > 0)
            {
                TSFClaim claim = dbContext.TSFClaims.Where(x => x.ID == tsfClaimId).FirstOrDefault();

                if (claim != null)
                {
                    claim.ModifiedUser = modifiedBy;
                    claim.ModifiedDate = DateTime.UtcNow;
                    claim.IsTaxApplicable = IsTaxApplicable ? (byte)1 : (byte)0;
                    dbContext.SaveChanges();
                }

                UpdateClaimCalcs(tsfClaimId);
            }
        }
        public void UpdateSupportingDocumentValue(int id, string docOptionValue, string modifiedBy)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            if (id > 0) // update existing Remittance Details
            {
                TSFClaim objTSFClaim = dbContext.TSFClaims.Where(x => x.ID == id).FirstOrDefault();

                if (objTSFClaim != null)
                {
                    objTSFClaim.SupportingDocOption = docOptionValue;
                    objTSFClaim.ModifiedUser = modifiedBy;
                    objTSFClaim.ModifiedDate = DateTime.UtcNow;
                    dbContext.SaveChanges();
                }
            }
        }
        /***
         * Note: this update claim calcs using in (include HST calculation) :
         * #875   AddRemittanceDetail(TSFRemittanceItemModel model)
         * #903   UpdatePenaltiesManually(int claimID, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false)
         * #921   UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true)
         * #1091  DeleteRemittanceDetail(int claimId, int detailId)
         * #1211  SetRemittanceStatus(int ClaimId, string status, string updatedBy, long assignToUser, bool isAssigningToMe)
         * #1874  AddInternalPaymentAdjustment(TSFClaimInternalPaymentAdjustment tsfclaimInternalPaymentAdjust)
         * #1891  RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
         * #1909  UpdateInternalPaymentAdjust(int claimInternalAdjustId, TSFClaimAdjustmentModalResult modalResult, long editUserId)
         **/
        private void UpdateClaimCalcs(int claimId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var claim = dbContext.TSFClaims.Find(claimId);
            //tire counts
            decimal TotalRemittancePayable = claim.TSFClaimDetails.Sum(a => a.TSFDue);
            // payment adjustments
            claim.AdjustmentTotal = claim.TSFClaimInternalPaymentAdjustments != null ? claim.TSFClaimInternalPaymentAdjustments.Where(t => t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount) : 0; //OTSTM2-485           
            // credit
            claim.Credit = claim.TSFClaimDetails.Where(a => a.CreditNegativeAdj > 0).Sum(a => a.CreditTSFDue);
            //subtotal
            var subtotal = TotalRemittancePayable + claim.AdjustmentTotal - claim.Credit;
            // calculate HST
            //OTSTM2-1224 enable HST calculation for steward 
            // OTSTM2-1315 tax include adjustment.
            claim.ApplicableTaxesHst = 0;
            if (Convert.ToBoolean(claim.IsTaxApplicable))
            {
                var temp = Math.Round((decimal)subtotal * (decimal)0.13, 2, MidpointRounding.AwayFromZero);
                if (temp > 0) claim.ApplicableTaxesHst = temp;
            }
            // penalties
            if (claim.IsPenaltyOverride == (byte)1)
            {
                claim.Penalties = 0;
            }
            else
            {
                claim.Penalties = GetEffectivePenalty(claim, TotalRemittancePayable);
            }  
            claim.PenaltiesManually = claim.Penalties;   
            claim.TotalRemittancePayable = TotalRemittancePayable;
            claim.TotalTSFDue = (decimal)subtotal+ claim.ApplicableTaxesHst + claim.Penalties ;

            claim.BalanceDue = Math.Round(claim.TotalTSFDue-(claim.PaymentAmount??0), 2, MidpointRounding.AwayFromZero);  //OTSTM2-485

            dbContext.SaveChanges();
        }

        private decimal GetEffectivePenalty(TSFClaim claim, decimal TotalRemittancePayable)
        {
            decimal penalty = 0, effectivePenalty = 0;
            //effectivePenaltyRecord based on claim period
            var effectivePenaltyRecord = rateStore.All.FirstOrDefault(r => r.PaymentType == 6 && r.ClaimType == 1 && r.EffectiveStartDate <= claim.Period.StartDate && r.EffectiveEndDate >= claim.Period.StartDate);
            if (effectivePenaltyRecord != null)
                penalty = effectivePenaltyRecord.ItemRate;

            //OTSTM2-56
            if (claim.ReceiptDate != null)
            {
                if (claim.Period.StartDate.AddMonths(2).AddDays(3).Date < claim.ReceiptDate.Value.Date && claim.Period.StartDate.Year != 2009)
                {
                    //OTSTM2-954
                    if (claim.Period.StartDate.Date >= new DateTime(2016, 7, 1))
                    {
                        if (TotalRemittancePayable > claim.Credit)
                            effectivePenalty = Math.Round(Convert.ToDecimal((TotalRemittancePayable - claim.Credit) * penalty / 100), 2, MidpointRounding.AwayFromZero);
                        else
                            effectivePenalty = 0;
                    }
                    else
                        effectivePenalty = Math.Round(Convert.ToDecimal(TotalRemittancePayable * penalty / 100), 2, MidpointRounding.AwayFromZero);

                }
                else
                {
                    effectivePenalty = 0;
                }
            }
            else if (claim.SubmissionDate != null)
            {
                if (claim.Period.StartDate.AddMonths(2).Date <= claim.SubmissionDate.Value.Date && claim.Period.StartDate.Year != 2009)
                {

                    if (claim.Period.StartDate.Date >= new DateTime(2016, 7, 1))
                    {
                        if (TotalRemittancePayable > claim.Credit)
                            effectivePenalty = Math.Round(Convert.ToDecimal((TotalRemittancePayable - claim.Credit) * penalty / 100), 2, MidpointRounding.AwayFromZero);
                        else
                            effectivePenalty = 0;
                    }
                    else
                        effectivePenalty = Math.Round(Convert.ToDecimal(TotalRemittancePayable * penalty / 100), 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    effectivePenalty = 0;
                }
            }
            else
            {
                if (claim.Period.StartDate.AddMonths(2).AddDays(3).Date < DateTime.UtcNow.Date && claim.Period.StartDate.Year != 2009)
                {

                    if (claim.Period.StartDate.Date >= new DateTime(2016, 7, 1))
                    {
                        if (TotalRemittancePayable > claim.Credit)
                            effectivePenalty = Math.Round(Convert.ToDecimal((TotalRemittancePayable - claim.Credit) * penalty / 100), 2, MidpointRounding.AwayFromZero);
                        else
                            effectivePenalty = 0;
                    }
                    else
                        effectivePenalty = Math.Round(Convert.ToDecimal(TotalRemittancePayable * penalty / 100), 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    effectivePenalty = 0;
                }
            }

            return effectivePenalty;
        }

        public void UpdateTsfClaim(TSFClaim tsfClaim)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var claim = dbContext.TSFClaims.FirstOrDefault(t => t.ID == tsfClaim.ID);

            //TODO update all fields
            if (claim != null)
            {
                claim.InBatch = tsfClaim.InBatch;
                claim.PaymentType = tsfClaim.PaymentType;
                claim.ModifiedUser = tsfClaim.ModifiedUser;
                claim.ModifiedDate = tsfClaim.ModifiedDate;
                claim.RecordState = tsfClaim.RecordState;
            }
            dbContext.SaveChanges();
        }


        public void DeleteRemittanceDetail(int claimId, int detailId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            TSFClaimDetail objTSFClaimDetail = dbContext.TSFClaimDetails.Find(detailId);
            if (objTSFClaimDetail != null)
            {
                if (objTSFClaimDetail.TSFClaimID == claimId)
                {
                    dbContext.TSFClaimDetails.Remove(objTSFClaimDetail);
                    dbContext.SaveChanges();
                }
                UpdateClaimCalcs(claimId);
            }
        }

        public TSFRemittanceItemModel GetRemittanceDetail(int claimId, int detailId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            TSFClaimDetail objTSFClaimDetail = dbContext.TSFClaimDetails.Find(detailId);
            if (objTSFClaimDetail != null && objTSFClaimDetail.TSFClaimID == claimId)
            {
                return
                  new TSFRemittanceItemModel
                  {
                      ClaimID = claimId,
                      DetailID = objTSFClaimDetail.ID,
                      Quantity = objTSFClaimDetail.TireSupplied,
                      NegativeAdjustment = objTSFClaimDetail.NegativeAdjustment,
                      AdjustmentReasonType = objTSFClaimDetail.AdjustmentReasonType,
                      OtherDesc = objTSFClaimDetail.OtherDesc,
                      Rate = objTSFClaimDetail.TSFRate,
                      Category = objTSFClaimDetail.Item.Name,
                      CategoryId = objTSFClaimDetail.ItemID,
                      Total = objTSFClaimDetail.TSFDue,
                      Net = objTSFClaimDetail.CountSupplied,
                      TSFRateDate = objTSFClaimDetail.TSFRateDate ?? null,  //add for 2x
                      TSFRateDateShortName = objTSFClaimDetail.TSFRateDate != null ? objTSFClaimDetail.TSFRateDate.Value.ToString("MMM yyyy") : null, //add for 2x

                      NegativeAdjDate = objTSFClaimDetail.NegativeAdjDate,
                      CreditItemID = objTSFClaimDetail.CreditItemID,
                      CreditTSFRate = objTSFClaimDetail.CreditTSFRate,
                      CreditTSFDue = objTSFClaimDetail.CreditTSFDue,
                      CreditNegativeAdj = objTSFClaimDetail.CreditNegativeAdj,
                  };
            }
            return null;
        }

        /// <summary>
        /// Sets the remittance status. used by ChangeStatus controller
        /// </summary>
        public StewardActivityHandlingModel SetRemittanceStatus(int ClaimId, string status, string updatedBy, long assignToUser, bool isAssigningToMe)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var claim = dbContext.TSFClaims.Find(ClaimId);
            claim.RecordState = status.ToString();
            var activityModel = new StewardActivityHandlingModel()
            {
                IsApproved = false,
                IsBackToParticipant = false,
                IsAssigned = false,
                IsSubmitted = false,
                IsUnassigned = false,
                IsAssignToMe = false,
                IsReassigned = false
            }; //OTSTM2-551
            if (status == ClaimStatus.Submitted.ToString())//"Submitted"
            {
                //claim.IsActive = true;
                claim.CreatedUser = updatedBy.ToString();
                claim.SubmissionDate = DateTime.UtcNow;
                claim.AssignedDate = null;
                if (assignToUser == 0) //OTSTM2-551
                {
                    activityModel.IsSubmitted = true;
                }
            }

            if ((status == ClaimStatus.UnderReview.ToString()) || (status == EnumHelper.GetEnumDescription(ClaimStatus.UnderReview))) //"UnderReview" || "Under Review"
            {
                claim.AssignedDate = DateTime.UtcNow;
                if (claim.StartedDate == null)
                {
                    claim.StartedDate = claim.AssignedDate;
                }

                if (claim.AssignToUser == null) //OTSTM2-551
                {
                    if (isAssigningToMe)
                    {
                        activityModel.IsAssignToMe = true;
                    }
                    else
                    {
                        activityModel.IsAssigned = true;
                    }
                }
                else
                {
                    activityModel.IsReassigned = true;
                }
            }

            if (status == ClaimStatus.Approved.ToString())//"Approved"
            {
                claim.CreatedDate = DateTime.UtcNow;
                claim.ApprovedDate = claim.CreatedDate;
                activityModel.IsApproved = true; //OTSTM2-551
            }
            else
            {
                claim.ModifiedUser = updatedBy.ToString();
            }

            if (-1 == assignToUser) //un-assign
            {
                claim.AssignToUser = null;
                activityModel.IsUnassigned = true; //OTSTM2-551
            }
            else if (0 != assignToUser)
            {
                claim.AssignToUser = assignToUser;
            }

            if (claim.RecordState == ClaimStatus.Open.ToString()) //OTSTM2-551
            {
                claim.AssignToUser = null;
                activityModel.IsBackToParticipant = true;
            }
            dbContext.SaveChanges();
            if (!string.IsNullOrEmpty(status)) //OTSTM2-56
                UpdateClaimCalcs(ClaimId);

            return activityModel;
        }

        /// <summary>
        /// Sets the remittance status. used by ApproveRemittance controller
        /// </summary>
        public decimal? SetRemittanceStatus(int ClaimId, string status, string updatedBy, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignToUser)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var claim = dbContext.TSFClaims.Find(ClaimId);
            if (!string.IsNullOrEmpty(status))
            {

                claim.RecordState = status.ToString();
                if (status == ClaimStatus.Submitted.ToString())//"Submitted"
                {
                    //claim.IsActive = true;
                    claim.CreatedUser = updatedBy.ToString();
                    claim.SubmissionDate = DateTime.UtcNow;
                    claim.AssignedDate = null;
                }
                else if ((status == ClaimStatus.UnderReview.ToString()) || (status == EnumHelper.GetEnumDescription(ClaimStatus.UnderReview))) //"UnderReview" || "Under Review"
                {
                    claim.AssignedDate = DateTime.UtcNow;
                    if (claim.StartedDate == null)
                    {
                        claim.StartedDate = DateTime.UtcNow;
                    }
                }
                else if (status == ClaimStatus.Approved.ToString())//"Approved"
                {
                    claim.CreatedDate = DateTime.UtcNow;
                    claim.ApprovedDate = claim.CreatedDate;
                    claim.ReceiptDate = claim.CreatedDate;
                }
                else if (status == "Open")
                {
                    //claim.IsActive = false;
                }
                else
                {
                    claim.ModifiedUser = updatedBy.ToString();
                }
            }

            if (-1 == assignToUser) //un-assign
            {
                claim.AssignToUser = null;
            }
            else if (0 != assignToUser)
            {
                claim.AssignToUser = assignToUser;
            }
            // objTSFClaim.ReceiptDate = model.PayReceiptDate;
            if (!string.IsNullOrEmpty(submitted)) //OTSTM2-56          
                claim.SubmissionDate = Convert.ToDateTime(submitted).ToUniversalTime();
            if (!string.IsNullOrEmpty(received))
                claim.ReceiptDate = Convert.ToDateTime(received).ToUniversalTime();
            if (!string.IsNullOrEmpty(deposit))
                claim.DepositDate = Convert.ToDateTime(deposit).ToUniversalTime();
            //if (!string.IsNullOrEmpty(referenceNo))
            claim.ChequeReferenceNumber = referenceNo.Trim();
            if (!string.IsNullOrEmpty(amount))
                claim.PaymentAmount = Convert.ToDecimal(amount);
            if (!string.IsNullOrEmpty(PaymentType))
                claim.PaymentType = PaymentType.Trim();

            //claim.BalanceDue = claim.TotalTSFDue - claim.PaymentAmount;
            claim.BalanceDue = Math.Round(claim.TotalTSFDue, 2, MidpointRounding.AwayFromZero) - Math.Round(claim.PaymentAmount.Value, 2, MidpointRounding.AwayFromZero) - Math.Round(((null != claim.Credit) ? claim.Credit.Value : 0), 2, MidpointRounding.AwayFromZero);

            dbContext.SaveChanges();
            if (!string.IsNullOrEmpty(received) || !string.IsNullOrEmpty(submitted))  //OTSTM2-56
                UpdateClaimCalcs(claim.ID);

            return claim.BalanceDue;
        }

        /// <summary>
        /// Auto Approval of $0 Remittance
        /// </summary>
        public void SetRemittanceStatusAuto(int ClaimId, string status, string updatedBy, string amount, long assignToUser)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var claim = dbContext.TSFClaims.Find(ClaimId);
            if (!string.IsNullOrEmpty(status))
            {

                claim.RecordState = status.ToString();
                if (status == ClaimStatus.Approved.ToString())//"Approved"
                {
                    claim.CreatedUser = updatedBy.ToString();
                    claim.CreatedDate = DateTime.UtcNow;
                    claim.SubmissionDate = claim.CreatedDate;
                    claim.ApprovedDate = claim.CreatedDate;
                    claim.AssignedDate = null;
                }
            }

            if (-1 == assignToUser) //un-assign
            {
                claim.AssignToUser = null;
            }
            else if (0 != assignToUser)
            {
                claim.AssignToUser = assignToUser;
            }
            // objTSFClaim.ReceiptDate = model.PayReceiptDate;
            //if (!string.IsNullOrEmpty(received))
            //    claim.ReceiptDate = Convert.ToDateTime(received);
            //if (!string.IsNullOrEmpty(deposit))
            //    claim.DepositDate = Convert.ToDateTime(deposit);
            //if (!string.IsNullOrEmpty(referenceNo))
            //    claim.ChequeReferenceNumber = referenceNo.Trim();
            if (!string.IsNullOrEmpty(amount))
                claim.PaymentAmount = Convert.ToDecimal(amount);
            //if (!string.IsNullOrEmpty(PaymentType))
            claim.PaymentType = null;

            //claim.BalanceDue = claim.TotalTSFDue - claim.PaymentAmount;
            claim.BalanceDue = Math.Round(claim.TotalTSFDue, 2, MidpointRounding.AwayFromZero) 
                             - Math.Round(claim.PaymentAmount.Value, 2, MidpointRounding.AwayFromZero) 
                             - Math.Round(((null != claim.Credit) ? claim.Credit.Value : 0), 2, MidpointRounding.AwayFromZero);

            dbContext.SaveChanges();
            //if (!string.IsNullOrEmpty(received))
            //    UpdateClaimCalcs(claim.ID);

        }

        private IEnumerable<User> GetAssignedUsers(int[] assignedUserIds)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            return dbContext.Users.Where(i => assignedUserIds.Any(n => n == i.ID)).ToList();
        }



        #region TSFClaim Attachment

        public void AddTSFClaimAttachments(int tsfclaimId, IEnumerable<AttachmentModel> attachments)
        {
            ConditionCheck.Bounds(tsfclaimId, "Argument is out of bounds", int.MaxValue, 0);
            var claim = entityStore.FindById(tsfclaimId);

            foreach (var attachmentModel in attachments)
            {
                var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
                claim.Attachments.Add(attachment);
            }

            entityStore.Update(claim);
        }

        public IEnumerable<AttachmentModel> GetTSFClaimAttachments(int tsfclaimId, bool bBankInfo = false)
        {
            ConditionCheck.Bounds(tsfclaimId, "Argument is out of bounds", int.MaxValue, 0);

            var attachments = entityStore.FindById(tsfclaimId).Attachments;
            var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachments);
            return attachmentModelList;
        }

        public AttachmentModel GetTSFClaimAttachment(int tsfclaimId, string fileUniqueName)
        {
            ConditionCheck.Bounds(tsfclaimId, "Argument is out of bounds", int.MaxValue, 0);

            var attachment = entityStore.FindById(tsfclaimId).Attachments.Single(r => r.UniqueName == fileUniqueName);

            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveTSFClaimAttachment(int tsfclaimId, string fileUniqueName)
        {
            ConditionCheck.Bounds(tsfclaimId, "Argument is out of bounds", int.MaxValue, 0);

            var currentContext = (context as TSFRemittanceBoundedContext);
            var claim = currentContext.TSFClaims.FirstOrDefault(a => a.ID == tsfclaimId);
            var attachment = claim.Attachments.FirstOrDefault(a => a.UniqueName == fileUniqueName);

            if (attachment != null)
            {
                claim.Attachments.Remove(attachment);
                //currentContext.Attachments.Remove(attachment);
                attachment.FileName = "DELETE";
                currentContext.Attachments.Attach(attachment);
                var attachmentEntry = currentContext.Entry(attachment);
                attachmentEntry.State = EntityState.Modified;
                attachmentEntry.Property(a => a.FileName).IsModified = true;

                currentContext.SaveChanges();
            }
        }

        #endregion

        #region TSFRemittance Notes
        public PaginationDTO<TSFRemittanceNoteViewMode, int> LoadTSFRemittanceNotes(int claimId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {

            var dbContext = context as TSFRemittanceBoundedContext;
            var query = from note in dbContext.TSFRemittanceNotes
                        join user in dbContext.Users on note.UserId equals user.ID into ps
                        from user in ps.DefaultIfEmpty()

                        where note.TSFClaimId == claimId
                        select new TSFRemittanceNoteViewMode
                        {
                            ID = note.ID,
                            ClaimId = note.TSFClaimId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId ?? 0,
                            AddedBy = note.UserId != null ? user.FirstName.Trim() + " " + user.LastName.Trim() : "System Admin"
                        };

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();
                query = query.AsEnumerable().Where(c => c.Note.ToLower().Contains(searchText) || c.AddedBy.ToLower().Contains(searchText) || ((c.CreatedDate != null) ? c.CreatedDate.ToString("yyyy-MM-dd") : "").ToLower().Contains(searchText)).AsQueryable();

            }

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
             .Take(pageSize)
             .ToList();

            return new PaginationDTO<TSFRemittanceNoteViewMode, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }


        public void AddTSFRemittanceNote(TSFRemittanceNote tsfRemittanceNote)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            if (dbContext.TSFRemittanceNotes.Where(i => i.Note == TreadMarksConstants.FirstTimePenalty && i.TSFClaimId == tsfRemittanceNote.TSFClaimId && i.UserId == null).Any())
            {
                return;//duplicate note exists.
            }

            dbContext.TSFRemittanceNotes.Add(tsfRemittanceNote);
            dbContext.SaveChanges();
        }


        public List<TSFRemittanceNoteViewMode> LoadTSFRemittanceNotesForExport(int claimId, string searchText,
            string sortcolumn, string sortdirection)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var query = from note in dbContext.TSFRemittanceNotes
                        join user in dbContext.Users on note.UserId equals user.ID into ps
                        from user in ps.DefaultIfEmpty()

                        where note.TSFClaimId == claimId
                        select new TSFRemittanceNoteViewMode
                        {
                            ID = note.ID,
                            ClaimId = note.TSFClaimId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId ?? 0,
                            AddedBy = note.UserId != null ? user.FirstName.Trim() + " " + user.LastName.Trim() : "System Admin"
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.AsEnumerable().Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText) || ((c.CreatedDate != null) ? c.CreatedDate.ToString("yyyy-MM-dd") : "").ToLower().Contains(searchText)).AsQueryable();
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "CreatedDate" : sortcolumn;

            if (sortdirection == "asc")
                query = query.AsQueryable<TSFRemittanceNoteViewMode>().OrderBy(sortcolumn);
            else
                query = query.AsQueryable<TSFRemittanceNoteViewMode>().OrderBy(sortcolumn + " descending");

            return query.ToList();
        }
        #endregion

        #region Export To Excel
        private List<TSFRemittanceBriefModel> LoadApplicationBriefModelsForCustomer(string searchText, string orderBy, string sortDirection, string regNo, Dictionary<string, string> columnSearchText)
        {
            string sSeparator = string.Empty;
            var dbContext = context as TSFRemittanceBoundedContext;
            dynamic query;


            query = from claim in dbContext.TSFClaims
                    join prd in dbContext.Periods on claim.PeriodID equals prd.ID into tsf
                    from prd in tsf.DefaultIfEmpty()

                    join user in dbContext.Users on claim.AssignToUser equals user.ID into tsf1
                    from user in tsf1.DefaultIfEmpty()
                    where (claim.RegistrationNumber == regNo || regNo == null)
                    select new TSFRemittanceBriefModel
                    {
                        ID = claim.ID,
                        Period = prd.ShortName,
                        PeriodDate = prd.StartDate,
                        Status = claim.RecordState,
                        Due = claim.TotalTSFDue,
                        BalanceDue = claim.BalanceDue != null ? claim.BalanceDue.Value : 0,
                        Payments = claim.PaymentAmount,
                        Submitted = claim.SubmissionDate,
                        SubmittedBy = claim.CreatedUser,
                        AssignedTo = user.FirstName.Trim() + " " + user.LastName.Trim(),
                        TotalReceivable = claim.TotalTSFDue// - (claim.Credit ?? 0) + (claim.AdjustmentTotal ?? 0),
                    };

            List<TSFRemittanceBriefModel> returnValue = new List<TSFRemittanceBriefModel>();

            foreach (var app in query)
            {
                //OTSTM2-984 comment out
                //if (app.Status == ClaimStatus.Open.ToString())
                //    app.Submitted = null; //show blank in UI

                if (null == app.AssignedTo) app.AssignedTo = "";

                returnValue.Add(app);
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();

                returnValue =
                  (from qry in returnValue.AsEnumerable()
                   join jn in dbContext.TSFClaims on qry.ID equals jn.ID
                   select new TSFRemittanceBriefModel
                   {
                       ID = qry.ID,
                       Period = qry.Period,
                       PeriodDate = qry.PeriodDate,
                       Status = qry.Status,
                       Due = qry.Due,
                       TotalReceivable = qry.TotalReceivable,
                       BalanceDue = (jn.BalanceDue != null) ? Convert.ToDecimal(jn.BalanceDue) : 0,
                       Payments = qry.Payments,
                       Submitted = qry.Submitted,
                       SubmittedBy = qry.SubmittedBy,
                       AssignedTo = qry.AssignedTo
                   }).Where(c => ((c.Submitted != null) ? c.Submitted.Value.ToString("yyyy-MM-dd") : "").ToLower().Contains(searchText)
                                  || c.Period.ToLower().Contains(searchText)
                                  || c.Due.ToString("0.00").ToLower().Contains(searchText) //OTSTM2-984
                                  || c.TotalReceivable.ToString("0.00").ToLower().Contains(searchText)
                                  || c.Status.ToString().ToLower().Contains(searchText)
                                  || (c.Payments != null ? c.Payments.Value.ToString("0.00") : "0.00").ToLower().Contains(searchText) //OTSTM2-984
                                  || c.BalanceDue.ToString("0.00").ToLower().Contains(searchText) //OTSTM2-984
                                  || ((c.SubmittedBy != null) ? c.SubmittedBy : "").ToString().ToLower().Contains(searchText))
                                .ToList<TSFRemittanceBriefModel>();

            }

            //OTSTM2-984 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            returnValue.ForEach(i =>
            {
                i.Submitted = (i.Submitted != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.Submitted.Value, TimeZoneInfo.Local) : i.Submitted;
            });

            //OTSTM2-984
            var columnSearchTextPeriod = columnSearchText["Period"].ToLower();
            var columnSearchTextStatus = columnSearchText["Status"].ToLower();
            var columnSearchTextDue = columnSearchText["Due"].ToLower();
            var columnSearchTextTotalReceivable = columnSearchText["TotalReceivable"];
            var columnSearchTextPayment = columnSearchText["Payment"].ToLower();
            var columnSearchTextBalanceDue = columnSearchText["BalanceDue"].ToLower();
            var columnSearchTextSubmitted = columnSearchText["Submitted"].ToLower();
            var columnSearchTextSubmittedBy = columnSearchText["SubmittedBy"].ToLower();

            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                returnValue = returnValue.Where(c => c.Period.ToString().ToLower().Contains(columnSearchTextPeriod)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                returnValue = returnValue.Where(c => c.Status.ToString().ToLower().Contains(columnSearchTextStatus)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextDue))
            {
                returnValue = returnValue.Where(c => c.Due.ToString("0.00").ToLower().Contains(columnSearchTextDue)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextTotalReceivable))
            {
                returnValue = returnValue.Where(c => c.TotalReceivable.ToString("0.00").ToLower().Contains(columnSearchTextTotalReceivable)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPayment))
            {
                returnValue = returnValue.Where(c => (c.Payments != null ? c.Payments.Value.ToString("0.00") : "0.00").ToLower().Contains(columnSearchTextPayment)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextBalanceDue))
            {
                returnValue = returnValue.Where(c => c.BalanceDue.ToString("0.00").ToLower().Contains(columnSearchTextBalanceDue)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                returnValue = returnValue.Where(c => ((c.Submitted != null) ? c.Submitted.Value.ToString("yyyy-MM-dd") : string.Empty).ToLower().Contains(columnSearchTextSubmitted)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmittedBy))
            {
                returnValue = returnValue.Where(c => ((c.SubmittedBy != null) ? c.SubmittedBy.ToString() : string.Empty).ToLower().Contains(columnSearchTextSubmittedBy)).ToList();
            }

            if (orderBy.ToLower() == "period")
                orderBy = "perioddate";
            if ((!string.IsNullOrEmpty(orderBy)) && (!string.IsNullOrEmpty(sortDirection)))
                //query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                returnValue = sortDirection == "asc"
                    ? returnValue.AsQueryable().OrderBy(orderBy).ToList<TSFRemittanceBriefModel>()
                    : returnValue.AsQueryable().OrderBy(orderBy + " descending").ToList<TSFRemittanceBriefModel>();
            return returnValue;
        }
        private List<TSFRemittanceBriefModel> LoadApplicationBriefModelsForAll(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            string sSeparator = string.Empty;
            var dbContext = context as TSFRemittanceBoundedContext;
            DateTime dtbefore = Convert.ToDateTime("2016-01-01"); //OTSTM2-188

            //OTSTM2-929 make consistency with list page
            var query = from claim in dbContext.TSFClaims
                        join user in dbContext.Users on claim.AssignToUser equals user.ID into claim_user
                        join customer in dbContext.Customers on claim.CustomerID equals customer.ID into claim_customer //OTSTM2-188
                        from set1 in claim_user.DefaultIfEmpty()
                        from set2 in claim_customer.DefaultIfEmpty() //OTSTM2-188
                        select new TSFRemittanceBriefModel
                        {
                            ID = claim.ID,
                            RegNo = claim.Customer.RegistrationNumber,
                            Company = claim.Customer.BusinessName,
                            Period = claim.Period.ShortName,
                            PeriodDate = claim.Period.StartDate,
                            Status = claim.RecordState,
                            Submitted = claim.SubmissionDate,
                            AssignedToUserId = claim.AssignToUser,
                            AssignedTo = set1.FirstName + " " + set1.LastName,
                            //OTSTM2-188
                            ReportingFrequency = claim.Period.StartDate >= dtbefore ?
                                                    (set2.CustomerSettingHistoryList.Count > 0 ?
                                                        (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == claim.Period.StartDate.Year).OrderByDescending(c => c.ChangeDate).FirstOrDefault() != null ?
                                                            set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == claim.Period.StartDate.Year).OrderByDescending(c => c.ChangeDate).FirstOrDefault().NewValue :
                                                            (claim.Period.StartDate.Year < DateTime.UtcNow.Year ?
                                                                (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == SqlFunctions.DateAdd("year", 1, claim.Period.StartDate).Value.Year).FirstOrDefault() != null ?
                                                                    (set2.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == SqlFunctions.DateAdd("year", 1, claim.Period.StartDate).Value.Year).FirstOrDefault().OldValue)
                                                                    : "12x"
                                                                )
                                                                : "12x"
                                                            )
                                                        )
                                                        : (claim.Period.StartDate.Year != DateTime.UtcNow.Year ? "12x" : ((bool)set2.RemitsPerYear ? "12x" : "2x"))
                                                    )
                                                : "12x",
                            DepositDate = claim.DepositDate,
                            TotalReceivable = claim.TotalTSFDue, // - (claim.Credit ?? 0) + (claim.AdjustmentTotal ?? 0),
                            ChequeAmount = claim.PaymentAmount ?? 0,
                            BalanceDue = claim.BalanceDue ?? 0
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.Submitted) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.Submitted) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.Submitted) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c =>
                            c.RegNo.ToString().Contains(searchText)
                            || c.Company.ToLower().Contains(searchText)
                            || c.Period.ToLower().Contains(searchText)
                            || c.Status.Contains(searchText)
                            || c.AssignedTo.Contains(searchText)
                            //OTSTM2-188
                            || c.ReportingFrequency.Contains(searchText)
                            || SqlFunctions.StringConvert(c.TotalReceivable, 18, 2).Contains(searchText) //OTSTM2-929
                            || SqlFunctions.StringConvert(c.ChequeAmount, 18, 2).Contains(searchText) //OTSTM2-929
                            || SqlFunctions.StringConvert(c.BalanceDue, 18, 2).Contains(searchText) //OTSTM2-929
                            );
                }
            }

            //OTSTM2-929
            var columnSearchTextRegNumber = columnSearchText["RegNumber"];
            var columnSearchTextCompany = columnSearchText["Company"];
            var columnSearchTextReportingFrequency = columnSearchText["ReportingFrequency"];
            var columnSearchTextPeriod = columnSearchText["Period"];
            var columnSearchTextSubmitted = columnSearchText["Submitted"];
            var columnSearchTextDepositDate = columnSearchText["DepositDate"];
            var columnSearchTextTotalReceivable = columnSearchText["TotalReceivable"];
            var columnSearchTextChequeAmount = columnSearchText["ChequeAmount"];
            var columnSearchTextBalanceDue = columnSearchText["BalanceDue"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAssignedTo = columnSearchText["AssignedTo"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextRegNumber))
            {
                query = query.Where(c => c.RegNo.ToString().Contains(columnSearchTextRegNumber));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
            {
                query = query.Where(c => c.Company.ToString().Contains(columnSearchTextCompany));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextReportingFrequency))
            {
                query = query.Where(c => c.ReportingFrequency.ToString().Contains(columnSearchTextReportingFrequency));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextPeriod))
            {
                query = query.Where(c => c.Period.ToString().Contains(columnSearchTextPeriod));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextDepositDate))
            {
                query = query.Where(c => (c.DepositDate != null ? (SqlFunctions.DatePart("yyyy", c.DepositDate.Value) + "-" + DbFunctions.Right("00" + c.DepositDate.Value.Month, 2) + "-" + DbFunctions.Right("00" + c.DepositDate.Value.Day, 2)) : string.Empty).ToString().Contains(columnSearchTextDepositDate));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextTotalReceivable))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.TotalReceivable, 18, 2).Contains(columnSearchTextTotalReceivable));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextChequeAmount))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.ChequeAmount, 18, 2).Contains(columnSearchTextChequeAmount));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextBalanceDue))
            {
                query = query.Where(c => SqlFunctions.StringConvert(c.BalanceDue, 18, 2).Contains(columnSearchTextBalanceDue));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedTo))
            {
                query = query.Where(c => c.AssignedTo.ToString().Contains(columnSearchTextAssignedTo));
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Submitted" : orderBy;
            orderBy = orderBy == "Period" ? "PeriodDate" : orderBy;

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            var result = query.ToList();

            //OTSTM2-929 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            result.ForEach(i =>
            {
                i.Submitted = (i.Submitted != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.Submitted.Value, TimeZoneInfo.Local) : i.Submitted;
            });

            if (!string.IsNullOrWhiteSpace(columnSearchTextSubmitted))
            {
                result = result.Where(c => (c.Submitted != null ? c.Submitted.Value.ToString("yyyy-MM-dd") : string.Empty).Contains(columnSearchTextSubmitted)).ToList();
            }

            return result;
        }
        public List<TSFRemittanceBriefModel> LoadRemittanceApplications(string searchText, string sortcolumn, string sortdirection, string regNo, Dictionary<string, string> columnSearchText)
        {
            if (regNo == null)
            {
                return LoadApplicationBriefModelsForAll(searchText, sortcolumn, sortdirection, columnSearchText);
            }
            else
            {
                return LoadApplicationBriefModelsForCustomer(searchText, sortcolumn, sortdirection, regNo, columnSearchText);
            }
        }
        #endregion

        public TSFClaim GetLastApprovedTSFRemittance(int customerID, int claimID)
        {
            var tsfClaim = entityStore.All.Where(t => t.CustomerID == customerID && t.ID != claimID && t.RecordState != "Open").OrderByDescending(p => p.SubmissionDate).FirstOrDefault();
            return tsfClaim;
        }
        public bool IsAnyPastPenalty(int customerID, int claimID)
        {
            return entityStore.All.Where(t => t.CustomerID == customerID && t.ID != claimID && t.Period.StartDate.Year > 2009 && t.RecordState == "Approved" && (t.Penalties > 0 || t.PenaltiesManually > 0)).Any();
        }

        //OTSTM2-485
        #region Internal Adjustment
        public PaginationDTO<TSFClaimInternalAdjustment, int> LoadTSFClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            var queryPaymentAdjustment = from tsfclaimInternalPaymentAdjust in dbContext.TSFClaimInternalPaymentAdjustment.AsNoTracking()
                                         where tsfclaimInternalPaymentAdjust.TSFClaimId == claimId && tsfclaimInternalPaymentAdjust.Status != TSFInternalAdjustmentStatus.Voided.ToString()
                                         select new TSFClaimInternalAdjustment
                                         {
                                             InternalAdjustmentId = tsfclaimInternalPaymentAdjust.ID,
                                             AdjustmentDate = tsfclaimInternalPaymentAdjust.AdjustDate,
                                             AdjustmentBy = tsfclaimInternalPaymentAdjust.AdjustUser.FirstName + " " + tsfclaimInternalPaymentAdjust.AdjustUser.LastName,
                                             TSFInternalAdjustmentType = TSFInternalAdjustmentType.Payment,
                                             Note = tsfclaimInternalPaymentAdjust.TSFClaimInternalAdjustmentNotes.FirstOrDefault() != null ? tsfclaimInternalPaymentAdjust.TSFClaimInternalAdjustmentNotes.FirstOrDefault().Note : string.Empty,
                                             TSFClaimInternalAdjustmentNotes = tsfclaimInternalPaymentAdjust.TSFClaimInternalAdjustmentNotes.OrderByDescending(a => a.CreatedDate).ToList()
                                         };
            var query = queryPaymentAdjustment.ToList().AsQueryable();

            List<TSFClaimInternalAdjustment> returnVal = query.ToList();
            if (!isStaff)
            {
                //Change to System
                returnVal.ForEach(c =>
                {
                    c.AdjustmentBy = "STAFF";
                });
            }

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                returnVal = returnVal.Where(c => c.TSFInternalAdjustmentType.ToString().ToLower().Contains(searchText.ToLower()) || c.AdjustmentBy.ToLower().Contains(searchText.ToLower()) || c.AdjustmentDate.ToString(TreadMarksConstants.DateFormat).Contains(searchText)).ToList();
            }

            //Applying sorting
            returnVal = sortDirection == "asc" ? returnVal.AsQueryable<TSFClaimInternalAdjustment>().OrderBy(orderBy).ToList() : returnVal.AsQueryable<TSFClaimInternalAdjustment>().OrderBy(orderBy + " descending").ToList();

            var totalRecords = returnVal.Count();
            var result = returnVal.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<TSFClaimInternalAdjustment, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public void AddInternalAdjustmentNote(TSFClaimInternalAdjustmentNote note)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            dbContext.TSFClaimInternalAdjustmentNote.Add(note);
            dbContext.SaveChanges();
        }

        public void AddInternalPaymentAdjustment(TSFClaimInternalPaymentAdjustment tsfclaimInternalPaymentAdjust)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            dbContext.TSFClaimInternalPaymentAdjustment.Add(tsfclaimInternalPaymentAdjust);
            dbContext.SaveChanges();
            UpdateClaimCalcs(tsfclaimInternalPaymentAdjust.TSFClaimId);
        }

        public void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            if (adjustmentType == 1)
            {
                var paymentAdjust = dbContext.TSFClaimInternalPaymentAdjustment.Where(c => c.ID == internalAdjustmentId).FirstOrDefault();

                //Not remove the internal adjustment, just change Status to "Voided"
                if (paymentAdjust != null)
                {
                    paymentAdjust.Status = TSFInternalAdjustmentStatus.Voided.ToString();
                    dbContext.SaveChanges();
                }
            }
            UpdateClaimCalcs(claimId);
        }

        public TSFClaimInternalPaymentAdjustment GetClaimInternalPaymentAdjust(int claimInternalPaymentAdjustId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            return dbContext.TSFClaimInternalPaymentAdjustment.FirstOrDefault(c => c.ID == claimInternalPaymentAdjustId);
        }

        public void UpdateInternalPaymentAdjust(int claimInternalAdjustId, TSFClaimAdjustmentModalResult modalResult, long editUserId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var editAdjust = dbContext.TSFClaimInternalPaymentAdjustment.FirstOrDefault(c => c.ID == claimInternalAdjustId);
            editAdjust.AdjustBy = editUserId;
            editAdjust.AdjustDate = DateTime.UtcNow;
            editAdjust.AdjustmentAmount = modalResult.Amount;
            editAdjust.PaymentType = (int)EnumHelper.ToEnum<TSFInternalAdjustmentPaymentType>(modalResult.PaymentType);
            dbContext.SaveChanges();
            UpdateClaimCalcs(editAdjust.TSFClaimId);
        }

        public List<TSFClaimInternalAdjustmentNote> LoadClaimInternalNotesForPayment(int ClaimInternalPaymentAdjustId)
        {
            var dbContext = context as TSFRemittanceBoundedContext;
            var query = dbContext.TSFClaimInternalAdjustmentNote.AsNoTracking().Include(c => c.User).Where(cl => cl.TSFInternalAdjustmentId == ClaimInternalPaymentAdjustId);
            return query.ToList();
        }

        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            var dbContext = context as TSFRemittanceBoundedContext;

            IQueryable<InternalNoteViewModel> query = null;

            query = dbContext.TSFClaimInternalAdjustmentNote.AsNoTracking().Where(cl => cl.TSFInternalAdjustmentId == ClaimInternalPaymentAdjustId)
                        .Join(dbContext.Users.AsNoTracking(), notes => notes.UserId, user => user.ID, (notes, user) => new InternalNoteViewModel
                        {
                            Note = notes.Note,
                            AddedOn = notes.CreatedDate,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        });

            if (!string.IsNullOrEmpty(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    query = query.Where(c => c.Note.ToLower().Contains(searchText.ToLower()) || c.AddedBy.ToLower().Contains(searchText.ToLower()));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;

            if (!sortReverse)
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn);
            }
            else
            {
                query = query.AsQueryable<InternalNoteViewModel>().OrderBy(sortcolumn + " descending");
            }

            return query.ToList();
        }

        #endregion

    }
}
