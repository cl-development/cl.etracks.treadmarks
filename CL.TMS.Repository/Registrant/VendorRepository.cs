﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.XPath;
using CL.Framework.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.Registrant;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Common.Enum;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.Framework.DTO;

namespace CL.TMS.Repository.Registrant
{
    public class VendorRepository : BaseRepository<RegistrantBoundedContext, Vendor, int>, IVendorRepository
    {
        private EntityStore<VendorActiveHistory, int> vendorActiveHistoryStore;
        private EntityStore<Address, int> addressStore;
        private EntityStore<Customer, int> customerStore;
        private EntityStore<CustomerActiveHistory, int> customerActiveHistoryStore;

        public VendorRepository()
        {
            vendorActiveHistoryStore = new EntityStore<VendorActiveHistory, int>(context);
            addressStore = new EntityStore<Address, int>(context);
            customerStore = new EntityStore<Customer, int>(context);
            customerActiveHistoryStore = new EntityStore<CustomerActiveHistory, int>(context);
        }

        public IEnumerable<ProcessorListModel> GetProcessorList()
        {
            var dbContext = context as RegistrantBoundedContext;
            var query = dbContext.Vendors.AsNoTracking().Where(r => r.VendorType == 4)
                .Select(r => new ProcessorListModel { ID = r.ID, Name = r.BusinessName, Number = r.Number });
            return query.ToList();
        }

        public List<GpParticipantDto> LoadParticipants()
        {
            var dbContext = context as RegistrantBoundedContext;
            //Load Vendor first
            var mailEnum = (int)AddressTypeEnum.Mail;
            var businessEnum = (int)AddressTypeEnum.Business;
            var queryVendor = from vendor in dbContext.Vendors.AsNoTracking()
                              select new GpParticipantDto
                              {
                                  RegistrationNumber = vendor.Number,
                                  BusinessName = vendor.BusinessName,
                                  InBatch = vendor.InBatch,
                                  GpUpdate = vendor.GpUpdate ?? false,
                                  Address = vendor.VendorAddresses.FirstOrDefault(va => va.AddressType == mailEnum) ?? vendor.VendorAddresses.FirstOrDefault(va => va.AddressType == businessEnum),
                                  Addresses = vendor.VendorAddresses.ToList(),
                                  RegistrantTypeId = vendor.VendorType,
                                  IsTaxExempt = vendor.IsTaxExempt,
                                  IsActive = vendor.IsActive,
                                  IsVendor = true,
                                  ObjectID = vendor.ID,
                                  //OTSTM2-1139 No need to check banking approved
                                  //BankInformation = vendor.BankInformations.FirstOrDefault(b => b.ReviewStatus == "Approved")
                              };
            var vendorResult = queryVendor.Where(p => (p.GpUpdate || !p.InBatch)).ToList();
            var queryCustomer = from customer in dbContext.Customers.AsNoTracking()
                                select new GpParticipantDto
                                {
                                    RegistrationNumber = customer.RegistrationNumber,
                                    BusinessName = customer.BusinessName,
                                    InBatch = customer.InBatch,
                                    GpUpdate = customer.GpUpdate ?? false,
                                    Address = customer.CustomerAddresses.FirstOrDefault(va => va.AddressType == mailEnum) ?? customer.CustomerAddresses.FirstOrDefault(va => va.AddressType == businessEnum),
                                    Addresses = customer.CustomerAddresses.ToList(),
                                    RegistrantTypeId = customer.CustomerType,
                                    IsTaxExempt = customer.IsTaxExempt,
                                    IsActive = customer.IsActive,
                                    IsVendor = false,
                                    ObjectID = customer.ID,
                                    BankInformation = null
                                };
            var resultCustomer = queryCustomer.Where(p => p.GpUpdate || !p.InBatch).ToList();

            return (vendorResult.Concat(resultCustomer)).ToList();
        }

        public IReadOnlyDictionary<int, string> GetVendorNameList()
        {
            var vendorNameList = new Dictionary<int, string>();

            //Dont pull Stewards (customer table - already excluded) and RPMs - QR codes are not required for them
            var listOfVendors = entityStore.All.Where(v => v.VendorType != 5).OrderBy(v => v.Number);

            foreach (var vendor in listOfVendors)
            {
                //vendorNameList.Add(vendor.ID, string.Format("{0} ({1})", vendor.BusinessName, vendor.Number));
                vendorNameList.Add(vendor.ID, string.Format("{0} - {1}", vendor.Number, vendor.BusinessName));
            }

            return vendorNameList;
        }

        //-----------Added by Frank, begin: new function-------------
        public IReadOnlyDictionary<int, string> GetListOfRegistrantsWithPageIndex(int pageIndex, int pageSize)
        {
            var listOfVendors = entityStore.All.Where(v => v.VendorType != 5).OrderBy(v => v.Number).Skip(pageSize * (pageIndex - 1)).Take(pageSize).Select(c => new { ID = c.ID, Number = c.Number, BusinessName = c.BusinessName }).ToList();

            return listOfVendors.ToDictionary(c => c.ID, c => c.Number + " " + c.BusinessName);
        }
        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement
        public string GetMatchedRegistrant(string regNumberEntered)
        {
            var matchedVendor = entityStore.All.Where(v => v.VendorType != 5).FirstOrDefault(v => v.Number == regNumberEntered);
            if (matchedVendor == null)
            {
                return "";
            }
            else
            {
                return matchedVendor.ID + ":" + matchedVendor.Number + ", " + matchedVendor.BusinessName;
            }
        }

        public Customer GetSingleCustomeWithApplicatioId(int appId)
        {
            if (appId < 0) { throw new ArgumentOutOfRangeException("appId"); }
            return (context as RegistrantBoundedContext).Customers.FirstOrDefault(c => c.ApplicationID == appId);
        }

        public Vendor GetSingleWithApplicationID(int appId)
        {
            if (appId < 0) { throw new ArgumentOutOfRangeException("AppId"); }
            var dbContext = (context as RegistrantBoundedContext);
            return dbContext.Vendors.FirstOrDefault(z => z.ApplicationID == appId);
        }

        public Vendor GetSingleVendorByID(int vendorId)
        {
            ConditionCheck.Bounds(vendorId, "vendorId", int.MaxValue);
            return entityStore.All.FirstOrDefault(c => c.ID == vendorId);
        }

        public Customer GetSingleCustomerByID(int customerId)
        {
            ConditionCheck.Bounds(customerId, "customerId", int.MaxValue);
            return customerStore.All.FirstOrDefault(c => c.ID == customerId);
        }

        public Vendor FindVendorByID(int vendorId)
        {
            if (vendorId < 0) { throw new ArgumentOutOfRangeException("vendorId"); }
            var dbContext = (context as RegistrantBoundedContext);
            return dbContext.Vendors.FirstOrDefault(c => c.ID == vendorId);
        }

        public Customer FindCustomerByID(int customerId)
        {
            if (customerId < 0) { throw new ArgumentOutOfRangeException("customerId"); }
            var dbContext = (context as RegistrantBoundedContext);
            return dbContext.Customers.FirstOrDefault(c => c.ID == customerId);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vendor"></param>
        public void AddVendor(Vendor vendor)
        {
            entityStore.Insert(vendor);
        }

        public void UpdateVendor(Vendor vendor)
        {
            //Update Addresses and Contacts
            entityStore.Update(vendor);
        }

        public void UpdateCustomer(Customer customer)
        {
            //Update Addresses and Contacts
            customerStore.Update(customer);
        }
        public void UpdateVendorKeywords(Vendor vendor)
        {
            vendor.KeyWords = this.GetKeyWords(vendor);
            entityStore.Update(vendor);
        }
        public void UpdateVendorKeywordsAll(int vendorType = -1)
        {
            var dbContext = context as RegistrantBoundedContext;

            //fix since collectors have 9000 + records 
            if (vendorType == 2)
            {
                var vendors = dbContext.Vendors.Where(v => v.VendorType == vendorType).OrderBy(v => v.ID).ToList();
                var i = 0;
                var count = vendors.Count();
                int inc = 1000;
                do
                {
                    var max = Math.Min(count - (i * inc), inc);
                    var res = vendors.Skip(i * inc);
                    if (max >= inc)
                    {
                        res = res.Take(max);
                    }
                    res.ToList().ForEach(j =>
                    {
                        j.KeyWords = this.GetKeyWords(j);
                    });
                    dbContext.SaveChanges();
                } while ((i++ * inc) < count);
            }
            else
            {
                dbContext.Vendors.Where(i => i.VendorType == vendorType || vendorType == -1).ToList().ForEach(i =>
                {
                    i.KeyWords = this.GetKeyWords(i);
                });
                dbContext.SaveChanges();
            }
        }

        public void UpdateCustomerKeywords(Customer customer)
        {
            customer.KeyWords = this.GetKeyWords(customer);
            customerStore.Update(customer);
        }
        public void UpdateCustomerKeywordsAll()
        {
            var dbContext = context as RegistrantBoundedContext;
            dbContext.Customers.ToList().ForEach(i =>
            {
                i.KeyWords = this.GetKeyWords(i);
            });
            dbContext.SaveChanges();
        }


        public void SetRemitanceStatus(int customerId, int ID, string PropName, int userId)
        {
            var customer = customerStore.All.FirstOrDefault(c => c.ID == customerId);
            if (customer != null)
            {
                switch (PropName)
                {
                    case "SRemitsPerYear2x":
                        //OTSTM2-21
                        var setting2x = new CustomerSettingHistory
                        {
                            CustomerId = customerId,
                            ColumnName = "RemitsPerYear",
                            OldValue = customer.RemitsPerYear == true ? "12x" : "2x",
                            NewValue = "2x",
                            ChangeBy = userId,
                            ChangeDate = DateTime.UtcNow
                        };
                        customer.CustomerSettingHistoryList.Add(setting2x);
                        customer.RemitsPerYear = false;
                        customer.RemitsPerYearSwitchDate = DateTime.UtcNow;
                        break;

                    //OTSTM2-1110 comment out
                    //case "SAuditOn":
                    //    customer.Audit = true;
                    //    customer.MOESwitchDate = DateTime.UtcNow;
                    //    break;

                    case "SMOEOn":
                        customer.MOE = true;
                        customer.MOESwitchDate = DateTime.UtcNow;
                        break;

                    case "SRemitsPerYear12x":
                        //OTSTM2-21
                        var setting12x = new CustomerSettingHistory
                        {
                            CustomerId = customerId,
                            ColumnName = "RemitsPerYear",
                            OldValue = customer.RemitsPerYear == true ? "12x" : "2x",
                            NewValue = "12x",
                            ChangeBy = userId,
                            ChangeDate = DateTime.UtcNow
                        };
                        customer.CustomerSettingHistoryList.Add(setting12x);
                        customer.RemitsPerYear = true;
                        customer.RemitsPerYearSwitchDate = DateTime.UtcNow;
                        break;

                    //OTSTM2-1110 comment out
                    //case "SAuditOff":
                    //    customer.Audit = false;
                    //    customer.MOESwitchDate = DateTime.UtcNow;
                    //    break;

                    case "SMOEOff":
                        customer.MOE = false;
                        customer.MOESwitchDate = DateTime.UtcNow;
                        break;
                }
            }
            customerStore.Update(customer);
        }

        public void ImportVendorDetails(IEnumerable<Vendor> vendorList)
        {
            foreach (var vendor in vendorList)
            {
                if (entityStore.All.Any(r => r.Number == vendor.Number))
                {
                    UpdateVendor(vendor);
                }
                else
                {
                    AddVendor(vendor);
                }
            }
        }

        public void AddVendors(IReadOnlyList<Vendor> vendors)
        {
            foreach (var vendor in vendors)
            {
                entityStore.Insert(vendor);
            }
        }

        public string GetVendorNumber(int vendorId)
        {
            if (vendorId <= default(int)) { throw new ArgumentOutOfRangeException("vendorId"); }
            string number = string.Empty;
            number = entityStore.FindById(vendorId).Number;
            return number;
        }

        public int? GetVendorId(string vendorNumber)
        {
            var registrantBoundedContext = context as RegistrantBoundedContext;

            var vendor = registrantBoundedContext.Vendors.SingleOrDefault(i => i.Number == vendorNumber);

            if (vendor != null) return vendor.ID;
            else return null;
        }

        public IReadOnlyList<Address> GetVendorAddressList(int vendorId)
        {
            if (vendorId <= default(int)) { throw new ArgumentOutOfRangeException("vendorId"); }
            var registrantBoundedContext = context as RegistrantBoundedContext;
            return registrantBoundedContext.Vendors.AsNoTracking()
                .FirstOrDefault(c => c.ID == vendorId).VendorAddresses.ToList();
        }

        public Address GetVendorAddressByType(int vendorId, int typeId)
        {
            if (vendorId <= default(int)) { throw new ArgumentOutOfRangeException("vendorId"); }
            if (typeId <= default(int)) { throw new ArgumentNullException("type"); }

            var registrantBoundedContext = context as RegistrantBoundedContext;
            var vendorAddressList = registrantBoundedContext.Vendors.AsNoTracking()
                .FirstOrDefault(c => c.ID == vendorId).VendorAddresses;
            if (vendorAddressList.Any(r => r.AddressType == typeId))
            {
                return vendorAddressList.FirstOrDefault(r => r.AddressType == typeId);
            }
            else
                return null;
        }

        public List<Item> GetVendorItems(int vendorId)
        {
            return entityStore.FindById(vendorId).Items.ToList();
        }
        public IList<Vendor> GetUpdatedVendorList(DateTime lastUpdated)
        {
            return entityStore.All.AsNoTracking().Where(r => r.LastUpdatedDate > lastUpdated).OrderBy(r => r.LastUpdatedDate).Take(500).ToList();
        }

        public long GetLastAddedVendorNumber()
        {
            var numbers = new List<long>();
            return entityStore.All.Max(r => r.Number).FirstOrDefault();
        }

        #region BankInformation

        public void AddBankInformation(BankInformation bankInfo)
        {
            var dbContext = context as RegistrantBoundedContext;

            dbContext.BanksInformations.Add(bankInfo);
            dbContext.SaveChanges();
        }

        public void RemoveBankInformation(BankInformation bankInfo)
        {
            var dbContext = context as RegistrantBoundedContext;

            dbContext.BanksInformations.Remove(bankInfo);
            dbContext.SaveChanges();
        }
        public void UpdateVendorContacts(List<ContactAddressDto> contactAddresses, int vendorID)
        {
            var dbContext = context as RegistrantBoundedContext;
            foreach (var row in contactAddresses)
            {
                var address = row.Address;
                if (row.Address.ID <= 0)
                {
                    var vendor = dbContext.Vendors.FirstOrDefault(i => i.ID == vendorID);
                    vendor.VendorAddresses.Add(address);
                    dbContext.Addresses.Add(address);
                    dbContext.SaveChanges();//to generate address.ID
                }
                var contact = dbContext.Contacts.FirstOrDefault(i => i.ID == row.Contact.ID);
                if (contact != null)
                {
                    contact.AddressID = address.ID;
                    dbContext.SaveChanges();
                }
            }
        }

        public void UpdateCustomerContacts(List<ContactAddressDto> contactAddresses, int customerID)
        {
            var dbContext = context as RegistrantBoundedContext;
            foreach (var row in contactAddresses)
            {
                var address = row.Address;
                if (row.Address.ID <= 0)
                {
                    var customer = dbContext.Customers.FirstOrDefault(i => i.ID == customerID);
                    customer.CustomerAddresses.Add(address);
                    dbContext.Addresses.Add(address);
                    dbContext.SaveChanges();
                }
                var contact = dbContext.Contacts.FirstOrDefault(i => i.ID == row.Contact.ID);
                if (contact != null)
                {
                    contact.AddressID = address.ID;
                    dbContext.SaveChanges();
                }
            }
        }

        public void RemoveAddresses(List<Address> addresses)
        {
            if ((addresses == null) || (addresses.Count == 0))
            {
                return;
            }
            //make sure not deleting any business addresses
            addresses = addresses.Where(i => i.AddressType != (int)AddressTypeEnum.Business).ToList();

            var dbContext = context as RegistrantBoundedContext;
            dbContext.Addresses.RemoveRange(addresses);
            dbContext.SaveChanges();
        }
        public void UpdateAddressGPS(int addressID, decimal lat, decimal lng)
        {
            var dbContext = context as RegistrantBoundedContext;
            var address = dbContext.Addresses.Find(addressID);
            if (address != null)
            {
                address.GPSCoordinate1 = lat;
                address.GPSCoordinate2 = lng;
                dbContext.SaveChanges();
            }
        }
        //OTSTM2-50
        public void RemoveContacts(List<Contact> contacts)
        {
            if ((contacts == null) || (contacts.Count == 0))
            {
                return;
            }
            var dbContext = context as RegistrantBoundedContext;
            dbContext.Contacts.RemoveRange(contacts);
            dbContext.SaveChanges();
        }

        public void createBankInformation(int vendorId, BankInformation bankInformation)
        {
            if (string.IsNullOrWhiteSpace(bankInformation.Email))
            {
                var vendor = entityStore.FindById(vendorId);
                var primaryContact = vendor.VendorAddresses.FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                if (primaryContact != null)
                {
                    bankInformation.Email = primaryContact.Email;
                }
            }

            string key = Token.GenerateRandomSecretKey();
            var registrantDbContext = (this.context as RegistrantBoundedContext);
            var bankInformationModel = registrantDbContext.BanksInformations.FirstOrDefault(c => c.ID == bankInformation.ID);
            bankInformationModel.BankName = bankInformation.BankName;
            bankInformationModel.Key = key;
            bankInformationModel.TransitNumber = Token.Encrypt(bankInformation.TransitNumber, key);
            bankInformationModel.BankNumber = Token.Encrypt(bankInformation.BankNumber, key);
            bankInformationModel.AccountNumber = Token.Encrypt(bankInformation.AccountNumber, key);
            bankInformationModel.Email = bankInformation.Email;
            bankInformationModel.CCEmail = bankInformation.CCEmail;
            bankInformationModel.EmailPaymentTransferNotification = bankInformation.EmailPaymentTransferNotification;
            bankInformationModel.NotifyToPrimaryContactEmail = bankInformation.NotifyToPrimaryContactEmail;
            registrantDbContext.SaveChanges();
        }

        public void UpdateBankInformation(BankInformation bankInformation)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var loadbankinfoData = dbContext.BanksInformations.FirstOrDefault(c => c.ID == bankInformation.ID);
            string key = Token.GenerateRandomSecretKey();
            loadbankinfoData.Key = key;
            loadbankinfoData.TransitNumber = Token.Encrypt(bankInformation.TransitNumber, key);
            loadbankinfoData.BankNumber = Token.Encrypt(bankInformation.BankNumber, key);
            loadbankinfoData.AccountNumber = Token.Encrypt(bankInformation.AccountNumber, key);
            loadbankinfoData.BankName = bankInformation.BankName;
            loadbankinfoData.CCEmail = bankInformation.CCEmail;
            loadbankinfoData.Email = bankInformation.Email;
            loadbankinfoData.ReviewStatus = bankInformation.ReviewStatus;

            dbContext.SaveChanges();
        }

        public BankInformation GetBankInformation(int vendorId)
        {
            var bankingInfo = entityStore.All.FirstOrDefault(c => c.ID == vendorId).BankInformations.FirstOrDefault();

            if (bankingInfo != null)
            {
                bankingInfo.BankNumber = Token.Decrypt(bankingInfo.BankNumber, bankingInfo.Key);
                bankingInfo.TransitNumber = Token.Decrypt(bankingInfo.TransitNumber, bankingInfo.Key);
                bankingInfo.AccountNumber = Token.Decrypt(bankingInfo.AccountNumber, bankingInfo.Key);
            }
            return bankingInfo;
        }

        public BankInformation GetBankInformationById(int bankId)
        {
            var registrantDbContext = (this.context as RegistrantBoundedContext);
            var bankingInfo = registrantDbContext.BanksInformations.FirstOrDefault(c => c.ID == bankId);
            if (bankingInfo != null)
            {
                bankingInfo.BankNumber = Token.Decrypt(bankingInfo.BankNumber, bankingInfo.Key);
                bankingInfo.TransitNumber = Token.Decrypt(bankingInfo.TransitNumber, bankingInfo.Key);
                bankingInfo.AccountNumber = Token.Decrypt(bankingInfo.AccountNumber, bankingInfo.Key);
            }
            var vendor = registrantDbContext.Vendors.FirstOrDefault(i => i.ID == bankingInfo.VendorId);
            if (vendor.VendorSupportingDocuments.Count > 0)
            {
                bankingInfo.SupportDocOption = vendor.VendorSupportingDocuments.Where(i => i.TypeID == (int)SupportingDocumentTypeEnum.CHQ).FirstOrDefault().Option;
            }

            if (bankingInfo.NotifyToPrimaryContactEmail)
            {
                //Find primary contact
                Contact primaryContact = null;
                var contractAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 3);
                if (contractAddress != null)
                {
                    primaryContact = contractAddress.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                }
                if (primaryContact == null)
                {
                    var businessAddess = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                    if (businessAddess != null)
                    {
                        primaryContact = businessAddess.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                    }
                }

                if (primaryContact != null)
                {
                    bankingInfo.Email = primaryContact.Email;
                }
            }
            return bankingInfo;
        }
        #endregion

        //public int GetVendorRegistrationNumber(int vendorTypeID)
        //{
        //    int RegNumber = 0;
        //    using (var conn = new SqlConnection(context.Database.Connection.ConnectionString))
        //    {
        //        using (var cmd = conn.CreateCommand())
        //        {
        //            cmd.CommandText = "sp_getMaxRegNumByVendorType";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add(new SqlParameter("@vendorTypeID", vendorTypeID));

        //            SqlParameter retval = new SqlParameter("@maxNum", SqlDbType.Int);
        //            retval.Direction = ParameterDirection.Output;
        //            cmd.Parameters.Add(retval);

        //            if (conn.State != ConnectionState.Open)
        //                conn.Open();
        //            cmd.ExecuteNonQuery();

        //            string outputvalue = cmd.Parameters["@maxNum"].Value.ToString();
        //            RegNumber = Int32.Parse(outputvalue);
        //        }
        //    }
        //    return RegNumber;
        //}

        public int GetCustomerRegistrationNumber()
        {
            return (context as RegistrantBoundedContext).SpGetMaxRegNumForCustomer();
        }

        public int GetVendorRegistrationNumber(int vendorTypeID)
        {
            return (context as RegistrantBoundedContext).SpGetMaxRegNumByVendorType(vendorTypeID);
        }

        public List<Item> GetDefaultItems(int itemCategoryNum)
        {
            return (context as RegistrantBoundedContext).Items.Where(c => c.ItemCategory == (int)itemCategoryNum).ToList();
        }
        public List<Item> GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum participantTypeID)
        {
            return (context as RegistrantBoundedContext).Items.Where(x => x.ItemParticipantTypes.Any(y => y.ParticipantTypeId == (int)participantTypeID)).ToList();
        }

        public List<ItemParticipantType> LoadAllItemParticipantTypes()
        {
            return (context as RegistrantBoundedContext).ItemParticipantTypes.ToList();
        }

        public IList<Vendor> GetVendorsByApplicationId(int applicationId)
        {
            return entityStore.All.AsNoTracking()
                .Where(v => v.ApplicationID == applicationId).ToList();
        }

        public IList<Customer> GetCustomersByApplicationId(int applicationId)
        {
            return customerStore.All.AsNoTracking()
                .Where(v => v.ApplicationID == applicationId).ToList();
        }

        private VendorReference FindVendor(string number)
        {
            //Find the Effective Group for current vendor
            var registrantBoundedContext = context as RegistrantBoundedContext;
            var vendorTmp = registrantBoundedContext.Vendors.FirstOrDefault(v => v.Number == number);
            var today = DateTime.Today;
            var rateGroupId = 0;
            RateTransaction rt = registrantBoundedContext.RateTransactions.FirstOrDefault(c => c.Category == 3 && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today);
            if (vendorTmp != null && rt != null)
            {
                switch (vendorTmp.VendorType)
                {
                    case 2:
                        rateGroupId = (int)(rt.PickupRateGroupId ?? 0);
                        break;
                    case 4:
                        rateGroupId = (int)(rt.DeliveryRateGroupId ?? 0);
                        break;
                }
            }

            if (rateGroupId == 0)
            {
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  where vendor.Number == number
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = null
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in (context as RegistrantBoundedContext).Customers.AsNoTracking()
                                        where customer.RegistrationNumber == number
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
                return result;
            }
            else
            {
                var groupName = registrantBoundedContext.VendorRateGroups.FirstOrDefault(r => r.RateGroupId == rateGroupId && r.VendorId == vendorTmp.ID).Group.GroupName;
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  where vendor.Number == number
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = groupName
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in (context as RegistrantBoundedContext).Customers.AsNoTracking()
                                        where customer.RegistrationNumber == number
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
            //set vendor service threshold
            setVendorThreshold(result, registrantBoundedContext);
                return result;
            }
        }

        public VendorReference GetVendorByNumber(string number)
        {
            var result = FindVendor(number);

            if (result != null)
            {
                result.PrimaryContact = RepositoryHelper.FindPrimaryContact(result.VendorAddresses);

                if (result.VendorType != 1)
                {
                    if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                        result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                }

                //var invitation = (context as RegistrantBoundedContext).Invitations.FirstOrDefault(c => c.VendorId == result.VendorId || c.CustomerId == result.VendorId);
                //if (invitation != null) result.RedirectUrl = invitation.RedirectUrl;

                if (result.ActivationDate == null)
                {
                    var activationRec = vendorActiveHistoryStore.All.Where(c => c.VendorID == result.VendorId).OrderBy(c => c.ActiveStateChangeDate).FirstOrDefault();
                    if (activationRec != null) result.ActivationDate = activationRec.ActiveStateChangeDate;
                }
            }

            return result;
        }
        public VendorReference GetVendorByNumber(string number, long userId, string userName)
        {
            var result = FindVendor(number, userId);

            if (result != null)
            {
                result.PrimaryContact = RepositoryHelper.FindPrimaryContact(result.VendorAddresses);

                //OTSTM2-239 check generator drop down
                //Collector definition in TypeDefinition is 2
                if (result.VendorType.Equals(2))
                {
                    if (!string.Equals(result.PrimaryBusinessActivity, "Generator") && (result.BankInformations == null || result.BankInformations.Count == 0))
                    {
                        result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                    }
                }
                else if (!result.VendorType.Equals(1))
                {
                    if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                        result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                }

                //var invitation = (context as RegistrantBoundedContext).Invitations.FirstOrDefault(c => c.VendorId == result.VendorId || c.CustomerId == result.VendorId);
                //if (invitation != null) result.RedirectUrl = invitation.RedirectUrl;

                if (result.ActivationDate == null)
                {
                    var activationRec = vendorActiveHistoryStore.All.Where(c => c.VendorID == result.VendorId).OrderBy(c => c.ActiveStateChangeDate).FirstOrDefault();
                    if (activationRec != null) result.ActivationDate = activationRec.ActiveStateChangeDate;
                }
            }

            return result;

        }

        private VendorReference FindVendor(string number, long userId)
        {
            var registrantBoundedContext = context as RegistrantBoundedContext;

            //Find the Effective Group for current vendor
            var vendorTmp = registrantBoundedContext.Vendors.FirstOrDefault(v => v.Number == number);
            var today = DateTime.Today;
            var rateGroupId = 0;
            if (vendorTmp != null)
            {
                switch (vendorTmp.VendorType)
                {
                    case 2:
                        rateGroupId = (int)registrantBoundedContext.RateTransactions.FirstOrDefault(c => c.Category == 3 && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today).PickupRateGroupId;
                        break;
                    case 4:
                        rateGroupId = (int)registrantBoundedContext.RateTransactions.FirstOrDefault(c => c.Category == 3 && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today).DeliveryRateGroupId;
                        break;
                }
            }

            if (rateGroupId == 0)
            {
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  join vendorUser in registrantBoundedContext.VendorUsers
                                  on vendor.ID equals vendorUser.VendorID
                                  where vendor.Number == number && vendorUser.UserID == userId && vendorUser.IsActive
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = null
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in registrantBoundedContext.Customers.AsNoTracking()
                                        join customerUser in registrantBoundedContext.CustomerUsers
                                        on customer.ID equals customerUser.CustomerId
                                        where customer.RegistrationNumber == number && customerUser.UserId == userId && customerUser.IsActive
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
            // set vendor service thresholds
            setVendorThreshold(result, registrantBoundedContext);
                return result;
            }
            else
            {
                var groupName = registrantBoundedContext.VendorRateGroups.FirstOrDefault(r => r.RateGroupId == rateGroupId && r.VendorId == vendorTmp.ID).Group.GroupName;
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  join vendorUser in registrantBoundedContext.VendorUsers
                                  on vendor.ID equals vendorUser.VendorID
                                  where vendor.Number == number && vendorUser.UserID == userId && vendorUser.IsActive
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = groupName
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in registrantBoundedContext.Customers.AsNoTracking()
                                        join customerUser in registrantBoundedContext.CustomerUsers
                                        on customer.ID equals customerUser.CustomerId
                                        where customer.RegistrationNumber == number && customerUser.UserId == userId && customerUser.IsActive
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
                return result;
            }
        }

        //private VendorReference FindVendor(string number, long userId)
        //{
        //    var registrantBoundedContext = context as RegistrantBoundedContext;
        //    //Search Vendor first
        //    var queryVendor = from vendor in entityStore.All.AsNoTracking()
        //                      join vendorUser in registrantBoundedContext.VendorUsers
        //                      on vendor.ID equals vendorUser.VendorID
        //                      where vendor.Number == number && vendorUser.UserID == userId && vendorUser.IsActive
        //                      select new VendorReference
        //                      {
        //                          VendorId = vendor.ID,
        //                          RegistrationNumber = vendor.Number,
        //                          BusinessName = vendor.BusinessName,
        //                          VendorType = vendor.VendorType,
        //                          IsActive = vendor.IsActive,
        //                          ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
        //                          ActivationDate = vendor.ActivationDate,
        //                          Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
        //                          VendorAddresses = vendor.VendorAddresses.ToList(),
        //                          IsVendor = true,
        //                          ApplicationID = vendor.ApplicationID,
        //                          CIsGenerator = vendor.CIsGenerator,
        //                          IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
        //                          OperatingName = vendor.OperatingName,
        //                          PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
        //                          CGeneratorDate = vendor.CGeneratorDate,
        //                          Status = vendor.RegistrantStatus,
        //                          CreatedDate = vendor.CreatedDate,
        //                          BankInformations = vendor.BankInformations
        //                      };
        //    var result = queryVendor.FirstOrDefault();

        //    if (result == null)
        //    {
        //        //Search customer 
        //        var queryCustomer = from customer in registrantBoundedContext.Customers.AsNoTracking()
        //                            join customerUser in registrantBoundedContext.CustomerUsers
        //                            on customer.ID equals customerUser.CustomerId
        //                            where customer.RegistrationNumber == number && customerUser.UserId == userId && customerUser.IsActive
        //                            select new VendorReference
        //                            {
        //                                VendorId = customer.ID,
        //                                RegistrationNumber = customer.RegistrationNumber,
        //                                BusinessName = customer.BusinessName,
        //                                VendorType = 1,
        //                                IsActive = customer.IsActive,
        //                                ActiveStatusChangeDate = customer.ActiveStateChangeDate,
        //                                ActivationDate = customer.ActivationDate,
        //                                Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
        //                                VendorAddresses = customer.CustomerAddresses.ToList(),
        //                                IsVendor = false,
        //                                ApplicationID = customer.ApplicationID,
        //                                CIsGenerator = null,
        //                                IsSubCollector = null, //OTSTM2-953
        //                                OperatingName = customer.OperatingName,
        //                                PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
        //                                CGeneratorDate = null,
        //                                Status = customer.RegistrantStatus,
        //                                CreatedDate = customer.CreatedDate
        //                            };
        //        result = queryCustomer.FirstOrDefault();
        //    }
        //    return result;
        //}

        public VendorReference GetVendorById(int vendorId)
        {
            //to do: find the Effective Group for current vendor
            var registrantBoundedContext = context as RegistrantBoundedContext;
            var vendorTmp = registrantBoundedContext.Vendors.FirstOrDefault(v => v.ID == vendorId);
            var today = DateTime.Today;
            var rateGroupId = 0;
            if (vendorTmp != null)
            {
                switch (vendorTmp.VendorType)
                {
                    case 2:
                        rateGroupId = (int)registrantBoundedContext.RateTransactions.FirstOrDefault(c => c.Category == 3 && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today).PickupRateGroupId;
                        break;
                    case 4:
                        rateGroupId = (int)registrantBoundedContext.RateTransactions.FirstOrDefault(c => c.Category == 3 && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today).DeliveryRateGroupId;
                        break;
                }
            }

            if (rateGroupId == 0)
            {
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  where vendor.ID == vendorId
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = null
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in (context as RegistrantBoundedContext).Customers.AsNoTracking()
                                        where customer.ID == vendorId
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
                if (result != null)
                {
                    result.PrimaryContact = RepositoryHelper.FindPrimaryContact(result.VendorAddresses);

                    //if (result.VendorType != 1)
                    //{
                    //    if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                    //        result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                    //}

                    //OTSTM2-239 check generator drop down
                    //Collector definition in TypeDefinition is 2
                    if (result.VendorType.Equals(2))
                    {
                        if (!string.Equals(result.PrimaryBusinessActivity, "Generator") && (result.BankInformations == null || result.BankInformations.Count == 0))
                        {
                            result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                        }
                    }
                    else if (!result.VendorType.Equals(1))
                    {
                        if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                            result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                    }

                    //var invitation = (context as RegistrantBoundedContext).Invitations.FirstOrDefault(c => c.VendorId == result.VendorId || c.CustomerId == result.VendorId);
                    //if (invitation != null) result.RedirectUrl = invitation.RedirectUrl;

                    if (result.ActivationDate == null)
                    {
                        var activationRec = vendorActiveHistoryStore.All.Where(c => c.VendorID == result.VendorId).OrderBy(c => c.ActiveStateChangeDate).FirstOrDefault();
                        if (activationRec != null) result.ActivationDate = activationRec.ActiveStateChangeDate;
                    }
                }

                return result;
            }
            else
            {
                var groupName = registrantBoundedContext.VendorRateGroups.FirstOrDefault(r => r.RateGroupId == rateGroupId && r.VendorId == vendorTmp.ID).Group.GroupName;
                //Search Vendor first
                var queryVendor = from vendor in entityStore.All.AsNoTracking()
                                  where vendor.ID == vendorId
                                  select new VendorReference
                                  {
                                      VendorId = vendor.ID,
                                      RegistrationNumber = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorType = vendor.VendorType,
                                      IsActive = vendor.IsActive,
                                      ActiveStatusChangeDate = vendor.ActiveStateChangeDate,
                                      ActivationDate = vendor.ActivationDate,
                                      Address = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1),
                                      VendorAddresses = vendor.VendorAddresses.ToList(),
                                      IsVendor = true,
                                      ApplicationID = vendor.ApplicationID,
                                      CIsGenerator = vendor.CIsGenerator,
                                      IsSubCollector = vendor.IsSubCollector, //OTSTM2-953
                                      OperatingName = vendor.OperatingName,
                                      PrimaryBusinessActivity = vendor.PrimaryBusinessActivity,
                                      CGeneratorDate = vendor.CGeneratorDate,
                                      Status = vendor.RegistrantStatus,
                                      CreatedDate = vendor.CreatedDate,
                                      BankInformations = vendor.BankInformations,
                                      GroupName = groupName
                                  };
                var result = queryVendor.FirstOrDefault();

                if (result == null)
                {
                    //Search customer 
                    var queryCustomer = from customer in (context as RegistrantBoundedContext).Customers.AsNoTracking()
                                        where customer.ID == vendorId
                                        select new VendorReference
                                        {
                                            VendorId = customer.ID,
                                            RegistrationNumber = customer.RegistrationNumber,
                                            BusinessName = customer.BusinessName,
                                            VendorType = 1,
                                            IsActive = customer.IsActive,
                                            ActiveStatusChangeDate = customer.ActiveStateChangeDate,
                                            ActivationDate = customer.ActivationDate,
                                            Address = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == 1),
                                            VendorAddresses = customer.CustomerAddresses.ToList(),
                                            IsVendor = false,
                                            ApplicationID = customer.ApplicationID,
                                            CIsGenerator = null,
                                            IsSubCollector = null, //OTSTM2-953
                                            OperatingName = customer.OperatingName,
                                            PrimaryBusinessActivity = customer.PrimaryBusinessActivity,
                                            CGeneratorDate = null,
                                            Status = customer.RegistrantStatus,
                                            CreatedDate = customer.CreatedDate,
                                            GroupName = null
                                        };
                    result = queryCustomer.FirstOrDefault();
                }
                if (result != null)
                {
                    result.PrimaryContact = RepositoryHelper.FindPrimaryContact(result.VendorAddresses);

                    //if (result.VendorType != 1)
                    //{
                    //    if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                    //        result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                    //}

                    //OTSTM2-239 check generator drop down
                    //Collector definition in TypeDefinition is 2
                    if (result.VendorType.Equals(2))
                    {
                        if (!string.Equals(result.PrimaryBusinessActivity, "Generator") && (result.BankInformations == null || result.BankInformations.Count == 0))
                        {
                            result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                        }

                    }
                    else if (!result.VendorType.Equals(1))
                    {
                        if ((result.BankInformations == null) || (result.BankInformations.Count == 0))
                            result.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", result.VendorId);
                    }

                    //var invitation = (context as RegistrantBoundedContext).Invitations.FirstOrDefault(c => c.VendorId == result.VendorId || c.CustomerId == result.VendorId);
                    //if (invitation != null) result.RedirectUrl = invitation.RedirectUrl;

                    if (result.ActivationDate == null)
                    {
                        var activationRec = vendorActiveHistoryStore.All.Where(c => c.VendorID == result.VendorId).OrderBy(c => c.ActiveStateChangeDate).FirstOrDefault();
                        if (activationRec != null) result.ActivationDate = activationRec.ActiveStateChangeDate;
                    }
                }
            //set vendor service threshold
            setVendorThreshold(result, registrantBoundedContext);
                return result;
            }

        }

        private void setVendorThreshold(VendorReference result, RegistrantBoundedContext registrantBoundedContext)
        {

            if (result != null)
            {
                //initial
                result.IsThresholdSet = false;
                result.ThresholdDays = 0;
                result.DaysSinceLastTCR = 0;

                if (result.VendorType.Equals(2))
                {
                    var vendorServiceThreshold = registrantBoundedContext.Sp_GetAllServiceThresholds().OrderByDescending(x=>x.ID).Where(x => x.VendorID == result.VendorId).FirstOrDefault();
                    if (vendorServiceThreshold != null)
                    {
                        result.IsThresholdSet = true;
                        result.ThresholdDays = vendorServiceThreshold.ThresholdDays;
                        result.DaysSinceLastTCR = vendorServiceThreshold.DaysSinceLastTCR??0;
                    }
                }
            }
        }


        public IEnumerable<SpGetRegistrantSpotlight> GetRegistrantSpotlights(string keyWords, string section, int? pageLimit)
        {
            return (context as RegistrantBoundedContext).SpGetRegistrantSpotlight(keyWords, section, pageLimit);
        }

        public IEnumerable<SpGetRegistrantSpotlightsPaginate> GetRegistrantSpotlightPaginate(string keyWords, string section, int start = 0, int pageLimit = 10)
        {
            return (context as RegistrantBoundedContext).SpGetRegistrantSpotlightsPaginate(keyWords, section, start, pageLimit);
        }
        public PaginationDTO<RegistrantSpotlightModel, int> LoadSpotlightViewMoreResult(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string keyWords, int pageLimit, List<TypeDefinition> GSModuleType, string section)
        {
            var sp_getRegistrantSpotlightsPaginate = (context as RegistrantBoundedContext).SpGetRegistrantSpotlightsPaginate(keyWords, section, 0, 1000000).ToList();

            var result = new List<RegistrantSpotlightModel>();

            result.AddRange(sp_getRegistrantSpotlightsPaginate.Select(i => new RegistrantSpotlightModel()
            {
                ID = i.ID,
                vendorID = i.vendorID,
                PlaceHolder1 = i.PlaceHolder1,
                PlaceHolder2 = i.PlaceHolder2,
                PlaceHolder3 = i.PlaceHolder3,
                PlaceHolder4 = i.PlaceHolder4,
                PlaceHolder5 = i.PlaceHolder5,
                ModuleType = GSModuleType.First(t => t.DefinitionValue == i.ModuleType).Code,
                Status = i.Status,
                TotalCount = i.TotalCount,
                PeriodStartDate = i.PeriodStartDate
            }));

            var query = result.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();

                //OTSTM2-960/963/964/965/966
                if (section == "Applications")
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || ("active".Contains(searchText) ? a.Status == "1" : false) || ("inactive".Contains(searchText) ? a.Status == "0" : false)
                                        || a.ModuleType.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }
                else if (section == "Users")
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || a.Status.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }
                else
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || a.Status.ToLower().Contains(searchText)
                                        || a.ModuleType.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }

            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;

            #region order
            switch (orderBy)
            {
                case "Status":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Status);
                    else
                        query = query.OrderByDescending(a => a.Status);
                    break;
                case "ModuleType":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.ModuleType);
                    else
                        query = query.OrderByDescending(a => a.ModuleType);
                    break;
                case "PlaceHolder1":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder1);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder1);
                    break;
                case "PlaceHolder2":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder2);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder2);
                    break;
                case "PlaceHolder3":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder3.Length).ThenBy(a => a.PlaceHolder3);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder3.Length).ThenByDescending(a => a.PlaceHolder3);
                    break;
                case "PlaceHolder4":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder4);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder4);
                    break;
                case "PlaceHolder5":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder5);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder5);
                    break;
                case "PeriodStartDate":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PeriodStartDate);
                    else
                        query = query.OrderByDescending(a => a.PeriodStartDate);
                    break;
            }

            #endregion

            var totalRecords = query.Count();
            var newResult = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RegistrantSpotlightModel, int>
            {
                DTOCollection = newResult,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        //OTSTM2-960/963/964/965/966
        public List<RegistrantSpotlightModel> LoadSpotlightResultExport(string searchText, string orderBy, string sortDirection, string keyWords, List<TypeDefinition> GSModuleType, string section)
        {
            var sp_getRegistrantSpotlightsPaginate = (context as RegistrantBoundedContext).SpGetRegistrantSpotlightsPaginate(keyWords, section, 0, 1000000).ToList();

            var result = new List<RegistrantSpotlightModel>();

            result.AddRange(sp_getRegistrantSpotlightsPaginate.Select(i => new RegistrantSpotlightModel()
            {
                ID = i.ID,
                vendorID = i.vendorID,
                PlaceHolder1 = i.PlaceHolder1,
                PlaceHolder2 = i.PlaceHolder2,
                PlaceHolder3 = i.PlaceHolder3,
                PlaceHolder4 = i.PlaceHolder4,
                PlaceHolder5 = i.PlaceHolder5,
                ModuleType = GSModuleType.First(t => t.DefinitionValue == i.ModuleType).Code,
                Status = i.Status,
                TotalCount = i.TotalCount,
                PeriodStartDate = i.PeriodStartDate
            }));

            var query = result.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                searchText = searchText.ToLower();

                if (section == "Applications")
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || ("active".Contains(searchText) ? a.Status == "1" : false) || ("inactive".Contains(searchText) ? a.Status == "0" : false)
                                        || a.ModuleType.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }
                else if (section == "Users")
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || a.Status.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }
                else
                {
                    query = query.Where(a => a.PlaceHolder1.ToLower().Contains(searchText)
                                        || a.Status.ToLower().Contains(searchText)
                                        || a.ModuleType.ToLower().Contains(searchText)
                                        || a.PlaceHolder2.ToLower().Contains(searchText)
                                        || a.PlaceHolder3.ToLower().Contains(searchText)
                                        || a.PlaceHolder4.ToLower().Contains(searchText)
                                        || a.PlaceHolder5.ToLower().Contains(searchText));
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "Status" : orderBy;

            #region order
            switch (orderBy)
            {
                case "Status":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.Status);
                    else
                        query = query.OrderByDescending(a => a.Status);
                    break;
                case "ModuleType":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.ModuleType);
                    else
                        query = query.OrderByDescending(a => a.ModuleType);
                    break;
                case "PlaceHolder1":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder1);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder1);
                    break;
                case "PlaceHolder2":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder2);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder2);
                    break;
                case "PlaceHolder3":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder3.Length).ThenBy(a => a.PlaceHolder3);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder3.Length).ThenByDescending(a => a.PlaceHolder3);
                    break;
                case "PlaceHolder4":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder4);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder4);
                    break;
                case "PlaceHolder5":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PlaceHolder5);
                    else
                        query = query.OrderByDescending(a => a.PlaceHolder5);
                    break;
                case "PeriodStartDate":
                    if (sortDirection == "asc")
                        query = query.OrderBy(a => a.PeriodStartDate);
                    else
                        query = query.OrderByDescending(a => a.PeriodStartDate);
                    break;
            }

            #endregion
            return query.ToList();
        }

        public void AddCustomer(Customer customer)
        {
            customerStore.Insert(customer);
        }

        public void UpdateVendorActive(Vendor vendor)
        {
            var currentContext = (context as RegistrantBoundedContext);
            currentContext.Entry(vendor).State = EntityState.Detached;
            using (var registrantContext = new RegistrantBoundedContext())
            {
                registrantContext.Vendors.Attach(vendor);
                var entry = registrantContext.Entry(vendor);
                entry.State = EntityState.Unchanged;
                entry.Property(c => c.IsActive).IsModified = true;
                entry.Property(c => c.ActiveStateChangeDate).IsModified = true;
                registrantContext.SaveChanges();
            }
        }

        public void UpdateCustomerAcitve(Customer customer)
        {
            var currentContext = (context as RegistrantBoundedContext);
            currentContext.Entry(customer).State = EntityState.Detached;

            using (var registrantContext = new RegistrantBoundedContext())
            {
                registrantContext.Customers.Attach(customer);
                var entry = registrantContext.Entry(customer);
                entry.State = EntityState.Unchanged;
                entry.Property(c => c.IsActive).IsModified = true;
                entry.Property(c => c.ActiveStateChangeDate).IsModified = true;
                registrantContext.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registrationNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>True if the registrant was not active the entire time between from and to</returns>
        public bool IsNotActive(int vendorID, DateTime transactionDate)
        {
            var vendorActiveDateRanges = GetVendorStatusDateRanges(vendorID);
            var date = transactionDate.Date;
            var item = vendorActiveDateRanges.FirstOrDefault(c =>
                c.StartDate <= date && c.EndDate >= date
            );
            if (item != null)
            {
                return !item.isActive;
            }
            else
            {
                return true;
            }
        }

        public IEnumerable<VendorStatusDateRangeModel> GetVendorStatusDateRanges(int vendorId)
        {
            var dbContext = context as RegistrantBoundedContext;
            var vendorActiveHistories = dbContext.VendorActiveHistories.Where(c => c.VendorID == vendorId && c.IsValid).ToList();

            var activeList = vendorActiveHistories.Where(c => c.ActiveState).OrderBy(c => c.ActiveStateChangeDate).ToList();
            var inactiveList = vendorActiveHistories.Where(c => !c.ActiveState).OrderBy(c => c.ActiveStateChangeDate).ToList();

            var statusRangeList = new List<VendorStatusDateRangeModel>();

            //Build active date range list
            activeList.ForEach(c =>
            {
                var vendorStatusDateRangeModel = new VendorStatusDateRangeModel();
                var startDate = c.ActiveStateChangeDate.Date;
                vendorStatusDateRangeModel.StartDate = startDate;
                vendorStatusDateRangeModel.isActive = true;
                //Find end date
                var inactiveItem = inactiveList.Where(i => i.ActiveStateChangeDate.Date > startDate).OrderBy(i => i.ActiveStateChangeDate).FirstOrDefault();
                if (inactiveItem != null)
                {
                    var endDate = inactiveItem.ActiveStateChangeDate.Date.AddDays(-1);
                    vendorStatusDateRangeModel.EndDate = endDate;
                }
                else
                {
                    vendorStatusDateRangeModel.EndDate = DateTime.MaxValue.Date;
                }

                statusRangeList.Add(vendorStatusDateRangeModel);
            });

            //Build inactive date range list
            inactiveList.ForEach(c =>
            {
                var vendorStatusDateRangeModel = new VendorStatusDateRangeModel();
                var startDate = c.ActiveStateChangeDate.Date;
                vendorStatusDateRangeModel.StartDate = startDate;
                vendorStatusDateRangeModel.isActive = false;
                //Find end date
                var activeItem = activeList.Where(i => i.ActiveStateChangeDate.Date > startDate).OrderBy(i => i.ActiveStateChangeDate).FirstOrDefault();
                if (activeItem != null)
                {
                    var endDate = activeItem.ActiveStateChangeDate.Date.AddDays(-1);
                    vendorStatusDateRangeModel.EndDate = endDate;
                }
                else
                {
                    vendorStatusDateRangeModel.EndDate = DateTime.MaxValue.Date;
                }

                statusRangeList.Add(vendorStatusDateRangeModel);
            });

            return statusRangeList.OrderBy(c => c.StartDate).ToList();
        }

        public List<InactiveDateRangeModel> GetVendorInactiveDateRanges(int vendorId)
        {
            var result = new List<InactiveDateRangeModel>();
            var vendorStatusDateRanges = GetVendorStatusDateRanges(vendorId).Where(i => !i.isActive).ToList();

            vendorStatusDateRanges.ForEach(c =>
            {
                var startDate = c.StartDate.Date;
                var endDate = c.EndDate.Date;
                result.Add(new InactiveDateRangeModel { StartDate = startDate, EndDate = endDate });
            });

            return result;
        }

        public bool IsVendorActive(int vendorId, DateTime on)
        {
            var vendorStatusDateRanges = GetVendorStatusDateRanges(vendorId);

            var date = on.Date;
            var item = vendorStatusDateRanges.FirstOrDefault(c => c.StartDate <= date && c.EndDate >= date);

            if (item != null) return item.isActive;
            else return false;
        }

        private string GetKeyWords(Vendor vendor)
        {
            var keyWords = new List<string>();
            keyWords.Add(string.Format("{0}//{1}//{2}//", vendor.BusinessName, vendor.OperatingName, vendor.Number));
            UpdateKeywords(vendor.VendorAddresses, ref keyWords);
            return string.Join("//", keyWords);
        }

        private string GetKeyWords(Customer customer)
        {
            var keyWords = new List<string>();
            keyWords.Add(string.Format("{0}//{1}//{2}//", customer.BusinessName, customer.OperatingName, customer.RegistrationNumber));
            UpdateKeywords(customer.CustomerAddresses, ref keyWords);
            return string.Join("//", keyWords);
        }

        private void UpdateKeywords(ICollection<Address> addresses, ref List<string> keyWords)
        {
            var validAddresses = addresses.Where(i => i.AddressType != (int)AddressTypeEnum.Sortyard).ToList();
            foreach (var address in validAddresses)
            {
                keyWords.Add(string.Format("{0}//{1}//{2}//{3}//{4}//{5}//{6}//{7}//", address.Address1, address.Address2, address.City, address.Province, address.PostalCode, address.Country, address.Email, address.Ext));
                if (address.Contacts != null)
                {
                    foreach (var contact in address.Contacts)
                    {
                        keyWords.Add(string.Format("{0}//{1}//{2}//{3}//{4}//{5}//{6}//", contact.FirstName, contact.LastName, contact.Position, contact.PhoneNumber, contact.Ext, contact.AlternatePhoneNumber, contact.Email));
                    }
                }
            }
        }


        #region VendorAttachment
        public void AddVendorAttachments(int vendorId, IEnumerable<AttachmentModel> attachments)
        {
            ConditionCheck.Bounds(vendorId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);

            foreach (var attachmentModel in attachments)
            {
                var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
                dbContext.Attachments.Add(attachment);
                var vendorAttachment = new VendorAttachment();
                vendorAttachment.VendorId = vendorId;
                vendorAttachment.AttachmentId = attachment.ID;
                dbContext.VendorAttachments.Add(vendorAttachment);
            }

            dbContext.SaveChanges();
        }
        public void UpdateVendorForGpBatch(List<int> vendorIds)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var query = dbContext.Vendors.Where(c => vendorIds.Contains(c.ID));
            query.ToList().ForEach(c =>
            {
                c.InBatch = true;
                c.GpUpdate = false;
            });
            dbContext.SaveChanges();
        }
        public void UpdateCustomerForGpBatch(List<int> customerIds)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var query = dbContext.Customers.Where(c => customerIds.Contains(c.ID));
            query.ToList().ForEach(c =>
                {
                    c.InBatch = true;
                    c.GpUpdate = false;
                });
            dbContext.SaveChanges();
        }
        public IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo = false)
        {
            ConditionCheck.Bounds(vendorId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);
            var attachments = dbContext.VendorAttachments.AsNoTracking().Where(c => c.VendorId == vendorId).Select(c => c.Attachment).ToList();

            var attachmentsBank = attachments.Where(a => a.IsBankingRelated);
            if (bBankInfo)
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachmentsBank);
                return attachmentModelList;
            }
            else
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachments);
                return attachmentModelList;
            }

        }

        public AttachmentModel GetVendorAttachment(int vendorId, string fileUniqueName)
        {
            ConditionCheck.Bounds(vendorId, "Argument is out of bounds", int.MaxValue, 0);
            var dbContext = (context as RegistrantBoundedContext);
            var attachment = dbContext.VendorAttachments.Where(c => c.VendorId == vendorId).Select(c => c.Attachment).FirstOrDefault(c => c.UniqueName == fileUniqueName);

            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveVendorAttachment(int vendorId, string fileUniqueName)
        {
            ConditionCheck.Bounds(vendorId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);
            var attachment = dbContext.VendorAttachments.Where(c => c.VendorId == vendorId).Select(c => c.Attachment).FirstOrDefault(c => c.UniqueName == fileUniqueName);
            if (attachment != null)
            {
                attachment.FileName = "DELETE";
                var vendorAttachment = dbContext.VendorAttachments.FirstOrDefault(c => c.VendorId == vendorId && c.AttachmentId == attachment.ID);
                if (vendorAttachment != null)
                {
                    dbContext.VendorAttachments.Remove(vendorAttachment);
                }
            }

            dbContext.SaveChanges();
        }
        public void RemoveVendorBankinfoAttachment(int vendorId)
        {
            ConditionCheck.Bounds(vendorId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);
            var attachments = dbContext.VendorAttachments.AsNoTracking().Where(c => c.VendorId == vendorId).Select(c => c.Attachment).ToList();

            var attachmentsBank = attachments.Where(a => a.IsBankingRelated).ToList();

            if ((attachmentsBank != null) && (attachmentsBank.Count > 0))
            {
                attachmentsBank.ForEach(attachmentFile =>
                    {
                        attachmentFile.FileName = "DELETE";
                        var vendorAttachment = dbContext.VendorAttachments.FirstOrDefault(c => c.AttachmentId == attachmentFile.ID);
                        if (vendorAttachment != null)
                        {
                            dbContext.VendorAttachments.Remove(vendorAttachment);
                        }
                    });
                dbContext.SaveChanges();
            }
        }

        public void AddVendorAttachments(List<VendorAttachment> vendorAttachments)
        {
            var dbContext = (context as RegistrantBoundedContext);
            dbContext.VendorAttachments.AddRange(vendorAttachments);
            dbContext.SaveChanges();
        }

        #endregion

        #region Customer Attachment

        public void AddCustomerAttachments(int customerId, IEnumerable<AttachmentModel> attachments)
        {
            ConditionCheck.Bounds(customerId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);

            foreach (var attachmentModel in attachments)
            {
                var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
                dbContext.Attachments.Add(attachment);
                var customerAttachment = new CustomerAttachment();
                customerAttachment.CustomerId = customerId;
                customerAttachment.AttachmentId = attachment.ID;
                dbContext.CustomerAttachments.Add(customerAttachment);
            }

            dbContext.SaveChanges();
        }

        public IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo = false)
        {
            ConditionCheck.Bounds(customerId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);
            var attachments = dbContext.CustomerAttachments.AsNoTracking().Where(c => c.CustomerId == customerId).Select(c => c.Attachment).ToList();

            var attachmentsBank = attachments.Where(a => a.IsBankingRelated);
            if (bBankInfo)
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachmentsBank);
                return attachmentModelList;
            }
            else
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachments);
                return attachmentModelList;
            }

        }

        public AttachmentModel GetCustomerAttachment(int customerId, string fileUniqueName)
        {
            ConditionCheck.Bounds(customerId, "Argument is out of bounds", int.MaxValue, 0);
            var dbContext = (context as RegistrantBoundedContext);
            var attachment = dbContext.CustomerAttachments.Where(c => c.CustomerId == customerId).Select(c => c.Attachment).FirstOrDefault(c => c.UniqueName == fileUniqueName);

            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveCustomerAttachment(int customerId, string fileUniqueName)
        {
            ConditionCheck.Bounds(customerId, "Argument is out of bounds", int.MaxValue, 0);

            var dbContext = (context as RegistrantBoundedContext);
            var attachment = dbContext.CustomerAttachments.Where(c => c.CustomerId == customerId).Select(c => c.Attachment).FirstOrDefault(c => c.UniqueName == fileUniqueName);
            if (attachment != null)
            {
                attachment.FileName = "DELETE";
                var customerAttachment = dbContext.CustomerAttachments.FirstOrDefault(c => c.CustomerId == customerId && c.AttachmentId == attachment.ID);
                if (customerAttachment != null)
                {
                    dbContext.CustomerAttachments.Remove(customerAttachment);
                }
            }

            dbContext.SaveChanges();
        }

        private void RemoveContact(int contactId)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var contact = dbContext.Contacts.FirstOrDefault(c => c.ID == contactId);
            if (contact != null)
            {
                dbContext.Contacts.Remove(contact);
                dbContext.SaveChanges();
            }
        }


        public void AddCustomerAttachments(List<CustomerAttachment> customerAttachments)
        {
            var dbContext = (context as RegistrantBoundedContext);
            dbContext.CustomerAttachments.AddRange(customerAttachments);
            dbContext.SaveChanges();
        }

        public bool ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var now = DateTime.Now.Date;
            var tomorrow = DateTime.Now.AddDays(1).Date;

            //Make all past record invalid between active state change date to now
            if (vendorActiveHistory.ActiveStateChangeDate.Date <= now)
            {
                var activeHistoryDate = vendorActiveHistory.ActiveStateChangeDate.AddDays(-1).Date;
                var invalidItems = dbContext.VendorActiveHistories.Where(c => c.VendorID == vendorActiveHistory.VendorID && c.ActiveStateChangeDate > activeHistoryDate && c.ActiveStateChangeDate < tomorrow).ToList();
                invalidItems.ForEach(c =>
                {
                    c.IsValid = false;
                });
            }

            //Make last same type record invalid if it in the future date
            var lastSameActiveHistory = dbContext.VendorActiveHistories.Where(c => c.VendorID == vendorActiveHistory.VendorID && c.ActiveState == vendorActiveHistory.ActiveState).OrderByDescending(c => c.CreateDate).FirstOrDefault();
            if (lastSameActiveHistory != null)
            {
                if (lastSameActiveHistory.ActiveStateChangeDate.Date > now && vendorActiveHistory.ActiveStateChangeDate.Date >= now)
                {
                    lastSameActiveHistory.IsValid = false;
                }
            }

            var currentUser = TMSContext.TMSPrincipal.Identity;
            vendorActiveHistory.CreatedBy = currentUser.Name;

            //Add the current vendor activity record
            dbContext.VendorActiveHistories.Add(vendorActiveHistory);
            var vendor = dbContext.Vendors.SingleOrDefault(v => v.ID == vendorActiveHistory.VendorID);
            if (vendor != null)
                vendor.GpUpdate = true;
            dbContext.SaveChanges();

            if ((vendor != null) && ((vendor.VendorType == (int)ClaimType.RPM) || (vendor.VendorType == (int)ClaimType.Processor)) && (!vendorActiveHistory.ActiveState) &&
                ((vendorActiveHistory.Reason == "Change in Ownership") || (vendorActiveHistory.Reason == "Change in Legal Name/HST") || (vendorActiveHistory.Reason == "Opted out of the Program")))
            {
                return true; //need to Initialize 3 Claims For Processor or RMP user
            }
            else
            {
                return false;
            }
        }

        public VendorActiveHistory LoadCurrenActiveHistoryForVendor(int vendorId)
        {
            //Get current vendor active state
            using (var dbContext = new RegistrantBoundedContext())
            {
                var user = TMSContext.TMSPrincipal.Identity;
                var updatedBy = user.Name;

                var now = DateTime.Now.AddDays(1).Date;
                var currentActiveHistory = dbContext.VendorActiveHistories.Where(c => c.VendorID == vendorId && c.IsValid && c.ActiveStateChangeDate < now).OrderByDescending(c => c.ActiveStateChangeDate).FirstOrDefault();
                var vendor = dbContext.Vendors.Where(c => c.ID == vendorId).FirstOrDefault();
                //Fixed banking issue
                if (vendor.IsActive != currentActiveHistory.ActiveState)
                {
                    vendor.IsActive = currentActiveHistory.ActiveState;
                    vendor.ActiveStateChangeDate = currentActiveHistory.ActiveStateChangeDate;
                    vendor.LastUpdatedDate = DateTime.UtcNow;
                    vendor.LastUpdatedBy = updatedBy;
                    dbContext.SaveChanges();

                    //Trigger to initialize a new claim when vendor is changed from inactive to active
                    if (vendor.IsActive)
                        currentActiveHistory.ChangeToActive = true;
                }
                return currentActiveHistory;
            }
        }

        public VendorActiveHistory LoadPreviousActiveHistoryForVendor(int vendorId, bool isActive)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var currentActiveHistory = dbContext.VendorActiveHistories.Where(c => c.VendorID == vendorId && c.ActiveState == isActive && c.IsValid).OrderByDescending(c => c.ActiveStateChangeDate).FirstOrDefault();
            return currentActiveHistory;
        }

        public void ActiveInactiveCustomer(CustomerActiveHistory customerActiveHistory)
        {
            var dbContext = (context as RegistrantBoundedContext);

            var now = DateTime.Now.Date;
            var tomorrow = DateTime.Now.AddDays(1).Date;

            //Make all past record invalid between active state change date to now
            if (customerActiveHistory.ActiveStateChangeDate.Date <= now)
            {
                var activeHistoryDate = customerActiveHistory.ActiveStateChangeDate.AddDays(-1).Date;
                var invalidItems = dbContext.CustomerActiveHistories.Where(c => c.CustomerID == customerActiveHistory.CustomerID && c.ActiveStateChangeDate > activeHistoryDate && c.ActiveStateChangeDate < tomorrow).ToList();
                invalidItems.ForEach(c =>
                {
                    c.IsValid = false;
                });
            }


            //Make last same type record invalid if it in the future date
            var lastSameActiveHistory = dbContext.CustomerActiveHistories.Where(c => c.CustomerID == customerActiveHistory.CustomerID && c.ActiveState == customerActiveHistory.ActiveState).OrderByDescending(c => c.CreateDate).FirstOrDefault();
            if (lastSameActiveHistory != null)
            {
                if (lastSameActiveHistory.ActiveStateChangeDate > now && customerActiveHistory.ActiveStateChangeDate.Date >= now)
                {
                    lastSameActiveHistory.IsValid = false;
                }
            }

            var currentUser = TMSContext.TMSPrincipal.Identity;
            customerActiveHistory.CreatedBy = currentUser.Name;

            //Add current active record
            dbContext.CustomerActiveHistories.Add(customerActiveHistory);
            var customer = dbContext.Customers.SingleOrDefault(c => c.ID == customerActiveHistory.CustomerID);
            if (customer != null)
                customer.GpUpdate = true;
            dbContext.SaveChanges();
        }

        public CustomerActiveHistory LoadCurrenActiveHistoryForCustomer(int customerId)
        {
            //Get current vendor active state
            using (var dbContext = new RegistrantBoundedContext())
            {
                var user = TMSContext.TMSPrincipal.Identity;
                var updatedBy = user.Name;

                var now = DateTime.Now.AddDays(1).Date;
                var currentActiveHistory = dbContext.CustomerActiveHistories.Where(c => c.CustomerID == customerId && c.IsValid && c.ActiveStateChangeDate < now).OrderByDescending(c => c.ActiveStateChangeDate).FirstOrDefault();
                var customer = dbContext.Customers.Where(c => c.ID == customerId).FirstOrDefault();
                if (customer.IsActive != currentActiveHistory.ActiveState)
                {
                    customer.IsActive = currentActiveHistory.ActiveState;
                    customer.ActiveStateChangeDate = currentActiveHistory.ActiveStateChangeDate;
                    customer.LastUpdatedDate = DateTime.UtcNow;
                    customer.LastUpdatedBy = updatedBy;
                    dbContext.SaveChanges();
                }
                return currentActiveHistory;
            }
        }

        public CustomerActiveHistory LoadPreviousActiveHistoryForCustomer(int customerId, bool isActive)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var currentActiveHistory = dbContext.CustomerActiveHistories.Where(c => c.CustomerID == customerId && c.ActiveState == isActive && c.IsValid).OrderByDescending(c => c.ActiveStateChangeDate).FirstOrDefault();
            return currentActiveHistory;
        }

        public bool UpdateVendorForRemoveBatch(string registrationNumber)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var vendor = dbContext.Vendors.FirstOrDefault(c => c.Number == registrationNumber);
            if (vendor != null)
            {
                vendor.InBatch = false;
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateCustomerForRemoveBatch(string registrationNumber)
        {
            var dbContext = (context as RegistrantBoundedContext);
            var vendor = dbContext.Customers.FirstOrDefault(c => c.RegistrationNumber == registrationNumber);
            if (vendor != null)
            {
                vendor.InBatch = false;
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region OTSTM-72

        public IList<Contact> GetAllPrimaryEmailAddressByVendorType(int vendorType)
        {
            var registrantBoundedContext = context as RegistrantBoundedContext;

            var allVendorAddresses = registrantBoundedContext.Vendors.Where(c => c.VendorType == vendorType && c.IsActive).Include(c => c.VendorAddresses);
            var allPrimaryContacts = new List<Contact>();
            allVendorAddresses.ToList().ForEach(c =>
            {
                var primaryContact = RepositoryHelper.FindPrimaryContact(c.VendorAddresses.ToList());
                allPrimaryContacts.Add(primaryContact);
            });
            return allPrimaryContacts;
        }

        #endregion

        #region OTSTM2-449
        /// <summary>
        /// Automatic Switch to 12x in Jan of every year
        /// </summary>
        /// <param name="changeBy"></param>
        public void AutoSwitchForAllCustomers(int changeBy)
        {
            using (var dbContext = new RegistrantBoundedContext())
            {
                var customers = dbContext.Customers.Include("CustomerSettingHistoryList").ToList();
                customers.ForEach(c =>
                {
                    var setting = c.CustomerSettingHistoryList.Where(l => l.ChangeDate.Year == DateTime.Now.AddYears(-1).Year).OrderByDescending(l => l.ChangeDate).FirstOrDefault();
                    //var setting = c.CustomerSettingHistoryList.Where(l => l.ChangeDate.Year == DateTime.Now.Year).OrderByDescending(l => l.ChangeDate).FirstOrDefault();
                    if (setting != null)
                    {
                        var setting12x = new CustomerSettingHistory()
                        {
                            CustomerId = c.ID,
                            ColumnName = "RemitsPerYear",
                            OldValue = setting.NewValue,
                            NewValue = "12x",
                            ChangeBy = changeBy,
                            ChangeDate = DateTime.UtcNow
                        };
                        c.CustomerSettingHistoryList.Add(setting12x);
                    }
                    else
                    {
                        var setting12x = new CustomerSettingHistory()
                        {
                            CustomerId = c.ID,
                            ColumnName = "RemitsPerYear",
                            OldValue = c.RemitsPerYear == true ? "12x" : "2x",
                            NewValue = "12x",
                            ChangeBy = changeBy,
                            ChangeDate = DateTime.UtcNow
                        };
                        c.CustomerSettingHistoryList.Add(setting12x);
                    }
                    c.RemitsPerYear = true;
                    c.RemitsPerYearSwitchDate = DateTime.UtcNow;
                });
                dbContext.SaveChanges();
            }
        }
        #endregion

        public bool RemoveTCRServiceThresohold(int vendorIdId, long userId)
        {
            var dbContext = context as RegistrantBoundedContext;

            var tmpTCR = dbContext.VendorServiceThresholds.FirstOrDefault(c => c.VendorID == vendorIdId && (c.IsDeleted == null || c.IsDeleted == false));

            if (tmpTCR != null)
            {
                tmpTCR.IsDeleted = true;
                tmpTCR.ModifiedDate = DateTime.UtcNow;
                tmpTCR.ModifiedByID = userId;

                dbContext.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
