﻿using CL.TMS.Common;
using CL.TMS.Common.Extension;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CL.TMS.Repository.System
{
    public class ProductCompositionRepository : BaseRepository<ProductCompositionContext, RecoverableMaterial, int>, IProductCompositionRepository
    {
        #region //Loading data

        //Loads all currently effective rates
        public RecoverableMaterial GetRecoverableMaterialByID(int materialID)
        {
            var dbContext = context as ProductCompositionContext;
            return dbContext.RecoverableMaterials.FirstOrDefault(x => x.ID == materialID);
        }
        public PaginationDTO<RecoverableMaterialsVM, int> LoadRecoverableMaterials(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as ProductCompositionContext;
            var query = from item in dbContext.RecoverableMaterials
                        select new RecoverableMaterialsVM
                        {
                            ID = item.ID,
                            MaterialName = item.MaterialName,
                            MaterialDescription = item.MaterialDescription,
                            AddedBy = item.CreatedByUser.FirstName + " " + item.CreatedByUser.LastName,
                            DateAdded = item.CreatedDate,
                            DateModified = item.ModifiedDate,
                            ModifiedBy = item.ModifiedByUser.FirstName + " " + item.ModifiedByUser.LastName,
                            Color = item.Color
                        };

            if (sortDirection == "asc")
                query = query.AsQueryable<RecoverableMaterialsVM>().OrderBy(orderBy);
            else
                query = query.AsQueryable<RecoverableMaterialsVM>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RecoverableMaterialsVM, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<MaterialCompositionsVM, int> LoadMaterialCompositions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as ProductCompositionContext;
            var dto = new PaginationDTO<MaterialCompositionsVM, int>
            {
                DTOCollection = new List<MaterialCompositionsVM>(),
                TotalRecords = 0,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };

            if (dbContext.ItemRecoverableMaterials == null)
            {
                return dto;
            }

            var itemLists = dbContext.ItemRecoverableMaterials.Include(x => x.Item).Include(x => x.RecoverableMaterial)
                        .GroupBy(g => g.ItemID)
                        .Select(c => new { itemID = c.Key, items = c.OrderByDescending(x => x.ModifiedDate).ToList() }).ToList();

            var query = itemLists.Select(x => new MaterialCompositionsVM()
            {
                ItemID = x.itemID,
                ItemName = x.items.FirstOrDefault()?.Item.Name,
                ItemShortName = x.items.FirstOrDefault()?.Item.ShortName,
                DateModified = x.items.FirstOrDefault(c => c.ModifiedDate != null)?.ModifiedDate,
                ModifiedBy = x.items.FirstOrDefault(c => c.ModifiedByID.HasValue)?.User.FirstName + " " +
                                        x.items.FirstOrDefault(c => c.ModifiedByID.HasValue)?.User.LastName,
                recoverableMaterials = dbContext.RecoverableMaterials.Select(c => new ItemModel<string>()
                {
                    //TODO: in future list recoverable materials will filter by product type. 
                    //now there is only one productType="Tire"
                    ItemID = c.ID, //==materialID
                    ItemName = c.MaterialName,
                    ItemValue = c.Color
                }).OrderBy(c => c.ItemName).ToList(),
                ItemMaterialList = x.items.Select(c => new ItemRecoverableMaterialVM()
                {
                    Color = c.RecoverableMaterial?.Color,
                    MaterialID = c.MaterialID,
                    MaterialName = c.RecoverableMaterial?.MaterialName,
                    Quantity = c.Quantity
                }).OrderBy(c => c.MaterialName).ToList()
            });

            dto.TotalRecords = query.Count();
            dto.DTOCollection = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return dto;
        }
        public bool CheckMaterialNameUnique(string name)
        {
            var dbContext = context as ProductCompositionContext;
            return dbContext.RecoverableMaterials.Any(x => x.MaterialName.Trim().ToLower() == name.Trim().ToLower());
        }
        #endregion //Loading data

        #region //Save

        public bool UpdateRecoverableMaterial(RecoverableMaterialsVM vm, long userId, string email, string fullName, int activityType)
        {
            var dbContext = context as ProductCompositionContext;
            var material = dbContext.RecoverableMaterials.FirstOrDefault(x => x.ID == vm.ID);
            if (material == null)
            {
                return false;
            }
            //add activity
            var activity = new Activity();
            if (material.MaterialName.ToLower().Trim() != vm.MaterialName.ToLower().Trim())
            {
                if (CheckMaterialNameUnique(vm.MaterialName))
                {
                    return false;
                }
                activity = new Activity()
                {
                    ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                    ActivityType = activityType,
                    CreatedTime = DateTime.Now,
                    Initiator = email,
                    InitiatorName = fullName,
                    Assignee = "",
                    AssigneeName = "",
                    ObjectId = material.ID,
                    Message = string.Format(fullName + " changed <strong> name </strong> of <strong> recoverable material</strong> from " +
                            "<strong>" + material.MaterialName + "</strong> to <strong>" + vm.MaterialName + "</strong>")
                };
                dbContext.Activities.Add(activity);
            }

            if (material.Color.Trim() != vm.Color.Trim())
            {
                activity = new Activity()
                {
                    ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                    ActivityType = activityType,
                    CreatedTime = DateTime.Now,
                    Initiator = email,
                    InitiatorName = fullName,
                    Assignee = "",
                    AssigneeName = "",
                    ObjectId = material.ID,
                    Message = string.Format(fullName + " changed <strong>colour</strong> of <strong>" + vm.MaterialName + "</strong>")
                };
                dbContext.Activities.Add(activity);
            }

            if (material.MaterialDescription.Trim() != vm.MaterialDescription.Trim())
            {
                activity = new Activity()
                {
                    ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                    ActivityType = activityType,
                    CreatedTime = DateTime.Now,
                    Initiator = email,
                    InitiatorName = fullName,
                    Assignee = "",
                    AssigneeName = "",
                    ObjectId = material.ID,
                    Message = string.Format(fullName + " changed <strong> description </strong> of <strong>" + vm.MaterialName + "</strong>")
                };
                dbContext.Activities.Add(activity);
            }
            //update recoverable material
            material.Color = vm.Color;
            material.MaterialDescription = vm.MaterialDescription;
            material.MaterialName = vm.MaterialName;
            material.ModifiedByID = userId;
            material.ModifiedDate = DateTime.UtcNow;

            dbContext.SaveChanges();
            return true;
        }
        public bool AddRecoverableMaterial(RecoverableMaterialsVM vm, long userID, string email, string fullName, int activityType)
        {
            var dbContext = context as ProductCompositionContext;

            if (CheckMaterialNameUnique(vm.MaterialName))
            {
                return false;
            }

            var material = new RecoverableMaterial()
            {
                Color = vm.Color,
                MaterialDescription = vm.MaterialDescription,
                MaterialName = vm.MaterialName,
                CreatedByID = userID,
                CreatedDate = DateTime.UtcNow,
            };

            dbContext.RecoverableMaterials.Add(material);
            dbContext.SaveChanges();
            //add activity
            var activity = new Activity()
            {
                ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                ActivityType = activityType,
                CreatedTime = DateTime.Now,
                Initiator = email,
                InitiatorName = fullName,
                ObjectId = material.ID,
                Assignee = "",
                AssigneeName = "",
                Message = string.Format(fullName + " added <strong>" + vm.MaterialName +
                            "</strong> as a <strong> recoverable material.</strong>")
            };

            dbContext.Activities.Add(activity);
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateMaterialComposition(MaterialCompositionsVM vm, long userId, string email, string fullName, int activityType)
        {
            var dbContext = context as ProductCompositionContext;
            var product = dbContext.ItemRecoverableMaterials.Where(x => x.ItemID == vm.ItemID);
            var activity = new Activity();
            foreach (var item in vm.ItemMaterialList)
            {
                var material = product.FirstOrDefault(c => c.MaterialID == item.MaterialID);

                if (material != null)
                {
                    if (material.Quantity.HasValue && material.Quantity == item.Quantity)
                    {
                        continue;
                    }
                    //add activity       
                    activity = new Activity()
                    {
                        ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                        ActivityType = activityType,
                        CreatedTime = DateTime.Now,
                        Initiator = email,
                        InitiatorName = fullName,
                        Assignee = "",
                        AssigneeName = "",
                        ObjectId = vm.ItemID.Value,
                        Message = string.Format(fullName + " changed the <strong>" + item.MaterialName +
                            "</strong> composition of <strong>" + vm.ItemName + "</strong> from <strong>" +
                            material.Quantity.Value.ToString("0") + "% </strong>to <strong>" + item.Quantity + "%</strong>")
                    };

                    dbContext.Activities.Add(activity);

                    material.Quantity = item.Quantity;
                    material.ModifiedByID = userId;
                    material.ModifiedDate = DateTime.Now;

                }
                else
                {

                    if (item.Quantity == 0)
                    {
                        continue;
                    }
                    var newMaterial = new ItemRecoverableMaterial()
                    {
                        ItemID = vm.ItemID,
                        MaterialID = item.MaterialID,
                        Quantity = item.Quantity,
                        ModifiedByID = userId,
                        ModifiedDate = DateTime.Now
                    };
                    dbContext.ItemRecoverableMaterials.Add(newMaterial);

                    activity = new Activity()
                    {
                        ActivityArea = TreadMarksConstants.AdminActivityProductCompositions,
                        ActivityType = activityType,
                        CreatedTime = DateTime.Now,
                        Initiator = email,
                        InitiatorName = fullName,
                        Assignee = "",
                        AssigneeName = "",
                        ObjectId = vm.ItemID.Value,
                        Message = string.Format(fullName + " changed the <strong>" + item.MaterialName +
                            "</strong> composition of <strong>" + vm.ItemName + "</strong> from <strong>0%</strong> to <strong>" + item.Quantity + "</strong>%")
                    };

                    dbContext.Activities.Add(activity);
                }
            }

            dbContext.SaveChanges();
            return true;
        }

        #endregion

        #region Estimated Weights
        public PaginationDTO<RecoveryEstimatedWeightListViewModel, int> LoadRecoveryEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            var dbContext = context as ProductCompositionContext;
            var query = dbContext.WeightTransactions.Where(c => c.Category == categoryId).AsNoTracking()
                        .Select(c => new RecoveryEstimatedWeightListViewModel()
                        {
                            ID = c.ID,
                            EffectiveStartDate = c.EffectiveStartDate,
                            EffectiveEndDate = c.EffectiveEndDate,
                            DateAdded = c.CreatedDate,
                            AddedBy = c.CreatedByUser.FirstName + " " + c.CreatedByUser.LastName,
                            ModifiedBy = (c.ModifiedByUser != null) ? c.ModifiedByUser.FirstName + " " + c.ModifiedByUser.LastName : "",
                            DateModified = c.ModifiedDate,
                            WeightNotes = c.WeightTransactionNotes.OrderByDescending(i => i.CreatedDate).ToList(),
                            Category = c.Category,
                        });

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "EffectiveStartDate" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RecoveryEstimatedWeightListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }

        public PaginationDTO<SupplyEstimatedWeightViewModel, int> LoadSupplyEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            var dbContext = context as ProductCompositionContext;
            var query = dbContext.ItemWeights.Where(c => c.SetRangeMax.HasValue && c.SetRangeMin.HasValue).AsNoTracking()
                        .Select(c => new SupplyEstimatedWeightViewModel()
                        {
                            ID = c.ItemID,
                            Min = (decimal)c.SetRangeMin,
                            Max = (decimal)c.SetRangeMax,
                            ProductName = c.item.Name,
                            ProductDescription = c.item.Description,
                            Category = categoryId,
                            ShortName = c.item.ShortName,
                        });

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "ID" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();

            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<SupplyEstimatedWeightViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }

        public bool IsFutureWeightExists(int category)
        {
            return (context as ProductCompositionContext).WeightTransactions.Any(c => c.EffectiveStartDate > DateTime.Now && c.Category == category);
        }

        public void AddRecoveryEstimatedWeights(WeightTransaction weightTransaction, WeightParamsDTO param)
        {
            var dbContext = context as ProductCompositionContext;

            processRecoveryEstimateWeight(weightTransaction, dbContext, param.REW);

            //NOTE: update the latest previous transaction effectiveEndDate;
            var previousTransaction = dbContext.WeightTransactions.Where(x => x.Category == weightTransaction.Category).OrderByDescending(i => i.EffectiveStartDate).FirstOrDefault();
            if (previousTransaction != null)
            {
                previousTransaction.EffectiveEndDate = weightTransaction.EffectiveStartDate.Date.AddDays(-1);
            }

            dbContext.WeightTransactions.Add(weightTransaction);

            dbContext.SaveChanges();
        }

        private void processRecoveryEstimateWeight(WeightTransaction weightTransaction, ProductCompositionContext dbContext, RecoveryEstimatedWeightsParam param)
        {
            var previousEffectiveEndtDate = weightTransaction.EffectiveStartDate.Date.AddDays(-1);
            var previousWeights = dbContext.ItemWeights.Where(c => param.items.Contains(c.ItemID)
                && c.EffectiveEndDate > weightTransaction.EffectiveStartDate).ToList();
            previousWeights.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });
        }

        public IEnumerable<ItemWeight> GetItemWeights(int transactionID)
        {
            var dbContext = context as ProductCompositionContext;
            if (transactionID > 0)
            {
                return dbContext.ItemWeights.Where(c => c.WeightTransactionID == transactionID);
            }
            else
            {
                var today = DateTime.Today;
                return dbContext.ItemWeights.Where(c => c.EffectiveStartDate <= today && c.EffectiveEndDate > today);
            }
        }

        public bool UpdateRecoveryEstimatedWeights(WeightDetailsVM weightDetailsVM, WeightParamsDTO param)
        {
            var dbContext = context as ProductCompositionContext;

            updateRecoveryEstimateWeight(dbContext, weightDetailsVM, param);

            //NOTE: update the latest previous transaction effectiveEndDate;
            DateTime previousEndDate = weightDetailsVM.PreviousEffectiveStartDate.AddDays(-1);
            var previousTransactions = dbContext.WeightTransactions.Where(x => x.Category == weightDetailsVM.Category).OrderByDescending(i => i.EffectiveStartDate).Skip(1).Take(1).FirstOrDefault();
            if (previousTransactions != null)
            {
                previousTransactions.EffectiveEndDate = weightDetailsVM.EffectiveStartDate.Date.AddDays(-1);
            }

            dbContext.SaveChanges();

            return true;
        }

        private void updateRecoveryEstimateWeight(ProductCompositionContext dbContext, WeightDetailsVM weightDetailsVM, WeightParamsDTO param)
        {
            var weightTransaction = dbContext.WeightTransactions.Include(c => c.ItemWeights).Include(c => c.WeightTransactionNotes).FirstOrDefault(c => c.ID == weightDetailsVM.WeightTransactionID);

            //Update rate transaction
            weightTransaction.EffectiveStartDate = weightDetailsVM.EffectiveStartDate;
            weightTransaction.ModifiedDate = DateTime.UtcNow;
            weightTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(weightDetailsVM.Note))
            {
                //add note
                var weightTransactionNote = new WeightTransactionNote();
                weightTransactionNote.WeightTransaction = weightTransaction;
                weightTransactionNote.Note = weightDetailsVM.Note;
                weightTransactionNote.CreatedDate = DateTime.UtcNow;
                weightTransactionNote.UserID = param.userId;
                if (weightTransaction.WeightTransactionNotes == null)
                    weightTransaction.WeightTransactionNotes = new List<WeightTransactionNote>();
                weightTransaction.WeightTransactionNotes.Add(weightTransactionNote);
            }
            #region Update Recovery Estimate Weight
            var weightItems = weightDetailsVM.RecoveryEstimatedWeights.GetType().GetProperties();
            var paramItems = param.REW.GetType().GetProperties();
            foreach (var property in weightItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name == property.Name).GetValue(param.REW);
                var item = weightTransaction.ItemWeights.FirstOrDefault(c => c.ItemID == paramID && c.WeightTransactionID == weightDetailsVM.WeightTransactionID);
                if (item != null)
                {
                    item.EffectiveStartDate = weightDetailsVM.EffectiveStartDate;
                    item.EffectiveEndDate = weightDetailsVM.EffectiveEndDate;
                    item.StandardWeight = (decimal)property.GetValue(weightDetailsVM.RecoveryEstimatedWeights); //rateDetailsVM.estimatedWeightsRate.AGLS;
                }
            }

            #endregion

            //update previous Rate-effective-start-date
            DateTime effectiveStartDate = weightDetailsVM.PreviousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.ItemWeights.Where(i => param.REW.items.Contains(i.ItemID)
                && i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = weightTransaction.EffectiveStartDate.AddDays(-1);
            });
        }

        public bool RemoveWeightTransaction(int weightTransactionID, string categoryName)
        {
            using (var dbContext = context as ProductCompositionContext)
            {
                var weightTransaction = dbContext.WeightTransactions.Where(m => m.ID == weightTransactionID).FirstOrDefault();
                if (weightTransaction != null)
                {
                    //1.remove existing data.
                    var notes = weightTransaction.WeightTransactionNotes.ToList();
                    if ((notes != null) && (notes.Count > 0))
                    {
                        dbContext.WeightNotes.RemoveRange(notes);
                    }


                    var itemWeights = weightTransaction.ItemWeights.ToList();
                    if (itemWeights != null && itemWeights.Count > 0)
                    {
                        dbContext.ItemWeights.RemoveRange(itemWeights);
                    }

                    //2.Update effectiveenddate for the previous records of ItemWeights
                    DateTime effectiveStartDate = weightTransaction.EffectiveStartDate.AddDays(-1);

                    var previousRates = dbContext.ItemWeights.Where(i => i.EffectiveEndDate == effectiveStartDate).ToList();
                    previousRates.ForEach(i =>
                    {
                        i.EffectiveEndDate = DateTime.MaxValue.Date;
                    });


                    //3. remove weighttransaction
                    dbContext.WeightTransactions.Remove(weightTransaction);

                    var previousRateTrans = dbContext.WeightTransactions.Where(c => c.Category == weightTransaction.Category && c.EffectiveStartDate <= DateTime.Today && c.EffectiveEndDate >= DateTime.Today).FirstOrDefault();
                    if (previousRateTrans != null)
                    {
                        previousRateTrans.EffectiveEndDate = DateTime.MaxValue.Date;
                    }


                    context.SaveChanges();
                }
            }
            return true;
        }

        public WeightTransactionNote AddTransactionNote(WeightTransactionNote transactionNote)
        {
            var dbContext = context as ProductCompositionContext;
            dbContext.WeightNotes.Add(transactionNote);
            dbContext.SaveChanges();
            return transactionNote;
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            var dbContext = context as ProductCompositionContext;

            var query = from note in dbContext.WeightNotes.AsNoTracking()
                        where note.WeightTransactionID == parentId
                        select new InternalNoteViewModel()
                        {
                            AddedOn = note.CreatedDate,
                            Note = note.Note,
                            AddedBy = note.User.FirstName + " " + note.User.LastName,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Note.ToLower().Contains(searchText)
                                        || c.AddedBy.Contains(searchText)
                                        );
                }

            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;
            query = !sortReverse ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");
            var totalRecords = query.Count();
            //var result = query.Skip(pageIndex).Take(pageSize).ToList();
            return query.ToList();
        }

        public List<InternalNoteViewModel> LoadWeightTransactionNoteByID(int weightTransactionID)
        {
            var dbContext = context as ProductCompositionContext;

            return dbContext.WeightNotes.Where(c => c.WeightTransactionID == weightTransactionID).Select(i => new InternalNoteViewModel
            {
                AddedOn = i.CreatedDate,
                Note = i.Note,
                AddedBy = i.User.FirstName + " " + i.User.LastName
            }).OrderByDescending(i => i.AddedOn).ToList();
        }

        public SupplyEstimatedWeightViewModel LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category)
        {
            var dbContext = context as ProductCompositionContext;
            var itemWeight = dbContext.ItemWeights.Where(c => c.ItemID == itemID).FirstOrDefault();
            SupplyEstimatedWeightViewModel result = new SupplyEstimatedWeightViewModel();

            result.ID = itemWeight.ItemID;
            result.ShortName = itemWeight.item.ShortName;
            result.ProductName = itemWeight.item.Name;
            result.ProductDescription = itemWeight.item.Description;
            result.Min = (decimal)itemWeight.SetRangeMin;
            result.Max = (decimal)itemWeight.SetRangeMax;
            result.RangeMin = (decimal)itemWeight.item.AvailableRangeMin;
            result.RangeMax = (decimal)itemWeight.item.AvailableRangeMax;

            return result;
        }

        public void updateSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM)
        {
            var dbContext = context as ProductCompositionContext;
            var itemWeight = dbContext.ItemWeights.Where(c => c.ItemID == supplyEstimatedWeightVM.ID).FirstOrDefault();
            itemWeight.item.Name = supplyEstimatedWeightVM.ProductName;
            itemWeight.item.Description = supplyEstimatedWeightVM.ProductDescription;
            itemWeight.SetRangeMin = supplyEstimatedWeightVM.Min;
            itemWeight.SetRangeMax = supplyEstimatedWeightVM.Max;

            dbContext.SaveChanges();
        }

        #endregion
    }
}