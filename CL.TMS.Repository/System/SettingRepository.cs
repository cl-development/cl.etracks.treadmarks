﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.Framework.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.ViewModel.Roles;
using System.Data.Entity;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.Repository.System
{
    public class SettingRepository:BaseRepository<SystemBoundedContext,Setting,int>,ISettingRepository
    {
        public IQueryable<Setting> GetAll()
        {
            return entityStore.All;
        }

        public string GetSettingValue(string key)
        {
            var firstOrDefault = entityStore.All.FirstOrDefault(c => c.Key == key);
            return firstOrDefault != null ? firstOrDefault.Value : string.Empty;
        }

        public void AddSetting(Setting setting)
        {
            ConditionCheck.Null(setting, "Setting");
            entityStore.Insert(setting);
        }

        public void EditSetting(Setting setting)
        {
            ConditionCheck.Null(setting, "Setting");
            entityStore.Update(setting);
        }

        public void DeleteSetting(Setting setting)
        {
            ConditionCheck.Null(setting, "Setting");
            entityStore.Delete(setting.ID);
        }


        public List<TypeDefinition> LoadDefinitions()
        {
            return (context as SystemBoundedContext).TypeDefinitions.ToList();
        }


        public List<Role> LoadUserRoles()
        {
            return (context as SystemBoundedContext).Roles.ToList();
        }


        public List<ParticipantType> LoadAllParticipantTypes()
        {
            return (context as SystemBoundedContext).ParticipantTypes.ToList();
        }

        public List<Item> LoadAllItems()
        {
            return (context as SystemBoundedContext).Items.ToList();
        }

        /// <summary>
        /// Load processor specific rates
        /// </summary>
        /// <returns></returns>
        public List<SpecificRateModel> LoadSpecificRates()
        {
            var dbContext = context as SystemBoundedContext;
            var query = from r in dbContext.Rates
                        join vr in dbContext.VendorRates on r.RateTransactionID equals vr.RateTransactionID
                        where (bool)r.IsSpecificRate
                        select new SpecificRateModel
                        {
                            ID = r.ID,
                            ItemID = r.ItemID,
                            RateTransactionID = r.RateTransactionID,
                            ItemRate = r.ItemRate,
                            PaymentType = r.PaymentType,
                            ClaimType = r.ClaimType,
                            EffectiveStartDate = r.EffectiveStartDate,
                            EffectiveEndDate = r.EffectiveEndDate,
                            VendorID = vr.VendorID,
                            ItemType = r.ItemType
                        };            
            return query.ToList();
        }

        /// <summary>
        /// Load all rates before this date (general rate exclude specific reate)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<Rate> LoadRates(DateTime date)
        {
            var dbContext = context as SystemBoundedContext;
            var query = dbContext.Rates.Where(c => c.IsSpecificRate == null || !(bool)c.IsSpecificRate);
            return query.ToList();
        }

        /// <summary>
        /// Load rate group rates
        /// </summary>
        /// <returns></returns>
        public List<RateGroupRate> LoadRateGroupRates()
        {
            var dbContext = context as SystemBoundedContext;
            return dbContext.RateGroupRates.ToList();
        }
        
        /// <summary>
        /// Load all Item Weights_recovery before this date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<ItemWeight> LoadRecoveryItemWeights(DateTime date)
        {
            return (context as SystemBoundedContext).ItemWeights.Where(c => (!c.SetRangeMax.HasValue) && (!c.SetRangeMin.HasValue)).ToList(); 
        }

        /// <summary>
        /// Load all Item Weights_supply
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<ItemWeight> LoadSupplyItemWeights()
        {
            return (context as SystemBoundedContext).ItemWeights.Where(c => c.SetRangeMax.HasValue && c.SetRangeMin.HasValue).ToList();
        }

        public List<TransactionType> LoadTransactionTypes()
        {
            return (context as SystemBoundedContext).TransactionTypes.ToList();
        }

        public List<PostalCode> LoadPostalCodes()
        {
            return (context as SystemBoundedContext).PostalCodes.ToList();
        }

        //OTSTM2-215
        public List<Region> LoadRegions()
        {
            return (context as SystemBoundedContext).Region.Include(r => r.RegionDetails).ToList();
        }

        public List<Zone> LoadZones()
        {
            return (context as SystemBoundedContext).Zones.ToList();
        }

        //OTSTM2-1016
        public List<Country> LoadCountries()
        {
            return (context as SystemBoundedContext).Country.ToList();
        }

        public void AddSystemActivity(SystemActivity systemActivity)
        {
            var dbContext = (context as SystemBoundedContext);
            dbContext.SystemActivities.Add(systemActivity);
            dbContext.SaveChanges();
        }

        public List<SystemActivity> LoadSystemActivity(string category, int activityId)
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.SystemActivities.Where(c => c.Category == category && c.ActivityId == activityId).OrderByDescending(c => c.CreatedDate).ToList();
        }

        public User GetSystemUser()
        {
            var dbContext = (context as SystemBoundedContext);
            var query = from user in dbContext.Users
                        join userRole in dbContext.UserRoles on user.ID equals userRole.UserID
                        join role in dbContext.Roles on userRole.RoleID equals role.ID
                        where role.Name == "Super Admin"
                        select user;
            return query.FirstOrDefault();
        }

        public List<GpiAccount> LoadGPIAccounts()
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.GPIAccounts.ToList();
        }

        //OTSTM2-1124 for Successor only
        public List<FIAccount> LoadFIAccounts()
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.FIAccounts.ToList();
        }

        //OTSTM2-486
        public List<BusinessActivity> LoadAllBusinessActivies()
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.BusinessActivities.ToList();
        }

        public List<Item> LoadAllItemsForReportingService()
        {
            using (var dbContext = new SystemBoundedContext())
            {
                return dbContext.Items.ToList();
            }
        }

        public List<RolePermissionViewModel> GetDefaultRolePermissions()
        {
            var dbContext = (context as SystemBoundedContext);
            var query = dbContext.AppResources.Select(c => new RolePermissionViewModel
            {
                Menu = c.ResourceMenu,
                Screen = c.ResourceScreen,
                Panel = c.Name,
                ResourceLevel = c.ResourceLevel,
                DisplayOrder = c.DisplayOrder,
                IsNoAccess = true,
                ResourceMenu = c.ResourceMenu,
                //OTSTM2-838
                ApplicableLevel = c.ApplicableLevel
            }).OrderBy(c => c.DisplayOrder);

            return query.ToList();
        }

        public List<AppResource> GetAppResources()
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.AppResources.ToList();
        }

        public List<AppPermission> GetAppPermissions()
        {
            var dbContext = (context as SystemBoundedContext);
            return dbContext.AppPermissions.ToList();
        }
    }
}