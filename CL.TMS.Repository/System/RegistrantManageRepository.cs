
using CL.Framework.Common;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace CL.TMS.Repository.System
{
    public class RegistrantManageRepository : BaseRepository<RegistrantBoundedContext, Vendor, int>, IRegistrantManageRepository
    {
        private EntityStore<VendorActiveHistory, int> vendorActiveHistoryStore;
        private EntityStore<Address, int> addressStore;

        public RegistrantManageRepository()
        {
            vendorActiveHistoryStore = new EntityStore<VendorActiveHistory, int>(context);
            addressStore = new EntityStore<Address, int>(context);
        }

        public PaginationDTO<RegistrantManageModel, long> LoadAllRegistrant(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            DateTime legacyStartDate = new DateTime(2009, 7, 1);
            using (var dbContext = new RegistrantBoundedContext())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;
                searchText = searchText.ToLower();

                //Load vendor first
                var vendorQuery = from vendor in dbContext.Vendors.AsNoTracking()
                                  select new RegistrantManageModel
                                  {
                                      Number = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorID = vendor.ID,
                                      Contact = vendor.ContactInfo,
                                      Status = vendor.IsActive ? "Active" : "Inactive",
                                      ParticipantType = vendor.VendorType,
                                      ApplicationID = vendor.ApplicationID ?? -1,
                                      BankInfoReviewStatus = (vendor.BankInformations.FirstOrDefault() != null) ? vendor.BankInformations.FirstOrDefault().ReviewStatus : "Open",
                                      BankInfoId = vendor.BankInformations.FirstOrDefault() != null ? vendor.BankInformations.FirstOrDefault().ID : 0,
                                      CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < legacyStartDate) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue)),
                                  };

                //Load customer
                var customerQuery = from customer in dbContext.Customers.AsNoTracking()
                                    select new RegistrantManageModel
                                    {
                                        Number = customer.RegistrationNumber,
                                        BusinessName = customer.BusinessName,
                                        VendorID = customer.ID,
                                        Contact = customer.ContactInfo,
                                        Status = customer.IsActive ? "Active" : "Inactive",
                                        ParticipantType = 1,
                                        ApplicationID = customer.ApplicationID ?? -1,
                                        BankInfoReviewStatus = string.Empty,
                                        BankInfoId = 0,
                                        CreatedDate = (null == customer.ConfirmationMailedDate) ? (customer.CreatedDate) : ((customer.ConfirmationMailedDate < legacyStartDate) ? (customer.ActivationDate ?? DateTime.MinValue) : (customer.ConfirmationMailedDate ?? DateTime.MinValue)),
                                    };

                var query = vendorQuery.Concat(customerQuery);//mS: 7097 totalRecords:11146 result.Count():700

                //Apply search text
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    DateTime dateValue;
                    if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                    {
                        if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                        {
                            var nextMonth = dateValue.AddMonths(1);
                            query = query.Where(c => (((DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)))
                                ));
                        }
                        else
                        {
                            query = query.Where(c => ((DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue))
                                ));
                        }
                    }
                    else
                    {
                        query = query.Where(c => c.Number.ToLower().Contains(searchText)
                                                || c.BusinessName.ToLower().Contains(searchText)
                                                || c.Contact.ToLower().Contains(searchText)
                                                || c.Status.ToLower().Contains(searchText)
                                                || c.BankInfoReviewStatus.ToLower().Contains(searchText)
                                                || DbFunctions.TruncateTime(c.CreatedDate).ToString().Contains(searchText) //OTSTM2-877
                                                );
                    }
                }

                //OTSTM2-931
                var columnSearchTextReg = columnSearchText["Reg"];
                var columnSearchTextCompany = columnSearchText["Company"];
                var columnSearchTextContact = columnSearchText["Contact"];
                var columnSearchTextApprovedDate = columnSearchText["ApprovedDate"];
                var columnSearchTextStatus = columnSearchText["Status"];
                var columnSearchTextBankStatus = columnSearchText["BankStatus"];           

                if (!string.IsNullOrWhiteSpace(columnSearchTextReg))
                {
                    query = query.Where(c => c.Number.ToString().Contains(columnSearchTextReg));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
                {
                    query = query.Where(c => c.BusinessName.ToString().Contains(columnSearchTextCompany));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextContact))
                {
                    query = query.Where(c => c.Contact.ToString().Contains(columnSearchTextContact));
                }               
                if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
                {
                    query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextBankStatus))
                {
                    query = query.Where(c => c.BankInfoReviewStatus.ToString().Contains(columnSearchTextBankStatus));
                }

                //Applying sorting
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

                //OTSTM2-931 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
                var result = query.ToList();
                result.ForEach(i =>
                {
                    i.CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(i.CreatedDate, TimeZoneInfo.Local);
                });

                if (!string.IsNullOrWhiteSpace(columnSearchTextApprovedDate))
                {
                    result = result.Where(c => c.CreatedDate.ToString("yyyy-MM-dd").ToLower().Contains(columnSearchTextApprovedDate)).ToList();
                }

                var totalRecords = result.Count();
                var returnValue = result.Skip(pageIndex).Take(pageSize).ToList();

                return new PaginationDTO<RegistrantManageModel, long>
                {
                    DTOCollection = returnValue,
                    TotalRecords = totalRecords,
                    PageNumber = pageIndex + 1,
                    PageSize = pageSize
                };
            }
        }

        public List<RegistrantManageModel> LoadRegistrants(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            DateTime legacyStartDate = new DateTime(2009, 7, 1);
            using (var dbContext = new RegistrantBoundedContext())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 180;
                searchText = searchText.ToLower();

                //Load vendor first
                var vendorQuery = from vendor in dbContext.Vendors.AsNoTracking()
                                  select new RegistrantManageModel
                                  {
                                      Number = vendor.Number,
                                      BusinessName = vendor.BusinessName,
                                      VendorID = vendor.ID,
                                      Contact = vendor.ContactInfo,
                                      Status = vendor.IsActive ? "Active" : "Inactive",
                                      ParticipantType = vendor.VendorType,
                                      ApplicationID = vendor.ApplicationID ?? -1,
                                      BankInfoReviewStatus = (vendor.BankInformations.FirstOrDefault() != null) ? vendor.BankInformations.FirstOrDefault().ReviewStatus : "Open",
                                      BankInfoId = vendor.BankInformations.FirstOrDefault() != null ? vendor.BankInformations.FirstOrDefault().ID : 0,
                                      CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < legacyStartDate) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue)),
                                  };

                //Load customer
                var customerQuery = from customer in dbContext.Customers.AsNoTracking()
                                    select new RegistrantManageModel
                                    {
                                        Number = customer.RegistrationNumber,
                                        BusinessName = customer.BusinessName,
                                        VendorID = customer.ID,
                                        Contact = customer.ContactInfo,
                                        Status = customer.IsActive ? "Active" : "Inactive",
                                        ParticipantType = 1,
                                        ApplicationID = customer.ApplicationID ?? -1,
                                        BankInfoReviewStatus = string.Empty,
                                        BankInfoId = 0,
                                        CreatedDate = (null == customer.ConfirmationMailedDate) ? (customer.CreatedDate) : ((customer.ConfirmationMailedDate < legacyStartDate) ? (customer.ActivationDate ?? DateTime.MinValue) : (customer.ConfirmationMailedDate ?? DateTime.MinValue)),
                                    };

                var query = vendorQuery.Concat(customerQuery);//mS: 7097 totalRecords:11146 result.Count():700

                //Apply search text
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    DateTime dateValue;
                    if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                    {
                        if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                        {
                            var nextMonth = dateValue.AddMonths(1);
                            query = query.Where(c => (((DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)))
                                ));
                        }
                        else
                        {
                            query = query.Where(c => ((DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue))
                                ));
                        }
                    }
                    else
                    {
                        query = query.Where(c => c.Number.ToLower().Contains(searchText)
                                                || c.BusinessName.ToLower().Contains(searchText)
                                                || c.Contact.ToLower().Contains(searchText)
                                                || c.Status.ToLower().Contains(searchText)
                                                || c.BankInfoReviewStatus.ToLower().Contains(searchText)
                                                || DbFunctions.TruncateTime(c.CreatedDate).ToString().Contains(searchText) //OTSTM2-877
                                                );
                    }
                }

                //OTSTM2-931
                var columnSearchTextReg = columnSearchText["Reg"];
                var columnSearchTextCompany = columnSearchText["Company"];
                var columnSearchTextContact = columnSearchText["Contact"];
                var columnSearchTextApprovedDate = columnSearchText["ApprovedDate"];
                var columnSearchTextStatus = columnSearchText["Status"];
                var columnSearchTextBankStatus = columnSearchText["BankStatus"];

                if (!string.IsNullOrWhiteSpace(columnSearchTextReg))
                {
                    query = query.Where(c => c.Number.ToString().Contains(columnSearchTextReg));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
                {
                    query = query.Where(c => c.BusinessName.ToString().Contains(columnSearchTextCompany));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextContact))
                {
                    query = query.Where(c => c.Contact.ToString().Contains(columnSearchTextContact));
                }               
                if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
                {
                    query = query.Where(c => c.Status.ToString().Contains(columnSearchTextStatus));
                }
                if (!string.IsNullOrWhiteSpace(columnSearchTextBankStatus))
                {
                    query = query.Where(c => c.BankInfoReviewStatus.ToString().Contains(columnSearchTextBankStatus));
                }

                //Applying sorting
                query = sortdirection == "asc" ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");

                //OTSTM2-931 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
                var result = query.ToList();
                result.ForEach(i =>
                {
                    i.CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(i.CreatedDate, TimeZoneInfo.Local);
                });

                if (!string.IsNullOrWhiteSpace(columnSearchTextApprovedDate))
                {
                    result = result.Where(c => c.CreatedDate.ToString("yyyy-MM-dd").ToLower().Contains(columnSearchTextApprovedDate)).ToList();
                }

                return result;

            }

        }
    }
}
