﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DAL;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Extension;
using System.Data.Entity;
using CL.TMS.DataContracts.ViewModel.Metric;

namespace CL.TMS.Repository.System
{
    public class ReportingRepository : BaseRepository<ReportingBoundedContext, int>, IReportingRepository
    {
        public ReportingRepository()
        {
        }
        public IEnumerable<ReportCategory> GetAllReportCategories(int Id = 0)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.ReportCategories.Where(i => i.ID == Id || Id == 0).ToList();
        }
        public IEnumerable<Report> GetReportsByCategoryId(int ReportingCategoryId)
        {
            return GetAllReportCategories(ReportingCategoryId).FirstOrDefault().Reports;
        }
        public IEnumerable<RegistrantWeeklyReport> GetRegistrantWeeklyReport()
        {
            var db = context as ReportingBoundedContext;
            var registrants = db.Sp_Rpt_RegistrantWeeklyReport();
            return registrants.Select(ModelAdapter.ToRegistrantWeeklyReport).ToList();
        }
        public IEnumerable<SpRptTsfExtractInBatchOnlyReportGp> GetTsfExtrtactInBatchOnlyReportGp()
        {
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_TsfExtractInBatchOnlyReportGp().ToList();
        }
        public IEnumerable<SpRptProcessorTIPIReport> GetProcessorTIPIReport(DateTime? startDate, DateTime? endDate)
        {
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_ProcessorTIPIReport(startDate, endDate).ToList();
        }
        public IEnumerable<SpRptHaulerCollectorComparisonReport> GetHaulerCollectorComparisonReport(DateTime? startDate, DateTime? endDate, string registrationNumber)
        {
            var db = context as ReportingBoundedContext;
            return db.sp_Rpt_HaulerCollectorComparisonReport(startDate, endDate, registrationNumber).ToList();
        }
        public Report GetReport(int Id)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.Reports.FirstOrDefault(i => i.ID == Id);
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireVM> GetStewardRevenueSupplyNewTireTypes(ReportDetailsViewModel detail)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var dbContext = context as ReportingBoundedContext;

            var query = from claim in dbContext.TSFClaims
                        where ((claim.Period.StartDate >= startingDate) &&
                            (claim.Period.StartDate >= firstDayOfStartDate) && (claim.Period.EndDate <= lastDatOfEndDate))
                        select new StewardRptRevenueSupplyNewTireVM
                        {
                            //ID = claim.ID,
                            Period = claim.Period.ShortName,
                            regNo = claim.Customer.RegistrationNumber,
                            legalName = claim.Customer.BusinessName,
                            webSubmissionDate = claim.SubmissionDate,
                            RegistrantSubTypeID = claim.Customer.RegistrantSubTypeID,
                            tsfStatus = claim.RecordState,
                            receiptDate = claim.ReceiptDate,
                            depositDate = claim.DepositDate,
                            chequeAmount = claim.PaymentAmount ?? 0,
                            totalReceiveable = claim.TotalRemittancePayable - (claim.Credit ?? 0), //OTSTM2-432 new change
                            Subtotal = claim.TotalRemittancePayable,
                            penalties = (claim.PenaltiesManually != null) ? claim.PenaltiesManually : claim.Penalties,
                            interest = "N/A",
                            TSFClaimDetail = claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Select(i => new TSFClaimDetailsVM() { ItemShortName = i.Item.ShortName, TSFDue = i.TSFDue, TireSupplied = i.TireSupplied, NegativeAdjustment = i.NegativeAdjustment }),
                            tires = (claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Sum(i => i.TireSupplied) - claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Sum(i => i.NegativeAdjustment)),
                            registrantStatus = claim.Customer.IsActive == true ? "Active" : "Inactive",
                            statusChangeDatetime = claim.Customer.ActiveStateChangeDate,
                            remittancePayableVal = claim.TotalTSFDue,
                            creditPayableVal = claim.Credit ?? 0,
                            submittedBy = claim.CreatedUser,
                            ChequeEFT = claim.ChequeReferenceNumber,
                            BalanceDue = claim.BalanceDue ?? 0,
                            AdjustmentTotal = claim.AdjustmentTotal ?? 0,
                            ApplicableTaxesHst = claim.ApplicableTaxesHst
                        };

            if (detail.RegNumber > 0)
            {
                query = query.Where(i => i.regNo == detail.RegNumber.ToString());
            }

            return query.ToList();
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVMsp> GetStewardRevenueSupplyNewTireCreditType_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_StewardNewTireCredit(firstDayOfStartDate, lastDatOfEndDate, detail.RegNumber == 0 ? string.Empty : detail.RegNumber.ToString()).ToList();
        }

        [Obsolete]
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVM> GetStewardRevenueSupplyNewTireCreditTypes(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var dbContext = context as ReportingBoundedContext;

            var query = from claim in dbContext.TSFClaims
                        where ((claim.Period.StartDate > startingDate) &&
                            (claim.Period.StartDate >= firstDayOfStartDate) && (claim.Period.EndDate <= lastDatOfEndDate))
                            && (claim.Period.StartDate >= TSFNegAdjSwitchDate)
                        select new StewardRptRevenueSupplyNewTireCreditVM
                        {
                            //ID = claim.ID,
                            Period = claim.Period.ShortName,
                            regNo = claim.Customer.RegistrationNumber,
                            legalName = claim.Customer.BusinessName,
                            webSubmissionDate = claim.SubmissionDate,
                            RegistrantSubTypeID = claim.Customer.RegistrantSubTypeID,
                            tsfStatus = claim.RecordState,
                            receiptDate = claim.ReceiptDate,
                            depositDate = claim.DepositDate,
                            chequeAmount = claim.PaymentAmount ?? 0,
                            totalReceiveable = claim.Credit ?? 0,
                            taxesDue = "N/A",
                            penaltiesVal = (claim.PenaltiesManually != null) ? claim.PenaltiesManually : claim.Penalties,
                            interest = "N/A",
                            TSFClaimDetail = claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Select(i => new TSFClaimDetailsVM() { ItemShortName = i.Item.ShortName, TSFDue = i.TSFDue, CreditTSFDue = i.CreditTSFDue, CreditNegativeAdj = i.CreditNegativeAdj, NegativeAdjustment = i.NegativeAdjustment }),//TireSupplied = i.TireSupplied, 
                            tires = claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Sum(i => (i.CreditNegativeAdj + i.NegativeAdjustment)),
                            registrantStatus = claim.Customer.IsActive == true ? "Active" : "Inactive",
                            statusChangeDatetime = claim.Customer.ActiveStateChangeDate,
                            creditPayableVal = claim.Credit ?? 0,
                            submittedBy = claim.CreatedUser,
                            ChequeEFT = claim.ChequeReferenceNumber,
                            NegativeAdjDate = claim.Period.StartDate,//claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).FirstOrDefault().NegativeAdjDate,
                        };

            if (detail.RegNumber > 0)
            {
                query = query.Where(i => i.regNo == detail.RegNumber.ToString());
            }

            return query.ToList();
        }
        public IEnumerable<StewardRptRevenueSupplyOldTireVM> GetStewardRevenueSupplyOldTireTypes(ReportDetailsViewModel detail)
        {
            DateTime? endDate = detail.EndDate;

            DateTime? effectiveEndDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateOldTireEnd);

            if (endDate > effectiveEndDate)
                endDate = effectiveEndDate;

            DateTime StartDate = new DateTime(detail.StartDate.Value.Year, detail.StartDate.Value.Month, 1);
            endDate = new DateTime(endDate.Value.Year, endDate.Value.Month, 1).AddMonths(1).AddSeconds(-1);


            var dbContext = context as ReportingBoundedContext;

            var query = from claim in dbContext.TSFClaims
                        where (claim.Period.StartDate >= StartDate) && (claim.Period.EndDate <= endDate)
                        select new StewardRptRevenueSupplyOldTireVM
                        {
                            //ID = claim.ID,
                            period = claim.Period.ShortName,
                            regNo = claim.Customer.RegistrationNumber,
                            legalName = claim.Customer.BusinessName,
                            webSubmissionDate = claim.SubmissionDate,
                            RegistrantSubTypeID = claim.Customer.RegistrantSubTypeID,
                            tsfStatus = claim.RecordState,
                            receiptDate = claim.ReceiptDate,
                            DepositDate = claim.DepositDate,
                            chequeAmount = claim.PaymentAmount ?? 0,
                            totalReceiveable = claim.TotalRemittancePayable,//Total Receivable = Subtotal (From Payments Panel of TSF Remittance)
                            AdjustmentTotal = claim.AdjustmentTotal ?? 0,
                            ApplicableTaxesHst = claim.ApplicableTaxesHst,
                            penaltiesVal = (claim.PenaltiesManually != null) ? claim.PenaltiesManually : claim.Penalties,
                            penalties = claim.PenaltiesManually ?? claim.Penalties,
                            interest = "N/A",
                            TSFClaimDetail = claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Select(i => new TSFClaimDetailsVM() { ItemShortName = i.Item.ShortName, TSFDue = i.TSFDue, TireSupplied = i.TireSupplied, NegativeAdjustment = i.NegativeAdjustment }),
                            tires = claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Sum(i => i.TireSupplied) - claim.TSFClaimDetails.Where(i => i.TSFClaimID == claim.ID).Sum(i => i.NegativeAdjustment),
                            registrantStatus = claim.Customer.IsActive == true ? "Active" : "Inactive",
                            statusChangeDate = claim.Customer.ActiveStateChangeDate,
                            remittancePayableVal = claim.TotalTSFDue,//21. Remittance Payable = TSF Due (Where TSF Due= Subtotal + Penalties)
                            remittancePayable = claim.TotalTSFDue + claim.AdjustmentTotal ?? 0 - (claim.Credit ?? 0),
                            submittedBy = claim.CreatedUser,
                            ChequeEFT = claim.ChequeReferenceNumber,
                            BalanceDue = claim.BalanceDue ?? 0
                        };

            if (detail.RegNumber > 0)
            {
                query = query.Where(i => i.regNo == detail.RegNumber.ToString());
            }

            return query.ToList();
        }
        public IEnumerable<WebListingCollectorReportPostalVM> GetWebListingCollectorReportPostal(string postalCode)
        {
            var dbContext = context as ReportingBoundedContext;
            var businessId = (int)AddressTypeEnum.Business;
            var query = from vendor in dbContext.Vendors
                        where vendor.VendorType == 2
                        && vendor.IsActive == true
                        && vendor.PrimaryBusinessActivity != TreadMarksConstants.Generator
                        && vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().PostalCode.StartsWith(postalCode)
                        select new WebListingCollectorReportPostalVM
                        {
                            CompanyName‎ = string.IsNullOrEmpty(vendor.OperatingName) ? vendor.BusinessName : vendor.OperatingName,
                            AddressLine‎1‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address1,
                            AddressLine‎2‎ = string.IsNullOrEmpty(vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address2) ? "-" : vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2,
                            City‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().City,
                            Province‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Province,
                            PostalCode‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().PostalCode,
                            PLT = vendor.Items.Where(i => i.ID == 2).FirstOrDefault() != null ? "X" : "-",
                            MT = vendor.Items.Where(i => i.ID == 3).FirstOrDefault() != null ? "X" : "-",
                            AGLS = vendor.Items.Where(i => i.ID == 4).FirstOrDefault() != null ? "X" : "-",
                            IND = vendor.Items.Where(i => i.ID == 5).FirstOrDefault() != null ? "X" : "-",
                            SOTR = vendor.Items.Where(i => i.ID == 6).FirstOrDefault() != null ? "X" : "-",
                            MOTR = vendor.Items.Where(i => i.ID == 7).FirstOrDefault() != null ? "X" : "-",
                            LOTR = vendor.Items.Where(i => i.ID == 8).FirstOrDefault() != null ? "X" : "-",
                            GOTR = vendor.Items.Where(i => i.ID == 9).FirstOrDefault() != null ? "X" : "-",
                            RegistrationNumber = vendor.Number,
                        };

            return query.OrderBy(v => v.City).ThenBy(v => v.CompanyName).ToList();
        }
        public IEnumerable<WebListingHaulerReport> GetWebListingHaulerReport()
        {
            var dbContext = context as ReportingBoundedContext;
            var query = from vendor in dbContext.Vendors
                        where vendor.VendorType == 3 && vendor.IsActive == true
                        select new WebListingHaulerReport
                        {
                            CompanyName‎ = string.IsNullOrEmpty(vendor.OperatingName) ? vendor.BusinessName : vendor.OperatingName,
                            AddressLine‎1‎ = vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address1,
                            AddressLine‎2‎ = string.IsNullOrEmpty(vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2) ? "-" : vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2,
                            City‎ = vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().City,
                            Province‎ = vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Province,
                            PostalCode‎ = vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().PostalCode,
                            RegistrationNumber = vendor.Number,
                        };
            return query.OrderBy(v => v.CompanyName).ToList();
        }
        public IEnumerable<WebListingRPMReport> GetWebListingRPMReportPostal(string postalCode)
        {
            var dbContext = context as ReportingBoundedContext;
            var businessId = (int)AddressTypeEnum.Business;
            var query = from vendor in dbContext.Vendors
                        where vendor.VendorType == 5
                        && vendor.IsActive == true
                        //&& vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().PostalCode.StartsWith(postalCode)
                        select new WebListingRPMReport
                        {
                            CompanyName‎ = string.IsNullOrEmpty(vendor.OperatingName) ? vendor.BusinessName : vendor.OperatingName,
                            AddressLine‎1‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address1,
                            AddressLine‎2‎ = string.IsNullOrEmpty(vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address2) ? "-" : vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2,
                            City‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().City,
                            Province‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Province,
                            PostalCode‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().PostalCode,
                            RegistrationNumber = vendor.Number,
                        };

            return query.ToList();
        }
        public IEnumerable<WebListingProcessorReport> GetWebListingProcessorReportPostal(string postalCode)
        {
            var dbContext = context as ReportingBoundedContext;
            var businessId = (int)AddressTypeEnum.Business;
            var query = from vendor in dbContext.Vendors
                        where vendor.VendorType == 4
                        && vendor.IsActive == true
                        //&& vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().PostalCode.StartsWith(postalCode)
                        select new WebListingProcessorReport
                        {
                            CompanyName‎ = string.IsNullOrEmpty(vendor.OperatingName) ? vendor.BusinessName : vendor.OperatingName,
                            AddressLine‎1‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address1,
                            AddressLine‎2‎ = string.IsNullOrEmpty(vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address2) ? "-" : vendor.VendorAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2,
                            City‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().City,
                            Province‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Province,
                            PostalCode‎ = vendor.VendorAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().PostalCode,
                            PLT = vendor.Items.Where(i => i.ID == 2).FirstOrDefault() != null ? "X" : "-",
                            MT = vendor.Items.Where(i => i.ID == 3).FirstOrDefault() != null ? "X" : "-",
                            AGLS = vendor.Items.Where(i => i.ID == 4).FirstOrDefault() != null ? "X" : "-",
                            IND = vendor.Items.Where(i => i.ID == 5).FirstOrDefault() != null ? "X" : "-",
                            SOTR = vendor.Items.Where(i => i.ID == 6).FirstOrDefault() != null ? "X" : "-",
                            MOTR = vendor.Items.Where(i => i.ID == 7).FirstOrDefault() != null ? "X" : "-",
                            LOTR = vendor.Items.Where(i => i.ID == 8).FirstOrDefault() != null ? "X" : "-",
                            GOTR = vendor.Items.Where(i => i.ID == 9).FirstOrDefault() != null ? "X" : "-",
                            RegistrationNumber = vendor.Number,
                        };

            return query.ToList();
        }
        public IEnumerable<WebListingStewardReport> GetWebListingStewardReportPostal(string postalCode)
        {
            var dbContext = context as ReportingBoundedContext;
            var businessId = (int)AddressTypeEnum.Business;
            var query = from customer in dbContext.Customers
                        where customer.CustomerType == 1
                        && customer.IsActive == true
                        //&& customer.CustomerAddresses.Where(i => i.AddressType == 1).FirstOrDefault().PostalCode.StartsWith(postalCode)
                        select new WebListingStewardReport
                        {
                            CompanyName‎ = string.IsNullOrEmpty(customer.OperatingName) ? customer.BusinessName : customer.OperatingName,
                            AddressLine‎1‎ = customer.CustomerAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address1,
                            AddressLine‎2‎ = string.IsNullOrEmpty(customer.CustomerAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Address2) ? "-" : customer.CustomerAddresses.Where(i => i.AddressType == 1).FirstOrDefault().Address2,
                            City‎ = customer.CustomerAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().City,
                            Province‎ = customer.CustomerAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().Province,
                            PostalCode‎ = customer.CustomerAddresses.Where(i => i.AddressType == businessId).FirstOrDefault().PostalCode,
                            RegistrationNumber = customer.RegistrationNumber,
                        };

            return query.ToList();
        }
        public IEnumerable<HaulerTireMovementReport> GetHaulerTireMovementReport_sp(ReportDetailsViewModel detail)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_HaulerTireMovement(firstDayOfStartDate, lastDatOfEndDate, detail.RegNumber == 0 ? string.Empty : detail.RegNumber.ToString()).ToList();
        }

        public IEnumerable<HaulerTireMovementReportVM> GetHaulerTireMovementReportVM(ReportDetailsViewModel detail)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_HaulerTireMovementDataValidation(firstDayOfStartDate, lastDatOfEndDate, detail.RegNumber == 0 ? string.Empty : detail.RegNumber.ToString()).ToList();
        }
        public IEnumerable<CollectorTireOriginReport> GetCollectorTireOriginReport(ReportDetailsViewModel detail)
        {
            DateTime? startDate = null, endDate = null;
            DateTime tempDate;

            if (detail.StartDate != null && DateTime.TryParse(detail.StartDate.ToString(), out tempDate))
            {
                startDate = new DateTime(tempDate.Year, tempDate.Month, 1);
            }
            if (detail.EndDate != null && DateTime.TryParse(detail.EndDate.ToString(), out tempDate))
            {
                endDate = new DateTime(tempDate.Year, tempDate.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            return (context as ReportingBoundedContext).Sp_Rpt_CollectorTireOriginReport(startDate, endDate, detail.RegNumber == 0 ? string.Empty : detail.RegNumber.ToString()).ToList();
        }

        public IEnumerable<HaulerVolumeReport> GetHaulerVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            var db = context as ReportingBoundedContext;
            var result = db.sp_Rpt_HaulerVolumeReport(startDate, endDate);
            return result.Select(ModelAdapter.ToHaulerVolumeReport).ToList();
        }
        public IEnumerable<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> GetDetailHaulerVolumeReportBasedOnScaleWeight(DateTime? startDate, DateTime? endDate)
        {
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_DetailHaulerVolumeReportBasedOnScaleWeight(startDate, endDate).ToList();
        }
        public IEnumerable<SpRptProcessorVolumeReport> GetProcessorVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_ProcessorVolumeReport(startDate, endDate).ToList();
        }
        public IEnumerable<RpmData> GetRpmData(ReportDetailsViewModel detail)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var query = from claimInventoryDetail in dbContext.ClaimInventoryDetail.AsNoTracking()
                        join transaction in dbContext.Transactions on claimInventoryDetail.TransactionId equals transaction.ID
                        where ((claimInventoryDetail.PeriodStartDate >= firstDayOfStartDate) && (claimInventoryDetail.PeriodEndDate <= lastDatOfEndDate))
                        && (claimInventoryDetail.TransactionTypeCode == TreadMarksConstants.SPSR)
                        select new RpmData
                        {
                            TransactionId = claimInventoryDetail.TransactionId,
                            RegNumber = claimInventoryDetail.RegistrationNumber,
                            ClaimPeriodStart = claimInventoryDetail.PeriodStartDate,
                            ClaimPeriodDescription = claimInventoryDetail.PeriodName,
                            Approve = transaction.ProcessingStatus,
                            RecordModified = transaction.TransactionAdjustments.Any() ? true : false,
                            DateSold = transaction.TransactionDate,
                            EndUseMarketDesc = claimInventoryDetail.ItemShortName,
                            ProductOtherDesc = claimInventoryDetail.ItemCode ?? "0",
                            TransactionNotes = transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),
                            SalesInvoiceNum = transaction.InvoiceNumber,
                            Purchaser = (transaction.CompanyInfo != null) ? (transaction.CompanyInfo.CompanyName ?? string.Empty) : string.Empty,
                            RpmActualWeight = claimInventoryDetail.ActualWeight,
                            RpmRate = claimInventoryDetail.TransactionItemRate,
                            TransactionNumber = transaction.FriendlyId.ToString(),
                        };

            //var query = from transaction in dbContext.Transactions
            //            join claimDetail in dbContext.ClaimDetail on transaction.ID equals claimDetail.TransactionId
            //            where (transaction.TransactionTypeCode == TreadMarksConstants.SPSR)
            //            && ((claimDetail.Claim.ClaimPeriod.StartDate >= firstDayOfStartDate) && (claimDetail.Claim.ClaimPeriod.EndDate <= lastDatOfEndDate))
            //            //&& (claimDetail.Claim.Participant.VendorType == 5)
            //            select new RpmData
            //            {
            //                TransactionId = transaction.ID,
            //                RegNumber = claimDetail.Claim.Participant.Number,
            //                ClaimPeriodStart = claimDetail.Claim.ClaimPeriod.StartDate,
            //                ClaimPeriodDescription = claimDetail.Claim.ClaimPeriod.ShortName,
            //                Approve = transaction.ProcessingStatus,
            //                RecordModified = transaction.TransactionAdjustments.Any() ? true : false,
            //                DateSold = transaction.TransactionDate,
            //                SalesInvoiceNum = transaction.InvoiceNumber,
            //                TransactionNotes = transaction.TransactionNotes.OrderByDescending(a => a.CreatedDate).ToList(),


            //                Purchaser = (transaction.CompanyInfo != null) ? (transaction.CompanyInfo.CompanyName ?? string.Empty) : string.Empty,
            //                EndUseMarketDesc = transaction.TransactionItems.FirstOrDefault().Item.ShortName,
            //                ProductOtherDesc = transaction.TransactionItems.FirstOrDefault().ItemCode ?? "0",
            //                RpmRate = transaction.TransactionItems.FirstOrDefault().Rate ?? 0,
            //                RecycledMaterialUsed = transaction.TransactionItems.FirstOrDefault().ActualWeight,
            //            };

            //var transIdList = query.Select(i => i.TransactionId).ToList();
            //List<TransactionAdjustment> acceptedAdjustmentList = dbContext.TransactionAdjustments.Where(i => transIdList.Contains(i.OriginalTransactionId) && i.Status == TransactionAdjustmentStatus.Accepted.ToString()).ToList();

            //acceptedAdjustmentList.ForEach(adj =>
            //{
            //    var transaction = query.SingleOrDefault(i => i.TransactionId == adj.OriginalTransactionId);

            //    if (transaction != null)
            //    {
            //        transaction.EndUseMarketDesc = adj.TransactionItems.FirstOrDefault().Item.ShortName;
            //        transaction.ProductOtherDesc = adj.TransactionItems.FirstOrDefault().ItemCode ?? "0";
            //        transaction.RpmRate = adj.TransactionItems.FirstOrDefault().Rate ?? 0;
            //        transaction.RecycledMaterialUsed = adj.TransactionItems.FirstOrDefault().ActualWeight;
            //        transaction.Purchaser = (adj.CompanyInfo != null) ? (adj.CompanyInfo.CompanyName ?? transaction.Purchaser) : transaction.Purchaser;
            //    }
            //});

            query.OrderBy(c => c.RegNumber).ThenBy(c => c.ClaimPeriodStart).ThenBy(c => c.DateSold);

            return query;
        }
        public IEnumerable<RpmCumulative> GetRpmCumulative(ReportDetailsViewModel detail)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var query = from claim in dbContext.Claim.AsNoTracking()
                        where ((claim.ClaimPeriod.StartDate >= firstDayOfStartDate) && (claim.ClaimPeriod.EndDate <= lastDatOfEndDate))
                        && (claim.Participant.VendorType == 5)
                        select new RpmCumulative
                        {
                            RegNumber = claim.Participant.Number,
                            ActiveStatus = claim.Participant.IsActive ? "Active" : "Inactive",
                            ClaimPeriodName = claim.ClaimPeriod.ShortName,
                            ClaimStatus = claim.Status,
                            ClaimSubmissionDate = claim.SubmissionDate,
                            ClaimStartedDate = claim.ReviewStartDate,
                            Subtotal = (claim.ClaimsAmountTotal??0)-(claim.TotalTax??0), //include hst
                            HST =claim.TotalTax??0,                                              
                            AmountPayable= claim.ClaimsAmountTotal??0,
                            IsTaxApplicable = claim.IsTaxApplicable,
                            OpeningInventoryVal = claim.ClaimSummary.TotalOpening ?? 0,
                            ClosingInventoryVal = claim.ClaimSummary.TotalClosing ?? 0,
                            AssignedToName = claim.AssignedToUser == null ? string.Empty : claim.AssignedToUser.FirstName + " " + claim.AssignedToUser.LastName,
                            SubmittedBy = claim.SubmittedUser != null ? claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName : string.Empty,
                        };

            query.OrderBy(c => c.RegNumber).ThenBy(c => c.ClaimStartedDate).ThenBy(c => c.ClaimSubmissionDate);

            return query.ToList();
        }
        public IEnumerable<ProcessorDispositionOfResidual> GetProcessorDispositionOfResidual(List<TypeDefinition> dispositionReasons, List<TypeDefinition> materialTypeList)
        {
            var dbContext = context as ReportingBoundedContext;
            var rows = from claim in dbContext.Claim
                       join claimDetail in dbContext.ClaimDetail on claim.ID equals claimDetail.ClaimId
                       join adjustment in dbContext.TransactionAdjustments on claimDetail.TransactionId equals adjustment.OriginalTransactionId into adj
                       from subAdj in adj.DefaultIfEmpty()
                       where claim.Participant.VendorType == 4 && claimDetail.Transaction.TransactionTypeCode == "DOR" && claimDetail.Transaction.ProcessingStatus != "Invalidated"
                       && (subAdj == null || subAdj.Status == "Accepted")
                       select new
                       {
                           ReportingPeriod = claim.ClaimPeriod.StartDate,
                           ProcessorRegNumber = claim.Participant.Number,
                           ProcessorsLegalName = claim.Participant.OperatingName,
                           Status = claim.Participant.IsActive ? "Active" : "Inactive",
                           RecordStatus = claim.Status,
                           DateOfDisposition = claimDetail.Transaction.TransactionDate,
                           NameOfDispositionSite = claimDetail.Transaction.VendorInfo.Number ?? (claimDetail.Transaction.CompanyInfo != null ? claimDetail.Transaction.CompanyInfo.CompanyName : string.Empty),
                           Address = (null != claimDetail.Transaction.CompanyInfo.Address.Country) ? string.Concat(claimDetail.Transaction.CompanyInfo.Address.City, ", ", claimDetail.Transaction.CompanyInfo.Address.Province, ", ", claimDetail.Transaction.CompanyInfo.Address.Country)
                                            : string.Concat(claimDetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City, ", ",
                                                                    claimDetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Province, ", ",
                                                                    claimDetail.Transaction.VendorInfo.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Country),
                           DispositionId = subAdj == null ? claimDetail.Transaction.DispositionReason : subAdj.DispositionReason,
                           MaterialType = subAdj == null ? claimDetail.Transaction.MaterialType : subAdj.MaterialType,
                           ScaleTicketInvoiceNumber = (subAdj == null ? (claimDetail.Transaction.ScaleTickets.Any() ? claimDetail.Transaction.ScaleTickets.FirstOrDefault().TicketNumber + " " : string.Empty)
                            : (subAdj.ScaleTickets.Any() ? subAdj.ScaleTickets.FirstOrDefault().TicketNumber + " " : string.Empty)) + claimDetail.Transaction.InvoiceNumber,
                           ScaleWeightTonnes = dbContext.ClaimPaymentDetail.Any(i => i.TransactionId == claimDetail.TransactionId) ? dbContext.ClaimPaymentDetail.Where(i => i.TransactionId == claimDetail.TransactionId).Sum(i => i.ActualWeight) :
                           (subAdj == null ? (claimDetail.Transaction.ScaleTickets.Any() ? claimDetail.Transaction.ScaleTickets.FirstOrDefault().InboundWeight - claimDetail.Transaction.ScaleTickets.FirstOrDefault().OutboundWeight : (claimDetail.Transaction.OnRoadWeight + claimDetail.Transaction.OffRoadWeight)) :
                            subAdj.ScaleTickets.Any() ? subAdj.ScaleTickets.FirstOrDefault().InboundWeight - subAdj.ScaleTickets.FirstOrDefault().OutboundWeight : ((subAdj.OnRoadWeight ?? 0) + (subAdj.OffRoadWeight ?? 0))),
                           TotalEstimatedWeight = dbContext.ClaimPaymentDetail.Any(i => i.TransactionId == claimDetail.TransactionId) ? dbContext.ClaimPaymentDetail.Where(i => i.TransactionId == claimDetail.TransactionId).Sum(i => i.AverageWeight) : 0,
                           PaymentAdjustment = dbContext.ClaimPaymentDetail.Any(i => i.TransactionId == claimDetail.TransactionId) ? dbContext.ClaimPaymentDetail.Where(i => i.TransactionId == claimDetail.TransactionId).Sum(i => i.Amount) : 0,
                           ClaimId = claim.ID,
                           TransactionId = claimDetail.TransactionId
                       };
            var result = new List<ProcessorDispositionOfResidual>();

            var items = (from claimPaymentDetail in dbContext.ClaimPaymentDetail
                         where claimPaymentDetail.TransactionTypeCode == "DOR"
                         select claimPaymentDetail).ToList();
            rows.ToList().ForEach(i =>
            {
                var dispositionId = i.DispositionId;
                var materialTypeId = i.MaterialType;
                var PLT = 0;
                var MT = 0;
                var AGLS = 0;
                var IND = 0;
                var SOTR = 0;
                var MOTR = 0;
                var LOTR = 0;
                var GOTR = 0;
                if (i.MaterialType > 0 && materialTypeList.FirstOrDefault(d => d.DefinitionValue == materialTypeId).Name == "Used Tire Sale")
                {
                    PLT = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "PLT" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    MT = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "MT" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    AGLS = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "AGLS" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    IND = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "IND" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    SOTR = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "SOTR" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    MOTR = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "MOTR" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    LOTR = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "LOTR" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                    GOTR = items.Where(x => x.ClaimId == i.ClaimId && x.ItemName == "GOTR" && x.TransactionId == i.TransactionId).Select(s => s.Quantity).Sum();
                }
                result.Add(new ProcessorDispositionOfResidual()
                {
                    ReportingPeriod = i.ReportingPeriod,
                    ProcessorRegNumber = i.ProcessorRegNumber,
                    ProcessorsLegalName = i.ProcessorsLegalName,
                    Status = i.Status,
                    RecordStatus = i.RecordStatus,
                    DateOfDisposition = i.DateOfDisposition,
                    NameOfDispositionSite = i.NameOfDispositionSite,
                    Address = i.Address,
                    DispositionReason = dispositionId > 0 ? dispositionReasons.FirstOrDefault(d => d.DefinitionValue == dispositionId).Description : string.Empty,
                    MaterialType = i.MaterialType > 0 ? materialTypeList.FirstOrDefault(d => d.DefinitionValue == materialTypeId).Name : string.Empty,
                    ScaleTicketInvoiceNumber = i.ScaleTicketInvoiceNumber,
                    ScaleWeightTonnes = DataConversion.ConvertKgToTon((double)i.ScaleWeightTonnes),
                    TotalEstimatedWeight = i.MaterialType == 5 ? i.TotalEstimatedWeight : 0, //Est Weight only applies to Used Tire Sales
                    PaymentAdjustment = Math.Round(i.PaymentAdjustment, 2, MidpointRounding.AwayFromZero),
                    PLT = PLT,
                    MT = MT,
                    AGLS = AGLS,
                    IND = IND,
                    SOTR = SOTR,
                    MOTR = MOTR,
                    LOTR = LOTR,
                    GOTR = GOTR
                });
            });
            return result;
        }
        public IEnumerable<StewardNonFilers> GetStewardNonFilers(DateTime? startDate, DateTime? endDate)
        {
            var dbContext = context as ReportingBoundedContext;
            var query = from customer in dbContext.Customers
                        where !(from claim in dbContext.TSFClaims
                                where ((startDate ?? claim.Period.StartDate) <= claim.Period.StartDate && claim.Period.EndDate <= (endDate ?? claim.Period.EndDate))
                                select claim.CustomerID
                                ).Contains(customer.ID)
                        select new
                        {
                            RegistrationNumber = customer.RegistrationNumber,
                            RegistrationType = "Steward",
                            LegalName = customer.BusinessName,
                            OperatingName = customer.OperatingName,
                            BusinessStartDate = customer.BusinessStartDate,

                            //OTSTM2-375 Add ReportingFrequency and remove GracePeriodStartDate, GracePeriodEndDate
                            //GracePeriodStartDate = customer.GracePeriodStartDate,
                            //GracePeriodEndDate = customer.GracePeriodEndDate,

                            //RemitsPerYear = customer.RemitsPerYear,
                            RemitsPerYear =
                            customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == (startDate.HasValue ? startDate.Value.Year : c.ChangeDate.Year)).OrderByDescending(c => c.ChangeDate).FirstOrDefault() != null
                            ? customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == (startDate.HasValue ? startDate.Value.Year : c.ChangeDate.Year)).OrderByDescending(c => c.ChangeDate).FirstOrDefault().NewValue == "12x"
                            :
                            customer.CustomerSettingHistoryList.Where(c => c.ChangeDate >= (startDate ?? c.ChangeDate)).OrderBy(c => c.ChangeDate).FirstOrDefault() != null
                            ? customer.CustomerSettingHistoryList.Where(c => c.ChangeDate >= (startDate ?? c.ChangeDate)).OrderBy(c => c.ChangeDate).FirstOrDefault().OldValue == "12x"
                            : customer.RemitsPerYear,
                            RegistrantStatus = customer.IsActive ? "Active" : "Inactive",
                            StatusChangeDate = customer.ActiveStateChangeDate,
                            CustomerAddresses = customer.CustomerAddresses
                        };
            var result = new List<StewardNonFilers>();
            query.ToList().ForEach(i =>
            {
                var primaryContact = RepositoryHelper.FindPrimaryContact(i.CustomerAddresses.ToList());
                var businessAddress = i.CustomerAddresses.FirstOrDefault(a => a.AddressType == (int)AddressTypeEnum.Business) ?? new Address();
                result.Add(new StewardNonFilers()
                {
                    //Period = i.Period,
                    RegistrationNumber = i.RegistrationNumber,
                    RegistrationType = i.RegistrationType,
                    LegalName = i.LegalName,
                    OperatingName = i.OperatingName,
                    BusinessStartDate = i.BusinessStartDate,

                    //OTSTM2-375 Add ReportingFrequency and remove GracePeriodStartDate, GracePeriodEndDate
                    //GracePeriodStartDate = i.GracePeriodStartDate,
                    //GracePeriodEndDate = i.GracePeriodEndDate,
                    ReportingFrequency = i.RemitsPerYear.Value ? "12x" : "2x",
                    RegistrantStatus = i.RegistrantStatus,
                    StatusChangeDate = i.StatusChangeDate,
                    ContactNumber = primaryContact.PhoneNumber,
                    ContactName = string.Format("{0} {1}", primaryContact.FirstName, primaryContact.LastName),
                    ContactEmail = primaryContact.Email,
                    ContactPosition = primaryContact.Position,
                    BusinessPostalCode = businessAddress.PostalCode,
                    BusinessEmail = businessAddress.Email,
                });
            });
            return result;
        }
        public IEnumerable<TSFRptOnlinePaymentsOutstandingVM> GetTSFOnlinePaymentsOutstanding(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate)
        {
            var dbContext = context as ReportingBoundedContext;
            var query = from claim in dbContext.TSFClaims
                        where (claim.RecordState != "Approved")//(claim.BalanceDue != 0) && (null != claim.BalanceDue) && 
                        select new TSFRptOnlinePaymentsOutstandingVM
                        {
                            ReportingPeriod = claim.Period.ShortName,
                            RegistrationNbr = claim.Customer.RegistrationNumber,
                            ActiveStatus = (claim.Customer.IsActive == true) ? "Active" : "InActive",
                            LegalName = claim.Customer.BusinessName,
                            SubmissionDate = claim.SubmissionDate,
                            SubmittedBy = claim.CreatedUser,
                            PrimaryContactFname = claim.Customer.CustomerAddresses.Where(i => (i.AddressType == (int)AddressTypeEnum.Business || i.AddressType == (int)AddressTypeEnum.Contact) && i.Contacts.Any(j => (bool)j.IsPrimary)).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).FirstName,
                            PrimaryContactLname = claim.Customer.CustomerAddresses.Where(i => (i.AddressType == (int)AddressTypeEnum.Business || i.AddressType == (int)AddressTypeEnum.Contact) && i.Contacts.Any(j => (bool)j.IsPrimary)).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).LastName,
                            //ContactTel = claim.Customer.ContactInfo.Substring(SqlFunctions.CharIndex(",", claim.Customer.ContactInfo) + 1 ?? 0, claim.Customer.ContactInfo.Length) + " " + claim.Customer.CustomerAddresses.Where(i => i.AddressType == 3).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).PhoneNumber ?? string.Empty,
                            ContactTel = claim.Customer.CustomerAddresses.Where(i => (i.AddressType == (int)AddressTypeEnum.Business || i.AddressType == (int)AddressTypeEnum.Contact) && i.Contacts.Any(j => (bool)j.IsPrimary)).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).PhoneNumber,
                            ContactTelExt = claim.Customer.CustomerAddresses.Where(i => (i.AddressType == (int)AddressTypeEnum.Business || i.AddressType == (int)AddressTypeEnum.Contact) && i.Contacts.Any(j => (bool)j.IsPrimary)).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).Ext,
                            BalanceDue = claim.BalanceDue ?? 0,
                            PaymentType = (claim.PaymentType == "C") ? "Cheque" : ((claim.PaymentType == "O") ? "Online" : (((claim.PaymentType == "E") ? "EFT" : string.Empty))),
                            RemittanceStatus = claim.RecordState,
                        };

            //var look = query.Where(k=>k.RegistrationNbr == "1005920").ToList();
            return query.OrderBy(v => v.RegistrationNbr).ThenBy(v => v.ReportingPeriod).ToList();
        }
        public IEnumerable<CollectorCumulativeReportVM> GetCollectorCumulativeReport(ReportDetailsViewModel model)
        {
            var dbContext = context as ReportingBoundedContext;
            var query = from claim in dbContext.Claim
                        where (((model.StartDate <= claim.SubmissionDate && claim.SubmissionDate <= model.EndDate) && (claim.SubmissionDate != null)) && (claim.Participant.VendorType == (int)ItemParticipantTypeEnum.Collector))
                        select new CollectorCumulativeReportVM
                        {
                            RegistrationNumber = claim.Participant.Number,
                            ClaimPeriod = claim.ClaimPeriod.ShortName,
                            DateSubmitted = claim.SubmissionDate,
                            Status = claim.Status,
                            HST = claim.TotalTax,
                            AmountWithHST = claim.ClaimsAmountTotal,
                            OperatingNameWeb = "N/A",
                            //OperatingNameTM = claim.Participant.BusinessName,
                            OperatingNameTM = claim.Participant.OperatingName,
                            MailedDate = (null == claim.Participant.ConfirmationMailedDate) ? (claim.Participant.CreatedDate) : ((claim.Participant.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (claim.Participant.ActivationDate ?? DateTime.MinValue) : (claim.Participant.ConfirmationMailedDate ?? DateTime.MinValue)),
                            ActiveState = claim.Participant.IsActive ? "Active" : "InActive",
                            InactiveDate = (claim.Participant.IsActive) ? null : claim.Participant.ActiveStateChangeDate,
                            //UserEmail = claim.Participant.VendorAddresses.Where(i => i.AddressType == (int)AddressTypeEnum.Business).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).Email ?? "System",
                            UserEmail = claim.Participant.VendorAddresses.Where(i => (i.AddressType == (int)AddressTypeEnum.Business || i.AddressType == (int)AddressTypeEnum.Contact) && i.Contacts.Any(j => (bool)j.IsPrimary)).FirstOrDefault().Contacts.FirstOrDefault(c => (bool)c.IsPrimary).Email,
                            DateFinalizedVal = claim.ApprovalDate,
                            FlagFinalized = "N/A",
                            DueDate = claim.ChequeDueDate
                        };

            return query.ToList();
        }
        public IEnumerable<HaulerTireCollectionReport> GetHaulerTireCollectionReport(ReportDetailsViewModel model)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime? firstDayOfStartDate = null, lastDatOfEndDate = null;
            if (DateTime.TryParse(model.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(model.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }
            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_HaulerTireCollectionReport(firstDayOfStartDate, lastDatOfEndDate, model.RegNumber == 0 ? string.Empty : model.RegNumber.ToString()).ToList();
        }

        #region Private Methods
        private IQueryable<Vendor> GetAllVendors()
        {
            return (context as ReportingBoundedContext).Vendors;
        }
        private IQueryable<Customer> GetAllCustomers()
        {
            return (context as ReportingBoundedContext).Customers;
        }
        #endregion

        #region OTSTM2-215
        public List<CollectionGenerationOfTiresFSAProcessing> GetCollectionGenerationOfTiresBasedOnFSAReport(DateTime? fromDate, DateTime? toDate, List<string> allFSAList)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(fromDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(toDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            //this query "come from the Hauler claims side as there is more complete data" (that's why no Vendor type involved, use transaction direction == 1 to select only inBound transaction)
            var query = from transaction in dbContext.Transactions
                        join transactionItem in dbContext.TransactionItems on transaction.ID equals transactionItem.TransactionId
                        join item in dbContext.Items on transactionItem.ItemID equals item.ID
                        join claimDetail in dbContext.ClaimDetail on transaction.ID equals claimDetail.TransactionId
                        join regionDetail in dbContext.RegionDetails on transaction.FromPostalCode.ToString().Substring(0, 3).ToUpper() equals regionDetail.FSA
                        where (claimDetail.Direction == true && ((transaction.TransactionTypeCode == TreadMarksConstants.TCR) || (transaction.TransactionTypeCode == TreadMarksConstants.UCR) || (transaction.TransactionTypeCode == TreadMarksConstants.STC) || (transaction.TransactionTypeCode == TreadMarksConstants.DOT)))
                        && (claimDetail.Claim.ClaimPeriod.StartDate >= firstDayOfStartDate && claimDetail.Claim.ClaimPeriod.EndDate <= lastDatOfEndDate) && allFSAList.Contains(transaction.FromPostalCode.ToString().Substring(0, 2).ToUpper())
                        && transaction.Status == TransactionStatus.Completed.ToString()
                        && (transaction.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString())
                        select new CollectionGenerationOfTiresFSAProcessing
                        {
                            FromPostalCode = transaction.FromPostalCode.Substring(0, 2).ToUpper(),
                            ShortName = item.ShortName,
                            RegionId = regionDetail.RegionId,
                            Quantity =
                            transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                       transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault(c => c.ItemID == item.ID).Quantity ?? 0
                                       : transaction.TransactionItems.FirstOrDefault(c => c.ItemID == item.ID && c.TransactionAdjustmentId == null).Quantity ?? 0,
                            transactionID = transaction.ID //introduce this ID to avoid duplicated records in query result
                        };
            //above where condition, allFSAList.Contains(transaction.FromPostalCode.ToString().Substring(0, 2).ToUpper()) may cause duplicated records, add above "transactionNumber = transaction.FriendlyId" and followed Distinct() to avoid duplicated records          

            return query.Distinct().ToList();
        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, List<string> postalList, string fromDate, string toDate)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(fromDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(toDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var query = from transaction in dbContext.Transactions
                        join claimDetail in dbContext.ClaimDetail on transaction.ID equals claimDetail.TransactionId
                        where ((transaction.TransactionTypeCode == TreadMarksConstants.TCR && claimDetail.Direction == true) || (transaction.TransactionTypeCode == TreadMarksConstants.UCR) || (transaction.TransactionTypeCode == TreadMarksConstants.STC) || (transaction.TransactionTypeCode == TreadMarksConstants.DOT && claimDetail.Direction == true))
                        && (claimDetail.Claim.ClaimPeriod.StartDate >= firstDayOfStartDate && claimDetail.Claim.ClaimPeriod.EndDate <= lastDatOfEndDate) && postalList.Contains(transaction.FromPostalCode.ToString().Substring(0, 3).ToUpper())
                        && transaction.Status == TransactionStatus.Completed.ToString()
                        && (transaction.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString())
                        select new CollectionGenerationOfTiresBasedOnFSAReportDetailsVM
                        {
                            HaulerRegistrationNumber = transaction.IncomingVendor != null ? transaction.IncomingVendor.Number : "",
                            HaulerName = transaction.IncomingVendor != null ? transaction.IncomingVendor.BusinessName : "",
                            PickupDate = transaction.TransactionDate,
                            TransactionType = transaction.TransactionTypeCode,
                            FormNumber = transaction.FriendlyId.ToString(),
                            RegistrationNumber = transaction.OutgoingVendor != null ? transaction.OutgoingVendor.Number : "",
                            CollectorName = transaction.OutgoingVendor != null ? transaction.OutgoingVendor.BusinessName : (transaction.CompanyInfo != null ? transaction.CompanyInfo.CompanyName : ""),
                            PostalCode = transaction.FromPostalCode,
                            PLT = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "PLT").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "PLT").FirstOrDefault().Quantity ?? 0,
                            MT = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "MT").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "MT").FirstOrDefault().Quantity ?? 0,
                            AGLS = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "AGLS").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "AGLS").FirstOrDefault().Quantity ?? 0,
                            IND = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "IND").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "IND").FirstOrDefault().Quantity ?? 0,
                            SOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "SOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "SOTR").FirstOrDefault().Quantity ?? 0,
                            MOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "MOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "MOTR").FirstOrDefault().Quantity ?? 0,
                            LOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "LOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "LOTR").FirstOrDefault().Quantity ?? 0,
                            GOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "GOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "GOTR").FirstOrDefault().Quantity ?? 0,
                        };

            //Applying sorting
            var returnVal = sortDirection == "asc" ? query.OrderBy(orderBy).ToList() : query.OrderBy(orderBy + " descending").ToList();

            var totalRecords = returnVal.Count();

            return new PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int>
            {
                DTOCollection = returnVal,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        #endregion

        #region OTSTM2-1226
        public List<CollectionGenerationOfTiresBasedOnRateGroupProcessingVM> GetCollectionGenerationOfTiresBasedOnRateGroup(DateTime? fromDate, DateTime? toDate, List<int> rateGroupIdList)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(fromDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(toDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            //this query "come from the Hauler claims side as there is more complete data" (that's why no Vendor type involved, use transaction direction == 1 to select only inBound transaction)

            var query = from transaction in dbContext.Transactions
                        join transactionItem in dbContext.TransactionItems on transaction.ID equals transactionItem.TransactionId
                        join item in dbContext.Items on transactionItem.ItemID equals item.ID
                        join claimDetail in dbContext.ClaimDetail on transaction.ID equals claimDetail.TransactionId
                        from rateTransaction in dbContext.RateTransactions
                        join vendorRateGroup in dbContext.VendorRateGroups on rateTransaction.PickupRateGroupId equals vendorRateGroup.RateGroupId
                        where (claimDetail.Direction == true && ((transaction.TransactionTypeCode == TreadMarksConstants.TCR) || (transaction.TransactionTypeCode == TreadMarksConstants.UCR) || (transaction.TransactionTypeCode == TreadMarksConstants.STC) || (transaction.TransactionTypeCode == TreadMarksConstants.DOT)))
                        && (claimDetail.Claim.ClaimPeriod.StartDate >= firstDayOfStartDate && claimDetail.Claim.ClaimPeriod.EndDate <= lastDatOfEndDate) && rateGroupIdList.Contains(vendorRateGroup.RateGroupId.Value)
                        && transaction.Status == TransactionStatus.Completed.ToString()
                        && (transaction.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString())
                        && (claimDetail.Claim.ClaimPeriod.StartDate >= rateTransaction.EffectiveStartDate && claimDetail.Claim.ClaimPeriod.EndDate <= rateTransaction.EffectiveEndDate)
                        select new CollectionGenerationOfTiresBasedOnRateGroupProcessingVM
                        {
                            //VendorId = vendorRateGroup.VendorId.Value,
                            ShortName = item.ShortName,
                            GroupId = vendorRateGroup.GroupId,
                            RateGroupId = vendorRateGroup.RateGroupId.Value,
                            EffectiveStartDate = rateTransaction.EffectiveStartDate,
                            EffectiveEndDate = rateTransaction.EffectiveEndDate,
                            Quantity =
                            transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                       transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.FirstOrDefault(c => c.ItemID == item.ID).Quantity ?? 0
                                       : transaction.TransactionItems.FirstOrDefault(c => c.ItemID == item.ID && c.TransactionAdjustmentId == null).Quantity ?? 0,
                            transactionID = transaction.ID //introduce this ID to avoid duplicated records in query result
                        };
            //above where condition, allFSAList.Contains(transaction.FromPostalCode.ToString().Substring(0, 2).ToUpper()) may cause duplicated records, add above "transactionNumber = transaction.FriendlyId" and followed Distinct() to avoid duplicated records

            return query.Distinct().ToList();
        }

        public RateTransaction GetStartRateTransaction(DateTime firstDayOfStartDate)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.RateTransactions.FirstOrDefault(r => DbFunctions.TruncateTime(r.EffectiveStartDate) <= DbFunctions.TruncateTime(firstDayOfStartDate) && r.Category == 3 && r.PickupRateGroupId.HasValue);
        }

        public RateTransaction GetEndRateTransaction(DateTime lastDatOfEndDate)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.RateTransactions.Where(r => DbFunctions.TruncateTime(r.EffectiveEndDate) >= DbFunctions.TruncateTime(lastDatOfEndDate) && r.Category == 3 && r.PickupRateGroupId.HasValue).OrderBy(r => r.EffectiveEndDate).FirstOrDefault();
        }

        public RateTransaction GetFirstRateTransaction()
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.RateTransactions.FirstOrDefault(r => r.Category == 3 && r.PickupRateGroupId.HasValue);
        }

        public List<RateTransaction> GetRateTransactionList(RateTransaction startRateTransaction, RateTransaction endRateTransaction)
        {
            var dbContext = context as ReportingBoundedContext;
            if (startRateTransaction != null)
            {
                return dbContext.RateTransactions.Where(r => r.ID >= startRateTransaction.ID && r.ID <= endRateTransaction.ID && r.Category == 3 && r.PickupRateGroupId.HasValue).Select(r => r).ToList();
            }
            else
            {
                return dbContext.RateTransactions.Where(r => r.ID <= endRateTransaction.ID && r.Category == 3 && r.PickupRateGroupId.HasValue).Select(r => r).ToList();
            }
        }

        public List<VendorRateGroup> GetVendorRateGroupList(int rateGroupId)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.VendorRateGroups.Where(c => c.RateGroupId == rateGroupId).ToList();
        }

        public List<int> GetVendorIdList(string pickupRateGroupName, string collectorGroupName)
        {
            var dbContext = context as ReportingBoundedContext;
            var result = dbContext.VendorRateGroups.Where(v => v.RateGroup.RateGroupName == pickupRateGroupName && v.Group.GroupName == collectorGroupName).Select(v => v.VendorId.Value).ToList();
            return result;
        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailByRateGroupHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, List<int> vendorIdList, string fromDate, string toDate)
        {
            var dbContext = context as ReportingBoundedContext;

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(fromDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(toDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var query = from transaction in dbContext.Transactions
                        join claimDetail in dbContext.ClaimDetail on transaction.ID equals claimDetail.TransactionId
                        where (claimDetail.Direction == true && ((transaction.TransactionTypeCode == TreadMarksConstants.TCR) || (transaction.TransactionTypeCode == TreadMarksConstants.UCR) || (transaction.TransactionTypeCode == TreadMarksConstants.STC) || (transaction.TransactionTypeCode == TreadMarksConstants.DOT)))
                        && (claimDetail.Claim.ClaimPeriod.StartDate >= firstDayOfStartDate && claimDetail.Claim.ClaimPeriod.EndDate <= lastDatOfEndDate) && vendorIdList.Contains(transaction.OutgoingId.Value)
                        && transaction.Status == TransactionStatus.Completed.ToString()
                        && (transaction.ProcessingStatus == TransactionProcessingStatus.Approved.ToString() || transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString())
                        select new CollectionGenerationOfTiresBasedOnFSAReportDetailsVM
                        {
                            HaulerRegistrationNumber = transaction.IncomingVendor != null ? transaction.IncomingVendor.Number : "",
                            HaulerName = transaction.IncomingVendor != null ? transaction.IncomingVendor.BusinessName : "",
                            PickupDate = transaction.TransactionDate,
                            TransactionType = transaction.TransactionTypeCode,
                            FormNumber = transaction.FriendlyId.ToString(),
                            RegistrationNumber = transaction.OutgoingVendor != null ? transaction.OutgoingVendor.Number : "",
                            CollectorName = transaction.OutgoingVendor != null ? transaction.OutgoingVendor.BusinessName : (transaction.CompanyInfo != null ? transaction.CompanyInfo.CompanyName : ""),
                            PostalCode = transaction.FromPostalCode,
                            PLT = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "PLT").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "PLT").FirstOrDefault().Quantity ?? 0,
                            MT = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "MT").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "MT").FirstOrDefault().Quantity ?? 0,
                            AGLS = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "AGLS").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "AGLS").FirstOrDefault().Quantity ?? 0,
                            IND = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "IND").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "IND").FirstOrDefault().Quantity ?? 0,
                            SOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "SOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "SOTR").FirstOrDefault().Quantity ?? 0,
                            MOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "MOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "MOTR").FirstOrDefault().Quantity ?? 0,
                            LOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "LOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "LOTR").FirstOrDefault().Quantity ?? 0,
                            GOTR = transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()) != null ?
                                transaction.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()).TransactionItems.Where(ti => ti.Item.ShortName == "GOTR").FirstOrDefault().Quantity ?? 0
                                : transaction.TransactionItems.Where(ti => ti.Item.ShortName == "GOTR").FirstOrDefault().Quantity ?? 0,
                        };

            //Applying sorting
            var returnVal = sortDirection == "asc" ? query.OrderBy(orderBy).ToList() : query.OrderBy(orderBy + " descending").Distinct().ToList();

            var totalRecords = returnVal.Count();

            return new PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int>
            {
                DTOCollection = returnVal,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        #endregion

        public List<StaffUserNameAndEmailViewModel> GetUserNameBySearchText(string searchText)
        {
            var dbContext = context as ReportingBoundedContext;
            var query = from user in dbContext.Users
                        join invitation in dbContext.Invitations on user.Email equals invitation.UserName
                        where (searchText != null ? (user.FirstName.Contains(searchText) || user.LastName.Contains(searchText)) : true) && invitation.VendorId == null && invitation.CustomerId == null && invitation.Status == "Invite Accepted"
                        select new StaffUserNameAndEmailViewModel
                        {
                            UserName = user.FirstName + " " + user.LastName,
                            Email = user.Email
                        };

            var userNameList = query.OrderBy(c => c.UserName).ToList();
            return userNameList;
        }

        public IEnumerable<StewardRptRevenueSupplyOldTireCreditVMsp> GetStewardRevenueSupplyOldTireCreditType_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            DateTime startingDate = Convert.ToDateTime(TreadMarksConstants.ReportEffectiveDateNewTireBegin);

            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(detail.StartDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(detail.EndDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            var db = context as ReportingBoundedContext;
            return db.Sp_Rpt_StewardOldTireCredit(firstDayOfStartDate, lastDatOfEndDate, detail.RegNumber == 0 ? string.Empty : detail.RegNumber.ToString()).ToList();
        }

        #region Metric

        public MetricVM LoadMetricDetailsByID(MetricParams paramModel)
        {
            int categoryID = paramModel.CategoryID ?? 0;
            var result = new MetricVM();

            switch (categoryID)
            {
                case (int)MetricCategoryEnum.YearToDateTSFDollar:
                    getYTDDollar(result);
                    break;
                case (int)MetricCategoryEnum.YearToDateTSFUnits:
                    getYTDUnit(result);
                    break;
                case (int)MetricCategoryEnum.StewardTSFStatusOverview:
                    getTSFStatus(result);
                    break;
                case (int)MetricCategoryEnum.TopTenStewards:
                    getTopTenStewards(result, paramModel);
                    break;
                case (int)MetricCategoryEnum.StewardPaymentMethods:
                    getPaymentMethod(result);
                    break;
            };
            return result;
        }

        private void getYTDDollar(MetricVM vm)
        {
            var dbContext = context as ReportingBoundedContext;
            vm.ytdVMs = dbContext.Sp_Rpt_YearToDateDollar().OrderByDescending(x => x.Year).ToList();
        }
        private void getYTDUnit(MetricVM vm)
        {
            var dbContext = context as ReportingBoundedContext;
            vm.ytdVMs = dbContext.Sp_Rpt_YearToDateUnit().OrderByDescending(x => x.Year).ToList();
        }
        private void getTSFStatus(MetricVM vm)
        {
            var dbContext = context as ReportingBoundedContext;
            vm.tsfStatusVMs = dbContext.Sp_Rpt_TSFStatusOverview().ToList();
            //fill first years missing months with 0
            var startYear = DateTime.Now.Year - 1;
            var startYearData = vm.tsfStatusVMs.Where(x => x.Year == startYear).ToList();
            if (startYearData.Count() < 12)
            {
                var hasMonth = false;
                var tempData = vm.tsfStatusVMs.ToList();
                for (var i = 1; i < 13; i++)
                {
                    hasMonth = startYearData.FirstOrDefault(x => x.Month == i) != null;
                    if (!hasMonth)
                    {
                        tempData.Add(new TSFStatusVM()
                        {
                            Year = startYear,
                            Month = i,
                            Revenue = 0
                        });
                    }
                }
                vm.tsfStatusVMs = tempData.OrderBy(x => x.Year).ThenBy(y => y.Month);
            }
            vm.tsfStatusTimeClassVMs = dbContext.Sp_Rpt_TSFStatusOverview_TimeClass().ToList();
        }
        private void getTopTenStewards(MetricVM vm, MetricParams paramModel)
        {
            var dbContext = context as ReportingBoundedContext;
            vm.topTenTableVMs = dbContext.Sp_Rpt_TopTenStewards(paramModel.StartDate)
                                .OrderByDescending(x => x.Amount)
                                .ThenBy(x => x.Steward)
                                .ToList();
            vm.topTenChartVMs = dbContext.Sp_Rpt_TopTenStewardCharts(paramModel.StartDate).ToList();
        }
        private void getPaymentMethod(MetricVM vm)
        {
            var dbContext = context as ReportingBoundedContext;
            vm.paymentMethodVMs = dbContext.Sp_Rpt_PaymentMethod();
        }

        public List<CollectionGenerationOfTiresBasedOnRateGroupVM> GetCollectorFSAReport(DateTime fromDate, DateTime toDate, string rateGroupIds)
        {
            var dbContext = context as ReportingBoundedContext;
            return dbContext.Sp_Rpt_CollectorFSAReport(fromDate, toDate, rateGroupIds).ToList();
        }
        #endregion
    }
}