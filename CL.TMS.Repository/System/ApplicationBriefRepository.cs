
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using CL.TMS.Common;
using CL.TMS.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CL.TMS.Common.Converters;
using System.Xml;
using System.Diagnostics;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Framework.DTO;
using CL.TMS.Security.Authorization;
using CL.TMS.Security;
using System.Data.Entity.SqlServer;

namespace CL.TMS.Repository.System
{
    public class ApplicationBriefRepository : BaseRepository<ApplicationBoundedContext, User, long>, IApplicationBriefRepository
    {
        public PaginationDTO<ApplicationViewModel, int> LoadAllApplicationBrief(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var query = LoadApplicationViewModels(searchText, orderBy, sortDirection, columnSearchText);

            //OTSTM2-982 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            var columnSearchTextDate = columnSearchText["Date"];
            var result = query.ToList();
            result.ForEach(i =>
            {
                i.ApplicationSubmitDate = (i.ApplicationSubmitDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.ApplicationSubmitDate.Value, TimeZoneInfo.Local) : i.ApplicationSubmitDate;
            });
            if (!string.IsNullOrWhiteSpace(columnSearchTextDate))
            {
                result = result.Where(c => ((c.ApplicationSubmitDate != null) ? c.ApplicationSubmitDate.Value.ToString("yyyy-MM-dd") : string.Empty).ToLower().Contains(columnSearchTextDate)).ToList();
            }

            var totalRecords = result.Count();
            var returnValue = result.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ApplicationViewModel, int>
            {
                DTOCollection = returnValue,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        private IQueryable<ApplicationViewModel> LoadApplicationViewModels(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var dbContext = context as ApplicationBoundedContext;
            int purgeDays = 7;
            int inviteExpDays = 7;
            try
            {
                purgeDays = int.Parse(dbContext.Settings.FirstOrDefault(i => i.Key == "Application.PurgeOpenApplicationAfterDays").Value);
                inviteExpDays = int.Parse(dbContext.Settings.FirstOrDefault(i => i.Key == "Application.InvitationExpiryDays").Value);
            }
            catch (Exception)
            {
                purgeDays = 7;
                inviteExpDays = 7;
            }
            ///field Application.ExpireDate is used for both [Application.PurgeOpenApplicationAfterDays] and [Application.InvitationExpiryDays], 
            ///this value will be reset to [Application.InvitationExpiryDays] after application send back to participant(restart another countdown cycle).
            DateTime purgeDate = DateTime.UtcNow.AddDays(inviteExpDays - purgeDays);
            var query = from app in dbContext.Applications.AsNoTracking()
                        join appInv in dbContext.ApplicationInvitations.AsNoTracking() on app.ID equals appInv.ApplicationID
                        where ((app.Status != ApplicationStatusEnum.BankInformationSubmitted.ToString())
                            && (app.Status != ApplicationStatusEnum.BankInformationApproved.ToString())
                            && (app.Status != ApplicationStatusEnum.Approved.ToString())
                            && (app.Status != ApplicationStatusEnum.Resend.ToString())
                            && (app.Status != ApplicationStatusEnum.Completed.ToString())
                            && ((app.ExpireDate == null) || ((app.ExpireDate > purgeDate) && ((app.Status == ApplicationStatusEnum.Open.ToString()) || (app.Status == ApplicationStatusEnum.BackToApplicant.ToString()))) || (((app.Status != ApplicationStatusEnum.Open.ToString())) && ((app.Status != ApplicationStatusEnum.BackToApplicant.ToString()))))
                            )
                        select new ApplicationViewModel
                        {
                            ID = app.ID,
                            Status = app.Status,
                            ApplicationSubmitDate = app.SubmittedDate,
                            assignedToName = app.AssignUser != null ? app.AssignUser.FirstName + " " + app.AssignUser.LastName : "",
                            ParticipantType = appInv.ParticipantType.Name,
                            BusinessName = app.Company,
                            ContactName = app.Contact,
                            TokenID = appInv.TokenID,
                            StatusIndex = app.StatusIndex,
                            //OTSTM2-347
                            Email = appInv.Email
                        };

            //OTSTM2-877 and 982 shared code, binding status between UI and backend, Rejected(UI)--Denied(backend), Under Review(UI)--Assigned/Reassigntorep/Onhold/Offhold/Underreview(backend), Open(UI)--Open/Backtoapplicant
            var searchTextReassigntorep = ApplicationStatusEnum.ReAssignToRep.ToString().ToLower();
            var searchTextOnhold = ApplicationStatusEnum.OnHold.ToString().ToLower();
            var searchTextOffhold = ApplicationStatusEnum.OffHold.ToString().ToLower();
            var searchTextUnderreview = ApplicationStatusEnum.UnderReview.ToString().ToLower();
            var searchTextAssigned = ApplicationStatusEnum.Assigned.ToString().ToLower();
            var searchTextDenied = ApplicationStatusEnum.Denied.ToString().ToLower();
            var searchTextOpen = ApplicationStatusEnum.Open.ToString().ToLower();
            var searchTextBacktoapplicant = ApplicationStatusEnum.BackToApplicant.ToString().ToLower();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.ApplicationSubmitDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ApplicationSubmitDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.ApplicationSubmitDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();

                    //OTSTM2-877 search based on backend status, add search email
                    if ("Rejected".ToLower().Contains(searchText) && "Under Review".ToLower().Contains(searchText) && "Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextDenied)
                                            || c.Status.ToLower().Contains(searchTextAssigned)
                                            || c.Status.ToLower().Contains(searchTextReassigntorep)
                                            || c.Status.ToLower().Contains(searchTextOnhold)
                                            || c.Status.ToLower().Contains(searchTextOffhold)
                                            || c.Status.ToLower().Contains(searchTextUnderreview)
                                            || c.Status.ToLower().Contains(searchTextOpen)
                                            || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if ("Rejected".ToLower().Contains(searchText) && "Under Review".ToLower().Contains(searchText) && !"Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextDenied)
                                            || c.Status.ToLower().Contains(searchTextAssigned)
                                            || c.Status.ToLower().Contains(searchTextReassigntorep)
                                            || c.Status.ToLower().Contains(searchTextOnhold)
                                            || c.Status.ToLower().Contains(searchTextOffhold)
                                            || c.Status.ToLower().Contains(searchTextUnderreview))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if ("Rejected".ToLower().Contains(searchText) && !"Under Review".ToLower().Contains(searchText) && "Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextDenied)
                                            || c.Status.ToLower().Contains(searchTextOpen)
                                            || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if (!"Rejected".ToLower().Contains(searchText) && "Under Review".ToLower().Contains(searchText) && "Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextAssigned)
                                            || c.Status.ToLower().Contains(searchTextReassigntorep)
                                            || c.Status.ToLower().Contains(searchTextOnhold)
                                            || c.Status.ToLower().Contains(searchTextOffhold)
                                            || c.Status.ToLower().Contains(searchTextUnderreview)
                                            || c.Status.ToLower().Contains(searchTextOpen)
                                            || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if ("Rejected".ToLower().Contains(searchText) && !"Under Review".ToLower().Contains(searchText) && !"Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextDenied))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if (!"Rejected".ToLower().Contains(searchText) && "Under Review".ToLower().Contains(searchText) && !"Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextAssigned)
                                            || c.Status.ToLower().Contains(searchTextReassigntorep)
                                            || c.Status.ToLower().Contains(searchTextOnhold)
                                            || c.Status.ToLower().Contains(searchTextOffhold)
                                            || c.Status.ToLower().Contains(searchTextUnderreview))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else if (!"Rejected".ToLower().Contains(searchText) && !"Under Review".ToLower().Contains(searchText) && "Open".ToLower().Contains(searchText))
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText)
                                            || c.Status.ToLower().Contains(searchTextOpen)
                                            || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }
                    else
                    {
                        query = query.Where(c => (c.Status.ToLower().Contains(searchText))
                                            || c.assignedToName.ToLower().Contains(searchText)
                                            || c.ParticipantType.ToLower().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                            || c.ContactName.ToLower().Contains(searchText)
                                            || c.Email.ToLower().Contains(searchText)
                                            || DbFunctions.TruncateTime(c.ApplicationSubmitDate).ToString().Contains(searchText)
                                        );
                    }

                }
            }

            //OTSTM2-982           
            var columnSearchTextType = columnSearchText["Type"];
            var columnSearchTextEmailAddress = columnSearchText["EmailAddress"];
            var columnSearchTextCompany = columnSearchText["Company"];
            var columnSearchTextContact = columnSearchText["Contact"];
            var columnSearchTextStatus = columnSearchText["Status"];
            var columnSearchTextAssignedTo = columnSearchText["AssignedTo"];

            if (!string.IsNullOrWhiteSpace(columnSearchTextType))
            {
                query = query.Where(c => c.ParticipantType.ToString().Contains(columnSearchTextType));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextEmailAddress))
            {
                query = query.Where(c => c.Email.ToString().Contains(columnSearchTextEmailAddress));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextCompany))
            {
                query = query.Where(c => c.BusinessName.ToString().Contains(columnSearchTextCompany));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextContact))
            {
                query = query.Where(c => c.ContactName.ToString().Contains(columnSearchTextContact));
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextStatus))
            {
                if ("Rejected".ToLower().Contains(columnSearchTextStatus) && "Under Review".ToLower().Contains(columnSearchTextStatus) && "Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextDenied)
                                        || c.Status.ToLower().Contains(searchTextAssigned)
                                        || c.Status.ToLower().Contains(searchTextReassigntorep)
                                        || c.Status.ToLower().Contains(searchTextOnhold)
                                        || c.Status.ToLower().Contains(searchTextOffhold)
                                        || c.Status.ToLower().Contains(searchTextUnderreview)
                                        || c.Status.ToLower().Contains(searchTextOpen)
                                        || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                    );
                }
                else if ("Rejected".ToLower().Contains(columnSearchTextStatus) && "Under Review".ToLower().Contains(columnSearchTextStatus) && !"Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextDenied)
                                        || c.Status.ToLower().Contains(searchTextAssigned)
                                        || c.Status.ToLower().Contains(searchTextReassigntorep)
                                        || c.Status.ToLower().Contains(searchTextOnhold)
                                        || c.Status.ToLower().Contains(searchTextOffhold)
                                        || c.Status.ToLower().Contains(searchTextUnderreview))
                                        && !(c.Status.ToLower().Contains(searchTextOpen))
                                        && !(c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                    );
                }
                else if ("Rejected".ToLower().Contains(columnSearchTextStatus) && !"Under Review".ToLower().Contains(columnSearchTextStatus) && "Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextDenied)
                                        || c.Status.ToLower().Contains(searchTextOpen)
                                        || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                        && !(c.Status.ToLower().Contains(searchTextAssigned))
                                        && !(c.Status.ToLower().Contains(searchTextReassigntorep))
                                        && !(c.Status.ToLower().Contains(searchTextOnhold))
                                        && !(c.Status.ToLower().Contains(searchTextOffhold))
                                        && !(c.Status.ToLower().Contains(searchTextUnderreview))
                                    );
                }
                else if (!"Rejected".ToLower().Contains(columnSearchTextStatus) && "Under Review".ToLower().Contains(columnSearchTextStatus) && "Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextAssigned)
                                        || c.Status.ToLower().Contains(searchTextReassigntorep)
                                        || c.Status.ToLower().Contains(searchTextOnhold)
                                        || c.Status.ToLower().Contains(searchTextOffhold)
                                        || c.Status.ToLower().Contains(searchTextUnderreview)
                                        || c.Status.ToLower().Contains(searchTextOpen)
                                        || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                        && !(c.Status.ToLower().Contains(searchTextDenied))
                                    );
                }
                else if ("Rejected".ToLower().Contains(columnSearchTextStatus) && !"Under Review".ToLower().Contains(columnSearchTextStatus) && !"Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextDenied))
                                        && !(c.Status.ToLower().Contains(searchTextAssigned))
                                        && !(c.Status.ToLower().Contains(searchTextReassigntorep))
                                        && !(c.Status.ToLower().Contains(searchTextOnhold))
                                        && !(c.Status.ToLower().Contains(searchTextOffhold))
                                        && !(c.Status.ToLower().Contains(searchTextUnderreview))
                                        && !(c.Status.ToLower().Contains(searchTextOpen))
                                        && !(c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                    );
                }
                else if (!"Rejected".ToLower().Contains(columnSearchTextStatus) && "Under Review".ToLower().Contains(columnSearchTextStatus) && !"Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextAssigned)
                                        || c.Status.ToLower().Contains(searchTextReassigntorep)
                                        || c.Status.ToLower().Contains(searchTextOnhold)
                                        || c.Status.ToLower().Contains(searchTextOffhold)
                                        || c.Status.ToLower().Contains(searchTextUnderreview))
                                        && !(c.Status.ToLower().Contains(searchTextDenied))
                                        && !(c.Status.ToLower().Contains(searchTextOpen))
                                        && !(c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                    );
                }
                else if (!"Rejected".ToLower().Contains(columnSearchTextStatus) && !"Under Review".ToLower().Contains(columnSearchTextStatus) && "Open".ToLower().Contains(columnSearchTextStatus))
                {
                    query = query.Where(c => (c.Status.ToLower().Contains(columnSearchTextStatus)
                                        || c.Status.ToLower().Contains(searchTextOpen)
                                        || c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                        && !(c.Status.ToLower().Contains(searchTextDenied))
                                        && !(c.Status.ToLower().Contains(searchTextAssigned))
                                        && !(c.Status.ToLower().Contains(searchTextReassigntorep))
                                        && !(c.Status.ToLower().Contains(searchTextOnhold))
                                        && !(c.Status.ToLower().Contains(searchTextOffhold))
                                        && !(c.Status.ToLower().Contains(searchTextUnderreview))
                                    );
                }
                else
                {
                    query = query.Where(c => c.Status.ToLower().Contains(columnSearchTextStatus)
                                        && !(c.Status.ToLower().Contains(searchTextDenied))
                                        && !(c.Status.ToLower().Contains(searchTextAssigned))
                                        && !(c.Status.ToLower().Contains(searchTextReassigntorep))
                                        && !(c.Status.ToLower().Contains(searchTextOnhold))
                                        && !(c.Status.ToLower().Contains(searchTextOffhold))
                                        && !(c.Status.ToLower().Contains(searchTextUnderreview))
                                        && !(c.Status.ToLower().Contains(searchTextOpen))
                                        && !(c.Status.ToLower().Contains(searchTextBacktoapplicant))
                                    );
                }
            }
            if (!string.IsNullOrWhiteSpace(columnSearchTextAssignedTo))
            {
                query = query.Where(c => c.assignedToName.ToString().Contains(columnSearchTextAssignedTo));
            }

            if (orderBy.ToLower() == "status")
                orderBy = "StatusIndex";

            if (!string.IsNullOrWhiteSpace(orderBy))
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            return query;

        }

        public List<ApplicationViewModel> LoadApplications(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            //OTSTM2-982 convert the followed date to local time, be consistent with the existing logic of controller, need no do conversion again in controller
            var columnSearchTextDate = columnSearchText["Date"];
            var result = LoadApplicationViewModels(searchText, sortcolumn, sortdirection, columnSearchText).ToList();
            result.ForEach(i =>
            {
                i.ApplicationSubmitDate = (i.ApplicationSubmitDate != null) ? TimeZoneInfo.ConvertTimeFromUtc(i.ApplicationSubmitDate.Value, TimeZoneInfo.Local) : i.ApplicationSubmitDate;
            });
            if (!string.IsNullOrWhiteSpace(columnSearchTextDate))
            {
                result = result.Where(c => ((c.ApplicationSubmitDate != null) ? c.ApplicationSubmitDate.Value.ToString("yyyy-MM-dd") : string.Empty).ToLower().Contains(columnSearchTextDate)).ToList();
            }
            return result;
        }

        public ApplicationViewModel GetFirstSubmitApplication()
        {
            //get user security
            var availableApplications = new List<string>();
            bool StewardRouting = SecurityContextHelper.StewardApplicationsWorkflow != null ? SecurityContextHelper.StewardApplicationsWorkflow.Routing : false;
            bool CollectorRouting = SecurityContextHelper.CollectorApplicationsWorkflow != null ? SecurityContextHelper.CollectorApplicationsWorkflow.Routing : false;
            bool HaulerRouting = SecurityContextHelper.HaulerApplicationsWorkflow != null ? SecurityContextHelper.HaulerApplicationsWorkflow.Routing : false;
            bool ProcessorRouting = SecurityContextHelper.ProcessorApplicationsWorkflow != null ? SecurityContextHelper.ProcessorApplicationsWorkflow.Routing : false;
            bool RPMRouting = SecurityContextHelper.RPMApplicationsWorkflow != null ? SecurityContextHelper.RPMApplicationsWorkflow.Routing : false;

            if (StewardRouting)
            {
                availableApplications.Add(TreadMarksConstants.StewardAccount);
            }
            if (CollectorRouting)
            {
                availableApplications.Add(TreadMarksConstants.CollectorAccount);
            }
            if (HaulerRouting)
            {
                availableApplications.Add(TreadMarksConstants.HaulerAccount);
            }
            if (ProcessorRouting)
            {
                availableApplications.Add(TreadMarksConstants.ProcessorAccount);
            }
            if (RPMRouting)
            {
                availableApplications.Add(TreadMarksConstants.RPMAccount);
            }


            var dbContext = context as ApplicationBoundedContext;
            var query = from app in dbContext.Applications.AsNoTracking()
                        join appInv in dbContext.ApplicationInvitations.AsNoTracking() on app.ID equals appInv.ApplicationID
                        where (app.Status == ApplicationStatusEnum.Submitted.ToString() && availableApplications.Contains(appInv.ParticipantTypeName))
                        select new ApplicationViewModel
                        {
                            ID = app.ID,
                            Status = app.Status,
                            ApplicationSubmitDate = app.SubmittedDate,
                            ParticipantType = appInv.ParticipantType.Name,
                            BusinessName = app.Company
                        };

            return query.OrderBy(c => c.ApplicationSubmitDate).FirstOrDefault();
        }
    }
}
