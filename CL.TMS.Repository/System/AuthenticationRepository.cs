﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using CL.Framework.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using Claim = System.Security.Claims.Claim;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Reporting;

namespace CL.TMS.Repository.System
{
    public class AuthenticationRepository : BaseRepository<SecurityBoundedContext, User, long>, IAuthenticationRepository
    {
        public AuthenticationUser FindUserByUserName(string userName)
        {
            ConditionCheck.NullOrEmpty(userName, "userName");
            var user = entityStore.All.FirstOrDefault(c => c.UserName == userName);
            var result = Mapper.Map<User, AuthenticationUser>(user);
            if (result != null)
                result.UserVendorClaims = GetUserVendors(user.ID).ToList();
            return result;
        }

        public void SetLoginDateTimeAndResetLockHandler(long userId)
        {
            var user = entityStore.FindById(userId);
            user.LastLoginDate = DateTime.UtcNow; //.ToUniversalTime();
            user.InvalidLoginAttempts = 0;
            user.IsLocked = false;
            user.PasswordReset = false;
            user.PasswordRestToken = null;
            user.PasswordRestSecretKey = null;
            entityStore.Update(user, c => c.LastLoginDate, c => c.InvalidLoginAttempts, c => c.IsLocked, c => c.PasswordReset, c => c.PasswordRestToken, c => c.PasswordRestSecretKey);
        }

        public void LockUser(long userId)
        {
            var user = entityStore.FindById(userId);
            user.IsLocked = true;
            entityStore.Update(user, c => c.IsLocked);
        }

        public bool InvalidLoginAttemptHandler(long userId, int maxLoginAttempts)
        {
            var user = entityStore.FindById(userId);
            var isLocked = false;
            user.InvalidLoginAttempts += 1;
            if (user.InvalidLoginAttempts >= maxLoginAttempts)
            {
                isLocked = true;
                user.IsLocked = true;
            }
            entityStore.Update(user, c => c.IsLocked, c => c.InvalidLoginAttempts);

            return isLocked;
        }


        public IEnumerable<string> GetUserRoles(long userId)
        {
            var listOfRoles = new List<string>();
            var dbContext = context as SecurityBoundedContext;

            //Load user role from userrole for staff user
            var roles = dbContext.Users.Include("Roles").FirstOrDefault(c => c.ID == userId).Roles.ToList();
            listOfRoles.AddRange(roles.Select(role => role.Name));

            if (listOfRoles.Count == 0)
            {
                //it is a participant user, load user roles for participant
                //don't load user role for multiple verdor scenario, user role will be loaded when user providing global vendor search
                //Load from vendoruser table
                if (!IsMultipleVendorMapped(userId))
                {
                    var vendorUserQuery = from role in dbContext.Roles
                                          join vendoruser in dbContext.VendorUsers on role.ID equals vendoruser.RoleId
                                          where vendoruser.UserID == userId && vendoruser.IsActive
                                          select role;
                    listOfRoles.AddRange(vendorUserQuery.Select(role => role.Name));

                    //Load from customeruser table
                    var customerUserQuery = from role in dbContext.Roles
                                            join customeruser in dbContext.CustomerUsers on role.ID equals customeruser.RoleId
                                            where customeruser.UserId == userId && customeruser.IsActive
                                            select role;
                    listOfRoles.AddRange(customerUserQuery.Select(role => role.Name));
                }
            }

            return listOfRoles;
        }

        private bool IsMultipleVendorMapped(long userId)
        {
            var dbContext = context as SecurityBoundedContext;
            var vendorUsers = dbContext.VendorUsers.Where(c => c.UserID == userId && c.IsActive).GroupBy(c => c.VendorID).Count();
            var customerUsers = dbContext.CustomerUsers.Where(c => c.UserId == userId && c.IsActive).GroupBy(c => c.CustomerId).Count();
            return (vendorUsers + customerUsers) > 1;
        }

        public IEnumerable<Claim> GetUserClaims(long userId)
        {
            var user = entityStore.FindById(userId);

            var claims = new[] {
                new Claim("Id", user.ID.ToString()),
                new Claim("UserName", user.UserName) ,
                new Claim("Email", user.Email)
            };

            var userPermissions = GetRolePermissionsClaims(userId);
            var result = claims.Concat(userPermissions);
            var userVendors = GetUserVendors(userId);
            result = result.Concat(userVendors);

            return result;
        }

        public string GetAfterLoginRedirectUrl(long userId)
        {
            string redirectUrl = string.Empty;
            var user = entityStore.FindById(userId);

            if (user.RedirectUserAfterLogin.HasValue && user.RedirectUserAfterLogin.Value)
            {
                redirectUrl = user.RedirectUserAfterLoginUrl;
            }

            return redirectUrl;
        }

        #region Private Methods
        private IEnumerable<Claim> GetRolePermissionsClaims(long userId)
        {
            var dbContext = context as SecurityBoundedContext;

            //Load user permission based on associated user role
            var user = dbContext.Users
                .Include("Roles")
                .Include("Roles.RolePermissions")
                .Include("Roles.RolePermissions.AppResource")
                .Include("Roles.RolePermissions.AppPermission")
                .Include("UserClaimsWorkflows")
                .Include("UserApplicationsWorkflows")
                .FirstOrDefault(c => c.ID == userId);

            var claims = new List<Claim>();

            //Load staff roles claims
            //Load maximum user role permission
            var roleIds = user.Roles.Select(c => c.ID).ToList();
            var rolePermissions = dbContext.RolePermissions.Where(c => roleIds.Contains(c.RoleId)).GroupBy(c => new { c.AppResource.ResourceMenu, c.AppResource.ResourceScreen, c.AppResource.Name, c.AppResource.DisplayOrder, c.AppResource.ResourceLevel })
                .Select(c =>
                  new UserPermissionViewModel
                  {
                      Menu = c.Key.ResourceMenu,
                      Screen = c.Key.ResourceScreen,
                      Panel = c.Key.Name,
                      DisplayOrder = c.Key.DisplayOrder,
                      ResourceLevel = c.Key.ResourceLevel,
                      MaxPermissionLevel = c.Max(i => i.AppPermission.PermissionLevel)
                  }
            ).ToList();

            rolePermissions.ForEach(c =>
            {
                var claimValue = string.Format("{0},{1}", c.Menu + "_" + c.Screen + "_" + c.Panel, c.MaxPermissionLevel);
                var claim = new Claim(TreadMarksConstants.RolePermissions, claimValue);
                claims.Add(claim);
            });

            //Load user maximum permission from vendoruser/customeruser
            var vendorRolesQuery = from role in dbContext.Roles
                                   join vendoruser in dbContext.VendorUsers on role.ID equals vendoruser.RoleId
                                   where vendoruser.UserID == userId && vendoruser.IsActive
                                   select role;
            var customerRolesQuery = from role in dbContext.Roles
                                     join customeruser in dbContext.CustomerUsers on role.ID equals customeruser.RoleId
                                     where customeruser.UserId == userId && customeruser.IsActive
                                     select role;

            var query = vendorRolesQuery.Union(customerRolesQuery);
            var participantRolePermissions = new List<RolePermission>();
            query.ToList().ForEach(c =>
            {
                participantRolePermissions.AddRange(c.RolePermissions);
            });

            var participantUserRolePermissions = participantRolePermissions.AsQueryable().GroupBy(c => new { c.AppResource.ResourceMenu, c.AppResource.ResourceScreen, c.AppResource.Name })
                  .Select(c =>
                  new UserPermissionViewModel
                  {
                      Menu = c.Key.ResourceMenu,
                      Screen = c.Key.ResourceScreen,
                      Panel = c.Key.Name,
                      MaxPermissionLevel = c.Max(i => i.AppPermission.PermissionLevel)
                  }
            ).ToList();

            participantUserRolePermissions.ForEach(c =>
            {
                var claimValue = string.Format("{0},{1}", c.Menu + "_" + c.Screen + "_" + c.Panel, c.MaxPermissionLevel);
                var claim = new Claim(TreadMarksConstants.RolePermissions, claimValue);
                claims.Add(claim);
            });

            //Load Claims Workflow claims
            foreach (var claimWorkflow in user.UserClaimsWorkflows)
            {
                var claim = new Claim(TreadMarksConstants.ClaimsWorkflow, string.Format("{0},{1},{2}", claimWorkflow.AccountName, claimWorkflow.Workflow, claimWorkflow.ActionGear));
                if (!claims.Contains(claim))
                    claims.Add(claim);
            }

            //Load Applications Workflow claims
            foreach (var applicationWorkflow in user.UserApplicationsWorkflows)
            {
                var claim = new Claim(TreadMarksConstants.ApplicationsWorkflow, string.Format("{0},{1},{2}", applicationWorkflow.AccountName, applicationWorkflow.Routing, applicationWorkflow.ActionGear));
                if (!claims.Contains(claim))
                    claims.Add(claim);
            }

            return claims;
        }

        private IEnumerable<Claim> GetUserVendors(long userId)
        {
            var userVendors = (context as SecurityBoundedContext).VendorUsers.Where(c => c.UserID == userId && c.IsActive).GroupBy(c => c.VendorID).Select(c => c.Key).ToList();
            var userCustomers = (context as SecurityBoundedContext).CustomerUsers.Where(c => c.UserId == userId && c.IsActive).GroupBy(c => c.CustomerId).Select(c => c.Key).ToList();
            var claims = new List<Claim>();
            userVendors.ForEach(c =>
            {
                var claim = new Claim("UserVendor", c.ToString());
                claims.Add(claim);
            });
            userCustomers.ForEach(c =>
            {
                var claim = new Claim("UserVendor", c.ToString());
                claims.Add(claim);

            });
            return claims;
        }

        #endregion
    }
}
