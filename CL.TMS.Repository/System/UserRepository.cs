﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Extension;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.Repository.System
{
    public class UserRepository : BaseRepository<SecurityBoundedContext, User, long>, IUserRepository
    {
        private EntityStore<UserPasswordArchive, int> userPasswordArchiveStore;

        public UserRepository()
        {
            userPasswordArchiveStore = new EntityStore<UserPasswordArchive, int>(context);
        }

        public void AddAfterloginRedirectUrl(long userId, string redirectUrl)
        {
            var user = entityStore.FindById(userId);

            user.RedirectUserAfterLogin = true;
            user.RedirectUserAfterLoginUrl = redirectUrl;

            entityStore.Update(user);
        }

        public void AddPasswordResetToken(long userId, string token, string secretKey)
        {
            ClearPasswordResetToken(userId);

            var user = entityStore.FindById(userId);
            user.PasswordRestToken = token;
            user.PasswordRestSecretKey = secretKey;
            user.PasswordReset = true;
            entityStore.Update(user);
        }

        public void AddUser(User user)
        {
            ConditionCheck.Null(user, "User");
            entityStore.Insert(user);
        }

        public void ArchiveOldPassword(long userId, string hashedPassword)
        {
            if (userId <= 0) { throw new ArgumentOutOfRangeException("userId"); }
            ConditionCheck.NullOrEmpty(hashedPassword, "hashedPassword");

            UserPasswordArchive passwordArchive = new UserPasswordArchive()
            {
                UserID = userId,
                LastLoginDate = DateTime.UtcNow,
                Password = hashedPassword,
            };
            userPasswordArchiveStore.Insert(passwordArchive);
        }

        public void ClearPasswordResetToken(long userId)
        {
            var user = entityStore.FindById(userId);
            user.PasswordRestToken = null;
            user.PasswordRestSecretKey = null;
            user.PasswordReset = false;
            entityStore.Update(user);
        }

        public void DeleteUser(User user)
        {
            ConditionCheck.Null(user, "user");
            entityStore.Delete(user.ID);
        }

        public User FindUserByUserId(long userId)
        {
            if (userId < 0) { throw new ArgumentOutOfRangeException("userId"); }
            return entityStore.FindById(userId);
        }

        public User FindUserByUserName(string userName)
        {
            ConditionCheck.NullOrEmpty(userName, "userName");
            var result = entityStore.All.FirstOrDefault(c => c.UserName == userName);

            return result;
        }

        public User FindUserByEmailAddress(string emailAddress)
        {
            ConditionCheck.NullOrEmpty(emailAddress, "emailAddress");
            return entityStore.All.SingleOrDefault(c => c.Email == emailAddress);
        }

        public User FindUserByName(string userName)
        {
            ConditionCheck.NullOrEmpty(userName, "userName");
            return entityStore.All.FirstOrDefault(c => c.UserName == userName);
        }

        public string FindUserNameByUserId(long userId)
        {
            return entityStore.FindById(userId).UserName;
        }

        public string FindEmailAddressByUsername(string Username)
        {
            return entityStore.All.FirstOrDefault(c => c.UserName == Username).Email;
        }

        /// <summary>
        /// Collection containing the token and secret key
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>Index 0 = Token, Index 1 = Secret Key</returns>
        public IReadOnlyList<string> GetPasswordResetToken(long userId)
        {
            var tokenInfo = new List<string>();
            var user = FindUserByUserId(userId);

            if (user != null)
            {
                tokenInfo.AddRange(new[] { user.PasswordRestToken, user.PasswordRestSecretKey });
            }

            return tokenInfo;
        }

        public IEnumerable<string> GetUserHashedPasswordArchive(long userId)
        {
            return entityStore.FindById(userId).UserPasswordArchive.Select(c => c.Password).ToList();
        }

        public void RemoveAfterloginRedirectUrl(long userId)
        {
            var user = entityStore.FindById(userId);
            user.RedirectUserAfterLoginUrl = "";
            user.RedirectUserAfterLogin = false;
            entityStore.Update(user);
        }

        public void ResetloginRedirectUrl(long userId, string redirectURL)
        {
            var user = entityStore.FindById(userId);
            user.RedirectUserAfterLoginUrl = redirectURL;
            user.RedirectUserAfterLogin = true;
            entityStore.Update(user);
        }

        public void ResetUserPassword(User user, string hashedPassword, int passwordExpirationInDays)
        {
            ConditionCheck.Null(user, "User");
            ConditionCheck.NullOrEmpty(hashedPassword, "hashedPassword");
            ArchiveOldPassword(user.ID, user.Password);
            user.Password = hashedPassword;
            user.LastPasswordResetDate = DateTime.UtcNow;
            user.PasswordExpirationDate = DateTime.UtcNow.AddDays(passwordExpirationInDays);
            user.IsLocked = false;
            user.InvalidLoginAttempts = 0;
            user.PasswordReset = false;
            user.PasswordRestToken = null;
            user.PasswordRestSecretKey = null;
            user.ModifiedDate = DateTime.UtcNow;
            entityStore.Update(user);
        }

        public void UpdateUser(User user)
        {
            if (user == null) { throw new ArgumentNullException("user"); }
            entityStore.Update(user);
        }

        /// <summary>
        /// Get user list by role name
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public IDictionary<long, string> GetUsersByGroup(string[] groupName)
        {
            var userIDNameList = new Dictionary<long, string>();
            var groupNameList = groupName.ToList();
            groupNameList.ForEach(c =>
            {
                //find role by name
                var role = (context as SecurityBoundedContext).Roles.FirstOrDefault(r => r.Name == c);
                if (role != null)
                {
                    var users = role.Users.ToList();
                    foreach (var user in users)
                    {
                        if (!userIDNameList.ContainsKey(user.ID))
                            userIDNameList.Add(user.ID, string.Format("{0} {1}", user.FirstName, user.LastName));
                    }
                }
            });
            return userIDNameList;
        }

        public IDictionary<long, string> GetUsersByApplicationsWorkflow(string accountName)
        {
            var userIDNameList = new Dictionary<long, string>();
            var dbContext = context as SecurityBoundedContext;

            var query = dbContext.UserApplicationsWorkflows.Where(c => c.AccountName == accountName && c.Routing).Select(c => new
            {
                UserId = c.UserId,
                FirstName = c.User.FirstName,
                LastName = c.User.LastName
            });
            query.ToList().ForEach(c =>
            {
                userIDNameList.Add(c.UserId, string.Format("{0} {1}", c.FirstName, c.LastName));
            });
            return userIDNameList;
        }

        public Dictionary<long, string> GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType)
        {
            var userIDNameList = new Dictionary<long, string>();
            var dbContext = context as SecurityBoundedContext;
            if (claimType == "Claim")
            {
                //check current claim status
                var workflow = TreadMarksConstants.RepresentativeWorkflow;
                var claimDbContext = new ClaimsBoundedContext();
                var claimProcess = claimDbContext.ClaimProcesses.FirstOrDefault(c => c.Claim.ID == claimId);
                if (claimProcess != null)
                {
                    if (claimProcess.RepresentativeReview != null)
                    {
                        workflow = TreadMarksConstants.LeadWorkflow;
                    }
                    if (claimProcess.LeadReview != null)
                    {
                        workflow = TreadMarksConstants.SupervisorWorkflow;
                    }
                    if (claimProcess.SupervisorReview != null)
                    {
                        workflow = TreadMarksConstants.Approver1Workflow;
                    }
                    if (claimProcess.Approver1 != null)
                    {
                        workflow = TreadMarksConstants.Approver2Workflow;
                    }
                }

                var query = dbContext.UserClaimsWorkflows.Where(c => c.AccountName == accountName && c.Workflow == workflow && !c.User.Inactive).Select(c => new
                {
                    UserId = c.UserId,
                    FirstName = c.User.FirstName,
                    LastName = c.User.LastName
                });
                query.ToList().ForEach(c =>
                {
                    userIDNameList.Add(c.UserId, string.Format("{0} {1}", c.FirstName, c.LastName));
                });
            }
            else
            {
                //for remittance -- get all user who has TreadMarksConstants.TSFClerkWorkflow
                var query = dbContext.UserClaimsWorkflows.Where(c => c.AccountName == TreadMarksConstants.StewardAccount && c.Workflow == TreadMarksConstants.TSFClerkWorkflow && !c.User.Inactive).Select(c => new
                {
                    UserId = c.UserId,
                    FirstName = c.User.FirstName,
                    LastName = c.User.LastName
                });
                query.ToList().ForEach(c =>
                {
                    userIDNameList.Add(c.UserId, string.Format("{0} {1}", c.FirstName, c.LastName));
                });
            }

            return userIDNameList;
        }

        public List<Role> LoadAllUserRoles()
        {
            return (context as SecurityBoundedContext).Roles.ToList();
        }

        public User FindUserDetailsByEmail(string email)
        {
            return entityStore.AllIncluding(c => c.Roles, c => c.UserClaimsWorkflows, c => c.UserApplicationsWorkflows)
                .FirstOrDefault(c => c.Email.Trim().ToLower() == email.Trim().ToLower());
        }

        public void DeleteUserDetails(long userId)
        {
            context.Database.ExecuteSqlCommand("exec sp_deleteuserdetails " + userId);
        }

        public void ActiveUserByEmail(string email, bool isActive)
        {
            var user = FindUserByEmailAddress(email);
            user.Inactive = !isActive;
            entityStore.Update(user);
        }

        //OTSTM2-1013
        public void UnlockStaffUserByEmail(string email)
        {
            var user = FindUserByEmailAddress(email);
            user.IsLocked = false;
            user.InvalidLoginAttempts = 0;
            entityStore.Update(user);
        }

        public void UnlockParticipantUserById(long userId)
        {
            var user = FindUserByUserId(userId);
            user.IsLocked = false;
            user.InvalidLoginAttempts = 0;
            entityStore.Update(user);
        }

        public List<User> GetClaimWorkflowUsers(string accountName, string workflowName)
        {
            var dbContext = context as SecurityBoundedContext;
            var query = from user in dbContext.Users
                        join userclaimsworkflow in dbContext.UserClaimsWorkflows
                        on user.ID equals userclaimsworkflow.UserId
                        where (userclaimsworkflow.AccountName == accountName && userclaimsworkflow.Workflow == workflowName) && (user.Inactive == false)
                        select user;
            return query.ToList();
        }

        public List<UserClaimsWorkflow> GetUserClaimsWorkflowsByUserID(int userID, string accountName, string workflow)
        {
            var dbContext = context as SecurityBoundedContext;
            var result = dbContext.UserClaimsWorkflows.Where(c => c.UserId == userID && c.Workflow == workflow); ;
            if (!string.IsNullOrEmpty(accountName))
            {
                result = result.Where(i => i.AccountName == accountName);
            }
            return result.ToList();
        }

        public List<ClaimWorkflowUser> LoadClaimWorkflowUsers(string Workflow, string AccountName)
        {
            var result = new List<ClaimWorkflowUser>();
            var dbContext = context as SecurityBoundedContext;

            var query = dbContext.UserClaimsWorkflows.Where(c => c.AccountName == AccountName && c.Workflow == Workflow && !c.User.Inactive)
                .Select(c => new
                {
                    UserId = c.UserId,
                    UserName = c.User.FirstName + " " + c.User.LastName
                });

            query.ToList().ForEach(c =>
            {
                var claimWorkflowUser = new ClaimWorkflowUser
                {
                    UserId = c.UserId,
                    UserName = c.UserName
                };
                result.Add(claimWorkflowUser);
            });
            return result;
        }

        public List<UserClaimsWorkflow> LoadAllClaimWorkflowUsers()
        {
            var dbContext = context as SecurityBoundedContext;
            return dbContext.UserClaimsWorkflows.Where(c => !c.User.Inactive).ToList();
        }

        public List<Role> LoadParticipantUserRoles(long userId, int participantId, bool isVendor)
        {
            var dbContext = context as SecurityBoundedContext;
            if (isVendor)
            {
                //Load from vendoruser table
                var query = from role in dbContext.Roles
                            join vendoruser in dbContext.VendorUsers on role.ID equals vendoruser.RoleId
                            where vendoruser.UserID == userId && vendoruser.VendorID == participantId && vendoruser.IsActive
                            select role;
                return query.ToList();
            }
            else
            {
                //Load from customeruser table
                var query = from role in dbContext.Roles
                            join customeruser in dbContext.CustomerUsers on role.ID equals customeruser.RoleId
                            where customeruser.UserId == userId && customeruser.CustomerId == participantId && customeruser.IsActive
                            select role;
                return query.ToList();
            }
        }
        public List<Role> LoadParticipantUserRolesView(long userId, int participantId, bool isVendor)
        {
            var dbContext = context as SecurityBoundedContext;
            if (isVendor)
            {
                //Load from vendoruser table
                var query = from role in dbContext.Roles
                            join vendoruser in dbContext.VendorUsers on role.ID equals vendoruser.RoleId
                            where vendoruser.UserID == userId && vendoruser.VendorID == participantId 
                            select role;
                return query.ToList();
            }
            else
            {
                //Load from customeruser table
                var query = from role in dbContext.Roles
                            join customeruser in dbContext.CustomerUsers on role.ID equals customeruser.RoleId
                            where customeruser.UserId == userId && customeruser.CustomerId == participantId 
                            select role;
                return query.ToList();
            }
        }
        public List<Claim> LoadParticipantUserPermissionClaims(long userId, int participantId, bool isVendor)
        {
            var claims = new List<Claim>();
            var dbContext = context as SecurityBoundedContext;
            if (isVendor)
            {
                var query = from role in dbContext.Roles
                            join vendoruser in dbContext.VendorUsers on role.ID equals vendoruser.RoleId
                            where vendoruser.UserID == userId && vendoruser.VendorID == participantId && vendoruser.IsActive
                            select role;

                var participantRolePermissions = new List<RolePermission>();
                query.ToList().ForEach(c =>
                {
                    participantRolePermissions.AddRange(c.RolePermissions);
                });

                var participantUserRolePermissions = participantRolePermissions.AsQueryable().GroupBy(c => new { c.AppResource.ResourceMenu, c.AppResource.ResourceScreen, c.AppResource.Name })
                      .Select(c =>
                      new UserPermissionViewModel
                      {
                          Menu = c.Key.ResourceMenu,
                          Screen = c.Key.ResourceScreen,
                          Panel = c.Key.Name,
                          MaxPermissionLevel = c.Max(i => i.AppPermission.PermissionLevel)
                      }
                ).ToList();

                participantUserRolePermissions.ForEach(c =>
                {
                    var claimValue = string.Format("{0},{1}", c.Menu + "_" + c.Screen + "_" + c.Panel, c.MaxPermissionLevel);
                    var claim = new Claim(TreadMarksConstants.RolePermissions, claimValue);
                    claims.Add(claim);
                });
            }
            else
            {
                var query = from role in dbContext.Roles
                            join customeruser in dbContext.CustomerUsers on role.ID equals customeruser.RoleId
                            where customeruser.UserId == userId && customeruser.CustomerId == participantId && customeruser.IsActive
                            select role;

                var participantRolePermissions = new List<RolePermission>();
                query.ToList().ForEach(c =>
                {
                    participantRolePermissions.AddRange(c.RolePermissions);
                });

                var participantUserRolePermissions = participantRolePermissions.AsQueryable().GroupBy(c => new { c.AppResource.ResourceMenu, c.AppResource.ResourceScreen, c.AppResource.Name })
                      .Select(c =>
                      new UserPermissionViewModel
                      {
                          Menu = c.Key.ResourceMenu,
                          Screen = c.Key.ResourceScreen,
                          Panel = c.Key.Name,
                          MaxPermissionLevel = c.Max(i => i.AppPermission.PermissionLevel)
                      }
                ).ToList();

                participantUserRolePermissions.ForEach(c =>
                {
                    var claimValue = string.Format("{0},{1}", c.Menu + "_" + c.Screen + "_" + c.Panel, c.MaxPermissionLevel);
                    var claim = new Claim(TreadMarksConstants.RolePermissions, claimValue);
                    claims.Add(claim);
                });
            }

            return claims;
        }

        public List<Claim> GetUserVendors(long userId)
        {
            var userVendors = (context as SecurityBoundedContext).VendorUsers.Where(c => c.UserID == userId && c.IsActive).GroupBy(c => c.VendorID).Select(c => c.Key).ToList();
            var userCustomers = (context as SecurityBoundedContext).CustomerUsers.Where(c => c.UserId == userId && c.IsActive).GroupBy(c => c.CustomerId).Select(c => c.Key).ToList();
            var claims = new List<Claim>();
            userVendors.ForEach(c =>
            {
                var claim = new Claim("UserVendor", c.ToString());
                claims.Add(claim);
            });
            userCustomers.ForEach(c =>
            {
                var claim = new Claim("UserVendor", c.ToString());
                claims.Add(claim);
            });
            return claims;
        }

        public void ActiveParticipantUser(long userId, int vendorId, bool isVendor, bool isActive)
        {
            var dbContext = context as SecurityBoundedContext;
            var user = dbContext.Users.FirstOrDefault(c => c.ID == userId);

            if (isVendor)
            {
                //OTSTM2-958 When a User status is changed from UI, all associated users  role records has to be updated
                var vendorUsers = dbContext.VendorUsers.Where(c => c.VendorID == vendorId && c.UserID == userId).ToList();
                vendorUsers.ForEach(c => c.IsActive = isActive);
                if (isActive)
                {
                    if (user.Inactive)
                    {
                        user.Inactive = false;
                    }
                }
                else
                {
                    var allVendorUseres = dbContext.VendorUsers.Where(c => c.UserID == userId && c.VendorID != vendorId);
                    var allCustomerUseres = dbContext.CustomerUsers.Where(c => c.UserId == userId);
                    if ((allVendorUseres.All(c => !c.IsActive)) && (allCustomerUseres.All(c => !c.IsActive)))
                    {
                        user.Inactive = true;
                    }
                }
            }
            else
            {
                var customerUsers = dbContext.CustomerUsers.Where(c => c.CustomerId == vendorId && c.UserId == userId).ToList();
                customerUsers.ForEach(c => c.IsActive = isActive);
                if (isActive)
                {
                    if (user.Inactive)
                    {
                        user.Inactive = false;
                    }
                }
                else
                {
                    var allCustomerUseres = dbContext.CustomerUsers.Where(c => c.UserId == userId && c.CustomerId != vendorId);
                    var allVendorUseres = dbContext.VendorUsers.Where(c => c.UserID == userId);
                    if ((allCustomerUseres.All(c => !c.IsActive)) && (allVendorUseres.All(c => !c.IsActive)))
                    {
                        user.Inactive = true;
                    }
                }
            }
            dbContext.SaveChanges();
        }

        #region //User Roles

        public PaginationDTO<RoleListViewModel, int> GetAllRoles(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as SecurityBoundedContext;
            var query = from role in dbContext.Roles
                        where role.RoleType == 1
                        select new RoleListViewModel
                        {
                            ID = role.ID, //roleID
                            RoleName = role.Name,
                            UpdateDate = role.UpdateDate,
                            AssociatedUsers = role.Users.Where(c => c.Email != "test@test.com").ToList().Count, //OTSTM2-856
                            AllAssociatedUsersList = role.Users.Where(c => c.Email != "test@test.com").Select(u => u.FirstName + " " + u.LastName).ToList() //OTSTM2-856
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.UpdateDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.UpdateDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.UpdateDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.RoleName.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<RoleListViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<RoleListViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RoleListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<string> GetUserNameByRoleName(string roleName)
        {
            var dbContext = context as SecurityBoundedContext;
            var userNameList = dbContext.Roles.Where(r => r.Name == roleName).FirstOrDefault().Users.Select(u => u.FirstName + " " + u.LastName).ToList();
            return userNameList;
        }

        public List<RolePermissionViewModel> GetRolePermissions(string roleName)
        {
            var dbContext = context as SecurityBoundedContext;
            var query = dbContext.RolePermissions.Where(c => c.Role.Name == roleName).Select(c =>
                  new RolePermissionViewModel
                  {
                      Id = c.ID,
                      RoleId = c.RoleId,
                      Menu = c.AppResource.ResourceMenu,
                      Screen = c.AppResource.ResourceScreen,
                      Panel = c.AppResource.Name,
                      ResourceLevel = c.AppResource.ResourceLevel,
                      DisplayOrder = c.AppResource.DisplayOrder,
                      IsNoAccess = c.AppPermission.Name == TreadMarksConstants.NoAccessPermission,
                      IsReadOnly = c.AppPermission.Name == TreadMarksConstants.ReadOnlyPermission,
                      IsEditSave = c.AppPermission.Name == TreadMarksConstants.EditSavePermission,
                      IsCustom = c.AppPermission.Name == TreadMarksConstants.CustomPermission,
                      ResourceMenu = c.AppResource.ResourceMenu,
                      //OTSTM2-838
                      ApplicableLevel = c.AppResource.ApplicableLevel
                  }
            ).OrderBy(c => c.DisplayOrder);
            return query.ToList();
        }

        public void AddNewUserRole(RoleViewModel roleViewModel, string userName, List<AppResource> appResources, List<AppPermission> appPermissions)
        {
            var dbContext = context as SecurityBoundedContext;
            var role = new Role();
            role.Name = roleViewModel.RoleName;
            role.CreateBy = userName;
            role.CreateDate = DateTime.UtcNow;
            role.UpdateDate = DateTime.UtcNow;
            role.RoleType = 1;
            roleViewModel.RolePermissions.ForEach(c =>
            {
                var rolePermission = new RolePermission();
                rolePermission.Role = role;
                rolePermission.AppResourceId = appResources.FirstOrDefault(a => a.Name == c.Panel && a.ResourceMenu == c.Menu && a.ResourceScreen == c.Screen).Id;
                if (c.IsNoAccess)
                {
                    rolePermission.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.NoAccessPermission).Id;
                }
                if (c.IsReadOnly)
                {
                    rolePermission.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.ReadOnlyPermission).Id;
                }
                if (c.IsEditSave)
                {
                    rolePermission.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.EditSavePermission).Id;
                }
                if (c.IsCustom)
                {
                    rolePermission.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.CustomPermission).Id;
                }
                role.RolePermissions.Add(rolePermission);
            });
            dbContext.Roles.Add(role);
            dbContext.SaveChanges();
        }

        public void UpdateUserRole(RoleViewModel roleViewModel, string userName, List<AppPermission> appPermissions)
        {
            var dbContext = context as SecurityBoundedContext;
            var userRole = dbContext.Roles.Include("RolePermissions").FirstOrDefault(c => c.ID == roleViewModel.Id);
            userRole.Name = roleViewModel.RoleName;
            userRole.CreateBy = userName;
            userRole.UpdateDate = DateTime.UtcNow;
            userRole.RolePermissions.ForEach(c =>
            {
                //Update appPermissionId only
                var rolePermission = roleViewModel.RolePermissions.FirstOrDefault(a => a.Id == c.ID);
                if (rolePermission.IsNoAccess)
                {
                    c.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.NoAccessPermission).Id;
                }
                if (rolePermission.IsReadOnly)
                {
                    c.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.ReadOnlyPermission).Id;
                }
                if (rolePermission.IsEditSave)
                {
                    c.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.EditSavePermission).Id;
                }
                if (rolePermission.IsCustom)
                {
                    c.AppPermissionId = appPermissions.FirstOrDefault(a => a.Name == TreadMarksConstants.CustomPermission).Id;
                }
            });
            dbContext.SaveChanges();
        }

        public bool DeleteUserRole(int roleID)
        {
            var dbContext = context as SecurityBoundedContext;
            var userRole = dbContext.Roles.FirstOrDefault(x => x.ID == roleID);
            var associateUsers = userRole.Users.ToList();
            if (userRole != null && (associateUsers == null || associateUsers.Count == 0))
            {
                //delete userrole records of this role
                context.Database.ExecuteSqlCommand("exec sp_deleteuserrole " + roleID);
                return true;
            }
            return false;
        }

        public List<RoleSelectionViewModel> GetStaffUserRoles()
        {
            var dbContext = context as SecurityBoundedContext;
            var query = dbContext.Roles.Where(c => c.RoleType == 1).Select(c => new RoleSelectionViewModel
            {
                Id = c.ID,
                RoleName = c.Name
            }).OrderBy(c => c.RoleName);
            return query.ToList();
        }

        public bool RoleNameExists(string roleName)
        {
            var dbContext = context as SecurityBoundedContext;
            return dbContext.Roles.Where(c => c.Name == roleName).Any();
        }

        public List<UserPermissionViewModel> LoadRolePermissionsForUser(string userName)
        {
            var dbContext = context as SecurityBoundedContext;
            var user = dbContext.Users.FirstOrDefault(c => c.Email == userName);
            var roleIds = user.Roles.Select(c => c.ID).ToList();
            var query = dbContext.RolePermissions.Where(c => roleIds.Contains(c.RoleId)).GroupBy(c => new { c.AppResource.ResourceMenu, c.AppResource.ResourceScreen, c.AppResource.Name, c.AppResource.DisplayOrder, c.AppResource.ResourceLevel })
                .Select(c =>
                  new UserPermissionViewModel
                  {
                      Menu = c.Key.ResourceMenu,
                      Screen = c.Key.ResourceLevel == 0 ? string.Empty : c.Key.ResourceScreen,
                      Panel = (c.Key.ResourceLevel == 0 || c.Key.ResourceLevel == 1) ? string.Empty : c.Key.Name,
                      DisplayOrder = c.Key.DisplayOrder,
                      ResourceLevel = c.Key.ResourceLevel,
                      MaxPermissionLevel = c.Max(i => i.AppPermission.PermissionLevel)
                  }
            ).OrderBy(c => c.DisplayOrder);
            return query.ToList();
        }

        #endregion //User Roles

        public List<UserClaimsWorkflow> LoadClaimsWorkflowForUser(string userEmail)
        {
            var dbContext = context as SecurityBoundedContext;
            var query = dbContext.Users.FirstOrDefault(u => u.Email == userEmail).UserClaimsWorkflows;
            return query.ToList();
        }

        public List<UserApplicationsWorkflow> LoadApplicationsWorkflowForUser(string userEmail)
        {
            var dbContext = context as SecurityBoundedContext;
            var query = dbContext.Users.FirstOrDefault(u => u.Email == userEmail).UserApplicationsWorkflows;
            return query.ToList();
        }
    }
}