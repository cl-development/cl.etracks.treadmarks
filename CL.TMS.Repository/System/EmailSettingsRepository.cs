﻿using CL.TMS.Common.Extension;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.EmailSettings;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CL.TMS.Repository.System
{
    public class EmailSettingsRepository : BaseRepository<EmailSettingsBoundedContext, Email, int>, IEmailSettingsRepository
    {
        //Email Settings - General
        public EmailSettingsGeneralVM LoadEmailSettingsGeneralInformation()
        {
            var dbContext = context as EmailSettingsBoundedContext;
            List<string> lValues = new List<string>() {
                "Email.BccApplicationPath",
                "Email.StewardApplicationApproveBCCEmailAddr",
                "Email.defaultFrom",
                "Email.CBCCEmailClaimAndApplication",
                "Email.CBCCEmailStewardAndRemittance",
                "Email.CBDefaultFromEmailAddr",
                "Email.CBHomepageURL",
                "Email.CBFacebookURL",
                "Email.CBTwitterURL"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();

            EmailSettingsGeneralVM emailSettingsGeneral = new EmailSettingsGeneralVM();
            emailSettingsGeneral.Email_BccApplicationPath = lSettings.FirstOrDefault(i => i.Key == "Email.BccApplicationPath") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.BccApplicationPath").Value : string.Empty;
            emailSettingsGeneral.Email_StewardApplicationApproveBCCEmailAddr = lSettings.FirstOrDefault(i => i.Key == "Email.StewardApplicationApproveBCCEmailAddr") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.StewardApplicationApproveBCCEmailAddr").Value : string.Empty;
            emailSettingsGeneral.Email_defaultFrom = lSettings.FirstOrDefault(i => i.Key == "Email.defaultFrom") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.defaultFrom").Value : string.Empty;
            emailSettingsGeneral.Email_CBCCEmailClaimAndApplication = lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailClaimAndApplication") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailClaimAndApplication").Value.Equals("1") : false;
            emailSettingsGeneral.Email_CBCCEmailStewardAndRemittance = lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailStewardAndRemittance") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailStewardAndRemittance").Value.Equals("1") : false;
            emailSettingsGeneral.Email_CBDefaultFromEmailAddr = lSettings.FirstOrDefault(i => i.Key == "Email.CBDefaultFromEmailAddr") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBDefaultFromEmailAddr").Value.Equals("1") : false;
            emailSettingsGeneral.Email_CBHomepageURL = lSettings.FirstOrDefault(i => i.Key == "Email.CBHomepageURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBHomepageURL").Value.Equals("1") : false;
            emailSettingsGeneral.Email_CBFacebookURL = lSettings.FirstOrDefault(i => i.Key == "Email.CBFacebookURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBFacebookURL").Value.Equals("1") : false;
            emailSettingsGeneral.Email_CBTwitterURL = lSettings.FirstOrDefault(i => i.Key == "Email.CBTwitterURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Email.CBTwitterURL").Value.Equals("1") : false;

            return emailSettingsGeneral;
        }

        public void UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM)
        {
            var dbContext = context as EmailSettingsBoundedContext;
            List<string> lValues = new List<string>() {
                "Email.BccApplicationPath",
                "Email.StewardApplicationApproveBCCEmailAddr",
                "Email.defaultFrom",
                "Email.CBCCEmailClaimAndApplication",
                "Email.CBCCEmailStewardAndRemittance",
                "Email.CBDefaultFromEmailAddr",
                "Email.CBHomepageURL",
                "Email.CBFacebookURL",
                "Email.CBTwitterURL"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();

            lSettings.FirstOrDefault(i => i.Key == "Email.BccApplicationPath").Value = emailSettingsGeneralVM.Email_BccApplicationPath;
            lSettings.FirstOrDefault(i => i.Key == "Email.StewardApplicationApproveBCCEmailAddr").Value = emailSettingsGeneralVM.Email_StewardApplicationApproveBCCEmailAddr;
            lSettings.FirstOrDefault(i => i.Key == "Email.defaultFrom").Value = emailSettingsGeneralVM.Email_defaultFrom;
            lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailClaimAndApplication").Value = emailSettingsGeneralVM.Email_CBCCEmailClaimAndApplication ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailStewardAndRemittance").Value = emailSettingsGeneralVM.Email_CBCCEmailStewardAndRemittance ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Email.CBDefaultFromEmailAddr").Value = emailSettingsGeneralVM.Email_CBDefaultFromEmailAddr ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Email.CBHomepageURL").Value = emailSettingsGeneralVM.Email_CBHomepageURL ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Email.CBFacebookURL").Value = emailSettingsGeneralVM.Email_CBFacebookURL ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Email.CBTwitterURL").Value = emailSettingsGeneralVM.Email_CBTwitterURL? "1" : "0";

            dbContext.SaveChanges();
        }

        //Email Settings - Content
        public Dictionary<int, string> GetEmailDisplayNamesList()
        {
            var dbContext = context as EmailSettingsBoundedContext;          
            return dbContext.Emails.OrderBy(e => e.DisplayName).ToDictionary(e => e.ID, e => e.DisplayName);
        }
       
        public Email GetEmailByID(int emailID)
        {
            var dbContext = context as EmailSettingsBoundedContext;
            return dbContext.Emails.Where(e => e.ID == emailID).FirstOrDefault();
        }

        public Email GetEmailByName(string emailName)
        {
            var dbContext = context as EmailSettingsBoundedContext;
            return dbContext.Emails.Where(e => e.Name == emailName).FirstOrDefault();
        }
        public List<EmailElement> GetEmailElements()
        {
            var dbContext = context as EmailSettingsBoundedContext;
            return dbContext.EmailElements.ToList();
        }

        public void SaveEmailContent(EmailVM emailContent)
        {
            var dbContext = context as EmailSettingsBoundedContext;
            var email = dbContext.Emails.Where(e => e.ID == emailContent.ID).FirstOrDefault();
            email.Body = emailContent.Body;
            email.Subject = emailContent.Subject;
            email.Title = emailContent.Subject;
            email.ButtonLabel = emailContent.ButtonLabel;
            email.IsEnabled = emailContent.IsEnabled;
            email.ModifiedByID = emailContent.ModifiedByID;
            email.ModifiedDate = DateTime.Now;
            dbContext.SaveChanges();            
        }

        public bool IsEmailEnabled(string emailName)
        {
            var dbContext = context as EmailSettingsBoundedContext;
            return dbContext.Emails.Where(e => e.Name == emailName).FirstOrDefault().IsEnabled;
        }        
    }
}