﻿using CL.TMS.Common.Extension;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CL.TMS.Repository.System
{
    public class CompanyBrandingRepository : BaseRepository<CompanyBrandingBoundedContext, TermsAndConditions, int>, ICompanyBrandingRepository
    {
        #region //Loading data

        //Loads all currently effective rates
        public TermsAndConditions GetTermConditionByID(int applicationTypeID)
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            var maxDay = DateTime.MaxValue.Date;
            return dbContext.TermAndConditions.FirstOrDefault(x => x.ApplicationTypeID == applicationTypeID && x.EffectiveEndDate==maxDay);
        }
        public string GetTermConditionContentByID(int? termCondtionID,int applicationTypeID=0) {
            var dbContext = context as CompanyBrandingBoundedContext;
            var result = string.Empty;
            if (termCondtionID.HasValue && termCondtionID.Value > 0)
            {
                var query = dbContext.TermAndConditions.FirstOrDefault(x => x.ID == termCondtionID);
                if (query != null) result = query.Content;
            }
            if (string.IsNullOrEmpty(result) && applicationTypeID>0) {
                var query = dbContext.TermAndConditions.Where(x => x.ApplicationTypeID == applicationTypeID).OrderBy(x => x.EffectiveStartDate).FirstOrDefault();
                if (query != null) result = query.Content;
            }
            return result;
        }
        public int GetCurrentTermConditionID(int applicationTypeID)
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            int result = 0;
            if (applicationTypeID > 0)
            {
                var maxDay = DateTime.MaxValue.Date;
                result = dbContext.TermAndConditions.FirstOrDefault(x => x.ApplicationTypeID == applicationTypeID && x.EffectiveEndDate == maxDay).ID;
            }
            return result;
        }
        #endregion //Loading data

        #region //Save

        public void SaveTermCondition(TermAndConditionVM vm, long userID)
        {           
            var dbContext = context as CompanyBrandingBoundedContext;
            var utcNow = DateTime.UtcNow;
            var maxDay = DateTime.MaxValue.Date;
            var previous = dbContext.TermAndConditions.Where(i => i.EffectiveEndDate == maxDay && i.ApplicationTypeID == vm.applicationTypeID).ToList();
            DateTime endNow = utcNow.AddSeconds(-1);
            //update previous transaction effective end date.
            previous.ForEach(i =>
            {
                i.EffectiveEndDate = endNow;
            });
            var tc = new TermsAndConditions()
            {
                EffectiveStartDate = utcNow,
                EffectiveEndDate = maxDay,
                CreatedByID = userID,
                CreatedDate= utcNow,
                ApplicationTypeID = vm.applicationTypeID,
                Content = vm.content
            };
            //add new term & condition
            dbContext.TermAndConditions.Add(tc);
            if (!string.IsNullOrEmpty(vm.note))
            {  
                // add note  
                var note = new TermsAndConditionsNote()
                {
                    ApplicationTypeID = Convert.ToInt32(vm.applicationTypeID),
                    Note = vm.note,
                    CreatedDate = utcNow,
                    UserID = userID
                };
                    
                dbContext.Notes.Add(note);
            }
            dbContext.SaveChanges();
        }

        #endregion //Add 

        #region //Remove 

        public bool RemoveTermCondition(int id)
        {
            using (var dbContext = context as CompanyBrandingBoundedContext)
            {
                var tc = dbContext.TermAndConditions.FirstOrDefault(x => x.ID == id);
                if (tc != null)
                {
                    dbContext.TermAndConditions.Remove(tc);

                    //Update effectiveenddate for the previous records of ItemWeights
                    DateTime effectiveStartDate = tc.EffectiveStartDate.AddDays(-1);
                    var previous = dbContext.TermAndConditions.Where(i => i.EffectiveEndDate == effectiveStartDate).ToList();
                    previous.ForEach(i =>
                    {
                        i.EffectiveEndDate = DateTime.MaxValue.Date;
                    });
                    context.SaveChanges();
                }
            }
            return true;
        }

        #endregion //Remove Rates

        #region //Notes


        public List<InternalNoteViewModel> LoadNoteByID(int applicationTypeID)
        {          
            var dbContext = context as CompanyBrandingBoundedContext;
            var query=dbContext.Notes.Where(c => c.ApplicationTypeID == applicationTypeID);
            var result = new List<InternalNoteViewModel>();
            if (query.Any())
            {
                result = query.Select(i => new InternalNoteViewModel
                {
                    AddedOn = i.CreatedDate,
                    Note = i.Note,
                    AddedBy = i.User.FirstName + " " + i.User.LastName
                }).OrderByDescending(i => i.AddedOn).ToList();
            }            
            return result;
        }
        public TermsAndConditionsNote AddNote(TermsAndConditionsNote note)
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            dbContext.Notes.Add(note);
            dbContext.SaveChanges();
            return note;
        }
        public List<InternalNoteViewModel> ExportNotesToExcel(int applicationTypeID, bool sortReverse, string sortcolumn, string searchText)
        {
            var dbContext = context as CompanyBrandingBoundedContext;

            var query = from note in dbContext.Notes.AsNoTracking()
                        where note.ApplicationTypeID == applicationTypeID
                        select new InternalNoteViewModel()
                        {
                            AddedOn = note.CreatedDate,
                            Note = note.Note,
                            AddedBy = note.User.FirstName + " " + note.User.LastName,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Note.ToLower().Contains(searchText)
                                        || c.AddedBy.Contains(searchText)
                                        );
                }

            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;
            query = !sortReverse ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");
            var totalRecords = query.Count();
            return query.ToList();
        }

        #endregion

        #region Company Information
        public CompanyInformationVM LoadCompanyInformation()
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            List<string> lValues = new List<string>() {
                "Company.Name",
                "Company.AddressLine1",
                "Company.AddressLine2",
                "Company.City",
                "Company.Province",
                "Company.PostalCode",
                "Company.Country",
                "Company.Phone",
                "Company.PhoneExt",
                "Company.Fax",
                "Company.Email",
                "Company.WebsiteURL",
                "Company.FacebookURL",
                "Company.TwitterURL"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();

            CompanyInformationVM companyInfo = new CompanyInformationVM();
            companyInfo.CompanyName = lSettings.FirstOrDefault(i => i.Key == "Company.Name") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Name").Value : string.Empty;
            companyInfo.AddressLine1 = lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine1") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine1").Value : string.Empty;
            companyInfo.AddressLine2 = lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine2") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine2").Value : string.Empty;
            companyInfo.City = lSettings.FirstOrDefault(i => i.Key == "Company.City") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.City").Value : string.Empty;
            companyInfo.Province = lSettings.FirstOrDefault(i => i.Key == "Company.Province") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Province").Value : string.Empty;
            companyInfo.PostalCode = lSettings.FirstOrDefault(i => i.Key == "Company.PostalCode") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.PostalCode").Value : string.Empty;
            companyInfo.Country = lSettings.FirstOrDefault(i => i.Key == "Company.Country") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Country").Value : string.Empty;
            companyInfo.PhoneNumber = lSettings.FirstOrDefault(i => i.Key == "Company.Phone") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Phone").Value : string.Empty;
            companyInfo.Ext = lSettings.FirstOrDefault(i => i.Key == "Company.PhoneExt") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.PhoneExt").Value : string.Empty;
            companyInfo.Fax = lSettings.FirstOrDefault(i => i.Key == "Company.Fax") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Fax").Value : string.Empty;
            companyInfo.Email = lSettings.FirstOrDefault(i => i.Key == "Company.Email") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.Email").Value : string.Empty;
            companyInfo.WebSite = lSettings.FirstOrDefault(i => i.Key == "Company.WebsiteURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.WebsiteURL").Value : string.Empty;
            companyInfo.Facebook = lSettings.FirstOrDefault(i => i.Key == "Company.FacebookURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.FacebookURL").Value : string.Empty;
            companyInfo.Twitter = lSettings.FirstOrDefault(i => i.Key == "Company.TwitterURL") != null ? lSettings.FirstOrDefault(i => i.Key == "Company.TwitterURL").Value : string.Empty;          

            return companyInfo;
        }

        public void UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath)
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            List<string> lValues = new List<string>() {
                "Company.Name",
                "Company.AddressLine1",
                "Company.AddressLine2",
                "Company.City",
                "Company.Province",
                "Company.PostalCode",
                "Company.Country",
                "Company.Phone",
                "Company.PhoneExt",
                "Company.Fax",
                "Company.Email",
                "Company.WebsiteURL",
                "Company.FacebookURL",
                "Company.TwitterURL",
                "Company.LogoURL"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();
            
            lSettings.FirstOrDefault(i => i.Key == "Company.Name").Value = companyInformationVM.CompanyName;
            lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine1").Value = companyInformationVM.AddressLine1;
            lSettings.FirstOrDefault(i => i.Key == "Company.AddressLine2").Value = companyInformationVM.AddressLine2;
            lSettings.FirstOrDefault(i => i.Key == "Company.City").Value = companyInformationVM.City;
            lSettings.FirstOrDefault(i => i.Key == "Company.Province").Value = companyInformationVM.Province;
            lSettings.FirstOrDefault(i => i.Key == "Company.PostalCode").Value = companyInformationVM.PostalCode;
            lSettings.FirstOrDefault(i => i.Key == "Company.Country").Value = companyInformationVM.Country;
            lSettings.FirstOrDefault(i => i.Key == "Company.Phone").Value = companyInformationVM.PhoneNumber;
            lSettings.FirstOrDefault(i => i.Key == "Company.PhoneExt").Value = companyInformationVM.Ext;
            lSettings.FirstOrDefault(i => i.Key == "Company.Fax").Value = companyInformationVM.Fax;
            lSettings.FirstOrDefault(i => i.Key == "Company.Email").Value = companyInformationVM.Email;
            lSettings.FirstOrDefault(i => i.Key == "Company.WebsiteURL").Value = companyInformationVM.WebSite;
            lSettings.FirstOrDefault(i => i.Key == "Company.FacebookURL").Value = companyInformationVM.Facebook;
            lSettings.FirstOrDefault(i => i.Key == "Company.TwitterURL").Value = companyInformationVM.Twitter;
            lSettings.FirstOrDefault(i => i.Key == "Company.LogoURL").Value = logoFilePath;

            dbContext.SaveChanges();
        }

        public string LoadCompanyLogoUrl()
        {
            var dbContext = context as CompanyBrandingBoundedContext;
            Setting setting = dbContext.AppSettings.FirstOrDefault(i => i.Key == "Company.LogoURL");
            var url = setting.Value;
           
            return url;
        }

        #endregion        
    }
}