﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Repository
{
    public class RepositoryHelper
    {
        public static Contact FindPrimaryContact(List<Address> lstAddress)
        {
            Contact primaryContact = null;

            var contactAddressList = lstAddress.Where(c => c.AddressType == 3).ToList();
            foreach(var contactAddress in contactAddressList)
            {
                primaryContact = contactAddress.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                if (primaryContact!=null)
                {
                    break;
                }
            }            
            if (primaryContact == null)
            {
                var businessAddess = lstAddress.FirstOrDefault(c => c.AddressType == 1);
                if (businessAddess != null) primaryContact = businessAddess.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);                
            }

            return primaryContact;
        }
    }
}
