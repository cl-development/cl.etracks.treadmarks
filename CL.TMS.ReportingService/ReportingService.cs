﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.Repository.Claims;
using CL.TMS.Repository.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Timers;

namespace CL.TMS.ReportingService
{
    public partial class ReportingService : ServiceBase
    {
        private Timer _timer;
        public ReportingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(60 * 60 * 1000); //every hour to run job
            _timer.Enabled = true;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var now = DateTime.Now;
            //Check if it is 3:00am to 4:00am
            if (IsTimeOfDayBetween(now, new TimeSpan(3, 0, 0), new TimeSpan(4, 0, 0)))
            {
                PopulateReportData();
            }
        }

        private bool IsTimeOfDayBetween(DateTime time, TimeSpan startTime, TimeSpan endTime)
        {
            if (endTime == startTime)
            {
                return true;
            }
            else if (endTime < startTime)
            {
                return time.TimeOfDay < endTime ||
                    time.TimeOfDay > startTime;
            }
            else
            {
                return time.TimeOfDay > startTime &&
                    time.TimeOfDay < endTime;
            }

        }

        protected override void OnStop()
        {
            _timer.Enabled = false;
            _timer.Stop();
        }

        #region Populate Report Data

        /// <summary>
        /// Populate report data for those claim missing report data in rptclaiminventory table
        /// </summary>
        private void PopulateReportData()
        {
            LogManager.LogInfo("Start populating claim inventory report data ...");
            try
            {
                var claimRepository = new ClaimsRepository();
                var claimDetails = claimRepository.GetMissingReportDataTransactionIds();
                var settingRepository = new SettingRepository();
                var items = settingRepository.LoadAllItemsForReportingService();
                var transactionItems = items.Where(c => c.ItemCategory == 5).ToList();

                claimDetails.ForEach(c =>
                {
                    PopulateClaimInventoryDetail(c.ClaimId, c.TransactionId, items, transactionItems);
                });
            }
            catch (Exception ex)
            {
                LogManager.LogExceptionWithMessage("Failed to populate report data", ex);
            }
            LogManager.LogInfo("Done of populating claim inventory report data ...");
        }

        private void PopulateClaimInventoryDetail(int claimId, int transactionId, List<Item> items, List<Item> transactionItems)
        {
            //Prepare data for calculation
            var claimsRepository = new ClaimsRepository();
            var claim = claimsRepository.FindClaimByClaimIdForReport(claimId);
            var claimDetails = claimsRepository.LoadClaimDetailsForReport(claimId, transactionId, items);
            var claimInventoryDetails = CalculateClaimInventoryDetails(claim, claimDetails, transactionItems);
            claimsRepository.UpdateClaimInventoryDetails(claim.ID, transactionId, claimInventoryDetails);
        }

        private List<ClaimInventoryDetail> CalculateClaimInventoryDetails(Claim claim, List<ClaimDetailViewModel> claimDetails, List<Item> transactionItems)
        {
            var claimInventoryDetails = new List<ClaimInventoryDetail>();
            var pltItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = transactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            claimDetails.ForEach(c =>
            {
                if ((c.PLTQty > 0) || (c.PLTActualWeight > 0) || (c.PLTAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, pltItem, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, TreadMarksConstants.Kg));
                }
                if ((c.MTQty > 0) || (c.MTActualWeight > 0) || (c.MTAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, mtItem, c.MTActualWeight, c.MTAverageWeight, c.MTQty, TreadMarksConstants.Kg));
                }
                if ((c.AGLSQty > 0) || (c.AGLSActualWeight > 0) || (c.AGLSAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, aglsItem, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, TreadMarksConstants.Kg));
                }
                if ((c.INDQty > 0) || (c.INDActualWeight > 0) || (c.INDAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, indItem, c.INDActualWeight, c.INDAverageWeight, c.INDQty, TreadMarksConstants.Kg));
                }
                if ((c.SOTRQty > 0) || (c.SOTRActualWeight > 0) || (c.SOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, sotrItem, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.MOTRQty > 0) || (c.MOTRActualWeight > 0) || (c.MOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, motrItem, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.LOTRQty > 0) || (c.LOTRActualWeight > 0) || (c.LOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, lotrItem, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.GOTRQty > 0) || (c.GOTRActualWeight > 0) || (c.GOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, gotrItem, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, TreadMarksConstants.Kg));
                }

                //Other item -- it should be first transaction item
                if ((c.PLTQty == 0) && (c.MTQty == 0) && (c.AGLSQty == 0) && (c.INDQty == 0) && (c.SOTRQty == 0) && (c.MOTRQty == 0) && (c.LOTRQty == 0) && (c.GOTRQty == 0))
                {
                    var transactionItem = c.TransactionItems.FirstOrDefault();
                    if (transactionItem != null)
                    {
                        var item = transactionItem.Item;
                        claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, item, transactionItem.ActualWeight, transactionItem.AverageWeight, transactionItem.Quantity ?? 0, TreadMarksConstants.Kg));
                    }
                }
            });

            return claimInventoryDetails;
        }

        private ClaimInventoryDetail GetClaimInventoryDetail(Claim claim, ClaimDetailViewModel c, Item item, decimal actualWeight, decimal averageWeight, int qty, int unitType)
        {
            var claimInventoryDetail = new ClaimInventoryDetail();
            claimInventoryDetail.ClaimId = claim.ID;
            claimInventoryDetail.TransactionId = c.Transaction.ID;
            claimInventoryDetail.Direction = c.Direction;
            claimInventoryDetail.ItemId = item.ID;
            claimInventoryDetail.ItemType = item.ItemType;
            claimInventoryDetail.ItemShortName = item.ShortName;
            claimInventoryDetail.ItemName = item.Name;
            claimInventoryDetail.Description = item.Description;
            claimInventoryDetail.Quantity = qty;
            claimInventoryDetail.AverageWeight = averageWeight;
            claimInventoryDetail.ActualWeight = actualWeight;
            claimInventoryDetail.UnitType = unitType;
            claimInventoryDetail.TransactionTypeCode = c.Transaction.TransactionTypeCode;
            claimInventoryDetail.TransactionProcessingStatus = c.Transaction.ProcessingStatus;
            claimInventoryDetail.ClaimPeriodId = claim.ClaimPeriodId;
            claimInventoryDetail.PeriodName = claim.ClaimPeriod.ShortName;
            claimInventoryDetail.PeriodStartDate = claim.ClaimPeriod.StartDate;
            claimInventoryDetail.PeriodEndDate = claim.ClaimPeriod.EndDate;
            claimInventoryDetail.VendorId = claim.ParticipantId;
            claimInventoryDetail.VendorType = claim.Participant.VendorType;
            claimInventoryDetail.BusinessName = claim.Participant.BusinessName;
            claimInventoryDetail.RegistrationNumber = claim.Participant.Number;
            claimInventoryDetail.CreatedDate = DateTime.UtcNow;
            var transactionItem = c.TransactionItems.FirstOrDefault();
            if (transactionItem != null)
            {
                claimInventoryDetail.ItemDescription = transactionItem.ItemDescription;
                claimInventoryDetail.ItemCode = transactionItem.ItemCode;
                claimInventoryDetail.TransactionItemRate = transactionItem.Rate;
            }
            return claimInventoryDetail;
        }
        #endregion
    }
}
