﻿namespace CL.TMS.ReportingService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerReporting = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerReporting = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerReporting
            // 
            this.serviceProcessInstallerReporting.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.serviceProcessInstallerReporting.Password = null;
            this.serviceProcessInstallerReporting.Username = null;
            // 
            // serviceInstallerReporting
            // 
            this.serviceInstallerReporting.DisplayName = "TM Reporting Service";
            this.serviceInstallerReporting.ServiceName = "ReportingService";
            this.serviceInstallerReporting.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerReporting,
            this.serviceInstallerReporting});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerReporting;
        private System.ServiceProcess.ServiceInstaller serviceInstallerReporting;
    }
}