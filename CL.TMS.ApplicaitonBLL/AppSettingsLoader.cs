﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Caching;
using CL.TMS.Common;
using CL.TMS.IRepository;

namespace CL.TMS.ApplicaitonBLL
{
    public class AppSettingsLoader
    {
        
        private ISettingRepository repository;
        public AppSettingsLoader(ISettingRepository repository)
        {
            this.repository = repository;
        }
        public List<Setting> LoadAllSettings()
        {
            if (CachingHelper.ContainsKey(CachKeyManager.AppSettingsKey))
            {
                return CachingHelper.GetItem<List<Setting>>(CachKeyManager.AppSettingsKey);
            }
            else
            {
                //Load form database
                var allSettings = repository.GetAll().ToList();
                //Add all setting to cache
                CachingHelper.AddItem(CachKeyManager.AppSettingsKey, allSettings);
                return allSettings;
            }
        }
    }
}
