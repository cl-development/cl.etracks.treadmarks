﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CL.TMS.Web.Infrastructure
{
    public class NewtonSoftJsonResult : ActionResult
    {
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }
        public JsonSerializerSettings SerializerSettings { get; set; }
        public Formatting Formatting { get; set; }

        public NewtonSoftJsonResult()
        {
            SerializerSettings = new JsonSerializerSettings();     
       
            SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        public NewtonSoftJsonResult(object data, bool camelCaseSetting=true)
        {
            SerializerSettings = new JsonSerializerSettings();
            if (camelCaseSetting)
            {
                SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }
            else
            {
                SerializerSettings.ContractResolver = new DefaultContractResolver();
            }
            Data = data;
        }


        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null) response.ContentEncoding = ContentEncoding;

            if (Data != null)
            {
                // using Json.NET to serialize
                var writer = new JsonTextWriter(response.Output) { Formatting = Formatting };
                var serializer = JsonSerializer.Create(SerializerSettings);
                serializer.Serialize(writer, Data);
                writer.Flush();
            }
        }
    }
}