﻿using CL.Framework.Logging;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.ExceptionHandling;
using CL.TMS.UI.Common.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using SystemIO = System.IO;

namespace CL.TMS.Web.Infrastructure
{
    public static class TransactionCommonHelper
    {
        public static bool SendAddTransactionEmailNotification(CL.TMS.DataContracts.ViewModel.Transaction.TransactionCreationNotificationDetailViewModel notificationDetailModel)
        {
            if (notificationDetailModel == null || !EmailContentHelper.IsEmailEnabled("NewTransactionAdded")) return false;
            try
            {
                Email.Email emailer = new Email.Email(
                    Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL"))
                );

                //string subject = "TreadMarks Notification for New Transaction";
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\PaperTransactionAddedEmailTemplate.html");
                //string body = SystemIO.File.ReadAllText(htmlbody);

                string body = EmailContentHelper.GetCompleteEmailByName("NewTransactionAdded");
                string subject = EmailContentHelper.GetSubjectByName("NewTransactionAdded");

                var siteUrl = string.Format("{0}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"));
                body = body.Replace("@siteUrl", siteUrl);

                body = body.Replace("@_TransactionType", notificationDetailModel.TransactionType);
                body = body.Replace("@_TransactionNumber", notificationDetailModel.TransactionNumber);
                body = body.Replace("@_CreatedBy", notificationDetailModel.CreatedBy);
                body = body.Replace("@_CreatedOn", notificationDetailModel.CreatedOn);
                body = body.Replace("@_DatetimeNowYear", DateTime.Now.Year.ToString()); //OTSTM2-979

                AlternateView alternateViewHTML = AttachLinkedResources(body);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), notificationDetailModel.Email, null, null, subject, body, null, null, alternateViewHTML);
                });

                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);

                if (rethrow)
                {
                    //throw; 
                }
                LogManager.LogExceptionWithMessage("Failed to email add transaction notification", ex);

                return false;
            }
        }

        public static bool SendTransactionAdjustmentEmailNotification(CL.TMS.DataContracts.ViewModel.Transaction.TransactionAdjustmentNotificationDetailViewModel notificationDetailModel)
        {
            if (!EmailContentHelper.IsEmailEnabled("TransactionAdjusted"))
                return false;

            try
            {
                Email.Email emailer = new Email.Email(
                    Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL"))
                );

                //string subject = "TreadMarks Notification for Transaction Adjustment";
                ///string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\TransactionAdjustmentEmailTemplate.html");
                //string body = SystemIO.File.ReadAllText(htmlbody);

                string body = EmailContentHelper.GetCompleteEmailByName("TransactionAdjusted");
                string subject = EmailContentHelper.GetSubjectByName("TransactionAdjusted");

                var siteUrl = string.Format("{0}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"));
                body = body.Replace("@siteUrl", siteUrl);

                body = body.Replace("@_TransactionType", notificationDetailModel.TransactionType);
                body = body.Replace("@_TransactionNumber", notificationDetailModel.TransactionNumber);
                body = body.Replace("@_InitiatedBy", notificationDetailModel.InitiatedBy);
                body = body.Replace("@_ClaimPeriod", notificationDetailModel.ClaimPeriod);
                body = body.Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                AlternateView alternateViewHTML = AttachLinkedResources(body);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), notificationDetailModel.Email, null, null, subject, body, null, null, alternateViewHTML);
                });

                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);

                if (rethrow)
                {
                    //throw; 
                }
                LogManager.LogExceptionWithMessage("Failed to email add transaction notification", ex);

                return false;
            }
        }

        private static AlternateView AttachLinkedResources(string body)
        {
            AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = "@CompanyLogo";
            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = "@TwitterLogo";
            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = "@ApplicationLogo";
            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            //OTSTM2-1003           
            var uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //var uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }
            return alternateViewHTML;
        }
    }
}