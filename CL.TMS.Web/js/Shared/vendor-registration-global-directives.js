﻿'use strict';

//helper functions
function checkZero(item) {
    return item.PLT == 0 && item.AGLS == 0 && item.GOTR == 0
            && item.IND == 0 && item.LOTR == 0
            && item.MOTR == 0 && item.MT == 0 && item.SOTR == 0;
}

function findAddedTire(element, tire) {
    return (element.Description === tire.TireOrigin && element.DefinitionValue === tire.TireOriginValue);
}

function setEachZero(curr, value, index) {
    if (typeof (curr[value]) == 'undefined' || !curr[value]) {
        curr[value] = 0;
    }
}


