﻿"use strict";

var commonClaimInternalNotesLib = angular.module("commonClaimInternalNotesLib", ['ui.bootstrap', 'datatables', 'datatables.scroller']);
var commonClaimInternalNotesApp = angular.module("commonClaimInternalNotesApp", ["ui.bootstrap", "commonLib", "commonClaimInternalNotesLib"]);

//this is a copy from \\CL.TMS.Web\js\Claims\common-claim-lib.js
commonClaimInternalNotesLib.directive('manageInternalNotes', ['$compile', '$window', '$http', '$filter', function ($compile, $window, $http, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'claim-internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@',
            parentId: '@',
        },
        link: function (scope, elem, attr, ngModel) {
            scope.noteTDWidthpixel = 0;//document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;//not exists yet
            scope.elem = elem;
            scope.focus = function () {
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            scope.removeIcon = function ($event) {
                scope.searchNote = '';
                var search = $event.currentTarget.parentElement.firstElementChild;
                $(search).focus();
            }

            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                }
            })

            scope.search = function () {

                //scope.internalNoteLength = scope.filtered.length;
                scope.internalNoteLength = $filter('filter')(scope.allInternalNotes, scope.searchNote).length;
            };

            scope.add = function () {
                if (scope.textAreaModel) {
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            parentId: scope.parentId,
                            notes: scope.textAreaModel
                        })
                    }).success(function (result) {
                        //Load results after 
                        $http({
                            url: scope.loadUrl,
                            method: "GET",
                            params: {
                                parentId: scope.parentId,
                            },
                        }).success(function (result) {
                            scope.textAreaModel = '';
                            scope.allInternalNotes = result;
                            //scope.quantity = scope.allInternalNotes.length;
                            //if (scope.quantity > 4) {
                            //    scope.showMore = false;
                            //}
                        })
                    })
                }
            };

            scope.stripNotes = function (note) {

                var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one //.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                var trimWidth = 0;
                var bestFitNbr = 50;
                var shortNote = '';
                ///if trimmed-note text width is longer than current TD.offsetWidth, try to find best match string.length of noteTrimedStr, then return to UI. 
                ///since string.length affect webui-popover horizontal position
                if (noteWidth + 15 >= scope.noteTDWidthpixel) { //OTSTM2-845 make sure "..." effect is same with or without vertical scrollbar
                    while (trimWidth < scope.noteTDWidthpixel - 15) {
                        bestFitNbr += 2;
                        shortNote = noteTrimedStr.substr(0, bestFitNbr);//incrase str size and try again
                        trimWidth = getTextWidth(shortNote, "0.875em Open Sans");
                    }
                    return shortNote + "...";
                }
                else {
                    return note;
                }
            };

            scope.checkNoteSize = function (note) {
                if (note != undefined) {
                    var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width') ? document.getElementById(scope.panelArrow + '-notes-width').offsetWidth : 0;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    var tdAddedBy = document.getElementById('tdAddedBy');
                    if (hasScrollBar(internalNoteBody)) { //OTSTM2-845 check if the vertical scrollbar appears
                        $(tdAddedBy).css("margin-left", "-20px"); //OTSTM2-845 "AddedBy" move left for alignment if vertical scrollbar appears
                        return (scope.noteTDWidthpixel <= noteWidth + 32);
                    }
                    else {
                        return (scope.noteTDWidthpixel <= noteWidth + 6);
                    }
                }
                else {
                    return false;
                }
            };

            angular.element($window).bind('resize', function () {

                var length = scope.allInternalNotes.length;
                for (var i = 0; i < length; i++) {
                    var noteTrimedStr = scope.allInternalNotes[i].Note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    var trimWidth = 0;
                    var bestFitNbr = 50;
                    var shortNote = scope.stripNotes(scope.allInternalNotes[i].Note);
                    scope.noteTDWidthpixel = 0;
                    var $div = $("<td ng-if=\"checkNoteSize(note.Note)\" width=\"75%\" style=\"cursor: pointer\" class=\"internal-note{{$index}}\"><span internal-note-hover-popover data-note=\"{{note.Note}}\">{{note.Note}}</span></td>");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width') ? document.getElementById(scope.panelArrow + '-notes-width').offsetWidth : 0;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    if (hasScrollBar(internalNoteBody)) {
                        if (scope.noteTDWidthpixel <= noteWidth + 32) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();

                            }
                        }
                    }
                    else {
                        if (scope.noteTDWidthpixel <= noteWidth + 6) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();
                            }
                        }
                    }
                }
                scope.$digest();
            });

        },
        controller: function ($scope, $rootScope) {

            $http({
                url: $scope.loadUrl,
                method: "GET",
                params: {
                    parentId: $scope.parentId,
                },
            }).then(function (result) {
                $scope.allInternalNotes = result.data;
                $scope.sortType = 'AddedOn';
                $scope.textAreaModel = '';
                $scope.sortReverse = true;
                $scope.searchNote = '';
                $scope.quantity = 5;
                //$scope.showMore = result.data.length > 4;//true;
                $scope.showMore = true;
                $scope.export = Global.InternalNoteSettings.InternalExportUrl;
            })

            $rootScope.$on('TIRE_ORIGIN_INTERNAL_NOTE', function (e, data) { //OTSTM2-386
                $scope.allInternalNotes = data;
                $scope.quantity = $scope.allInternalNotes.length;
                //if ($scope.quantity > 4) {
                //    $scope.showMore = false;
                //}
            });

        }
    };
}]);

commonClaimInternalNotesLib.directive('internalNoteHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {

    var getTemplate = function (contentType) {
        return $templateCache.get('internalNotePopoverTemplate.html');
    };

    var header = '<span><strong>Internal Notes</strong></span>' +
        '<button type="button" class="close" ng-click="closeMe()")><span aria-hidden="true">×</span></button>';

    var getHeader = function (contentType) {
        return $templateCache.get('internalNotePopoverHeader.html');
    };

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var note = toHtml(attrs.note);
            $templateCache.put('internalNotePopoverTemplate.html', '<div style="overflow: auto; height: 300px; padding:true;">' + note + '</div>');
            $templateCache.put('internalNotePopoverHeader.html', header);

            var content = getTemplate();
            var popoverheader = getHeader();

            $rootScope.insidePopover = false;
            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                //max-width:500px;
                placement: 'top',
                title: function () {
                    return $compile(popoverheader)(scope);
                },
                html: true
            }).on("show.bs.popover", function () {
                $(this).data("bs.popover").tip().css({ "width": "500px", "max-width": "500px", "cursor": "default" });
            });

            $(element).bind('click', function (e) {
                $timeout(function () {
                    if ($("div.popover").length > 1) {//if more then popover open
                        $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close existing one
                    }
                    Global.InternalNoteSettings.currentOpenId = $("div.popover.in").attr('id');
                }, 100);
            });

            scope.closeMe = function () {
                $(element).popover('hide');
            }

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }

        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                //$('.popover').on('mouseenter', function () {
                //    $rootScope.insidePopover = true;
                //});
                //$('.popover').on('mouseleave', function () {
                //    $rootScope.insidePopover = false;
                //    $(element).popover('hide');
                //});
            }

            $('#processor-claims-summary-notes').on('hidden.bs.collapse', function () {
                $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close opened popover
            });
        }
    };
});

commonClaimInternalNotesLib.directive('internalNotesExport', ['$http', '$document', function ($http, $document) {

    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            id: '@',
            searchText: '@',
            sortReverse: '@',
            sortColumn: '@',
            url: '@',
            model: '=',
            fileName: '@'
        },
        link: function (scope, elem, attr, ngModel) {
            elem.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                var req = { parentId: scope.$parent.parentId, searchText: scope.searchText, sortReverse: scope.sortReverse, sortcolumn: scope.sortColumn, model: scope.model };
                var submitVal = {
                    url: scope.url,
                    method: "GET",
                    //params: JSON.stringify(req)
                    params: req,
                }
                return $http(submitVal).then(function (result) {

                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = scope.fileName + "-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });
            });
        },
        controller: function ($scope) {

        }
    };
}]);

function hasStyle(styles, prop) {
    var result = "";

    styles && styles.split(";").forEach(function (e) {
        var style = e.split(":");
        if ($.trim(style[0]) === prop) {
            result = style[1];
        }
    });
    return result;
};

document.body.addEventListener('click', function (e) {
    if (Global.InternalNoteSettings == undefined) {//participant page
        return;
    }
    var insidePopover = clickInsidePopover(e.target);
    if (!insidePopover) {
        $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close opened popover
    }
}, false);

function clickInsidePopover(e) {
    if ((e == null) || (e == undefined)) {
        return false;
    } else if ((e.className === 'popover-content') || (e.className === 'popover-title') || (e.className.startsWith('popover fade'))) {
        return true;
    }
    else {
        return clickInsidePopover(e.parentElement);
    }
}

//OTSTM2-845 option 1 for getting text width, need to add a <canvas> tag
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    //var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var canvas = document.getElementById('myCanvas'); //OTSTM2-845 get the added <canvas>
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return Math.round(metrics.width);
}

//OTSTM2-845 option 2 for getting text width
function getTextWidthDOM(text, font) {
    var f = font || '12px arial',
        o = $('<span>' + text + '</span>')
              .css({ 'font': f, 'float': 'left', 'white-space': 'nowrap' })
              .css({ 'visibility': 'hidden' })
              .appendTo($('body')),
        w = o.width();
    o.remove();
    return w;
}

//OTSTM2-845 check if vertical scrollbar appears
function hasScrollBar(obj) {
    if (obj) {
        return $(obj).get(0).scrollHeight > $(obj).height();
    } else {
        return false;
    }
}
