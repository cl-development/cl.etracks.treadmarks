/* global CurrentUser */
(function ($) {

    $.fn.createDataTable = function (options) {
        TM = {};
        TM.settings = $.extend({
            ajaxHandler: '',
            viewMoreId: '',
            totalRecordsId: '',
            searchId: '',
            exportId: '',
            foundId: '',
            removeIconId: '',
            serverSide: true,
            scrollY: 170, //OTSTM2-857, the height of note section
            scrollCollapse: false,
            scrollX: true,
            autoWidth: true,
            rowUrl: '',
            ColumnId: 0,
            pageSize: 5, 
        }, options);
        dynamicColumns = [];
        for (var i = 0; i < TM.settings.columnNames.length; i++) {
            var columnName = TM.settings.columnNames[i].columnName ? TM.settings.columnNames[i].columnName : '';
            var className = TM.settings.columnNames[i].className ? TM.settings.columnNames[i].className : '';

            var row = {
                className: className,
                data: null,
                render: function (data, type, full, meta) {
                    if (TM.settings.columnNames[meta.col].columnName) {
                        var columnName = TM.settings.columnNames[meta.col].columnName;

                        if (columnName == 'Note') {
                            var html = '';
                            var note = data[columnName];

                            if (note.length > TM.settings.notesLength) {
                                var shortNote = note.substr(0, TM.settings.notesLength);
                                html = '<span tabindex="124" id="notesNbr' + meta.row + '" data-placement="top" data-text="' + note + '">' + shortNote + '...</span>'
                            }
                            else {
                                html = '<span>' + note + '</span>'
                            }
                            return html;
                        }
                        else {
                            return data[columnName];
                        }
                    } else {
                        return '';
                    }
                }
            };
            dynamicColumns.push(row);
        }

        var table = this.DataTable({
            serverSide: TM.settings.serverSide,
            ajax: TM.settings.ajaxHandler,
            scrollY: TM.settings.scrollY,
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            scrollCollapse: true,
            processing: false,
            //"paging" : false,
            order: [[0, "desc"]],
            searching: true,
            "dom": "rtiS",
            info: false,
            deferRender: true,
            "scroller": {
                displayBuffer: 100,
                rowHeight: 70,
                serverWait: 100,
                loadingIndicator: false
            },
            columns: [
                {
                    name: "CreatedDateString",
                    width: "12%", //OTSTM2-857
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.CreatedDateString + "</span>";
                    }
                },
                {
                    name: "Note",
                    width: "75%", //OTSTM2-857
                    data: null,
                    render: function (data, type, full, meta) {
                        var html = '';
                        var bestFitNbr = 100;
                        //TODO: should refactor replace code to a helper function
                        var note = data.Note.replace(/['"]+/g, '').replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;").replace(/ {1,}/g, " ");//replace multiple space with one
                        var shortNote = '';
                        var noteWidth = getTextWidth(note, "14px Open Sans");
                        var trimWidth = 0;
                        var panelWidth = document.getElementById("panelInternalNotesHeading-headingOne").offsetWidth;
                        var clientWidth = Math.round(panelWidth * 0.68);//estimate of column-Note width. When table is invislable(chevron closed), .offsetWidth of td will be 0, use width of internal notes chevron instead
                        if (noteWidth >= clientWidth) {
                            while (trimWidth < clientWidth) {
                                bestFitNbr += 2;
                                shortNote = note.substr(0, bestFitNbr);//incrase str size and try again
                                trimWidth = getTextWidth(shortNote, "14px Open Sans");
                            }
                            html = '<span tabindex="124" id="notesNbr' + meta.row + '" data-placement="top" data-text="' + note + '">' + shortNote + '...</span>';
                        }
                        else {
                            html = '<span>' + note + '</span>';
                        }
                        return html;
                    }
                },
                {
                    name: "AddedBy",
                    width: "13%", //OTSTM2-857
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.AddedBy + "</span>";
                    }
                }
            ],
            initComplete: function (settings, json) {

                $('.dataTables_scrollBody').css('overflow-y', 'hidden');
                $(TM.settings.totalRecordsId).val('Total ' + settings.fnRecordsTotal());
            },
            drawCallback: function (settings) {
                var direction = settings.aaSorting[0][1];
                $('.sort').html('<i class="fa fa-sort"></i>');
                (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
                $(TM.settings.foundId).html('Found ' + settings.fnRecordsDisplay());
                (settings.fnDisplayEnd() >= TM.settings.pageSize) ? $(TM.settings.viewMoreId).css('visibility', 'visible') : $(TM.settings.viewMoreId).css('visibility', 'hidden');

                var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                var searchValue = $(TM.settings.searchId).val();
                var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
                $(TM.settings.exportId).attr('href', url);
            }
        });

        table.on("draw.dt", function () {
            $("[id^='notesNbr']").css("cursor", "pointer");
            $("[id^='notesNbr']").webuiPopover({
                width: '500',
                height: '300',
                padding: true,
                multi: false,
                closeable: true,
                title: 'Internal Notes',
                type: 'html',
                trigger: 'click',

                content: function () {
                    var internalNote = $(this).attr('data-text');
                    var res = internalNote.split('\n');
                    var result = "";
                    var arrayLength = res.length;
                    for (var i = 0; i < arrayLength; i++) {
                        result = result + '<p>' + res[i] + '</p>';
                    }
                    return result;
                },
                delay: { show: 100, hide: 100 },
            });

        });

        $(TM.settings.viewMoreId).on('click', function () {
            $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
            $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
            $(this).hide();
        });

        $(TM.settings.searchId).on('keyup', function () {
            //table.search(this.value).draw(false);
            var str = $(this).val();
            var direction;
            var sortColumn;

            if (str.length >= 1) {
                $(this).css('visibility', 'visible');
                $(TM.settings.foundId).css('display', 'block');
                $(TM.settings.foundId).siblings('.remove-icon').show();
                table.search(str).draw(false);
            }
            else {
                $(TM.settings.foundId).siblings('.remove-icon').hide();
                $(TM.settings.searchId).removeAttr('style');
                $(TM.settings.foundId).css('display', 'none');
                table.search(str).draw(false);
            }
        });
        //on clear field click
        $(TM.settings.removeIconId).click(function () {
            table.search("").draw(false);
            $('#found').hide();
        });

        $(TM.settings.addNotes).on('click', function () {

            var textNote = $(TM.settings.textNote).val();
            if (textNote.length == 0) {
                return;
            }

            var isVendor = TM.settings.isVendor == "True";
            var isCustomer = TM.settings.isCustomer == "True";

            if (isVendor) {
                var req = { vendorId: TM.settings.vendorID, notes: textNote };
            }
            else if (isCustomer) {
                var req = { customerId: TM.settings.customerID, notes: textNote };
            }
            else {
                var req = { applicationID: TM.settings.appID, notes: textNote };
            }

            $.ajax({
                type: 'POST',
                url: TM.settings.addNotesHandler,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    $(TM.settings.textNote).val('');
                    table.draw(false);
                },
                failure: function (data) {
                }
            });
        });

    };//end create table

    $('#panelInternalNotes').on('shown.bs.collapse', function () {
        //console.log('chevron open');
    });
    $('#panelInternalNotes').on('hidden.bs.collapse', function () {
        $('#tblInternalNoteList').find("[id^='notesNbr']").each(function () { $(this).webuiPopover('hide') });//close opened popover
    });

    //get the width of string
    function getTextWidth(text, font) {
        // re-use canvas object for better performance
        var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
        var context = canvas.getContext("2d");
        context.font = font;
        var metrics = context.measureText(text);
        return Math.round(metrics.width);
    }
    $("#search").keydown(function (evt) {
        if (evt.keyCode == 13) {
            evt.preventDefault();
            return false;
        }
    });     

}(jQuery));