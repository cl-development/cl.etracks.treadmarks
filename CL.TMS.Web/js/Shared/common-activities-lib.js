﻿"use strict";

var commonActivitiesLib = angular.module("commonActivitiesLib", ['ui.bootstrap', 'datatables', 'datatables.scroller']);
var RegistrationActivitiesApp = angular.module("RegistrationActivitiesApp", ["ui.bootstrap", "commonLib", "commonActivitiesLib"]);//used for all applications activity tracking
//directives section
commonActivitiesLib.directive('commonActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "activityService", function claimActivityPanel($window, DTOptionsBuilder, DTColumnBuilder, $timeout, activityService) {
    return {

        scope: {
            snpUrl: '@',
            activityArea: '@',
            activityType: '@',
        },

        restrict: 'E',

        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));                       
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));                       
                    }
                }
                scope.$digest();
            });

            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '600',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }       
            }
        },

        templateUrl: 'claimActivityPanel.html',

        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {
            $scope.dtInstance = {};

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width','15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date +' '+ date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.snpUrl,
                global: false,
                type: "GET",//"POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);                  
                }
            }).withOption('processing', true)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order',[0,'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));                          
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    var direction = settings.aLastSort[0].dir;
                    var searchValue = $('#searchActivity').val();
                    var claimId = (Global.CommonActivity) ? Global.CommonActivity.ObjectId : 0;
                    var url = "/System/Common/GetAllActivitiesExport?searchText=CCC&sortColumn=AAA&sortDirection=BBB&claimId=DDD";
                    url = (Global.CommonActivity) ? Global.CommonActivity.ExportActivityUrl : '';
                    url = url.replace('AAA', sortColumn).replace('BBB', direction).replace('CCC', searchValue).replace('DDD',claimId);
                    $('#exportActivities').attr('href', url);
                    $scope.$apply();
                });

            $scope.url = $scope.snpUrl; //keep original snpUrl
            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false
            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;                
                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };              
                var searchValue = $('#searchActivity').val();
                if (searchValue) { //change original snpUrl by appending searchValue, because searchValue cannot be passed to backend by DataGridModel param if "More" button is clicked (maybe a bug of angular datatables)
                    var url = $scope.snpUrl;
                    url = url + "&temp=" + searchValue;
                    $scope.snpUrl = url;
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }            
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 190)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc : "data",
                                    url: $scope.snpUrl,
                                    global:false, // this makes sure ajaxStart is not triggered
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };                   
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);

                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';                   
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //Auto reload datatable after new activity generated
            $rootScope.$on('TOTAL_ACTIVITY', function (e, data) {
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            });

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    $scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.snpUrl,
                                   global:false,
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }                 
            }

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '600',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }

            function dateTimeConvert(data) {
                if (data === null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches === null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                second = second < 10 ? '0' + second : second;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
                //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

                return {
                    date: d,
                    time: curr_time
                }
            }         
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace


//OTSTM2-424
commonActivitiesLib.controller("activityController", ["$scope", "activityService", function ($scope, activityService) {
    $scope.hasNew =false;
    var data = { claimId: Global.CommonActivity.ObjectId };
    console.log(Global.CommonActivity.ExportActivityUrl);
    activityService.totalActivity(data).then(function (dataResult) {
        $scope.totalActivity = dataResult;
    });

    $scope.increasingTotalActivity = function (claimId) {
        var data = { claimId: claimId };
        activityService.totalActivity(data).then(function (dataResult) {
            $scope.totalActivity = dataResult;
            $scope.$parent.$emit('TOTAL_ACTIVITY', dataResult);
        });        
        $scope.$apply();
    }
}]);

//service section
commonActivitiesLib.factory('activityService', ["$http", function ($http) {

    var factory = {};
    //OTSTM2-424
    factory.totalActivity = function (data) {
        var req = { claimId: data.claimId };
        var submitVal = {
            url: "/System/Common/TotalActivity",
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    return factory;
}]);

String.prototype.strongMe = function () {
    if (this) {
        return ' &lt;strong&gt;' + this + '&lt;/strong&gt; ';
    } else {
        return this; //avoid null
    }
};

//***************** js of  participant build-activity-log functions************
//*****************************************************************************
function BuildActivityLogContent(thisObj) {
    //initial actObj
    var actObj = {
        contactPerson: GlobalSearch.UserName(),
        active: "changed",
        fieldName: $(thisObj).data("fieldname") ? $(thisObj).data("fieldname") : "",
        defaultValue: thisObj.defaultValue ? thisObj.defaultValue : "empty",
        thisvalue: thisObj.value ? thisObj.value.trim() : "empty",
        panelName: $(thisObj.closest('form > div.panel.panel-default')).find('div.panel-heading > h4 > a').text().trim(),
        sectionName: $(thisObj).data("sectionname") ? $(thisObj).data("sectionname") : "",
        actLogStr: function () { return "" },
        hasAct: false
    }
    actObj = buildActContent(actObj, thisObj);
    actObj.hasAct = (actObj.panelName+actObj.fieldName+actObj.sectionName).length>0 ? true : false;
    Global.CommonActivity.ActivitiesLog = actObj; 
    thisObj.defaultValue = thisObj.value;
    if (thisObj.type == 'select-one') {
        thisObj.defaultValue = thisObj.options[thisObj.selectedIndex].text;
    }
    if (thisObj.type == 'radio') {
        $(thisObj.closest('tbody > tr')).attr('data-reportval', thisObj.value);
    }
    return actObj.panelName + '_' + actObj.fieldName + '_' + actObj.active + '_' + actObj.contactPerson + "_" + actObj.sectionName;
}

function buildActContent(actObj, thisObj) {
    switch (actObj.panelName) {
        case 'Business Location':
            actObj = businessLocation(actObj, thisObj);
            break;
        case 'Contact Information':
            actObj = contactInformation(actObj, thisObj);
            break;
        case 'Tire Details':
            actObj = tireDetails(actObj, thisObj);
            break;
        case 'Collector Details':
            actObj = collectorDetails(actObj, thisObj);
            break;
        case 'Steward Details':
            actObj = stewardDetails(actObj, thisObj);
            break;
        case 'Supporting Documents':
            actObj = supportDocuments(actObj, thisObj);
            break;
        case 'Terms and Conditions':
            actObj = termsConditions(actObj, thisObj);
            break;
       
        default:
            actObj.actLogStr += actObj.contactPerson
                 + actObj.active.strongMe() + " the "
                 + actObj.fieldName.strongMe() + " in the "
                 + actObj.panelName.strongMe() + " panel from "
                 + actObj.defaultValue.strongMe() + ' to '
                 + actObj.thisvalue.strongMe();
            break;
    }
    return actObj;
}
//specific panel action
function businessLocation(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            actObj.active = "selected";
            actObj.thisvalue = thisObj.options[thisObj.selectedIndex].text;
            if (actObj.defaultValue == 'Select Country') {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " "
                    + actObj.thisvalue.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel"
                    + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                            + actObj.active.strongMe() + " the "
                            + actObj.fieldName.strongMe() + " in the "
                            + actObj.panelName.strongMe() + " panel from "
                            + actObj.defaultValue.strongMe() + ' to '
                            + actObj.thisvalue.strongMe()
                            + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            }
           
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe()
                        + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
            }
            break;
        case 'text':
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for the " + actObj.sectionName.strongMe() : "");
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe()
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            }
            break;
    }   
    return actObj;
}
function contactInformation(actObj, thisObj) {
    actObj.sectionName = $(thisObj.closest('form div.parent')).data("sectionname");///$(thisObj.closest('form div.parent[class^="template"]')).data('fieldname');
    actObj.sectionName = actObj.sectionName ? actObj.sectionName : "";
    if (!thisObj.type && $(thisObj).data("buttontype") == "removed") {
        thisObj.type = "button";
    }
    switch (thisObj.type) {
        case 'select-one':
            actObj.active = "selected";
            actObj.thisvalue = thisObj.options[thisObj.selectedIndex].text;
            actObj.defaultValue = actObj.defaultValue == "empty" ? "Select Country" : actObj.defaultValue;
            if (actObj.defaultValue == 'Select Country') {
                    actObj.actLogStr = function () {
                        return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel for "
                        + actObj.sectionName.strongMe();
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel for "
                    + actObj.sectionName.strongMe()
                    + " from "
                    + actObj.defaultValue.strongMe() + ' to '
                    + actObj.thisvalue.strongMe();
                }
            }
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                + actObj.active.strongMe() + " the "
                + actObj.fieldName.strongMe()
                + " in the "
                + actObj.panelName.strongMe() + " panel"
                + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
            }
            break;
        case 'radio':
            break;
        case 'text':
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " "
                    + actObj.defaultValue.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel"
                    + (actObj.sectionName.length > 0 ? " for the " + actObj.sectionName.strongMe() : "");
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel "
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe(): "")
                        + ' from '
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
        case 'button':           
            actObj.active = $(thisObj).data("buttontype");
            if (actObj.active == "added") {
                actObj.sectionName = $($(".ctemp div.parent")[$(".ctemp div.parent").length - 1]).data("sectionname");
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                         + actObj.active.strongMe()
                         + actObj.sectionName.strongMe() + " in"
                         + actObj.panelName.strongMe() + " panel";
                }
            } else if (actObj.active == "removed") {
                actObj.sectionName += " " + $(thisObj.closest('form div.parent')).find("input.form-control[id*='FirstName']").val() + " "
                                    + $(thisObj.closest('form div.parent')).find("input.form-control[id*='LastName']").val();
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                         + actObj.active.strongMe() + " the "
                         + actObj.sectionName.strongMe() + " from"
                         + actObj.panelName.strongMe() + " panel";
                }
            }
            break;
    }
    return actObj;
}
function tireDetails(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            break;
        case 'checkbox':
           // actObj.active = thisObj.checked ? " added" : " removed";
            // actObj.contactPerson = GlobalSearch.IsStaff() == "True" ? "Staff" : "Applicant";
            actObj.active = thisObj.checked ? " checked" : " unchecked";
            actObj.sectionName = $(thisObj.closest("div.sectionname")).data("sectionname")
            actObj.sectionName = actObj.sectionName ? actObj.sectionName : "";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe()
                    + (actObj.fieldName.length > 0 ? " a " : " the ")
                    + actObj.sectionName.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel "
                    + (actObj.fieldName.length > 0 ? ": " + actObj.fieldName.strongMe() : "");
            }
            break;
        case 'radio':
            break;
        case 'text':
            actObj.sectionName = $(thisObj.closest("div.sectionname")).data("sectionname")
            actObj.sectionName = actObj.sectionName ? actObj.sectionName : "";
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for the " + actObj.sectionName.strongMe() : "");
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe()
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            }
            break;
    }
    return actObj;
}
function collectorDetails(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            actObj.active = "selected";
            actObj.thisvalue = thisObj.options[thisObj.selectedIndex].text;
            if (actObj.defaultValue == 'Select Activity Type') {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";
           // actObj.contactPerson = GlobalSearch.IsStaff() == "True" ? "Staff" : "Applicant";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                      + actObj.active.strongMe() + " the "
                      + actObj.fieldName.strongMe()
                      + " in the "
                      + actObj.panelName.strongMe() + " panel"
                      + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
            }
            break;
        case 'radio':
            actObj.active = "selected";           
            var temp = $(thisObj.closest("div.form-group")).attr('data-reportval');
            actObj.defaultValue = temp == "True" ? "Yes" : "No";
            $(thisObj.closest("div.form-group")).attr('data-reportval', actObj.thisvalue);
            actObj.thisvalue = actObj.thisvalue == "True" ? "Yes" : "No";         
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in "
                    + actObj.panelName.strongMe() + " panel from "
                    + actObj.defaultValue.strongMe() + ' to '
                    + actObj.thisvalue.strongMe();
            }
            break;
        case 'text':
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
    }
    return actObj;
}
function stewardDetails(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            actObj.active = "selected";
            actObj.thisvalue = thisObj.options[thisObj.selectedIndex].text;
            if (actObj.defaultValue == 'Select Activity Type') {
                //actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";           
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel"
            }
            break;
        case 'radio':
            actObj.active = "selected";           
            switch (actObj.fieldName) {
                case "Are you currently registered with RPRA?":
                    var temp = $(thisObj.closest("div.form-group")).attr('data-reportval');
                    actObj.defaultValue = temp == "True" ? "Yes" : "No";
                    $(thisObj.closest("div.form-group")).attr('data-reportval', actObj.thisvalue);
                    actObj.thisvalue = actObj.thisvalue == "True" ? "Yes": "No";
                    break;
                case "Please select your Steward Type." :
                    switch (actObj.thisvalue) {
                        case "1":
                            var temp = $(thisObj.closest("div.form-group")).attr('data-reportval');
                            actObj.defaultValue = temp == "1" ? "Original Equipment Manufacturer (OEM)" : (temp == "2" ? "Tire Manufacturer/Brand Owner" : "First Importer");
                            $(thisObj.closest("div.form-group")).attr('data-reportval', actObj.thisvalue);
                            actObj.thisvalue = "Original Equipment Manufacturer (OEM)";
                            actObj.fieldName = "Please select your Steward Type";
                            break;
                        case "2":
                            var temp = $(thisObj.closest("div.form-group")).attr('data-reportval');
                            actObj.defaultValue = temp == "1" ? "Original Equipment Manufacturer (OEM)": (temp == "2" ? "Tire Manufacturer/Brand Owner": "First Importer");
                            $(thisObj.closest("div.form-group")).attr('data-reportval', actObj.thisvalue);
                            actObj.thisvalue = "Tire Manufacturer/Brand Owner";
                            actObj.fieldName = "Please select your Steward Type";
                            break;
                        case "3":
                            var temp = $(thisObj.closest("div.form-group")).attr('data-reportval');
                            actObj.defaultValue = temp == "1" ? "Original Equipment Manufacturer (OEM)" : (temp == "2" ? "Tire Manufacturer/Brand Owner" : "First Importer");
                            $(thisObj.closest("div.form-group")).attr('data-reportval', actObj.thisvalue);
                            actObj.thisvalue = "First Importer";
                            actObj.fieldName = "Please select your Steward Type";
                            break;
                    }
            }
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in "
                    + actObj.panelName.strongMe() + " panel from "
                    + actObj.defaultValue.strongMe() + ' to '
                    + actObj.thisvalue.strongMe();
                }            
            break;
        case 'text':
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
        case 'textarea':
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel";
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                }
            }
            break;
    }
    return actObj;
}
function supportDocuments(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel";
            }
            break;
        case 'radio':
            actObj.active = "selected";
            actObj.fieldName = thisObj.closest('tbody > tr').innerText.trim();
            actObj.thisvalue = actObj.thisvalue.substring(6, actObj.thisvalue.length).toUpperCase();
            var temp = $(thisObj.closest('tbody > tr')).attr('data-reportval');
            actObj.defaultValue = temp.substring(6, temp.length).toUpperCase();
            actObj.actLogStr = function () {
                return actObj.contactPerson
                      + actObj.active.strongMe() + " the "
                      + actObj.fieldName.strongMe() + " from "
                      + actObj.defaultValue.strongMe() + ' to '
                      + actObj.thisvalue.strongMe()
                      + " in "
                      + actObj.panelName.strongMe() + " panel.";
            }
            break;
        case 'text':
            actObj.actLogStr = function () {
                return actObj.contactPerson
                     + actObj.active.strongMe() + " the "
                     + actObj.fieldName.strongMe() + " in the "
                     + actObj.panelName.strongMe() + " panel from "
                     + actObj.defaultValue.strongMe() + ' to '
                     + actObj.thisvalue.strongMe();
            }
            break;
    }
    return actObj;
}
function termsConditions(actObj, thisObj) {
    switch (thisObj.type) {
        case 'select-one':
            break;
        case 'checkbox':
            actObj.active = thisObj.checked ? " checked" : " unchecked";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                    + actObj.active.strongMe() + " the "
                    + actObj.fieldName.strongMe() + " in the "
                    + actObj.panelName.strongMe() + " panel";
            }
            break;
        case 'radio':
            actObj.fieldName = thisObj.closest('tbody > tr').innerText.trim();
            actObj.thisvalue = thisObj.parentElement.textContent;
            actObj.defaultValue = $(thisObj.closest('tbody > tr')).attr('data-reportval');
            actObj.active = "selected";
            actObj.actLogStr = function () {
                return actObj.contactPerson
                      + actObj.active.strongMe() + " the "
                      + actObj.fieldName.strongMe() + " in the "
                      + actObj.panelName.strongMe() + " panel from "
                      + actObj.defaultValue.strongMe() + ' to '
                      + actObj.thisvalue.strongMe();
            }
            break;
        case 'text':           
            if (actObj.thisvalue == "empty") {
                actObj.active = "deleted";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.defaultValue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for the " + actObj.sectionName.strongMe() : "");
                }
            } else if (actObj.defaultValue == "empty") {
                actObj.active = "added";
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " "
                        + actObj.thisvalue.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel"
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "");
                }
            } else {
                actObj.actLogStr = function () {
                    return actObj.contactPerson
                        + actObj.active.strongMe() + " the "
                        + actObj.fieldName.strongMe() + " in the "
                        + actObj.panelName.strongMe() + " panel "
                        + (actObj.sectionName.length > 0 ? " for " + actObj.sectionName.strongMe() : "")
                        + " from "
                        + actObj.defaultValue.strongMe() + ' to '
                        + actObj.thisvalue.strongMe();
                       
                }
            }
            break;
    }
    return actObj;
}

function addActivityLog(activityArea, logArray) {
    var logContent = "",isFunction=false;
    isFunction=Global.CommonActivity.ActivitiesLog && typeof Global.CommonActivity.ActivitiesLog.actLogStr === "function";
    if (isFunction) {
        logContent = Global.CommonActivity.ActivitiesLog.actLogStr();
    } else {
        logContent = Global.CommonActivity.ActivitiesLog;
    }
    if (!logArray) {
        $.ajax({
            url: '/System/Common/AddCommonActivity',
            dataType: 'json',
            type: 'POST',
            data: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: activityArea, logContent: logContent, ActivityType: Global.CommonActivity.ActivityType },
            success: function (result) {
                if (result) {
                    switch (result.status) {
                        case "Valid Data"://success, reload activity panel
                            angular.element(document.getElementById("panelClaimActivity")).scope().dtInstance.DataTable.draw(false);
                            if (isFunction) {
                                Global.CommonActivity.ActivitiesLog.hasAct = false;//avoid duplicate log
                            }
                            break;
                        default:
                            break;
                    }
                }
            },
            error: function (result) {
                console.log('error:', result);
            }
        });
    } else {
        $.ajax({
            url: '/System/Common/AddCommonActivityBatch',
            dataType: 'json',
            type: 'POST',
            data: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: activityArea, logContent: logContent, logBatch: logArray, ActivityType: Global.CommonActivity.ActivityType },
            success: function (result) {
                if (result) {
                    switch (result.status) {
                        case "Valid Data"://success, reload activity panel
                            angular.element(document.getElementById("panelClaimActivity")).scope().dtInstance.DataTable.draw(false);
                            if (isFunction) {
                                Global.CommonActivity.ActivitiesLog.hasAct = false;//avoid duplicate log
                            }
                            break;
                        default:
                            break;
                    }
                }
            },
            error: function (result) {
                console.log('error:', result);
            }
        });
    }
}
//************************** end of participant build-activity-log functions
//**************************************************************************