﻿var ApplicationCommon = function () {

    //*Sort Yard Handler*//
    var sortYard = function () {

        //Constants
        var SortYardTemplateSelectorPrefix = "#sortYardTemplate";
        var SortYardForm = "#fmSortYardDetails";
        var SortYardCountInput = "#SortYardCount";

        //Variables
        var _sortYardTemplateBase = $('.sortyardtemp').val();
        var _sortYardTemplateCount = $('#sorYardCount').val();


        //Methods
        var resetSortYardCount = function () {
            $(SortYardCountInput).val('0');
            return false;
        };

        var getYardCount = function () {
            var count = $(SortYardCountInput).val();
            return count;
        }

        var setYardCount = function (count) {
            resetSortYardCount();
            $(SortYardCountInput).val(count);
            return false;
        }

        var initialize = function () {
            for (var i = 0; i < _sortYardTemplateCount; i++) {
                var sortYardViewModelTemplate = $(SortYardTemplateSelectorPrefix + i).val();
                $(sortYardViewModelTemplate).appendTo(SortYardForm);

                if (i == 0) {
                    $('.sort-yard-address-disabled').attr('disabled', 'disabled');
                }

                setYardCount(_sortYardTemplateCount);
            }
            return false;
        };

        var addRow = function () {
            var yardCount = getCount();
            var newYardCount = yardCount++;
            var sortYardtempIncremental = "<div class='ctemp'><div class='sortYardtemplate" + newYardCount + "'>" + _sortYardTemplateBase + '</div></div><hr>';

            setYardCount(newYardCount);

            //set the datamaxstorage of the new added sortyard to 0 so its not nan 
            $('.sortYardtemplate' + currAddCount + ' input[data-maxstorage]').attr('data-maxstorage', 0);
            $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < yardCount; i++) {
                $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {

                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                    if ((typeof $(this).attr('id') != 'undefined')) {
                        $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
                    }
                });

                if (i == currAddCount) {
                    $('.sortYardtemplate' + i + ' input[type=text]').removeClass('sort-yard-address-disabled');
                    $('.sortYardtemplate' + i + ' select').removeClass('sort-yard-address-disabled');
                }
            }
            return false;
        }

        var setTotalSortYarStorageCapacity = function () {
            var inputs = $("input[data-maxstorage]");
            var maxStorageCap = 0;
            inputs.each(function () {
                if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
                    maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
                }
            });

            if (!isNaN(parseFloat(maxStorageCap))) {
                $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
            }

        }

        var bindSortYardSelect = function () {
            $(SortYardTemplateSelectorPrefix + currAddCount + " select").each(function () {
                if (currAddCount > 0)
                    $(this).children().removeAttr('selected');
            });
        }

        var renameSortYardLabel = function () {
            var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
            for (var j = 0; j < label.length; j++) {
                var num = 1;
                $(label[j]).each(function () {
                    var newLabel = $(this).text().replace(/\d+/, num++);
                    $(this).text(newLabel);
                });
            }
        }

        var copyBusinessLocationAddress = function () {
            $('.sortyard-address').on('focusout', function () {
                var attr = $(this).attr('data-sortyard');
                if (typeof attr !== typeof undefined && attr) {
                    if ($("input[name*='" + attr + "']").length > 0)
                        $("input[name*='" + attr + "']").val($(this).val());
                    if ($("select[name*='" + attr + "']").length > 0) {
                        $("select[name*='" + attr + "']").val($(this).val());
                    }
                }
            });
        };

        return
        {
            getCount: getCount;
            resetSortYardCount: resetSortYardCount;
            sortYardTemplete: sortYardTemplete;
            initialize: initialize;
            addRow: addRow;
            setTotalSortYarStorageCapacity: setTotalSortYarStorageCapacity;
            bindSortYardSelect: bindSortYardSelect;
            renameSortYardLabel: renameSortYardLabel;
            copyBusinessLocationAddress: copyBusinessLocationAddress;
        };
    };

    var populateProcessorDropDownList = function (selectValue, toTrigger) {
        var processorDropDownList = $('.processorDropDownList');
        processorDropDownList.empty();
        processorDropDownList.append('<option>Select Processor</option>');

        $.ajax({
            url: '/default/GetProcessorList',
            method: 'GET',
            dataType: 'json',
            success: function (result) {
                $(result).each(function (index, data) {

                    var option = '';
                    if (data.ID != selectValue) {
                        option = "<option value='" + data.ID + "'>" + data.Text + "</option>";
                    }
                    else {
                        option = "<option selected value='" + data.ID + "'>" + data.Text + "</option>";
                    }

                    processorDropDownList.append(option);
                });
                if (toTrigger) {
                    processorDropDownList.trigger('change');
                }                
            },
            fail: function (jqXHR, textStatus, error) {
                console.log(error);
            }
        });
    };

    var setContactAddressCountryDropDown = function() {
        $('INPUT[id^="contactAddressCountry"]').each(function () {
            var selection = $(this).val();
            //Need to srite this stupid code to handle selection based on dynamic ids
            switch (selection.toLowerCase())
            {
                case "canada":
                    selection = "Canada";
                    break;
                case "usa":
                    selection = "USA";
                    break;
                case "other":
                    selection = "Other";
                    break;
            }
            if (selection != "") {
                var contactAddressCountrySelectName = $(this).attr("name");                
                $('SELECT[name="' + contactAddressCountrySelectName + '"]').val(selection);
            }
        });
    }

    var SubmitButtonHandler = function SubmitButtonHandler() {
        var errorStatusIcons = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

        $(".greenCheckmark").each(function () {
            if ($(this).attr("src") == errorStatusIcons[1]) {
                $('#disabledsubmitApplicationBtn').hide();
                $('#submitApplicationBtn').show();
            }
            else {
                $('#disabledsubmitApplicationBtn').show();
                $('#submitApplicationBtn').hide();

                return false;
            }
        });
    }

    return {
        sortYard: sortYard,
        populateProcessorDropDownList: populateProcessorDropDownList,
        SubmitButtonHandler: SubmitButtonHandler,
        setContactAddressCountryDropDown: setContactAddressCountryDropDown 
    };
}();

var StaffCommonValidations = {

    BusinessLocation: function () {
        //reviewCheckBox validation OTSTM2-83    
        $('#fmBusinessLocation').panelReviewCheckBoxValidate({
            flagId: '#flagBusinessLocation',
            rules: {
                'BusinessLocation.BusinessName': {
                    required: true,
                    isChecked: $('#cbBusinessName').is(':checked')
                },
                'BusinessLocation.OperatingName': {
                    required: true,
                    isChecked: $('#cbOperatingName').is(':checked')
                },

                'BusinessLocation.BusinessAddress.AddressLine1': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbAddress1').is(':checked')
                },
                'BusinessLocation.BusinessAddress.AddressLine2': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbAddress2').is(':checked')
                },

                'BusinessLocation.BusinessAddress.City': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbCity').is(':checked')
                },
                'BusinessLocation.BusinessAddress.Postal': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbPostalCode').is(':checked')
                },
                'BusinessLocation.BusinessAddress.Province': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbProvince').is(':checked')
                },

                'BusinessLocation.BusinessAddress.Country': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbCountry').is(':checked')
                },

                'BusinessLocation.Phone': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbPhone').is(':checked')
                },

                'BusinessLocation.Extension': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbExt').is(':checked')
                },

                'BusinessLocation.Email': {
                    required: true,
                    isChecked: $('#BusinessLocation_cbEmail').is(':checked')
                },

                //Mailing Address Same As
                'BusinessLocation.MailingAddress.AddressLine1': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_Address1').is(':checked')
                },

                'BusinessLocation.MailingAddress.AddressLine2': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_Address2').is(':checked')
                },

                'BusinessLocation.MailingAddress.City': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_City').is(':checked')
                },

                'BusinessLocation.MailingAddress.Postal': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_PostalCode').is(':checked')
                },

                'BusinessLocation.MailingAddress.Province': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_Province').is(':checked')
                },

                'BusinessLocation.MailingAddress.Country': {
                    required: function () {
                        return !$('#MailingAddressSameAsBusiness').is(':checked');
                    },
                    isChecked: $('#BusinessLocation_cbMail_Country').is(':checked')
                },
            }
        });
    },

    ContactInfo: function () {
        $('#fmContactInfo').dynamicReviewChkboxValidate({
            flagId: '#flagContactInformation',
            formId: '#fmContactInfo',
            submitBtn: '#btnAddContact',
            isIgnore: true,
            //single ignore
            ignoreTrigger: '#ContactAddressSameAsBusinessAddress',
            ignoreRule: function () {
                //when ignore trigger is pressed it would execute this code block with parent template needed
                var contactAddressDifferent = $(this).children(".contactAddressDifferent");
                var allReviewChkboxesContactAddress = $(contactAddressDifferent).find(".validation-checkbox");
                var ignore = $(this).find('#ContactAddressSameAsBusinessAddress');

                if ($(ignore).is(':checked')) {
                    $(allReviewChkboxesContactAddress).each(function () {
                        $(this).addClass('ignore')
                    });
                }
                else {
                    $(allReviewChkboxesContactAddress).each(function () {
                        $(this).removeClass('ignore')
                    });
                }
            }
        });
    },

    SupportingDoc: function () {
        $('#fmSupportingDoc').panelReviewCheckBoxValidate({
            flagId: '#StaffFlagSupportingDocuments',
            rules: {
                'SupportingDocuments.RequiredDocuments': {
                    required: true,
                    isChecked: $('#cbRequiredDocuments').is(':checked')
                }
            }
        });
    },

    TermsAndConditions: function () {
        $('#fmTermsAndConditions').panelReviewCheckBoxValidate({
            flagId: '#flagTermsAndConditions',
            rules: {
                'TermsAndConditions.SigningAuthorityFirstName': {
                    required: true,
                    isChecked: $('#cbSigningAuthority').is(':checked')
                },

                'TermsAndConditions.SigningAuthorityLastName': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbSigningAuthorityLastName').is(':checked')
                },

                'TermsAndConditions.SigningAuthorityPosition': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbSigningAuthorityPosition').is(':checked')
                },

                'TermsAndConditions.FormCompletedByFirstName': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbFormCompletedByFirstName').is(':checked')
                },

                'TermsAndConditions.FormCompletedByLastName': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbFormCompletedByLastName').is(':checked')
                },

                'TermsAndConditions.FormCompletedByPhone': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbFormCompletedByPhone').is(':checked')
                },

                'TermsAndConditions.AgreementAcceptedByFullName': {
                    required: true,
                    isChecked: $('#TermsAndConditions_cbAgreementAcceptedByFullName').is(':checked')
                },
            }
        });
    }
}

!function ($) {
    "use strict";

    var Form = function (el, options) {
        this.elem = el;
        this.$element = $(el);
        this.options = options;
    };

    Form.prototype = {
        defaults: {
            greyFlagPath: '/Images/comp-register-all-status-flag-grey.png',
            greenFlagPath: '/Images/comp-register-all-status-flag-green.png'
        },

        init: function () {
            this.config = $.extend({}, this.defaults, this.options);

            this.checkPanelValidation();

            this.checkBoxListener();

            return this;
        },

        checkBoxListener: function () {
            var self = this;
            $('#' + this.elem.id + ' :input').change(function () {
                var chkBoxElem = this;
                //check if that element has data-att-chkbox attribute
                if (chkBoxElem.attributes.hasOwnProperty('data-att-chkbox')) {
                    //somehow nodeValue gives the value of the 'data-att-chkbox'
                    var ruleName = chkBoxElem.attributes['data-att-chkbox'].nodeValue;
                    var rule = self.findRule(ruleName);
                    
                    if (!$.isEmptyObject(rule)) {
                        //if so then find that element in the rules and update its IsChecked property with current checked state
                        var chckboxState = $(this).is(':checked');

                        //update the current rule
                        rule[ruleName].isChecked = chckboxState;
                    }
                }
                self.checkPanelValidation();
            })
        },        

        checkPanelValidation: function () {
            var hasUnchecked = false;

            $.each(this.config.rules, function (prop, val) {
                //check if its boolean or function
                switch (typeof this.required) {
                    case "boolean":
                        //check for any rules that satisfies required true and isChecked false
                        if (isTrueAndIsNotChecked(this)) {
                            hasUnchecked = true;
                            return;
                        }
                        break;
                    case "function":
                        //function check
                        if (isFuncTrueAndNotChecked(this)) {
                            hasUnchecked = true;
                            return;
                        }
                }                
            });

            // if there are in the unchecked list then flag as grey
            if (hasUnchecked) {
                this.updatePanelFlag(this.config.greyFlagPath);
            }
            else {
                this.updatePanelFlag(this.config.greenFlagPath);
            }
        },

        updatePanelFlag: function (flag) {
            $(this.config.flagId).attr('src', flag)
        },

        findRule: function (ruleName) {
            var rule = {};
            $.each(this.config.rules, function (prop, val) {
                if (prop === ruleName) {
                    rule[prop] = this;
                }
            });
            return rule;
        },
    }

    //Private functions
    function isTrueAndIsNotChecked(rule){
        return (rule.required && !rule.isChecked);
    }

    //only for functions
    function isFuncTrueAndNotChecked(funcRule) {
        //if not checked then required
        return (funcRule.required() && !funcRule.isChecked);
    }

    $.fn.panelReviewCheckBoxValidate = function (options) {
        return this.each(function () {
            new Form(this, options).init();
        });
    };

}(window.jQuery);

!function ($) {
    "use strict";

    var Form = function (el, options) {
        this.elem = el;
        this.$element = $(el);
        this.options = options;
    };

    Form.prototype = {
        defaults: {
            parent: '.parent',
            dynamic_chkBox: '.dynamic-review-chckbox',
            greyFlagPath: '/Images/comp-register-all-status-flag-grey.png',
            greenFlagPath: '/Images/comp-register-all-status-flag-green.png'
        },

        init: function () {
            this.settings = $.extend({}, this.defaults, this.options);

            this.formPanelListener();
            
            if (this.settings.isIgnore) {
                //listen to changes for the ignoreTrigger i.e .maxStorageCapacity
                this.ignoreEventListener();
                this.updateIgnore();
            }

            this.panelValidation();
        },
        //this event listener will listen to all review checkboxes except ignoreTrigger i.e .maxStorageCapacity
        formPanelListener: function () {
            var self = this;

            self.settings.ignoreTrigger = self.settings.ignoreTrigger || ''

            //check for review checkbox changes for the form except with ignoreTrigger i.e .maxStorageCapacity
            $(self.settings.formId).on('change', ':not(' + self.settings.ignoreTrigger + '):checkbox', function () {
                self.panelValidation();
            });

            //Add template
            $(self.settings.submitBtn).on('click', function () {
                if (self.settings.isIgnore) {
                    self.updateIgnore();
                }
                self.panelValidation();
            });
        },

        //check if panel is valid by searching all review checkboxes inside each template with 
        //.dynamic-review-chckbox class 
        //it will also skip all attributes with .ignore class
        panelValidation: function () {
            //UPDATE FLAG
            //all review checkboxes ignore excluded for each template
            var self = this;
            var isValid = true;

            //for each template
            $(this.settings.formId + ' ' + this.settings.parent).each(function () {

                var template = this.classList[0]; //get template

                //count of all review checkboxes inside that template except the .ignore class 
                var singleTemplateCount = $('.' + template + ' ' + self.settings.dynamic_chkBox + ':not(.ignore):checkbox').length;

                //count of all review checkboxes which are CHECKED ONLY except the .ignore class 
                var count = $('.' + template + ' ' + self.settings.dynamic_chkBox + ':not(.ignore):checkbox:checked').length;

                if (singleTemplateCount !== count) {
                    isValid = false;
                    return;
                }
            });

            if (isValid) {
                this.updatePanelFlag(this.settings.greenFlagPath);
            }
            else {
                this.updatePanelFlag(this.settings.greyFlagPath);
            }
        },
        //this event listener wil only listen to ignoreTrigger i.e .maxStorageCapacity
        //will call business rules to update class .ignore after changes to  ignoreTrigger
        ignoreEventListener: function () {
            var self = this;
            //any change to class with i.e .maxStorageCapacity
            $(self.settings.formId).on('change', self.settings.ignoreTrigger, function () {
                //get the parent template of the ignoreTrigger
                var parent = $(this).closest(self.settings.parent)

                //pass the parent template to the ignoreRule function
                self.settings.ignoreRule.call(parent);
                //validate the panel
                self.panelValidation();
            });
        },

        updateIgnore: function () {
            var self = this;
            //for every page load update all trigger ignore rule
            $(this.settings.formId + ' ' + this.settings.parent).each(function () {
                //pass the parent template
                self.settings.ignoreRule.call(this);
            });
        },

        updatePanelFlag: function (flag) {
            $(this.settings.flagId).attr('src', flag)
        },
    }

    $.fn.dynamicReviewChkboxValidate = function (options) {
        return this.each(function () {
            new Form(this, options).init();
        });        
    }

}(window.jQuery);
