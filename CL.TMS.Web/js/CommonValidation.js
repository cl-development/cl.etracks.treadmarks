﻿
var CommonValidation = (function () {

	var _statusImageArray = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];
	
    var initializeStandardFormValidations = function () {

		
        // * Restrict Input To Decimal Number Only *//
        $('body').on('keypress', 'INPUT.only-decimalnumber', function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        //* Restrict Input To Numbers Only *//
        $("body .only-numbers").keydown(function (event) {
            // Allow only backspace and delete
            if (event.keyCode == 46 || event.keyCode == 8) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.keyCode != 9 && event.keyCode != 16 && !(event.keyCode >= 96 && event.keyCode <= 105) && (event.keyCode < 48 || event.keyCode > 57)) {
                    event.preventDefault();
                }
            }
        });
        //$('body').on('keypress', 'INPUT.only-numbers', function (event) {
        //    $(this).val($(this).val().replace(/[^\d].+/, ""));
        //    if ((event.which < 48 || event.which > 57)) {
        //        event.preventDefault();
        //    }
        //});

        //* Restrict Input To Decimal Numbers Only - 9 numbers 2 decimal places  *//
        $('body').on('keypress', 'INPUT.only-decimalnumber-th-2dp', function (event) {
            var value = $(this).val().replace(/[^0-9\.]/g, '');
            $(this).val(value);
            if ((event.which != 46 || value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            else {
                if (event.which != 46 && value.indexOf('.') == -1 && value.length >= 9) {
                    event.preventDefault();
                }
                else {
                    var tempVal = value.split('.', 2);

                    if (event.which != 46 && tempVal[1] != null && tempVal[1].length >= 2) {
                        event.preventDefault();
                    }
                    else if (event.which != 46 && (tempVal[0] != null && tempVal[0].length >= 9 && tempVal[1] != null && tempVal[1].length >= 2)) {
                        event.preventDefault();
                    }
                }
            }

        });
        //* Restrict Input To Decimal Numbers Only - 9 numbers 4 decimal places  *//
        $('body').on('keypress', 'INPUT.only-decimalnumber-th-4dp', function (event) {			
            var value = $(this).val().replace(/[^0-9\.]/g, '');
            $(this).val(value);
            if ((event.which != 46 || value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            else {
                if (event.which != 46 && value.indexOf('.') == -1 && value.length >= 9) {
                    event.preventDefault();
                }
                else {
                    var tempVal = value.split('.', 2);

                    if (event.which != 46 && tempVal[1] != null && tempVal[1].length >= 4) {
                        event.preventDefault();
                    }
                    else if (event.which != 46 && (tempVal[0] != null && tempVal[0].length >= 9 && tempVal[1] != null && tempVal[1].length >= 4)) {
                        event.preventDefault();
                    }
                }

            }
        });

        $('body').on('keypress', 'INPUT.only-decimalnumber-th-4dp-rpm', function (event) {

            if (event.keyCode == 8 && event.keyCode == 9 && event.keyCode == 16 && (event.keyCode >= 96 && event.keyCode <= 105)) {
                //allow other keys
            } else {
                if (event.keyCode != 8) {
                    var value = $(this).val().replace(/[^0-9\.]/g, '');
                    $(this).val(value);
                    if ((event.which != 46 || value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
                    else {
                        if (event.which != 46 && value.indexOf('.') == -1 && value.length >= 9) {
                            event.preventDefault();
                        }
                        else {
                            var tempVal = value.split('.', 2);

                            if (event.which != 46 && tempVal[1] != null && tempVal[1].length >= 4) {
                                event.preventDefault();
                            }
                            else if (event.which != 46 && (tempVal[0] != null && tempVal[0].length >= 9 && tempVal[1] != null && tempVal[1].length >= 4)) {
                                event.preventDefault();
                            }
                        }

                    }
                }
            }

        });

        return false;
    };



	function getStatusImages(){
		return _statusImageArray;
	}
	
    return {
        initializeStandardFormValidations: initializeStandardFormValidations,
		getStatusImages: getStatusImages
    };

})();