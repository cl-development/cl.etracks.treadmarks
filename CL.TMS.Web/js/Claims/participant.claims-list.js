﻿(function ($) {

    $.fn.createDataTable = function (options) {
		TM = {};
        TM.settings = $.extend({
            ajaxHandler: '',
			viewMoreId: '',
			totalRecordsId: '',
			searchId: '',
			exportId: '',
			exportUrl: '',
			foundId: '',
			removeIconId : '',
            scrollY: 300,
            scrollX: true,
			serverSide: true,
			rowUrl: '',
			ColumnId : 0,
			pageSize: 5,
        }, options);

        var table = this.DataTable({
            serverSide: TM.settings.serverSide,
            ajax: TM.settings.ajaxHandler,
            scrollY: TM.settings.scrollY,
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            scrollCollapse: true,
            createdRow: function (row, data, index) {
				$(row).addClass('cursor-pointer');
				var claimId = data[TM.settings.ID];
				$(row).on('click', function(){
				    window.location.href = TM.settings.rowUrl + claimId;
				});	
            },
            processing: false,
            //"paging" : false,
            order: [[1, "desc"]],
            searching: true,
            "dom": "rtiS",
            info: false,
            deferRender: true,
            "scroller": {
                displayBuffer: 100,
                rowHeight: 70,
                serverWait: 100,
                loadingIndicator: false
            },
            columns: [
						{
						    name: "ClaimId",
						    data: null,
						    className: 'display-none',
						    render: function (data, type, full, meta) {
						        return "<span>" + data.ID + "</span>";
						    }
						},			
						{
						    name: "PeriodName",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.PeriodName + "</span>";
						    }
						},
						{
						    name: "Status",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.Status + "</span>";
						    }
						},
						{
						    name: "Amount",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.Amount.toFixed(2) + "</span>";
						    }
						},			
						{
						    name: "SubmittedDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.SubmittedDate != null && data.SubmittedDate !== undefined) {
						            return dateTimeConvert(data.SubmittedDate);
						        } else {
						            return '';
						        }
						    }
						},
						{
						    name: "PaymentDue",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.PaymentDue != null && data.PaymentDue !== undefined) {
						            return dateTimeConvert(data.PaymentDue);
						        } else {
						            return '';
						        }
						    }
						},
						{
						    name: "EftNumber",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.EftNumber + "</span>";
						    }
						},			
						{
						    name: "PaymentDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.PaymentDate != null && data.PaymentDate !== undefined) {
						            return dateTimeConvert(data.PaymentDate);
						        } else {
						            return '';
						        }
						    }
						},
						{
						    name: "AssignedDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.AssignedDate != null && data.AssignedDate !== undefined) {
						            return dateTimeConvert(data.AssignedDate);
						        } else {
						            return '';
						        }
						    }

						},
						{
						    name: "SubmittedBy",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.SubmittedBy + "</span>";
						    }
						}	

            ],
            initComplete: function (settings, json) { 
				$('.dataTables_scrollBody').css('overflow-y', 'hidden');			
			},
            drawCallback: function (settings) { 
				var direction = settings.aaSorting[0][1];
				$('.sort').html('<i class="fa fa-sort"></i>');
				(direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
				$(TM.settings.foundId).html('Found ' + settings.fnRecordsDisplay());
				(settings.fnDisplayEnd() >= TM.settings.pageSize) ? $(TM.settings.viewMoreId).css('visibility', 'visible') : $(TM.settings.viewMoreId).css('visibility', 'hidden');

				var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
				var searchValue = $(TM.settings.searchId).val();
				var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
				$(TM.settings.exportId).attr('href', url);
			},
            footerCallback: function (row, data, start, end, display) { 
			
			}
        });
		
    $(TM.settings.viewMoreId).on('click', function () {	
        $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });		
		
    $(TM.settings.searchId).on('keyup', function () {
        var str = $(this).val();
        var direction;
        var sortColumn;

        if (str.length >= 1) {
            $(this).css('visibility', 'visible');
            $(TM.settings.foundId).css('display', 'block');
            $(TM.settings.foundId).siblings('.remove-icon').show();
            table.search(str).draw(false);
        }
        else {
            $(TM.settings.foundId).siblings('.remove-icon').hide();
            $(TM.settings.searchId).removeAttr('style');
            $(TM.settings.foundId).css('display', 'none');
            table.search(str).draw(false);
        }
    });		
		
    $(TM.settings.removeIconId).click(function (n) {
        $(this).siblings("input").val("");
        $(this).hide();
        $(TM.settings.foundId).css('display', 'none');
        //$(TM.settings.searchId).trigger('keyup');
        table.search("").draw(false);
    });		
		
    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    }
		
    };//end create table

}(jQuery));


