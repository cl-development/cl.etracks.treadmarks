﻿"use strict";

var commonClaimLib = angular.module("commonClaimLib", ['ui.bootstrap', 'commonLib', 'datatables', 'datatables.scroller']);

//directives section
commonClaimLib.directive('claimWorkflow', ['claimService', '$uibModal', '$window', '$http', function (claimService, $uibModal, $window, $http) {
    return {
        restrict: 'E',
        scope: {
            workFlow: '@',
            claimStatusModel: '=',
            vendorType: '@',
            //OTSTM2-488
            totalClosingOnRoad: '=',
            totalClosingOffRoad: '=',
            totalClosingTotal: '=',
            //OTSTM2-663,664
            closingInventory: "="

        },
        templateUrl: 'claimWorkflow.html',
        controller: function ($scope) {
            var settings = {
                trigger: 'hover',
                //title: 'send claim back',
                multi: true,
                closeable: true,
                style: 'center',
                delay: { show: 300, hide: 500 },
                padding: true,
                placement: 'top-left',
            };

            claimService.getClaimWorkflowViewModel().then(function (data) {
                $scope.claimWorkflowModel = data;//typeof data: ClaimWorkflowViewModel
                var popoverSettings = { content: $scope.claimWorkflowModel.sendBackInfo, width: 250 };
                if ($scope.claimWorkflowModel.sendBackInfo) {//this means $scope.claimWorkflowModel.sendBackInfo not (null or empty), equal !string.IsNullOrEmpty()
                    $('.workflow-popover').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverSettings));
                }
            });

            $scope.enableWorkflowButton = Global.Settings.Permission.EnableWorkflowButton;

            $scope.backToParticipant = function () {
                //OTSTM2-434
                if ($scope.vendorType === "2") {
                    makeRequest(backToParticipantValidation);
                }
                else {
                    backToParticipantValidation();
                }

                //original
                //backToParticipantValidation();
            }

            $scope.workflowProcess = function (claimRole, fromStatus, toStatus, userRole) {
                //OTSTM2-434
                if ($scope.vendorType === "2") {
                    //check supporting doc here
                    if (fromStatus < toStatus) {//pass to right side
                        if (claimService.CheckSupportingFiles('workflowProcess')) {
                            return;
                        }
                    }
                    makeRequest(workflowValidation, claimRole, fromStatus, toStatus, userRole);
                }
                else {
                    workflowValidation(claimRole, fromStatus, toStatus, userRole);
                }

                //original
                //workflowValidation(claimRole, fromStatus, toStatus, userRole);
            };

            $scope.approveClaim = function (fromStatus, toStatus) {
                //OTSTM2-434
                if ($scope.vendorType === "2") {
                    //check supporting doc here
                    if (fromStatus < toStatus) {//pass to right side
                        if (claimService.CheckSupportingFiles('approveClaim')) {
                            return;
                        }
                    }
                    makeRequest(approveValidation, fromStatus, toStatus);
                }
                else {
                    approveValidation(fromStatus, toStatus)
                }

                //original
                //approveValidation(fromStatus, toStatus)
            };

            //OTSTM2-434
            function makeRequest(callback) {

                var args = Array.prototype.slice.call(arguments, 1);

                //make an ajax call to get most up to date total inbound
                $http({ url: Global.ParticipantClaimsSummary.IsStaffTotalInboundOutbound, method: "POST" }).then(function (result) {//Global.ParticipantClaimsSummary.IsStaffTotalInboundOutbound is only for collector claim: $scope.vendorType === "2"
                    if (!result.data.isTotalInboundOutbound) {
                        openErrorModal("The total inbound doesn't match with total outbound.");
                        return;
                    }
                    else {
                        callback.apply(this, args)
                    }
                });
            }

            function backToParticipantValidation() {
                var isClaimOnHold = $scope.claimStatusModel.ClaimOnhold;
                var isAuditOnHold = $scope.claimStatusModel.AuditOnhold;

                if (isClaimOnHold) {
                    openErrorModal("Workflow cannot be moved back or forward until the claim is taken off hold. Please refer to claim period(s) on hold.");
                }
                else if (isAuditOnHold) {
                    openErrorModal("Workflow cannot be moved back or forward until the claim is taken off hold for Audit. Please refer to claim period(s) on hold.");
                }
                else {
                    var claimParticipantModalInstance = $uibModal.open({
                        templateUrl: 'modalParticipant.html',
                        controller: 'claimParticipantConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                    });
                    claimParticipantModalInstance.result.then(function () {
                        claimService.claimBackToParticipant().then(function (data) {
                            if (data.status == "refresh") {
                                $window.location.reload();
                            }
                        });
                    });
                }
            }

            function workflowValidation(claimRole, fromStatus, toStatus, userRole) {
                claimService.ValidateWorkflowProcess().then(function (data) {
                    var isForwardRole = fromStatus < toStatus;
                    var isTotalClosingNegative = false; //OTSTM2-488, 663, 664
                    if ($scope.vendorType === "3") {
                        isTotalClosingNegative = $scope.totalClosingOnRoad < 0 || $scope.totalClosingOffRoad < 0 || $scope.totalClosingTotal < 0; //OTSTM2-488
                    }
                    else if ($scope.vendorType === "4" || $scope.vendorType === "5") {
                        isTotalClosingNegative = $scope.closingInventory < 0; //OTSTM2-663,664
                    }

                    if (isForwardRole && data) {
                        openErrorModal(data);
                    }
                    else {
                        //OTSTM2-488, 663, 664
                        if (isForwardRole && isTotalClosingNegative) {
                            openErrorModal("This claim has a <strong>negative closing inventory</strong> value and cannot be approved <br>until an adjustment is made.");
                            return;
                        }

                        ///$scope.workFlow: "Collector" / "Hauler" / "Processor" / "RPM";   
                        ///userRole:'Representative'/'Lead'/'Supervisor'/'Approver 1'/'Approver 2'     (enum ClaimWorkflowStatus)
                        claimService.loadClaimWorkflowUsers(userRole, $scope.workFlow).then(function (data) {
                            $scope.claimUsers = data;
                            var claimProcessingModalInstance = $uibModal.open({
                                templateUrl: 'modalClaimProcessing.html',
                                controller: 'claimProcessingCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    users: function () {
                                        return $scope.claimUsers;
                                    },
                                    userRole: function () {
                                        return userRole;
                                    }
                                }
                            });

                            claimProcessingModalInstance.result.then(function (selectedUser) {
                                claimService.updateClaimWorkflowStatus(selectedUser.userId, fromStatus, toStatus).then(function (data) {
                                    if (data.status == "refresh") {
                                        $scope.claimStatusModel.UnderReview = "Under Review by " + selectedUser.userName;
                                        //claimService.getClaimWorkflowViewModel().then(function (data) {
                                        //    $scope.claimWorkflowModel = data;
                                        //});
                                        $window.location.reload();
                                    }
                                });
                            });
                        });
                    }
                });
            }

            function approveValidation(fromStatus, toStatus) {
                claimService.ValidateWorkflowProcess().then(function (data) {
                    var isTotalClosingNegative = false; //OTSTM2-488, 663, 664
                    if ($scope.vendorType === "3") {
                        isTotalClosingNegative = $scope.totalClosingOnRoad < 0 || $scope.totalClosingOffRoad < 0 || $scope.totalClosingTotal < 0; //OTSTM2-488
                    }
                    else if ($scope.vendorType === "4" || $scope.vendorType === "5") {
                        isTotalClosingNegative = $scope.closingInventory < 0; //OTSTM2-663,664
                    }

                    if (data) {
                        openErrorModal(data);
                    }
                    else {
                        //OTSTM2-488, 663, 664
                        if (isTotalClosingNegative) {
                            openErrorModal("This claim has a <strong>negative closing inventory</strong> value and cannot be approved <br>until an adjustment is made.");
                            return;
                        }

                        //validate claim amount, threshold and assignee before approving                       
                        claimService.ConcurrentUsersValidationBeforeApproving(fromStatus).then(function (data) {
                            if (data.isWarningModalPopup) {
                                var WarningModalInstance = $uibModal.open({
                                    templateUrl: 'modalWarningMessage.html',
                                    controller: 'modalWarningMessageCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        errorMessage: function () {
                                            return "This Claim's Grand Total has changed and now requires a different level of approval. Please send the claim back to a lower Approver or forward to a higher Approver.";
                                        },
                                    }
                                });
                            }
                            else {
                                var claimApprovalModalInstance = $uibModal.open({
                                    templateUrl: 'modalApproveClaim.html',
                                    controller: 'claimApproveClaimConfirmCtrl',
                                    size: 'lg',
                                    backdrop: 'static'
                                });
                                claimApprovalModalInstance.result.then(function () {
                                    claimService.approveClaim(fromStatus, toStatus).then(function (data) {
                                        if (data.status == "refresh") {
                                            $window.location.reload();
                                        } else {
                                            openErrorModal(data.status); //"Approve claim failure. Please checking the data again.");
                                            return;
                                        }
                                    });
                                });
                            }
                        })
                    }
                })
            }

            //OTSTM2-434
            function openErrorModal(message) {
                $uibModal.open({
                    templateUrl: 'modalErrorMessage.html',
                    controller: 'claimWarningMessageCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        errorMessage: function () {
                            return message;
                        },
                        headerText: function () {
                            return 'Unable to Proceed';
                        }
                    }
                });
            }

            $scope.$watch("claimWorkflowModel", function (newVal, oldVal) {
                //if (oldVal) {
                //    console.log('$watch("old.sendBackInfo")', oldVal.sendBackInfo);
                //}
                //if (newVal) {
                //    console.log('$watch("new.sendBackInfo")', newVal.sendBackInfo);
                //}
                if (newVal != oldVal && (typeof ewVal) != undefined) {
                    if (newVal.sendBackInfo != '') {
                        $('.workflow-popover').webuiPopover('destroy').webuiPopover($.extend({}, settings, { content: newVal.sendBackInfo, width: 250 }));
                    } else {
                        $('.workflow-popover').webuiPopover('destroy');
                    }
                }
            }, true);
        }
    };
}]);

commonClaimLib.directive("claimBreadCrumb", ['$http', function ($http) {

    return {
        restrict: 'E',
        templateUrl: 'claimSummaryBreadCrumb.html',
        scope: true,
        link: function (scope, elem, attr, ngModel) {
        },
        controller: function ($scope) {
            $http.get(Global.ParticipantClaimsSummary.LoadClaimPeriods).then(function (result) {
                $scope.allClaimPeriods = result.data;
                $scope.numberOfClaimPeriods = result.data.length;
                for (var i = 0; i < result.data.length; i++) {
                    //$scope.allClaimPeriods[i].Url = "/Collector/Claims/ClaimSummary/?claimId=" + $scope.allClaimPeriods[i].claimId; 
                    $scope.allClaimPeriods[i].Url = Global.ParticipantClaimsSummary.BreadCrumbUrl + "?claimId=" + $scope.allClaimPeriods[i].claimId;
                }
                $scope.countSelection = 1;
            });

            $scope.getClaimPeriod = function (count) {
                if (count == 1)
                    $scope.ClaimPeriods = $scope.allClaimPeriods.slice(count);
                else {
                    $scope.ClaimPeriods = $scope.allClaimPeriods;
                }
            };

            $scope.moreOnClick = function (event) {
                $scope.countSelection = $scope.numberOfClaimPeriods;
                event.stopPropagation();
            };

            $scope.$watch('countSelection', function (newVal, oldVal) {
                if (newVal != oldVal) {
                    $scope.getClaimPeriod(newVal);
                }
            }, true);
        }
    };
}]);

commonClaimLib.directive("claimStatus", ['$uibModal', 'claimService', function ($uibModal, claimService) {
    return {
        retrict: 'E',
        templateUrl: 'claimStatus.html',
        scope: {
            claimStatusModel: '='
        },
        link: function (scope, elem, attr, ngModel) {

        },
        controller: function ($scope) {
            $scope.transactionUrls = {
                totalTransactionUrl: 'totalTransactionUrl.html',
                paperTransactionUrl: 'paperTransactionUrl.html',
                mobileTransactionUrl: 'mobileTransactionUrl.html',
                unreviewedTransactionUrl: 'unreviewedTransactionUrl.html',
                ajustedTransactionUrl: 'ajustedTransactionUrl.html',
                approvedTransactionUrl: 'approvedTransactionUrl.html',
                reviewedTransactionUrl: 'reviewedTransactionUrl.html',
                invalidatedTransactionUrl: 'invalidatedTransactionUrl.html'
            };

            $scope.claimOnhold = function () {
                if ($scope.claimStatusModel.isFutureAuditOnHold)
                    openErrorModal("This claim cannot be placed on this hold type.  In order to proceed, please see subsequent claim(s) hold type.");
                else if ($scope.claimStatusModel.isPreviousAuditOnHold) {
                    openErrorModal("This claim cannot be placed on this hold type.  In order to proceed, please see previous claim(s) on hold.");
                }
                else {
                    var claimOnholdModalInstance = $uibModal.open({
                        templateUrl: 'modalClaimOnhold.html',
                        controller: 'claimOnnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "on-hold";
                            }
                        }
                    });
                    claimOnholdModalInstance.result.then(function () {
                        claimService.claimOnholdOffHold(true).then(function (data) {
                            $scope.claimStatusModel.ClaimOnholdDate = data.updatedDate;
                            $scope.claimStatusModel.ReviewDue = data.claimReviewDueDate;
                            $scope.claimStatusModel.ClaimOnhold = true;
                        });
                    });
                }
            }

            function openErrorModal(message) {
                $uibModal.open({
                    templateUrl: 'modalErrorMessage.html',
                    controller: 'claimWarningMessageCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        errorMessage: function () {
                            return message;
                        },
                        headerText: function () {
                            return 'Unable to Proceed';
                        }
                    }
                });
            }
            $scope.auditOnhold = function () {
                if ($scope.claimStatusModel.isFutureClaimOnHold)
                    openErrorModal("This claim cannot be placed on this hold type.  In order to proceed, please see subsequent claim(s) hold type.");
                else if ($scope.claimStatusModel.isPreviousClaimOnHold) {
                    openErrorModal("This claim cannot be placed on this hold type.  In order to proceed, please see previous claim(s) on hold.");
                }
                else {
                    var auditOnholdModalInstance = $uibModal.open({
                        templateUrl: 'modalAuditOnhold.html',
                        controller: 'auditOnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "on-hold";
                            }
                        }
                    });
                    auditOnholdModalInstance.result.then(function () {
                        claimService.auditOnholdOffHold(true).then(function (data) {
                            $scope.claimStatusModel.AuditOnholdDate = data.updatedDate;
                            $scope.claimStatusModel.ReviewDue = data.claimReviewDueDate;
                            $scope.claimStatusModel.AuditOnhold = true;
                        });
                    });
                }
            }

            $scope.auditOffhold = function () {
                if ($scope.claimStatusModel.isPreviousAuditOnHold) {
                    var auditOnholdModalWarningInstance = $uibModal.open({
                        templateUrl: 'modalAuditOnholdWarning.html',
                        controller: 'auditOnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "";
                            }
                        }
                    });
                }
                else {
                    var auditOffholdModalInstance = $uibModal.open({
                        templateUrl: 'modalAuditOnhold.html',
                        controller: 'auditOnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "off-hold";
                            }
                        }
                    });
                    auditOffholdModalInstance.result.then(function () {
                        claimService.auditOnholdOffHold(false).then(function (data) {
                            $scope.claimStatusModel.AuditOffholdDate = data.updatedDate;
                            $scope.claimStatusModel.ReviewDue = data.claimReviewDueDate;
                            $scope.claimStatusModel.AuditOnhold = false;
                        });
                    });
                }
            }

            $scope.claimOffhold = function () {
                if ($scope.claimStatusModel.isPreviousClaimOnHold) {
                    var claimOnholdModalWarningInstance = $uibModal.open({
                        templateUrl: 'modalClaimOnholdWarning.html',
                        controller: 'claimOnnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "";
                            }
                        }
                    });
                }
                else {
                    var claimOffholdModalInstance = $uibModal.open({
                        templateUrl: 'modalClaimOnhold.html',
                        controller: 'claimOnnHoldConfirmCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            infoMessage: function () {
                                return "off-hold";
                            }
                        }
                    });
                    claimOffholdModalInstance.result.then(function () {
                        claimService.claimOnholdOffHold(false).then(function (data) {
                            $scope.claimStatusModel.ClaimOffholdDate = data.updatedDate;
                            $scope.claimStatusModel.ReviewDue = data.claimReviewDueDate;
                            $scope.claimStatusModel.ClaimOnhold = false;
                        });
                    });
                }
            }
        }
    };
}]);

commonClaimLib.directive('haulerExport', [function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        template: '<i class="fa fa-file-text-o fa-fw uiaction-export""></i>',
        link: function (scope, element, attr, ngModel) {

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                var firstTable = document.querySelectorAll('#' + attr.firstTable);

                if (attr.secondTable != null) {
                    var secondTable = document.querySelectorAll('#' + attr.secondTable);
                    var fTable = angular.element(firstTable[0]);
                    var sTable = angular.element(secondTable[0]);
                    var csvVal = createCSV(fTable) + createCSV(sTable);
                }
                else {
                    var table = angular.element(firstTable[0]);
                    var csvVal = createCSV(table);
                }

                //var filename = firstTable[0].attributes.name.value + '.csv'

                var currentDate = new Date();
                var dd = currentDate.getDate();
                var mm = currentDate.getMonth() + 1;
                var yyyy = currentDate.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                currentDate = yyyy + '-' + mm + '-' + dd;

                var filename = firstTable[0].attributes.name.value + "-" + currentDate + '.csv';

                var link = $('<a/>', {
                    style: 'display:none',
                    href: 'data:application/octet-stream;base64,' + btoa(csvVal),
                    download: filename
                }).appendTo('body')
                link[0].click()
                link.remove();
            })

            function createCSV(table) {
                var csvVal = '';
                for (var i = 0; i < table[0].rows.length; i++) {

                    var rowData = table[0].rows[i].cells;
                    for (var j = 0; j < rowData.length; j++) {
                        //check if row contains anchor
                        if (rowData[j].innerHTML.indexOf('a href=') > -1) {
                            var anchorObj = createElement(rowData[j]);

                            var calcVal = '';
                            if (typeof (anchorObj.innerHTML) !== "undefined") {
                                calcVal = anchorObj.innerHTML;
                            }

                            if (typeof (anchorObj.textContent) !== "undefined") {
                                calcVal = anchorObj.textContent.trim();
                            }

                            if (isNumeric(calcVal.replace(/,/g, "")))
                                csvVal = csvVal + calcVal.replace(/,/g, "") + ",";
                            else
                                csvVal = csvVal + calcVal + ",";
                        }
                        else if (rowData[j].innerHTML.indexOf('<p') > -1) {
                            var pObj = createElement(rowData[j]);
                            if (isNumeric(pObj.innerHTML.replace(/,/g, "")))
                                csvVal = csvVal + pObj.innerHTML.replace(/,/g, "") + ",";
                            else
                                csvVal = csvVal + pObj.innerHTML + ",";
                        }
                        else if (rowData[j].innerHTML.indexOf('<em') > -1) {
                            var emObj = createElement(rowData[j]);
                            if (isNumeric(emObj.innerHTML.replace(/,/g, "")))
                                csvVal = csvVal + emObj.innerHTML.replace(/,/g, "") + ",";
                            else
                                csvVal = csvVal + emObj.innerHTML + ",";
                        }
                        else if (rowData[j].innerHTML.indexOf('<div') > -1) {
                            csvVal = csvVal + "\n";
                        }
                        else if (rowData[j].innerHTML.indexOf('<strong') > -1) {
                            var strongObj = createElement(rowData[j]);
                            if (isNumeric(strongObj.innerHTML.replace(/,/g, "")))
                                csvVal = csvVal + strongObj.innerHTML.replace(/,/g, "") + ",";
                            else
                                csvVal = csvVal + strongObj.innerHTML + ",";
                        }
                        else if (rowData[j].innerHTML.indexOf('&nbsp;') > -1) {
                            csvVal = csvVal + " ,";
                        }
                        else {
                            //check if its numeric
                            if (isNumeric(rowData[j].innerHTML.replace(/,/g, "")))
                                csvVal = csvVal + rowData[j].innerHTML.replace(/,/g, "") + ",";
                            else
                                csvVal = csvVal + rowData[j].innerHTML + ",";
                        }
                        if (isHTML(csvVal)) {
                            csvVal = stripOutHTML(csvVal);
                        }
                    }
                    csvVal = csvVal.substring(0, csvVal.length - 1);
                    csvVal = csvVal + "\n";
                }
                csvVal = csvVal + "\n";
                return csvVal;
            }

            function isNumeric(val) {
                return !isNaN(parseFloat(val)) && isFinite(val);
            }

            function stripOutHTML(html) {
                var temp = document.createElement('div');
                temp.innerHTML = html;
                return temp.textContent || temp.innerText || "";
            }

            function isHTML(val) {
                var div = document.createElement('div');
                div.innerHTML = val;
                for (var child = div.childNodes, i = child.length; i--;) {
                    if (child[i].nodeType == 1)
                        return true;
                }
                return false;
            }

            function createElement(data) {
                var temp = document.createElement('div');
                temp.innerHTML = data.innerHTML;
                var obj = temp.firstChild;
                return obj;
            }

        }
    }
}]);

commonClaimLib.directive('focusInput', [function () {
    return {
        scope: { focusInput: '=focusInput' },
        link: function (scope, element) {
            scope.$watch('focusInput', function (value) {
                if (value === true) {
                    element[0].focus();
                    scope.focusInput = false;
                }
            });
        }
    };
}]);

commonClaimLib.directive('commonServerExport', ['claimService', '$document', function (claimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            url: '@',
            fileName: '@',
            type: '@',
            subType: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                if (scope.type == "Processor") {
                    var updatedModel = useProcessor();
                }

                if (scope.type == "RPM") {
                    var updatedModel = useRPM();
                }

                claimService.exportList(updatedModel, scope.url).success(function (result) {
                    var currentDate = new Date();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });

            });

            function useProcessor() {
                if (scope.subType == "Inbound") {
                    var processorModel = [
                    { RowName: Global.ParticipantClaimsSummary.Resources.TotalOpening, Actual: scope.model.TotalOpening.toFixed(4) },
                    { RowName: 'PTR', Actual: scope.model.PTR.toFixed(4) },
                    { RowName: 'PIT', Actual: scope.model.PIT.toFixed(4) },
                    { RowName: 'ADJ', Actual: scope.model.AdjustIn.toFixed(4) },
                    { RowName: Global.ParticipantClaimsSummary.Resources.TotalInbound, Actual: scope.model.TotalInbound.toFixed(4) }
                    ];
                }
                if (scope.subType == "Outbound") {
                    var processorModel = [
                    { RowName: 'SPS', Actual: scope.model.SPS.toFixed(4) },
                    { RowName: 'PIT', Actual: scope.model.PITOut.toFixed(4) },
                    { RowName: 'DOR', Actual: scope.model.DOR.toFixed(4) },
                    { RowName: 'ADJ', Actual: scope.model.AdjustOut.toFixed(4) },
                    { RowName: Global.ParticipantClaimsSummary.Resources.TotalOutbound, Actual: scope.model.TotalOutbound.toFixed(4) }
                    ];
                }
                if (scope.subType == "InventoryAdj") {
                    var processorModel = [
                    { RowName: Global.ParticipantClaimsSummary.Resources.OverallAdjustments, Actual: scope.model.TotalAdjustment.toFixed(4) },
                    { RowName: Global.ParticipantClaimsSummary.Resources.ClosingInventory, Actual: scope.model.ClosingInventory.toFixed(4) },
                    { RowName: Global.ParticipantClaimsSummary.Resources.ProcessingCapacity, Actual: scope.model.TotalCapacity.toFixed(4) }
                    ];
                }

                return processorModel;
            }

            function useRPM() {
                if (scope.subType == "Inbound") {
                    var rpmModel = [
                    { RowName: Global.ParticipantClaimsSummary.Resources.OpeningInventory, Actual: scope.model.TotalOpening !== null ? scope.model.TotalOpening.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'SIR', Actual: scope.model.SIR !== null ? scope.model.SIR.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'ADJ', Actual: scope.model.AdjustIn !== null ? scope.model.AdjustIn.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: Global.ParticipantClaimsSummary.Resources.TotalInbound, Actual: scope.model.TotalInbound !== null ? scope.model.TotalInbound.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' }
                    ];
                }

                if (scope.subType == "Outbound") {
                    var rpmModel = [
                    { RowName: 'SPS', Actual: scope.model.SPS !== null ? scope.model.SPS.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'ADJ', Actual: scope.model.AdjustOut !== null ? scope.model.AdjustOut.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: Global.ParticipantClaimsSummary.Resources.TotalOutbound, Actual: scope.model.TotalOutbound !== null ? scope.model.TotalOutbound.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' }
                    ];
                }

                if (scope.subType == "InventoryAdj") {
                    var rpmModel = [
                    { RowName: Global.ParticipantClaimsSummary.Resources.OverallAdjustments, Actual: scope.model.OverallAdjustments !== null ? scope.model.OverallAdjustments.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: Global.ParticipantClaimsSummary.Resources.ClosingInventory, Actual: scope.model.ClosingInventory !== null ? scope.model.ClosingInventory.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: Global.ParticipantClaimsSummary.Resources.ManufacturingCapacity, Actual: scope.model.TotalCapacity !== null ? scope.model.TotalCapacity.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' }
                    ];
                }

                return rpmModel;
            }

            function isNumeric(val) {
                if ((val !== undefined) && (val !== null)) {
                    return !isNaN(parseFloat(val)) && isFinite(val);
                }
                return '';
            }
            function isNull(val) {
                if ((val !== undefined) && (val !== null)) {
                    return val;
                }
                return '';
            }
        }
    }

}]);

commonClaimLib.directive('manageInternalNotes', ['$compile', '$window', '$http', 'claimService', '$filter', function ($compile, $window, $http, claimService, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'claim-internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@',
            parentId: '@'
        },
        link: function (scope, elem, attr, ngModel) {

            scope.noteTDWidthpixel = 0;//document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;//not exists yet
            scope.elem = elem;
            scope.focus = function () {
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            scope.removeIcon = function ($event) {
                scope.searchNote = '';
                var search = $event.currentTarget.parentElement.firstElementChild;
                $(search).focus();
            }

            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                }
            })

            scope.search = function () {

                //scope.internalNoteLength = scope.filtered.length;
                scope.internalNoteLength = $filter('filter')(scope.allInternalNotes, scope.searchNote).length;
            };

            scope.add = function () {
                if (scope.textAreaModel) {
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            parentId: scope.parentId,
                            notes: scope.textAreaModel
                        })
                    }).success(function (result) {
                        //Load results after 
                        $http({
                            url: scope.loadUrl,
                            method: "GET",
                            params: {
                                parentId: scope.parentId,
                            },
                        }).success(function (result) {
                            scope.textAreaModel = '';
                            scope.allInternalNotes = result;
                            //scope.quantity = scope.allInternalNotes.length;
                            //if (scope.quantity > 4) {
                            //    scope.showMore = false;
                            //}
                        })
                    })
                }
            };

            scope.stripNotes = function (note) {

                var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one //.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                var trimWidth = 0;
                var bestFitNbr = 50;
                var shortNote = '';
                ///if trimmed-note text width is longer than current TD.offsetWidth, try to find best match string.length of noteTrimedStr, then return to UI. 
                ///since string.length affect webui-popover horizontal position
                if (noteWidth + 15 >= scope.noteTDWidthpixel) { //OTSTM2-845 make sure "..." effect is same with or without vertical scrollbar
                    while (trimWidth < scope.noteTDWidthpixel - 15) {
                        bestFitNbr += 2;
                        shortNote = noteTrimedStr.substr(0, bestFitNbr);//incrase str size and try again
                        trimWidth = getTextWidth(shortNote, "0.875em Open Sans");
                    }
                    return shortNote + "...";
                }
                else {
                    return note;
                }
            };

            scope.checkNoteSize = function (note) {

                if (note != undefined) {
                    var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    if (scope.noteTDWidthpixel == 0) {
                        if (document.getElementById(scope.panelArrow + '-notes-width')) {
                            scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;
                        }
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    var tdAddedBy = document.getElementById('tdAddedBy');
                    if (hasScrollBar(internalNoteBody)) { //OTSTM2-845 check if the vertical scrollbar appears
                        $(tdAddedBy).css("margin-left", "-20px"); //OTSTM2-845 "AddedBy" move left for alignment if vertical scrollbar appears
                        return (scope.noteTDWidthpixel <= noteWidth + 32);
                    }
                    else {
                        return (scope.noteTDWidthpixel <= noteWidth + 6);
                    }
                }
                else {
                    return false;
                }

            };

            angular.element($window).bind('resize', function () {

                var length = scope.allInternalNotes.length;
                for (var i = 0; i < length; i++) {
                    var noteTrimedStr = scope.allInternalNotes[i].Note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    var trimWidth = 0;
                    var bestFitNbr = 50;
                    var shortNote = '';
                    scope.noteTDWidthpixel = 0;
                    var $div = $("<td ng-if=\"checkNoteSize(note.Note)\" width=\"75%\" style=\"cursor: pointer\" class=\"internal-note{{$index}}\"><span internal-note-hover-popover data-note=\"{{note.Note}}\">{{note.Note}}</span></td>");
                    if (scope.noteTDWidthpixel == 0) {
                        if (document.getElementById(scope.panelArrow + '-notes-width')) {
                            scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;
                        }
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    if (hasScrollBar(internalNoteBody)) {
                        if (scope.noteTDWidthpixel <= noteWidth + 32) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();

                            }
                        }
                    }
                    else {
                        if (scope.noteTDWidthpixel <= noteWidth + 6) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();

                            }
                        }
                    }

                }
                scope.$digest();
            });

        },
        controller: function ($scope, $rootScope) {

            $http({
                url: $scope.loadUrl,
                method: "GET",
                params: {
                    parentId: $scope.parentId,
                },
            }).then(function (result) {
                $scope.allInternalNotes = result.data;
                $scope.sortType = 'AddedOn';
                $scope.textAreaModel = '';
                $scope.sortReverse = true;
                $scope.searchNote = '';
                $scope.quantity = 4;
                //$scope.showMore = result.data.length > 4;//true;
                $scope.showMore = true;
                $scope.export = Global.InternalNoteSettings.InternalExportUrl;
            })

            $rootScope.$on('TIRE_ORIGIN_INTERNAL_NOTE', function (e, data) { //OTSTM2-386
                $scope.allInternalNotes = data;
                $scope.quantity = $scope.allInternalNotes.length;
                //if ($scope.quantity > 4) {
                //    $scope.showMore = false;
                //}
            });

        }
    };
}]);

commonClaimLib.directive('internalNoteHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {

    var getTemplate = function (contentType) {
        return $templateCache.get('internalNotePopoverTemplate.html');
    };

    var header = '<span><strong>Internal Notes</strong></span>' +
        '<button type="button" class="close" ng-click="closeMe()")><span aria-hidden="true">×</span></button>';

    var getHeader = function (contentType) {
        return $templateCache.get('internalNotePopoverHeader.html');
    };

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var note = toHtml(attrs.note);
            $templateCache.put('internalNotePopoverTemplate.html', '<div style="overflow: auto; height: 300px; padding:true;">' + note + '</div>');
            $templateCache.put('internalNotePopoverHeader.html', header);

            var content = getTemplate();
            var popoverheader = getHeader();

            $rootScope.insidePopover = false;
            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                //max-width:500px;
                placement: 'top',
                title: function () {
                    return $compile(popoverheader)(scope);
                },
                html: true
            }).on("show.bs.popover", function () {
                $(this).data("bs.popover").tip().css({ "width": "500px", "max-width": "500px", "cursor": "default" });
            });
            //.on("shown.bs.popover", function () { $(this).data("bs.popover").tip().css({"cursor": "default" }); });
            //.on("hidden.bs.popover", function () { console.log('"hidden.bs.popover"'); });

            //$(element).bind('mouseenter', function (e) {
            //    $timeout(function () {
            //        if (!$rootScope.insidePopover) {
            //            $(element).popover('show');
            //            scope.attachEvents(element);
            //        }
            //    }, 200);
            //});
            //$(element).bind('mouseleave', function (e) {
            //    $timeout(function () {
            //        if (!$rootScope.insidePopover)
            //            $(element).popover('hide');
            //    }, 400);
            //});

            $(element).bind('click', function (e) {
                $timeout(function () {
                    if ($("div.popover").length > 1) {//if more then popover open
                        $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close existing one
                    }
                    Global.InternalNoteSettings.currentOpenId = $("div.popover.in").attr('id');
                }, 100);
            });

            scope.closeMe = function () {
                $(element).popover('hide');
            }

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }

        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                //$('.popover').on('mouseenter', function () {
                //    $rootScope.insidePopover = true;
                //});
                //$('.popover').on('mouseleave', function () {
                //    $rootScope.insidePopover = false;
                //    $(element).popover('hide');
                //});
            }

            $('#processor-claims-summary-notes').on('hidden.bs.collapse', function () {
                //console.log('chevron close');
                $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close opened popover
            });
        }
    };
});

commonClaimLib.directive('internalNotesExport', ['$http', '$document', function ($http, $document) {

    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            id: '@',
            searchText: '@',
            sortReverse: '@',
            sortColumn: '@',
            url: '@',
            model: '=',
            fileName: '@'
        },
        link: function (scope, elem, attr, ngModel) {
            elem.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                //using parent id for loading rates for specific processor bulls eye
                //var req = { id: scope.id, searchText: scope.searchText, sortReverse: scope.sortReverse, sortcolumn: scope.sortColumn, model: scope.model };
                var req = {
                    parentId: scope.$parent.parentId, searchText: scope.searchText, sortReverse: scope.sortReverse, sortcolumn: scope.sortColumn, model: scope.model
                };
                var submitVal = {
                    url: scope.url,
                    method: "POST",
                    data: JSON.stringify(req)
                }
                return $http(submitVal).then(function (result) {

                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = scope.fileName + "-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });
            });
        },
        controller: function ($scope) {

        }
    };
}]);

commonClaimLib.directive('commonPaymentAdjustExport', ['claimService', '$document', function (claimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            url: '@',
            fileName: '@',
            claimType: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                var subtotal=0;
                if (scope.claimType == "Processor") {                 
                    subtotal += scope.model.PTR !== null ? scope.model.PTR:0;
                    subtotal += scope.model.SPS !== null ? scope.model.SPS:0;
                    subtotal += scope.model.PITOutbound !== null ? scope.model.PITOutbound:0;
                    subtotal += scope.model.DOR !== null ? scope.model.DOR:0;
                    subtotal += scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment:0;
                
                    var updatedModel = [
                    { RowName: 'PTR', Payment: scope.model.PTR !== null ? scope.model.PTR.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'SPS', Payment: scope.model.SPS !== null ? scope.model.SPS.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'PIT Outbound', Payment: scope.model.PITOutbound !== null ? scope.model.PITOutbound.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : ''},
                    { RowName: 'DOR', Payment: scope.model.DOR !== null ? scope.model.DOR.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Adjustments', Payment: scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Subtotal ', Payment: subtotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })},
                    { RowName: 'HST ' + (scope.model.HSTBase * 100).toFixed(2) + '%', Payment: scope.model.HST !== null ? scope.model.HST.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Grand Total', Payment: scope.model.GroundTotal !== null ? scope.model.GroundTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Due', Payment: formatDate(scope.model.PaymentDueDate) },
                    { RowName: 'Cheque/EFT #', Payment: scope.model.ChequeNumber },
                    { RowName: 'Paid', Payment: formatDate(scope.model.PaidDate) },
                    { RowName: 'Amount Paid ($)', Payment: scope.model.AmountPaid !== null ? scope.model.AmountPaid.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Mailed', Payment: formatDate(scope.model.MailedDate) },
                    { RowName: scope.model.isStaff ? 'Batch#' : "", Payment:scope.model.isStaff ? scope.model.BatchNumber :""},
                    ];
                }

                if (scope.claimType == "Hauler") {
                    var updatedModel = [
                    { RowName: 'Transportation Premium', Payment: scope.model.NorthernPremium !== null ? scope.model.NorthernPremium.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'DOT Premium', Payment: scope.model.DOTPremium !== null ? scope.model.DOTPremium.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'STC Premium', Payment: scope.model.STCPremium !== null ? scope.model.STCPremium.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Adjustments', Payment: scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Subtotal', Payment: scope.model.SubTotal !== null ? scope.model.SubTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'HST ' + (scope.model.HSTBase * 100).toFixed(2) + '%', Payment: scope.model.HST !== null ? scope.model.HST.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Total', Payment: scope.model.PaymentTotal !== null ? scope.model.PaymentTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Ineligible Inventory Payment', Payment: scope.model.IneligibleInventoryPayment !== null ? scope.model.IneligibleInventoryPayment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Ineligible Inventory Adjustments', Payment: scope.model.IneligibleInventoryPaymentAdjust !== null ? scope.model.IneligibleInventoryPaymentAdjust.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Grand Total', Payment: scope.model.GrandTotal !== null ? scope.model.GrandTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Due', Payment: formatDate(scope.model.PaymentDueDate) },
                    { RowName: 'Cheque/EFT #', Payment: scope.model.ChequeNumber },
                    { RowName: 'Paid', Payment: formatDate(scope.model.PaidDate) },
                    { RowName: 'Amount Paid ($)', Payment: scope.model.AmountPaid !== null ? scope.model.AmountPaid.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Mailed', Payment: formatDate(scope.model.MailedDate) },
                    { RowName: scope.model.isStaff ? 'Batch#' : "", Payment: scope.model.isStaff ? scope.model.BatchNumber : "" },
                    ];
                }

                if (scope.claimType == "RPM") {                                  
                    subtotal += scope.model.SPS !== null ? scope.model.SPS : 0;
                    subtotal += scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment : 0;
                    var updatedModel = [
                        { RowName: 'SPS', Payment: scope.model.SPS !== null ? scope.model.SPS.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                        { RowName: 'Payment Adjustments', Payment: scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                        { RowName: 'Subtotal', Payment: subtotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })},
                        { RowName: 'HST ' + (scope.model.HSTBase * 100).toFixed(2) + '%', Payment: scope.model.HST !== null ? scope.model.HST.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                        { RowName: 'Grand Total', Payment: scope.model.GroundTotal !== null ? scope.model.GroundTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                        { RowName: 'Payment Due', Payment: formatDate(scope.model.PaymentDueDate) },
                        { RowName: 'Cheque/EFT #', Payment: scope.model.ChequeNumber },
                        { RowName: 'Paid', Payment: formatDate(scope.model.PaidDate) },
                        { RowName: 'Amount Paid ($)', Payment: scope.model.AmountPaid !== null ? scope.model.AmountPaid.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                        { RowName: 'Mailed', Payment: formatDate(scope.model.MailedDate) },
                         { RowName: scope.model.isStaff ? 'Batch#' : "", Payment: scope.model.isStaff ? scope.model.BatchNumber : "" },
                    ];
                }

                claimService.exportList(updatedModel, scope.url).success(function (result) {
                    var currentDate = formatDate(new Date());

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });

            });
            function formatDate(val) {

                if ((val !== undefined) && (val !== null)) {
                    var date = new Date(val);

                    var dd = date.getDate();
                    var mm = date.getMonth() + 1;
                    var yyyy = date.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    date = yyyy + '-' + mm + '-' + dd;

                    return date;
                }
            }
            function isNumeric(val) {
                if ((val !== undefined) && (val !== null)) {
                    return !isNaN(parseFloat(val)) && isFinite(val);
                }
                return '';
            }
            function isNull(val) {
                if ((val !== undefined) && (val !== null)) {
                    return val;
                }
                return '';
            }
        }
    }

}]);

commonClaimLib.directive('commonStatusPanelExport', ['claimService', '$document', function (claimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            url: '@',
            fileName: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                var updatedModel = [
                    {
                        Type: '1',
                        firstCol: formatDate(scope.model.Submitted),
                        secondCol: formatDate(scope.model.Assigned),
                        thirdCol: formatDate(scope.model.Started),
                        fourthCol: formatDate(scope.model.ReviewDue),
                        fifthCol: formatDate(scope.model.Reviewed),
                        sixthCol: formatDate(scope.model.Approved)
                    },
                    {
                        Type: '2',
                        firstCol: 'Claims',
                        secondCol: scope.model.ClaimOnhold ? 'On' : 'Off',
                        thirdCol: formatDate(scope.model.ClaimOnholdDate),
                        fourthCol: formatDate(scope.model.ClaimOffholdDate),
                        fifthCol: scope.model.ClaimOnholdDays,

                    },
                    {
                        Type: '2',
                        firstCol: 'Audit',
                        secondCol: scope.model.AuditOnhold ? 'On' : 'Off',
                        thirdCol: formatDate(scope.model.AuditOnholdDate),
                        fourthCol: formatDate(scope.model.AuditOffholdDate),
                        fifthCol: scope.model.AuditOnholdDays,
                    },
                    {
                        Type: '3',
                        firstCol: 'Total',
                        secondCol: scope.model.TotalTransactions,
                        thirdCol: 'Paper',
                        fourthCol: scope.model.PaperTransactions,
                        fifthCol: 'Mobile',
                        sixthCol: scope.model.MobileTransactions,
                        seventhCol: 'Unreviewed',
                        eighthCol: scope.model.UnreviewTransactions
                    },
                    {
                        Type: '3',
                        firstCol: 'Adjusted',
                        secondCol: scope.model.AdjustedTransactions,
                        thirdCol: 'Approved',
                        fourthCol: scope.model.ApprovedTransactions,
                        fifthCol: 'Reviewed',
                        sixthCol: scope.model.ReviewedTransactions,
                        seventhCol: 'Invalidated',
                        eighthCol: scope.model.InvalidatedTransactions
                    }
                ];

                //var specialCaseTypeTwo =
                //{
                //    Type: '2',
                //    firstCol: 'On',
                //    secondCol: formatDate(scope.model.ClaimOnholdDate),
                //    thirdCol: formatDate(scope.model.ClaimOffholdDate),
                //    fourthCol: scope.model.ClaimOnholdDays
                //};

                //if (!scope.model.ClaimOnhold) {
                //    specialCaseTypeTwo.firstCol = 'Off';
                //}

                //updatedModel.push(specialCaseTypeTwo);

                claimService.exportList(updatedModel, scope.url).success(function (result) {
                    var currentDate = formatDate(new Date());

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });
            });

            function formatDate(val) {

                if ((val !== undefined) && (val !== null)) {
                    var date = new Date(val);

                    var dd = date.getDate();
                    var mm = date.getMonth() + 1;
                    var yyyy = date.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }

                    date = yyyy + '-' + mm + '-' + dd;

                    return date;
                }
            }
        }
    }
}]);

//controller section
commonClaimLib.controller('claimOnnHoldConfirmCtrl', ['$scope', '$uibModalInstance', 'infoMessage', function ($scope, $uibModalInstance, infoMessage) {
    $scope.infoMessage = infoMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

commonClaimLib.controller('auditOnHoldConfirmCtrl', ['$scope', '$uibModalInstance', 'infoMessage', function ($scope, $uibModalInstance, infoMessage) {
    $scope.infoMessage = infoMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

commonClaimLib.controller('claimParticipantConfirmCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
commonClaimLib.controller('claimProcessingCtrl', ['$scope', '$uibModalInstance', 'users', 'userRole', function ($scope, $uibModalInstance, users, userRole) {
    $scope.users = users;
    $scope.userRole = userRole;

    $scope.confirm = function () {
        $uibModalInstance.close($scope.selectedUser);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
commonClaimLib.controller('claimApproveClaimConfirmCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
commonClaimLib.controller('claimWarningMessageCtrl', ['$scope', '$uibModalInstance', 'errorMessage', 'headerText', function ($scope, $uibModalInstance, errorMessage, headerText) {
    $scope.headerText = 'Claim Process';
    if (headerText) {
        $scope.headerText = headerText;
    }
    $scope.errorMessage = errorMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

commonClaimLib.controller('modalWarningMessageCtrl', ['$scope', '$uibModalInstance', 'errorMessage', '$window', function ($scope, $uibModalInstance, errorMessage, $window) {
    $scope.errorMessage = errorMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $window.location.reload();

    };
}]);

//OTSTM2-270 Validation Directives
commonClaimLib.directive('decimalRequired', [function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {

            var REGEX = new RegExp('^-?[0-9]\\d*\\.?\\d{0,' + attr.decimalRequired + '}$');

            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {
                if (REGEX.test(viewValue) || !viewValue) {
                    ngModelCtrl.$setValidity('isDecimalRequired', true);
                    return viewValue;
                } else {
                    ngModelCtrl.$setValidity('isDecimalRequired', false);
                    return viewValue;
                }
            });
        }
    };
}]);

//service section
commonClaimLib.factory('claimService', ["$http", function ($http) {

    var factory = {
    };

    factory.submitInventoryAdjustment = function (modalResult) {
        var req = {
            modalResult: modalResult
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.SubmitInventoryAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.removeInventoryAdjustment = function (adjustmentType, internalAdjustmentId) {
        var req = {
            adjustmentType: adjustmentType, internalAdjustmentId: internalAdjustmentId
        }
        var submitVal = {
            url: Global.ParticipantInternalAdjustmentSummary.RemoveInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.getInternalAdjustment = function (adjustmentType, internalAdjustmentId) {
        var req = {
            adjustmentType: adjustmentType, internalAdjustmentId: internalAdjustmentId
        }
        var submitVal = {
            url: Global.ParticipantInternalAdjustmentSummary.GetInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.editInventoryAdjustment = function (modalResult) {
        var req = {
            modalResult: modalResult
        }
        var submitVal = {
            url: Global.ParticipantInternalAdjustmentSummary.EditInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.claimOnholdOffHold = function (isOnhold) {
        var req = {
            isOnhold: isOnhold
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ClaimOnholdOffholdUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.auditOnholdOffHold = function (isOnhold) {
        var req = {
            isOnhold: isOnhold
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.AuditOnholdOffholdUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.getClaimWorkflowViewModel = function (claimsAmountTotal) {
        var req = {
            claimsAmountTotal: claimsAmountTotal
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ClaimWorkflowViewModelUrl,
            method: "GET",
            params: {
                claimsAmountTotal: claimsAmountTotal
            },//since Claim.ClaimsAmountTotal value update need much more time, use the value from $scope.XXXPayment.claimsAmountTotal at server side to determine the current work flow position.
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });

        //return $http.get(Global.ParticipantClaimsSummary.ClaimWorkflowViewModelUrl)
        //    .then(function (result) {
        //        return result.data;
        //    });
    }

    factory.claimBackToParticipant = function () {
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ClaimBackToParticipantUrl,
            method: "POST",
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.loadClaimWorkflowUsers = function (workflow, accountName) {
        var req = {
            Workflow: workflow, AccountName: accountName
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.LoadClaimWorkflowUsersUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.updateClaimWorkflowStatus = function (reviewedBy, fromStatus, toStatus) {
        var req = {
            reviewedBy: reviewedBy, fromStatus: fromStatus, toStatus: toStatus
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateClaimWorkflowStatusUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.UpdateHST = function (HST, isTaxApplicable) {
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID, HST: HST, IsTaxApplicable: isTaxApplicable
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateHSTUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.approveClaim = function (fromStatus, toStatus) {
        var req = {
            fromStatus: fromStatus, toStatus: toStatus
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ApproveClaimUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    //OTSTM2-1084 validate claim amount, threshold and assignee before approving
    factory.ConcurrentUsersValidationBeforeApproving = function (fromStatus) {
        var req = {
            fromStatus: fromStatus
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ConcurrentUsersValidationBeforeApprovingUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.exportInternalNotes = function (searchText, sortdirection, sortcolumn) {
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID, searchText: searchText, sortdirection: sortdirection, sortcolumn: sortcolumn
        };
        var submitVal = {
            url: Global.StaffInternalNotes.ExportUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.ValidateWorkflowProcess = function () {
        var submitVal = {
            url: Global.ParticipantClaimsSummary.ValidateWorkflowProcessUrl,
            method: "POST"
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.updateClaimMailDate = function (mailDate) {
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID, mailDate: mailDate
        };
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateClaimMailDateUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.exportList = function (data, url) {
        var submitVal = {
            url: url,
            method: "POST",
            data: JSON.stringify(data)
        }
        return $http(submitVal);
    }

    //OTSTM2-386
    factory.addInternalNote = function (data) {
        var req = {
            id: Global.InternalNoteSettings.ID,
            notes: data
        }
        var submitVal = {
            url: Global.InternalNoteSettings.AddInternalNotesUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.loadInternalNote = function () {
        var req = {
            id: Global.InternalNoteSettings.ID
        }
        var submitVal = {
            url: Global.InternalNoteSettings.LoadInternalNotesUrl,
            method: "GET",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.CheckSupportingFiles = function (data) {
        var returnVal = (($("input:radio[value='optionupload']:checked").length > 0) && ($('#filesToUploadGrid tr').length <= 0));//true == 'has error'
        $('#fileUploadDropzone').toggleClass('has-error', returnVal).toggleClass('dropzone-required', returnVal);

        var localScope = angular.element(document.getElementById("fileupload")).scope();
        localScope.isRequiredErrorFlag = returnVal;//show [Required] error for $('#fileUploadDropzone')
        return returnVal;
    }
    factory.ReloadSupportingDocs = false; //true === "need to refresh supporting doc list"
    factory.isRequiredError = false; //true === "missing required support doc"

    return factory;
}]);

document.body.addEventListener('click', function (e) {
    if (Global.InternalNoteSettings == undefined) {//participant page
        return;
    }
    var insidePopover = clickInsidePopover(e.target);
    if (!insidePopover) {
        $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close opened popover
    }
}, false);

function clickInsidePopover(e) {
    if ((e == null) || (e == undefined)) {
        return false;
    } else if ((e.className === 'popover-content') || (e.className === 'popover-title') || (e.className.startsWith('popover fade'))) {
        return true;
    }
    else {
        return clickInsidePopover(e.parentElement);
    }
}

//OTSTM2-845 option 1 for getting text width, need to add a <canvas> tag
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    //var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var canvas = document.getElementById('myCanvas'); //OTSTM2-845 get the added <canvas>
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return Math.round(metrics.width);
}

//OTSTM2-845 option 2 for getting text width
function getTextWidthDOM(text, font) {
    var f = font || '12px arial',
        o = $('<span>' + text + '</span>')
              .css({ 'font': f, 'float': 'left', 'white-space': 'nowrap' })
              .css({ 'visibility': 'hidden' })
              .appendTo($('body')),
        w = o.width();
    o.remove();
    return w;
}
//OTSTM2-845 check if vertical scrollbar appears
function hasScrollBar(obj) {
    return $(obj).get(0).scrollHeight > $(obj).height();
}

//OTSTM2-735 avoid multiple clicking workflow buttons to open multiple modals
$(document).ready(function () {
    $(".workflow-btn").on('click', function (event) {
        event.preventDefault();
        var el = $(this);
        el.prop('disabled', true);
        setTimeout(function () { el.prop('disabled', false); }, 1000);
    });
});