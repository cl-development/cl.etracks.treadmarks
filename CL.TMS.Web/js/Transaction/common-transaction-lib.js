﻿"use strict";

var commonTransactionLib = angular.module("commonTransactionLib", []);

// Directives Section
///For Add new paper transaction, used by All participant type and All Transaction type
commonTransactionLib.directive('openTransactionPaperForm', ['GlobalSettingService', '$window', function (GlobalSettingService, $window) {
    return {
        restrict: "A",
        scope: {
            claimId: "@",
            typeId: "@",
            direction: "@"
        },
        link: function (scope, element) {
            element.click(function (e) {
                scope.open();
            });
        },
        controller: function ($rootScope, $scope, $http, $uibModal) {
            $scope.open = function () {
                $http({
                    url: GlobalSettingService.InitializePaperFormServiceURL(),
                    method: "POST",
                    data: { id: $scope.claimId, type: $scope.typeId, direction: $scope.direction }
                }).success(function (result) {
                    if (result.status) {
                        var paperFormModal = $uibModal.open({
                            templateUrl: 'paper-form.html',//different new paper transaction type define it's own specific template with same Id 'paper-form.html', used in modal dialog.
                            controller: 'PaperFormModalCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                paperFormModel: function () { return result.data; }
                            }
                        });

                        paperFormModal.result.then(function () {//After close modal dialog, reload current page.
                            $window.location.reload();
                        });

                        //OTSTM2-1214
                        //paperFormModal.rendered.then(function () {
                        //    $('[name="eventNumber"]').focusout(function (e) {
                        //        //$(this).siblings(".event-number-validity").addClass("display-none");                                                           
                        //    });
                        //});

                    } else {
                    }
                }).error(function () {
                });
            };
        }
    };
}]);

commonTransactionLib.directive('creationStatusMessage', [function () {
    return {
        restrict: 'E',
        scope: {
            statusModel: "="
        },
        templateUrl: 'creation-status-message.html',
        controller: function ($scope) {
            $scope.hide = function () {
                return $scope.statusModel.display = false;
            };
        }
    };
}]);

/* Start of Paper Form Components */
commonTransactionLib.directive('formDatePicker', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            labelText: "@",
            form: "=",
            formDate: "="
        },
        templateUrl: 'formDatePicker.html',
        controller: function ($rootScope, $scope) {
            $scope.focusDate = new Date();

            $scope.getDate = function (date) {
                return date.substring(0, 10);
            };

            $scope.datePicker = {
                dateTimeFormat: GlobalSettingService.DatePickerFormat(),
                status: false
            };

            //OTSTM2-558 remove .setHours(0, 0, 0, 0) like new Date(date).setHours(0, 0, 0, 0)
            $scope.disabled = function (date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date);

                    for (var i = 0; i < $scope.formDate.disabledDateRangeList.length; i++) {
                        var disabledDateRange = $scope.formDate.disabledDateRangeList[i];
                        var startDay = new Date(disabledDateRange.startDate);
                        var endDay = new Date(disabledDateRange.endDate);

                        if (dayToCheck >= startDay && dayToCheck <= endDay) {
                            return true;
                        }
                    }
                }

                return false;
            };

            $scope.disabledClass = function (date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date);

                    for (var i = 0; i < $scope.formDate.disabledDateRangeList.length; i++) {
                        var disabledDateRange = $scope.formDate.disabledDateRangeList[i];
                        var startDay = new Date(disabledDateRange.startDate);
                        var endDay = new Date(disabledDateRange.endDate);

                        if (dayToCheck >= startDay && dayToCheck <= endDay) {
                            return 'disabledformdate';
                        }
                    }
                }
                return '';
            };


            $scope.openDatePicker = function ($event) {
                $scope.datePicker.status = true;
            };

            $scope.$watch('formDate', function () {
                if (angular.isDefined($scope.formDate)) {
                    $scope.focusDate = (new Date($scope.formDate.focusDate));
                    //OTSTM2-1215 validate event number when selecting a date
                    if ($scope.formDate.on != null && $scope.form.eventNumber.$touched) {
                        $rootScope.$emit('EVENT_NUMBER_VALIDATION', null);
                    }
                    
                }
            }, true);
        }
    };
}]);

commonTransactionLib.directive('formNumberCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            claimId: "@",
            formNumber: "=",
            formType: "@"
        },
        templateUrl: 'form-number.html',
        controller: function ($scope, $http) {
            $scope.isTCR = function () { return ($scope.formType == "TCR") };
            $scope.isDOT = function () { return ($scope.formType == "DOT") };
            $scope.isSTC = function () { return ($scope.formType == "STC") };
            $scope.isUCR = function () { return ($scope.formType == "UCR") };
            $scope.isHIT = function () { return ($scope.formType == "HIT") };
            $scope.isRTR = function () { return ($scope.formType == "RTR") };
            $scope.isPTR = function () { return ($scope.formType == "PTR") };
            $scope.isPIT = function () { return ($scope.formType == "PIT") };

            $scope.isFormNumberValid = function () {
                return $scope.form.formNumber.$valid;
            };

            $scope.duplicationInSameClaim = false;

            $scope.checkDuplication = function () {
                if ($scope.isFormNumberValid()) {
                    $http({
                        url: GlobalSettingService.CheckFormNumberDuplicationServiceURL(),
                        method: "POST",
                        data: { claimId: $scope.claimId, formNumber: $scope.formNumber.trim() }
                    }).success(function (result) {
                        if (result.data.isFound) {
                            $scope.form.formNumber.$setValidity("duplication", false);
                            $scope.duplicationInSameClaim = result.data.inSameClaim;

                            $scope.$broadcast('show-errors-event');
                        }
                    }).error(function () {
                    });
                }
            };

            $scope.resetFormNumberDuplicationError = function () {
                $scope.duplicationInSameClaim = false;
                $scope.form.formNumber.$setValidity("duplication", true);
            };
        }
    };
}]);

commonTransactionLib.directive('vendorInformationCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            transactionDate: "=",
            vendorNumber: "=",
            vendorCode: "@",
            label: "@"
        },
        templateUrl: 'vendor-information-collector.html',
        controller: function ($rootScope, $scope, $http) {
            $scope.vendorInfoRequired = true;

            $scope.isMultiMode = function () {
                var vendorCodeList = $scope.vendorCode.split(',');
                return vendorCodeList.length > 1;
            };
            $scope.isCollector = function () { return ($scope.vendorCode == "Collector") || ($scope.isVendorValid() && $scope.vendor.info.vendorTypeCode == "Collector") };
            $scope.isHauler = function () { return ($scope.vendorCode == "Hauler") || ($scope.isVendorValid() && $scope.vendor.info.vendorTypeCode == "Hauler") };
            $scope.isProcessor = function () { return ($scope.vendorCode == "Processor") || ($scope.isVendorValid() && $scope.vendor.info.vendorTypeCode == "Processor") };
            $scope.isRPM = function () { return ($scope.vendorCode == "RPM") || ($scope.isVendorValid() && $scope.vendor.info.vendorTypeCode == "RPM") };

            $scope.vendor = null;

            $scope.isVendorNumberValid = function () {
                return $scope.form.vendorNumber.$valid;
            };

            $scope.resetVendorInfo = function () {
                $scope.vendor = null;
                if ($scope.form.vendorNumber != null) {
                    $scope.form.vendorNumber.$setValidity("notReady", true);
                    $scope.form.vendorNumber.$setValidity("notFound", true);
                    $scope.form.vendorNumber.$setValidity("notActive", true);
                    $scope.form.vendorNumber.$setValidity("sameAsCurrent", true);
                }
            };

            $scope.isVendorValid = function () {
                return $scope.vendor != null && $scope.vendor.isFound;
            };

            $scope.$watch('transactionDate', function () {
                if ($scope.isVendorValid() && $scope.transactionDate.on == null) {
                    $scope.form.vendorNumber.$setValidity("notReady", false);
                }

                if ($scope.transactionDate.on != null) {
                    $scope.form.vendorNumber.$setValidity("notReady", true);
                    $scope.form.vendorNumber.$setValidity("notActive", true);
                    $scope.form.vendorNumber.$validate();
                    $scope.getVendorInfo();
                }
            }, true);

            $scope.getVendorInfo = function () {
                if ($scope.vendorInfoRequired && $scope.isVendorNumberValid()) {
                    if ($scope.transactionDate.on == null) {
                        $scope.form.vendorNumber.$setValidity("notReady", false);
                    } else {
                        $http({
                            url: GlobalSettingService.GetVendorInfoServiceURL(),
                            method: "POST",
                            data: { vendorNumber: $scope.vendorNumber, transactionDate: $scope.transactionDate.on, vendorCodeList: $scope.vendorCode.split(',') }
                        }).success(function (result) {
                            if (result.status) {
                                if (result.data.sameAsCurrent) {
                                    $scope.vendor = null;
                                    $scope.form.vendorNumber.$setValidity("sameAsCurrent", false);
                                } else {
                                    if (result.data.isActive) {
                                        $scope.vendor = result.data;
                                        $scope.form.vendorNumber.$setValidity("notFound", result.data.isFound);
                                    } else {
                                        $scope.vendor = null;
                                        $scope.form.vendorNumber.$setValidity("notActive", false);
                                    }
                                }
                            }
                            else {
                                $scope.vendor = null;
                                $scope.form.vendorNumber.$setValidity("notFound", false);
                            }

                            $scope.$broadcast('show-errors-event');
                        }).error(function () {
                        });
                    }
                }
            };

            $rootScope.$on('set-vendor-information-requirement', function (e, message) {
                $scope.vendorInfoRequired = message.required;

                if (angular.isDefined($scope.form) && angular.isDefined($scope.vendorNumber)) {
                    $scope.vendorNumber = null;
                    $scope.resetVendorInfo();
                }
            });
        }
    };
}]);

commonTransactionLib.directive('tireTypeCount', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            tireTypeList: "=",
            generateTires: "=",
            mandatory: "@",
            adjust: "@"
        },
        templateUrl: 'tire-type-count.html',
        controller: function ($rootScope, $scope) {
            $scope.tireTypeCountRequired = true;
            $scope.tireTypeCountAdjusted = false;

            $scope.$watch('mandatory', function () {
                if (angular.isDefined($scope.mandatory) && $scope.mandatory == 'false') {
                    $scope.tireTypeCountRequired = false;
                } else $scope.tireTypeCountRequired = true;
            }, true);

            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.generateTiresObj = { val: angular.isDefined($scope.generateTires) ? $scope.generateTires : false, originalVal: angular.isDefined($scope.generateTires) ? $scope.generateTires : false };
            $scope.tireTypeCountInvalid = false;

            $scope.tireTypeAvailable = function (type) {
                var tireType = _.find($scope.tireTypeList, function (i) { return i.tireType == type; });
                return !_.isUndefined(tireType);
            };

            $scope.tireTypeByType = function (type) {
                return _.find($scope.tireTypeList, function (i) {
                    //comment out for OTSTM2-941, enable by #1232 and set it as ng-init
                    if (i.count == null) {
                        i.count = i.originalCount != null ? i.originalCount : 0;
                    }
                    return i.tireType == type;
                });
            };

            $scope.displayGenerateTires = function () {
                return angular.isDefined($scope.generateTires);
            };

            $scope.isInitTireCount = true;

            $scope.$watch('tireTypeList', function (e) {
                var tireElem = $(".showTireTypeCountError");
                if ($scope.isInitTireCount && tireElem != null && tireElem.length > 0) {
                    $(".showTireTypeCountError").on('blur', function (e) {
                        $(e.target).toggleClass("tire-type-count-errors", e.target.value.length == 0);
                    });
                    $scope.isInitTireCount = false;
                }
                if (angular.isDefined($scope.tireTypeList)) {
                    var validTireTypeList = _.filter($scope.tireTypeList, function (i) { return i.count != null });
                    var contributingTireTypeList = _.filter($scope.tireTypeList, function (i) { return i.count >= 1 });
                    $scope.tireTypeCountInvalid = (validTireTypeList.length == $scope.tireTypeList.length && contributingTireTypeList.length <= 0);

                    if ($scope.isAdjustment()) {
                        var unmatchedTireType = _.find($scope.tireTypeList, function (i) { return i.count != i.originalCount; });
                        $scope.tireTypeCountAdjusted = !_.isUndefined(unmatchedTireType);
                    }
                }
            }, true);

            $scope.$watch('generateTiresObj', function () {
                if (angular.isDefined($scope.generateTires) && angular.isDefined($scope.generateTiresObj)) {
                    $scope.generateTires = $scope.generateTiresObj.val;
                }
            }, true);

            $scope.tireTypeCountInvalidError = function () {
                return $rootScope.submitAttempted && $scope.tireTypeCountInvalid;
            };
        }
    };
}]);

commonTransactionLib.directive('supportingDocumentSelector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            list: "="
        },
        templateUrl: 'supportingDocumentSelector.html',
        controller: function ($rootScope, $scope) {
            $scope.documentUploadRequired = false;
            $scope.$watch('list', function () {
                $scope.documentUploadRequired = $scope.isDocumentUploadRequired();
                $rootScope.$emit('rootscope-broadcast', eventDetail());
            }, true);

            function eventDetail() { return { name: 'supportdoc-changed-broadcast', data: $scope.documentUploadRequired }; };

            $scope.isDocumentUploadRequired = function () {
                var uploadRequiredList = _.filter($scope.list, function (i) {
                    return i.documentFormat == 3 && i.required;
                });
                return uploadRequiredList.length > 0;
            };

        }
    };
}]);

commonTransactionLib.directive('documentUploader', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            transactionId: "@",
            uploadList: "=",
            isRequired: "="
        },
        templateUrl: 'documentUploader.html',
        controller: function ($rootScope, $scope, $http) {
            $scope.onFileSelect = function ($files) {
                if (_.isUndefined(_.find($scope.uploadList, function (i) { return i.name == $files[0].name; }))) {
                    $http.uploadFile({
                        url: GlobalSettingService.UploadTransactionFileServiceURL(),
                        file: $files[0],
                        data: { transactionId: $scope.transactionId }
                    }).success(function (result) {
                        if (result.status) {
                            $scope.uploadList.push(result.data);
                        }
                    }).error(function (error) {
                    });
                }
            };

            $scope.delete = function (doc) {
                $http({
                    url: GlobalSettingService.DeleteTemporaryTransactionFileServiceURL(),
                    method: "POST",
                    data: { attachmentId: doc.attachmentId, transactionId: $scope.transactionId }
                }).success(function (result) {
                    if (result.status) {
                        $scope.uploadList = _.filter($scope.uploadList, function (i) { return i.attachmentId != doc.attachmentId; });
                    }
                }).error(function () {
                });
            };

            $rootScope.$on('clean-up-temporary-document-uploads', function (e, message) {
                if ($scope.uploadList.length > 0) {
                    var attachmentIdList = _.pluck($scope.uploadList, 'attachmentId');

                    $http({
                        url: GlobalSettingService.CleanUpTemporaryTransactionFilesServiceURL(),
                        method: "POST",
                        data: { attachmentIdList: attachmentIdList, transactionId: $scope.transactionId }
                    })
                }
            });

            $scope.isRequiredError = function () {
                return $scope.isRequired && $scope.uploadList.length <= 0;
            };

            $scope.dropZoneStyle = function () {
                if ($scope.isRequiredError())
                    return "dropzone-required";
                else
                    return "dropzone";
            };

            $scope.$on("supportdoc-changed-broadcast", function (e, message) {
                //$scope.isRequired = message;///comment out for May7 release (paitial fix - Y)
            });
        }
    };
}]);

commonTransactionLib.directive('unregisteredCompanyCollector', ['GlobalSettingService', 'ValidationService', function (GlobalSettingService, ValidationService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            companyInfo: "=",
            companyLabelText: "@",
            countryList: "="
        },
        templateUrl: 'unregisteredCompany.html',
        controller: function ($rootScope, $scope) {
            $scope.companyInfoRequired = true;

            $rootScope.$on('set-unregistered-company-requirement', function (e, message) {
                $scope.companyInfoRequired = message.required;

                if (angular.isDefined($scope.companyInfo)) {
                    $scope.companyInfo.companyName = null;
                    $scope.companyInfo.phoneNumber = null;
                    $scope.companyInfo.address1 = null;
                    $scope.companyInfo.address2 = null;
                    $scope.companyInfo.city = null;
                    $scope.companyInfo.province = null;
                    $scope.companyInfo.postalCode = null;
                    $scope.companyInfo.country = "Canada";
                }
            });

            $scope.countryChange = function () {
                if (($scope.companyInfo.country == "Canada" || $scope.companyInfo.country == "United States of America (the)") && !_.isNull($scope.companyInfo.phoneNumber)) {
                    $scope.form.phoneNumber.$setValidity("validFormat", ValidationService.validatePhoneNumber($scope.companyInfo.phoneNumber));
                } else $scope.form.phoneNumber.$setValidity("validFormat", true);

                if ($scope.companyInfo.country == "Canada" && !_.isNull($scope.companyInfo.postalCode)) {
                    $scope.form.postalCode.$setValidity("validFormat", ValidationService.validatePostalCode($scope.companyInfo.postalCode));
                } else if ($scope.companyInfo.country == "United States of America (the)" && !_.isNull($scope.companyInfo.postalCode)) {
                    $scope.form.postalCode.$setValidity("validFormat", ValidationService.validateZipCode($scope.companyInfo.postalCode));
                } else $scope.form.postalCode.$setValidity("validFormat", true);

                $scope.$broadcast('show-errors-event');
            };
        }
    };
}]);

commonTransactionLib.directive('scaleTicketCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            scaleTicket: "=",
            mandatory: "@",
            disable: "@"
        },
        templateUrl: 'scale-ticket.html',
        controller: function ($scope) {
            $scope.isTicketNumberMandatory = function () {
                if ((angular.isDefined($scope.mandatory) && $scope.mandatory == "true") || $scope.scaleTicket.weight > 0) return true;
                else return false;
            };

            $scope.isDisabled = function () {
                if (angular.isDefined($scope.disable) && $scope.disable == "true") return true;
                else return false;
            };

            $scope.weighTypeAvailable = function (type) {
                return _.contains($scope.scaleTicket.weightTypeList, type);
            };

            $scope.weighTypeStyle = function (id, selectionId) {
                if (id == selectionId) return "btn btn-xs btn-success";
                else return "btn btn-xs btn-default";
            };

            $scope.weighTypeLabel = function (selectionId) {
                if (selectionId == 1) return "LBS";
                else if (selectionId == 2) return "KG";
                else if (selectionId == 3) return "TON";
            };
        }
    };
}]);

commonTransactionLib.directive('ptrDisplayVariance', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        templateUrl: 'ptr-variance.html',
        controller: function ($scope) {
            $scope.variance = { valid: false, value: null };

            $scope.$watch('trans', function () {
                if ($scope.transForm.scaleWeight.$valid && !$scope.isTireTypeCountMissing()) {
                    var estimatedWeight = 0.00;
                    for (var i = 0; i < $scope.trans.tireTypeList.length; i++) {
                        estimatedWeight += $scope.trans.tireTypeList[i].count * $scope.trans.tireTypeList[i].standardWeight
                    }

                    if (estimatedWeight > 0) {
                        $scope.variance.valid = true;
                        if ($scope.trans.scaleTicket.weightType == 2) {
                            $scope.variance.value = (($scope.trans.scaleTicket.weight - estimatedWeight) / estimatedWeight) * 100;
                        }

                        if ($scope.trans.scaleTicket.weightType == 1) {
                            $scope.variance.value = ((GlobalSettingService.ConvertLbToKg($scope.trans.scaleTicket.weight) - estimatedWeight) / estimatedWeight) * 100;
                        }

                        if ($scope.trans.scaleTicket.weightType == 3) {
                            $scope.variance.value = ((GlobalSettingService.ConvertTonToKg($scope.trans.scaleTicket.weight) - estimatedWeight) / estimatedWeight) * 100;
                        }
                    } else {
                        $scope.variance.valid = false;
                        $scope.variance.value = null;
                    }
                } else {
                    $scope.variance.valid = false;
                    $scope.variance.value = null;
                }
            }, true);

            $scope.varianceDisplayStyle = function () {
                return $scope.variance.value >= 15.00 ? "text-danger bold" : "";
            };

            $scope.isTireTypeCountMissing = function () {
                for (var i = 0; i < $scope.trans.tireTypeList.length; i++) {
                    if ($scope.trans.tireTypeList[i].count === undefined || $scope.trans.tireTypeList[i].count == null) return true;
                }

                return false;
            };
        }
    };
}]);

commonTransactionLib.directive('invoiceNumberCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            invoiceNumber: "="
        },
        templateUrl: 'invoice-number.html',
        controller: function ($scope) {
        }
    };
}]);

commonTransactionLib.directive('internalNoteCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            internalNote: "=",
            adjust: "@",
            label: "@"
        },
        templateUrl: 'internalNote.html',
        controller: function ($scope) {
            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };
        }
    };
}]);

commonTransactionLib.directive('dotScaleTicketHandler', function () {
    return {
        restrict: 'A',
        controller: function ($scope) {
            $scope.$watch('trans.scaleTicket', function () {
                if (angular.isDefined($scope.trans.scaleTicket)) {
                    var supportingDocument = _.find($scope.trans.supportingDocumentList, function (i) { return i.documentName == "Scale Ticket"; });
                    supportingDocument.required = $scope.trans.scaleTicket.ticketNumber != null && $scope.trans.scaleTicket.ticketNumber.length > 0; //make sure passing boolean to backend other than null (otherwise, backend ModelState.IsValid check will return false
                }
            }, true);
        }
    };
});

commonTransactionLib.directive('dorMaterialTypeCollector', [function () {
    return {
        restrict: 'E',
        scope: {
            materialTypeId: "=",
            disable: "@"
        },
        templateUrl: 'dor-material-type.html',
        controller: function ($scope) {
            $scope.isDisabled = function () {
                if (angular.isDefined($scope.disable) && $scope.disable == "true") return true;
                else return false;
            };

            $scope.nameId = function () {
                return $scope.isDisabled() ? "materialTypeDisabled" : "materialType";
            };
        }
    };
}]);

commonTransactionLib.directive('dorDispositionReasonCollector', [function () {
    return {
        restrict: 'E',
        scope: {
            dispositionReasonId: "=",
            materialTypeId: "=",
            readOnly: "@"
        },
        templateUrl: 'dor-disposition-reason.html',
        controller: function ($rootScope, $scope) {
            $scope.isReadOnly = function () {
                if (angular.isDefined($scope.readOnly) && $scope.readOnly == "true") return true;
                else return false;
            };
            $scope.nameId = function () {
                return $scope.isReadOnly() ? "DispositionReasonDisabled" : "DispositionReason";
            };

            $scope.disbaleReuseResale = true;
            $scope.disbaleRecycling = true;
            $scope.disbaleEnergy = true;
            $scope.disbaleLandfill = true;

            $scope.$watch('materialTypeId', function () {
                if (angular.isDefined($scope.materialTypeId)) {
                    if (!$scope.isReadOnly()) {
                        //$scope.dispositionReasonId = null;
                        $scope.disbaleReuseResale = !($scope.materialTypeId == 4 || $scope.materialTypeId == 5); //1
                        if ($scope.disbaleReuseResale && $scope.dispositionReasonId == 1)
                            $scope.dispositionReasonId = null;
                        $scope.disbaleRecycling = !($scope.materialTypeId == 2 || $scope.materialTypeId == 3 || $scope.materialTypeId == 4); //2
                        if ($scope.disbaleRecycling && $scope.dispositionReasonId == 2)
                            $scope.dispositionReasonId = null;
                        $scope.disbaleEnergy = !($scope.materialTypeId == 3); //3
                        if ($scope.disbaleEnergy && $scope.dispositionReasonId == 3)
                            $scope.dispositionReasonId = null;
                        $scope.disbaleLandfill = !($scope.materialTypeId == 1 || $scope.materialTypeId == 2 || $scope.materialTypeId == 3 || $scope.materialTypeId == 4); //4
                        if ($scope.disbaleLandfill && $scope.dispositionReasonId == 4)
                            $scope.dispositionReasonId = null;
                    }
                }
            }, true);

            $scope.dispositionReasonIdError = function () {
                if ($scope.isReadOnly()) return false;

                return ($rootScope.submitAttempted && $scope.dispositionReasonId == null);
            };
        }
    };
}]);

commonTransactionLib.directive('unitTypeWeight', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            vm: "=",
            mandatory: "@",
            disable: "@"
        },
        templateUrl: 'unit-weight.html',
        controller: function ($scope) {
            $scope.isDisabled = function () {
                if (angular.isDefined($scope.disable) && $scope.disable == "true") return true;
                else return false;
            };

            $scope.weighTypeStyle = function (id, selectionId) {
                if (id == selectionId) return "btn btn-xs btn-success";
                else return "btn btn-xs btn-default";
            };

            $scope.weighTypeLabel = function (selectionId) {
                if (selectionId == 1) return "LBS";
                else if (selectionId == 2) return "KG";
                else if (selectionId == 3) return "TON";
            };
        }
    };
}]);
/* End of Paper Form Components */


/* Start of Validation Related Components */
commonTransactionLib.directive('validateFormNumber', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validFormat = function (modelValue) {
                if (attr.validateFormNumber == "") return true;

                if (attr.validateFormNumber == "TCR" && /^[1][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "DOT" && /^[2][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "STC" && /^[5][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "UCR" && /^[6][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "HIT" && /^[3][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "RTR" && /^[4][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "PTR" && /^[7][0-9]{7,}$/.test(modelValue)) return true;
                if (attr.validateFormNumber == "PIT" && /^[8][0-9]{7,}$/.test(modelValue)) return true;

                return false;
            };
        }
    };
}]);

commonTransactionLib.directive('validScaleWeight', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            inboundWeight: '=',
            outboundWeight: '=',          
        },
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 13;
            var scale = (angular.isDefined(attr.scale) && attr.scale != "") ? parseInt(attr.scale) : 4;

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9\.]/g, '');
                var decimalCheck = clean.split('.');

                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                } else clean = decimalCheck[0];

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }

                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
            
            //OTSTM2-1235 The validation about the business rule: inbound > outbound
            element.bind('focusout', function (event) {
                var inboundWeight = parseFloat(scope.inboundWeight);
                var outboundWeight = parseFloat(scope.outboundWeight);
                if (outboundWeight >= inboundWeight) {
                    $('#GrossTareBusinessRuleErrorMessage').css("display", "block");
                    $('#GrossTareBusinessRuleErrorMessage').addClass("required-validity");
                    $('[name="singleScaleInboundWeight"]').addClass("has-error");
                    $('[name="singleScaleOutboundWeight"]').addClass("has-error");
                }
                else {
                    $('#GrossTareBusinessRuleErrorMessage').css("display", "none");
                    $('#GrossTareBusinessRuleErrorMessage').removeClass("required-validity");
                    $('[name="singleScaleInboundWeight"]').removeClass("has-error");
                    $('[name="singleScaleOutboundWeight"]').removeClass("has-error");
                }
            });
        }       
    };
}]);

commonTransactionLib.directive('validateEventNumber', ['GlobalSettingService', 'ValidationService', function (GlobalSettingService, ValidationService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validFormat = function (modelValue) {
                if (ValidationService.validateEventNumber(modelValue)) return true;

                return false;
            };
        }
    };
}]);

commonTransactionLib.directive('validatePhoneNumber', ['GlobalSettingService', 'ValidationService', function (GlobalSettingService, ValidationService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validFormat = function (modelValue) {
                if ((attr.validatePhoneNumber == "Canada" || attr.validatePhoneNumber == "United States of America (the)") && !_.isEmpty(modelValue)) {
                    return ValidationService.validatePhoneNumber(modelValue);
                }

                return true;
            };
        }
    };
}]);

commonTransactionLib.directive('validatePostalCode', ['GlobalSettingService', 'ValidationService', function (GlobalSettingService, ValidationService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validFormat = function (modelValue) {
                if (attr.validatePostalCode == "Canada" && !_.isEmpty(modelValue)) {
                    return ValidationService.validatePostalCode(modelValue);
                }

                if (attr.validatePostalCode == "United States of America (the)" && !_.isEmpty(modelValue)) {
                    return ValidationService.validateZipCode(modelValue);
                }

                return true;
            };
        }
    };
}]);
/* End of Validation Related Components */


/* Start of Review ToolBar Related Components */
commonTransactionLib.directive('transactionHandler', ['$window', 'GlobalSettingService', function ($window, GlobalSettingService) {
    return {
        restrict: "A",
        link: function (scope, element) {
            element.click(function (e) {
                if ((typeof _cancelMouseClick !== "undefined") && (_cancelMouseClick)) {
                    _cancelMouseClick = false;
                    return;
                }
                if (e.target.nodeName == "TD" || e.target.nodeName == "TR") { //OTSTM2-897 trigger auto click event will have nodeName "TR"
                    //OTSTM2-573 scope.table is initialized in transaction-generic-list after every draw of table
                    if (typeof $(e.target).closest("tr").attr("transaction-id") !== "undefined") scope.open($(e.target).closest("tr").attr("transaction-id"), 'view', function () { scope.table.currTable.search(scope.table.searchVal).draw(false); });
                }
            });
        },
        controller: function ($rootScope, $scope, $http, $uibModal) {
            $scope.open = function (transactionId, requestedMode, done) {
                $http({
                    url: GlobalSettingService.GetGetTransactionHandlerClaimDataServiceURL(),
                    method: "POST",
                    data: { claimId: Global.Transaction.ClaimId }
                }).success(function (result) {
                    if (result.status) {
                        var transactionHandlerModal = $uibModal.open({
                            templateUrl: 'transaction-handler.html',
                            controller: 'TransactionHandlerModelCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            keyboard: false,
                            resolve: {
                                handlerConfig: function () {
                                    return {
                                        vendorType: Global.Transaction.VendorCode,
                                        claimId: Global.Transaction.ClaimId,
                                        transactionId: transactionId,
                                        transactionAdjustmentId: null,
                                        requestedMode: requestedMode,
                                        periodName: result.data.periodName
                                    };
                                }
                            }
                        });

                        transactionHandlerModal.result.then(function (refresh) {
                            if (refresh) {
                                if (done) {
                                    done(); //OTSTM2-573
                                }
                            }
                        });
                    } else {
                    }
                }).error(function () {
                });
            }
        }
    };
}]);

commonTransactionLib.directive('transactionAdjustmentHandler', ['$window', 'GlobalSettingService', function ($window, GlobalSettingService) {
    return {
        restrict: "A",
        link: function (scope, element) {
            //element.click(function (e) {
            //    scope.openMe($(e.target).closest("tr").attr("transaction-id"), $(e.target).closest("tr").attr("transaction-adjustment-id"), 'view');
            //});
        },
        controller: function ($rootScope, $scope, $http, $uibModal) {
            $scope.openMe = function (transactionId, transactionAdjustmentId, requestedMode, done) {
                $http({
                    url: GlobalSettingService.GetGetTransactionHandlerClaimDataServiceURL(),
                    method: "POST",
                    data: { claimId: Global.Transaction.ClaimId }
                }).success(function (result) {
                    if (result.status) {
                        var transactionHandlerModal = $uibModal.open({
                            templateUrl: 'transaction-handler.html',
                            controller: 'TransactionHandlerModelCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            keyboard: false,
                            resolve: {
                                handlerConfig: function () {
                                    return {
                                        vendorType: Global.Transaction.VendorCode,
                                        claimId: Global.Transaction.ClaimId,
                                        transactionId: transactionId,
                                        transactionAdjustmentId: transactionAdjustmentId,
                                        requestedMode: requestedMode,
                                        periodName: result.data.periodName
                                    };
                                }
                            }
                        });

                        transactionHandlerModal.result.then(function (refresh) {
                            //if (refresh) $window.location.reload();
                            if (refresh) {
                                if (done) {
                                    done(); //OTSTM2-573
                                }
                            }
                        });
                    } else {
                    }
                }).error(function () {
                });
            }
        }
    };
}]);

commonTransactionLib.directive('transactionReviewToolbar', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            ds: "="
        },
        templateUrl: 'transaction-review-toolbar.html',
        controller: function ($rootScope, $scope, $http) {

            $scope.initialized = false;

            $scope.list = [];
            $scope.currentTransaction = null;
            //$scope.currentIndex = 0;

            $scope.$watch('ds.getReviewToolbarSetting()', function () {

                $scope.reviewToolbarSetting = $scope.ds.getReviewToolbarSetting();

                if ($scope.reviewToolbarSetting != null) {
                    $http({
                        url: GlobalSettingService.GetTransactionSearchListServiceURL(),
                        method: "POST",
                        data: {
                            claimId: $scope.ds.getClaimId(),
                            transactionFormat: $scope.reviewToolbarSetting.transactionFormat,
                            processingStatus: $scope.reviewToolbarSetting.processingStatus,
                            transactionType: $scope.reviewToolbarSetting.transactionType
                        }
                    }).success(function (result) {
                        if (result.status) {
                            $scope.list = result.data;
                            $scope.ds.setReviewToolBarAsExists(true);

                            if (!$scope.initialized) $scope.initialized = true;
                            else {
                                if ($scope.list.length > 0) {
                                    $scope.ds.setCurrentTransactionId($scope.list[0].id);
                                } else {
                                    $scope.list = [];
                                }
                            }

                            $scope.currentTransaction = _.find($scope.list, function (i) { return i.id == $scope.ds.getCurrentTransactionId(); });
                        }
                    }).error(function () {
                    });
                }
            }, true);

            $scope.next = function () {
                if ($scope.disableNext()) return;

                $scope.currentTransaction = $scope.getNextTransaction();
                $scope.ds.setCurrentTransactionId($scope.currentTransaction.id);
            };

            $scope.previous = function () {
                if ($scope.disablePrevious()) return;

                $scope.currentTransaction = $scope.getPreviousTransaction();
                $scope.ds.setCurrentTransactionId($scope.currentTransaction.id);
            };

            $scope.adjust = function () {
                if ($scope.disableAdjust()) return;

                $scope.ds.setRequestedMode('adjust')
            };

            $scope.updateStatus = function (status, navigateTo) {
                if (status == 'invalidate' && $scope.disableInvalidate()) return;
                if (status == 'approve' && $scope.disableApprove()) return;

                $http({
                    url: GlobalSettingService.GetUpdateTransactionProcessingStatusServiceURL(),
                    method: "POST",
                    data: {
                        transactionId: $scope.ds.getCurrentTransactionId(),
                        status: status,
                        currentStatus: $scope.currentTransaction.processingStatus
                    }
                }).success(function (result) {
                    if (result.status) {
                        $scope.currentTransaction.processingStatus = result.data.newStatus;
                        //OTSTM2-765 refresh transaction internal note area
                        if (result.data.newStatus == "Invalidated") {
                            $scope.$parent.$emit('TRANSACTION_NOTE', result);
                        }

                        if (navigateTo == 'previous') $scope.previous();
                        if (navigateTo == 'next') $scope.next();
                    }
                }).error(function () {
                });
            };

            $scope.inAdjustMode = function () { return $scope.ds.getRequestedMode() == 'adjust'; };
            $scope.isListEmpty = function () { return $scope.list.length <= 0; };

            $scope.disablePrevious = function () {
                return $scope.getPreviousTransaction() == null || $scope.inAdjustMode() || $scope.isListEmpty();
            };
            $scope.disableNext = function () {
                return $scope.getNextTransaction() == null || $scope.inAdjustMode() || $scope.isListEmpty();
            };
            $scope.disableAdjust = function () {
                return $scope.inAdjustMode() || $scope.isListEmpty() || !$scope.ds.isAdjustmentAllowed() || $scope.currentTransaction.processingStatus != "Unreviewed";
            };
            $scope.disableInvalidate = function () {
                return $scope.inAdjustMode() || $scope.isListEmpty() || $scope.currentTransaction.processingStatus == "Approved" || $scope.isDisableApproveInvalidate();
            };
            $scope.disableApprove = function () {
                return $scope.inAdjustMode() || $scope.isListEmpty() || $scope.currentTransaction.processingStatus == "Invalidated" || $scope.isDisableApproveInvalidate();
            };

            //OTSTM2-660
            $scope.getNextTransaction = function () {
                if ($scope.list.length > 0) {

                    var currentIndex = $scope.getIndex($scope.list, $scope.currentTransaction);

                    if (currentIndex == -1 || currentIndex === $scope.list.length - 1) {
                        return null;
                    }
                    currentIndex++;

                    return $scope.list[currentIndex];
                }
            }

            $scope.getPreviousTransaction = function () {
                if ($scope.list.length > 0) {

                    var currentIndex = $scope.getIndex($scope.list, $scope.currentTransaction);

                    if (currentIndex <= 0) {
                        return null;
                    }
                    currentIndex--;

                    return $scope.list[currentIndex];
                }
            }

            $scope.isDisableApproveInvalidate = function () {
                var reviewToolBar = $scope.ds.getTransactionReviewToolBar()
                //reviewtool bar is only accessible when claim is under review else it is null
                if (reviewToolBar) {
                    return ((reviewToolBar.hasPendingAdjustments && $scope.currentTransaction.processingStatus === "Unreviewed") || reviewToolBar.isOtherClaimApproved)
                }
                return false;
            };

            $scope.getIndex = function (arr, val) {
                var allIds = arr.map(function (curr) {
                    return curr.id
                })
                return allIds.indexOf(val.id)
            }
        }
    };
}]);
/* End of Review ToolBar Related Components */


/* Start of Transaction Adjustment */
commonTransactionLib.directive('adjustmentToggle', function () {
    return {
        restrict: 'E',
        scope: {
            preview: "="
        },
        templateUrl: 'adjustment-toggle.html',
        controller: function ($rootScope, $scope, $http) {
        }
    };
});


commonTransactionLib.directive('tcrPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'tcr-paper-adjustment.html'
    };
});

commonTransactionLib.directive('dotPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dot-paper-adjustment.html'
    };
});

commonTransactionLib.directive('hitPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'hit-paper-adjustment.html'
    };
});

commonTransactionLib.directive('ptrPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ptr-paper-adjustment.html'
    };
});

commonTransactionLib.directive('stcPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'stc-paper-adjustment.html'
    };
});

commonTransactionLib.directive('ucrPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ucr-paper-adjustment.html'
    };
});

commonTransactionLib.directive('rtrPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'rtr-paper-adjustment.html'
    };
});

commonTransactionLib.directive('pitPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'pit-paper-adjustment.html'
    };
});

commonTransactionLib.directive('spsPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sps-paper-adjustment.html'
    };
});

commonTransactionLib.directive('sirPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sir-paper-adjustment.html'
    };
});

commonTransactionLib.directive('spsrPaperAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'spsr-paper-adjustment.html'
    };
});

commonTransactionLib.directive('dorPaperAdjustment', function () {
    return {
        restrict: 'E',
        templateUrl: 'dor-paper-adjustment.html',
        controller: function ($scope, $http) {
            $scope.tireRimsRequired = function () {
                return $scope.vm.adjModel.productDetail.materialTypeId == 4;
            };

            $scope.usedTireSaleRequired = function () {
                return $scope.vm.adjModel.productDetail.materialTypeId == 5;
            };
        }
    };
});


commonTransactionLib.directive('tcrMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'tcr-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('dotMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dot-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('hitMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'hit-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('ptrMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ptr-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('stcMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'stc-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('ucrMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ucr-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('rtrMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'rtr-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('pitMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'pit-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('spsMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sps-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('sirMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sir-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('spsrMobileAdjustment', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'spsr-mobile-adjustment.html'
    };
});

commonTransactionLib.directive('dorMobileAdjustment', function () {
    return {
        restrict: 'E',
        templateUrl: 'dor-mobile-adjustment.html'
    };
});


commonTransactionLib.directive('supportingDocumentAttachmentAdjust', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            list: "="
        },
        templateUrl: 'supporting-document-attachment-adjust.html',
        controller: function ($scope) {
            $scope.loaded = false;
            $scope.currentSupportingDocument = null;

            $scope.$watch('list', function () {
                if (angular.isDefined($scope.list)) {
                    $scope.currentSupportingDocument = $scope.list[0];
                    $scope.loaded = true;
                }
            }, true);

            $scope.viewSupportingDocument = function (sd) {
                $scope.currentSupportingDocument = sd;
            };

            $scope.docStyle = function (doc) {
                if (doc.isRemoved) return "smoothzoompanPhotoThumb sd-attachment-highlight";
                else return "smoothzoompanPhotoThumb";
            };
        }
    };
}]);
/* End of Transaction Adjustment */


/* Start of Paper Transaction Views */
commonTransactionLib.directive('tcrPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'tcr-paper-view.html'
    };
});

commonTransactionLib.directive('dotPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dot-paper-view.html'
    };
});

commonTransactionLib.directive('ucrPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ucr-paper-view.html'
    };
});

commonTransactionLib.directive('hitPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'hit-paper-view.html'
    };
});

commonTransactionLib.directive('stcPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'stc-paper-view.html'
    };
});

commonTransactionLib.directive('ptrPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ptr-paper-view.html',
        controller: function ($scope) {
            $scope.varianceDisplayStyle = function () {
                return $scope.vm.variance >= 15.00 ? "text-danger bold" : "";
            };
        }
    };
});

commonTransactionLib.directive('rtrPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'rtr-paper-view.html'
    };
});

commonTransactionLib.directive('sirPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sir-paper-view.html'
    };
});

commonTransactionLib.directive('spsrPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'spsr-paper-view.html'
    };
});

commonTransactionLib.directive('spsPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sps-paper-view.html'
    };
});

commonTransactionLib.directive('pitPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'pit-paper-view.html'
    };
});

commonTransactionLib.directive('dorPaperView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dor-paper-view.html'
    };
});
/* Start of Transaction Views */


/* Start of Mobile Transaction Views */
commonTransactionLib.directive('tcrMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'tcr-mobile-view.html'
    };
});

commonTransactionLib.directive('mobileGpsHeader', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'mobile-gps-header.html',
        controller: function ($scope, $uibModal) {
            $scope.viewMapDetail = function () {
                var mapModal = $uibModal.open({
                    templateUrl: 'mobile-geo-map-modal.html',
                    controller: 'MapModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        gps: function () { return $scope.vm.mobileTransactionGPS; },
                        postal: function () { return $scope.vm.inboundParticipant.postalCode; }
                    }
                });

                mapModal.result.then(function () {
                    $window.location.reload();
                });
            };
        }
    };
});

commonTransactionLib.directive('dotMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dot-mobile-view.html'
    };
});

commonTransactionLib.directive('ucrMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ucr-mobile-view.html'
    };
});

commonTransactionLib.directive('hitMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'hit-mobile-view.html'
    };
});

commonTransactionLib.directive('stcMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'stc-mobile-view.html'
    };
});

commonTransactionLib.directive('ptrMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'ptr-mobile-view.html'
    };
});

commonTransactionLib.directive('rtrMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'rtr-mobile-view.html'
    };
});

commonTransactionLib.directive('sirMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sir-mobile-view.html'
    };
});

commonTransactionLib.directive('spsrMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'spsr-mobile-view.html'
    };
});

commonTransactionLib.directive('spsMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'sps-mobile-view.html'
    };
});

commonTransactionLib.directive('pitMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'pit-mobile-view.html'
    };
});

commonTransactionLib.directive('dorMobileView', function () {
    return {
        restrict: 'E',
        scope: {
            vm: "="
        },
        templateUrl: 'dor-mobile-view.html'
    };
});
/* Start of Mobile Transaction Views */


/* Start of View Components */
commonTransactionLib.directive('transactionHandlerHeaderView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        templateUrl: 'transaction-handler-header.html',
        controller: function ($scope) {
            $scope.modalHeaderStyle = function (isMobile) {
                if (isMobile) return "modal-header modal-header-le";
                else return "modal-header";
            };
        }
    };
}]);

commonTransactionLib.directive('transactionHandlerWorkflow', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        templateUrl: 'transaction-handler-workflow.html',
        controller: function ($scope) {
        }
    };
}]);

commonTransactionLib.directive('adjustmentDetailView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            adjustmentDetail: "=",
            displayOriginal: "="
        },
        templateUrl: 'adjustment-detail.html',
        controller: function ($scope) {
            $scope.statusStyle = function () {
                if ($scope.adjustmentDetail) {
                    if ($scope.adjustmentDetail.status == "Pending") return "panel-table-status color-tm-orange-bg";
                    if ($scope.adjustmentDetail.status == "Accepted") return "panel-table-status color-tm-green-bg";
                    if ($scope.adjustmentDetail.status == "Rejected" || $scope.adjustmentDetail.status == "SystemRejected") return "panel-table-status color-tm-red-bg";
                    if ($scope.adjustmentDetail.status == "Recalled") return "panel-table-status color-tm-gray-bg";
                }
            };
        }
    };
}]);

commonTransactionLib.directive('tireTypeCountView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            tireTypeList: "=",
            generateTires: "="
        },
        templateUrl: 'tire-type-count-view.html',
        controller: function ($scope) {
            $scope.tireTypeAvailable = function (type) {
                var tireType = _.find($scope.tireTypeList, function (i) { return i.tireType == type; });
                return !_.isUndefined(tireType);
            };

            $scope.tireTypeByType = function (type) {
                return _.find($scope.tireTypeList, function (i) { return i.tireType == type; });
            };

            $scope.displayGenerateTires = function () {
                return angular.isDefined($scope.generateTires);
            };

            $scope.changeAlertStyle = function (type) {
                var tireType = _.find($scope.tireTypeList, function (i) { return i.tireType == type; });

                if (!_.isUndefined(tireType) && tireType.changed) return "form-control-static text-danger bold";
                else return "form-control-static";
            };
        }
    };
}]);

commonTransactionLib.directive('dateDisplayView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            label: "@",
            date: "=",
            format: "@"
        },
        templateUrl: 'date-display-view.html',
        controller: function ($scope) {
        }
    };
}]);

commonTransactionLib.directive('dateDisplayBoldView', ['$compile', '$filter', function ($compile, $filter) {
    return {
        restrict: 'E',
        scope: {
            label: "@",
            date: "=",
            format: "@"
        },
        link: function (scope, element, attrs) {
            var html = [];
            html.push('<label><strong>' + scope.label + '</strong></label>');
            html.push('<p class="margin-top--3">' + $filter('date')(scope.date, scope.format) + '</p>');
            element.html(html.join(''));
            $compile(element.contents())(scope);
        }
    }
}]);

commonTransactionLib.directive('participantInfoView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            info: "="
        },
        templateUrl: 'participant-info-view.html',
        controller: function ($scope, $uibModal) {
            $scope.viewMapDetail = function () {
                var mapModal = $uibModal.open({
                    templateUrl: 'geo-map-modal.html',
                    controller: 'MapModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        gps: function () { return $scope.info.gps; },
                        postal: function () { return $scope.info.postalCode; }
                    }
                });

                mapModal.result.then(function () {
                    $window.location.reload();
                });
            };
        }
    };
}]);

commonTransactionLib.directive('supportingDocumentView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            supportingDocumentDetail: "="
        },
        templateUrl: 'supporting-document-view.html',
        controller: function ($scope) {
            $scope.loaded = false;
            $scope.currentSupportingDocument = null;

            $scope.$watch('supportingDocumentDetail', function () {
                if (angular.isDefined($scope.supportingDocumentDetail)) {
                    $scope.currentSupportingDocument = $scope.supportingDocumentDetail.documentList[0];
                    $scope.loaded = true;
                }
            }, true);

            $scope.viewSupportingDocument = function (sd) {
                $scope.currentSupportingDocument = sd;
            };
        }
    };
}]);

commonTransactionLib.directive('photoView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            photoList: "="
        },
        templateUrl: 'photo-view.html',
        controller: function ($scope) {
            $scope.previewDomainName = GlobalSettingService.MobilePhotoPreviewDomainName();

            $scope.loaded = false;
            $scope.currentPhoto = null;
            $scope.currentPhotoSrc = null;

            $scope.$watch('photoList', function () {
                if (angular.isDefined($scope.photoList) && $scope.photoList.length > 0) {
                    $scope.currentPhoto = $scope.photoList[0];
                    $scope.currentPhotoSrc = $scope.previewDomainName + $scope.currentPhoto.src;
                    $scope.loaded = true;
                }
            }, true);

            $scope.viewPhoto = function (p) {
                $scope.currentPhoto = p;
                $scope.currentPhotoSrc = $scope.previewDomainName + $scope.currentPhoto.src;
            };
        }
    };
}]);

commonTransactionLib.directive('scaleTicketView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            scaleTicketModel: "=",
            adjust: "@"
        },
        templateUrl: 'scale-ticket-view.html',
        controller: function ($scope) {

            $scope.previewDomainName = GlobalSettingService.MobilePhotoPreviewDomainName();

            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.inboundTicket = null;
            $scope.outboundTicket = null;
            $scope.$watch('scaleTicketModel', function () {
                if (angular.isDefined($scope.scaleTicketModel)) {
                    if (!$scope.scaleTicketModel.singleTicketType) {
                        $scope.inboundTicket = _.find($scope.scaleTicketModel.tickets, function (i) { return i.ticketType == 1; });
                        $scope.outboundTicket = _.find($scope.scaleTicketModel.tickets, function (i) { return i.ticketType == 2; });
                    }
                }
            }, true);
        }
    };
}]);

commonTransactionLib.directive('paperScaleTicketView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            scaleTicketModel: "=",
            unit: "@"
        },
        templateUrl: 'paper-scale-ticket-view.html',
        controller: function ($scope) {
            if (!angular.isDefined($scope.unit)) {
                $scope.unit = "KG";
            }
        }
    };
}]);

commonTransactionLib.directive('commentView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            commentList: "="
        },
        templateUrl: 'comment-view.html',
        controller: function ($scope) {
        }
    };
}]);

commonTransactionLib.directive('signatureView', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            signature: "="
        },
        templateUrl: 'signature-view.html',
        controller: function ($scope) {
            $scope.previewDomainName = GlobalSettingService.MobilePhotoPreviewDomainName();
        }
    };
}]);

commonTransactionLib.directive('ptrAmountCalculationDetailView', [function () {
    return {
        restrict: 'A',
        scope: {
            itemList: "="
        },
        link: function (scope, element, attrs) {
            if (angular.isDefined(scope.itemList)) {
                var detail = "<table class='table'><thead><tr><th class='txt-r'></th>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].name + "</th>"
                detail += "</tr></thead><tbody>";

                detail += "<tr class='txt-r'><td><strong>Est. Weight</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].estWeight + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Scale Weight&nbsp;(kg)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].scaleWeight + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Rate&nbsp;($/kg)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].rate + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Amount ($)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].amount.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + "</th>" //OTSTM2-423
                detail += "</tr>";

                detail += "</table>";

                var settings = {
                    trigger: 'hover',
                    title: 'Detailed Calculation',
                    multi: true,
                    closeable: true,
                    style: '',
                    delay: { show: 300, hide: 800 },
                    padding: true
                };
                var materialPopoverOneSettings = { content: detail, width: 700 };
                angular.element(element).webuiPopover('destroy').webuiPopover($.extend({}, settings, materialPopoverOneSettings));
            }
        }
    };
}]);

commonTransactionLib.directive('dorAmountCalculationDetailView', [function () {
    return {
        restrict: 'A',
        scope: {
            itemList: "="
        },
        link: function (scope, element, attrs) {
            if (angular.isDefined(scope.itemList)) {
                var detail = "<table class='table'><thead><tr><th class='txt-r'></th>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].name + "</th>"
                detail += "</tr></thead><tbody>";

                detail += "<tr class='txt-r'><td><strong>Est. Weight</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].estWeight + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Scale Weight&nbsp;(t)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].scaleWeight + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Rate&nbsp;($/t)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].rate + "</th>"
                detail += "</tr>";

                detail += "<tr class='txt-r'><td><strong>Amount ($)</strong></td>";
                for (var i = 0; i < scope.itemList.length; i++) detail += "<th class='txt-r'>" + scope.itemList[i].amount.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + "</th>" //OTSTM2-423
                detail += "</tr>";

                detail += "</table>";

                var settings = {
                    trigger: 'hover',
                    title: 'Detailed Calculation',
                    multi: true,
                    closeable: true,
                    style: '',
                    delay: { show: 300, hide: 800 },
                    padding: true
                };
                var materialPopoverOneSettings = { content: detail, width: 700 };
                angular.element(element).webuiPopover('destroy').webuiPopover($.extend({}, settings, materialPopoverOneSettings));
            }
        }
    };
}]);

commonTransactionLib.directive('pitProductDetailView', [function () {
    return {
        restrict: 'E',
        scope: {
            productDetail: "=",
            direction: "="
        },
        templateUrl: 'pit-product-detail-view.html'
    };
}]);

commonTransactionLib.directive('dorTireRimDetailView', [function () {
    return {
        restrict: 'E',
        scope: {
            tireRimDetail: "="
        },
        templateUrl: 'dor-tire-rim-detail-view.html'
    };
}]);

commonTransactionLib.directive('dorUsedTireSaleDetailView', [function () {
    return {
        restrict: 'E',
        scope: {
            usedTireSaleDetail: "="
        },
        templateUrl: 'dor-used-tire-sale-detail-view.html'
    };
}]);

commonTransactionLib.directive('manageInternalNote', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            internalNoteModel: "="
        },
        templateUrl: 'manage-internal-notes.html',
        controller: function ($rootScope, $scope, $http) {
            $scope.quantity = 2;
            $scope.showMore = true;
            $scope.sortType = 'addedOn';
            $scope.sortReverse = true;
            $scope.filterText = "";
            $scope.disableBtnAdd = (Global.Transaction.AccessPermission != "EditSave" && Global.Transaction.AccessPermission != "True"); //OTSTM2-1304

            $scope.addNote = function () {
                if ($scope.internalNoteModel.newNote.trim().length > 0) {
                    $http({
                        url: '/System/Transactions/AddTransactionNote',
                        method: "POST",
                        data: { id: $scope.internalNoteModel.transactionId, note: $scope.internalNoteModel.newNote.trim() }
                    }).success(function (result) {
                        if (result.status) {
                            $scope.internalNoteModel.newNote = "";
                            $scope.internalNoteModel.noteList.push(result.data);
                            $rootScope.$emit('internal-note-added');
                        }
                    }).error(function () {
                    });
                }
            };

            $scope.getShortNote = function (note) {
                if (note.length > 50) return note.substring(0, 49) + " ...";
                else return note;
            };

            $scope.popoverRequired = function (note) {
                return (note.length > 50);
            };

            //OTSTM2-765 refresh transaction internal note
            $rootScope.$on('TRANSACTION_NOTE', function (e, data) {
                $http({
                    url: '/System/Transactions/GetAllTransactionNote',
                    method: "POST",
                    data: { id: $scope.internalNoteModel.transactionId }
                }).success(function (result) {
                    if (result.status) {
                        $scope.internalNoteModel.noteList = result.data;
                    }
                }).error(function () {
                });
            });
        }
    };
}]);

commonTransactionLib.directive('detailedNoteView', [function () {
    return {
        restrict: 'A',
        scope: {
            note: "="
        },
        link: function (scope, element, attrs) {
            if (attrs.detailedNoteView == "false") return;

            var detail = "<div style='overflow: auto; height: 250px; padding: 3px;'>";
            detail += toHtml(scope.note);
            detail += "</div>";

            var settings = {
                trigger: 'hover',
                title: 'Note',
                multi: true,
                closeable: true,
                style: '',
                delay: { show: 300, hide: 500 },
                padding: false
            };
            var popoverSettings = { content: detail, width: 600 };
            angular.element(element).webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverSettings));

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }
        }
    };
}]);

/* End of View Components */



// Controllers Section
commonTransactionLib.controller('PaperFormModalCtrl', ['$rootScope', '$scope', '$http', '$uibModal', '$uibModalInstance', 'GlobalSettingService', 'paperFormModel', function ($rootScope, $scope, $http, $uibModal, $uibModalInstance, GlobalSettingService, paperFormModel) {
    $rootScope.submitAttempted = false;
    $scope.statusModel = { display: false, errorType: null };
    $scope.trans = paperFormModel;
    $scope.documentUploadRequired = false;

    $scope.submitPaperForm = function () {
        if (!$rootScope.submitAttempted) $rootScope.submitAttempted = true;
        $scope.documentUploadRequired = $scope.isDocumentUploadRequired();

        $scope.$broadcast('show-errors-event');

        if ($scope.transForm.$invalid || $scope.documentUploadInvalid() || $scope.tireTypeCountInvalid()) return;

        //OTSTM2-1214 add event number validation check condition
        if ($scope.trans.formType == "STC") {
            $http({
                url: GlobalSettingService.EventNumberValidationCheck(),
                method: "POST",
                data: {
                    eventNumber: $scope.trans.eventNumber,
                    transactionStartDate: $scope.trans.formDate.on
                }
            }).success(function (result) {

                $('[name="eventNumber"]').siblings(".event-number-validity").removeClass("display-none");

                switch (result.status) {
                    case 0:
                        $scope.eventNumberValidationCondition = 'Pass';
                        $scope.openConfirmation('submit-paper-form', 'submitConfirm.html');
                        break;
                    case 1:
                        $scope.eventNumberValidationCondition = 'NotExisting';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        $('[name="eventNumber"]').focus();
                        break;
                    case 2:
                        $scope.eventNumberValidationCondition = 'IsInUse';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        $('[name="eventNumber"]').focus();
                        return;
                        break;
                    case 3:
                        $scope.eventNumberValidationCondition = 'NotInDateRange';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        $('[name="eventNumber"]').focus();
                        return;
                        break;

                    default:

                }
            }).error(function () {
            });
        }
        else {
            $scope.openConfirmation('submit-paper-form', 'submitConfirm.html');
        }
    };

    $scope.cancelPaperForm = function () {
        $rootScope.$emit('clean-up-temporary-document-uploads');
        $uibModalInstance.dismiss('cancel');
    };

    $scope.openConfirmation = function (eventName, templateName) {
        var confirmModal = $uibModal.open({
            templateUrl: templateName,
            controller: 'ConfirmModalCtrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                eventName: function () { return eventName; }
            }
        });

        confirmModal.result.then(function (data) {
            if (data == 'submit-paper-form') {
                $http({
                    url: GlobalSettingService.AddPaperFormServiceURL(),
                    method: "POST",
                    data: { transactionDataModel: $scope.trans }
                }).success(function (result) {
                    if (result.status) {
                        $uibModalInstance.close();
                    } else {
                        $scope.statusModel.display = true;
                        $scope.statusModel.errorType = result.errorType;
                    }
                }).error(function () {
                });
            }
        });
    };

    $scope.$watch('trans.tireTypeList', function () {
        if (angular.isDefined($scope.trans.tireTypeList) && $rootScope.submitAttempted) {
            $scope.documentUploadRequired = $scope.isDocumentUploadRequired();
        }
    }, true);
    $scope.isDocumentUploadRequired = function () {
        var uploadRequiredList = _.filter($scope.trans.supportingDocumentList, function (i) {
            return i.documentFormat == 3 && i.required;
        });
        return uploadRequiredList.length > 0;
    };

    $scope.documentUploadInvalid = function () {
        return $scope.documentUploadRequired && $scope.trans.uploadDocumentList.length <= 0;
    };

    $scope.tireTypeCountInvalid = function () {
        if (!angular.isDefined($scope.trans.tireTypeList) || $scope.trans.tireTypeList.length <= 0) return false;

        var tireTypeList = _.filter($scope.trans.tireTypeList, function (i) { return i.count >= 1 });
        return tireTypeList.length <= 0;
    };

}]);

commonTransactionLib.controller('ConfirmModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'GlobalSettingService', 'eventName', function ($rootScope, $scope, $http, $uibModalInstance, GlobalSettingService, eventName) {
    $scope.eventName = eventName;

    $scope.confirm = function () {
        $uibModalInstance.close($scope.eventName);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

///used by transaction detail and trans Adj modal dialog
commonTransactionLib.controller('TransactionHandlerModelCtrl', ['$rootScope', '$scope', '$http', '$uibModal', '$uibModalInstance', '$window', 'GlobalSettingService', 'TransactionHandlerService', 'handlerConfig', function ($rootScope, $scope, $http, $uibModal, $uibModalInstance, $window, GlobalSettingService, TransactionHandlerService, handlerConfig) {
    $scope.THSvr = TransactionHandlerService;
    $scope.initialized = false;
    $scope.vm = null;

    $scope.refreshList = false;
    $scope.currentTransactionId = null;
    $scope.documentUploadRequired = false;
    $scope.isStaff = Global.Settings.isStaff;

    $scope.init = function () {
        if (!$scope.initialized) {
            $scope.THSvr.initialize(handlerConfig);
        }
    }

    $scope.$watch('THSvr.getCurrentTransactionId()', function () {
        $scope.currentTransactionId = $scope.THSvr.getCurrentTransactionId();
        if ($scope.currentTransactionId != null) {
            $http({
                url: GlobalSettingService.GetTransactionDetailDataServiceURL(),
                method: "POST",
                data: {
                    id: $scope.currentTransactionId,
                    vendorType: $scope.THSvr.getVendorType(),
                    adjRequested: $scope.THSvr.getRequestedMode() == "adjust",
                    claimId: $scope.THSvr.getClaimId(),
                    adjustmentId: handlerConfig.transactionAdjustmentId
                }
            }).success(function (result) {
                if (result.status) {
                    $scope.THSvr.setCurrentModel(result.data);
                    $scope.vm = $scope.THSvr.getCurrentModel();
                    $scope.vm.isStaff = $scope.isStaff;
                    if (!$scope.initialized) {
                        $scope.THSvr.initializeReviewToolbar(($scope.vm.direction == 2) ? '-' + $scope.vm.typeCode : $scope.vm.typeCode);
                        $scope.initialized = true;
                    }
                }
            }).error(function () {
            });
        }
    }, true);

    $scope.$watch('THSvr.getRequestedMode()', function () {
        if ($scope.initialized && $scope.THSvr.getRequestedMode() == "adjust") {
            $http({
                url: '/System/Transactions/GetTransactionAdjustmentData',
                method: "POST",
                data: { id: $scope.currentTransactionId }
            }).success(function (result) {
                if (result.status) {
                    $scope.vm.adjModel = result.data;
                    $scope.vm.currentMode = "adjust";
                }
            }).error(function () {
            });
        }
    }, true);

    $scope.cancel = function () {
        if ($scope.THSvr.isReviewToolBarExists()) {
            $scope.vm.currentMode = "view";
            if ($scope.THSvr.getRequestedMode() == "adjust") $scope.THSvr.setRequestedMode("view");
        }
        else $uibModalInstance.close();
    };

    // Start of Adjustment
    $scope.submitAdjustment = function () {
        $scope.$broadcast('show-errors-event');

        //OTSTM2-1235 add gross/tare validation only for PTR
        if (($scope.vm.typeCode == "PTR" || $scope.vm.typeCode == "DOT" || $scope.vm.typeCode == "PIT") && $scope.vm.deviceName != "Paper Form") {
            if ($scope.transForm.$invalid || $scope.tireTypeCountInvalid() || $scope.grossTareBusinessRuleInvalid()) return;
        }
        else {
            if ($scope.transForm.$invalid || $scope.tireTypeCountInvalid()) return;
        }
       
        $http({
            url: "/System/Transactions/Submit" + $scope.vm.typeCode + "Adjustment/",
            method: "POST",
            data: {
                adjustmentDataModel: $scope.vm.adjModel,
                vendorType: $scope.THSvr.getVendorType(),
                claimId: $scope.THSvr.getClaimId(),
                getUpdatedData: $scope.THSvr.isReviewToolBarExists()
            }
        }).success(function (result) {
            if (result.status) {
                if (!$scope.THSvr.isReviewToolBarExists()) $uibModalInstance.close(true);
                else {
                    $scope.THSvr.setCurrentModel(result.data);
                    $scope.vm = $scope.THSvr.getCurrentModel();
                    if ($scope.THSvr.getRequestedMode() == "adjust") $scope.THSvr.setRequestedMode("view");
                    $(".modal.fade.ng-isolate-scope.in").scrollTop(0);
                }
            }
        }).error(function () {
        });
    };

    $scope.tireTypeCountInvalid = function () {
        if ($scope.vm.adjModel == null) return false;
        if (!angular.isDefined($scope.vm.adjModel.tireTypeList) || $scope.vm.adjModel.tireTypeList.length <= 0) return false;

        var tireTypeList = _.filter($scope.vm.adjModel.tireTypeList, function (i) { return i.count >= 1 });
        return tireTypeList.length <= 0;
    };
    
    //OTSTM2-1235 The validation about the business rule: inbound > outbound
    $scope.grossTareBusinessRuleInvalid = function () {
        if (($scope.vm.adjModel.transactionType == 'PTR' || $scope.vm.adjModel.transactionType == 'DOT' || $scope.vm.adjModel.transactionType == 'PIT') && $scope.vm.adjModel.scaleTicketDetail) {
            var inboundWeight = parseFloat($scope.vm.adjModel.scaleTicketDetail.tickets[0].inboundWeight);
            var outboundWeight = parseFloat($scope.vm.adjModel.scaleTicketDetail.tickets[0].outboundWeight);
            if (outboundWeight >= inboundWeight) {
                $('#GrossTareBusinessRuleErrorMessage').css("display", "block");
                $('#GrossTareBusinessRuleErrorMessage').addClass("required-validity");
                $('[name="singleScaleInboundWeight"]').addClass("has-error");
                $('[name="singleScaleOutboundWeight"]').addClass("has-error");
                return true;
            }
            else {
                $('#GrossTareBusinessRuleErrorMessage').css("display", "none");
                $('#GrossTareBusinessRuleErrorMessage').removeClass("required-validity");
                $('[name="singleScaleInboundWeight"]').removeClass("has-error");
                $('[name="singleScaleOutboundWeight"]').removeClass("has-error");
                return false;
            }
        }
        else {
                return false;
        }            
    }
    // End of Adjustment


    // Start of Review
    $scope.submitReview = function (accepted) {
        $http({
            url: "/System/Transactions/SubmitAdjustmentReview/",
            method: "POST",
            data: {
                id: $scope.currentTransactionId,
                accepted: accepted,
                getUpdatedData: $scope.THSvr.isReviewToolBarExists(),
                notes: $scope.vm.adjustmentNote,
                claimId: $scope.THSvr.getClaimId(),
                vendorType: $scope.THSvr.getVendorType()
            }
        }).success(function (result) {
            if (result.status) {
                if (!$scope.THSvr.isReviewToolBarExists()) {
                    $uibModalInstance.close(true);
                    //reload page
                    $window.location.reload();
                }
                else {
                    //OTSTM2-660
                    $scope.THSvr.setCurrentModel(result.data);
                    $scope.vm = $scope.THSvr.getCurrentModel();

                    $scope.vm.adjustmentDetail.status = accepted ? "Accepted" : "Rejected";
                    $scope.vm.adjustmentReviewAllowed = false;
                    $scope.vm.pendingAdjustmentExits = false;
                    if (!accepted) {//adjustment is rejected
                        $scope.vm.displayOriginal = true;
                        $scope.vm.activeAdjustmentExits = false;
                        $scope.vm.adjustmentDetail = null;
                        $scope.vm.adjusted = null;
                    } else {//adjustment is accepted
                        $scope.vm.displayOriginal = false;
                    }
                    $(".modal.fade.ng-isolate-scope.in").scrollTop(0);

                    //OTSTM2-660
                    if ($scope.THSvr.getRequestedMode() == "adjust") $scope.THSvr.setRequestedMode("view");
                }
            }
        }).error(function () {
        });
    };
    // End of Review

    // Start of Recall
    $scope.submitRecall = function () {
        $http({
            url: "/System/Transactions/SubmitAdjustmentRecall/",
            method: "POST",
            data: {
                id: $scope.currentTransactionId,
                getUpdatedData: $scope.THSvr.isReviewToolBarExists(),
                notes: $scope.vm.adjustmentNote,
                claimId: $scope.THSvr.getClaimId(),
                vendorType: $scope.THSvr.getVendorType()
            }
        }).success(function (result) {
            if (result.status) {
                if (!$scope.THSvr.isReviewToolBarExists()) $uibModalInstance.close(true);
                else {
                    //OTSTM2-660
                    $scope.THSvr.setCurrentModel(result.data);
                    $scope.vm = $scope.THSvr.getCurrentModel();

                    $scope.vm.adjustmentRecallAllowed = false;
                    $scope.vm.displayOriginal = true;
                    $scope.vm.pendingAdjustmentExits = false;
                    $scope.vm.activeAdjustmentExits = false;
                    $scope.vm.adjustmentDetail = null;
                    $scope.vm.adjusted = null;
                    $(".modal.fade.ng-isolate-scope.in").scrollTop(0);

                    //OTSTM2-660
                    if ($scope.THSvr.getRequestedMode() == "adjust") $scope.THSvr.setRequestedMode("view");
                }
            }
        }).error(function () {
        });
    };
    // End of Recall

    // Start of Reject Adjustment
    $scope.rejectAdjustment = function () {
        $http({
            url: "/System/Transactions/SubmitRejectAdjustment/",
            method: "POST",
            data: {
                id: $scope.currentTransactionId,
                getUpdatedData: $scope.THSvr.isReviewToolBarExists(),
                notes: $scope.vm.adjustmentNote,
                claimId: $scope.THSvr.getClaimId(),
                vendorType: $scope.THSvr.getVendorType()
            }
        }).success(function (result) {
            if (result.status) {
                if (!$scope.THSvr.isReviewToolBarExists()) {
                    $uibModalInstance.close(true);
                    //reload page
                    $window.location.reload();
                }
                else {
                    //OTSTM2-660
                    $scope.THSvr.setCurrentModel(result.data);
                    $scope.vm = $scope.THSvr.getCurrentModel();

                    $scope.vm.adjustmentRejectAllowed = false;
                    $scope.vm.displayOriginal = true;
                    $scope.vm.pendingAdjustmentExits = false;
                    $scope.vm.activeAdjustmentExits = false;
                    $scope.vm.adjustmentDetail = null;
                    $scope.vm.adjusted = null;
                    $(".modal.fade.ng-isolate-scope.in").scrollTop(0);

                    //OTSTM2-660
                    if ($scope.THSvr.getRequestedMode() == "adjust") $scope.THSvr.setRequestedMode("view");
                }
            }
        }).error(function () {
        });
    };
    // End of Reject Adjustment

    $rootScope.$on('internal-note-added', function (e, message) {
        $scope.refreshList = true;
    });

    ///use this for all $broadcast
    $rootScope.$on('rootscope-broadcast', function (event, message) {
        $scope.$broadcast(message.name, message.data);
    });

    $scope.close = function () {
        $uibModalInstance.close($scope.THSvr.isReviewToolBarExists() || $scope.refreshList);
    };

    $scope.init();
}]);

commonTransactionLib.controller('MapModalCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'gps', 'postal', function ($scope, $http, $uibModal, $uibModalInstance, gps, postal) {
    $scope.gps = gps;
    $scope.postal = postal;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

commonTransactionLib.filter('findInArrayFilter', function () {
    return function (items, attr, search) {
        var result = [];
        var returnval = {};
        var i = 0, j = 0;
        $.each(items, function (key, value) {
            i++;
            $.each(value, function (key2, value2) {
                j++;
                if (value2 == search && key2 == attr) {
                    returnval = value;
                    return false;//no continue
                }
            });
            if (Object.keys(returnval).length > 0) {//object has attribute
                return false;//no continue
            }
        });
        return returnval;
    }
});

commonTransactionLib.filter('stringOperation', function () {
    return function (item) {
        return (!!item) ? item.charAt(0).toUpperCase() + item.substr(1).toLowerCase() : '';
    }
});

// Service Section
commonTransactionLib.factory('GlobalSettingService', function () {
    return {
        ConvertLbToKg: function (weight) { return weight * 0.453600; },
        ConvertKgToLb: function (weight) { return weight * 2.204586; },
        ConvertTonToKg: function (weight) { return weight * 1000; },
        ConvertKgToTon: function (weight) { return weight * 0.001; },
        ConvertLbToTon: function (weight) { return weight * 0.0004536; },
        ConvertTonToLb: function (weight) { return weight * 2204.586; },

        DatePickerFormat: function () { return Global.Settings.DatePickerFormat; },

        // Service URLs
        InitializePaperFormServiceURL: function () { return Global.Settings.InitializePaperFormServiceURL; },
        AddPaperFormServiceURL: function () { return Global.Settings.AddPaperFormServiceURL; },

        CheckFormNumberDuplicationServiceURL: function () { return Global.Settings.CheckFormNumberDuplicationServiceURL; },
        GetVendorInfoServiceURL: function () { return Global.Settings.GetVendorInfoServiceURL; },
        GetTransactionDetailDataServiceURL: function () { return Global.Settings.GetTransactionDetailDataServiceURL; },
        GetTransactionSearchListServiceURL: function () { return Global.Settings.GetTransactionSearchListServiceURL; },
        GetUpdateTransactionProcessingStatusServiceURL: function () { return Global.Settings.GetUpdateTransactionProcessingStatusServiceURL; },
        GetGetTransactionHandlerClaimDataServiceURL: function () { return Global.Settings.GetGetTransactionHandlerClaimDataServiceURL; },

        UploadTransactionFileServiceURL: function () { return Global.Settings.UploadTransactionFileServiceURL; },
        DeleteTemporaryTransactionFileServiceURL: function () { return Global.Settings.DeleteTemporaryTransactionFileServiceURL; },
        CleanUpTemporaryTransactionFilesServiceURL: function () { return Global.Settings.CleanUpTemporaryTransactionFilesServiceURL; },

        // File Preview Root Paths
        FileUploadPreviewDomainName: function () { return Global.Settings.FileUploadPreviewDomainName; },
        MobilePhotoPreviewDomainName: function () { return Global.Settings.MobilePhotoPreviewDomainName; },

        //OTSTM2-1214, Event Number validation check
        EventNumberValidationCheck: function () { return Global.Settings.EventNumberValidationCheckURL; }
    };
});

commonTransactionLib.factory('ValidationService', function () {
    var EventNumberFormat = /^[0-9]{5,}$/;
    var PhoneNumberFormat = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$/;
    var PostalCodeFormat = /^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]$/;
    var ZipCodeFormat = /^[0-9]{5}(?:[- ][0-9]{4})?$/;

    return {
        validateEventNumber: function (number) { return EventNumberFormat.test(number); },
        validatePhoneNumber: function (number) { return PhoneNumberFormat.test(number); },
        validatePostalCode: function (code) { return PostalCodeFormat.test(code); },
        validateZipCode: function (code) { return ZipCodeFormat.test(code); },
    };
});

commonTransactionLib.factory('TransactionHandlerService', function () {
    var vendorType = null;
    var claimId = null;
    var claimPeriodName = null;

    var RequestedMode = null;
    var CurrentTransactionId = null;

    var reviewToolBarExists = false;
    var reviewToolbarSetting = null;

    var currentDataModel = null;

    return {
        initialize: function (config) {
            reviewToolbarSetting = { transactionFormat: "Both", processingStatus: "All", transactionType: "All" };

            RequestedMode = config.requestedMode; //"view" transaction or "adjust" transaction
            vendorType = config.vendorType;
            claimId = config.claimId;
            claimPeriodName = config.periodName;

            CurrentTransactionId = config.transactionId;
        },

        getVendorType: function () { return vendorType; },
        getClaimId: function () { return claimId; },
        getClaimPeriodName: function () { return claimPeriodName; },

        setCurrentTransactionId: function (id) { return CurrentTransactionId = id; },
        getCurrentTransactionId: function () { return CurrentTransactionId; },

        initializeReviewToolbar: function (type) { reviewToolbarSetting.transactionType = type; },
        getReviewToolbarSetting: function () { return reviewToolbarSetting; },

        setReviewToolBarAsExists: function (exists) { return reviewToolBarExists = exists },
        isReviewToolBarExists: function () { return reviewToolBarExists },

        setRequestedMode: function (mode) { return RequestedMode = mode; },
        getRequestedMode: function () { return RequestedMode; },

        setCurrentModel: function (dataModel) { return currentDataModel = dataModel; },
        getCurrentModel: function () { return currentDataModel; },

        isAdjustmentAllowed: function () {
            if (currentDataModel != null && currentDataModel.adjustmentAllowedInClaim && !currentDataModel.pendingAdjustmentExits) return true;

            return false;
        },

        isActiveAdjustmentExists: function () {
            if (currentDataModel != null && currentDataModel.activeAdjustmentExits) return true;

            return false;
        },

        //OTSTM2-660
        getTransactionReviewToolBar: function () {
            var currModel = this.getCurrentModel()
            if (currModel) {
                return currModel.transactionReviewToolBar
            }
            return null;
        },
        setTransactionReviewToolBar: function (reviewToolBar) {
            var currModel = this.getCurrentModel()
            currModel.transactionReviewToolBar = reviewToolBar
        }

    };
});