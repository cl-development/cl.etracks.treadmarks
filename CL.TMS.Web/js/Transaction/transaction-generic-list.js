﻿/* global CurrentUser */
(function ($) {
    $.fn.createDataTable = function (options) {
        $('#transactionListName').text(Global.Transaction.ListName);
        TM = {};
        UserRoles = { Read: 6, Write: 7, Submit: 8, ParticipantAdmin: 9 };
        TM.settings = $.extend({
            ajaxHandler: '',
            viewMoreId: '',
            totalRecordsId: '',
            searchId: '',
            exportId: '',
            exportUrl: '',
            foundId: '',
            removeIconId: '',
            modalResendEmailId: '',
            buttonResendEmailId: '',
            modalActivateId: '',
            buttonActivateId: '',
            modalInactivateId: '',
            buttonInactivateId: '',
            modalEditUserId: '',
            buttonEditUserNextId: '',
            modalEditUserConfirmId: '',
            buttonEditUserId: '',
            checkboxRoleClassName: '',
            serverSide: true,
            scrollY: 485,
            scrollX: true,
            rowUrl: '',
            ColumnId: 0,
            pageSize: 12,
            PageReadonly: false,
            TransID: '', //OTSTM2-897
        }, options);

        $(TM.settings.exportId).attr('href', 'javascript: void(0)');

        dynamicColumns = [];

        for (var i = 0; i < TM.settings.columnNames.length; i++) {
            var columnName = TM.settings.columnNames[i].columnName ? TM.settings.columnNames[i].columnName : '';
            var className = TM.settings.columnNames[i].className ? TM.settings.columnNames[i].className : '';
            var orderable = typeof TM.settings.columnNames[i].orderable !== undefined ? TM.settings.columnNames[i].orderable : true;
            var width = typeof TM.settings.columnNames[i].width !== undefined ? TM.settings.columnNames[i].width : '';
            //console.log('columnName:' + columnName + 'orderable :', orderable);
            var row = {
                className: className,
                orderable: orderable,
                width: width,
                data: null,
                render: function (data, type, full, meta) {
                    if (TM.settings.columnNames[meta.col].columnName) {
                        var columnName = TM.settings.columnNames[meta.col].columnName;
                        //console.log('columnName:' + columnName + " " + data[columnName]);
                        if ((columnName === 'SyncAdd') || (columnName === 'End') || (columnName === 'Start') || ('FormDate' === columnName) || (columnName === 'TransactionDate') || (columnName === 'InvoiceDate')) {
                            return dateTimeConvert(data[columnName]);
                        } else if ((columnName === 'RetailPrice') || (columnName === 'Rate') || (columnName === 'AmountDollar') || (columnName === 'AmountDollar_Ton')) {//show 2 decimal --RPM:AmountDollar //TotalWeight will be 2 decimal now?
                            var returnval = data[columnName];
                            if ((typeof (returnval) !== "undefined") && (null !== returnval))
                                return returnval.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }); //OTSTM2-423
                            else return '';
                        } else if (columnName === 'Variance') {
                            var returnval = data[columnName];
                            if ((typeof (returnval) !== "undefined") && (null !== returnval)) {
                                if (Math.abs(returnval) < 0.005) {
                                    returnval = 0; //-0.00 fix
                                }
                                if ((Math.abs(data[columnName])) > 15) {
                                    return '<span class="flag-qtyover">' + returnval.toFixed(2) + '</span>';
                                } else {
                                    return returnval.toFixed(2);
                                }
                            }
                            else return '';
                        } else if ((columnName === 'OnRoadWeight') || (columnName === 'OffRoadWeight') || (columnName === 'ScaleWeight') || ('EstWeight' === columnName) || (columnName === 'TotalWeight')) {//all double type, show 4 decimal
                            var returnval = data[columnName];
                            if ((typeof (returnval) !== "undefined") && (null !== returnval))
                                return returnval.toFixed(4);
                            else return '';
                        } else if (columnName === 'IsGenerateTires') {
                            if (data.IsGenerateTires)
                                return "<span><i class='fa fa-check-circle color-tm-gray'></i></span>";
                            else
                                return "<span></span>";
                        } else if ((columnName === 'Notes') || (columnName === 'NotesAllText')) {
                            var html = '';
                            if (data[columnName]) {
                                html = '<label>' +
                                        '<i id="notes' + meta.row + '" class="fa fa-comment color-tm-grey" data-placement="auto" title="" data-text="' + data[columnName] + '"></i>' +
                                        '</label>';
                            }
                            return html;
                        } else if (columnName === 'ReviewStatus') {//transaction review status
                            var html = '';
                            switch (data[columnName]) {
                                case "Unreviewed":
                                    html = '<div class="panel-table-status color-tm-gray-bg">' + data[columnName] + '</div>';
                                    break;
                                case "Invalid":
                                    html = '<div class="panel-table-status color-tm-red-bg">' + data[columnName] + '</div>';
                                    break;
                                case "Approved":
                                    html = '<div class="panel-table-status color-tm-green-bg">' + data[columnName] + '</div>';
                                    break;
                                default:
                                    html = '<div class="panel-table-status color-tm-yellow-bg">' + data[columnName] + '</div>';
                                    break;
                            }
                            return html;
                        } else if (columnName === 'Status') {//transaction status
                            var html = '';
                            switch (data[columnName]) {
                                case "Voided":
                                    html = '<div class="panel-table-status color-tm-gray-bg">' + data[columnName] + '</div>';
                                    break;
                                case "Error" || "Rejected":
                                    html = '<div class="panel-table-status color-tm-red-bg">' + data[columnName] + '</div>';
                                    break;
                                case "Completed":
                                    html = '<div class="panel-table-status color-tm-green-bg">' + data[columnName] + '</div>';
                                    break;
                                case "Incomplete":
                                    html = '<div class="panel-table-status color-tm-yellow-bg">' + data[columnName] + '</div>';
                                    break;
                                default: //Pending 
                                    html = '<div class="panel-table-status color-tm-yellow-bg">' + data[columnName] + '</div>';
                                    break;
                            }
                            return html;
                        } else if (columnName === 'Adjustment') {
                            if (data[columnName]) {
                                return '<div class="panel-table-status-default">' + data[columnName] + '</div>'
                            } else {
                                return '';
                            }
                        } else if (columnName === 'BadgeUser') {
                            if (data[columnName].length > 15)
                                return data[columnName].substring(0, 13) + '...';
                            else
                                return data[columnName];
                        } else if (columnName === 'Description') {
                            if (data[columnName].length > 12)
                                return data[columnName].substring(0, 10) + '...';
                            else
                                return data[columnName];
                        } else if (columnName === 'Destination') {
                            return data[columnName].replace('Canada', 'Ca');
                        } else if (columnName === 'ProductCode') {
                            return data[columnName].replace('PRODUCT CODE', 'PC');
                        } else if ((columnName === 'PLT') || (columnName === 'MT') || (columnName === 'AGLS') || (columnName === 'IND')
                            || (columnName === 'SOTR') || (columnName === 'MOTR') || (columnName === 'LOTR') || (columnName === 'GOTR')) { //If tire count exceeds the limit, it will be marked in red 
                            var html = data[columnName];

                            //OTSTM2-1064
                            if ("staff" === TM.settings.userRole && Global.TransactionThresholds.Threshold_CBIndividualTireCount.length != 0 && Global.TransactionThresholds.Threshold_CBIndividualTireCount == '1') { 
                                switch (columnName) {
                                    case "PLT":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountPLT.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountPLT))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "MT":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountMT.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountMT))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "AGLS":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountAGLS.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountAGLS))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "IND":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountIND.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountIND))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "SOTR":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountSOTR.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountSOTR))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "MOTR":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountMOTR.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountMOTR))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "LOTR":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountLOTR.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountLOTR))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    case "GOTR":
                                        if (Global.TransactionThresholds.Threshold_IndividualTireCountGOTR.length != 0) {
                                            if (data[columnName] >= parseInt(Global.TransactionThresholds.Threshold_IndividualTireCountGOTR))
                                                html = '<span class="flag-qtyover">' + data[columnName] + '</span>';
                                        }
                                        break;
                                    default:
                                        html = data[columnName];
                                        break;
                                }
                            }                           
                            return html;
                        } else if (columnName === 'Actions') { // action gear
                            if (TM.settings.transactionType === "ALL") {
                                var status = data.Status;//this is Transaction Status

                                var linkClass = ("Open" === data.ClaimReviewStatus) ? "not-active" : "";
                                var html = '';
                                if (true === TM.settings.PageReadonly) return html;
                                if (status === "Pending" && TM.settings.currParticipantId === data.CreatedVendorId && !data.MobileFormat) {
                                    html = '<div class="btn-group dropdown-menu-actions">' +
                                        '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                        '<div class="popover-menu"><ul>' +
                                        '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalVoid + '" data-toggle="modal" href="javascript:void(0)"><i class="fa fa-times"></i> Void</a></li>' +
                                        '</ul></div></div>';
                                } else if (status === "Pending" && TM.settings.currParticipantId !== data.CreatedVendorId && !data.MobileFormat) {
                                    html = '<div class="btn-group dropdown-menu-actions">' +
                                        '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                        '<div class="popover-menu"><ul>' +
                                        '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalAccept + '" data-toggle="modal" href="javascript:void(0)" class=' + linkClass + '><i class="fa fa-check"></i> Accept</a></li>' +
                                        '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalReject + '" data-toggle="modal" href="javascript:void(0)" class=' + linkClass + '><i class="fa fa-times"></i> Reject</a></li>' +
                                        '</ul></div></div>';
                                } else if (status === "Incomplete" && data.MobileFormat && ("staff" === TM.settings.userRole)) {
                                    if (Global.Transaction.AccessPermission !== 'EditSave') {
                                        return '';
                                    }
                                    html = '<div class="btn-group dropdown-menu-actions">' +
                                        '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                        '<div class="popover-menu"><ul>' +
                                        '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalVoid + '" data-toggle="modal" href="javascript:void(0)" class=' + linkClass + '><i class="fa fa-times"></i> Void</a></li>' +
                                        '</ul></div></div>';
                                }

                                return html;
                            }

                            if ("staff" === TM.settings.userRole) { // staff user, for specific transaction type, TCR, DOT....  
                                var html = '';
                                if (Global.Transaction.AccessPermission !== 'EditSave') {
                                    return html;
                                }
                                if (true === TM.settings.PageReadonly) return html;
                                if (("Open" === TM.settings.ClaimReviewStatus) || ("UnderReview" === TM.settings.ClaimReviewStatus) || ("Submitted" === TM.settings.ClaimReviewStatus)) { //base on enum ClaimStatus
                                    switch (data.ReviewStatus) { // base on transaction ProcessingStatus
                                        case "Unreviewed":
                                            html += '<div class="btn-group dropdown-menu-actions">' +
                                                '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                                '<div class="popover-menu"><ul data-transId="' + data.ID + '">';

                                            html += '<li class="transaction-adjustment"><a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Adjust</a></li>';

                                            if (("Open" !== TM.settings.ClaimReviewStatus) && ("Submitted" !== TM.settings.ClaimReviewStatus)) {
                                                html += '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalApprove + '" data-toggle="modal" href="javascript:void(0)">' + '<i class="fa fa-check"></i> Approve</a></li>';
                                            }
                                            html += '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalInvalidate + '" data-toggle="modal" href="javascript:void(0)">' + '<i class="fa fa-ban"></i> Invalidate</a></li>';

                                            //if ((Global.Transaction.ListName == 'All SPS Transactions') && (!data.MobileFormat)) {
                                            if (Global.Transaction.TransactionType === 'SPSR' && !data.MobileFormat) {
                                                html += '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalRetailPrice + '" data-toggle="modal" href="">' + '<i class="fa fa-usd"></i> RP</a></li>';
                                            }
                                            html += '</ul></div></div>';

                                            break;
                                        case "Invalidated":
                                        case "Invalid":
                                            html = '<div class="btn-group dropdown-menu-actions">' +
                                                '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                                '<div class="popover-menu"><ul data-transId="' + data.ID + '">';

                                            html += '<li class="transaction-adjustment"><a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Adjust</a></li>';
                                            if ("Open" !== TM.settings.ClaimReviewStatus) {
                                                html += '<li><a data-rowId="' + meta.row + '"  data-target="' + TM.settings.modalApprove + '" data-toggle="modal" href="javascript:void(0)">' + '<i class="fa fa-check"></i> Approve</a></li>';
                                            }
                                            html += '</ul></div></div>';
                                            break;
                                        case "Approved":
                                            html = '<div class="btn-group dropdown-menu-actions">' +
                                                '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                                '<div class="popover-menu"><ul data-transId="' + data.ID + '">';

                                            html += '<li class="transaction-adjustment"><a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Adjust</a></li>';
                                            html += '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalInvalidate + '" data-toggle="modal" href="javascript:void(0)">' + '<i class="fa fa-ban"></i> Invalidate</a></li>';

                                            html += '</ul></div></div>';
                                            break;
                                        default:
                                            html = '';
                                            break;
                                    }
                                }//else, claim status == 'Approved'

                                return html;
                            } else { // participant user, for specific transaction type, TCR, DOT....                                
                                var html = '';
                                if (true === TM.settings.PageReadonly) return html;
                                html = '<div class="btn-group dropdown-menu-actions">' +
                                '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title=""><i class="glyphicon glyphicon-cog"></i></a>' +
                                '<div class="popover-menu"><ul data-transId="' + data.ID + '">';

                                html += '<li class="transaction-adjustment"><a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Adjust</a></li>';
                                if (("Unreviewed" === data.ReviewStatus) && ('Open' === TM.settings.ClaimReviewStatus) && (Global.Transaction.TransactionType === 'SPSR')) {
                                    html += '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalVoid + '" data-toggle="modal" href="javascript:void(0)"><i class="fa fa-times"></i> Void</a></li>';
                                };

                                html += '</ul></div></div>';

                                if ('Open' === TM.settings.ClaimReviewStatus) {
                                    // Empty?
                                };

                                return html;
                            }

                            return '';
                        } else { // general column, return raw value
                            return data[columnName];
                        }
                    } else { // [meta.col].columnName is empty or null
                        return "<span>" + "</span>";
                    } // end of if (TM.settings.columnNames[meta.col].columnName)
                } // end of render: function 
            };

            dynamicColumns.push(row);
        }

        var table = this.DataTable({
            serverSide: TM.settings.serverSide,
            ajax: {
                "url": TM.settings.ajaxHandler,
                "data": {
                    "DataType": TM.settings.transactionType,
                    "DataSubType": TM.settings.subDataType,
                }
            },
            scrollY: TM.settings.scrollY,
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            scrollCollapse: true,
            createdRow: function (row, data, index) {
                $(row).addClass('cursor-pointer');
                $(row).attr('transaction-id', data.ID);

                if (TM.settings.transactionType === "ALL") {
                    var content = '';
                    switch (data.Status) {
                        case "Error":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-red-bg">' + data.Status + '</div>'));
                            break;
                        case "Incomplete":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-yellow-bg">' + data.Status + '</div>'));
                            break;
                        case "Voided":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-gray-light-bg">' + data.Status + '</div>'));
                            break;
                        case "Completed":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-green-bg">' + data.Status + '</div>'));
                            break;
                        case "Pending":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-orange-bg">' + data.Status + '</div>'));
                            break;
                        case "Rejected":
                            $('td', row).eq(0).empty().append($('<div class="panel-table-status color-tm-red-bg">' + data.Status + '</div>'));
                            break;
                        default:
                            $('td', row).eq(0).empty().append('');
                    };
                }
            },
            processing: false,
            order: [[1, "desc"]],
            searching: true,
            "dom": "rtiS",
            info: false,
            deferRender: true,

            //PageLength = parseInt((scrollY / rowHeight, 10) + 1) * displayBuffer; 
            //current load 400 transaction records per request, since Max number for RPM SPSR transaction(slow loading caused by multi table join) May claim has 330 records.
            "scroller": {
                displayBuffer: 20,//This is pages, not records! default:9
                rowHeight: 25,
                serverWait: 100,
                loadingIndicator: false
            },
            columns: dynamicColumns,
            initComplete: function (settings, json) {
                $('.dataTables_scrollBody').css('overflow-y', 'hidden');
                $(TM.settings.totalRecordsId).val('Total ' + settings.fnRecordsTotal());

                if (TM.settings.RoutingCount > 0) {
                    $(TM.settings.asgnToMeBtn).show();
                } else {
                    $(TM.settings.asgnToMeBtn).hide();
                };

                //$(TM.settings.exportId).attr('href', TM.settings.exportUrl);
                var url = TM.settings.exportUrl.replace('AAA', TM.settings.columnNames[settings.aLastSort[0].col].columnName).replace('BBB', settings.aaSorting[0][1]).replace('----', $(TM.settings.searchId).val());
                $(TM.settings.exportId).attr('href', url);

                //OTSTM2-897 trigger auto click event when searchText is not empty (come from global search)
                if (TM.settings.TransID != 0) {                    
                    $('#tblTransactionlist tbody tr[transaction-id="' + TM.settings.TransID + '"]').click();
                    //$("#tblTransactionlist tbody tr")[0].click(); 
                }
                
            },
            preDrawCallback: function (settings, json) {
                if (undefined !== settings.json) {
                    TM.settings.ClaimReviewStatus = settings.json.claimStatus;
                    TM.settings.PageReadonly = settings.json.PageReadonly;
                }
            },
            drawCallback: function (settings) {
                //console.log('settings.aoData.length:', settings.aoData.length);
                var direction = settings.aaSorting[0][1];
                $('.sort').html('<i class="fa fa-sort"></i>');
                (direction === 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

                (settings.aoData.length >= TM.settings.pageSize) ? $(TM.settings.viewMoreId).css('visibility', 'visible') : $(TM.settings.viewMoreId).css('visibility', 'hidden');

                var sortColumn = TM.settings.columnNames[settings.aLastSort[0].col].columnName;
                var searchValue = $(TM.settings.searchId).val();

                var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
                $(TM.settings.exportId).attr('href', url);

                $(TM.settings.foundId).html('Found ' + settings.fnRecordsDisplay());
                if (searchValue === '') {
                    $(TM.settings.foundId).html(' ');
                }

                //OTSTM2-573 for opening transaction handler open
                var scope = angular.element(document.getElementById("tblTransactionlist")).scope();
                //OTSTM2-573
                scope.table = {
                    currTable: table,
                    searchVal: $('#search').val()
                }

                $('body').off().on('click', '.transaction-adjustment', function (e) {
                    //To only remove registered 'click' event handlers:
                    //    $('body').off('click').on('click', '.transaction-adjustment', function (e)
                    //        or
                    //    $('body').one('click',function() {
                    //        or
                    //    e.stopImmediatePropagation(); // when you just want your event to be handled without any bubbling and without effecting any other events already being handled. 
                    //      It happens due to the particular event is bound multiple times to the same element.
                    var localScope = angular.element(document.getElementById("tblTransactionlist")).scope();
                    if (typeof $(e.target).closest("ul").attr("data-transId") !== "undefined") localScope.open($(e.target).closest("ul").attr("data-transId"), 'adjust', function () { table.search($(TM.settings.searchId).val()).draw(false); });
                });
            },
            footerCallback: function (row, data, start, end, display) {
            },
            rowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //OTSTM2-1064
                if ("staff" === TM.settings.userRole) {
                    switch (TM.settings.transactionType) {
                        case "DOR":
                            if (aData['FlagQtyOverDOR']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "DOT":
                            if (aData['FlagQtyOverDOT']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "HIT":
                            if (aData['FlagQtyOverHIT']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;                      
                        case "PTR":
                            if (aData['FlagQtyOverPTR']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "RTR":
                            if (aData['FlagQtyOverRTR']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "STC":
                            if (aData['FlagQtyOverSTC']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "TCR":
                            if (aData['FlagQtyOverTCR']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        case "UCR":
                            if (aData['FlagQtyOverUCR']) {
                                $(nRow).addClass('flag-qtyover');
                            }
                            break;
                        default:
                    }                   
                }             
                return nRow;
            },
        });

        _pageX = 0;
        _pageY = 0;
        _cancelMouseClick = false;

        $('#tblTransactionlist tbody').on('mousedown', 'tr', function (e) {
            _pageX = e.pageX;
            _pageY = e.pageY;
        });
        $('#tblTransactionlist tbody').on('mouseup', 'tr', function (e) {
            _pageX = e.pageX - _pageX;
            _pageY = e.pageY - _pageY;
            if ((Math.abs(_pageX) > 5) || (Math.abs(_pageY) > 5)) {
                _cancelMouseClick = true;
            }
        });

        table.on("draw.dt", function () {
            $("[id^='actions']").popover({
                placement: "bottom",
                container: "body",
                html: true,
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                content: function () {
                    return $($(this)).siblings(".popover-menu").html();
                },
            });

            $("[id^='note']").webuiPopover({
                width: '500',
                height: '300',
                padding: true,
                multi: true,
                closeable: true,
                title: 'Transaction Notes',
                type: 'html',
                trigger: 'hover',

                content: function () {
                    var internalNote = $(this).attr('data-text');
                    var res = internalNote.split('\n');
                    var result = "";
                    var arrayLength = res.length;
                    for (var i = 0; i < arrayLength; i++) {
                        result = result + '<p>' + res[i] + '</p>';
                    }
                    return result;
                },
                delay: { show: 100, hide: 100 },
            });
        });

        $.fn.dataTable.ext.errMode = 'none';
        table.on('error.dt', function (e, settings, techNote, message) {
            console.log('settings.jqXHR.statusText:', settings.jqXHR.statusText + '\r\nsettings.jqXHR.status', settings.jqXHR.status);
            window.location.href = "/System/Common/EmptyPage";
        });

        $(TM.settings.viewMoreId).off('click').on('click', function () {
            $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
            $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
            $(this).hide();
        });

        $(TM.settings.searchId).off('keypress').on('keypress', function (e) {
            if (e.keyCode === 13) {
                var str = $(this).val();
                if (TM.settings.currentSearchTxt === str) {
                    return;//search string not changing, do nothing;
                } else {
                    TM.settings.currentSearchTxt = str;
                }

                if (str.length >= 1) {
                    $(this).css('visibility', 'visible');
                    $(TM.settings.foundId).css('display', 'block');
                    $(TM.settings.foundId).siblings('.remove-icon').show();
                    table.search(str).draw(false);
                }
                else {
                    $(TM.settings.foundId).siblings('.remove-icon').hide();
                    $(TM.settings.searchId).removeAttr('style');
                    $(TM.settings.foundId).css('display', 'none');
                    table.search(str).draw(false);
                }
            }
        });

        $(TM.settings.searchId).off('focusout').on('focusout', function (e) {
            if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
              || (e.keyCode === 35) || (e.keyCode === 36)) //End Home
            { return; }

            var str = $(this).val();
            if (TM.settings.currentSearchTxt === str) {
                return;//search string not changing, do nothing;
            } else {
                TM.settings.currentSearchTxt = str;
            }
            if (str.length >= 1) {
                $(this).css('visibility', 'visible');
                $(TM.settings.foundId).css('display', 'block');
                $(TM.settings.foundId).siblings('.remove-icon').show();
                table.search(str).draw(false);
            }
            else {
                $(TM.settings.foundId).siblings('.remove-icon').hide();
                $(TM.settings.searchId).removeAttr('style');
                $(TM.settings.foundId).css('display', 'none');
                table.search(str).draw(false);
            }
        });

        $('.uiaction-export').on('click', function (e) {
            e.stopPropagation();
            var url = $(TM.settings.exportId).attr('href');
            console.log('url:' + url);
            window.location.href = $(TM.settings.exportId).attr('href');
        });
        //on clear field click
        $(TM.settings.removeIconId).click(function () {
            $(this).siblings("input").val("");
            TM.settings.currentSearchTxt = '';
            $(this).hide();
            $(TM.settings.foundId).css('display', 'none');
            //$(TM.settings.searchId).trigger('keyup');
            table.search("").draw(false);
        });

        var hdSearchText = $('#hdSearchText').val();
        if (hdSearchText != null && hdSearchText) {
            $('#search').val(hdSearchText);
            table.search(hdSearchText).draw(false);
        }

        var dateTimeConvert = function (data) {
            if (data == null) return '1/1/1950';
            if (data.indexOf('-62135578800000') > -1) return '';

            var r = /\/Date\(([0-9]+)\)\//gi;
            var matches = data.match(r);
            if (matches == null) return data.substring(0, 10);
            var result = matches.toString().substring(6, 19);
            var epochMilliseconds = result.replace(
            /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
            '$1');
            var b = new Date(parseInt(epochMilliseconds));
            var c = new Date(b.toString());
            var curr_date = c.getDate();
            if (curr_date < 10) {
                curr_date = '0' + curr_date;
            }
            var curr_month = c.getMonth() + 1;
            if (curr_month < 10) {
                curr_month = '0' + curr_month;
            }
            var curr_year = c.getFullYear();
            //var curr_h = c.getHours();
            //var curr_m = c.getMinutes();
            //var curr_s = c.getSeconds();
            //var curr_offset = c.getTimezoneOffset() / 60;
            var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
            return d;
        }

        //on load transaction detail
        $("#" + TM.settings.modalPaperForm).on('shown.bs.modal', function (event) {
            console.log(TM.settings.modalPaperForm + "loaded");
        });

        $("#" + TM.settings.modaliPad).on('shown.bs.modal', function (event) {
            console.log(TM.settings.modaliPad + "loaded");
        });

        $(TM.settings.modalRetailPrice).on('shown.bs.modal', function (event) {
            var with2Decimals = $(TM.settings.TextInputFieldName).val().toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            if (with2Decimals == 0) {
                $(TM.settings.TextInputFieldName).trigger('change');
            }
        });

        //Staff Transaction activity
        $(TM.settings.modalInvalidate).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalInvalidateTypeText).html(transactionTypeConvert(currentRow.Type));
            $(TM.settings.modalInvalidateTransNumText).html(currentRow.TransactionFriendlyId);

            $(TM.settings.btnInvalidateYes).attr('data-transId', currentRow.ID);
        });

        $(TM.settings.modalRetailPrice).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalRetailPriceTransNumText).html(currentRow.TransactionFriendlyId);
            $(TM.settings.btnAddRetailPriceYes).attr('data-transId', currentRow.ID);
            $(TM.settings.TextInputFieldName).val(currentRow.RetailPrice);
        });

        $(TM.settings.btnInvalidateYes).on('click', function () {
            var transId = $(TM.settings.btnInvalidateYes).attr('data-transId');
            ajaxHandler(TM.settings.InvalidateTransactionHandler, transId);
        });

        $(TM.settings.btnAddRetailPriceYes).on('click', function (e) {
            var transId = $(TM.settings.btnAddRetailPriceYes).attr('data-transId');
            if (parseFloat(($('#retailPrice-input').val())) >= 0) {
                ajaxHandler(TM.settings.AddRetailPriceHandler, transId, $('#retailPrice-input').val());
            } else {
                e.stopPropagation();
            }
        });

        $(TM.settings.TextInputFieldName).on('change', function (e) {
            if ($(TM.settings.TextInputFieldName).val().toString().match(/^-?\d+(?:\.\d{0,2})?/) == null) {
                $(TM.settings.btnAddRetailPriceYes).prop("disabled", true);
                return;
            }
            var with2Decimals = $(TM.settings.TextInputFieldName).val().toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];//without rounding! //.toFixed(2): with rounding
            $(TM.settings.TextInputFieldName).val(with2Decimals);
            if (with2Decimals == 0) {
                $(TM.settings.btnAddRetailPriceYes).prop("disabled", true);
            } else {
                $(TM.settings.btnAddRetailPriceYes).prop("disabled", false);
            }
        });

        $('.btn').on('mouseover', function (e) {
            if ($(TM.settings.TextInputFieldName).val() !== '') {
                $(TM.settings.TextInputFieldName).trigger('change');
            }
        });

        $(TM.settings.modalApprove).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalApproveTypeText).html(transactionTypeConvert(currentRow.Type));
            $(TM.settings.modalApproveTransNumText).html(currentRow.TransactionFriendlyId);

            $(TM.settings.btnApproveYes).attr('data-transId', currentRow.ID);
        });

        $(TM.settings.btnApproveYes).on('click', function () {
            var transId = $(TM.settings.btnApproveYes).attr('data-transId');
            ajaxHandler(TM.settings.ApproveTransactionHandler, transId);
        });

        //All transaction
        $(TM.settings.modalVoid).on('show.bs.modal', function (event) {//All transaction && RPM SPSR Trans in claim
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();

            $(TM.settings.modalVoidTypeText).html(transactionTypeConvert(currentRow.Type));
            $(TM.settings.modalVoidTransNumText).html(currentRow.TransNum);

            $(TM.settings.btnVoidYes).attr('data-transId', currentRow.TransactionId);
        });

        $(TM.settings.modalAccept).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();

            $(TM.settings.modalAcceptTypeText).html(transactionTypeConvert(currentRow.Type));
            $(TM.settings.modalAcceptTransNumText).html(currentRow.TransNum);

            $(TM.settings.btnAcceptTransaction).attr('data-transId', currentRow.TransactionId);
        });

        $(TM.settings.modalReject).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();

            $(TM.settings.modalRejectTypeText).html(transactionTypeConvert(currentRow.Type));
            $(TM.settings.modalRejectTransNumText).html(currentRow.TransNum);

            $(TM.settings.btnRejectTransaction).attr('data-transId', currentRow.TransactionId);
        });

        $(TM.settings.btnVoidYes).on('click', function () {
            var transId = $(TM.settings.btnVoidYes).attr('data-transId');
            ajaxHandler(TM.settings.VoidTransactionHandler, transId);
        });

        $(TM.settings.btnAcceptTransaction).on('click', function () {
            var transId = $(TM.settings.btnAcceptTransaction).attr('data-transId');
            ajaxHandler(TM.settings.AcceptTransactionHandler, transId);
        });

        $(TM.settings.btnRejectTransaction).on('click', function () {
            var transId = $(TM.settings.btnRejectTransaction).attr('data-transId');
            ajaxHandler(TM.settings.RejectTransactionHandler, transId);
        });

        var ajaxHandler = function (url, transID, optionalArg) {
            var req = { transactionId: transID };
            if (typeof optionalArg !== 'undefined') {
                req = { transactionId: transID, retailPrice: optionalArg };
            }
            $.ajax({
                type: 'POST',
                url: url,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    table.search($(TM.settings.searchId).val()).draw(false); //OTSTM2-573
                    //window.location.reload(true);
                },
                failure: function (data) {
                    //window.location.reload(true);
                    table.search($(TM.settings.searchId).val()).draw(false);
                },
            });
        }

        var transactionTypeConvert = function (data) {
            switch (data) {
                case 2:
                    return 'TCR';
                case 3:
                    return 'DOT';
                case 4:
                    return 'STC';
                case 5:
                    return 'UCR';
                case 6:
                    return 'HIT';
                case 7:
                    return 'RTR';
                case 8:
                    return 'PTR';
                case 9:
                    return 'PIT';
                case 10:
                    return 'SPS';
                case 11:
                    return 'DOR';
                case 12:
                    return 'SPSR';
                default:
                    return '';
            }
        };
        $('.sorting_disabled').children("a").remove();

        $("[data-toggle='popover']").on('show.bs.popover', function () {//somehow not bond
            console.log('The popover is about to be shown.');
        });
        $("[data-toggle='popover']").on('shown.bs.popover', function () {
            console.log('The popover is now fully shown.');
        });
        
        $('#tblTransactionlist tbody').on('click', 'tr', function (e) {
            var rowIndex = this.rowIndex - 1;
            if (_cancelMouseClick) {
                _cancelMouseClick = false;
                return;
            }
            var data = table.row(this).data();
            var nodeName = e.target.nodeName;
            curentTransactionId = data.ID;

            if ((nodeName === "I") || ((nodeName === "A"))) {

                if ((!!data.TokenID) && (!!data.ID)) {
                };

                $.ajax({
                    url: '/System/Transactions/ToDisableTransactionActionGear',
                    method: 'POST',
                    dataType: "JSON",
                    data: { claimId: Global.Transaction.ClaimId, transactionId: data.ID },
                    success: function (result) {
                        if (result.vendorStatus) {
                            //$("a[data-rowid='" + rowIndex + "'][data-target=" + TM.settings.modalAccept + "]").parent().addClass('avoid-clicks');//disable "Accept" li 
                            $("a[data-rowid='" + rowIndex + "'][data-target=" + TM.settings.modalAccept + "]").parent().parent().attr('title', result.vendorStatus).attr('data-toggle', 'tooltip');//disabled tag, tooltip will not work, add to parent tag
                        }
                        if (result.data) {
                            //$("a[data-rowid='" + rowIndex + "']").parent().addClass('avoid-clicks');
                            $("ul[data-transid=" + curentTransactionId + "]").addClass('avoid-clicks');//disable all li
                        }
                    },
                    failure: function (data) {
                    },
                });

                return;//and open popover menu
            };
        });
        TM.settings.currentSearchTxt = '';
    };//end create table
}(jQuery));
