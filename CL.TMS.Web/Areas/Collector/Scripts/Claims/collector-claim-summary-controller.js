﻿'use strict';

collectorClaimApp.controller("collectorClaimSummaryController", ["$scope", '$http', "$filter", "collectorClaimService", '$uibModal', 'claimService', function ($scope, $http, $filter, collectorClaimService, $uibModal, claimService) {
    $scope.dataloaded = false;
    $scope.submitBtn = false;

    collectorClaimService.getCollectorClaimDetails().then(function (data) {
        $scope.claimPeriod = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimPeriod;
        $scope.vendorId = data.collectorClaimSummaryViewModel.ClaimCommonModel.VendorId;
        $scope.claimId = data.claimId;
        $scope.claimSummaryModel = data.collectorClaimSummaryViewModel;

        $scope.PageIsReadonly = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
        $scope.disableAddTransAdjBtn = ((data.collectorClaimSummaryViewModel.ClaimCommonModel.StatusString == "Approved") || $scope.PageIsReadonly);

        //Inbound
        $scope.inboundModel = data.collectorClaimSummaryViewModel.InboundList;
        $scope.totalInbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalInbound);
        $scope.tireOriginItems = data.TireOriginItems;
        $scope.completeTireOriginItems = angular.copy($scope.tireOriginItems);
        $scope.tireOriginValidations = data.TireOriginValidations;

        $scope.claimSupportingDocVMList = data.collectorClaimSummaryViewModel.ClaimSupportingDocVMList;
        $scope.supportDocIsRequired = false;
        $scope.claimSupportingDocVMList.forEach(function (item) {
            if (item.SelectedOption === 'optionupload') {
                $scope.supportDocIsRequired = true;
            }
        });

        //ReuseTires
        $scope.reuseTireModel = data.collectorClaimSummaryViewModel.ReuseTire;
        $scope.reuseTireOriginItems = angular.copy($scope.tireOriginItems);

        //Outbound
        $scope.TCR = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TCR);
        $scope.DOT = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.DOT);
        //OTSTM2-588
        $scope.TotalEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalEligible);
        $scope.TotalIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalIneligible);
        $scope.TotalOutbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalOutbound);

        //OTSTM2-588 
        //Adjustment
        $scope.TotalAdjustmentEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentEligible);
        $scope.TotalAdjustmentIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentIneligible);
        $scope.TotalAdjustments = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustments);

        //Submit Info
        $scope.submitInfo = {
            periodShortName: data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimPeriodName,
            registrationNum: data.collectorClaimSummaryViewModel.ClaimCommonModel.RegistrationNumber
        };

        $scope.readOnlyRestriction = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;

        $scope.dataloaded = true;

        $scope.InternalAdjustTypes = [
           {
               name: 'Weight',
               id: '1'
           },
           {
               name: 'Tire Counts',
               id: '2'
           },
           {
               name: 'Yard Count',
               id: '3'
           },
           {
               name: 'Payment',
               id: '4'
           }
        ];

        $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
            var viewItem = $filter('filter')($scope.InternalAdjustTypes, { name: internalAdjustType })[0];
            claimService.getInternalAdjustment(viewItem.id, internalAdjustId).then(function (data) {

                var eligibleAdjustmentModalInstance = $uibModal.open({ 
                    templateUrl: 'InternalAdjustAddAdjustment.html',
                    controller: 'PEligibleAdjustmentCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        chooseTypes: function () {
                            return $scope.InternalAdjustTypes;
                        },
                        modalResult: function () {
                            return data;
                        },
                        selectedItem: function () {
                            return viewItem;
                        },
                        chooseTypeIsDisable: function () {
                            return true;
                        },
                        isReadOnly: function () {
                            return true;
                        }
                    }
                });
                eligibleAdjustmentModalInstance.result.then(function (modalResult) {
                    var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                        templateUrl: 'internalAdjustConfirmModal.html',
                        controller: 'PEligibleAdjustmentConfirmCtrl',
                        size: 'lg',
                        resolve: {
                            modalResult: function () {
                                return modalResult;
                            }
                        }
                    });
                    eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                        claimService.editInventoryAdjustment(modalResult).then(function (data) {
                            if (data.status == "refresh") {
                                refreshPanels();
                            }
                        });
                    });
                });
            });

        };

        $scope.rbNewValue = function (value) {
            collectorClaimService.updateSupportingDocOption({ claimID: this.supportingDocItem.ClaimId, defValue: this.supportingDocItem.DefinitionValue, option: this.supportingDocItem.SelectedOption }).then(function (result) {
                if ($("input:radio[value='optionupload']:checked").length > 0) {
                    $scope.supportDocIsRequired = true;
                } else {
                    $scope.supportDocIsRequired = false;
                }
            });
        };

        $scope.$watch(function () { return collectorClaimService.ReloadSupportingDocs; }, function (val) {
            $http({ url: Global.ParticipantClaimsSummary.LoadSupportingDocsUrl, method: "POST", data: { claimId: $scope.claimId } }).then(function (result) {
                $scope.claimSupportingDocVMList = result.data;
                $scope.supportDocIsRequired = false;
                $scope.claimSupportingDocVMList.forEach(function (item) {
                    if (item.SelectedOption === 'optionupload') {
                        $scope.supportDocIsRequired = true;
                    }
                });
            });
            collectorClaimService.ReloadSupportingDocs = false;
        });

        $scope.$watch(function () { return $scope.supportDocIsRequired; }, function (newVal, oldVal) {
            if (!newVal) {//not require support doc
                $('#fileUploadDropzone').toggleClass('has-error', false).toggleClass('dropzone-required', false).toggleClass('dropzone', true);
                $scope.isRequiredErrorFlag = false;
            }
        });

        $scope.dropZoneStyle = function () {
            if ($scope.isRequiredError())
                return "dropzone-required";
            else
                return "dropzone";
        };

        $('#filesToUploadGrid').on('update', function (e, msg) {//called while add/remove support file
            collectorClaimService.updateUI(e,msg);
        });

    });

    var category = 1;
    $scope.loadTopInfoBar = function () {
        var isSpecific = true;
        var addNewRateModal = $uibModal.open({
            templateUrl: "collector-modal-load-top-info-bar.html",
            controller: 'loadTopInfoCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                category: function () {
                    return $scope.category;
                },
                vendorId: function () {
                    return $scope.vendorId;
                },
                claimPeriod: function () {
                    return $scope.claimPeriod;
                }
            }
        });
    }

}]);

collectorClaimApp.controller('TireRemoveModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'removeContent', 'collectorClaimService', function ($rootScope, $scope, $http, $uibModalInstance, removeContent, collectorClaimService) {
    $scope.removeContent = removeContent;

    if ($scope.removeContent.type == 1) {
        $scope.removeMessage = removeContent.item.TireOrigin;
        $scope.messageType = Global.ParticipantClaimsSummary.TiresOriginRemoveMessage;
    }
    if ($scope.removeContent.type == 2) {
        $scope.removeMessage = '';  //removeContent.item.ReuseTires;
        $scope.messageType = Global.ParticipantClaimsSummary.ReuseTiresOriginRemoveMessage;
    }

    $scope.confirm = function () {
        if ($scope.removeContent.type == 1) {
            collectorClaimService.removeTireOrigin($scope.removeContent.item);            
        }
        if ($scope.removeContent.type == 2) {
            collectorClaimService.removeReuseTireOrigin($scope.removeContent.item);            
        }
        $uibModalInstance.close($scope.removeContent);

        if (($scope.removeContent.item.TireOriginValue == 2) || ($scope.removeContent.item.TireOriginValue == 3) || ($scope.removeContent.item.TireOriginValue == 4) || ($scope.removeContent.item.TireOriginValue == 9)) {
            collectorClaimService.ReloadSupportingDocs = true;
        }

    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('TireWarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//OTSTM2-543
collectorClaimApp.controller('TireReminderModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'reminderMessage', function ($rootScope, $scope, $http, $uibModalInstance, reminderMessage) {
    $scope.reminderMessage = reminderMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('ErrorModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'errorMessage', function ($rootScope, $scope, $http, $uibModalInstance, errorMessage) {
    $scope.errorMessage = errorMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('WarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('ModalSubmitOneModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'collectorClaimService', 'submitClaimModel', function ($rootScope, $scope, $http, $uibModalInstance, $window, collectorClaimService, submitClaimModel) {
    $scope.finalSubmitClaim = function () {
        collectorClaimService.FinalSubmitClaim(submitClaimModel).then(function (data) {
            if (data.status == "Valid Data") {
                $scope.submitBtn = false;
                $uibModalInstance.close($scope.submitBtn);
                //$window.location.reload();
            }            
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('ModalSubmitTwoModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'submitInfo', '$window', function ($rootScope, $scope, $http, $uibModalInstance, submitInfo, $window) {

    $scope.submitInfo = submitInfo;

    $scope.confirm = function () {
        $uibModalInstance.close();
        $window.location.reload();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('PEligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;

    $scope.confirm = function () {
        $scope.modalResult.selectedItem = $scope.selectedItem;
        $uibModalInstance.close($scope.modalResult);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('PEligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('loadTopInfoCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'vendorId', 'claimPeriod', 'collectorClaimService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, category, vendorId, claimPeriod, collectorClaimService, $rootScope) {
    var params = {
        vendorId: vendorId,
        claimPeriodStart: claimPeriod.StartDate,
        claimPeriodEnd: claimPeriod.EndDate,
        rateGroupCategory: 'PickupGroup',
    };

    collectorClaimService.getCollectorTopInfoPickupRateGroupDetail(params).then(function (result) {
        $scope.vm = result;
        console.log($scope.vm.data);
        $scope.vm.isView = true;
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };
}]);