﻿//Collector participant-index.js

$(function () {
    CommonValidation.initializeStandardFormValidations();
    BusinessLocation();
    ContactInformation();  
    TireDetails();
    CollectorDetails();
    TermsAndConditions();

    var initialize = function () {
        titleCase();
        //restrictOnlyNumbers();
        //DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        DisplayEASR_ECA($('#CollectorDetails_BusinessActivity').val(), 'Auto Dismantler');
        InitAutoSave();
    }
    initialize();

    $('#submitApplicationBtn').on('click', function () {
        var Id = $("[name='ID']").val();

        $.ajax({
            url: '/Collector/Registration/SubmitApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: Id, tokenId: Global.ParticipantIndex.Resources.TokenId },
            complete: function () {
                window.location.reload();
            }
        });
    });
    if ($("#isBackToApplicant").val() == "True") {
        if ($('#CollectorDetails_IsTaxExempt').is(':checked')) {
            $('#flagCollectorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
            $('#CollectorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
            $('#CollectorDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
        }
        else {
            $('#flagCollectorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
            $('#CollectorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
            $('#CollectorDetails_CommercialLiabHstNumber').attr('readonly', false);
        }
    }
    //fix backspace error for checkboxes
    $('input[type="checkbox"]').keydown(function (e) {
        if (e.keyCode === 8) {
            return false;
        }
    });
});

function BusinessLocation() {
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    $('#MailingAddressSameAsBusiness').click(function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {
            CopyMailingAddress();
        }
        else {
            ClearMailingAddress();
        }
        toggleMailingAddress();
    });

    toggleMailingAddress();
}

function CopyMailingAddress() {
    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('input[name="BusinessLocation.MailingAddress.City"]').val($('#BusinessLocation_BusinessAddress_City').val())
    $('input[name="BusinessLocation.MailingAddress.Province"]').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('select[name="BusinessLocation.MailingAddress.Country"]').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearMailingAddress() {
    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val('')
    $('input[name="BusinessLocation.MailingAddress.City"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Province"]').val('')
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function ContactInformation() {
    $('.isContactAddressSameAsBusinessAddress').on('click', function () {
        contactAddressSameAs();
    });

    //#region
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        var fieldname = $("#contactTemplate" + i).data('fieldname').replace('Contact 0', 'Primary Contact');
        contactTemplate = "<div class='template" + i + " template' data-fieldname='" + fieldname + "'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }
    $('#btnAddContact').on('click', function () {
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
        titleCase();
    });

    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            currAddCount = contactTemplateCount++;
            temp = "<div class='ctemp'><div class='template" + currAddCount + "'>" + temp + '</div></div><hr>';
            $(temp).appendTo('#divContactTemplate');

            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }

    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            /*
                if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                    if ($(this).is(":checked")) {
                        $(this).closest('.row').next('.contactAddressDifferent').hide();
                    }
                    else {
                        $(this).closest('.row').next('.contactAddressDifferent').show();
                    }
                }
            */
        });
    }

    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }
    primaryContactCheckName();
}

function TireDetails() {
    //code for collector details to go here if required
    $('input.numberinput').on('change', function () {
        var number = Number(this.value);
        if (number) {
            if (0.0 > number) {
                this.value = '';
            }
        }
        else {
            this.value = '';
        }
    });

    $('input[type=checkbox][name="TireDetails.TireItem"]').change(function () {
        $("#fmTireDetails").valid();
    });

    $('select').on('change', function () {
        selectedText = CollectorDetails_BusinessActivity.options[CollectorDetails_BusinessActivity.selectedIndex].text.toLowerCase();
        //var d = Date.prototype.getUTCTime();
        if (('generator' == selectedText)) {
            var now = new Date;
            var utc_timestamp = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());

            var d = new Date();
            var n = d.toUTCString();

            $('#GeneratorStatus').val(true);
            $('#GeneratorDate').val(n);
        } else {
            $('#GeneratorStatus').val('');
            $('#GeneratorDate').val('');
        }
    });
}

function CollectorDetails() {
    //applies to radiobutton for CollectorDetails.CollectorDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    //OTSTM2-486
    $('#CollectorDetails_BusinessActivity').change(function () {
        DisplayEASR_ECA(this.value, 'Auto Dismantler');
    });

    $('#CollectorDetails_CollectorDetailsVendor_WSIBNumber').on('focusout', function () {
        DisplayWSIBDocumentOption();
    });

    $('#CollectorDetails_CollectorDetailsVendor_CommercialLiabHSTNumber').on('focusout', function () {
        DisplayHSTDocumentOption();
    });

    $('#CollectorDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });
    var disabledHSTTextBox = function () {
        if (!($('#CollectorDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#CollectorDetails_IsTaxExempt').is(':checked')) {
                $('#CollectorDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
            }
            else {
                $('#CollectorDetails_CommercialLiabHstNumber').attr('readonly', false);
            }
        }
    }

    $('#CollectorDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });

    disabledHSTTextBox(); 

    $('input[type="radio"]').click(function () {
        // For collector Cvor detail.
        if ($(this).attr('id') == 'CollectorDetails_CollectorDetailsVendor_HIsGVWR') {
            if ($(this).val() == 'True') {
                $('#CVORNumberCollector').show();
                $('#CVORExpiryDateCollector').show();
            }
            else {
                $('#CVORNumberCollector').hide();
                $('#CVORExpiryDateCollector').hide();
            }
        }
        ////If collector have 1 or more employee.
        //if ($(this).attr('id') == 'CollectorDetails_HasMoreThanOneEmp') {
        //    if ($(this).val() == 'True') {
        //        $('#WSIBNumberCollector').show();
        //    }
        //    else {
        //        $('#WSIBNumberCollector').hide();
        //    }
        //}
    });

    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    $('#dpBusinessStartDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpInsuranceExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#pnlWSIB').toggle($('.radio input.wsib:checked').val() == 'True');
}

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
}

/* Common Functions */
var restrictOnlyNumbers = function () {
    $(".only-numbers").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 16 && !(event.keyCode >= 96 && event.keyCode <= 105) && (event.keyCode < 48 || event.keyCode > 57)) {
                event.preventDefault();
            }
        }
    });
}


var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
        //$(this).closest('.row').next('.contactAddressDifferent').find('input:checkbox').removeAttr('checked')
    });
}

//capitalize first letter of address
var titleCase = function () {
    //$('.title-case').on('keyup', function () {
    //    var str = $(this).val();
    //    var r = str.match(/[A-Za-z]/gi);
    //    if (r) {
    //        var i = str.indexOf(r[0]);
    //        if (i > -1) {
    //            var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
    //            $(this).val(titleCaseStr);
    //        }
    //    }
    //});
}

function processJson() {
    var myform = $('#MainId');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var data = $('form').serializeArray();
    data = data.concat(
        $('#MainId input[type=checkbox]:not(:checked)').map(
            function () {
                return { "name": this.name, "value": this.value }
            }).get());
    data = data.concat(
        $('#MainId input[type=radio]:not(:checked)').map(
            function () {
                return { "name": this.name, "value": null }
            }).get());
    //data.push({ name: 'ID', value: $('#hdnRegId').val() });

    $('#item_checkbox input[type="checkbox"]').each(function () {
        if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
            data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
        }
    });

    if ($('#MailingAddressSameAsBusiness').is(':checked')) {
        data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
    }
    disabled.attr('disabled', 'disabled');

    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant') {
        //insert current invalid forms back into model
        var tmpList = Global.ParticipantIndex.Model.InvalidFormFields;
        for (var i = 0; i < tmpList.length; i++) {
            data.push({ name: 'InvalidFormFields[]', value: tmpList[i] });
        }
    }

    if (Global.ParticipantIndex.Model.Status == 'Open' || Global.ParticipantIndex.Model.Status == 'None') {
        var len = data.length;
        for (var j = 0; j < len; j++) {
            data.push({ name: 'InvalidFormFields[]', value: data[j].name });
        }
    }
    //remove leading and trailing space
    var len = data.length;
    for (var i = 0; i < len; i++) {
        data[i].value = $.trim(data[i].value);
    }

    xhr = $.ajax({
        url: '/Collector/Registration/SaveModel',
        dataType: 'json',
        type: 'POST',
        data: $.param(data),
        success: function (result) {
            if (result) {
                switch (result.toLowerCase()) {
                    case "success":
                        //if (Global.CommonActivity.ActivitiesLog && Global.CommonActivity.ApplicationStatus != 'Open' && Global.CommonActivity.ApplicationStatus != 'None') {
                        //    $.ajax({
                        //        url: '/System/Common/AddCommonActivity',
                        //        dataType: 'json',
                        //        type: 'POST',
                        //        data: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: 'Collector', logContent: Global.CommonActivity.ActivitiesLog },
                        //        success: function (result) {
                        //            if (result) {
                        //                switch (result.status) {
                        //                    case "Valid Data":
                        //                        break;
                        //                    default:
                        //                        break;
                        //                }
                        //            }
                        //        },
                        //        error: function (result) {
                        //            console.log('error:', result);
                        //        }
                        //    });
                        //}
                        break;
                    default:
                        window.location = 'http://' + window.location.hostname + '/Collector/Registration/Message?msg=' + result;
                        break;
                }
            }
        },
        error: function (result) {
            console.log('SaveModel error');
        }
    });
    //abortAjax(xhr);
}

function abortAjax(xhr) {
    if (xhr && xhr.readystate != 4) {
        xhr.initclass();
    }
}

var certificateOfApproval = function () {
    var allSortYards = 0;
    var capp = $("input[data-maxstorage]");
    capp.each(function () {
        $(this).attr('data-maxstorage', $(this).val());

        if (parseFloat($(this).attr('data-maxstorage')) > 0) {
            allSortYards++;
        }
        if (parseFloat($(this).attr('data-maxstorage')) < 50) {
            var certApp = $(this).parents('.row').find('.COA');
            $(certApp).find('input:text').each(function () {
                $(this).val('');
            });
            $(this).parents('.row').find('.COA').hide();
        }
        else {
            $(this).parents('.row').find('.COA').show();
        }
    });
    $('#SortYardCount').text(allSortYards);
}

var totalStorage = function () {
    var maxStorageCap = 0;
    var inputs = $("input[data-maxstorage]");
    inputs.each(function () {
        if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
            maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        }
    });
}

function DisplayWSIBDocumentOption() {
    var show = ($('#CollectorDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
    $('#pnlWSIB').toggle(show);
    PanelGreenCheckSuppDoc();
}

function DisplayEASR_ECA(selectedOption, autoDismantler) {
    $('#eASR_eCADocument').toggle(selectedOption.toLowerCase() === autoDismantler.toLowerCase());
}

function DisplayHSTDocumentOption() {
    var show = !$('#CollectorDetails_IsTaxExempt').is(':checked');
    $('#hSTDocument').toggle(show);
    PanelGreenCheckSuppDoc();
}

function _isEmpty(value) {
    return (value == null || value.length === 0);
}

function InitAutoSave() {
    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        $('#MainId :input').eq(0).trigger('change');
    });

    $('#MainId :input').on('change', function () {
        var apphauler = $(this);
        $.when(
        apphauler.focusout()).then(function () {
            //certificateOfApproval();
            processJson();
            //totalStorage();
            contactAddressSameAs();
        });
    });

    $('#MainId').on('change', '.ctemp', function () {
        var ctemp = $(this);
        $.when(
        ctemp.focusout()).then(function () {
            certificateOfApproval();
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });

    //$('#ApplicationHaulerID :input').on('change', function () {
    //    var apphauler = $(this);
    //    $.when(
    //    apphauler.focusout()).then(function () {
    //        certificateOfApproval();
    //        processJson();
    //        totalStorage();
    //        contactAddressSameAs();
    //    });
    //});

    //$('#ApplicationHaulerID').on('change', '.ctemp', function () {
    //    var ctemp = $(this);
    //    $.when(
    //    ctemp.focusout()).then(function () {
    //        certificateOfApproval();
    //        processJson();
    //        totalStorage();
    //        contactAddressSameAs();
    //    });
    //});
}

/* End Common Functions */

/* Gabe UI styles */
(function () {
    // Popover Setup
    var settings = {
        trigger: 'hover',
        //title:'Pop Title',
        //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
        //width:300,
        multi: true,
        closeable: true,
        style: '',
        delay: { show: 300, hide: 800 },
        padding: true
    };

    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
      businessLocationLBNSettings = {
          content: businessLocationLBNContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
      businessLocationBONSettings = {
          content: businessLocationBONContent,
          width: 300,
          height: 100,
      };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationBONSettings));

    // Sort Yard Details: All Sort Yards Capacity
    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
      sortYardDetailsASYCSettings = {
          content: sortYardDetailsASYCContent,
          width: 300,
          height: 100,
      };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsASYCSettings));

    // Sort Yard Details: This Sort Yard Capacity
    var sortYardDetailsTSYCContent = $('#sortYardDetailsTSYC').html(),
      sortYardDetailsTSYCSettings = {
          content: sortYardDetailsTSYCContent,
          width: 300,
          height: 100,
      };
    var popLargeTSYC = $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsTSYCSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
      tiredetailsPopoverPLTSettings = {
          content: tiredetailsPopoverPLTContent,
          width: 500,
          height: 210,
      };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
      tiredetailsPopoverMTSettings = {
          content: tiredetailsPopoverMTContent,
          width: 380,
          height: 160,
      };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverAGLSSettings));
    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
      tiredetailsPopoverINDSettings = {
          content: tiredetailsPopoverINDContent,
          width: 360,
          height: 120,
      };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverINDSettings));

    // Tire Details: Small Off the Road Tire (SOTR)
    var tiredetailsPopoverSOTRContent = $('#tiredetailsPopoverSOTR').html(),
      tiredetailsPopoverSOTRSettings = {
          content: tiredetailsPopoverSOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeSOTR = $('.tiredetailsPopoverSOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverSOTRSettings));

    // Tire Details: Medium Off the Road Tire (MOTR)
    var tiredetailsPopoverMOTRContent = $('#tiredetailsPopoverMOTR').html(),
      tiredetailsPopoverMOTRSettings = {
          content: tiredetailsPopoverMOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeMOTR = $('.tiredetailsPopoverMOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMOTRSettings));

    // Tire Details: Large Off the Road Tire (LOTR)
    var tiredetailsPopoverLOTRContent = $('#tiredetailsPopoverLOTR').html(),
      tiredetailsPopoverLOTRSettings = {
          content: tiredetailsPopoverLOTRContent,
          width: 420,
          height: 120,
      };
    var popLargeLOTR = $('.tiredetailsPopoverLOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverLOTRSettings));

    // Tire Details: Giant Off the Road Tire (GOTR)
    var tiredetailsPopoverGOTRContent = $('#tiredetailsPopoverGOTR').html(),
      tiredetailsPopoverGOTRSettings = {
          content: tiredetailsPopoverGOTRContent,
          width: 420,
          height: 100,
      };
    var popLargeGOTR = $('.tiredetailsPopoverGOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverGOTRSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBLC').html(),
      supportingDocumentsAIMBLSettings = {
          content: supportingDocumentsAIMBLContent,
          width: 500,
          height: 140,
      };
    var popLargeAIMBL = $('.supportingDocumentsAIMBLC').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLIC').html(),
      supportingDocumentsCLISettings = {
          content: supportingDocumentsCLIContent,
          width: 500,
          height: 260,
      };
    var popLargeCLI = $('.supportingDocumentsCLIC').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCLISettings));

    // Supporitng Documents: HST # Certificate
    var supportingDocumentsHSTContent = $('#supportingDocumentsHSTC').html(),
      supportingDocumentsHSTSettings = {
          content: supportingDocumentsHSTContent,
          width: 500,
          height: 160,
      };
    var popLargeCLI = $('.supportingDocumentsHSTC').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsHSTSettings));

    // Supporitng Documents: WSIB Certificate
    var supportingDocumentsWSIBContent = $('#supportingDocumentsWSIBC').html(),
      supportingDocumentsWSIBSettings = {
          content: supportingDocumentsWSIBContent,
          width: 500,
          height: 86,
      };
    var popLargeCLI = $('.supportingDocumentsWSIBC').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsWSIBSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCHC').html(),
      supportingDocumentsVOCHSettings = {
          content: supportingDocumentsVOCHContent,
          width: 674,
          height: 359,
      };
    var popLargeVOCH = $('.supportingDocumentsVOCHC').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsVOCHSettings));

    // Tire Details & Service Area: Zone 1
    var serviceAreaZone1Content = $('#serviceAreaZone1').html(),
      serviceAreaZone1Settings = {
          content: serviceAreaZone1Content,
          width: 674,
          height: 359,
      };
    var popLargeVOCH = $('.serviceAreaZone1').webuiPopover('destroy').webuiPopover($.extend({}, settings, serviceAreaZone1Settings));

    $('#MainId :input').each(function () {
        this.defaultValue = this.value;
        if (this.type == 'select-one') {
            this.defaultValue = this.options[this.selectedIndex].text;
        }
        if (this.type == 'radio' && this.checked) {
            $(this.closest('tbody > tr')).attr('data-reportval', this.value);
        }
    })

})();

// Script; Expand on checkbox deselect
$('.isMailingAddressDifferent').click(function () {
    $(".mailingAddressDifferent").toggle(this.unchecked);
});
$('.isContactAddressDifferent').click(function () {
    $(".contactAddressDifferent").toggle(this.unchecked);
});
$('.isBankTransferNotficationPrimaryContact').click(function () {
    $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
});
/* End Gabe UI Styles */