﻿using MicrosoftSystem = System.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SystemIO = System.IO;
using MicrosoftWeb = System.Web;
using MicrosoftIO = System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Hauler;
using Newtonsoft.Json;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.Helper;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.DataContracts.Adapter;
using System.Text;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ExceptionHandling;
using CL.TMS.Configuration;
using CL.TMS.Web.Infrastructure;
using CL.TMS.PDFGenerator;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.Web.Areas.Collector.Controllers
{
    [AllowAnonymous]
    public class RegistrationController : BaseController
    {
        #region Fields
        private ICollectorRegistrationService collectorRegistrationService;
        private ICollectorRegistrationInvitationService applicationInvitationService; //IApplicationInvitationService        
        private IApplicationService applicationService; //IApplicationService applicationService;
        private IUserService userService;
        private IRegistrantService registrantService;
        private IFileUploadService fileUploadService;
        private ICompanyBrandingServices companyBrandingService;
        private IMessageService messageService;
        private string uploadRepositoryPath;
        #endregion

        #region Constructors
        public RegistrationController(ICollectorRegistrationInvitationService applicationInvitationService, ICollectorRegistrationService collectorRegistrationService,
            IUserService userService, IApplicationService applicationService, IRegistrantService registrantService, IFileUploadService fileUploadService, ICompanyBrandingServices companyBrandingService,
            IMessageService messageService)
        {
            this.collectorRegistrationService = collectorRegistrationService;
            this.applicationInvitationService = applicationInvitationService;
            this.applicationService = applicationService;
            this.registrantService = registrantService;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.companyBrandingService = companyBrandingService;
            this.messageService = messageService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }
        #endregion

        #region Action Methods

        [AllowAnonymous]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsCollectorApplication)]
        [HttpGet]
        public ActionResult Index(Guid id)
        {
            if (SiteSettings.Instance.ApplicationInstance == "PRO")
                ViewBag.ngApp = "vendorRegistrationGlobalApp";
            CollectorRegistrationModel model = collectorRegistrationService.GetByTokenId(id);

            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }

            int appID = applicationInvitationService.GetByTokenID(id).ApplicationID.Value;
            ApplicationStatusEnum status = (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), applicationService.GetByApplicationID(appID).Status);

            if ((status == ApplicationStatusEnum.Open) || (status == ApplicationStatusEnum.BackToApplicant))
            {
                if ((model.ExpireDate != null) && (model.ExpireDate < DateTime.UtcNow))
                {
                    return RedirectToAction("InvitationExpired", "AppInvitation", new { area = "System" });
                }
            }

            if (status == ApplicationStatusEnum.Approved || status == ApplicationStatusEnum.Completed || status == ApplicationStatusEnum.BankInformationSubmitted || status == ApplicationStatusEnum.BankInformationApproved)
            {
                return RedirectToAction("RegistrationIndex", "Registration", new { vId = model.VendorID });
            }
            else
            {
                #region from form object

                if (status == ApplicationStatusEnum.BankInformationSubmitted || status == ApplicationStatusEnum.BankInformationApproved)
                {
                    var BankInfo = registrantService.GetBankInformation(model.VendorID);

                    if (BankInfo.ReviewStatus == BankInfoReviewStatus.Submitted.ToString() || BankInfo.ReviewStatus == BankInfoReviewStatus.Approved.ToString())
                    {
                        model.BankingInformation = AutoMapper.Mapper.Map<BankingInformationRegistrationModel>(BankInfo);
                    }
                }

                #region prepare Model

                if ((null == model.ContactInformationList) || (model.ContactInformationList.Count == 0))
                {
                    model.ContactInformationList = new List<ContactRegistrationModel>() { };
                    model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
                }
                if (null == model.ContactInformationList[0].ContactAddress)
                {
                    model.ContactInformationList[0].ContactAddress = new ContactAddressModel();
                }
                if (model.TireDetails == null)
                {
                    model.TireDetails = collectorRegistrationService.GetAllItemsList().TireDetails;
                }
                model.TireDetails.Status = model.Status;

                if (model.SupportingDocuments == null)
                {
                    model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
                }
                switch (status)
                {
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                        model.SupportingDocuments.ID = model.VendorID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetVendorAttachments(model.VendorID).Count();
                        model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Registrant;
                        break;
                    default:
                        model.SupportingDocuments.ID = model.ID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetAttachments(model.ID).Count();
                        break;
                }

                model.BusinessLocation.Status = model.Status;
                foreach (var contact in model.ContactInformationList)
                {
                    contact.Status = model.Status;
                }
                foreach (var contact in model.ContactInformationList)
                {
                    contact.Status = model.Status;
                }

                model.CollectorDetails.Status = model.Status;
                model.TermsAndConditions.Status = model.Status;
                model.TermsAndConditions.ParticipantType = "Collector";
                //OTSTM2-973 TermsAndConditions
                model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

                model.SupportingDocuments.ParticipantType = "Collector";

                model.SupportingDocuments.Status = model.Status;

                model.RegistrantActiveStatus = model.RegistrantActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantActiveStatus;
                model.RegistrantInActiveStatus = model.RegistrantInActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantInActiveStatus;


                #endregion

                var temp = DataLoader.CollectorBusinessActivities;
                var List = new List<SelectListItem>();
                foreach (var element in temp)
                {
                    List.Add(new SelectListItem
                    {
                        Value = element.Name,
                        Text = element.Name
                    });
                }
                model.CollectorDetails.BusinessActivites = List;
                var user = TMSContext.TMSPrincipal;

                if (user.Identity.IsAuthenticated)
                {
                    if (SecurityContextHelper.CurrentVendor != null)
                        ViewBag.ParticipantInfo = SecurityContextHelper.CurrentVendor;

                    if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                    {
                        if (model.VendorID != 0 && (status == ApplicationStatusEnum.Approved
                            || status == ApplicationStatusEnum.Completed
                            || status == ApplicationStatusEnum.BankInformationSubmitted
                            || status == ApplicationStatusEnum.BankInformationApproved))
                        {
                            var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForVendor(model.VendorID);
                            var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, false);
                            var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, true);

                            model.ActiveInactive.RegistrantActive = previousActiveHistory;
                            model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                            model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                            if (model.ActiveInactive.RegistrantInactive == null)
                            {
                                model.ActiveInactive.RegistrantInactive = new VendorActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }
                            if (model.ActiveInactive.RegistrantActive == null)
                            {
                                model.ActiveInactive.RegistrantActive = new VendorActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }

                            model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                            model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                            model.ActiveInactive.Status = model.Status;
                        }
                        //OTSTM2-850, use the common code about Claim Activity             
                        ViewBag.ClaimsId = appID;
                        if (SecurityContextHelper.IsStaff())
                        {
                            ViewBag.HasActivityPanel = 1;
                        }
                        return View("StaffIndex", model);
                    }
                    return View("ParticipantIndex", model);
                }
                if (null != applicationService)
                {
                    switch (
                        (ApplicationStatusEnum)
                            Enum.Parse(typeof(ApplicationStatusEnum),
                                applicationService.GetByApplicationID(appID).Status))
                    {
                        case ApplicationStatusEnum.Open:
                        case ApplicationStatusEnum.BackToApplicant:
                            break;
                        case ApplicationStatusEnum.Submitted:
                        case ApplicationStatusEnum.Approved:
                        case ApplicationStatusEnum.BankInformationSubmitted:
                        case ApplicationStatusEnum.BankInformationApproved:
                        case ApplicationStatusEnum.Completed:
                        case ApplicationStatusEnum.OnHold:
                        case ApplicationStatusEnum.Denied:
                        case ApplicationStatusEnum.Assigned:
                        default:
                            return RedirectToAction("ApplicationSubmittedPage", "Default", new { area = "" });
                    }
                }
                return View("ParticipantIndex", model);

                #endregion
            }

        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsCollectorApplication)]
        [HttpGet]
        public ActionResult RegistrationIndex(int vId)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if (SiteSettings.Instance.ApplicationInstance == "PRO")
                    ViewBag.ngApp = "vendorRegistrationGlobalApp";
            }
            CollectorRegistrationModel model = collectorRegistrationService.GetApprovedApplication(vId);
            model.VendorID = vId;
            ViewBag.SelectedMenu = "Applications";
            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }

            //OTSTM2-427: set up global search  
            var vendor = registrantService.GetVendorById(vId);
            if (vendor != null)
            {
                //Keep it in session
                //Vendor Context Check
                if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 2)
                {
                    Session["SelectedVendor"] = vendor;
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            if (model.Status == ApplicationStatusEnum.BankInformationSubmitted || model.Status == ApplicationStatusEnum.BankInformationApproved)
            {
                var BankInfo = registrantService.GetBankInformation(vId);

                if (BankInfo.ReviewStatus == BankInfoReviewStatus.Submitted.ToString() || BankInfo.ReviewStatus == BankInfoReviewStatus.Approved.ToString())
                {
                    model.BankingInformation = AutoMapper.Mapper.Map<BankingInformationRegistrationModel>(BankInfo);
                }
            }

            #region prepare Model


            if ((model.ContactInformationList == null) || (model.ContactInformationList.Count == 0))
            {
                model.ContactInformationList = new List<ContactRegistrationModel>() { };
                model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
            }
            if (model.ContactInformationList[0].ContactAddress == null)
            {
                model.ContactInformationList[0].ContactAddress = new ContactAddressModel();
            }
            if (model.TireDetails == null)
            {
                model.TireDetails = collectorRegistrationService.GetAllItemsList().TireDetails;
            }
            model.TireDetails.Status = model.Status;

            if (model.SupportingDocuments == null)
            {
                model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Registrant };
            }
            model.SupportingDocuments.ID = vId;
            model.SupportingDocuments.CountOfUploadedDocuments = GetVendorAttachments(vId).Count();
            model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Registrant;

            model.BusinessLocation.Status = model.Status;
            foreach (var contact in model.ContactInformationList)
            {
                contact.Status = model.Status;
            }
            foreach (var contact in model.ContactInformationList)
            {
                contact.Status = model.Status;
            }

            model.CollectorDetails.Status = model.Status;
            model.TermsAndConditions.Status = model.Status;
            model.TermsAndConditions.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("collector") > -1).Name;
            //OTSTM2-973 TermsAndConditions
            model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

            model.SupportingDocuments.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("collector") > -1).Name;

            model.SupportingDocuments.Status = model.Status;

            model.RegistrantActiveStatus = model.RegistrantActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantActiveStatus;
            model.RegistrantInActiveStatus = model.RegistrantInActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantInActiveStatus;

            #endregion


            var temp = DataLoader.CollectorBusinessActivities;
            var List = new List<SelectListItem>();
            foreach (var element in temp)
            {
                List.Add(new SelectListItem
                {
                    Value = element.Name,
                    Text = element.Name
                });
            }
            model.CollectorDetails.BusinessActivites = List;
            var user = TMSContext.TMSPrincipal;

            if (user.Identity.IsAuthenticated)
            {
                if (SecurityContextHelper.CurrentVendor != null)
                    ViewBag.ParticipantInfo = SecurityContextHelper.CurrentVendor;

                if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                {
                    var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForVendor(model.VendorID);
                    var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, false);
                    var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, true);

                    model.ActiveInactive.RegistrantActive = previousActiveHistory;
                    model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                    model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                    if (model.ActiveInactive.RegistrantInactive == null)
                    {
                        model.ActiveInactive.RegistrantInactive = new VendorActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }
                    if (model.ActiveInactive.RegistrantActive == null)
                    {
                        model.ActiveInactive.RegistrantActive = new VendorActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }

                    model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                    model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                    model.ActiveInactive.Status = model.Status;

                    //Get Group Name (effective) from vendor reference
                    model.GroupName = vendor.GroupName;
                    //OTSTM2-850, use the common code about Claim Activity             
                    ViewBag.ClaimsId = vendor.ApplicationID;
                    if (SecurityContextHelper.IsStaff())
                    {
                        ViewBag.HasActivityPanel = 1;
                    }

                    return View("StaffIndex", model);
                }
                return View("ParticipantIndex", model);
            }
            if (null != applicationService)
            {
                switch (model.Status)
                {
                    case ApplicationStatusEnum.Open:
                    case ApplicationStatusEnum.BackToApplicant:
                        break;
                    case ApplicationStatusEnum.Submitted:
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                    case ApplicationStatusEnum.OnHold:
                    case ApplicationStatusEnum.Denied:
                    case ApplicationStatusEnum.Assigned:
                    default:
                        return RedirectToAction("ApplicationSubmittedPage", "Default", new { area = "" });
                }
            }

            return View("ParticipantIndex", model);
        }

        [AllowAnonymous]
        public ActionResult NotFound()
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            return View();

        }


        [AllowAnonymous]
        public ActionResult Message()
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SaveModel(CollectorRegistrationModel model)
        {
            try
            {
                var IsCollectorAutoSaveDisabled = false;

                if (SecurityContextHelper.IsStaff())
                {
                    var user = TMSContext.TMSPrincipal;
                    var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Collector")).ToList();
                    IsCollectorAutoSaveDisabled = ApplicationHelper.IsCollectorAutoSaveDisabled(rolePermissionsClaims);
                }

                if (!IsCollectorAutoSaveDisabled)
                {
                    #region TireDetailsItemType

                    PopulateTireDetails(model);

                    #endregion

                    //OTSTM2-50 fix existing issue that check the following checkbox at the first time cannot be remembered after refreshing page
                    model.ContactInformationList.ToList().ForEach(c =>
                    {
                        if (c.ContactAddressSameAsBusinessAddress == false && c.ContactInformation.ContactAddressSameAsBusiness == true)
                        {
                            c.ContactAddressSameAsBusinessAddress = true;
                            c.ContactInformation.ContactAddressSameAsBusiness = false;
                        }
                    });

                    model.UserIPAddress = ("::1" == MicrosoftWeb.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftWeb.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow; //Use UTC time to avoid DateTime Serialze issue in JavaScriptSerializer().Serialize()
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI
                    int appID = model.ID;
                    string updatedBy = MicrosoftWeb.HttpContext.Current.User.Identity.Name;
                    collectorRegistrationService.UpdateUserExistsinApplication(model, appID, updatedBy);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CollectorApplicationParticipantResults(CollectorRegistrationModel model)
        {
            throw new NotImplementedException();
        }

        //Test Comment
        [AllowAnonymous]
        [HttpPost]
        public ActionResult SubmitApplication(int applicationId, Guid tokenId)
        {
            ApplicationInvitation appInvitation = applicationInvitationService.GetByTokenID(tokenId);
            if (appInvitation == null)
            {
                return new HttpStatusCodeResult(401);
            }

            this.collectorRegistrationService.SetApplicationStatus(applicationId, ApplicationStatusEnum.Submitted, null, null);

            string logContent = "<strong>Applicant submitted</strong> the application";
            this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, applicationId);

            return new HttpStatusCodeResult(200);
        }

        public IList<Item> TireDetailsItemType(CollectorRegistrationModel model)
        {

            //This method formats the TireDetailsItems list and creates an item model to populate
            IList<string> tiredetailsItemType = model.TireDetailsItemTypeString;
            var tireDetailsResult = new List<Item>();
            string name, shortname;
            int id;
            bool isChecked;

            foreach (string item in tiredetailsItemType)
            {
                string[] split = item.Split(':');
                name = split[0];
                shortname = split[1];
                Int32.TryParse(split[split.Length - 1], out id);

                if (name.Contains('_'))
                {
                    //chb_name tries to split by _ and get the final portion
                    string[] namesplit = name.Split('_');
                    name = namesplit[namesplit.Length - 1];
                }
                Boolean.TryParse(split[split.Length - 2], out isChecked);
                tireDetailsResult.Add(new Item() { Name = name, ShortName = shortname, ID = id });
            }
            return tireDetailsResult;
        }

        [HttpPost]
        public JsonResult GeneratorOnOff(int vendorId, bool Status, DateTime date)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                this.registrantService.UpdateVendorGeneratorStatus(vendorId, Status, date);

                return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        //OTSTM2-953
        [HttpPost]
        public JsonResult SubCollectorOnOff(int vendorId, bool Status, DateTime date)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                this.registrantService.UpdateVendorSubCollectorStatus(vendorId, Status, date);

                return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        private IEnumerable<AttachmentModel> GetAttachments(int applicationId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (applicationId > 0)
            {
                attachments = this.fileUploadService.GetApplicationAttachments(applicationId);
            }
            return attachments;
        }

        private IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (vendorId > 0)
            {
                attachments = this.fileUploadService.GetVendorAttachments(vendorId);
            }
            return attachments;
        }

        private void DeleteAttachment(int applicationId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadService.GetApplicationAttachment(applicationId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadService.RemoveApplicationAttachment(applicationId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }

        private void PopulateTireDetails(CollectorRegistrationModel model)
        {
            if (model.TireDetails != null)
            {
                if (model.TireDetails.TireDetailsItemType != null)
                {
                    model.TireDetails.TireDetailsItemType = ApplicationHelper.GetTireDetailsItemType(model).ToList();
                }
                else
                {
                    model.TireDetails.TireDetailsItemType = new List<TireDetailsItemTypeModel>();
                }
            }
            else
            {
                model.TireDetails = new CollectorTireDetailsRegistrationModel()
                {
                    TireDetailsItemType = new List<TireDetailsItemTypeModel>()
                };
            }

            model.TireDetails.TireDetailsItemType = ApplicationHelper.GetTireDetailsItemType(model).ToList();
        }

        private IList<TireDetailsItemTypeModel> GetTireDetailsItemType(CollectorRegistrationModel model)
        {
            //This method formats the TireDetailsItems list and creates an item model to populate
            IList<string> tiredetailsItemType = model.TireDetailsItemTypeString;
            var tireDetailsResult = new List<TireDetailsItemTypeModel>();
            string name, shortname, datamyattri;
            int id;
            bool isChecked;

            foreach (string item in tiredetailsItemType)
            {
                string[] split = item.Split(':');
                datamyattri = split[0];
                Boolean.TryParse(split[1], out isChecked);
                Int32.TryParse(split[split.Length - 1], out id);

                if (datamyattri.Contains('-'))
                {
                    //datamyattri tries to split by - for name and shortname and get the final portion
                    string[] namesplit = datamyattri.Split('-');
                    name = namesplit[namesplit.Length - 1];
                    shortname = namesplit[0];

                    tireDetailsResult.Add(new TireDetailsItemTypeModel() { Name = name, ShortName = shortname, isChecked = isChecked, ID = id });
                }
            }
            return tireDetailsResult;
        }

        private bool ComposeAndSendEmail(ApplicationEmailModel model)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(model.Email);
            MailMessage message = new MailMessage(from, to) { };

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.CollectorApplicationApprovedEmailTempl"));
            //string emailBody = MicrosoftIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("ApprovedApplicationCollector");
            string subject = EmailContentHelper.GetSubjectByName("ApprovedApplicationCollector");

            var inviteUrl = string.Format("{0}{1}{2}", SiteSettings.Instance.DomainName, "/System/Invitation/AcceptInvitation/", model.InvitationToken);
            DateTime expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));


            emailBody = emailBody
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.AcceptInvitationUrl, inviteUrl)
                 .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.InvitationExpirationDateTime, expireDate.ToString(TreadMarksConstants.DateFormat) + " at " + expireDate.ToString("hh:mm tt"))
                 .Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = CollectorApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            //string subject = MessageResource.CollectorApplicationApprovedEmailSubject;

            var signModel = new CL.TMS.PDFGenerator.Models.SignModel()
            {
                BusinessName = model.BusinessName,
                RegistrationNumber = model.RegistrationNumber,
                UserID = "1",
                Address = string.Format("{0}, {1}, {2}, {3}, {4}", model.Address1, model.City, model.ProvinceState, model.PostalCode, model.Country),
            };

            Signs pdf = new Signs(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\pdf\Signs_Collector.pdf"));
            pdf.GenerateSingleSign(signModel);

            var attachemnts = new List<CL.TMS.Email.AttachmentType>();
            attachemnts.Add(new CL.TMS.Email.AttachmentType() { Content = pdf.GetContent(), FileName = "QRCode-" + model.RegistrationNumber.ToString() + "-1.pdf" });
            bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            SiteSettings.Instance.GetSettingValue("Invitation.WelcomeSubject");
            Task.Factory.StartNew(() =>
            {
                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, null, bSendBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, model, attachemnts, alternateViewHTML);
            });
            return true;
        }

        private void AddActivity(UserReference assignerUser, string ActivityArea, string logContent, int ObjectId)
        {
            var activity = new Activity();
            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant";
            string tmp = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.';
            this.messageService.AddActivity(new Activity()
            {
                ObjectId = ObjectId,
                InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant",
                CreatedTime = DateTime.Now,
                ActivityType = TreadMarksConstants.VendorRegistrationActivity,// 6: application activity
                ActivityArea = ActivityArea,
                Message = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.',
                Initiator = assignerUser != null ? assignerUser.UserName : "Applicant",
                Assignee = assignerUser != null ? assignerUser.UserName : "Applicant@test.com",
                AssigneeName = assignerUser != null ? assignerUser.UserName : "Applicant",
            });
        }

        #region WorkFlow
        [HttpPost]
        public JsonResult ChangeStatus(string applicationId, string status, long userId = 0, string assgnedUserName = "")
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int applicationID;
            int.TryParse(applicationId, out applicationID);

            if (applicationID == 0) { throw new ArgumentException("applicationId"); }
            ApplicationStatusEnum applicationStatus;
            Enum.TryParse<ApplicationStatusEnum>(status, true, out applicationStatus);

            var user = TMSContext.TMSPrincipal.Identity;
            string updatedBy = user.Name;

            //string userName = MicrosoftWeb.HttpContext.Current.Request.QueryString["userName"];

            if (string.Compare(status, ApplicationStatusEnum.Approved.ToString(), StringComparison.OrdinalIgnoreCase) != 0)
            {
                //Approve status will be updated by approve button
                //return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);

                //if its backtoApplicant send an email
                if (string.Compare(applicationStatus.ToString(), ApplicationStatusEnum.BackToApplicant.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    var collectorApplicationModel = this.collectorRegistrationService.GetFormObject(applicationID);
                    string Email = this.applicationInvitationService.GetEmailByApplicationId(applicationID);
                    var backToApplicantHaulerEmailModel = new ApplicationEmailModel()
                    {
                        BusinessLegalName = collectorApplicationModel.BusinessLocation.BusinessName ?? "Applicant",
                        BusinessName = collectorApplicationModel.BusinessLocation.OperatingName,
                        Address1 = collectorApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                        City = collectorApplicationModel.BusinessLocation.BusinessAddress.City,
                        ProvinceState = collectorApplicationModel.BusinessLocation.BusinessAddress.Province,
                        PostalCode = collectorApplicationModel.BusinessLocation.BusinessAddress.Postal,
                        Country = collectorApplicationModel.BusinessLocation.BusinessAddress.Country,
                        Email = Email,
                    };

                    this.collectorRegistrationService.SendEmailForBackToApplicant(applicationID, backToApplicantHaulerEmailModel);
                }
                this.collectorRegistrationService.SetApplicationStatus(applicationID, applicationStatus, null, updatedBy, userId);
            }


            string logContent = string.Empty;
            switch (status.ToLower())
            {
                case "onhold":
                    logContent = "Application placed <strong>On-Hold</strong> by " + SecurityContextHelper.CurrentUser.FullName;
                    break;
                case "denied":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>rejected</strong>" + " this application";
                    break;
                case "backtoapplicant":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " sent this application <strong>back</strong> to the <strong>Applicant</strong>";
                    break;
                case "approved":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>approved</strong> this application ";
                    break;
                case "completed":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " send this application <strong>back</strong> to the <strong>Applicant</strong>";
                    break;
                case "offhold":
                    logContent = "Application placed <strong>Off-Hold</strong> by " + SecurityContextHelper.CurrentUser.FullName;
                    break;
                case "assigned":
                    logContent = "Application " + " <strong>assigned</strong> to " + "<strong>" + assgnedUserName + "</strong>";
                    break;
                case "unassigned":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>un-assigned</strong> the application from " + "<strong>" + assgnedUserName + "</strong>";
                    break;
                case "reassigned":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>assigned</strong> application to " + "<strong>" + assgnedUserName + "</strong>";
                    break;
                //case "cancel":
                //    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>cancel</strong> the change for this application";
                //    break;
                default:
                    if (userId == -1)//un-assign
                    {
                        logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>un-assigned</strong> the application from " + "<strong>" + assgnedUserName + "</strong>";
                    }
                    break;
            }
            if (applicationID > 0 && !string.IsNullOrEmpty(logContent))
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, applicationID);
            }

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DenyApplication(CollectorApplicationDenyModel collectorApplicationDenyModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Collector")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsCollectorRouting = SecurityContextHelper.CollectorApplicationsWorkflow != null ? SecurityContextHelper.CollectorApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsCollectorRouting)
            {
                int applicationID;
                int.TryParse(collectorApplicationDenyModel.applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }
                string denyReasons = string.Join(",", collectorApplicationDenyModel.validationErrorMessages);
                //TODO: Check for call centre role
                var user = TMSContext.TMSPrincipal.Identity;
                string updatedBy = user.Name;

                this.collectorRegistrationService.SetApplicationStatus(applicationID, ApplicationStatusEnum.Denied, denyReasons, updatedBy);

                var collectorApplicationModel = this.collectorRegistrationService.GetFormObject(applicationID);
                string Email = this.applicationInvitationService.GetEmailByApplicationId(applicationID);
                var approvedHaulerApplicationModel = new ApplicationEmailModel()
                {
                    BusinessLegalName = collectorApplicationModel.BusinessLocation.BusinessName,
                    BusinessName = collectorApplicationModel.BusinessLocation.OperatingName,
                    Address1 = collectorApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                    City = collectorApplicationModel.BusinessLocation.BusinessAddress.City,
                    ProvinceState = collectorApplicationModel.BusinessLocation.BusinessAddress.Province,
                    PostalCode = collectorApplicationModel.BusinessLocation.BusinessAddress.Postal,
                    Country = collectorApplicationModel.BusinessLocation.BusinessAddress.Country,
                    Email = Email
                };

                string message = "";
                if (EmailContentHelper.IsEmailEnabled("NewApplicationDenied"))
                {
                    message = ComposeAndSendDenyEMail(approvedHaulerApplicationModel, collectorApplicationDenyModel.validationErrorMessages);
                }
                else
                    message = "This email is disabled. Please contact us for assistance.";

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        private string ComposeAndSendDenyEMail(ApplicationEmailModel approvedApplicationModel, string[] validationErrorMessages)
        {
            string message = string.Empty;

            try
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.DenyHaulerApplicationEmailTemplLocation"));
                //string emailBody = SystemIO.File.ReadAllText(htmlbody);
                string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationDenied");
                string subject = EmailContentHelper.GetSubjectByName("NewApplicationDenied");

                emailBody = emailBody
                    .Replace(CollectorApplicationEmailTemplPlaceHolders.ApplicationType, "Collector")
                    .Replace(CollectorApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(CollectorApplicationEmailTemplPlaceHolders.BusinessLegalName, approvedApplicationModel.BusinessLegalName)
                    .Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                if (validationErrorMessages != null && validationErrorMessages.Length > 0)
                {
                    var validationErrorMessagesHtmlBlock = new StringBuilder();
                    foreach (var errorMessage in validationErrorMessages)
                    {
                        string validationErrorMessageHtml = string.Format("<p style='color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;'>{0}</p>", errorMessage);
                        validationErrorMessagesHtmlBlock.Append(validationErrorMessageHtml);
                    }
                    emailBody = emailBody.Replace(CollectorApplicationEmailTemplPlaceHolders.ValidationErrorMessages, validationErrorMessagesHtmlBlock.ToString());
                }
                else
                {
                    emailBody = emailBody.Replace(CollectorApplicationEmailTemplPlaceHolders.ValidationErrorMessages, string.Empty);
                }

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = CollectorApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Boolean.Parse(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                //string subject = MessageResource.CollectorDeclinedSubject;
                bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

                //OTSTM2-132 BCC value update
                Task.Factory.StartNew(() =>
                {
                    //OTSTM2-132 BCC value update
                    email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), approvedApplicationModel.Email, null, bSendBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null,
                        subject, emailBody, null, null, alternateViewHTML);
                });

                message = string.Format("Collector application deny email was successfully sent to {0} at email {1}", approvedApplicationModel.BusinessLegalName, approvedApplicationModel.Email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                message = ex.Message;
            }

            return message;
        }

        [HttpPost]
        public JsonResult ApproveApplication(string applicationId)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int selectedVendorGroupId = 0;
            int.TryParse(MicrosoftWeb.HttpContext.Current.Request.Params["SelectedVendorGroupId"], out selectedVendorGroupId);

            int rateGroupId = 0;
            int.TryParse(MicrosoftWeb.HttpContext.Current.Request.Params["EffectiveRateGroupId"], out rateGroupId);

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Collector")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsCollectorRouting = SecurityContextHelper.CollectorApplicationsWorkflow != null ? SecurityContextHelper.CollectorApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsCollectorRouting && (selectedVendorGroupId != 0) && (rateGroupId != 0))
            {
                int applicationID;
                int.TryParse(applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }

                var applicationModel = this.collectorRegistrationService.GetFormObject(applicationID);
                applicationModel.FormObject = string.Empty;
                bool generatorOn = (applicationModel.CollectorDetails.BusinessActivity ?? string.Empty).IndexOf("Generator") > -1;
                applicationModel.CollectorDetails.GeneratorStatus = generatorOn;
                applicationModel.CollectorDetails.GeneratorDate = DateTime.UtcNow;
                applicationModel.CollectorDetails.SubCollectorStatus = false; //OTSTM2-953
                applicationModel.CollectorDetails.SubCollectorDate = DateTime.UtcNow; //OTSTM2-953
                var json = JsonConvert.SerializeObject(applicationModel);
                applicationModel.FormObject = json;
                applicationModel.SelectedVendorGroupId = selectedVendorGroupId;
                applicationModel.rateGroupId = rateGroupId;
                applicationModel.approvedByUserId = SecurityContextHelper.CurrentUser.Id;
                var vendorId = this.collectorRegistrationService.ApproveApplication(applicationModel, applicationID);
                if (vendorId > 0)//approve successfully returned
                {
                    registrantService.AddAppUser(1, vendorId, HttpContext.User.Identity.Name);//create first AppUser then create QRCode with Welcome Email
                    string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>approved</strong> this application ";
                    this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, applicationID);
                }

                //OTSTM2-239 check generator drop down
                bool isGenerator = true;
                if (!string.Equals(applicationModel.CollectorDetails.BusinessActivity, "Generator"))
                {
                    isGenerator = false;
                }

                ApplicationEmailModel approvedApplicationModel = this.collectorRegistrationService.GetApprovedApplicantInformation(applicationID);

                //Create User first to Get invitation token
                approvedApplicationModel.InvitationToken = CreateUserInvitation(approvedApplicationModel, vendorId, applicationID, isGenerator);

                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationCollector"))
                {
                    bool result = ComposeAndSendEmail(approvedApplicationModel);
                }

                return Json(new { status = MessageResource.ValidData, isValid = true, vendorID = vendorId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //  Update save in DB 
        public ActionResult SaveModelDB(CollectorRegistrationModel model)
        {
            try
            {
                var user = TMSContext.TMSPrincipal;
                var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Collector")).ToList();
                var IsCollectorAutoSaveDisabled = ApplicationHelper.IsCollectorAutoSaveDisabled(rolePermissionsClaims);

                if (!IsCollectorAutoSaveDisabled)
                {
                    #region TireDetailsItemType

                    PopulateTireDetails(model);

                    #endregion

                    if (model.VendorID != 0)
                    {

                        model.ContactInformationList.ToList().ForEach(c =>
                        {
                            c.isEmptyContact = false;
                            if (c.ContactInformation.ID == 0)
                            {
                                c.ContactAddressSameAsBusinessAddress = c.ContactInformation.ContactAddressSameAsBusiness;

                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (c.ContactAddressSameAsBusinessAddress)
                                    {
                                        model.ContactInformationList.Remove(c);
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            model.ContactInformationList.Remove(c);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (!c.ContactAddressSameAsBusinessAddress)
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            c.isEmptyContact = true;
                                        }
                                    }
                                }
                            }
                        });
                    }

                    model.UserIPAddress = ("::1" == MicrosoftWeb.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftWeb.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow; //Use UTC time to avoid DateTime Serialze issue in JavaScriptSerializer().Serialize()
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI

                    var vendorId = this.collectorRegistrationService.UpdateApproveApplication(model, model.VendorID);

                    return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult ResendApproveRegistrant(int vendorId, int? applicationId)
        {
            if (vendorId == 0)
            {
                throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
            }
            ApplicationEmailModel approvedApplicationModel = this.registrantService.GetApprovedRegistrantInformation(vendorId);

            if (approvedApplicationModel != null)
            {
                var invitation = userService.FindInvitation(vendorId, approvedApplicationModel.Email, true);
                var expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
                if (invitation != null)
                {
                    invitation.FirstName = approvedApplicationModel.PrimaryContactFirstName;
                    invitation.LastName = approvedApplicationModel.PrimaryContactLastName;
                    invitation.ExpireDate = expireDate;
                    userService.UpdateInvitation(invitation);
                    approvedApplicationModel.InvitationToken = invitation.InviteGUID;
                }
                else
                {
                    var userIp = Request.UserHostAddress;
                    var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

                    var vendor = registrantService.GetSingleVendorByID(vendorId);
                    string reditectURL = string.Empty;
                    if (vendor.RegistrantStatus == "Approved")
                        reditectURL = "/System/FinancialIntegration/BankingInformation/" + vendorId;

                    var newinvitation = ModelAdapter.ToInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, reditectURL, vendorId, 0);
                    userService.CreateParticipantDefaultUserInvitation(newinvitation);
                    approvedApplicationModel.InvitationToken = newinvitation.InviteGUID;
                }

                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationCollector"))
                {
                    bool result = ComposeAndSendEmail(approvedApplicationModel);

                    if (!result)
                        throw new Exception("Email failed to send");
                }

                string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>Re-Sent</strong> the welcome letter to <strong>Participant</strong>.";
                if (invitation.ApplicationId.HasValue)
                {
                    this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, applicationId ?? 0);
                }

                return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
        }

        private Guid CreateUserInvitation(ApplicationEmailModel approvedApplicationModel, int vendorId, int applicationId, bool isGenerator)
        {
            var userIp = Request.UserHostAddress;
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            //OTSTM2-239 check generator drop down
            string ReditectURL = null;
            if (!isGenerator)
            {
                ReditectURL = "/System/FinancialIntegration/BankingInformation/" + vendorId;
            }

            var invitation = ModelAdapter.ToInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, ReditectURL, vendorId, applicationId);
            userService.CreateParticipantDefaultUserInvitation(invitation);
            return invitation.InviteGUID;
        }

        #endregion
    }
}