﻿using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.Web.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.DataContracts.ViewModel.Claims.Export;
using CL.TMS.Common;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.Web.Areas.Collector.Controllers
{
    public class ClaimsController : ClaimBaseController
    {
        #region Properties
        private IClaimService claimService;
        private IFileUploadService fileUploadService;
        private ICollectorClaimService collectorClaimService;
        #endregion

        #region Constructor
        public ClaimsController(ICollectorClaimService collectorClaimService, IClaimService claimService, IFileUploadService fileUploadService)
        {
            this.collectorClaimService = collectorClaimService;
            this.claimService = claimService;
            this.fileUploadService = fileUploadService;
        }
        #endregion

        #region Action Methods
        public ActionResult Index()
        {
            if (SecurityContextHelper.CurrentVendor == null || SecurityContextHelper.CurrentVendor.VendorType != 2)
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;
            ViewBag.SelectedMenu = "Claims";

            if (!SecurityContextHelper.IsStaff())
            {
                return View("ParticipantClaimIndex");
            }
            else
            {
                var model = new ClaimsCommonHeaderViewModel()
                {
                    RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber,
                    OperatingName = SecurityContextHelper.CurrentVendor.OperatingName,
                    IsActive = SecurityContextHelper.CurrentVendor.IsActive,
                    Generator = SecurityContextHelper.CurrentVendor.CIsGenerator,
                    PrimaryBusinessActivity = SecurityContextHelper.CurrentVendor.PrimaryBusinessActivity,
                    Address = SecurityContextHelper.CurrentVendor.Address,
                    PrimaryContact = SecurityContextHelper.CurrentVendor.PrimaryContact,
                    CGeneratorDate = SecurityContextHelper.CurrentVendor.CGeneratorDate,
                    ActiveStatusChangeDate = SecurityContextHelper.CurrentVendor.ActiveStatusChangeDate

                };
                return View("StaffClaimIndex", model);
            }
        }
        [ClaimsAuthorization(TreadMarksConstants.ClaimsCollectorClaimSummary)]
        public ActionResult ClaimSummary(int claimId, int? vId = null)
        {
            if (null == SecurityContextHelper.CurrentVendor && vId == null)//session time out
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            else if (vId != null)
            {
                var vendor = claimService.GetVendorById(vId.Value);
                if (vendor != null)
                {
                    //Keep it in session
                    //Vendor Context Check
                    if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 2)
                        Session["SelectedVendor"] = vendor;
                    else
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
                else
                    return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.ngApp = "collectorClaimApp";
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.ClaimsId = claimId;
            //OTSTM2-551 only claim summary page for staff has activity panel (can get real time activity)
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.HasActivityPanel = 1;
            }
            ViewBag.ClaimsStatus = claimService.GetClaimStatus(claimId);
            ViewBag.UserId = SecurityContextHelper.CurrentUser.Id;
            ViewBag.UserName = string.Format("{0} {1}", SecurityContextHelper.CurrentUser.FirstName, SecurityContextHelper.CurrentUser.LastName);
            ViewBag.VendorId = SecurityContextHelper.CurrentVendor.VendorId;

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = false;

            var vendorReference = claimService.GetVendorReference(claimId);
            if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 2)
            {
                if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                {
                    return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }

            if (SecurityContextHelper.IsStaff())
            {
                var model = new ClaimsCommonHeaderViewModel()
                {
                    RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber,
                    OperatingName = SecurityContextHelper.CurrentVendor.OperatingName,
                    IsActive = SecurityContextHelper.CurrentVendor.IsActive,
                    Generator = SecurityContextHelper.CurrentVendor.CIsGenerator,
                    PrimaryBusinessActivity = SecurityContextHelper.CurrentVendor.PrimaryBusinessActivity,
                    Address = SecurityContextHelper.CurrentVendor.Address,
                    PrimaryContact = SecurityContextHelper.CurrentVendor.PrimaryContact,
                    CGeneratorDate = SecurityContextHelper.CurrentVendor.CGeneratorDate,
                    ActiveStatusChangeDate = SecurityContextHelper.CurrentVendor.ActiveStatusChangeDate
                };
                return View("StaffClaimSummary", model);
            }
            else
            {
                return View("ParticipantClaimSummary");
            }

        }

        #region Collector Claim Summary

        [HttpGet]
        public ActionResult ClaimDetails(int claimId)
        {
            var collectorClaimSummaryViewModel = this.collectorClaimService.LoadCollectorClaimSummary(claimId);
            if (SecurityContextHelper.IsStaff())
            {
                PopulateTransationUrlForStatusPanel(collectorClaimSummaryViewModel.ClaimStatus, "Collector", claimId);
            }
            collectorClaimSummaryViewModel.ClaimCommonModel.ClaimPeriodName = collectorClaimSummaryViewModel.ClaimCommonModel.ClaimPeriod.ShortName;
            collectorClaimSummaryViewModel.ClaimCommonModel.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            collectorClaimSummaryViewModel.ClaimCommonModel.VendorId = SecurityContextHelper.CurrentVendor.VendorId;

            if (collectorClaimSummaryViewModel.ClaimStatus != null)
            {
                collectorClaimSummaryViewModel.ClaimStatus.Submitted = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.Submitted);
                collectorClaimSummaryViewModel.ClaimStatus.Assigned = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.Assigned);
                collectorClaimSummaryViewModel.ClaimStatus.Started = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.Started);
                collectorClaimSummaryViewModel.ClaimStatus.ReviewDue = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.ReviewDue);
                collectorClaimSummaryViewModel.ClaimStatus.Approved = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.Approved);
                collectorClaimSummaryViewModel.ClaimStatus.Reviewed = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.Reviewed);
                collectorClaimSummaryViewModel.ClaimStatus.ClaimOnholdDate = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.ClaimOnholdDate);
                collectorClaimSummaryViewModel.ClaimStatus.ClaimOffholdDate = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.ClaimOffholdDate);
                collectorClaimSummaryViewModel.ClaimStatus.AuditOnholdDate = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.AuditOnholdDate);
                collectorClaimSummaryViewModel.ClaimStatus.AuditOffholdDate = ConvertTimeFromUtc(collectorClaimSummaryViewModel.ClaimStatus.AuditOffholdDate);
            }


            var TireOriginItems = AppDefinitions.Instance.TypeDefinitions["TireOrigin"].ToList();

            var TireOriginValidations = AppDefinitions.Instance.TypeDefinitions["TireOriginFieldLevelValidation"].ToList();

            collectorClaimSummaryViewModel.ClaimSupportingDocVMList.ForEach(c =>
            {
                var item = TireOriginItems.Where(i => i.DefinitionValue == c.DefinitionValue).FirstOrDefault();
                c.TypeDescription = "Reason # " + item.DisplayOrder.ToString() + " " + item.Description;
            });
            collectorClaimSummaryViewModel.ClaimSupportingDocVMList = collectorClaimSummaryViewModel.ClaimSupportingDocVMList.OrderBy(c => c.TypeDescription).ToList();
            var result = new
            {
                collectorClaimSummaryViewModel = collectorClaimSummaryViewModel,
                TireOriginItems = TireOriginItems,
                TireOriginValidations = new { Message = MessageResource.TireOriginFieldLevelValidation, Reminder = MessageResource.TireOriginFieldLevelReminder.Replace("[CompanyName]", @CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.Name").Value), TireOriginValidations } //OTSTM2-543 added Reminder
            };

            string jsonString = JsonConvert.SerializeObject(result);
            return Content(jsonString, "application/json");
        }

        #region Participant Inbound Panel
        [HttpPost]
        public ActionResult LoadInboundTireOrigin(int claimId)
        {
            if (!ModelState.IsValid)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.InvalidData } };
            }

            var inboundTires = this.collectorClaimService.LoadInbound(claimId);

            return new NewtonSoftJsonResult(inboundTires, false);
        }

        [HttpPost]
        public ActionResult LoadSupportingDocs(int claimId)
        {
            if (!ModelState.IsValid)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.InvalidData } };
            }

            List<ClaimSupportingDocVM> supportdingDocs = this.collectorClaimService.LoadSupportingDocs(claimId);

            var TireOriginItems = AppDefinitions.Instance.TypeDefinitions["TireOrigin"].ToList();

            supportdingDocs.ForEach(c =>
            {
                var item = TireOriginItems.Where(i => i.DefinitionValue == c.DefinitionValue).FirstOrDefault();
                c.TypeDescription = "Reason # " + item.DisplayOrder.ToString() + " " + item.Description;
            });
            return new NewtonSoftJsonResult(supportdingDocs.OrderBy(c => c.TypeDescription), false);
        }
        [HttpPost]
        public ActionResult AddTireOrigin(int claimId, ItemRow row, string tireOrigin, int tireOriginValue)
        {
            if (!ModelState.IsValid)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.InvalidData } };
            }

            var tireOriginItem = returnTireOriginModel(row, tireOrigin, tireOriginValue);

            this.collectorClaimService.AddTireOrigin(claimId, tireOriginItem);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        [HttpPost]
        public ActionResult RemoveTireOrigin(int claimId, ItemRow row, string tireOrigin, int tireOriginValue)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var tireOriginItem = returnTireOriginModel(row, tireOrigin, tireOriginValue);

            this.collectorClaimService.RemoveTireOrigin(claimId, tireOriginItem);

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTireOrigin(int claimId, ItemRow row, string tireOrigin, int tireOriginValue)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            var tireOriginItem = returnTireOriginModel(row, tireOrigin, tireOriginValue);

            this.collectorClaimService.UpdateTireOrigin(claimId, tireOriginItem);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }
        [HttpPost]
        public ActionResult UpdateSupportingDocOption(int claimId, int defValue, string option)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            this.collectorClaimService.UpdateSupportingDocOption(claimId, defValue, option);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        public TireOriginItemRow returnTireOriginModel(ItemRow row, string tireOrigin, int tireOriginValue)
        {
            return new TireOriginItemRow
            {
                PLT = row.PLT,
                AGLS = row.AGLS,
                GOTR = row.GOTR,
                IND = row.IND,
                LOTR = row.LOTR,
                MOTR = row.MOTR,
                MT = row.MT,
                SOTR = row.SOTR,
                TireOrigin = tireOrigin,
                TireOriginValue = tireOriginValue
            };
        }

        #endregion

        #region OTSTM2-131 Reuse
        [HttpPost]
        public ActionResult AddUpdatedReuseTires(int claimId, ItemRow row)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            this.collectorClaimService.AddReuseTires(claimId, new ReuseTiresItemRow
            {
                PLT = row.PLT,
                AGLS = row.AGLS,
                GOTR = row.GOTR,
                IND = row.IND,
                LOTR = row.LOTR,
                MOTR = row.MOTR,
                MT = row.MT,
                SOTR = row.SOTR
            });

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        [HttpPost]
        public ActionResult EditUpdatedReuseTires(int claimId, ItemRow row)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            this.collectorClaimService.UpdateReuseTires(claimId, new ReuseTiresItemRow
            {
                PLT = row.PLT,
                AGLS = row.AGLS,
                GOTR = row.GOTR,
                IND = row.IND,
                LOTR = row.LOTR,
                MOTR = row.MOTR,
                MT = row.MT,
                SOTR = row.SOTR
            });

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        [HttpPost]
        public ActionResult RemoveUpdatedReuseTire(int claimId, ItemRow row)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            this.collectorClaimService.RemoveReuseTires(claimId, new ReuseTiresItemRow
            {
                PLT = row.PLT,
                AGLS = row.AGLS,
                GOTR = row.GOTR,
                IND = row.IND,
                LOTR = row.LOTR,
                MOTR = row.MOTR,
                MT = row.MT,
                SOTR = row.SOTR
            });

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LoadReuseTire(int claimId)
        {
            if (!ModelState.IsValid)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.InvalidData } };
            }

            var reuseTires = this.collectorClaimService.LoadReuseTires(claimId).ReuseTire;

            return new NewtonSoftJsonResult(reuseTires, false);
        }

        #endregion

        #region Submit Button
        [HttpPost]
        public JsonResult SubmitClaimCheck(CollectorSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }

                var updatedSubmitClaimModel = collectorClaimService.CollectorClaimSubmitBusinessRule(submitClaimModel);

                if (updatedSubmitClaimModel.Errors.Count > 0)
                {
                    //handle errors
                    var result = new { status = "Errors", Type = SubmitBusinessRuleType.Error.ToString(), Message = updatedSubmitClaimModel.Errors.First() };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                if (updatedSubmitClaimModel.Warnings.Count > 0)
                {
                    //handle warnings
                    return Json(new { status = "Warnings", Type = SubmitBusinessRuleType.Warning.ToString(), Warnings = updatedSubmitClaimModel.Warnings }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = "Valid Data", Type = SubmitBusinessRuleType.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult FinalSubmitClaim(int claimId, CollectorSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }

                //Before final submit claim, call business role check again for checking if it is auto approval
                var updatedSubmitClaimModel = collectorClaimService.CollectorClaimSubmitBusinessRule(submitClaimModel);
                if (updatedSubmitClaimModel.Errors.Count == 0)
                {
                    claimService.SubmitClaim(claimId, true);
                }
                else
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = "Valid Data" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Workflow
        [HttpPost]
        public JsonResult IsStaffTotalInboundOutbound(int claimId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }

            var isTotalInboundOutbound = this.collectorClaimService.IsStaffTotalInboundOutbound(claimId);

            return Json(new { status = "Valid Data", isTotalInboundOutbound = isTotalInboundOutbound }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Collector Export Participant
        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelInbound(List<TireOriginItemRow> InboundList)
        {
            var columnNames = new List<string>(){
                "TireOrigin", "PLT", "MT","AG/LS", "IND", "SOTR","MOTR", "LOTR", "GOTR"
            };

            var delegates = new List<Func<TireOriginItemRow, string>>()
            {
                u => u.TireOrigin,
                u => u.PLT.ToString(),
                u => u.MT.ToString(),
                u => u.AGLS.ToString(),
                u => u.IND.ToString(),
                u => u.SOTR.ToString(),
                u => u.MOTR.ToString(),
                u => u.LOTR.ToString(),
                u=> u.GOTR.ToString(),
            };

            string csv = InboundList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("File-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelReuseTires(IList<ReuseTiresItemRow> ReuseTiresList)
        {
            ReuseTiresList[0].ReuseTires = string.Empty;

            var columnNames = new List<string>(){
                "ReuseTires", "PLT", "MT","AG/LS", "IND", "SOTR","MOTR", "LOTR", "GOTR"
            };

            var delegates = new List<Func<ReuseTiresItemRow, string>>()
            {
                u => u.ReuseTires,
                u => u.PLT.ToString(),
                u => u.MT.ToString(),
                u => u.AGLS.ToString(),
                u => u.IND.ToString(),
                u => u.SOTR.ToString(),
                u => u.MOTR.ToString(),
                u => u.LOTR.ToString(),
                u=> u.GOTR.ToString(),
            };

            string csv = ReuseTiresList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("File-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelPaymentAdjustment(IList<CommonPaymentAdjustExportViewModel> PaymentAdjList)
        {
            var columnNames = new List<string>(){
                "", "Rate ($)", "QTY","Payment ($)"
            };

            var delegates = new List<Func<CommonPaymentAdjustExportViewModel, string>>()
            {
                u => u.RowName,
                u => u.Rate,
                u => u.QTY,
                u => u.Payment
            };

            string csv = PaymentAdjList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("File-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Collector Export Staff

        #endregion

        #endregion
    }
}