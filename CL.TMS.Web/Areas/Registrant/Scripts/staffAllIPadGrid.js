﻿var iPadGrid;
var iPadGridCounter = 0;
$(function () {
    _PageSize = 7;
    iPadGrid = $('#iPadGrid').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: "GetIPadFleetList",
        deferRender: true,
        dom: "rtiS",
        scrollY: 260,
        scrollX: true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        scrollCollapse: false,
        searching: true,
        ordering: true,
        order: [[0, "desc"]],
        autoWidth: false,
        columns: [
             {
                 width: "40%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return IPadIDColumnRender(data.Number, data.Activated);
                 }
             },
             {
                 width: "40%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return AssignToColumnRender(data.AssignedToNumber, data.Activated);
                 }
             },
             {
                 className: "td-center",
                 width: "20%",
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return IPadGridActionColumn(data.ID, data.Number, data.AssignedToName, data.AssignedToNumber, "", "H", data.IsAssigned, data.Activated);
                 }

             },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            (settings.fnRecordsTotal() >= _PageSize) ? $('#iPadViewMore').css('visibility', 'visible') : $('#iPadViewMore').css('visibility', 'hidden');
        },
        scroller: {
            displayBuffer: 100,
            rowHeight: 40,
            serverWait: 100,
        },

    });
});

function IPadIDColumnRender(iPadNumber, activated) {

    var span = ""
    if (activated) {
        span = "<span>" + iPadNumber + "</span>";
    }
    else {
        span = "<span style=\"color:#b1b1b1\">" + iPadNumber + "</span>";
    }
    return span;
}

function AssignToColumnRender(assignedToNumber, activated) {

    var span = "<span>"
    if (!activated) {
        span = "<span style=\"color:#b1b1b1\">";
    }

    if (assignedToNumber == "") {
        span = span + "Unassigned</span>";
    }
    else {
        span = span + assignedToNumber + "</span>";
    }

    return span;
}


function IPadGridActionColumn(ID, iPadNumber, assignedToName, assignedToNumber, userID, barcodeGroup, isAssigned, isActivated) {
    //console.log('iPadGridCounter:' + ++iPadGridCounter);
    var content = "";
    //fix for safari
    var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex=\"" + ID + "\"" : "";
    var disableClick = (Security.Ipad_QrCode.Resource.Ipad === Security.Ipad_QrCode.Constants.ReadOnly) ? "disable-click" : "";//OTSTM2-155
    if (isActivated) {
        content = "<div class=\"btn-group dropdown-menu-actions\">" +
                    "<a id=\"IPadActions" + ID + "\" data-toggle=\"popover\" tabindex=\"" + ID + "\" data-trigger=\"focus\"  href=\"#\">" +
                    "<i class=\"glyphicon glyphicon-cog\"></i>" +
                    "</a><div class=\"popover-menu\"><ul>";

        if (isAssigned) {
            content += "<li><a id=\"PrintDeviceLabel" + ID + "\" href=\"/default/DeviceLabelGen?deviceID=" + iPadNumber + "&businessName=" + assignedToName + "&regNumber=" + assignedToNumber + "&userID=" + userID + "&barcodeGroup=" + barcodeGroup + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Print Label</a></li>";
            content += "<li class=\"" + disableClick + "\"><a id=\"UnAssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalUnassigniPad\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\"  data-assignedto=\"" + assignedToNumber + "\"><i class=\"fa fa-minus-square-o\"></i> Unassign</a></li>";
        }
        else {
            content += "<li class=\"" + disableClick + "\"><a id=\"AssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalAssigniPad\"><i class=\"fa fa-plus-square-o\"></i> Assign</a></li>";
        }
        content += "<li class=\"" + disableClick + "\"><a id=\"DactivateAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalDeactivateiPad\"><i class=\"fa fa-remove\"></i> Deactivate</a></li>";
        content += "</ul></div></div>";
    }

    return content;
}

$("#iPadGridSearchInput").on("keyup", function () {
    var s = $(this).val();
    iPadGrid.search(s).draw(false);
});


$(window).load(function () {
    //$("#iPadGrid").parents(".dataTables_scrollBody").css("overflow", "hidden");
    $("#iPadGrid").parents(".dataTables_scrollBody").css("overflow-x", "auto");
});

$("#iPadViewMore").on("click", function () {
    var v1 = $("#iPadGrid_wrapper");
    var v2 = $("#iPadGrid_wrapper").find(".dataTables_scrollBody");
    $("#iPadGrid_wrapper").find(".dataTables_scrollBody").css("overflow", "auto");
    $(this).hide();
});

$("#modalAddiPads").on("click", "#modalAddiPadOption1", function () {
    $("#modalAddiPadRegistrantList").prop("disabled", true);
});

$("#modalAddiPads").on("click", "#modalAddiPadOption2", function () {
    $("#modalAddiPadRegistrantList").prop("disabled", false);
});

$("#modalAddiPads").on("show.bs.modal", function (event) {
    var amountOfIPadsToAdd = parseInt($("#amountOfIPadsToAddText").val());

    $("#modalAddiPadsAmountValue").val(amountOfIPadsToAdd);

    var verbage;
    if (amountOfIPadsToAdd == 1) {
        verbage = "this " + amountOfIPadsToAdd + " iPad";
    }
    else {
        verbage = "these " + amountOfIPadsToAdd + " iPads";
    }

    $("#modalAddiPadsAmountText").html(verbage);
    $("#modalAddiPadOption1").prop("checked", true);
    $("#modalAddiPadRegistrantList").attr("disabled", true);
    $("#modalAddiPadRegistrantList").hide();
    $("#modalAddiPadRegistrantList").empty();
});

$("#modalAddiPads").on("click", "#modalAddiPadOption1", function () {
    $("#modalAddiPadNextBtn").attr("disabled", false);
    $("#modalAddiPadRegistrantList").prop("disabled", true);
    $("#modalAddiPadRegistrantList").hide();
});

$("#modalAddiPads").on("click", "#modalAddiPadOption2", function () {
    $("#modalAddiPadNextBtn").attr("disabled", true);
    $("#modalAddiPadRegistrantList").show();
    $("#modalAddiPadRegistrantList").empty();

    $.ajax({
        url: "GetListOfRegistrants",
        success: function (result) {
            $("#modalAddiPadRegistrantList").append("<option selected='selected' value='0'></option>");
            $.each(result, function (key, value) {
                $("#modalAddiPadRegistrantList").append("<option value='" + value.Value + "'>" + value.Text + "</option>");
            });
        }
    });
});

$("#modalAddiPads").on("change", "#modalAddiPadRegistrantList", function () {
    var selectedRegistrantValue = parseInt($(this).find(":selected").val());
    if (selectedRegistrantValue > 0) {
        $("#modalAddiPadNextBtn").attr("disabled", false);
    }
    else {
        $("#modalAddiPadNextBtn").attr("disabled", true);
    }
});

$("#modalAddiPads").on("click", "#modalAddiPadNextBtn", function () {

    var amountOfIPadsToAdd = parseInt($("#modalAddiPadsAmountValue").val());

    var verbage;

    if (amountOfIPadsToAdd == 1) {
        verbage = amountOfIPadsToAdd + " iPad";
    }
    else {
        verbage = amountOfIPadsToAdd + " iPads";
    }

    if ($("#modalAddiPadOption1").is(":checked")) {
        $("#modalAddiPadsConfirm").modal("show");
        $("#modalAddiPadsConfirmAmount").html(verbage);
        $("#modalAddiPadNextBtn").attr("disabled", false);
    }
    else {
        var selectedRegistrantValue = $("#modalAddiPadRegistrantList").find(":selected").val();
        $("#selectRegistrantValidationMsg").hide();
        $("#modalAddiPadsToRegConfirmRegValue").val(selectedRegistrantValue);

        var selectedRegistrantText = $("#modalAddiPadRegistrantList").find(":selected").text();
        $("#modalAddiPadsToRegConfirmRegText").html(selectedRegistrantText);

        $("#modalAddiPadsToRegConfirm").modal("show");
        //console.log(verbage);
        $("#modalAddiPadsToRegConfirmAmount").html(verbage);
    }
});

$("#modalAddiPadsConfirm").on("click", "#modalAddiPadsConfirmYesBtn", function () {
    $.ajax({
        url: "AddIPad",
        data: {
            registrantId: null,
            amount: parseInt($("#modalAddiPadsAmountValue").val()),
        },
        success: function () {
            $("#amountOfIPadsToAddText").val("");
            iPadGrid.draw(true);
        }
    });
});

$("#modalAddiPadsToRegConfirm").on("click", "#modalAddiPadsToRegConfirmYesBtn", function () {
    $.ajax({
        url: "AddIPad",
        data: {
            registrantId: $("#modalAddiPadsToRegConfirmRegValue").val(),
            amount: parseInt($("#amountOfIPadsToAddText").val()),
        },
        success: function () {
            $("#amountOfIPadsToAddText").val("");
            iPadGrid.draw(true);
        }
    });
});

//Assign iPad start//
$("#modalAssigniPad").on("show.bs.modal", function (event) {

    $("#modalAssigniPadNextBtn").attr("disabled", true);
    $("#modalAssigniPadRegistrantList").empty();

    $.ajax({
        url: " GetListOfRegistrants",
        success: function (result) {
            $("#modalAssigniPadRegistrantList").append("<option selected='selected' value='0'></option>");
            $.each(result, function (key, value) {
                $("#modalAssigniPadRegistrantList").append("<option value='" + value.Value + "'>" + value.Text + "</option>");
            });
        }
    });

    var trigger = $(event.relatedTarget);
    var iPadID = trigger.data("deviceid");
    var iPadUID = trigger.data("uid");
    $("#modalAssignIPadConfirmIPadID").html(iPadID);
    $("#modalAssignIPadConfirmIPadIDValue").val(iPadUID);

});

$("#modalAssigniPad").on("change", "#modalAssigniPadRegistrantList", function () {
    var selectedRegistrantValue = parseInt($(this).find(":selected").val());
    if (selectedRegistrantValue > 0) {
        $("#modalAssigniPadNextBtn").attr("disabled", false);
    }
    else {
        $("#modalAssigniPadNextBtn").attr("disabled", true);
    }
});

$("#modalAssignIPadConfirm").on("show.bs.modal", function (event) {

    var selectedRegistrant = $("#modalAssigniPadRegistrantList").find(":selected");
    $("#modalAssignIPadConfirmRegValue").val(selectedRegistrant.val());
    $("#modalAssignIPadConfirmRegText").html(selectedRegistrant.text());
});

$("#modalAssignIPadConfirm").on("click", "#modalAssignIPadConfirmYesBtn", function () {
    $.ajax({
        url: "AssignIPad",
        data: {
            iPadId: $("#modalAssignIPadConfirmIPadIDValue").val(),
            registrantId: $("#modalAssignIPadConfirmRegValue").val(),
        },
        success: function () {
            iPadGrid.draw(true);
        }
    });
});
//Assign iPad end//

//UnAssign iPad start//
$("#modalUnassigniPad").on("show.bs.modal", function (event) {

    var trigger = $(event.relatedTarget);
    var iPadID = trigger.data("deviceid");
    var iPadUID = trigger.data("uid");
    var assignedTo = trigger.data("assignedto");

    $("#modalUnassigniPadIDValue").val(iPadUID);
    $("#modalUnassigniPadIDText").html(iPadID);
    $("#modalUnassigniPadRegValue").val(assignedTo);
    $("#modalUnassigniPadRegText").html(assignedTo);
});

$("#modalUnassigniPad").on("click", "#modalUnassigniPadConfirmYesBtn", function () {
    $.ajax({
        url: "UnAssignIPad",
        data: {
            iPadId: $("#modalUnassigniPadIDValue").val(),
            registrantId: $("#modalUnassigniPadRegValue").val(),
        },
        success: function () {
            iPadGrid.draw(true);
        }
    });
});
//UnAssign iPad end//

//Deactivate iPad start//
$("#modalDeactivateiPad").on("show.bs.modal", function (event) {

    var trigger = $(event.relatedTarget);
    var iPadID = trigger.data("deviceid");
    var iPadUID = trigger.data("uid");
    $("#modalDeactivateiPadValue").val(iPadUID);
    $("#modalDeactivateiPadText").html(iPadID);
});

$("#modalDeactivateiPad").on("click", "#modalDeactivateiPadConfirmYesBtn", function () {
    $.ajax({
        url: "DeActivateIPad",
        data: {
            iPadId: $("#modalDeactivateiPadValue").val(),
        },
        success: function () {
            iPadGrid.draw(true);
            $("#amountOfIPadsToAddText").val("")
        }
    });
});

$("#amountOfIPadToAddBtn").click(function (e) {
    var amountToAdd = parseInt($("#amountOfIPadsToAddText").val().trim());
    if (isNaN(amountToAdd) || amountToAdd <= 0) {
        e.stopPropagation();
        $("#addIpadValidationMsg").show();
    }
    else {
        $("#addIpadValidationMsg").hide();
    }
});
//Deactivate iPad end//

$("#exportIPadDeviceLabelCollection").on("click", function (e) {
    e.stopPropagation();
    var searchValue = $("#iPadGridSearchInput").val().trim();
    window.open("/Default/DeviceLabelCollection?activated=true&assigned=true&searchValue=" + searchValue);
});

$("#amountOfIPadsToAddText").on("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#amountOfIPadToAddBtn").trigger("click");
    }
});

$("#iPadGrid").on("draw.dt", function () {
    var recordsTotal = iPadGrid.page.info().recordsTotal;
    if (recordsTotal == 0) {
        $(this).find('.dataTables_empty').html('No matching records found');
    }
    var s = $("#iPadGridSearchInput").val();
    if (s != "") {
        $("#iPadGridSearchInputNumFound").html("Found " + iPadGrid.page.info().recordsTotal);
    }
    else {
        $("#iPadGridSearchInputNumFound").html("");
    }

    $("[id^='IPadActions']").popover({
        placement: "bottom",
        container: "body",
        html: true,
        template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
        content: function () {
            return $($(this)).siblings(".popover-menu").html();
        },
    });

});

$("#iPadGridSearchInputRemoveText").on("click", function () {
    iPadGrid.search("").draw(false);
});

$(".btn-cancel-add-ipad").on("click", function () {
    $("#amountOfIPadsToAddText").val("");
});
