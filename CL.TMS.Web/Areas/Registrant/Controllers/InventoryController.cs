﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.UI.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.ViewModel.Common;
using System.Threading;
using CL.TMS.Security.Authorization;
using CL.TMS.Common;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.Web.Areas.Registrant.Controllers
{
    public class InventoryController : BaseController
    {
        #region Fields
        private IRegistrantService registrantService;
        private const string ExcelExportSession = "ExcelExportSession";
        #endregion

        #region Constructors
        public InventoryController(IRegistrantService registrantService)
        {
            this.registrantService = registrantService;
        }
        #endregion

        #region Public Methods

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodes)]
        public ActionResult IPadQRCodeAll()
        {
            //OTSTM2-155
            ViewBag.SelectedMenu = "IPadQRCode";
            return View();
        }

        public JsonResult GetIPadFleetList(DataGridModel model)
        {

            string searchValue = model.Search["value"].Trim();
            int orderColumnIndex;
            int.TryParse(model.Order[0]["column"], out orderColumnIndex);
            string orderColumnDir = model.Order[0]["dir"];

            var listOfIPads = registrantService.IPadList(model.Start, model.Length, searchValue, orderColumnIndex, orderColumnDir);

            var count = 0;

            if (!string.IsNullOrEmpty(searchValue))
            {
                count = registrantService.IPadList(searchValue).Count();
            }
            else
            {
                count = registrantService.IPadList().Count();

            }
            var recordsFiltered = count;

            var iPadFleetDataTable = new IPadFleetDataTable()
            {
                data = listOfIPads,
                recordsTotal = count,
                recordsFiltered = count,
                draw = model.Draw,
            };

            return Json(iPadFleetDataTable, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListOfRegistrants()
        {
            var vendorNameList = registrantService.GetVendorNameList();

            var registrantList = new List<TextValuePair>();

            foreach (var vendorName in vendorNameList)
            {
                registrantList.Add(new TextValuePair() { Text = vendorName.Value, Value = vendorName.Key.ToString() });
            }

            return Json(registrantList, JsonRequestBehavior.AllowGet);
        }

        //-----------Added by Frank, begin: new function-------------

        public JsonResult GetListOfRegistrantsWithPageIndex(int pageIndex)
        {
            var pageSize = 20;
            var vendorNameList = registrantService.GetListOfRegistrantsWithPageIndex(pageIndex, pageSize);

            var registrantList = new List<TextValuePair>();

            foreach (var vendorName in vendorNameList)
            {
                registrantList.Add(new TextValuePair() { Text = vendorName.Value, Value = vendorName.Key.ToString() });
            }

            return Json(registrantList, JsonRequestBehavior.AllowGet);
        }

        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement
        public string GetMatchedRegistrant(string regNumberEntered)
        {
            return registrantService.GetMatchedRegistrant(regNumberEntered);
        }

        public void AddIPad(int amount, int? registrantId)
        {
            string currentUser = HttpContext.User.Identity.Name;

            registrantService.AddIPad(amount, registrantId, 1, currentUser);
        }

        public void AssignIPad(int iPadId, int registrantId)
        {
            string currentUser = HttpContext.User.Identity.Name;

            registrantService.AssignIPad(iPadId, registrantId, currentUser);
        }

        public void UnAssignIPad(int iPadId, string unAssignBy)
        {
            string userName = HttpContext.User.Identity.Name;
            registrantService.UnAssignIPad(iPadId, userName);
        }

        public void DeActivateIPad(int iPadId)
        {
            string userName = HttpContext.User.Identity.Name;
            registrantService.DeActiveIPad(iPadId, userName);
        }

        public JsonResult GetQRCodeList(DataGridModel model)
        {
            string searchValue = model.Search["value"].Trim();
            int orderColumnIndex;
            int.TryParse(model.Order[0]["column"], out orderColumnIndex);
            string orderColumnDir = model.Order[0]["dir"];

            var listOfQRCodes = registrantService.GetAppUserList(model.Start, model.Length, model.Search["value"].Trim(), orderColumnIndex, orderColumnDir);

            var count = listOfQRCodes.Count();
            //if (!string.IsNullOrEmpty(searchValue))
            //{
            //    count = registrantService.GetAppUserList(searchValue).Count();
            //}
            //else
            //{
            //    count = registrantService.GetAppUserList().Count();
            //}

            //registrantService.GetAppUserList().Count();
            var recordsFiltered = count;

            var qrCodelist = new List<QRCodeRow>();
            int qrCodeListCount = listOfQRCodes.Count;

            for (int i = 0; i < qrCodeListCount; i++)
            {
                int nextItemOnList = 0;
                var qrCodeRow = new QRCodeRow();
                if (i == 0 || i % 2 == 0)
                {

                    qrCodeRow.ID_l = listOfQRCodes[i].ID;
                    qrCodeRow.UID_l = listOfQRCodes[i].VendorAppUserID;
                    qrCodeRow.QRCodeNumber_l = listOfQRCodes[i].Number;
                    qrCodeRow.BusinessNumber_l = listOfQRCodes[i].Number;
                    //qrCodeRow.BusinessNumber_l = listOfQRCodes[i].VendorNumber;
                    //qrCodeRow.BusinessName_l = listOfQRCodes[i].VendorName;
                    //var primaryAddress_l = listOfQRCodes[i].Addresses.FirstOrDefault(r => r.AddressType == 1);
                    // test code
                    //if (primaryAddress_l != null)
                    //qrCodeRow.BusinessPrimaryAddress_l = string.Format("{0}, {1}, {2}, {3}, {4}", primaryAddress_l.Address1, primaryAddress_l.City, primaryAddress_l.Province, primaryAddress_l.PostalCode, primaryAddress_l.Country);
                    qrCodeRow.IsActivated_l = listOfQRCodes[i].Active;

                    nextItemOnList = (i + 1);
                    if (nextItemOnList < qrCodeListCount)
                    {
                        qrCodeRow.ID_r = listOfQRCodes[nextItemOnList].ID;
                        qrCodeRow.UID_r = listOfQRCodes[nextItemOnList].VendorAppUserID;
                        qrCodeRow.QRCodeNumber_r = listOfQRCodes[nextItemOnList].Number;
                        qrCodeRow.BusinessNumber_r = listOfQRCodes[nextItemOnList].Number;
                        //qrCodeRow.BusinessName_r = listOfQRCodes[nextItemOnList].VendorName;
                        //var primaryAddress_r = listOfQRCodes[nextItemOnList].Addresses.FirstOrDefault(r => r.AddressType == 1);
                        //if (primaryAddress_r != null)
                        //qrCodeRow.BusinessPrimaryAddress_r = string.Format("{0}, {1}, {2}, {3}, {4}", primaryAddress_r.Address1, primaryAddress_r.City, primaryAddress_r.Province, primaryAddress_r.PostalCode, primaryAddress_r.Country);
                        qrCodeRow.IsActivated_r = listOfQRCodes[nextItemOnList].Active;
                        //qrCodeRow.BusinessNumber_r = listOfQRCodes[nextItemOnList].VendorNumber;
                    }
                }


                i = nextItemOnList;
                qrCodelist.Add(qrCodeRow);
            }

            var iPadFleetDataTable = new QRCodeDataTable()
            {
                data = qrCodelist,
                recordsTotal = count,
                recordsFiltered = count,
                draw = model.Draw,
            };

            return Json(iPadFleetDataTable, JsonRequestBehavior.AllowGet);
        }

        public void AddQRCode(int amount, int registrantId)
        {

            string currentUser = HttpContext.User.Identity.Name;

            registrantService.AddAppUser(amount, registrantId, currentUser);
        }

        public void DeActivateQRCode(int qrCodeID)
        {
            registrantService.DeActivateAppUser(qrCodeID);
        }

        public void ExportIPadAndQRCodeGridToExcel(string iPadSearchValue = "", string qrCodeSearchValue = "")//this method was used in [\CL.TMS.Web\Areas\Registrant\Scripts\iPadQRCodeAll.js] which is exclude from solution due to #OTSTM2-905
        {
            using (var excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "QRCode";

                //ComposeIPadExcelWorkbook(excel, iPadSearchValue);
                ComposeQRCodeExcelWorkbook(excel, qrCodeSearchValue);

                //to do
                //Core.ExcelFileHandler.Export(excel);
            }
        }

        private void ComposeIPadExcelWorkbook(ExcelPackage excelPackage, string iPadSearchValue)
        {
            var iPadSheet = excelPackage.Workbook.Worksheets.Add("iPad Fleet");
            iPadSheet.Cells[1, 1].Value = "iPad ID";
            iPadSheet.Cells[1, 2].Value = "Assigned To Number";
            iPadSheet.Cells[1, 3].Value = "Assigned To Business Name";
            iPadSheet.Cells[1, 4].Value = "Activated";

            //IPadModel changed to Asset
            var iPadList = new List<IPadModel>();

            if (!string.IsNullOrEmpty(iPadSearchValue.Trim()))
            {
                iPadList = registrantService.IPadList(iPadSearchValue.Trim()).ToList();
            }
            else
            {
                iPadList = registrantService.IPadList().ToList();

            }

            int iPadRow = 2;
            foreach (var iPad in iPadList)
            {
                iPadSheet.Cells[iPadRow, 1].Value = iPad.Number;
                iPadSheet.Cells[iPadRow, 2].Value = (!string.IsNullOrEmpty(iPad.AssignedToNumber) ? iPad.AssignedToNumber : "Unassigned");
                iPadSheet.Cells[iPadRow, 3].Value = (!string.IsNullOrEmpty(iPad.AssignedToName) ? iPad.AssignedToName : "Unassigned");
                iPadSheet.Cells[iPadRow, 4].Value = iPad.Activated;

                iPadRow++;
            }
        }

        private void ComposeQRCodeExcelWorkbook(ExcelPackage excelPackage, string qrCodeSearchValue)
        {
            var qrCodeSheet = excelPackage.Workbook.Worksheets.Add("QRCode");
            qrCodeSheet.Cells[1, 1].Value = "QRCode";
            qrCodeSheet.Cells[1, 2].Value = "Activated";

            //AppUserModel changed to AppUser
            var appUserList = new List<AppUserModel>();

            appUserList = registrantService.GetAppUserList(qrCodeSearchValue.Trim()).ToList();

            int qrCodeRow = 2;
            var qrCodeSortesList = appUserList.OrderBy(r => r.VendorAppUserID).ToList();
            foreach (var qrCode in qrCodeSortesList)
            {
                qrCodeSheet.Cells[qrCodeRow, 1].Value = qrCode.Number;
                qrCodeSheet.Cells[qrCodeRow, 2].Value = qrCode.Active;

                qrCodeRow++;
            }
        }

        #endregion
    }
}