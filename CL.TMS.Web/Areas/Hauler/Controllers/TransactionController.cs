﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.Security.Authorization;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Configuration;

using Newtonsoft.Json;
using CL.TMS.Common.Enum;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using CL.TMS.Security;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.Web.Areas.Hauler.Controllers
{
    public class TransactionController : BaseController
    {
        #region Properties
        private ITransactionService transactionService;
        private IRegistrantService registrantService;
        private IClaimService claimService;
        #endregion


        #region Constructor
        public TransactionController(ITransactionService transactionService, IRegistrantService registrantService, IClaimService claimService)
        {
            this.transactionService = transactionService;
            this.registrantService = registrantService;
            this.claimService = claimService;
        }
        #endregion


        #region Action Methods
        public ActionResult Index(int id, string type, int direction)
        {
            ViewBag.ngApp = "TransactionApp";
            ViewBag.VendorCode = TreadMarksConstants.Hauler;

            var claimStatus = this.claimService.GetClaimStatus(id);

            ViewBag.ClaimId = id;
            ViewBag.TypeId = type;
            ViewBag.Direction = direction;

            if (null != SecurityContextHelper.CurrentVendor)
            {
                ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;

                var vendorReference = claimService.GetVendorReference(id);
                if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 3)
                {
                    if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                    {
                        return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                    }
                    else
                    {
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                    }
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = (SecurityContextHelper.IsStaff() && claimStatus == ClaimStatus.UnderReview);

            //OTSTM2-155
            bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryOutbound) == SecurityResultType.EditSave);
            switch (type)
            {
                case TreadMarksConstants.TCR:
                case TreadMarksConstants.DOT:
                case TreadMarksConstants.STC:
                case TreadMarksConstants.UCR:
                    bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                    break;

                case TreadMarksConstants.HIT:

                    if (direction == 1) bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                    break;
                default:
                    break;
            };
            if (SecurityContextHelper.IsStaff()) ViewBag.AllowAddTransaction = this.claimService.IsClaimAllowAddNewTransaction(id) && bWritePermission;
            else ViewBag.AllowAddTransaction = (bWritePermission && claimStatus == ClaimStatus.Open);

            ViewBag.ClaimPeriod = this.claimService.GetClaimPeriod(id).ShortName;
            ViewBag.AllowEdit = bWritePermission;

            switch (type)
            {
                case TreadMarksConstants.TCR:
                    ViewBag.TransactionType = TreadMarksConstants.TCR;
                    ViewBag.HeaderName = "TCR Transactions";
                    return View("TCRTransactions");

                case TreadMarksConstants.DOT:
                    ViewBag.TransactionType = TreadMarksConstants.DOT;
                    ViewBag.HeaderName = "DOT Transactions";
                    return View("DOTTransactions");

                case TreadMarksConstants.STC:
                    ViewBag.TransactionType = TreadMarksConstants.STC;
                    ViewBag.HeaderName = "STC Transactions";
                    return View("STCTransactions");

                case TreadMarksConstants.UCR:
                    ViewBag.TransactionType = TreadMarksConstants.UCR;
                    ViewBag.HeaderName = "UCR Transactions";
                    return View("UCRTransactions");

                case TreadMarksConstants.HIT:
                    ViewBag.TransactionType = TreadMarksConstants.HIT;
                    ViewBag.HeaderName = (direction == 1) ? "Inbound HIT Transactions" : "Outbound HIT Transactions";

                    if (direction == 1) return View("HITInBoundTransactions");
                    else
                    {
                        // Only staff can add papaer form in Outbound 
                        ViewBag.AllowAddTransaction = (SecurityContextHelper.IsStaff() && ViewBag.AllowAddTransaction);
                        return View("HITOutBoundTransactions");
                    }

                case TreadMarksConstants.RTR:
                    ViewBag.TransactionType = TreadMarksConstants.RTR;
                    ViewBag.HeaderName = "RTR Transactions";
                    return View("RTRTransactions");

                case TreadMarksConstants.PTR:
                    ViewBag.TransactionType = TreadMarksConstants.PTR;
                    ViewBag.HeaderName = "PTR Transactions";
                    return View("PTRTransactions");

                default:
                    return RedirectToAction("Index", "Claims", new { area = "Hauler" });
            };
        }
        #endregion


        #region Service Methods

        #region Paper Form Service Methods
        [HttpPost]
        public NewtonSoftJsonResult initializePaperForm(int id, string type, int direction)
        {
            var claimPeriod = this.claimService.GetClaimPeriod(id);

            IHaulerTransactionViewModel paperFormModel = new HaulerTransactionViewModel();

            switch (type)
            {
                case TreadMarksConstants.TCR:
                    paperFormModel = new HaulerTCRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.TCR)));
                    break;

                case TreadMarksConstants.DOT:
                    paperFormModel = new HaulerDOTTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.DOT)));
                    break;

                case TreadMarksConstants.STC:
                    paperFormModel = new HaulerSTCTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.STC)));
                    break;

                case TreadMarksConstants.UCR:
                    paperFormModel = new HaulerUCRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.UCR)));
                    break;

                case TreadMarksConstants.HIT:
                    paperFormModel = new HaulerHITTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.HIT)));

                    var vmHIT = paperFormModel as HaulerHITTransactionViewModel;
                    vmHIT.IsInbound = (direction == 1);
                    break;

                case TreadMarksConstants.RTR:
                    paperFormModel = new HaulerRTRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.RTR)));

                    var vmRTR = paperFormModel as HaulerRTRTransactionViewModel;
                    vmRTR.CountryList.AddRange(DataLoader.GetCountriesByIso3166());
                    break;

                case TreadMarksConstants.PTR:
                    paperFormModel = new HaulerPTRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.PTR)));
                    break;

                default:
                    return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            };

            paperFormModel.CurrentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
            paperFormModel.ClaimId = id;

            // Form date related setting    
            TransactionHelper.initializeFormDate(paperFormModel.FormDate, claimPeriod, ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary));
            paperFormModel.FormDate.DisabledDateRangeList = new List<TransactionDisabledDateRangeViewModel>();
            paperFormModel.FormDate.DisabledDateRangeList.AddRange(transactionService.GetVendorInactiveDateRanges(SecurityContextHelper.CurrentVendor.VendorId));


            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = paperFormModel } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionTCR(HaulerTCRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                transactionDataModel.Outbound = registrantService.GetVendorByNumber(transactionDataModel.CollectorNumber.ToString());

                var creationNotification = transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Outbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionDOT(HaulerDOTTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                transactionDataModel.Outbound = registrantService.GetVendorByNumber(transactionDataModel.CollectorNumber.ToString());

                var creationNotification = transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Outbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionSTC(HaulerSTCTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                transactionDataModel.Outbound = null;

                transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionUCR(HaulerUCRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                transactionDataModel.Outbound = null;

                transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionHIT(HaulerHITTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                if (transactionDataModel.IsInbound)
                {
                    transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                    transactionDataModel.Outbound = registrantService.GetVendorByNumber(transactionDataModel.HaulerNumber.ToString());
                }
                else
                {
                    transactionDataModel.Inbound = registrantService.GetVendorByNumber(transactionDataModel.HaulerNumber.ToString());
                    transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;
                }

                var creationNotification = transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.IsInbound && transactionDataModel.Outbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);
                if (!transactionDataModel.IsInbound && transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionRTR(HaulerRTRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = null;
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionPTR(HaulerPTRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = registrantService.GetVendorByNumber(transactionDataModel.ProcessorNumber.ToString());
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                var creationNotification = transactionService.AddHaulerPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }
        #endregion

        #region List Handler Service Methods
        public ActionResult GetTransactionListHandler(DataGridModelExtend param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;

                    #region columns

                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    columns.Add(0, "DeviceName");
                    columns.Add(1, "Badge");
                    columns.Add(2, "TransactionDate");
                    columns.Add(3, "TransactionFriendlyId");
                    columns.Add(4, "ReviewStatus");
                    columns.Add(5, "Adjustment");
                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, "VendorGroupName");
                            columns.Add(9, "PostalCode");
                            columns.Add(10, "IsGenerateTires");
                            columns.Add(11, "OnRoadWeight");
                            columns.Add(12, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.DOT:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, "VendorGroupName");
                            columns.Add(9, "PostalCode");
                            columns.Add(10, "IsGenerateTires");
                            columns.Add(11, "ScaleWeight");
                            columns.Add(12, "EstWeight");
                            break;

                        case TreadMarksConstants.UCR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorGroupName");
                            columns.Add(8, "PostalCode");
                            columns.Add(9, "PaymentEligible");
                            columns.Add(10, "OnRoadWeight");
                            columns.Add(11, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.HIT:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, "VendorGroupName");
                            columns.Add(9, "PostalCode");
                            columns.Add(10, "OnRoadWeight");
                            columns.Add(11, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.STC:
                            columns.Add(6, "NameofGroupIndividual");
                            //OTSTM2-1215
                            columns.Add(7, "EventNumber");
                            columns.Add(8, "OnRoadWeight");
                            columns.Add(9, "OffRoadWeight");
                            //columns.Add(7, "VendorGroupName");
                            //columns.Add(8, "PostalCode");
                            //columns.Add(9, "OnRoadWeight");
                            //columns.Add(10, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.PTR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, "VendorGroupName");
                            columns.Add(9, "ScaleTicketNbr");
                            columns.Add(10, "OnRoadWeight");
                            columns.Add(11, "OffRoadWeight");
                            columns.Add(12, "ScaleWeight");
                            columns.Add(13, "EstWeight");
                            columns.Add(14, "Variance");

                            break;

                        case TreadMarksConstants.RTR:
                            columns.Add(6, "MarketLocation");
                            columns.Add(7, "TireMarketType");
                            columns.Add(8, "BuyerName");
                            columns.Add(9, "InvoiceNbr");
                            columns.Add(10, "OnRoadWeight");
                            columns.Add(11, "OffRoadWeight");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();
                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryOutbound) == SecurityResultType.EditSave);

                    PaginationDTO<HaulerCommonTransactionListViewModel, int> transactions = new PaginationDTO<HaulerCommonTransactionListViewModel, int>();
                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            transactions = transactionService.LoadHaulerTCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.DOT:
                            transactions = transactionService.LoadHaulerDOTTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.UCR:
                            transactions = transactionService.LoadHaulerUCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.HIT:
                            transactions = transactionService.LoadHaulerHITTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId, param.DataSubType);
                            if (param.DataSubType.Contains("inbound"))
                            {
                                bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            }
                            break;
                        case TreadMarksConstants.STC:
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            transactions = transactionService.LoadHaulerSTCTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.PTR:
                            transactions = transactionService.LoadHaulerPTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.RTR:
                            transactions = transactionService.LoadHaulerRTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }

                    var json = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json, false);
                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData, error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetHaulerStaffTransactionsList(DataGridModelExtend param)
        {
            //var allTransactions = this.transactionService.LoadAllTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, VendorId);

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    //int iZoneType = 1;
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    columns = new Dictionary<int, string>();
                    columns.Add(0, "DeviceName");
                    columns.Add(1, "TransactionFriendlyId");
                    columns.Add(2, "ReviewStatus");
                    columns.Add(3, "Adjustment");
                    columns.Add(4, "Notes");
                    columns.Add(5, "TransactionDate");
                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorGroupName");
                            columns.Add(8, "FSA");
                            columns.Add(9, TreadMarksConstants.PLT);
                            columns.Add(10, TreadMarksConstants.MT);
                            columns.Add(11, TreadMarksConstants.AGLS);
                            columns.Add(12, TreadMarksConstants.IND);
                            columns.Add(13, TreadMarksConstants.SOTR);
                            columns.Add(14, TreadMarksConstants.MOTR);
                            columns.Add(15, TreadMarksConstants.LOTR);
                            columns.Add(16, TreadMarksConstants.GOTR);
                            columns.Add(17, "IsGenerateTires");
                            columns.Add(18, "OnRoadWeight");
                            columns.Add(19, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.DOT:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorGroupName");
                            columns.Add(8, "FSA");
                            columns.Add(9, TreadMarksConstants.MOTR);
                            columns.Add(10, TreadMarksConstants.LOTR);
                            columns.Add(11, TreadMarksConstants.GOTR);
                            columns.Add(12, "IsGenerateTires");
                            columns.Add(13, "ScaleWeight");
                            columns.Add(14, "EstWeight");
                            break;

                        case TreadMarksConstants.UCR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorGroupName");
                            columns.Add(8, "FSA");
                            columns.Add(9, TreadMarksConstants.PLT);
                            columns.Add(10, TreadMarksConstants.MT);
                            columns.Add(11, TreadMarksConstants.AGLS);
                            columns.Add(12, TreadMarksConstants.IND);
                            columns.Add(13, TreadMarksConstants.SOTR);
                            columns.Add(14, TreadMarksConstants.MOTR);
                            columns.Add(15, TreadMarksConstants.LOTR);
                            columns.Add(16, TreadMarksConstants.GOTR);
                            columns.Add(17, "OnRoadWeight");
                            columns.Add(18, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.HIT:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, TreadMarksConstants.PLT);
                            columns.Add(8, TreadMarksConstants.MT);
                            columns.Add(9, TreadMarksConstants.AGLS);
                            columns.Add(10, TreadMarksConstants.IND);
                            columns.Add(11, TreadMarksConstants.SOTR);
                            columns.Add(12, TreadMarksConstants.MOTR);
                            columns.Add(13, TreadMarksConstants.LOTR);
                            columns.Add(14, TreadMarksConstants.GOTR);
                            columns.Add(15, "OnRoadWeight");
                            columns.Add(16, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.STC:
                            columns.Add(6, "NameofGroupIndividual");
                            //OTSTM2-1215
                            columns.Add(7, "EventNumber");                         
                            columns.Add(8, TreadMarksConstants.PLT);
                            columns.Add(9, TreadMarksConstants.MT);
                            columns.Add(10, TreadMarksConstants.AGLS);
                            columns.Add(11, TreadMarksConstants.IND);
                            columns.Add(12, TreadMarksConstants.SOTR);
                            columns.Add(13, TreadMarksConstants.MOTR);
                            columns.Add(14, TreadMarksConstants.LOTR);
                            columns.Add(15, TreadMarksConstants.GOTR);
                            columns.Add(16, "OnRoadWeight");
                            columns.Add(17, "OffRoadWeight");
                            //columns.Add(7, "VendorGroupName");
                            //columns.Add(8, "FSA");
                            //columns.Add(9, TreadMarksConstants.PLT);
                            //columns.Add(10, TreadMarksConstants.MT);
                            //columns.Add(11, TreadMarksConstants.AGLS);
                            //columns.Add(12, TreadMarksConstants.IND);
                            //columns.Add(13, TreadMarksConstants.SOTR);
                            //columns.Add(14, TreadMarksConstants.MOTR);
                            //columns.Add(15, TreadMarksConstants.LOTR);
                            //columns.Add(16, TreadMarksConstants.GOTR);
                            //columns.Add(17, "OnRoadWeight");
                            //columns.Add(18, "OffRoadWeight");
                            break;

                        case TreadMarksConstants.PTR:
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorGroupName");
                            columns.Add(8, "FSA");
                            columns.Add(9, TreadMarksConstants.PLT);
                            columns.Add(10, TreadMarksConstants.MT);
                            columns.Add(11, TreadMarksConstants.AGLS);
                            columns.Add(12, TreadMarksConstants.IND);
                            columns.Add(13, TreadMarksConstants.SOTR);
                            columns.Add(14, TreadMarksConstants.MOTR);
                            columns.Add(15, TreadMarksConstants.LOTR);
                            columns.Add(16, TreadMarksConstants.GOTR);
                            columns.Add(17, "OnRoadWeight");
                            columns.Add(18, "OffRoadWeight");
                            columns.Add(19, "ScaleWeight");
                            columns.Add(20, "EstWeight");
                            columns.Add(21, "Variance");

                            //iZoneType = 2;
                            break;

                        case TreadMarksConstants.RTR:
                            columns.Add(6, "MarketLocation");
                            columns.Add(7, "TireMarketType");
                            columns.Add(8, "BuyerName");
                            columns.Add(9, "InvoiceNbr");
                            columns.Add(10, TreadMarksConstants.PLT);
                            columns.Add(11, TreadMarksConstants.MT);
                            columns.Add(12, TreadMarksConstants.AGLS);
                            columns.Add(13, TreadMarksConstants.IND);
                            columns.Add(14, TreadMarksConstants.SOTR);
                            columns.Add(15, TreadMarksConstants.MOTR);
                            columns.Add(16, TreadMarksConstants.LOTR);
                            columns.Add(17, TreadMarksConstants.GOTR);
                            columns.Add(18, "OnRoadWeight");
                            columns.Add(19, "OffRoadWeight");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> transactions = new PaginationDTO<HaulerStaffCommonTransactionListViewModel, int>();//transactionService.LoadHaulerStaffTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);

                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryOutbound) == SecurityResultType.EditSave);

                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            transactions = transactionService.LoadHaulerStaffTCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.DOT:
                            transactions = transactionService.LoadHaulerStaffDOTTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.UCR:
                            transactions = transactionService.LoadHaulerStaffUCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.HIT:
                            transactions = transactionService.LoadHaulerStaffHITTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId, param.DataSubType);
                            if (param.DataSubType.Contains("inbound"))
                            {
                                bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            }
                            break;
                        case TreadMarksConstants.STC:
                            transactions = transactionService.LoadHaulerStaffSTCTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.PTR:
                            transactions = transactionService.LoadHaulerStaffPTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.RTR:
                            transactions = transactionService.LoadHaulerStaffRTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }

                    //OTSTM2-1064
                    var cBTotalTireCount = Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBTotalTireCount").Value);
                    var totalTireCountDOT = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOT").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOT").Value);
                    var totalTireCountHIT = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountHIT").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountHIT").Value);
                    var totalTireCountPTR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountPTR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountPTR").Value);
                    var totalTireCountRTR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountRTR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountRTR").Value);
                    var totalTireCountSTC = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountSTC").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountSTC").Value);
                    var totalTireCountTCR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountTCR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountTCR").Value);
                    var totalTireCountUCR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountUCR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountUCR").Value);

                    transactions.DTOCollection.ForEach(c =>
                    {
                        c.SetFlagQtyOverDOT(cBTotalTireCount, totalTireCountDOT);
                        c.SetFlagQtyOverHIT(cBTotalTireCount, totalTireCountHIT);
                        c.SetFlagQtyOverPTR(cBTotalTireCount, totalTireCountPTR);
                        c.SetFlagQtyOverRTR(cBTotalTireCount, totalTireCountRTR);
                        c.SetFlagQtyOverSTC(cBTotalTireCount, totalTireCountSTC);
                        c.SetFlagQtyOverTCR(cBTotalTireCount, totalTireCountTCR);
                        c.SetFlagQtyOverUCR(cBTotalTireCount, totalTireCountUCR);
                    });

                    var json = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json, false);
                }
            }
            return Json(new { status = "", error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);
        }


        #endregion        

        #region Export to CSV Service Methods
        public ActionResult ExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            columns.Add("iPad/Form");
            columns.Add("Badge");
            columns.Add("Transaction Date");
            List<Func<HaulerCommonTransactionListViewModel, string>> delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    columns.Add("TCR #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Collector");
                    columns.Add("Business Name");
                    columns.Add("VendorGroupName");
                    columns.Add("Postal Code");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => (null == u.VendorNumber)?string.Empty:u.VendorNumber.ToString(),
                        u => u.VendorBusinessName,
                        u => u.VendorGroupName,
                        u => u.PostalCode,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.DOT:
                    columns.Add("DOT #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Collector");
                    columns.Add("Business Name");
                    columns.Add("VendorGroupName");
                    columns.Add("Postal Code");
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("Scale Weight(KG)");
                    columns.Add("Est. Weight(KG)");

                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.VendorNumber,
                        u => u.VendorBusinessName,
                        u => u.VendorGroupName,
                        u => u.PostalCode,
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.UCR:
                    columns.Add("UCR #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Reg.#/Site name");
                    columns.Add("VendorGroupName");
                    columns.Add("Postal Code");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Reason Not Eligible");
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.VendorNumber,
                        u => u.VendorGroupName,
                        u => u.PostalCode,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.ReasonNotEligible,
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.HIT:
                    columns.Add("HIT #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Hauler");
                    columns.Add("Business Name");
                    columns.Add("VendorGroupName");
                    columns.Add("Postal Code");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.VendorNumber,
                        u => u.VendorBusinessName,
                        u => u.VendorGroupName,
                        u => u.PostalCode,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    if (null != dataSubType)
                    {
                        sHITtype = "-" + dataSubType.ToString();
                    }
                    break;
                case TreadMarksConstants.STC:
                    columns.Add("STC #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Name of Group/Individual");
                    columns.Add("Event Number"); //OTSTM2-1215
                    //columns.Add("VendorGroupName"); //OTSTM2-1215
                    //columns.Add("Postal Code"); //OTSTM2-1215
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.NameofGroupIndividual,
                        u => u.EventNumber, //OTSTM2-1215
                        //u => u.VendorGroupName, //OTSTM2-1215
                        //u => u.PostalCode, //OTSTM2-1215
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;
                case TreadMarksConstants.PTR:
                    columns.Add("PTR #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Processor");
                    columns.Add("Business Name");
                    columns.Add("VendorGroupName");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Scale Ticket Number");
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    columns.Add("Scale Weight(KG)");
                    columns.Add("Est. Weight(KG)");
                    columns.Add("Variance");
                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.VendorNumber,

                        u => u.VendorBusinessName,
                        u => u.VendorGroupName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.ScaleTicketNbr,
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),

                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                        u => Math.Round(u.Variance, 2, MidpointRounding.AwayFromZero).ToString(),
                    };

                    break;
                case TreadMarksConstants.RTR:
                    columns.Add("RTR #");
                    columns.Add("Review Status"); //OTSTM2-416
                    columns.Add("Adjusted By"); //OTSTM2-416
                    columns.Add("Market Location");
                    columns.Add("Tire Market Type");
                    columns.Add("Buyer Name");
                    columns.Add("Invoice Number");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    delegates = new List<Func<HaulerCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(), //OTSTM2-416
                        u => u.Adjustment.ToString(), //OTSTM2-416
                        u => u.MarketLocation.ToString(),

                        u => u.TireMarketType,
                        u => u.BuyerName,
                        u => u.InvoiceNbr,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                default:
                    break;
            };
            #endregion

            //var Transactions = this.transactionService.GetExportDetailsParticipantTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);
            List<HaulerCommonTransactionListViewModel> transactions = new List<HaulerCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    transactions = transactionService.LoadHaulerTCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.DOT:
                    transactions = transactionService.LoadHaulerDOTTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.UCR:
                    transactions = transactionService.LoadHaulerUCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.HIT:
                    transactions = transactionService.LoadHaulerHITTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId, dataSubType).DTOCollection;
                    break;
                case TreadMarksConstants.STC:
                    transactions = transactionService.LoadHaulerSTCTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.PTR:
                    transactions = transactionService.LoadHaulerPTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.RTR:
                    transactions = transactionService.LoadHaulerRTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }

            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult StaffExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            int vendorId = (null != vendor) ? vendor.VendorId : 0;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            List<Func<HaulerStaffCommonTransactionListViewModel, string>> delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    columns.Add("iPad/Form");
                    columns.Add("TCR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Collector");
                    columns.Add("VendorGroupName");
                    columns.Add("FSA");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        u => u.VendorGroupName,
                        u => u.VendorRateGroupName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.DOT:
                    columns.Add("iPad/Form");
                    columns.Add("DOT #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Collector");
                    columns.Add("VendorGroupName");
                    columns.Add("FSA");
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("Scale Weight(KG)");
                    columns.Add("Est. Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        u => u.VendorGroupName,
                        u => u.VendorRateGroupName,
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.UCR:
                    columns.Add("iPad/Form");
                    columns.Add("UCR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Reg.#/Site Name");
                    columns.Add("VendorGroupName");
                    columns.Add("FSA");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        u => u.VendorGroupName,
                        u => u.VendorRateGroupName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.HIT:
                    columns.Add("iPad/Form");
                    columns.Add("HIT #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.STC:
                    columns.Add("iPad/Form");
                    columns.Add("STC #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Group/Ind name");
                    columns.Add("Event Number"); //OTSTM2-1215
                    //columns.Add("VendorGroupName"); //OTSTM2-1215
                    //columns.Add("FSA"); //OTSTM2-1215
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.NameofGroupIndividual,
                        u => u.EventNumber, //OTSTM2-1215
                        //u => u.VendorGroupName, //OTSTM2-1215
                        //u => u.VendorRateGroupName, //OTSTM2-1215
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                case TreadMarksConstants.PTR:
                    columns.Add("iPad/Form");
                    columns.Add("PTR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Processor");
                    columns.Add("VendorGroupName");
                    columns.Add("FSA");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");
                    columns.Add("Scale Weight");
                    columns.Add("Est Weight");
                    columns.Add("Variance %");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        u => u.VendorGroupName,
                        u => u.VendorRateGroupName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                        u => Math.Round((u.Variance ?? 0), 2, MidpointRounding.AwayFromZero).ToString(),
                    };

                    break;

                case TreadMarksConstants.RTR:
                    columns.Add("iPad/Form");
                    columns.Add("RTR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Market location");
                    columns.Add("Market Type");
                    columns.Add("Buyer name");
                    columns.Add("invoice#");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("On Road Weight(KG)");
                    columns.Add("Off Road Weight(KG)");

                    delegates = new List<Func<HaulerStaffCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.MarketLocation,
                        u => u.TireMarketType,
                        u => u.BuyerName,
                        u => u.InvoiceNbr,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.OnRoadWeight.ToString(),
                        u => u.OffRoadWeight.ToString(),
                    };
                    break;

                default:
                    break;
            };
            #endregion

            //var Transactions = this.transactionService.GetExportDetailsStaffTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);
            List<HaulerStaffCommonTransactionListViewModel> transactions = new List<HaulerStaffCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    transactions = transactionService.LoadHaulerStaffTCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.DOT:
                    transactions = transactionService.LoadHaulerStaffDOTTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.UCR:
                    transactions = transactionService.LoadHaulerStaffUCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.HIT:
                    transactions = transactionService.LoadHaulerStaffHITTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId, dataSubType).DTOCollection;
                    break;
                case TreadMarksConstants.STC:
                    transactions = transactionService.LoadHaulerStaffSTCTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.PTR:
                    transactions = transactionService.LoadHaulerStaffPTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.RTR:
                    transactions = transactionService.LoadHaulerStaffRTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }
            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #endregion
    }
}