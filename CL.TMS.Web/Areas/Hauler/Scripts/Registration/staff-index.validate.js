﻿//ApplicationHauler js
$(function () {
    CommonValidation.initializeStandardFormValidations();

    /** Diable all the checkbox after approve */
    if ($('#status').val() == 'Approved' || $('#status').val() == 'BankInformationSubmitted' || $('#status').val() == 'BankInformationApproved') {
        $('#ApplicationHaulerID input[type=checkbox]').not("[name='TireDetails.TireItem'],[class='isMailingAddressDifferent'],[id='ContactAddressSameAsBusinessAddress']").attr('disabled', 'true');
        $('#HaulerDetails_IsTaxExempt').removeAttr('disabled');

    }
    var cbMailingAddressSameAsBusiness = $('#MailingAddressSameAsBusiness');
    var cbContactAddressSameAsBusinessAddress = $('#ContactAddressSameAsBusinessAddress');
    var cbHaulerDetailsHaulerDetailsVendorIsTaxExempt = $('#HaulerDetails_HaulerDetailsVendor_CommercialLiabHSTNumber');

    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];
    var doHighlight = true;

    /* START GLOBAL VALIDATION METHODS */

    var stripHtml = function (html) {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    };

    var dynamicErrorMsg = function (params, element) {
        var labelName = 'Field';
        if ($(element).prev('label').html()) {
            var labelElementHtml = $($(element).prev('label').html());
            labelName = labelElementHtml.html();
        }
        else if ($(element).parent('div.dropdown').prevAll('label')) {
            labelName = stripHtml($(element).parent('div.dropdown').prevAll('label').html());
        }
        return 'Invalid ' + labelName;
    };

    //removes the green/red highlights from element
    var resetHighlight = function (element, isRequired) {
        $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
        $(element).next('span').removeClass('glyphicon-remove');
        $(element).nextAll('.help-block:first').remove();
        if (isRequired) {
            $(element).closest('.form-group').addClass('has-required');
            $(element).next('span').addClass('glyphicon-ok');
        }
    }

    $.validator.addMethod('lessThanOrEqualTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam <= today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('greaterThanTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam > today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-required", function (value, element) {
        if (value)
            return true;
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-regex-phone", function (value, element) {
        var regex = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$|^$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, 'Expected format ###-###-####');

    $.validator.addMethod("dynamic-regex-postal", function (value, element) {
        var regex = /^[A-Za-z][0-9][A-Za-z]([ ]?)[0-9][A-Za-z][0-9]|([0-9]{5})(?:[- ][0-9]{4})?$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-regex-email", function (value, element) {
        var regex = /^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("TireDetailsRequired", function (value, element) {
        var radioStr = $(element).val();
        if (typeof radioStr == 'undefined') {
            return false;
        }
        var radioBool = JSON.parse(radioStr.toLowerCase());
        if (radioBool && $("select[name='TireDetails.HRelatedProcessor'] option:selected").index() == "0") {
            return false;
        }
        //value is no; thus return true
        return !radioBool;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("HRelatedProcessorRequired", function (value, element) {
        if (typeof $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'undefined') {
            return false;
        }
        else if ($("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'True' && $('#HRelatedProcessor')[0].selectedIndex == 0) {
            return false;
        }
        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('dynamic-certificate-required', function (value, element) {
        var index = $(element).index('.dynamic-certificate-required');
        var maxStorage = $('input[name="SortYardDetails[' + index + '].MaxStorageCapacity"]').attr('data-maxstorage');
        var val = parseFloat(maxStorage);
        if (val && val >= 50) {
            if (value)
                return true;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-whitespace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    //$.validator.addMethod('frenchCharacters', function (value, element) {
    //      return this.optional(element) || /[^a-zA-Z0-9 àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]{1,}/i.test(value);
    //})

    //$.validator.addMethod('specialCharacters', function (value, element) {
    //      return this.optional(element) || /^[a-zA-Z0-9!@#$&()\\-`.+,/\"]{1,}$/i.test(value);
    //});

    /* END GLOBAL VALIDATION METHODS */

    /* START BUSINESS INFORMATION VALIDATION */

    var validatorBusinessInfo = $('#fmBusinessLocation').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'BusinessLocation.BusinessName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.OperatingName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.BusinessAddress.AddressLine1': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.City': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.Province': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Postal': {
                required: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$'
            },
            'BusinessLocation.BusinessAddress.Country': {
                required: true,
            },
            'BusinessLocation.Phone': {
                required: true,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
            },
            'BusinessLocation.Extension': {
                required: false
            },
            //'BusinessLocation.BusinessLocationAddress.Ext': {
            //    required: false,
            //    //inInvalidList: true
            //},
            'BusinessLocation.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.AddressLine1': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.City': {
                required: function (element) {
                    return !cbMailingAddressSameAsBusiness.is(':checked');
                }
            },
            'BusinessLocation.MailingAddress.Province': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.Postal': {
                //inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.Country': {
                // inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'BusinessLocation.BusinessName': 'Invalid Business Name',
            'BusinessLocation.OperatingName': 'Invalid Operating Name',
            'BusinessLocation.BusinessAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.BusinessAddress.City': 'Invalid City',
            'BusinessLocation.BusinessAddress.Province': 'Invalid Province/State',
            'BusinessLocation.BusinessAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.BusinessAddress.Postal': 'Invalid Postal Code',
            'BusinessLocation.BusinessAddress.Country': 'Invalid Country',
            'BusinessLocation.Phone': {
                regquired: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'BusinessLocation.Extension': 'Invalid Ext',
            'BusinessLocation.Email': 'Invalid Email',
            'BusinessLocation.MailingAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.MailingAddress.City': 'Invalid City',
            'BusinessLocation.MailingAddress.Province': 'Invalid Province/State',
            'BusinessLocation.MailingAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.MailingAddress.Postal': 'Invalid Postal/Zip',
            'BusinessLocation.MailingAddress.Country': 'Invalid Country',
        }
    });

    /* END BUSINESS INFORMATION VALIDATION */

    /* CONTACT INFORMATION VALIDATION */

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            '': {
                required: true,
                regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'ContactInformation[0].ContactInformationContact.Name': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.Position': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Ext': {
                required: false
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$'
            },
            'ContactInformation[0].ContactInformationAddress.Address1': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': 'Invalid Primary Contact Name',
            'ContactInformation[0].ContactInformationContact.Position': 'Invalid Position',
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'ContactInformation[0].ContactInformationContact.Ext': 'Invalid Ext.',
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: 'Expected format ###-###-####',
            },
            'ContactInformation[0].ContactInformationContact.Email': 'Invalid Email',
            'ContactInformation[0].ContactInformationAddress.Address1': 'Invalid Address Line 1',
            'ContactInformation[0].ContactInformationAddress.Address2': 'Invalid Address Line 2',
            'ContactInformation[0].ContactInformationAddress.City': 'Invalid City',
            'ContactInformation[0].ContactInformationAddress.PostalCode': 'Invalid Postal /Zip',
            'ContactInformation[0].ContactInformationAddress.Province': 'Invalid Province/State',
            'ContactInformation[0].ContactInformationAddress.Country': 'Invalid Country',
        }
    });

    /* END CONTACT INFORMATION VALIDATION */

    /* SORT YARD DETAILS */

    var validatorSortYardInfo = $('#fmSortYardDetails').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            //return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.City': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Province': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Address2': {
                required: false
            },
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsAddress.Country': {
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': {
                regex: '^[0-9]{1,}$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': {
                inInvalidList: true,
                required: function (element) {
                    var txtMaxStorageCap = $('#SortYardDetails_0__SortYardDetailsVendorStorageSiteModel_MaxStorageCapacity');
                    var val = parseFloat(txtMaxStorageCap.val());
                    if (val)
                        return val >= 50;
                    else
                        return false;
                }
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': 'Invalid Address Line 1',
            'SortYardDetails[0].SortYardDetailsAddress.City': 'Invalid City',
            'SortYardDetails[0].SortYardDetailsAddress.Province': 'Invalid Province/State',
            'SortYardDetails[0].SortYardDetailsAddress.Address2': 'Invalid Address Line 2',
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': 'Invalid Postal/Zip',
            'SortYardDetails[0].SortYardDetailsAddress.Country': 'Invalid Country',
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': 'Invalid Capacity (tonnes)',
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': 'Invalid Certificate of Approval #'
        }
    });

    /* END SORT YARD DETAILS */

    /* HAULER DETAILS */

    var validatorHaulerDetailsInfo = $('#fmHaulerDetails').validate({
        ignore: '.no-validate, :hidden',

        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                if (!$(element).next('span').hasClass('input-group-addon'))
                    $(element).next('span').addClass('glyphicon-remove');
                else {
                    if ($(element).parent('.input-group').next('span').hasClass('glyphicon-ok')) {
                        $(element).parent('.input-group').next('span').removeClass('glyphicon-ok');
                        $(element).parent('.input-group').next('span').addClass('glyphicon-remove');
                    }
                }
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                if (!$(element).next('span').hasClass('input-group-addon'))
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                else {
                    if ($(element).parent('.input-group').next('span').hasClass('glyphicon-remove')) {
                        $(element).parent('.input-group').next('span').removeClass('glyphicon-remove');
                        $(element).parent('.input-group').next('span').addClass('glyphicon-ok');
                    }
                }
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            'HaulerDetails.BusinessStartDate': {
                required: true,
                lessThanOrEqualTodayDate: true
            },
            'HaulerDetails.BusinessNumber': {
                required: true
            },
            'HaulerDetails.CommercialLiabHstNumber': {
                regex: '^[0-9]{9}$',
                required: function (element) {
                    return !cbHaulerDetailsHaulerDetailsVendorIsTaxExempt.is(':checked');
                }
            },
            'HaulerDetails.IsTaxExempt': {
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#HaulerDetails_HaulerDetailsVendor_CommercialLiabHSTNumber'), true);
                    }
                    return false;
                }
            },
            'HaulerDetails.HasMoreThanOneEmp': {
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#HaulerDetails_HaulerDetailsVendor_WSIBNumber'), true);
                    }
                    return true;
                }
            },
            'HaulerDetails.HIsGVWR': {
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#HaulerDetails_HaulerDetailsVendor_CVORNumber'), true);
                        resetHighlight($('#HaulerDetails_HaulerDetailsVendor_CVORExpiryDate'), true);
                    }
                    return true;
                }
            },

            'HaulerDetails.CommercialLiabInsurerName': {
                required: true
            },
            'HaulerDetails.CommercialLiabInsurerExpDate': {
                required: true,
                greaterThanTodayDate: true
            },
            'HaulerDetails.CvorNumber': {
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR = $('#HaulerDetails_HaulerDetailsVendor_HIsGVWR:checked');
                    return rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'HaulerDetails.CvorExpiryDate': {
                greaterThanTodayDate: true,
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR = $('#HaulerDetails_HaulerDetailsVendor_HIsGVWR:checked');
                    return rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'HaulerDetails.WsibNumber': {
                regex: '^[0-9]{7}$',
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHasMoreThanOneEmp = $('#HaulerDetails_HaulerDetailsVendor_HasMoreThanOneEmp:checked');
                    return rbtnHaulerDetailsHaulerDetailsVendorHasMoreThanOneEmp.val() == 'True';
                }
            },
        },
        messages: {
            'HaulerDetails.BusinessStartDate': '',
            'HaulerDetails.BusinessNumber': 'Invalid Ontario Business Number',
            'HaulerDetails.CommercialLiabHstNumber': 'Invalid HST Registration Number',
            'HaulerDetails.CommercialLiabInsurerName': 'Invalid Comercial Liability Insurance',
            'HaulerDetails.CommercialLiabInsurerExpDate': '',
            'HaulerDetails.CvorExpiryDate': '',
            'HaulerDetails.CvorNumber': 'Invalid CVOR Number',
            'HaulerDetails.WsibNumber': 'Invalid WSIP Number',
            'HaulerDetails.HIsGVWR': '',
            'HaulerDetails.HasMoreThanOneEmp': ''
        }
    });

    /* END HAULER DETAILS */

    /* TERMS AND CONDITIONS */

    var validatorTermsInfo = $('#fmTermsAndConditions').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TermsAndConditions.SigningAuthorityFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityPosition': {
                required: true,
            },
            'TermsAndConditions.FormCompletedByFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByPhone': {
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                required: true,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': {
                required: true,
                regex: '^([^a-z]*)([^a-z ]{2,})([ ]{1,}[^a-z ]+)*([ ]{1,}[^a-z ]{2,})+([^a-z]+)*$',
                //regex: '^([^a-z]{0,})*[^a-z ]{2,}[ ]{1,}([^a-z]{0,})*[^a-z ]{2,}([^a-z]{0,})*$',
            },
            'TermsAndConditions.AgreementAcceptedCheck': {
                required: true
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-signature')) {
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-required');
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-success');
                    $(element).closest('.termsandconditions-signature').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                else {
                    if ($(element).closest('.form-group').hasClass('has-required'))
                        $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-signature')) {
                    $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
                else {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
            }
        },
        success: function (e) {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TermsAndConditions.SigningAuthorityFirstName': 'Invalid First Name',
            'TermsAndConditions.SigningAuthorityLastName': 'Invalid Last Name',
            'TermsAndConditions.SigningAuthorityPosition': 'Invalid Position',
            'TermsAndConditions.FormCompletedByFirstName': 'Invalid First Name',
            'TermsAndConditions.FormCompletedByLastName': 'Invalid Last Name',
            'TermsAndConditions.FormCompletedByPhone': {
                required: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone'
            },
            'TermsAndConditions.AgreementAcceptedByFullName': 'Invalid Full Name',
            'TermsAndConditions.AgreementAcceptedCheck': 'Must be checked for submission'
        }
    });

    /* END TERMS AND CONDITIONS */

    /* Banking Information */
    var validatorBankingInfo = $('#frmBankingInformation').validate({
        ignore: ".ignore",
        onkeyup: function (element) {
            this.element(element);
        },

        onfocusout: function (element) {
            this.element(element);
        },
        rules: {
            'BankName': {
                //regex: '/^[\w\-\s]+$/',
                required: true,
                minlength: 2,
                maxlength: 50,
            },
            'TransitNumber': {
                //regex:'[0-9]',
                required: true,
                digits: true,
                minlength: 5,
                maxlength: 5,
            },
            'BankNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 3,
            },
            'AccountNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 15,
            },
            'Email': {
                required: ($('.isBankTransferNotficationPrimaryContact').is(':checked') ? false : true),
                email: true,
            },
            'CCEmail': {
                email: true,
            },
            'uploadFiles': {
                data: 'supportingDocumentsOption1'
            }
        },

        highlight: function (element) {
            if ($(element).val() || $(element).val() == '') {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            if ($(element).val()) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ($(element).val()) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
    });
    /*End Banking Information */

    /************************************************************************************************************
     ******************************* TIRE DETAILS START STAFF ********************************************************
     ************************************************************************************************************/
    var validatorTireDetailsInfo = $('#fmTireDetails').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight) {
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#item_checkbox'));
                }
                else if (element.attr("type") == "radio") {
                    error.insertAfter($(element).closest('.form-group'));
                }
                else {
                    ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'))
                }
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TireDetails.TireItem': {
                required: true
            },
            'TireDetails.HHasRelatioshipWithProcessor': {
                required: true,
                TireDetailsRequired: true
            },

            'TireDetails.HRelatedProcessor': {
                required: {
                    depends: function (element) {
                        return $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == "True";
                    }
                },
                HRelatedProcessorRequired: true
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-tiredetails')) {
                    $(element).closest('.form-group').css('border', '1px solid #a94442');
                }

                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');

                $('form #fmTireDetails label').addClass();
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-tiredetails')) {
                    $(element).closest('.form-group').css('border', 'none');
                }
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TireDetails.TireItem': 'Invalid Tire Type',
            'TireDetails.HRelatedProcessor': 'Invalid Processor',
            'TireDetails.HHasRelatioshipWithProcessor': ''
        }
    });

    /************************************************************************************************************
     ******************************* TIRE DETAILS ENDS  ********************************************************
     ************************************************************************************************************/
});

//OTSTM2-83
$(function () {
    StaffCommonValidations.BusinessLocation();
    StaffCommonValidations.ContactInfo();
    StaffCommonValidations.SupportingDoc();
    StaffCommonValidations.TermsAndConditions();

    //Sort Yard Details
    $('#fmSortYardDetails').dynamicReviewChkboxValidate({
        flagId: '#flagSortYard',
        formId: '#fmSortYardDetails',
        submitBtn: '#btnAddSort',

        //fmSortYardDetails business rules
        isIgnore: true,
        ignoreTrigger: '.maxStorageCapacity',
        ignoreRule: function () {
            //this function will contain parent template when called from dynamicReviewChkboxValidate
            var parent = this;
            if (this.length > 0) {
                parent = this[0] //get parent using parameters
            }
            //#SortYardDetails_0__MaxStorageCapacity
            var templateNumber = parent.className.match(/\d/);
            var maxStorageCapacityId = '#SortYardDetails_0__MaxStorageCapacity'.replace(/[0-9]/g, templateNumber);

            var maxStorageCapacity = $(parent).find(maxStorageCapacityId);
            var certificateOfApp_cb = $(parent).find('#SortYardDetails_cbCertificateOfApproval');

            //add ignore class if maxStorageCapacity > 50
            if ($(maxStorageCapacity).val() >= 50) {
                $(certificateOfApp_cb).removeClass('ignore');
            }
            else {
                $(certificateOfApp_cb).addClass('ignore');
            }
        }
    });

    //reviewCheckBox validation OTSTM2-83    
    $('#fmTireDetails').panelReviewCheckBoxValidate({
        flagId: '#flagTireDetails',
        rules: {
            'TireDetails.TireItem': {
                required: true,
                isChecked: $('#TireDetails_cbHRelatedProcessor').is(':checked')
            },
            'TireDetails.HHasRelatioshipWithProcessor': {
                required: true,
                isChecked: $('.relationship-ots').is(':checked')
            },
        }
    });

    //reviewCheckBox validation OTSTM2-83    
    $('#fmHaulerDetails').panelReviewCheckBoxValidate({
        flagId: '#flagHaulerDetails',
        rules: {
            'HaulerDetails.BusinessStartDate': {
                required: true,
                isChecked: $('#HaulerDetails_cbBusinessStartDate').is(':checked')
            },
            'HaulerDetails.BusinessNumber': {
                required: true,
                isChecked: $('#HaulerDetails_cbBusinessNumber').is(':checked')
            },

            'HaulerDetails.CommercialLiabHstNumber': {
                required: true,
                isChecked: $('#HaulerDetails_cbCommercialLiabHSTNumber').is(':checked')
            },
            'HaulerDetails.CommercialLiabInsurerExpDate': {
                required: true,
                isChecked: $('#HaulerDetails_cbCommercialLiabInsurerExpDate').is(':checked')
            },

            'HaulerDetails.HIsGvwr': {
                required: true,
                isChecked: $('#HaulerDetails_cbHIsGvwr').is(':checked')
            },
            'HaulerDetails.CvorNumber': {
                required: function () {
                    return ($('#HaulerDetails.HIsGvwr:checked').val() == 'True' || $('#pnlCVOR').css('display') !== 'none');
                },
                isChecked: $('#HaulerDetails_cbCvorNumber').is(':checked')
            },
            'HaulerDetails.CvorExpiryDate': {
                required: function () {
                    return ($('#HaulerDetails.HIsGvwr:checked').val() == 'True' || $('#pnlCVOR').css('display') !== 'none');
                },
                isChecked: $('#HaulerDetails_cbCvorExpiryDate').is(':checked')
            },
            'HaulerDetails.HasMoreThanOneEmp': {
                required: true,
                isChecked: $('#HaulerDetails_cbHasMoreThanOneEmp').is(':checked')
            },
            'HaulerDetails.WsibNumber': {
                required: function () {
                    return ($('.HaulerDetails_HasMoreThanOneEmp:checked').val() == 'True' || $('#pnlWSIB').css('display') !== 'none');
                },
                isChecked: $('#HaulerDetails_cbWsibNumber').is(':checked')
            },
        }
    });

});