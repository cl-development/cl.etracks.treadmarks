﻿// Popover Setup
var settings = {
    trigger: 'hover',
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};
$(function () {
    var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';
    var selectRelatedProcessorValue = $('#HRelatedProcessorSelectedValue').val();
    ApplicationCommon.populateProcessorDropDownList(selectRelatedProcessorValue, !IsHaulerAutoSaveDisabled);
    "use strict";
    BusinessLocation();
    ContactInformation();
    SortYardDetails();
    TireDetails();
    HaulerDetails();
    BankingInformation();
    AuditGroupSelection();
    SupportingDocuments();
    TermsAndConditions();
    var initialize = function () {
        DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        DisplayProcessorLetterTireDetails();
        contactAddressSameAs();
        ApplicationCommon.setContactAddressCountryDropDown();
        restrictOnlyNumbers();
        InitAutoSave();
        certificateOfApproval();
        titleCase();
        setExportUrl();
        //fix backspace error for checkboxes
        $('input[type="checkbox"]').keydown(function (e) {
            if (e.keyCode === 8) {
                return false;
            }
        });
    }
    initialize();
});
function setExportUrl() {
    var url = Global.StaffIndex.ExportToPdfUrl;
    $('#exportToPdf').attr('href', url);

}

function BusinessLocation() {
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    $('#MailingAddressSameAsBusiness').click(function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {
            CopyMailingAddress();
        }
        else {
            ClearMailingAddress();
        }
        toggleMailingAddress();
    });
    toggleMailingAddress();
};

function CopyMailingAddress() {
    $('#BusinessLocation_MailingAddress_AddressLine1').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('#BusinessLocation_MailingAddress_City').val($('#BusinessLocation_BusinessAddress_City').val())
    $('#BusinessLocation_MailingAddress_Province').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('#BusinessLocation_MailingAddress_AddressLine2').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('#BusinessLocation_MailingAddress_Postal').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('#BusinessLocation_MailingAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearMailingAddress() {
    $('#BusinessLocation_MailingAddress_AddressLine1').val('')
    $('#BusinessLocation_MailingAddress_City').val('')
    $('#BusinessLocation_MailingAddress_Province').val('')
    $('#BusinessLocation_MailingAddress_AddressLine2').val('')
    $('#BusinessLocation_MailingAddress_Postal').val('')
    $('#BusinessLocation_MailingAddress_Country').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function CopyContactAddress() {
    $('#ContactInformationList_0__ContactAddress_AddressLine1').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('#ContactInformationList_0__ContactAddress_City').val($('#BusinessLocation_BusinessAddress_City').val())
    $('#ContactInformationList_0__ContactAddress_Province').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('#ContactInformationList_0__ContactAddress_AddressLine2').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('#ContactInformationList_0__ContactAddress_Postal').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('#ContactInformationList_0__ContactAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearContactAddress() {
    $('#ContactInformationList_0__ContactAddress_AddressLine1').val('')
    $('#ContactInformationList_0__ContactAddress_City').val('')
    $('#ContactInformationList_0__ContactAddress_Province').val('')
    $('#ContactInformationList_0__ContactAddress_AddressLine2').val('')
    $('#ContactInformationList_0__ContactAddress_Postal').val('')

    $('select[name="ContactInformationList_0__ContactAddress_Country.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function ContactInformation() {
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();

    if (temp != undefined) {
        temp = temp.replace(/value=\".+\"/g, "value=''"); //OTSTM2-50 remove all default value of input
    }
    
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        contactTemplate = "<div class='template" + i + " parent'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    // CopyContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    //   ClearContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    var initCheckboxClearField = function () {
        $('input[type="checkbox"]').on('click', function () {
            clearField(this);
        });
    }

    $('#btnAddContact').on('click', function () {
        initCheckboxClearField();
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
    });
    $('.isContactAddressDifferent').click(function () {
        $(".contactAddressDifferent").toggle(this.unchecked);
    });
    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            currAddCount = contactTemplateCount++;
            tempDiv = "<div class='ctemp'><div class='template" + currAddCount + " parent'>" + temp + '</div></div><hr>'; //OTSTM2-50 change temp to tempDiv to avoid multiple <hr> after removing and adding
            $(tempDiv).appendTo('#divContactTemplate');
            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    //  CopyContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    //  ClearContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }
    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }

    primaryContactCheckName();

    //OTSTM2-50 
    $(document).on("click", ".btnRemoveContact", function (evt) {

        evt.stopImmediatePropagation();
        var contactSection = $(this).parent().parent();
        if (Global.StaffIndex.Settings.IsApprovedApplication) { //for registered application
            clearAllInput(contactSection);

            contactTemplateCount--;
            if (contactSection.parent().hasClass("ctemp")) { //for new added contact without refresh page
                if (contactSection.hasClass("template1") && $(".template2").length > 0) { //remove contact1, switch contact2 to contact1
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");

                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                }
                else { //remove contact2 or remove contact1 (no contact2)
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                }
            }
            else { //for existing contact
                if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                    contactSection.next().remove();
                    contactSection.remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");

                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                    //to remember selected country when switching from contact2 to contact1
                    var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                    switch (selectedValue) {
                        case "Canada":
                            $(".template1").find("select option:contains('Canada')").prop('selected', true);
                            break;
                        case "USA":
                            $(".template1").find("select option:contains('USA')").prop('selected', true);
                            break;
                        case "Other":
                            $(".template1").find("select option:contains('Other')").prop('selected', true);
                            break;
                        case "":
                            $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                            break;
                    }
                }
                else {
                    contactSection.next().remove();
                    contactSection.remove();
                }
            }
            $('#btnAddContact').closest('div.row').show();
        }
        else { //for unregistered application
            contactSection.addClass("contactToBeDeleted"); //add a temporary class to remember the contact that to be removed
            $("#modalRemoveContactConfirmation").modal("show");

            var currentContactClassName = contactSection.attr("class");
            var index = currentContactClassName.indexOf("template");
            var value = currentContactClassName.charAt(index +8);
            var msg = "Are you sure you want to remove Contact " + value + "?";           
            $('#modalRemoveContactConfirmation').on('shown.bs.modal', function () {
                $("#contactRemovingMsg").text(msg);
            });
        }
    });

    //remove above temporarily added class when click "Cancel" button in Modal
    $('#modalRemoveContactConfirmation').on('hidden.bs.modal', function () {
        $(".contactToBeDeleted").removeClass("contactToBeDeleted");
    });

    $(document).on("click", "#modalRemoveContactYesBtn", function (evt) {

        var contactSection = $(".contactToBeDeleted");
        clearAllInput(contactSection);

        contactTemplateCount--;
        if (contactSection.parent().hasClass("ctemp")) {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");

                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

            }
            else {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
            }
        }
        else {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.next().remove();
                contactSection.remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");

                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

                var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                switch (selectedValue) {
                    case "Canada":
                        $(".template1").find("select option:contains('Canada')").prop('selected', true);
                        break;
                    case "USA":
                        $(".template1").find("select option:contains('USA')").prop('selected', true);
                        break;
                    case "Other":
                        $(".template1").find("select option:contains('Other')").prop('selected', true);
                        break;
                    case "":
                        $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                        break;
                }

            }
            else {
                contactSection.next().remove();
                contactSection.remove();
            }
        }
        $('#btnAddContact').closest('div.row').show();
        processJson(); //to autosave
    });

    var clearAllInput = function (element) {
        element.find("input[type=text]").val("");
        element.find("input[type=checkbox]").prop("checked", false);
        element.find("select").find('option').removeAttr('selected');
        element.find("input[type=hidden]").val("0");
    }

    var replaceHtml = function (html) {
        var index = html.indexOf("ContactInformationList");
        var value = html.charAt(index + 23);
        var replaceValue = value - 1;
        var rp1 = "ContactInformationList_" + replaceValue;
        var rp2 = "ContactInformationList[" + replaceValue;
        var html = html.replace(/ContactInformationList_\d/g, rp1).replace(/ContactInformationList\[\d/g, rp2);
        return html;
    }
};

function SortYardDetails() {
    /* SORT YARD DETAILS */   

    //disabled and uncheck COA checkbox
    $('.sort-yard-capacity-cb').on('click', function () {
        if (!$(this).is(':checked'))
            $('.sort-yard-COA-cb').attr('disabled', 'disabled').removeAttr('checked');
        else
            $('.sort-yard-COA-cb').removeAttr('disabled');
    });
    /* END SORT YARD DETAILS */

    var sortYardtemp = $(".sortyardtemp").val();
    var sortYardtempBase = $(".sortyardtemp").val();

    if (sortYardtemp != undefined) {
        sortYardtempBase = sortYardtempBase.replace(/value=\".+\"/g, "value=''"); //OTSTM2-642 remove all default value of input
    }
    
    var sortYardtempIncremental;
    var sortYardTemplateCount = $('#sorYardCount').val();
    var maxStorageCap = 0;
    var currAddCount = 0;
    $('#SortYardCount').text("0");

    for (var i = 0; i < sortYardTemplateCount; i++) {
        var sortYardTemplate = $("#sortYardTemplate" + i).val();
        sortYardTemplate = "<div class='sortYardtemplate" + i + " parent'>" + sortYardTemplate + '</div><hr>';

        $(sortYardTemplate).appendTo('#fmSortYardDetails');

        //check if to hide certificate of approval
        var storage = $(".sortYardtemplate" + i + " input[data-maxstorage]");
        storage = parseFloat($(storage).attr('data-maxstorage'));

        if (!isNaN(storage)) {
            if (storage < 50) {
                //hide the div
                var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                $(certificate).closest('.col-md-4').hide();
            }
        }
        else {
            //hide the div
            var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
            $(certificate).closest('.col-md-4').hide();
        }

        if (i == 0)
            $('.sort-yard-address-disabled').attr('disabled', 'disabled');
        else {
            $('.sortYardtemplate' + i + ' input[type=text]').removeClass('sort-yard-address-disabled');
            $('.sortYardtemplate' + i + ' select').removeClass('sort-yard-address-disabled');
        }
    }
    $('#SortYardCount').text(sortYardTemplateCount);

    $('#btnAddSort').on('click', function () {
        addSortRow();
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
        titleCase();
    });

    var addSortRow = function () {
        currAddCount = sortYardTemplateCount++;
        //$('#SortYardCount').text(sortYardTemplateCount);  //OTSTM2-642 comment out because sortyardcount will only be updated after autosaving or saving
        sortYardtempIncremental = "<div class='ctemp'><div class='sortYardtemplate" + currAddCount + " parent'>" + sortYardtempBase + '</div></div><hr>';
        //Replace the default values
        sortYardtempIncremental = sortYardtempIncremental.replace(/\[0]/g, '[' + currAddCount + ']').replace(/\_0__/g, '_' + currAddCount + '__');
        $(sortYardtempIncremental).appendTo('#fmSortYardDetails');

        //set the datamaxstorage of the new added sortyard to 0 so its not nan
        $('.sortYardtemplate' + currAddCount + ' input[data-maxstorage]').attr('data-maxstorage', 0);
        $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');
        
        //OTSTM2-642 add "required" validation for unregistered application (only for new added sort yard), at the mean time, leave no validation for registered application as existing (0 can be added by system automatically for empty input after saving) 
        if (!Global.StaffIndex.Settings.IsApprovedApplication) { //for unregistered application
            $(".maxStorageCapacity").not(".dynamic-required").addClass("dynamic-required"); //add "dynamic-required" class if no "dynamic-required" class
        }

        for (var i = 0; i < sortYardTemplateCount; i++) {
            $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {
                if ((typeof $(this).attr('name') != 'undefined')) {
                    $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                }
                if ((typeof $(this).attr('id') != 'undefined')) {
                    $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
                }
            });

            //check if to hide certificate of approval
            var storage = $(".sortYardtemplate" + i + " input[data-maxstorage]");
            storage = parseFloat($(storage).attr('data-maxstorage'));

            if (!isNaN(storage)) {
                if (storage < 50) {
                    //hide the div
                    var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                    $(certificate).closest('.col-md-4').hide();
                }
            }
            else {
                //hide the div
                var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                $(certificate).closest('.col-md-4').hide();
            }       

            if (i == currAddCount) {
                $('.sortYardtemplate' + i + ' input[type=text]').removeClass('sort-yard-address-disabled');
                $('.sortYardtemplate' + i + ' select').removeClass('sort-yard-address-disabled');
            }

        }
    }
    var totalStorage = function () {
        var inputs = $("input[data-maxstorage]");
        maxStorageCap = 0;
        inputs.each(function () {
            //OTSTM2-642 comment out above and apply the followed code
            if (!isNaN(parseFloat($(this).val()))) {
                maxStorageCap += parseFloat($(this).val());
            }
        });
        if (!isNaN(parseFloat(maxStorageCap))) {
            $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
        }
    }
    var bindSortYardSelect = function () {
        $(".sortYardtemplate" + currAddCount + " select").each(function () {
            if (currAddCount > 0)
                $(this).children().removeAttr('selected');
        });
    }
    var renameSortYardLabel = function () {
        var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
        for (var j = 0; j < label.length; j++) {
            var num = 1;
            $(label[j]).each(function () {
                var newLabel = $(this).text().replace(/\d+/, num++);
                $(this).text(newLabel);
            });
        }
    }

    //copies code from business location to sort yard
    $('.sortyard-address').on('focusout', function () {
        var attr = $(this).attr('data-sortyard');
        if (typeof attr !== typeof undefined && attr) {
            if ($("input[name*='" + attr + "']").length > 0)
                $("input[name*='" + attr + "']").val($(this).val());
            if ($("select[name*='" + attr + "']").length > 0) {
                $("select[name*='" + attr + "']").val($(this).val());
            }
        }
    }).on();

    var initSortYard = function () {
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
    }
    initSortYard();

    // Sort Yard Details: This Sort Yard Capacity
    var sortYardDetailsTSYCContent = $('#sortYardDetailsTSYC').html(),
      sortYardDetailsTSYCSettings = {
          content: sortYardDetailsTSYCContent,
          width: 300,
          height: 100,
      };

    var popLargeTSYC = $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsTSYCSettings));

    //OTSTM2-642 
    $(document).on("click", ".btnRemoveSortYard", function (evt) {

        evt.stopImmediatePropagation();
        var sortYardSection = $(this).parent().parent();
        if (Global.StaffIndex.Settings.IsApprovedApplication) { //for registered application
            clearAllInput(sortYardSection);

            sortYardTemplateCount--;
            if (sortYardSection.parent().hasClass("ctemp")) { //if the removed sortyard is new added (without refresh page)
                if (sortYardSection.parent().nextAll("div").length > 0) { //if the removed sortyard is not the last one
                    //get current sort yard class name, for example 'sortYardtemplate3 parent'
                    var currentSortYardSectionClassName = sortYardSection.attr("class");
                    //get all followed sortyard
                    var allFollowdSiblings = sortYardSection.parent().nextAll("div");

                    //remove the current sortyard
                    sortYardSection.parent().next().remove(); //remove <hr>
                    sortYardSection.parent().remove();

                    //get the value followed by class name 'sortYardtemplate' of current removed sortyard                                       
                    var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                    var value = parseInt(num);

                    //do id and name replacement(decrement) for the following sortyard, 
                    //for example, replace id, "SortYardDetails_3__SortYardAddress_AddressLine1" as "SortYardDetails_2__SortYardAddress_AddressLine1"
                    // or replace name, "SortYardDetails[3].SortYardAddress.AddressLine1" as "SortYardDetails[2].SortYardAddress.AddressLine1"
                    for(var i = 0; i < allFollowdSiblings.length; i++) {                    
                        var originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                        doTheReplacement(originalSortYardSection, value +i);
                    }
                }
                else { //if the removed sortyard is the last one, remove directly
                    sortYardSection.parent().next().remove();
                    sortYardSection.parent().remove();
                }
            }
            else { //for existing sortyard (not new added)
                if (sortYardSection.nextAll("div").length > 0) {//if the removed sortyard is not the last one

                    //get current sort yard class name, for example 'sortYardtemplate3 parent'
                    var currentSortYardSectionClassName = sortYardSection.attr("class");
                    var allFollowdSiblings = sortYardSection.nextAll("div");

                    //remove the current sortyard
                    sortYardSection.next().remove();
                    sortYardSection.remove();

                    //get the value followed by class name 'sortYardtemplate' of current removed sortyard                    
                    var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                    var value = parseInt(num);

                    for (var i = 0; i < allFollowdSiblings.length; i++) {
                        var originalSortYardSection = allFollowdSiblings[i];
                        if ($(originalSortYardSection).hasClass("ctemp")) { //if the followed sortyard is new added
                            originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                        }
                        doTheReplacement(originalSortYardSection, value + i);
                    }                    
                }
                else { //if the removed sortyard is the last one, remove directly
                    sortYardSection.next().remove();
                    sortYardSection.remove();
                }
            }
            totalStorage(); //update Maximum Capacity of Sort Yards
            
            certificateOfApproval(); //update Total # of Sort Yards
        }
        else { //for unregistered application
            sortYardSection.addClass("sortYardToBeDeleted"); //add a temporary class to remember the sortyard that to be removed
            $("#modalRemoveSortYardConfirmation").modal("show");

            var currentSortYardSectionClassName = sortYardSection.attr("class");                        
            var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];           
            var value = parseInt(num) + 1;
            var msg = "Are you sure you want to remove Sort Yard " + value + "?";
            $('#modalRemoveSortYardConfirmation').on('shown.bs.modal', function () {
                $("#sortYardRemovingMsg").text(msg);
            });
        }
    });

    //remove above temporarily added class when click "Cancel" button in Modal
    $('#modalRemoveSortYardConfirmation').on('hidden.bs.modal', function () {
        $(".sortYardToBeDeleted").removeClass("sortYardToBeDeleted");
    });

    $(document).on("click", "#modalRemoveSortYardYesBtn", function (evt) {

        var sortYardSection = $(".sortYardToBeDeleted"); //get the removed sortyard
        clearAllInput(sortYardSection);

        sortYardTemplateCount--;
        if (sortYardSection.parent().hasClass("ctemp")) {
            if (sortYardSection.parent().nextAll("div").length > 0 ) {

                var currentSortYardSectionClassName = sortYardSection.attr("class");
                var allFollowdSiblings = sortYardSection.parent().nextAll("div");

                sortYardSection.parent().next().remove();
                sortYardSection.parent().remove();
                
                var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                var value = parseInt(num);
           
                for (var i = 0; i < allFollowdSiblings.length; i++) {                    
                    var originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                    doTheReplacement(originalSortYardSection, value + i);
                }               
            }
            else {
                sortYardSection.parent().next().remove();
                sortYardSection.parent().remove();
            }
        }
        else {
            if (sortYardSection.nextAll("div").length > 0) {
                
                var currentSortYardSectionClassName = sortYardSection.attr("class");
                var allFollowdSiblings = sortYardSection.nextAll("div");

                sortYardSection.next().remove();
                sortYardSection.remove();
                               
                var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                var value = parseInt(num);

                for (var i = 0; i < allFollowdSiblings.length; i++) {   
                    var originalSortYardSection = allFollowdSiblings[i];
                    if ($(originalSortYardSection).hasClass("ctemp")) {
                        originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                    }                    
                    doTheReplacement(originalSortYardSection, value + i);
                }               
            }
            else {
                sortYardSection.next().remove();
                sortYardSection.remove();
            }
        }
        processJson(); //to autosave
        totalStorage(); //update Maximum Capacity of Sort Yards

        certificateOfApproval(); //update Total # of Sort Yards
    });

    var clearAllInput = function (element) {
        element.find("input[type=text]").val("");
        element.find("input[type=checkbox]").prop("checked", false);
        element.find("select").find('option').removeAttr('selected');
        element.find("input[type=hidden]").val("0");
    }
   
    var doTheReplacement = function (originalSortYardSection, replaceValue) {

        //there is a difference of 1 between UI and corresponding id and name, for example, UI is "Sort Yard 2 Address Line 1", but the id is "SortYardDetails_1__SortYardAddress_AddressLine1"
        var replaceValuePlusOne = parseInt(replaceValue) + 1; 
       
        //replace id and name
        var htmlString = $(originalSortYardSection).html();
        var rp1 = "SortYardDetails_" + replaceValue;
        var rp2 = "SortYardDetails[" + replaceValue;
        var htmlString = htmlString.replace(/SortYardDetails_\d+/g, rp1).replace(/SortYardDetails\[\d+/g, rp2);
        $(originalSortYardSection).html(htmlString);
        //replace class name
        $(originalSortYardSection).removeClass("sortYardtemplate" + replaceValuePlusOne).addClass("sortYardtemplate" + replaceValue);
        //replace UI
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-address1-label").text("Sort Yard " + replaceValuePlusOne + " Address Line 1");
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-address2-label").text("Sort Yard " + replaceValuePlusOne + " Address Line 2");
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-capacity").text("Sort Yard " + replaceValuePlusOne + " Capacity (tonnes)");       
    }
};
function TireDetails() {
    //code if necessary
    $('#cb_tiredetails').change(function () {
        if ($(this).is(":checked")) $('#cb_tiredetails').attr('checked', true);
        else $('#cb_tiredetails').attr('checked', false);
    });

    /*Needed for tiredetails validation */
    var OTSProcessorRadio;
    var OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();

    //fix
    //if OTS Processor is null disable the dropdown list
    if (!$('input[type=radio][class=HHasRelatioshipWithProcessor]:checked').val() || $('input[type=radio][class=HHasRelatioshipWithProcessor]:checked').val() == 'False') {
        $('.HHasRelatioshipWithProcessor').attr('disabled', true);
        $('#HRelatedProcessorSelectedValue').val('');
    }

    $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
        if (this.checked)
            OTSProcessorRadio = $(this).val();
    });

    $('input[type=radio][class=HHasRelatioshipWithProcessor]').change(function () {
        DisplayProcessorLetterTireDetails();

        OTSProcessorRadio = $(this).val();

        if (OTSProcessorRadio == "False") {
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').closest('.form-group').removeClass('has-error');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').removeClass('has-success');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').addClass('has-required');
            $('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
            $("#modalConfirmRelationshipWithProcessor").modal('show');
        }
        else {
            $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
        }
        OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();
        $("#fmTireDetails").valid()
    });

    $('select[name="TireDetails.HRelatedProcessor"]').change(function () {
        $("#fmTireDetails").valid();
    });

    $("#modalConfirmRelationshipWithProcessorNo").on("click", function () {
        //$("#modalConfirmRelationshipWithProcessor").modal('hide');
        //$('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
        //$('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
        //OTSProcessorRadio = "True";
        //$("#fmTireDetails").valid();
        //$("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
        //    if ($(this).val() == "True") {
        //        this.checked = true;
        //    }
        //    else this.checked = false;
        //});
    });
    $("#modalConfirmRelationshipWithProcessorYes").on("click", function () {
        $("#modalConfirmRelationshipWithProcessor").modal('hide');
        $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", true);
    });
    /*End tire details validation*/
};

function HaulerDetails() {
    //applies to radiobutton for HaulerDetails_CVORNumber
    $('.cvor').on('change', function () {
        DisplayCVORDocumentOption();
    });

    //applies to radiobutton for HaulerDetails.HaulerDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    $('#HaulerDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });
        
    var readOnlyStr = String(typeof Global.StaffIndex.Model.IsReadOnly !== 'undefined' ? Global.StaffIndex.Model.IsReadOnly : "true");
    var readOnlyBool = readOnlyStr.toLowerCase() == "true";

    //security
    var calendarReadOnlyBool = Global.StaffIndex.Security.HaulerDetailsCalendarReadonly.toLowerCase() == 'true';

    if (!calendarReadOnlyBool) {
        $('#dpBusinessStartDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#dpInsuranceExpiryDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#dpCVORExpiryDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    }

    var disabledHSTTextBox = function () {
        if (!($('#HaulerDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#HaulerDetails_IsTaxExempt').is(':checked'))
                $('#HaulerDetails_CommercialLiabHstNumber').val('').attr('disabled', 'disabled');
            else
                $('#HaulerDetails_CommercialLiabHstNumber').removeAttr('disabled');
        }
    }
    $('#HaulerDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });

    $('.radio input.cvor').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlCVOR').toggle(show);
        if (!show) {
            $('#pnlCVOR').find('input').val('');
        }
    });

    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    //disable checkboxes for CVORAndExpiryDate
    var disableCVORAndExpiryDateCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#HaulerDetails_cbCvorNumber').removeAttr('disabled');
            $('#HaulerDetails_cbCvorExpiryDate').removeAttr('disabled');
        }
        else {
            $('#HaulerDetails_cbCvorNumber').attr('disabled', 'disabled').removeAttr('checked');
            $('#HaulerDetails_cbCvorExpiryDate').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    //disable checkboxes for WSIBNumber
    var disableWSIBNumberCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#HaulerDetails_cbWsibNumber').removeAttr('disabled');
        }
        else {
            $('#HaulerDetails_cbWsibNumber').attr('disabled', 'disabled');
        }
    }

    $('#HaulerDetails_cbHIsGvwr').on('change', function () {
        disableCVORAndExpiryDateCheckbox(this);
    });

    $('#HaulerDetails_cbHasMoreThanOneEmp').on('change', function () {
        disableWSIBNumberCheckbox(this);
    });

    disabledHSTTextBox();
    $('#pnlCVOR').toggle($('.radio input.cvor:checked').val() == 'True');
    $('#pnlWSIB').toggle($('.radio input.wsib:checked').val() == 'True');
};
function BankingInformation() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });

    //Check whether all Bank Info validation check boxes are checked, if so, then change the application
    //status to completed
    $('.bank-info').on('click', function () {

        var allBankInfoChecked = true;

        $('.bank-info').each(function () {
            if (!$(this).is(":checked")) {
                allBankInfoChecked = false;
            }
        });

        if (allBankInfoChecked) {
            $.ajax({
                url: '/Collector/Registration/ChangeStatus',
                method: 'POST',
                dataType: "JSON",
                data: { applicationId: $('#applicationId').val(), status: 'Completed' },
                success: function (data) {
                    if (data == 'Success') {
                        window.location.reload();
                    }
                },
            });
        }
    });

};
function AuditGroupSelection() {

    $('#listOfStaff').empty().append('<option value="0" selected="selected"></option>');
    $("#modalAssignAssignBtn").attr("disabled", "disabled");

    $.ajax({
        url: '/System/Common/GetUsersInAuditGroup',
        method: 'GET',
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, data) {
                $('#listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')
            });
        },
        failure: function (data) {

        },
    });
}
function SupportingDocuments() {
    $("div #panelSupportingDocuments [type=checkbox]").each(function () {
        $(this).attr('name', $(this).attr('id'));
    });
};

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
};

function InitAutoSave() {
    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        clearField(this);
        $('#ApplicationHaulerID :input').eq(0).trigger('change');
    });

    //OTSTM2-50/642 adding for new added contact autosave
   // $(document).on("change", ".ctemp :input", function (evt) {
    $(document).on("change", "#ApplicationHaulerID :input:not(#inactivedropdown):not(#activedropdown)", function (evt) {

        var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';

        evt.stopImmediatePropagation(); //avoid being fired twice

        //ET-79
        var apphauler = $(this);
        if (apphauler.is(':checkbox')) {
            clearField(apphauler);
        }

        //OTSTM2-50/642 save textbox, checkbox or select option value to the template, keep value for removing
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            if (ctemp[0].name != "TireDetails.TireItem") {
                var value = ctemp.is(':checked');
            }
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;
            }
            if (ctemp[0].name != "TireDetails.TireItem") {
                this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
                this.defaultChecked = value;
            }
        }
        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country")>=0) {
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }
        
        $.when(
        ctemp.focusout()).then(function () {
            
            certificateOfApproval(); //OTSTM2-642 added for fixing existing issue (capacity>50, Certificate of Approval # not appears)
            switch (Global.StaffIndex.Settings.status) {
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
                case Global.StaffIndex.Settings.ApplicationStatus.Approved:
                case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                    break;
                default:
                    if (!IsHaulerAutoSaveDisabled) {
                        processJson();
                    }
                    else {
                        $('#modalSaveError').modal('toggle')
                    }
                    break;

            }
            contactAddressSameAs();
            totalStorage();  //update Maximum Capacity of Sort Yards
        });
    });

    //$('#ApplicationHaulerID :input').on('change', function () {
    $(document).on("change", "#ApplicationHaulerID :input:not(#inactivedropdown):not(#activedropdown)", function (evt) { //OTSTM2-50/642 change the "Change" event

        evt.stopImmediatePropagation(); //OTSTM2-50 avoid firing twice (bubble event)

        var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';

        var id = this.id;
        var apphauler = $(this);
        if (apphauler.is(':checkbox')) {
            clearField(apphauler);
        }
        
        //OTSTM2-50/642 save textbox, checkbox or select option value to the template, keep value for removing
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            var value = ctemp.is(':checked');
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;                
            }
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
            this.defaultChecked = value;
        }
        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country") >= 0) {
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }

        $.when(
        apphauler.focusout()).then(function () {

            certificateOfApproval();
            switch (Global.StaffIndex.Settings.status) {
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
                case Global.StaffIndex.Settings.ApplicationStatus.Approved:
                case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                    break;
                default:
                    if (!IsHaulerAutoSaveDisabled) {
                        processJson();
                    }
                    else {
                        $('#modalSaveError').modal('toggle')
                    }
                    break;
            }
            totalStorage();
            contactAddressSameAs();
        });
    });

    //OTSTM2-50/642 comment out
    //$('#ApplicationHaulerID').on('change', '.ctemp', function () {

    //    var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';

    //    var id = this.id;
    //    var ctemp = $(this);
    //    $.when(
    //    ctemp.focusout()).then(function () {

    //        //fix
    //        certificateOfApproval();
    //        switch (Global.StaffIndex.Settings.status) {
    //            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
    //            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
    //            case Global.StaffIndex.Settings.ApplicationStatus.Approved:
    //            case Global.StaffIndex.Settings.ApplicationStatus.Completed:
    //                break;
    //            default:
    //                if (!IsHaulerAutoSaveDisabled) {
    //                    processJson();
    //                }
    //                else {
    //                    $('#modalSaveError').modal('toggle')
    //                }
    //                break;
    //        }
    //        totalStorage();
    //        contactAddressSameAs();
    //    });
    //});
   
}
var processJson = function () {
    if ($('#status').val() != 'Approved') {
        var form = $('form');
        //enable disabled controls which have classes and reenable them after
        var tmpEnabled = form.find('input.sort-yard-address-disabled,select.sort-yard-address-disabled').removeAttr('disabled');
        var disabled = form.find(':input:disabled').removeAttr('disabled');

        var data = form.serializeArray();
        tmpEnabled = tmpEnabled.attr('disabled', 'disabled');
        disabled.attr('disabled', 'disabled');

        data = data.concat(
            $('#ApplicationHaulerID input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": this.value }
                    }).get());

        data = data.concat(
            $('#ApplicationHaulerID input[type=radio]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": null }
                    }).get());

        $('input[type="checkbox"]').each(function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }

                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }

        //remove leading and trailing space
        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }

        var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';
        if (!IsHaulerAutoSaveDisabled) {
            $.ajax({
                url: '/Hauler/Registration/SaveModel',
                dataType: 'json',
                type: 'POST',
                data: $.param(data),
                success: function (result) {
                    if (result) {
                        switch (result.toLowerCase()) {
                            case "success":
                                break;
                                //default:
                                //    window.location = 'http://' + window.location.hostname + '/Hauler/Registration/Message?msg=' + result;
                                //    break;
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            $('#modalSaveError').modal('toggle')
        }
    }
}
var certificateOfApproval = function () {

    var allSortYards = 0;
    var capp = $("input[data-maxstorage]");
    capp.each(function () {
        $(this).attr('data-maxstorage', $(this).val());

        if (parseFloat($(this).attr('data-maxstorage')) > 0) {
            allSortYards++;
        }

        var maxStorage = parseFloat($(this).attr('data-maxstorage'));
        
        //OTSTM2-642 fix the existing issue that capacity>50, Certificate of Approval # appear in all sort yards
        //remove .parents('.row') because it return multiple, add parent().closest('.row') because it return single
        if (!maxStorage) { //if capacity is empty, clear Certificate of Approval # and hide it
            var certApp = $(this).parent().closest('.row').find('.COA'); 
            $(certApp).find('input:text').each(function () {
                $(this).val('');
             });
            $(this).parent().closest('.row').find('.COA').hide(); 
        }
        else if (maxStorage < 50) { //if capacity < 50, clear Certificate of Approval # and hide it
            var certApp = $(this).parent().closest('.row').find('.COA'); 
            $(certApp).find('input:text').each(function () {
                $(this).val('');
            });
            $(this).parent().closest('.row').find('.COA').hide(); 
        }
        else { //if capacity > 50, show Certificate of Approval #
            $(this).parent().closest('.row').find('.COA').show(); 
        }
    });
    $('#SortYardCount').text(allSortYards);
}
var totalStorage = function () {
    var maxStorageCap = 0;
    var inputs = $("input[data-maxstorage]");
    inputs.each(function () {
        //if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
        //    maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        //}
        //OTSTM2-642 comment out above and apply the followed code
        if (!isNaN(parseFloat($(this).val()))) {
                maxStorageCap += parseFloat($(this).val());
        }
    });
    if (!isNaN(parseFloat(maxStorageCap))) {
        $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
    }
}
var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        //   CopyContactAddress();
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
    });
}

var handleBusinessAddressCheckboxForContact = function () {

    $('.ContactAddressSameAsBusinessAddress').each(function () {
        var isChecked = $(this).is(':checked');
        $(this).next('input[type="hidden"]').val(isChecked);
    });

}

/* COMMON FUNCTIONS */
function _isEmpty(value) {
    return (value == null || value.length === 0);
}
var restrictOnlyNumbers = function () {
    $(".only-numbers").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    });
}
function DisplayCVORDocumentOption() {
    var show = ($('.HaulerDetails_HIsGvwr:checked').val() == 'True');
    $('#cVORDocument').toggle(show);
}
function DisplayWSIBDocumentOption() {
    var show = ($('.HaulerDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
}
function DisplayHSTDocumentOption() {
    var show = !$('#HaulerDetails_IsTaxExempt').is(':checked');
    $('#hSTDocument').toggle(show);
}
function DisplayProcessorLetterTireDetails() {
    var show = ($('.HHasRelatioshipWithProcessor:checked').val() == 'True');
    $('#processorRelationshipLetter').toggle(show);
}

function titleCase() {
    $('.title-case').on('keypress', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}

var clearField = function (element) {
    if (!$(element).is(':checked')) {
        var nameAttr = $(element).attr('data-att-chkbox');
        if (nameAttr) {
            if ($('input[name="' + nameAttr + '"]').length > 0) {
                var control = $('input[name="' + nameAttr + '"]');

                if (control.attr('type') == 'radio') {
                    control.prop('checked', false);
                }
                else if (control.attr('type') == 'checkbox') {
                    control.prop('checked', false);
                }
                else {
                    control.val('');
                }
            }
            else if ($('select[name="' + nameAttr + '"]').length > 0) {
                $('select[name="' + nameAttr + '"]').find('option').removeAttr('selected');
            }
        }
    }
}

var renameSortYardLabel = function () {
    var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
    for (var j = 0; j < label.length; j++) {
        var num = 1;
        $(label[j]).each(function () {
            var newLabel = $(this).text().replace(/\d+/, num++);
            $(this).text(newLabel);
        });
    }
}

/* END COMMON FUNCTIONS */

/* WORKFLOW CODE */
$(function () {
    $(window).load(function () {
        $(document).ready(function () {
            $('#modalDeny').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApplicant').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOnhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOffhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalAssign').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApproveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalResend').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalSaveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalCancelApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            //ActiveInactive

            $('#switchInactiveToActive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#switchActiveToInactive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });
        });
    });

    $('#modalSaveDBYesBtn').on('click', function () {

        handleBusinessAddressCheckboxForContact();

        var form = $('form');
        var disabled = form.find(':input:disabled').removeAttr('disabled'); //for saving when all fields are disabled

        var data = form.serializeArray();
        disabled.attr('disabled', 'disabled');

        data = data.concat(
            $('#ApplicationHaulerID input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": this.value }
                    }).get());

        data = data.concat(
            $('#ApplicationHaulerID input[type=radio]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": null }
                    }).get());

        $('input[type="checkbox"]').each(function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }

                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }
        if ($('#status').val() != 'Approved') {
            data.push({ name: 'BankingInformation.BankName', value: $('#BankName').val() });
            data.push({ name: 'BankingInformation.BankNumber', value: $('#BankNumber').val() });
            data.push({ name: 'BankingInformation.AccountNumber', value: $('#AccountNumber').val() });
            data.push({ name: 'BankingInformation.TransitNumber', value: $('#TransitNumber').val() });
        }

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        $.ajax({
            url: '/Hauler/Registration/SaveModelDB',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (data) {
                if (data.isValid) {
                    window.location.reload(true);
                }
                else {
                    $('#save-error-text').html(data);
                    $('#modalSaveError').modal('toggle');
                }
            },
            failure: function (data) {
            }
        });
    });

    var checkReviewCheckBoxForApproveAndDenyList = function () {
        //check if any same as mailing address for business location
        if ($('input.isMailingAddressDifferent').is(':checked')) {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //check if any same as mailing address for contact information
        if ($('input.isContactAddressSameAsBusinessAddress').is(':checked')) {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for each sort yard details COA Capacity tonnes whether certificate of approval appears
        $('.COA input[type=checkbox]').each(function () {
            if ($(this).closest('.COA').css('display') == "none") {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            } else {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            }
        });

        //for hauler details GVWR
        //if display is none and add that attribute
        if ($('#pnlCVOR').css('display') == "none") {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for hauler details WSIB
        //if display is none and add that attribute
        if ($('#pnlWSIB').css('display') == "none") {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }
    }

    var GetValidationErrorMessages = function () {
        var validationErrorMessages = [];
        checkReviewCheckBoxForApproveAndDenyList();
        var validationCheckBoxes = $('INPUT.validation-checkbox:checkbox:not(:checked)');
        $.each(validationCheckBoxes, function () {
            var validationMessage = $(this).data('validate-message');
            validationErrorMessages.push(validationMessage);
        });
        var additionalReasons = $('#modelDenyAdditionalReasonInput').val();
        if (additionalReasons.length != '' && additionalReasons.length > 0) {
            validationErrorMessages.push(additionalReasons);
        }

        var uploadedFilesValidationMessage = $('.upload-file-selection');
        //console.log(uploadedFilesValidationMessage);
        $.each(uploadedFilesValidationMessage, function () {
            var validationMessage = $(this).attr('data-uploadedFile-validation-message');
            if (validationMessage != undefined && validationMessage != '') {
                validationErrorMessages.push(validationMessage);
            }
        });

        return validationErrorMessages;
    }

    var reasonList;

    $('#modalDeny').on('shown.bs.modal', function () {
        $('#denyReasonsList').empty();
        $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
        reasonList = GetValidationErrorMessages();
        $.each(reasonList, function (index) {
            $('#denyReasonsList').append('<li>' + reasonList[index] + '</li>');
        });
        if (reasonList.length > 0) {
            $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
        } else {
            var comment = $.trim($('#modelDenyAdditionalReasonInput').val());
            if (comment.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            };
        };
    });

    $('#modelDenyAdditionalReasonInput').on('change', function () {
        if (reasonList.length <= 0) {
            if (this.value.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            } else {
                $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
            };
        };
    });

    $('#listOfStaff').on('change', function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#modalAssignAssignBtn").removeAttr("disabled");
            $("#modalAssignAssignBtn").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#modalAssignAssignBtn").attr("disabled", "disabled");
        }
    });

    $('.workflow').on('click', function () {
        var status = $(this).attr('data-status');
        var userID = 0;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }
        if (status != "") {
            if (status.toLowerCase() == 'backtoapplicant') {
                //as per OTSTM-864 clear all fields that have their checkbox unchecked

                $('input[type="checkbox"]:not(:checked)').each(function () {
                    clearField(this);
                });

                $('#hdnExplicitSave').trigger('click');//saves data
            }
            //let model save first then change status
            setTimeout(function () {
                $.ajax({
                    url: '/Hauler/Registration/ChangeStatus',
                    method: 'POST',
                    dataType: "JSON",
                    data: { applicationId: $('#applicationId').val(), status: status, userID: userID },
                    success: function (data) {
                        window.location.reload(true);
                    },
                });
            }, 1000);
        }
    });

    $('#modalApproveApproveBtn').on('click', function () {
        $.ajax({
            url: '/Hauler/Registration/ApproveApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val() },
            success: function (data) {
                if (data.isValid) {
                    var url = Global.StaffIndex.Settings.afterApproveRedirectUrl + '?vId=' + data.vendorID
                    window.location = url;
                }
            },
            failure: function (data) {
            }
        });
    });

    $('#approveBtn').on('click', function () {
        //check if any fields are checked
        $('#approveBtn').removeAttr("data-target");

        checkReviewCheckBoxForApproveAndDenyList();
        var isAllValidationChecksCompleted = true;

        $('input:checkbox.validation-checkbox.no-validate').each(function () {
            if (!this.checked && !this.hasAttribute('approve-validation')) {
                isAllValidationChecksCompleted = false;
            }
        });

        if (isAllValidationChecksCompleted && $('#fmBusinessLocation').valid() && $('#fmContactInfo').valid() && $('#fmSortYardDetails').valid() && $('#fmHaulerDetails').valid() && $('#fmTermsAndConditions').valid() && $('#fmTireDetails').valid()) {
            $(this).attr("data-target", "#modalApproveApplication");
        }
        else {
            // alert('Validation error. One or more fields are not valid.');
            $('#modalApproveValidateWarning').modal('show');
        }
    });

    $('#saveBtn').on('click', function () {

        //check if any fields are checked
        $('#saveBtn').removeAttr("data-target");


        var IsHaulerAutoSaveDisabled = Global.StaffIndex.Security.IsHaulerAutoSaveDisabled.toLowerCase() == 'true';
        if (!IsHaulerAutoSaveDisabled) {
            if ($('#fmBusinessLocation').valid() &&
                $('#fmContactInfo').valid() &&
                $('#fmSortYardDetails').valid() &&
                $('#fmHaulerDetails').valid() &&
                $('#fmTermsAndConditions').valid() &&
                $('#fmTireDetails').valid()) {
                $(this).attr("data-target", "#modalSaveApplication");
            }
            else {
                // alert('Validation error. One or more fields are not valid.');
                $('#modalApproveValidateWarning').modal('show');
            }
        }
        else {
            $('#modalSaveError').modal('toggle')
        }
    });

    $('#modalDenyDenyApplicationBtn').on('click', function () {
        $.ajax({
            url: '/Hauler/Registration/DenyApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val(), validationErrorMessages: GetValidationErrorMessages() },
            failure: function (data) {
            },
        });
    });

    $('#modalResendYesBtn').on('click', function () {

        //added
        switch (Global.StaffIndex.Settings.status) {
            case Global.StaffIndex.Settings.ApplicationStatus.Approved:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
            case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                var url = '/Hauler/Registration/ResendApproveRegistrant'
                var data = { vendorId: Global.StaffIndex.Settings.vendorID };

                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: "JSON",
                    data: data,
                    failure: function (data) {
                        if (data.isValid) {
                            window.location.reload(true);
                        }
                    },
                    success: function (data) {
                        if (data.isValid) {
                            window.location.reload(true);
                        }
                    },
                });

                break;
            default:
                break;
        }
    });

    //comment out for OTSTM2-279
    //$('#showApprovedItem').show();
    //$('#hideApprovedItem').hide();
});
/* END WORKFLOW CODE */

/* GABE STYLES */

(function () {
    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
      businessLocationLBNSettings = {
          content: businessLocationLBNContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
      businessLocationBONSettings = {
          content: businessLocationBONContent,
          width: 300,
          height: 100,
      };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationBONSettings));

    // Sort Yard Details: All Sort Yards Capacity
    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
      sortYardDetailsASYCSettings = {
          content: sortYardDetailsASYCContent,
          width: 300,
          height: 100,
      };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsASYCSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
      tiredetailsPopoverPLTSettings = {
          content: tiredetailsPopoverPLTContent,
          width: 500,
          height: 210,
      };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
      tiredetailsPopoverMTSettings = {
          content: tiredetailsPopoverMTContent,
          width: 380,
          height: 160,
      };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverAGLSSettings));
    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
      tiredetailsPopoverINDSettings = {
          content: tiredetailsPopoverINDContent,
          width: 360,
          height: 120,
      };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverINDSettings));

    // Tire Details: Small Off the Road Tire (SOTR)
    var tiredetailsPopoverSOTRContent = $('#tiredetailsPopoverSOTR').html(),
      tiredetailsPopoverSOTRSettings = {
          content: tiredetailsPopoverSOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeSOTR = $('.tiredetailsPopoverSOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverSOTRSettings));

    // Tire Details: Medium Off the Road Tire (MOTR)
    var tiredetailsPopoverMOTRContent = $('#tiredetailsPopoverMOTR').html(),
      tiredetailsPopoverMOTRSettings = {
          content: tiredetailsPopoverMOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeMOTR = $('.tiredetailsPopoverMOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMOTRSettings));

    // Tire Details: Large Off the Road Tire (LOTR)
    var tiredetailsPopoverLOTRContent = $('#tiredetailsPopoverLOTR').html(),
      tiredetailsPopoverLOTRSettings = {
          content: tiredetailsPopoverLOTRContent,
          width: 420,
          height: 120,
      };
    var popLargeLOTR = $('.tiredetailsPopoverLOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverLOTRSettings));

    // Tire Details: Giant Off the Road Tire (GOTR)
    var tiredetailsPopoverGOTRContent = $('#tiredetailsPopoverGOTR').html(),
      tiredetailsPopoverGOTRSettings = {
          content: tiredetailsPopoverGOTRContent,
          width: 420,
          height: 100,
      };
    var popLargeGOTR = $('.tiredetailsPopoverGOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverGOTRSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBL').html(),
        supportingDocumentsAIMBLSettings = {
            content: supportingDocumentsAIMBLContent,
            width: 500,
            height: 140,
        };
    var popLargeAIMBL = $('.supportingDocumentsAIMBL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLI').html(),
        supportingDocumentsCLISettings = {
            content: supportingDocumentsCLIContent,
            width: 500,
            height: 260,
        };
    var popLargeCLI = $('.supportingDocumentsCLI').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCLISettings));

    // Supporitng Documents: CVOR Abstract Level II
    var supportingDocumentsCVORContent = $('#supportingDocumentsCVOR').html(),
        supportingDocumentsCVORSettings = {
            content: supportingDocumentsCVORContent,
            width: 460,
            height: 170,
        };
    var popLargeCVOR = $('.supportingDocumentsCVOR').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCVORSettings));

    // Supporitng Documents: Processor Relationship Letter
    var supportingDocumentsPRLContent = $('#supportingDocumentsPRL').html(),
        supportingDocumentsPRLSettings = {
            content: supportingDocumentsPRLContent,
            width: 460,
            height: 105,
        };
    var popLargePRL = $('.supportingDocumentsPRL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsPRLSettings));

    // Supporitng Documents: supportingDocumentsHST
    var supportingDocumentsHSTContent = $('#supportingDocumentsHST').html(),
    supportingDocumentsHSTSettings = {
        content: supportingDocumentsHSTContent,
        width: 500,
        height: 160,
    };
    var popLargePRL = $('.supportingDocumentsHST').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsHSTSettings));

    // Supporitng Documents: supportingDocumentWSIB
    var supportingDocumentWSIBContent = $('#supportingDocumentWSIB').html(),
        supportingDocumentWSIBSettings = {
            content: supportingDocumentWSIBContent,
            width: 500,
            height: 86,
        };
    var popLargeVOCH = $('.supportingDocumentWSIB').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentWSIBSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCH').html(),
        supportingDocumentsVOCHSettings = {
            content: supportingDocumentsVOCHContent,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.supportingDocumentsVOCH').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsVOCHSettings));

    // Popover: Participant Information
    var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
        popoverParticipantInfoSettings = {
            content: popoverParticipantInfoContent,
            width: 270,
        };
    var popLargeLBN = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));
})();

//OTSTM2-642 add "required" validation for unregistered application (only for existing sort yard), at the mean time, leave no validation for registered application as existing (0 can be added by system automatically for empty input after saving)
$(document).ready(function () {
    $(window).load(function () {
        if (!Global.StaffIndex.Settings.IsApprovedApplication) { //for unregistered application
            $(".maxStorageCapacity").addClass("dynamic-required");
        }
    });
});
