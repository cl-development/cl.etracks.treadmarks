﻿'use strict';
haulerClaimApp.controller("haulerClaimSummaryController", ["$scope", "$filter", "haulerClaimService", '$uibModal', 'claimService', function ($scope, $filter, haulerClaimService, $uibModal, claimService) {

    $scope.submitBtn = false;
    haulerClaimService.getClaimDetails().then(function (data) {
        //populating claim details
        $scope.HaulerClaimSummary = data;

        $scope.ItemSubmission = data.InventoryAdjustment.ItemSubmission;

        $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
        $scope.disableAddTransAdjBtn = ((data.ClaimCommonModel.StatusString == "Approved") || $scope.PageIsReadonly);

        $scope.submitInfo = {
            periodShortName: data.ClaimCommonModel.ClaimPeriodName,
            registrationNum: data.ClaimCommonModel.RegistrationNumber
        };
        $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission = haulerClaimService.getValueFromKeyVal($scope.ItemSubmission);
        $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
        $scope.claimDetailsLoaded = true;
    });

    haulerClaimService.getClaimPayment().then(function (data) {
        $scope.HaulerPayment = data;
    });

    $scope.yardCountSubmission = function () {
        $scope.yardCountMaster = angular.copy($scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission);
    };

    $scope.AddYardCountSubmission = function () {
        angular.copy($scope.yardCountMaster, $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission);
        haulerClaimService.addYardCountSubmission($scope.yardCountMaster);
    };

    $scope.InternalAdjustTypes = [
       {
           name: 'Weight',
           id: '1'
       },
       {
           name: 'Tire Counts',
           id: '2'
       },
       {
           name: 'Yard Count',
           id: '3'
       },
       {
           name: 'Payment',
           id: '4'
       }
    ];
    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        var viewItem = $filter('filter')($scope.InternalAdjustTypes, { name: internalAdjustType })[0];
        claimService.getInternalAdjustment(viewItem.id, internalAdjustId).then(function (data) {
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'PEligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    itemSubmission: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                    },
                    tireCountBalance: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                    },
                    tireCountBalanceWithoutAdj: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                    },
                    //tireCountBeforeAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                    //},
                    //tireCountAfterAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                    //},
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (modalResult) {
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'PEligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

}]);

haulerClaimApp.controller('ErrorModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'errorMessage', function ($rootScope, $scope, $http, $uibModalInstance, errorMessage) {
    $scope.errorMessage = errorMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

haulerClaimApp.controller('WarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

haulerClaimApp.controller('ModalSubmitOneModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'haulerClaimService', function ($rootScope, $scope, $http, $uibModalInstance, $window, haulerClaimService) {

    $scope.finalSubmitClaim = function () {
        haulerClaimService.FinalSubmitClaim().then(function (data) {
            if (data.status == "Valid Data") {
                $scope.submitBtn = false;
                $uibModalInstance.close($scope.submitBtn);
                //$window.location.reload();
            }
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

haulerClaimApp.controller('ModalSubmitTwoModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'submitInfo', '$window', function ($rootScope, $scope, $http, $uibModalInstance, submitInfo, $window) {

    $scope.submitInfo = submitInfo;

    $scope.confirm = function () {
        $uibModalInstance.close();
        $window.location.reload();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

haulerClaimApp.controller('PEligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'itemSubmission', 'tireCountBalance', 'tireCountBalanceWithoutAdj', 'selectedItem', 'chooseTypeIsDisable', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, itemSubmission, tireCountBalance, tireCountBalanceWithoutAdj, selectedItem, chooseTypeIsDisable, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.itemSubmission = itemSubmission;
    $scope.tireCountBalance = tireCountBalance;
    $scope.tireCountBalanceWithoutAdj = tireCountBalanceWithoutAdj;    
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.confirm = function () {
        $scope.modalResult.selectedItem = $scope.selectedItem;
        $uibModalInstance.close($scope.modalResult);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

haulerClaimApp.controller('PEligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);



