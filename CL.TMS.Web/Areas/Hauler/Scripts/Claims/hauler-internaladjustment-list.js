﻿$(function () {
    var pageSize = 7;
    //Define data table
    var tableId = '#tblHaulerInternalAdjusList';
    var table = $(tableId).DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.ParticipantInternalAdjustmentSummary.InternalAdjustmentHandleUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY: 300,
        scrollCollapse: false,
        searching: true,
        ordering: true,
        order: [[0, "desc"]],

        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },
        createdRow: function (row, data, index) {
            $(row).addClass('cursor-pointer');
            var internalAdjustmentId = data.InternalAdjustmentId;
            var adjustmentType = data.AdjustmentType;
            $(row).on('click', function () {
                angular.element(document.getElementById('haulerClaimSummary')).scope().viewInternalAdjust(internalAdjustmentId, adjustmentType);
            });
        },
        columns: [
                 {
                     name: "AdjustmentDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentDate.toString().substring(0, 10) + "</span>";
                     }
                 },
                 {
                     name: "AdjustmentType",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentType + "</span>";
                     }
                 },
                 {
                     name: "AdjustmentBy",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentBy + "</span>";
                     }
                 },
                 {
                     name: "InternalAdjustmentId",
                     data: null,
                     className: 'display-none',
                     render: function (data, type, full, meta) {
                         return "<span>" + data.InternalAdjustmentId + "</span>";
                     }
                 }
        ],
        initComplete: function (settings, json) {
            var scrollBody = $(tableId).parent('.dataTables_scrollBody')
            $(scrollBody).css({ 'overflow-y': 'hidden' });
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#internalAdjustFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= pageSize) ? $('#tblHaulerInternalAdjusList_viewmore').css('visibility', 'visible') : $('#tblHaulerInternalAdjusList_viewmore').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#internalAdjustSearch').val();
            var url = Global.ParticipantInternalAdjustmentSummary.InternalAdjustmentExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#exportInternalAdj').attr('href', url);
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    $('#tblHaulerInternalAdjusList_viewmore').on('click', function () {
        var scrollBody = $(tableId).parent('.dataTables_scrollBody')
        $(scrollBody).css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('#internalAdjustSearch').on('keyup', function () {
        Global.noLoadSpinner = true;//stop load spinner
        table.search(this.value).draw(false);
    });

    $('#internalAdjustSearchBtn').on('click', function () {
        var searchValue = $('#internalAdjustSearch').val();
        table.search(searchValue).draw(false);
    });

    $('#internalAdjustRemove').on('click', function () {
        table.search("").draw(false);
        $('#internalAdjustFound').hide();
    });

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    };
});