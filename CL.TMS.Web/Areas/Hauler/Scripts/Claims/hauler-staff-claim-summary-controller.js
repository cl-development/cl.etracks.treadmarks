﻿'use strict';
haulerClaimApp.controller("haulerStaffClaimSummaryController", ["$scope", "$filter", "haulerClaimService", '$uibModal', '$interval', 'claimService', function ($scope, $filter, haulerClaimService, $uibModal, $interval, claimService) {
    haulerClaimService.getClaimDetails().then(function (data) {
        $scope.HaulerClaimSummary = data;
        
        $scope.ClaimCommonModel = data.ClaimCommonModel;
        $scope.ItemSubmission = data.InventoryAdjustment.ItemSubmission;
        $scope.TireCountBalance = data.InventoryAdjustment.TireCountBalance;
        $scope.TireCountBalanceWithoutAdj = data.InventoryAdjustment.TireCountBalanceWithoutAdj;
        //$scope.TireCountBeforeAdj = data.InventoryAdjustment.TireCountBeforeAdj;
        //$scope.TireCountAfterAdj = data.InventoryAdjustment.TireCountAfterAdj;

        $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission = haulerClaimService.getValueFromKeyVal($scope.ItemSubmission);
        $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance = haulerClaimService.getValueFromKeyVal($scope.TireCountBalance);
        $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountBalanceWithoutAdj);
        //$scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountBeforeAdj);
        //$scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountAfterAdj);
        $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;

        $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;
        
        $scope.claimDetailsLoaded = true;
        if ((data.ClaimStatus.StatusString == "Approved") || (data.ClaimStatus.StatusString == "Receive Payment")) {
            $scope.ShowMailDate = true;
        }
        else {
            $scope.ShowMailDate = false;
        }
        if (data.ClaimStatus.StatusString == "Approved") {
            $scope.DisableInternalAdjustmentsBtn = true;
        }
        else {
            $scope.DisableInternalAdjustmentsBtn = Global.Settings.Permission.DisableInternalAdjustmentsBtn;
        }
        $scope.getStatusColor = setStatusColor(data.ClaimStatus.StatusString);
    });

    haulerClaimService.getClaimPayment().then(function (data) {
        $scope.HaulerPayment = data;
        $scope.$watch("HaulerPayment.MailedDate", function () {
            if ($scope.HaulerPayment.MailedDate != null) {
                claimService.updateClaimMailDate($scope.HaulerPayment.MailedDate);
            }
        });
        console.log('$scope.HaulerPayment.GrandTotal:', $scope.HaulerPayment.GrandTotal);
    });

    var refreshPanels = function () {
        var stop;

        //call hauler claim service to refresh panel
        haulerClaimService.getClaimDetails().then(function (data) {
            $scope.HaulerClaimSummary = data;
            $scope.ClaimCommonModel = data.ClaimCommonModel;
            $scope.ItemSubmission = data.InventoryAdjustment.ItemSubmission;
            $scope.TireCountBalance = data.InventoryAdjustment.TireCountBalance;
            $scope.TireCountBalanceWithoutAdj = data.InventoryAdjustment.TireCountBalanceWithoutAdj;            
            //$scope.TireCountBeforeAdj = data.InventoryAdjustment.TireCountBeforeAdj;
            //$scope.TireCountAfterAdj = data.InventoryAdjustment.TireCountAfterAdj;
            $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission = haulerClaimService.getValueFromKeyVal($scope.ItemSubmission);
            $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance = haulerClaimService.getValueFromKeyVal($scope.TireCountBalance);
            $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountBalanceWithoutAdj);
            //$scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountBeforeAdj);
            //$scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj = haulerClaimService.getValueFromKeyVal($scope.TireCountAfterAdj);


            $scope.claimDetailsLoaded = true;

            //refresh internal adjustment datatable
            $('#internalAdjustSearchBtn').click();
        });
        haulerClaimService.getClaimPayment().then(function (data) {
            $scope.HaulerPayment = data;
            
            claimService.getClaimWorkflowViewModel($scope.HaulerPayment.GrandTotal).then(function (data) {//update workflow UI
                var localScope = angular.element(document.getElementById("claimWorkflowBar")).scope();
                if (localScope) {
                    localScope.claimWorkflowModel = data;//typeof data: ClaimWorkflowViewModel
                }
            });
        });
    }

    $scope.WeightChooseTypes = [
        {
            name: 'Weight',
            id: '1'
        },
        {
            name: 'Tire Counts',
            id: '2'
        },
        {
            name: 'Yard Count',
            id: '3'
        },
    ];
    var initializeModalResult = function () {
        return {
            selectedItem: null,
            direction: 'overall',
            eligibility: 'eligible',
            onroad: 0,
            offroad: 0,
            plt: 0,
            mt: 0,
            agls: 0,
            ind: 0,
            sotr: 0,
            motr: 0,
            lotr: 0,
            gotr: 0,


            pltBeforeAdj: 0,
            mtBeforeAdj: 0,
            aglsBeforeAdj: 0,
            indBeforeAdj: 0,
            sotrBeforeAdj: 0,
            motrBeforeAdj: 0,
            lotrBeforeAdj: 0,
            gotrBeforeAdj: 0,

            pltAfterAdj: 0,
            mtAfterAdj: 0,
            aglsAfterAdj: 0,
            indAfterAdj: 0,
            sotrAfterAdj: 0,
            motrAfterAdj: 0,
            lotrAfterAdj: 0,
            gotrAfterAdj: 0,

            
            paymentType: 'Overall',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,
            unitType: 2,
            //OTSTM2-270
            internalNote: null
        };
    }

    //Eligible/Ineligible Adjustments Modal
    $scope.eligibleAdjustments = function (initialEligibility) {
        var modalResult = initializeModalResult();
        modalResult.eligibility = initialEligibility;
        var eligibleAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'EligibleAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.WeightChooseTypes;
                },
                modalResult: function () {
                    return modalResult;
                },
                itemSubmission: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                },
                tireCountBalance: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                },
                tireCountBalanceWithoutAdj: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                },                
                //tireCountBeforeAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                //},
                //tireCountAfterAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                //},
                selectedItem: function () {
                    return $scope.WeightChooseTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return null;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        eligibleAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'EligibleAdjustmentConfirmCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {
                        refreshPanels();
                    }
                });
            });
        });
    }

    //Yard Count Adjustment Modal when user presses Validate from SUmmary page
    $scope.yardCountAdjustments = function (initialEligibility) {
        var modalResult = initializeModalResult();
        modalResult.eligibility = initialEligibility;
        var eligibleAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'EligibleAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.WeightChooseTypes;
                },
                modalResult: function () {
                    modalResult.pltBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT;
                    modalResult.mtBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MT;
                    modalResult.aglsBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS;
                    modalResult.indBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.IND;
                    modalResult.sotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR;
                    modalResult.motrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR;
                    modalResult.lotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR;
                    modalResult.gotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR;

                    (modalResult.isAdd)
                    {
                        modalResult.pltAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT;
                        modalResult.mtAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MT;
                        modalResult.aglsAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS;
                        modalResult.indAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.IND;
                        modalResult.sotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR;
                        modalResult.motrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR;
                        modalResult.lotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR;
                        modalResult.gotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR;
                    }
                    return modalResult;
                },
                itemSubmission: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                },
                tireCountBalance: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                },
                tireCountBalanceWithoutAdj: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                },
                //tireCountBeforeAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                //},
                //tireCountAfterAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                //},
                selectedItem: function () {
                    return $scope.WeightChooseTypes[2];
                },
                chooseTypeIsDisable: function () {
                    return true;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        eligibleAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'EligibleAdjustmentConfirmCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {
                        refreshPanels();
                    }
                });
            });
        });
    }

    $scope.InternalAdjustTypes = [
       {
           name: 'Weight',
           id: '1'
       },
       {
           name: 'Tire Counts',
           id: '2'
       },
       {
           name: 'Yard Count',
           id: '3'
       },
       {
           name: 'Payment',
           id: '4'
       }
    ];

    //Internal adjustment modal
    $scope.internalAdjustments = function (initialEligibility) {
        var modalResult = initializeModalResult();
        modalResult.eligibility = initialEligibility;
        var eligibleAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'EligibleAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.InternalAdjustTypes;
                },
                modalResult: function () {
                    modalResult.pltBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT;
                    modalResult.mtBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MT;
                    modalResult.aglsBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS;
                    modalResult.indBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.IND;
                    modalResult.sotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR;
                    modalResult.motrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR;
                    modalResult.lotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR;
                    modalResult.gotrBeforeAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR;

                    (modalResult.isAdd)
                    {
                        modalResult.pltAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT;
                        modalResult.mtAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MT;
                        modalResult.aglsAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS;
                        modalResult.indAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.IND;
                        modalResult.sotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR;
                        modalResult.motrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR;
                        modalResult.lotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR;
                        modalResult.gotrAfterAdj = $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR;
                    }

                    return modalResult;
                },
                itemSubmission: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                },
                tireCountBalance: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                },
                tireCountBalanceWithoutAdj: function () {
                    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                },
                //tireCountBeforeAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                //},
                //tireCountAfterAdj: function () {
                //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                //},
                selectedItem: function () {
                    return $scope.InternalAdjustTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        
        eligibleAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'EligibleAdjustmentConfirmCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {
                        refreshPanels();
                    }
                });
            });
        });
    }

    //Remove internal adjustment
    $scope.removeInternalAdjust = function (data) {
        var internalAdjustmentId = data.InternalAdjustmentId;
        var internalAdjustmentType = data.InternalAdjustmentType;
        claimService.removeInventoryAdjustment(internalAdjustmentType, internalAdjustmentId).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    }

    

    //Edit internal adjustment
    $scope.editInternalAdjust = function (internalAdjustId, internalAdjustType) {

        //$scope.counter = 0;
        //$scope.change_yardcount_plt = function() {
        //    $scope.counter++;
        //};

        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    itemSubmission: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                    },
                    tireCountBalance: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                    },
                    tireCountBalanceWithoutAdj: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                    },
                    //tireCountBeforeAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                    //},
                    //tireCountAfterAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                    //},
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return false;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }                    
                }
            });

            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        modalResult: function () {                            
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });                
            });

        });
    }

    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var viewItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];

            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    itemSubmission: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.ItemSubmission;
                    },
                    tireCountBalance: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalance;
                    },
                    tireCountBalanceWithoutAdj: function () {
                        return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj;
                    },
                    //tireCountBeforeAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountBeforeAdj;
                    //},
                    //tireCountAfterAdj: function () {
                    //    return $scope.HaulerClaimSummary.InventoryAdjustment.TireCountAfterAdj;
                    //},
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    },
                    //OTSTM2-270
                    internalAdjustmentId: function () {
                        return internalAdjustId;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    refreshPanels();
                    return
                }               

                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

    //Mail Date Picker
    $scope.datePicker = {
        status: false
    };

    $scope.openDatePicker = function ($event) {
        $scope.datePicker.status = true;
    };


    //InternalNotesSettings for ClaimInternalNotes
    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalNotesUrl;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalNotesUrl;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalNotes;
    var setStatusColor = function (status) {
        switch (status) {
            case "Submitted":
                return 'color-tm-orange-bg';
                break;
            case "Open":
                return 'color-tm-blue-bg';
                break;
            case "Under Review":
                return 'color-tm-yellow-bg';
                break;
            case "Approved":
                return 'color-tm-green-bg';
                break;
            default:
                return '';
        }
    }
}]);

haulerClaimApp.controller('EligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'itemSubmission', 'tireCountBalance', 'tireCountBalanceWithoutAdj', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, itemSubmission, tireCountBalance, tireCountBalanceWithoutAdj, selectedItem, chooseTypeIsDisable, disableForReadOnlyStaff, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.itemSubmission = itemSubmission;
    $scope.tireCountBalance = tireCountBalance;
    $scope.tireCountBalanceWithoutAdj = tireCountBalanceWithoutAdj;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; //OTSTM2-155
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";
    $scope.confirm = function (isValid) {

        //OTSTM2-270
        if (isValid === false || !isValid) return;

        $scope.modalResult.selectedItem = $scope.selectedItem;
        
        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: false });
    }

    $scope.cancel = function () {
        //$uibModalInstance.dismiss('cancel');
        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: true });
    };

    /*
    ---------------------------------------------------------------------------------
    ------------------validations start here OTSTM2-270------------------------------
    */
    //OTSTM2-270 updating loadUrl and addUrl for InternalNotes for 'internalAdjustModal.html'
    //id = 4 is the InternalAdjustTypes for Payment
    if ($scope.isReadOnly) {
        if (selectedItem.id === '4') {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
        }
        else {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
        }
    }   

    //OTSTM2-270 get current choosetype
    $scope.returnCurrentChooseType = function (item) {

        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });
        return curr[0].isValid();
    }

    //OTSTM2-270 param1 = item is current choose item & param1 = rule is the type from error rules for each validationtypes
    $scope.getErrorRule = function (item, rule) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });

        if (curr[0].errorRules.hasOwnProperty(rule)) {
            return curr[0].errorRules[rule]($scope.internaladjustForm);
        }
    }

    
    //OTSTM2-489
    $scope.change_yardcount_plt = function () {
        $scope.modalResult.pltAfterAdj = angular.isNumber($scope.modalResult.plt) ? $scope.modalResult.plt + modalResult.pltBeforeAdj : modalResult.pltBeforeAdj;
    }

    $scope.change_yardcount_mt = function () {
        $scope.modalResult.mtAfterAdj = angular.isNumber($scope.modalResult.mt) ? $scope.modalResult.mt + modalResult.mtBeforeAdj : modalResult.mtBeforeAdj;
    }

    $scope.change_yardcount_agls = function () {
        $scope.modalResult.aglsAfterAdj = angular.isNumber($scope.modalResult.agls) ? $scope.modalResult.agls + modalResult.aglsBeforeAdj : modalResult.aglsBeforeAdj;
    }

    $scope.change_yardcount_ind = function () {
        $scope.modalResult.indAfterAdj = angular.isNumber($scope.modalResult.ind) ? $scope.modalResult.ind + modalResult.indBeforeAdj : modalResult.indBeforeAdj;
    }

    $scope.change_yardcount_sotr = function () {
        $scope.modalResult.sotrAfterAdj = angular.isNumber($scope.modalResult.sotr) ? $scope.modalResult.sotr + modalResult.sotrBeforeAdj : modalResult.sotrBeforeAdj;
    }

    $scope.change_yardcount_motr = function () {
        $scope.modalResult.motrAfterAdj = angular.isNumber($scope.modalResult.motr) ? $scope.modalResult.motr + modalResult.motrBeforeAdj : modalResult.motrBeforeAdj;
    }

    $scope.change_yardcount_lotr = function () {
        $scope.modalResult.lotrAfterAdj = angular.isNumber($scope.modalResult.lotr) ? $scope.modalResult.lotr + modalResult.lotrBeforeAdj : modalResult.lotrBeforeAdj;
    }

    $scope.change_yardcount_gotr = function () {
        $scope.modalResult.gotrAfterAdj = angular.isNumber($scope.modalResult.gotr) ? $scope.modalResult.gotr + modalResult.gotrBeforeAdj : modalResult.gotrBeforeAdj;
    }


    //OTSTM2-270
    $scope.isTireTypeAllZeros = function () {
        return validation.checkAllTireItemsZero($scope.modalResult)
    }

    //OTSTM2-270
    $scope.allZeroesOrEmpty = function () {
        var args = [].slice.call(arguments);

        return validation.checkAllFieldsZero(args)
    }

    var validation = {

        init: function () {
            var self = this;

            this.validationTypes = [
                   {
                       name: 'Weight',
                       id: '1',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.offroad, $scope.modalResult.onroad])
                       },
                       errorRules: {
                           _onRoad: function (form) {
                               return (form.onroad.$error.isDecimalRequired && !form.onroad.$pristine) || (form.onroad.$error.number && !form.onroad.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.onroad, $scope.modalResult.offroad]))
                           },
                           _offRoad: function (form) {
                               return (form.offroad.$error.isDecimalRequired && !form.offroad.$pristine) || (form.offroad.$error.number && !form.offroad.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.onroad, $scope.modalResult.offroad]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Tire Counts',
                       id: '2',
                       isValid: function () {
                           return self.isValid() && !self.checkAllTireItemsZero($scope.modalResult);
                       },
                       errorRules: {
                           _plt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_plt.$error.number && !form.tirecount_plt.$pristine))
                           },
                           _mt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_mt.$error.number && !form.tirecount_mt.$pristine))
                           },
                           _agls: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_agls.$error.number && !form.tirecount_agls.$pristine))
                           },
                           _ind: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_ind.$error.number && !form.tirecount_ind.$pristine))
                           },
                           _sotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_sotr.$error.number && !form.tirecount_sotr.$pristine))
                           },
                           _motr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_motr.$error.number && !form.tirecount_motr.$pristine))
                           },
                           _lotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_lotr.$error.number && !form.tirecount_lotr.$pristine))
                           },
                           _gotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_gotr.$error.number && !form.tirecount_gotr.$pristine))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Yard Count',
                       id: '3',
                       isValid: function () {
                           return self.isValid() && !self.checkAllTireItemsZero($scope.modalResult);
                       },
                       errorRules: {
                           _plt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_plt.$error.number && !form.yardcount_plt.$pristine))
                           },
                           _mt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_mt.$error.number && !form.yardcount_mt.$pristine))
                           },
                           _agls: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_agls.$error.number && !form.yardcount_agls.$pristine))
                           },
                           _ind: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_ind.$error.number && !form.yardcount_ind.$pristine))
                           },
                           _sotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_sotr.$error.number && !form.yardcount_sotr.$pristine))
                           },
                           _motr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_motr.$error.number && !form.yardcount_motr.$pristine))
                           },
                           _lotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_lotr.$error.number && !form.yardcount_lotr.$pristine))
                           },
                           _gotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.yardcount_gotr.$error.number && !form.yardcount_gotr.$pristine))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Payment',
                       id: '4',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.amount])
                       },
                       errorRules: {
                           _amount: function (form) {
                               return (form.amount.$error.isDecimalRequired && !form.amount.$pristine) || (form.amount.$error.number && !form.amount.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.amount]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   }
            ];

            $scope.$watch('selectedItem', function () {
                if ($scope.internaladjustForm) {
                    $scope.internaladjustForm.$setPristine();
                    $scope.internaladjustForm.$setUntouched();
                }
            });
        },

        isValid: function () {
            return ($scope.internaladjustForm.$valid);
        },

        checkAllTireItemsZero: function (item) {
            var tireObj = {
                plt: item.plt,
                agls: item.agls,  
                gotr: item.gotr,  
                ind: item.ind, 
                lotr: item.lotr,  
                motr: item.motr, 
                mt: item.mt,
                sotr: item.sotr
            };

            var arr = _.values(tireObj)
            var isValid = arr.every(function (val) {
                return (val === 0 || val === null);
            });
            return isValid;
        },
        //para accepts only an array
        checkAllFieldsZero: function (arr) {
            if (arr instanceof Array) {
                return arr.every(function (val) {
                    return (val === 0 || val === null);
                });
            }
        },

        getAllValidationTypes: function () {
            return this.validationTypes;
        },

        empty: function (item) {
            return item.plt == null || item.agls == null || item.gotr == null || item.ind == null || item.lotr == null
                || item.motr == null || item.mt == null || item.sotr == null;
        }
    }

    validation.init();
    /*
    ------------------validations end here------------------------------
    --------------------------------------------------------------------
    */
}]);

haulerClaimApp.controller('EligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
