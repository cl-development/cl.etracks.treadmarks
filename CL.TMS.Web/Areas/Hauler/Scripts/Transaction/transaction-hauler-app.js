﻿"use strict";

// Transaction APP
var app = angular.module('TransactionApp', ['Transaction.directives', 'Transaction.controllers', 'ui.bootstrap', 'commonLib', 'commonTransactionLib']);


// Controllers Section
var controllers = angular.module('Transaction.controllers', []);

controllers.controller('TransactionCtrl', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);


// Directives Section
var directives = angular.module('Transaction.directives', []);

directives.directive('ucrPickedUpFromHandler', function () {
    return {
        restrict: 'A',
        controller: function ($rootScope, $scope) {
            $scope.$watch('trans.paymentEligible', function () {
                if (angular.isDefined($scope.trans.pickedUpFrom) && angular.isDefined($scope.trans.paymentEligible) && $scope.trans.paymentEligible == "1") {
                    $scope.trans.pickedUpFrom = 'unregistered';
                }
            }, true);

            $scope.$watch('trans.pickedUpFrom', function () {
                if (angular.isDefined($scope.trans.pickedUpFrom)) {
                    $rootScope.$emit('set-unregistered-company-requirement', { required: $scope.trans.pickedUpFrom == 'unregistered' });
                    $rootScope.$emit('set-vendor-information-requirement', { required: $scope.trans.pickedUpFrom == 'registered' });
                }
            }, true);
        }
    };
});

directives.directive('ucrPaymentEligibleCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            paymentEligible: "=",
            paymentEligibleOther: "="
        },
        templateUrl: 'ucrPaymentEligible.html',
        controller: function ($scope) {
        }
    };
});

directives.directive('stcEventNumberCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            eventNumber: "=",
            adjustFlag: "@", //OTSTM2-1214
            dateCollected: "=", //OTSTM2-1215
            eventNumberValidationCondition: "=" //OTSTM2-1214
        },
        templateUrl: 'stcEventNumber.html',
        controller: function ($rootScope, $scope, $http, GlobalSettingService) { //OTSTM2-1215 focus out validation and datetimepicker selecting validation
            $rootScope.$on('EVENT_NUMBER_VALIDATION', function (e) {
                eventNumberValidationCheck($scope, $http, GlobalSettingService);
            });

            $('[name="eventNumber"]').focusout(function (e) {
                eventNumberValidationCheck($scope, $http, GlobalSettingService);              
            });
        }
    };
});

directives.directive('rtrTireMarketCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            tireMarket: "=",
            countryList: "=",
            supportingDocumentList: "=",
            companyInfo: "="
        },
        templateUrl: 'rtrTireMarket.html',
        controller: function ($scope) {
            $scope.filterOutCanadaAndUSA = function (element) {
                return element.match(/^Canada|USA/) ? false : true;
            };

            $scope.tireMarketLocationChange = function () {
                var supportingDocument = _.find($scope.supportingDocumentList, function (i) { return i.documentName == "Bill of Lading"; });
                supportingDocument.required = $scope.tireMarket.location == "USA";

                if ($scope.tireMarket.location == "Canada: Ontario") $scope.companyInfo.province = "Ontario";
                else $scope.companyInfo.province = "";

                if ($scope.tireMarket.location == "Canada: Ontario" || $scope.tireMarket.location == "Canada: Not Ontario") $scope.companyInfo.country = "Canada";
                else if ($scope.tireMarket.location == "USA") $scope.companyInfo.country = "USA";
                else $scope.companyInfo.country = "";
            };
        }
    };
});

function eventNumberValidationCheck($scope, $http, GlobalSettingService) {
    if ($scope.form.eventNumber && $scope.form.eventNumber.$valid) {
        if ($scope.dateCollected.on == null) {                       
            $scope.dateValidation = true;
            $('[name="eventNumber"]').addClass("validation-focus");
            $('[name="eventNumber"]').siblings(".event-number-validity").removeClass("display-none");
            $('[name="eventNumber"]').siblings(".event-number-validity").removeClass("ng-hide");
        }
        else {
            $scope.dateValidation = false;
            $('[name="eventNumber"]').removeClass("validation-focus");
            $http({
                url: GlobalSettingService.EventNumberValidationCheck(),
                method: "POST",
                data: {
                    eventNumber: $scope.eventNumber,
                    transactionStartDate: $scope.dateCollected.on
                }
            }).success(function (result) {

                $('[name="eventNumber"]').siblings(".event-number-validity").removeClass("display-none");

                switch (result.status) {
                    case 0:
                        $scope.eventNumberValidationCondition = 'Pass';
                        $('[name="eventNumber"]').removeClass("validation-focus");
                        break;
                    case 1:
                        $scope.eventNumberValidationCondition = 'NotExisting';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        break;
                    case 2:
                        $scope.eventNumberValidationCondition = 'IsInUse';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        return;
                        break;
                    case 3:
                        $scope.eventNumberValidationCondition = 'NotInDateRange';
                        $('[name="eventNumber"]').addClass("validation-focus");
                        return;
                        break;

                    default:

                }
            }).error(function () {
            });
        }
    }
    else {
        $('[name="eventNumber"]').siblings(".event-number-validity").addClass("display-none");
    }
}