﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Diagnostics;

using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.Security.Authorization;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;

using Newtonsoft.Json;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Security;
using CL.TMS.UI.Common.Security;
using System.Web.Routing;

namespace CL.TMS.Web.Areas.RPM.Controllers
{
    public class TransactionController : BaseController
    {
        #region Properties
        private ITransactionService transactionService;
        private IRegistrantService registrantService;
        private IClaimService claimService;
        #endregion


        #region Constructor
        public TransactionController(ITransactionService transactionService, IRegistrantService registrantService, IClaimService claimService)
        {
            this.transactionService = transactionService;
            this.registrantService = registrantService;
            this.claimService = claimService;
        }
        #endregion


        #region Action Methods
        public ActionResult Index(int id, string type, int direction)
        {

            ViewBag.ngApp = "TransactionApp";
            ViewBag.VendorCode = TreadMarksConstants.RPM;

            var claimStatus = this.claimService.GetClaimStatus(id);

            ViewBag.ClaimId = id;
            ViewBag.TypeId = type;
            ViewBag.Direction = direction;

            if (null != SecurityContextHelper.CurrentVendor)
            {
                ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;

                var vendorReference = claimService.GetVendorReference(id);
                if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 5)
                {
                    if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                    {
                        return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                    }
                    else
                    {
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                    }
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = (SecurityContextHelper.IsStaff() && claimStatus == ClaimStatus.UnderReview);

            //OTSTM2-155
            bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummaryOutbound) == SecurityResultType.EditSave);
            if (type == TreadMarksConstants.SPS)
            {
                bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummaryInbound) == SecurityResultType.EditSave);
            }
            if (SecurityContextHelper.IsStaff()) ViewBag.AllowAddTransaction = this.claimService.IsClaimAllowAddNewTransaction(id) && bWritePermission;
            else ViewBag.AllowAddTransaction = (bWritePermission && claimStatus == ClaimStatus.Open);

            ViewBag.ClaimPeriod = this.claimService.GetClaimPeriod(id).ShortName;
            ViewBag.AllowEdit = bWritePermission;

            switch (type)
            {
                case TreadMarksConstants.SPS:
                    //SPS for processor is SIR for RPM
                    ViewBag.TransactionType = TreadMarksConstants.SPS;
                    ViewBag.HeaderName = "SIR Transactions";
                    return View("SIRTransactions");

                case TreadMarksConstants.SPSR:
                    ViewBag.TransactionType = TreadMarksConstants.SPSR;
                    ViewBag.HeaderName = "SPS Transactions";
                    return View("SPSRTransactions");

                default:
                    return RedirectToAction("Index", "Claims", new { area = "RPM" });
            };
        }

        [HttpPost]
        public JsonResult VoidTransaction(int transactionId)
        {
            transactionService.VoidSPSRTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Service Methods

        #region Paper Form Service Methods
        [HttpPost]
        public NewtonSoftJsonResult initializePaperForm(int id, string type, int direction)
        {
            var claimPeriod = this.claimService.GetClaimPeriod(id);

            IRPMTransactionViewModel paperFormModel = new RPMTransactionViewModel();

            switch (type)
            {
                case TreadMarksConstants.SPSR:
                    paperFormModel = new RPMSPSRTransactionViewModel();
                    paperFormModel.initialize();

                    var vmSPSR = paperFormModel as RPMSPSRTransactionViewModel;
                    vmSPSR.CountryList.AddRange(DataLoader.CountryNames); //OTSTM2-1016
                    vmSPSR.ProductList.AddRange(TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue, SecurityContextHelper.CurrentVendor.VendorId, claimPeriod.StartDate, claimPeriod.EndDate));
                    break;

                default:
                    return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            };

            paperFormModel.CurrentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
            paperFormModel.ClaimId = id;

            // Form date related setting            
            TransactionHelper.initializeFormDate(paperFormModel.FormDate, claimPeriod, ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummary));
            paperFormModel.FormDate.DisabledDateRangeList = new List<TransactionDisabledDateRangeViewModel>();
            paperFormModel.FormDate.DisabledDateRangeList.AddRange(transactionService.GetVendorInactiveDateRanges(SecurityContextHelper.CurrentVendor.VendorId));


            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = paperFormModel } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionSPSR(RPMSPSRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = null;
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                transactionService.AddRPMPapeFormTransaction(transactionDataModel);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }
        #endregion

        //OTSTM2-81
        #region Import SPSTransactions
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase fileToImport, int claimId)
        {
            int count = 0;
            try
            {
                if (fileToImport == null)
                {
                    return Json(new { Status = "fail", MessageTitle = "Warning", MessageBody = "Import Failed. No file importing" }, JsonRequestBehavior.AllowGet);
                }
                else if (fileToImport.ContentLength == 0)
                {
                    return Json(new { Status = "fail", MessageTitle = "Warning", MessageBody = "Your import has failed because the CSV file contained no data. Please review your CSV file and try again." }, JsonRequestBehavior.AllowGet);
                }
                else if (Path.GetExtension(fileToImport.FileName) != ".csv")
                {
                    return Json(new { Status = "fail", MessageTitle = "Warning", MessageBody = "Your import has failed because the file you have tried to import was not a CSV file. Please review your file and try again. " }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    count = transactionService.ImportSPSTransactions(new StreamReader(fileToImport.InputStream), claimId);
                    if (count == 0)
                    {
                        return Json(new { Status = "fail", MessageTitle = "Warning", MessageBody = "Your import has failed because the CSV file contained no data. Please review your CSV file and try again." }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                //OTSTM2-531
                var dataErrorList = ex.Data["Data_Error_List"];
                return Json(new { Status = "datafail", MessageTitle = "Warning", MessageBodyDataError = dataErrorList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Status = "success", MessageTitle = "Confirm", MessageBody = "Your CSV file <strong>" + fileToImport.FileName + "</strong> has been successfully imported <strong>" + count + "</strong> SPS transactions" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region List Handler Service Methods
        public ActionResult GetTransactionListHandler(DataGridModelExtend param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();

                    switch (transactionType)
                    {
                        case TreadMarksConstants.SPS:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "VendorNumber");

                            columns.Add(5, "BusinessName");
                            columns.Add(6, "InvoiceNbr");
                            columns.Add(7, "ProductType");
                            columns.Add(8, "MeshRange");
                            columns.Add(9, "TotalWeight");
                            break;


                        case TreadMarksConstants.SPSR:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "BusinessName");

                            columns.Add(5, "Destination");
                            columns.Add(6, "ProductType");
                            columns.Add(7, "ProductCode");
                            columns.Add(8, "Description");
                            columns.Add(9, "Percentage");

                            columns.Add(10, "TotalWeight");
                            columns.Add(11, "Rate");
                            columns.Add(12, "AmountDollar");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    var transactions = transactionService.LoadRPMVendorTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);

                    //foreach (var item in transactions.DTOCollection)
                    //{
                    //    if (null != item.TransactionNotes)
                    //    {
                    //        foreach (var item2 in item.TransactionNotes)
                    //        {
                    //            if (item2.User != null) { item2.User = null; }
                    //        }
                    //    }
                    //}
                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummaryOutbound) == SecurityResultType.EditSave);

                    var json = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json, false);

                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData, error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetRPMStaffTransactionsList(DataGridModelExtend param)
        {
            //var allTransactions = this.transactionService.LoadAllTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, VendorId);

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    columns = new Dictionary<int, string>();
                    switch (transactionType)
                    {
                        case TreadMarksConstants.SPS:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "VendorNumber");
                            columns.Add(6, "BusinessName");
                            columns.Add(7, "InvoiceNbr");
                            columns.Add(8, "ProductType");
                            columns.Add(9, "MeshRange");
                            columns.Add(10, "TotalWeight");
                            break;


                        case TreadMarksConstants.SPSR:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "BusinessName");
                            columns.Add(6, "Destination");
                            columns.Add(7, "ProductType");
                            columns.Add(8, "ProductCode");
                            columns.Add(9, "Description");

                            columns.Add(10, "Percentage");
                            columns.Add(11, "TotalWeight");
                            columns.Add(12, "Rate");
                            columns.Add(13, "AmountDollar");
                            columns.Add(14, "RetailPrice");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    var transactions = transactionService.LoadRPMStaffTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);
                    var resourceName = (transactionType == TreadMarksConstants.SPS) ? TreadMarksConstants.ClaimsRPMClaimSummaryInbound : TreadMarksConstants.ClaimsRPMClaimSummaryOutbound;
                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(resourceName) == SecurityResultType.EditSave);

                    //foreach (var item in transactions.DTOCollection)
                    //{
                    //    if (null != item.TransactionNotes)
                    //    {
                    //        foreach (var item2 in item.TransactionNotes)
                    //        {
                    //            if (item2.User != null) { item2.User = null; }
                    //        }
                    //    }
                    //}

                    var json1 = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json1, false);
                }
            }
            return Json(new { status = "", error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export to CSV Service Methods
        public ActionResult ExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sFileTypename = "";
            //columns.Add("iPad/Form");
            //columns.Add("Badge");
            //columns.Add("Transaction Date");
            List<Func<RPMCommonTransactionListViewModel, string>> delegates = new List<Func<RPMCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.SPS:
                    columns.Add("Invoice Date");
                    columns.Add("SIR #");
                    columns.Add("Rev. Stat.");
                    columns.Add("Adj.");
                    columns.Add("Processor #");

                    columns.Add("Business Name");
                    columns.Add("Invoice#");
                    columns.Add("Product Type");
                    columns.Add("Mesh Range");
                    columns.Add("Actual (t)");

                    delegates = new List<Func<RPMCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => u.ReviewStatus,
                        u => u.Adjustment,
                        u => u.VendorNumber,

                        u => u.BusinessName,
                        u => u.InvoiceNbr,
                        u => u.ProductType,
                        u => u.MeshRange,
                        u => u.TotalWeight.ToString(),
                    };
                    sFileTypename = "SIR";
                    break;

                case TreadMarksConstants.SPSR:
                    columns.Add("Invoice Date");
                    columns.Add("SPS #");
                    columns.Add("Rev. Stat.");
                    columns.Add("Adj.");
                    columns.Add("Business Name");

                    columns.Add("Destination");
                    columns.Add("Product Type");
                    columns.Add("Product Code");
                    columns.Add("Description");
                    columns.Add("%");

                    columns.Add("Actual(t)");
                    columns.Add("Rate($/t)");
                    columns.Add("Amount($)");

                    delegates = new List<Func<RPMCommonTransactionListViewModel, string>>()
                    {
                        u => u.InvoiceDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => u.ReviewStatus,
                        u => u.Adjustment,
                        u => u.BusinessName,

                        //u => (null == u.VendorNumber)?string.Empty:u.VendorNumber.ToString(),

                        u => u.Destination,
                        u => u.ProductType,
                        u => u.ProductCode,
                        u => u.Description,
                        u => u.Percentage.ToString(),

                        u => u.TotalWeight.ToString(),
                        u => u.Rate.ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar_Ton)
                    };
                    sFileTypename = "SPS";
                    break;

                default:
                    break;
            };
            #endregion

            var Transactions = this.transactionService.GetRPMExportDetailsParticipantTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);

            foreach (var item in Transactions)
            {
                if (null != item.TransactionNotes)
                {
                    foreach (var item2 in item.TransactionNotes)
                    {
                        if (item2.User != null) { item2.User = null; }
                    }
                }
            }


            string csv = Transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}-{1}.csv", sFileTypename, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult StaffExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            int vendorId = (null != vendor) ? vendor.VendorId : 0;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sFileTypename = "";
            List<Func<RPMCommonTransactionListViewModel, string>> delegates = new List<Func<RPMCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.SPS:
                    columns.Add("Invoice Date");
                    columns.Add("SIR #");
                    columns.Add("Rev. Stat.");
                    columns.Add("Adj.");
                    columns.Add("Notes");
                    columns.Add("Processor #");

                    columns.Add("Business Name");

                    columns.Add("Invoice#");

                    columns.Add("Product Type");
                    columns.Add("Mesh Range");
                    columns.Add("Actual (t)");

                    delegates = new List<Func<RPMCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => u.ReviewStatus,
                        u => u.Adjustment,
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.VendorNumber,

                        u => u.BusinessName,

                        u => u.InvoiceNbr,

                        u => u.ProductType,
                        u => u.MeshRange,
                        u => u.TotalWeight.ToString(),
                    };
                    sFileTypename = "SIR";
                    break;

                case TreadMarksConstants.SPSR:
                    columns.Add("Invoice Date");
                    columns.Add("SPS #");
                    columns.Add("Rev. Stat.");
                    columns.Add("Adj.");
                    columns.Add("Notes");
                    columns.Add("Business Name");

                    columns.Add("Destination");
                    columns.Add("Product Type");
                    columns.Add("Product Code");
                    columns.Add("Description");
                    columns.Add("%");

                    columns.Add("Actual(t)");
                    columns.Add("Rate($/t)");
                    columns.Add("Amount($)");
                    columns.Add("RP($)");

                    delegates = new List<Func<RPMCommonTransactionListViewModel, string>>()
                    {
                        u => u.InvoiceDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => u.ReviewStatus,
                        u => u.Adjustment,
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.BusinessName,

                        //u => (null == u.VendorNumber)?string.Empty:u.VendorNumber.ToString(),

                        u => u.Destination,
                        u => u.ProductType,
                        u => u.ProductCode,
                        u => u.Description,
                        u => u.Percentage.ToString(),

                        u => u.TotalWeight.ToString(),
                        u => u.Rate.ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar_Ton), //OTSTM2-423
                        u => u.RetailPrice != null ? String.Format( "{0:#,##0.##}", u.RetailPrice)  : "", //OTSTM2-423
                    };
                    sFileTypename = "SPS";
                    break;

                default:
                    break;
            };
            #endregion

            var Transactions = this.transactionService.GetRPMExportDetailsStaffTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);

            foreach (var item in Transactions)
            {
                if (null != item.TransactionNotes)
                {
                    foreach (var item2 in item.TransactionNotes)
                    {
                        if (item2.User != null) { item2.User = null; }
                    }
                }
                item.TotalWeight = Math.Round(item.TotalWeight, 4);
            }


            string csv = Transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}-{1}.csv", sFileTypename, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #endregion
    }
}