﻿using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.Framework.DTO;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RPMServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Claims.Export;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;

namespace CL.TMS.Web.Areas.RPM.Controllers
{
    public class ClaimsController : ClaimBaseController
    {

        #region Properties

        private IRPMClaimService rpmClaimService;
        private IClaimService claimService;
        private IFileUploadService fileUploadService;

        #endregion

        #region Constructor

        public ClaimsController(IRPMClaimService rpmClaimService, IClaimService claimService, IFileUploadService fileUploadService)
        {

            this.rpmClaimService = rpmClaimService;
            this.claimService = claimService;
            this.fileUploadService = fileUploadService;

        }
        #endregion

        #region Action Methods

        // GET: /RPM/Claims/
        public ActionResult Index()
        {
            if (SecurityContextHelper.CurrentVendor == null || SecurityContextHelper.CurrentVendor.VendorType != 5)
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            ViewBag.SelectedMenu = "Claims";
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;

            if (!SecurityContextHelper.IsStaff())
            {
                return View("ParticipantClaimIndex");
            }
            else
            {
                var model = new ClaimsCommonHeaderViewModel()
                {
                    RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber,
                    OperatingName = SecurityContextHelper.CurrentVendor.OperatingName,
                    IsActive = SecurityContextHelper.CurrentVendor.IsActive,
                    Generator = SecurityContextHelper.CurrentVendor.CIsGenerator,
                    PrimaryBusinessActivity = SecurityContextHelper.CurrentVendor.PrimaryBusinessActivity,
                    Address = SecurityContextHelper.CurrentVendor.Address,
                    PrimaryContact = SecurityContextHelper.CurrentVendor.PrimaryContact,
                    CGeneratorDate = SecurityContextHelper.CurrentVendor.CGeneratorDate,
                    ActiveStatusChangeDate = SecurityContextHelper.CurrentVendor.ActiveStatusChangeDate
                };
                return View("StaffClaimIndex", model);
            }
        }
        [ClaimsAuthorization(TreadMarksConstants.ClaimsRPMClaimSummary)]
        public ActionResult ClaimSummary(int claimId, int? vId = null)
        {
            if (null == SecurityContextHelper.CurrentVendor && vId == null)//session time out
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            else if (vId != null)
            {
                var vendor = claimService.GetVendorById(vId.Value);
                if (vendor != null)
                {
                    //Keep it in session
                    //Vendor Context Check
                    if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 5)
                        Session["SelectedVendor"] = vendor;
                    else
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
                else
                    return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }
            ViewBag.ngApp = "rpmClaimApp";
            ViewBag.ClaimsId = claimId;
            //OTSTM2-551 only claim summary page for staff has activity panel (can get real time activity)
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.HasActivityPanel = 1;
            }
            ViewBag.ClaimsStatus = claimService.GetClaimStatus(claimId);
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.UserId = SecurityContextHelper.CurrentUser.Id;
            ViewBag.UserName = string.Format("{0} {1}", SecurityContextHelper.CurrentUser.FirstName, SecurityContextHelper.CurrentUser.LastName);

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = false;
            ViewBag.AllowEdit = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummary) == SecurityResultType.EditSave);

            var vendorReference = claimService.GetVendorReference(claimId);
            if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 5)
            {
                if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                {
                    return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }

            var model = new ClaimsCommonHeaderViewModel()
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber,
                OperatingName = SecurityContextHelper.CurrentVendor.OperatingName,
                IsActive = SecurityContextHelper.CurrentVendor.IsActive,
                Generator = SecurityContextHelper.CurrentVendor.CIsGenerator,
                PrimaryBusinessActivity = SecurityContextHelper.CurrentVendor.PrimaryBusinessActivity,
                Address = SecurityContextHelper.CurrentVendor.Address,
                PrimaryContact = SecurityContextHelper.CurrentVendor.PrimaryContact,
                CGeneratorDate = SecurityContextHelper.CurrentVendor.CGeneratorDate,
                ActiveStatusChangeDate = SecurityContextHelper.CurrentVendor.ActiveStatusChangeDate
            };
            return View(SecurityContextHelper.IsStaff() ? "StaffClaimSummary" : "ParticipantClaimSummary", model);
        }

        [HttpGet]
        public ActionResult ClaimDetails(int claimId)
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            var result = rpmClaimService.LoadClaimSummaryData(vendorId, claimId);
            if (SecurityContextHelper.IsStaff())
            {
                PopulateTransationUrlForStatusPanel(result.ClaimStatus, "RPM", claimId);
            }
            result.ClaimCommonModel.ClaimPeriodName = result.ClaimCommonModel.ClaimPeriod.ShortName;
            result.ClaimCommonModel.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;

            if (null != result.ClaimStatus)
            {
                result.ClaimStatus.Submitted = ConvertTimeFromUtc(result.ClaimStatus.Submitted);
                result.ClaimStatus.Assigned = ConvertTimeFromUtc(result.ClaimStatus.Assigned);
                result.ClaimStatus.Started = ConvertTimeFromUtc(result.ClaimStatus.Started);
                result.ClaimStatus.ReviewDue = ConvertTimeFromUtc(result.ClaimStatus.ReviewDue);
                result.ClaimStatus.Approved = ConvertTimeFromUtc(result.ClaimStatus.Approved);
                result.ClaimStatus.Reviewed = ConvertTimeFromUtc(result.ClaimStatus.Reviewed);
                result.ClaimStatus.ClaimOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOnholdDate);
                result.ClaimStatus.ClaimOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOffholdDate);
                result.ClaimStatus.AuditOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOnholdDate);
                result.ClaimStatus.AuditOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOffholdDate);
            }

            return new NewtonSoftJsonResult(result, false);
        }

        [HttpGet]
        public ActionResult ExportToExcel(string searchText = "")
        {
            //var dataTable = RPMClaimService.GetExportDetails(searchText);
            //var result = DataTableHelper.ConvertToString(dataTable);
            //var bytes = Encoding.UTF8.GetBytes(result);
            //var filename = string.Format("Claims-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            //return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
            return null;
        }

        [HttpPost]
        public JsonResult SubmitClaimCheck(RPMSubmitClaimViewModel submitClaimModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }

            var updatedSubmitClaimModel = this.rpmClaimService.RPMClaimSubmitBusinessRule(submitClaimModel);

            if (updatedSubmitClaimModel.Errors.Count > 0)
            {
                //handle errors
                var result = new { status = "Errors", Type = SubmitBusinessRuleType.Error.ToString(), Message = updatedSubmitClaimModel.Errors.First() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (updatedSubmitClaimModel.Warnings.Count > 0)
            {
                //handle warnings
                return Json(new { status = "Warnings", Type = SubmitBusinessRuleType.Warning.ToString(), Warnings = updatedSubmitClaimModel.Warnings }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "Valid Data", Type = SubmitBusinessRuleType.Success.ToString() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FinalSubmitClaim(int claimId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
            this.claimService.SubmitClaim(claimId, false);

            return Json(new { status = "Valid Data" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export
        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelPaymentAdjustment(IList<CommonPaymentAdjustExportViewModel> PaymentAdjList)
        {
            var columnNames = new List<string>(){
                "","Payment ($)"
            };

            var delegates = new List<Func<CommonPaymentAdjustExportViewModel, string>>()
            {
                u => u.RowName,
                u => u.Payment
            };

            string csv = PaymentAdjList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ReuseTiresClaimSummary-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion
    }
}