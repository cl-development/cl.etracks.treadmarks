﻿"use strict";

// Transaction APP
var app = angular.module('TransactionApp', ['Transaction.directives', 'Transaction.controllers', 'ui.bootstrap', 'commonLib', 'commonTransactionLib']);


// Controllers Section
var controllers = angular.module('Transaction.controllers', []);

controllers.controller('TransactionCtrl', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);


// Directives Section
var directives = angular.module('Transaction.directives', []);

directives.directive('spsProductDetailCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            productDetail: "=",
            productList: "=",
            totalWeight: "=",
            adjust: "@",
            unitType: "=",
        },
        templateUrl: 'sps-product-detail.html',
        controller: function ($scope) {
            $scope.currentProduct = null;
            $scope.adjustmentInitialized = false;
            $scope.isStaff = Global.Settings.isStaff;

            
            $scope.isAdjustment = function () {
                if ((angular.isDefined($scope.adjust) && $scope.adjust == "true")) {
                    $scope.AdjustmentMode = true;
                } else {
                    $scope.AdjustmentMode = false;
                }
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.$watch('productDetail', function () {
                if (angular.isDefined($scope.productDetail) && $scope.isAdjustment() && !$scope.adjustmentInitialized) {
                    var product = _.find($scope.productList, function (i) { return i.productId == $scope.productDetail.productTypeId; });
                    if (!_.isUndefined(product)) $scope.currentProduct = product;
                    $scope.adjustmentInitialized = true;
                }
            }, true);
            
            $scope.$watch('currentProduct', function () {
                if ($scope.currentProduct == null) {
                    $scope.productDetail.productTypeId = null;                    
                    $scope.productDetail.productRate = null;                    
                }
                else {
                    $scope.productDetail.productTypeId = $scope.currentProduct.productId;                    
                    $scope.productDetail.productRate = $scope.currentProduct.rate;
                }
            }, true);

            $scope.showAmount = function () {
                if ($scope.productDetail.productRate != null && $scope.totalWeight != null) return true;

                return false;
            };

            $scope.getAmount = function () {
                if (($scope.productDetail == null) || ($scope.productDetail.productRate == null) || ($scope.totalWeight == null)) {
                    return;
                }
                if ($scope.unitType == 1) return $scope.productDetail.productRate * GlobalSettingService.ConvertLbToTon($scope.totalWeight).toFixed(4);
                if ($scope.unitType == 2) return $scope.productDetail.productRate * GlobalSettingService.ConvertKgToTon($scope.totalWeight).toFixed(4);
                if ($scope.unitType == 3) return ($scope.productDetail.productRate * $scope.totalWeight);
            };

        }
    };
}]);

directives.directive('sirProductDetailCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            productDetail: "=",
            productList: "=",
            scaleTicket: "=",
            adjust: "@",
            mobile: "@"
        },
        templateUrl: 'sir-product-detail.html',
        controller: function ($scope) {
            $scope.currentProduct = null;
            $scope.adjustmentInitialized = false;

            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.isMobile = function () {
                return (angular.isDefined($scope.mobile) && $scope.mobile == "true");
            };

            $scope.$watch('productDetail', function () {
                if (angular.isDefined($scope.productDetail) && $scope.isAdjustment() && !$scope.adjustmentInitialized) {
                    var product = _.find($scope.productList, function (i) { return i.productId == $scope.productDetail.productTypeId; });
                    if (!_.isUndefined(product)) $scope.currentProduct = product;
                    $scope.adjustmentInitialized = true;
                }
            }, true);

            $scope.productChange = function () {
                if ($scope.currentProduct == null) {
                    $scope.productDetail.productTypeId = null;
                    $scope.productDetail.productRate = null;
                }
                else {
                    $scope.productDetail.productTypeId = $scope.currentProduct.productId;
                    $scope.productDetail.productRate = $scope.currentProduct.rate;
                }

                if (!$scope.showMeshRange()) {
                    $scope.productDetail.meshRangeStart = null;
                    $scope.productDetail.meshRangeEnd = null;
                }

                if (!$scope.showIsFreeOfSteel()) {
                    $scope.productDetail.isFreeOfSteel = null;
                }

                if (!$scope.showProductDescription()) {
                    $scope.productDetail.productDescription = null;
                }
            };

            $scope.getScaleTicketWeight = function () {
                var weight = 0.0000;
                if ($scope.isMobile()) {
                    _.each($scope.scaleTicket.tickets, function (i) {
                        weight = weight + (((_.isNull(i.inboundWeight) ? 0.0000 : i.inboundWeight) - (_.isNull(i.outboundWeight) ? 0.0000 : i.outboundWeight)));
                    });
                } else {
                    weight = _.isNull($scope.scaleTicket.weight) ? 0.0000 : $scope.scaleTicket.weight;
                }

                return weight;
            };

            $scope.showAmount = function () {
                return ($scope.productDetail.productRate != null && ($scope.getScaleTicketWeight() * 1) > 0.00);
            };

            $scope.getAmount = function () {
                if ($scope.scaleTicket.weightType == 1) return $scope.productDetail.productRate * GlobalSettingService.ConvertLbToTon($scope.getScaleTicketWeight());
                if ($scope.scaleTicket.weightType == 2) return $scope.productDetail.productRate * GlobalSettingService.ConvertKgToTon($scope.getScaleTicketWeight());
                if ($scope.scaleTicket.weightType == 3) return ($scope.productDetail.productRate * $scope.getScaleTicketWeight());
            };

            $scope.showMeshRange = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP1' || $scope.currentProduct.productCode == 'TDP2' || $scope.currentProduct.productCode == 'TDP3'));
            };

            $scope.showIsFreeOfSteel = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP1' || $scope.currentProduct.productCode == 'TDP2'));
            };

            $scope.showProductDescription = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP4'));
            };
        }
    };
}]);