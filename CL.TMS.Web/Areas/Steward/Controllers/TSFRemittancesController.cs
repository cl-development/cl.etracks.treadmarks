﻿using MicrosoftSystem = System.Web;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Security.Interfaces;
using CL.TMS.ServiceContracts.SystemServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using Microsoft.AspNet.Identity;
using CL.TMS.DataContracts.DomainEntities;
using System.Security.Claims;
using CL.TMS.UI.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.Configuration;
using CL.TMS.ServiceContracts.StewardServices;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Resources;
using System.IO;
using CL.Framework.Logging;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Helper;
using System.Net.Mail;
using System.Net.Mime;

using SystemIO = System.IO;
using System.Configuration;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;
using CL.TMS.Web.Infrastructure;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.Framework.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;
using System.Diagnostics;

namespace CL.TMS.Web.Areas.Steward.Controllers
{
    [Authorize]
    public class TSFRemittancesController : BaseController
    {
        #region Service
        private IRegistrantService registrantService;
        private IStewardRegistrationInvitationService applicationInvitationService; //IApplicationInvitationService        
        private IDashBoardService dashBoardService;
        private IUserService userService;
        private ITSFRemittanceService remittanceService;
        private IFileUploadService fileUploadService;
        private string uploadRepositoryPath;
        private IMessageService messageService;

        public enum ActivityTypeEnum
        {
            StaffSubmitted = 1,
            BackToUnderReview = 2,
            AutoApproved = 3,
            IntAdjRemoved = 4,
        }


        #endregion

        #region Constructors
        public TSFRemittancesController(IRegistrantService registrantService, IStewardRegistrationInvitationService applicationInvitationService, IDashBoardService dashBoardService,
            IUserService userService, ITSFRemittanceService remittanceService, IFileUploadService fileUploadService, IMessageService messageService)
        {
            this.registrantService = registrantService;
            this.applicationInvitationService = applicationInvitationService;
            this.dashBoardService = dashBoardService;
            this.userService = userService;
            this.remittanceService = remittanceService;
            this.fileUploadService = fileUploadService;
            this.messageService = messageService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost

        }
        private IEnumerable<AttachmentModel> GetAttachments(int tsfClaimID)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (tsfClaimID > 0)
            {
                attachments = this.fileUploadService.GetTSFClaimAttachments(tsfClaimID);
            }
            return attachments;
        }
        private void DeleteAttachment(int applicationId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadService.GetTSFClaimAttachment(applicationId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileFullPath = Path.Combine(uploadRepositoryPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadService.RemoveTSFClaimAttachment(applicationId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }
        #endregion

        #region ActionMethod
        [HttpGet]
        public ActionResult Index()
        {
            //Fixed vendor context invalid exception 
            if ((SecurityContextHelper.CurrentVendor != null) && (SecurityContextHelper.CurrentVendor.IsVendor))
            {
                //return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }

            //ToDo: redirct to action baed on user role.          
            ViewBag.curentUserId = SecurityContextHelper.CurrentUser.Id;
            ViewBag.SelectedMenu = "Remittance";
            if ((SecurityContextHelper.IsStaff()))//or is participant user
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (SecurityContextHelper.CurrentVendor != null)
                {
                    if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RemittanceTSFRemittances) == SecurityResultType.NoAccess)
                    {
                        Response.Redirect("/System/Common/UnauthorizedPage");
                    }
                    if (string.IsNullOrWhiteSpace(SecurityContextHelper.CurrentVendor.RegistrationNumber))
                        ParticipantPageHelper.SetCurrentVendor(registrantService, SecurityContextHelper.CurrentVendor.VendorId);

                    ViewBag.RegistrationNumber = vendor.RegistrationNumber;
                    var registerationNo = vendor.RegistrationNumber;
                    var customer = registrantService.GetVendorByNumber(registerationNo);
                    if (customer != null)
                    {
                        ViewBag.BusinessName = customer.BusinessName;
                        ViewBag.Address = customer.Address;
                        ViewBag.PrimaryContact = customer.PrimaryContact;
                        ViewBag.ActivityApprovedDate = customer.ActiveStatusChangeDate;
                        ViewBag.ActivityStatus = SecurityContextHelper.CurrentVendor.IsActive ? ActivityStatusEnum.Active : ActivityStatusEnum.InActive;
                    }

                    var model = new List<TSFRemittanceBriefModel>();
                    ApplyUserSecurity(TreadMarksConstants.RemittanceTSFRemittances);
                    return View("StaffTSFRemittanceSummary", model);
                }
                else
                {
                    if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RemittanceRemittancesAssignment) == SecurityResultType.NoAccess)
                    {
                        Response.Redirect("/System/Common/UnauthorizedPage");
                    }

                    var model = new List<TSFRemittanceBriefModel>();
                    ApplyUserSecurity(TreadMarksConstants.RemittanceRemittancesAssignment);
                    return View("StaffTSFRemittanceAssignment", model);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(SecurityContextHelper.CurrentVendor.RegistrationNumber))
                    ParticipantPageHelper.SetCurrentVendor(registrantService, SecurityContextHelper.CurrentVendor.VendorId);

                var registerationNo = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                var model = new List<TSFRemittanceBriefModel>();

                //Participant side
                ApplyParticipantUserSecurity();
                return View("ParticipantTSFRemittanceSummary", model);
            }
        }

        [HttpGet]
        public ActionResult GetFirstSubmitremittanceService()
        {
            var firstSubmitRemittanceService = remittanceService.GetFirstSubmitRemittance();
            if (firstSubmitRemittanceService != null)
            {
                firstSubmitRemittanceService.CurrentUser = SecurityContextHelper.CurrentUser;
            }

            return new NewtonSoftJsonResult(firstSubmitRemittanceService, false);
        }
        private void ApplyParticipantUserSecurity()
        {
            bool bWritePermission = SecurityContextHelper.HasEditable;
            var isSubmittable = SecurityContextHelper.HasSubmitPermission;

            ViewBag.AllowEdit = bWritePermission || isSubmittable;
            ViewBag.AllowSubmit = isSubmittable;
            ViewBag.AllowAddNew = bWritePermission || isSubmittable;
        }

        [ClaimsAuthorization(TreadMarksConstants.RemittanceTSFRemittance)]
        [HttpGet]
        public ActionResult StaffIndex(int? id, int? vId = null)//vId is added for global search result Remittance summary displaying
        {
            if (null == SecurityContextHelper.CurrentVendor && vId == null)
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            //Fixed vendor context invalid exception 
            if ((SecurityContextHelper.CurrentVendor != null) && (SecurityContextHelper.CurrentVendor.IsVendor) && (vId == null))
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }
            if (vId != null)
            {
                ParticipantPageHelper.SetCurrentVendor(registrantService, vId.Value);
            }

            if (string.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                ApplyUserSecurity();
                return RedirectToAction("Login", "Account", new { area = "", ReturnUrl = "Steward/TSFRemittances/StaffIndex/=" + id });
            }
            VendorReference vendorReference = new VendorReference();
            if (id.HasValue)
            {
                vendorReference = remittanceService.GetVendorReference(id.Value);
            }
            if (SecurityContextHelper.CurrentUser != null)
            {
                ViewData["ApplicationID"] = id;
                ViewBag.IsActive = (SecurityContextHelper.CurrentVendor == null) ? false : SecurityContextHelper.CurrentVendor.IsActive;
                string sArea = string.Empty;
                if (id.HasValue)
                {
                    var model = remittanceService.GetTSFRemittanceByClaimId(id.Value);
                    if (model == null) //wrong id, claim doesn't exist.
                    {
                        return RedirectToAction("EmptyPage", "Common", new { area = "System" });
                    }
                    if ((SecurityContextHelper.CurrentVendor != null) && (model.CustomerID != SecurityContextHelper.CurrentVendor.VendorId || SecurityContextHelper.CurrentVendor.IsVendor))
                    {
                        if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                        {
                            return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                        }
                        else
                        {
                            return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                        }
                    }
                    model.IsDeleteVisible = (id > 0) ? IsDeletable(id.Value) : false;//id == 0 means during create new TFSRemittance, and before use date time picker to select a period, so don't show delete button, because TFSRemittance is not created yet
                    model.StatusName = model.StatusName != null && model.StatusName != string.Empty ? model.StatusName : "None";
                    ClaimStatus status = (ClaimStatus)Enum.Parse(typeof(ClaimStatus), model.StatusName.Replace(" ", string.Empty), true);
                    model.Status = status;
                    ViewBag.assignToMeCompanyName = model.BusinessName;
                    ViewBag.RegNo = model.RegistrationNumber;
                    ViewBag.IsCreatedByStaff = model.IsCreatedByStaff; //OTSTM2-56
                    ViewBag.ngApp = "tsfStaffClaimLib"; //OTSTM2-485
                    //OTSTM2-553 only staffindex page for staff has activity panel (can get real time activity)
                    if (SecurityContextHelper.IsStaff())
                    {
                        ViewBag.HasRemittanceActivityPanel = 1;
                    }
                    ViewBag.TsfClaimsId = id;

                    if ((ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RemittanceTSFRemittance) == SecurityResultType.EditSave))//this is level 1 permission, means all sub item(level 2) has EditSave permission then: ViewBag.AllowEdit = true; 
                        ViewBag.AllowEdit = true;
                    else
                        ViewBag.AllowEdit = false;
                    DateTime dt = Convert.ToDateTime(model.PeriodDate);
                    if (model.SRemitsPerYears == false)
                    {
                        if (model.PeriodDate >= Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFMonthYearSelectionDate").Value))
                        {
                            if (!CheckDate(model.PeriodDate))
                            {
                                var TSFRateDateList = remittanceService.GetTSFRateDateList(dt);
                                model.TSFRateDateList = new SelectList(TSFRateDateList, "ShortName", "ShortName");
                            }
                        }

                    }
                    var ratelist = remittanceService.GetRateList(dt);
                    var rateListFiltered = from c in ratelist where !(from o in model.LineItems where o.Quantity > 0 select o.CategoryId).Contains(c.ID) select c;
                    var creditRateListFiltered = from c in ratelist where !(from o in model.LineItems where o.NegativeAdjustment > 0 select o.CategoryId).Contains(c.ID) select c;
                    model.Tires = new SelectList(rateListFiltered, "Id", "Name");
                    model.CreditTires = new SelectList(creditRateListFiltered, "Id", "Name");

                    //Bind Adj.Reason
                    model.AdjustmentReasonTypeList = GetReasonlist();
                    if (id != null && id > 0)
                    {
                        if (model.SupportingDocuments == null)
                        {
                            model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Claims };
                        }
                        model.SupportingDocuments.ID = model.ID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetAttachments(model.ID).Count();
                        model.SupportingDocuments.ParticipantType = "Steward";
                        //model.SupportingDocuments.Status = model.Status;
                        model.SupportingDocuments.negativeAdj = model.SupportingDocOption;
                    }

                    ApplyUserSecurity();

                    model.PayReceiptDate = ConvertTimeFromUtc(model.PayReceiptDate);
                    model.PayDepositDate = ConvertTimeFromUtc(model.PayDepositDate);
                    model.RemitsPerYearSwitchDate = ConvertTimeFromUtc(model.RemitsPerYearSwitchDate);
                    model.ApprovedDate = ConvertTimeFromUtc(model.ApprovedDate);
                    model.ChequeDue = ConvertTimeFromUtc(model.ChequeDue);
                    //OTSTM2-1110 comment out
                    //model.Onhold = ConvertTimeFromUtc(model.Onhold);
                    //model.Offhold = ConvertTimeFromUtc(model.Offhold);
                    model.Assigned = ConvertTimeFromUtc(model.Assigned);
                    model.StartedDate = ConvertTimeFromUtc(model.StartedDate);
                    //model.ActivityApprovedDate = ConvertTimeFromUtc(model.ActivityApprovedDate);
                    model.SubmittedDate = ConvertTimeFromUtc(model.SubmittedDate);
                    if (!model.IsPenaltyOverride && model.PeriodDate < new DateTime(2016, 7, 1) && model.Penalties > 0)
                    {
                        model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1})", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
                    }
                    else if (!model.IsPenaltyOverride && model.PeriodDate >= new DateTime(2016, 7, 1) && model.Penalties > 0 && model.Credit < model.TotalRemittancePayable)
                    {
                        model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1}) less Credit.", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
                    }

                    if (!string.IsNullOrWhiteSpace(model.RegistrationNumber) && model.CustomerID > 0)
                        ParticipantPageHelper.SetCurrentVendor(registrantService, model.CustomerID);

                    return View(model);
                }
                else
                {
                    int customerID = 0;
                    if (id == null) { id = 0; }

                    var vendor = SecurityContextHelper.CurrentVendor;
                    if (SecurityContextHelper.CurrentVendor != null)
                    {
                        vendor = SecurityContextHelper.CurrentVendor;
                        customerID = vendor.VendorId;
                    }

                    string periodStartDate = "2009-09-01";
                    periodStartDate = GetPeriodStartDate(periodStartDate);
                    ViewBag.PeriodStartDate = periodStartDate;

                    var model = remittanceService.GetTSFRemittanceById(id.Value, customerID);
                    DateTime dt = model.PeriodDate.HasValue ? model.PeriodDate.Value : DateTime.Now;
                    var ratelist = remittanceService.GetRateList(dt);
                    model.Tires = new SelectList(ratelist, "Id", "Name");
                    //Bind Adj.Reason
                    model.AdjustmentReasonTypeList = GetReasonlist();

                    ApplyUserSecurity(TreadMarksConstants.RemittanceTSFRemittance, model.Status, model);

                    model.PayReceiptDate = ConvertTimeFromUtc(model.PayReceiptDate);
                    model.PayDepositDate = ConvertTimeFromUtc(model.PayDepositDate);
                    model.RemitsPerYearSwitchDate = ConvertTimeFromUtc(model.RemitsPerYearSwitchDate);
                    model.ApprovedDate = ConvertTimeFromUtc(model.ApprovedDate);
                    model.ChequeDue = ConvertTimeFromUtc(model.ChequeDue);
                    //OTSTM2-1110 comment out
                    //model.Onhold = ConvertTimeFromUtc(model.Onhold);
                    //model.Offhold = ConvertTimeFromUtc(model.Offhold);
                    model.Assigned = ConvertTimeFromUtc(model.Assigned);
                    model.StartedDate = ConvertTimeFromUtc(model.StartedDate);
                    //model.ActivityApprovedDate = ConvertTimeFromUtc(model.ActivityApprovedDate);
                    model.SubmittedDate = ConvertTimeFromUtc(model.SubmittedDate);

                    return View(model);

                }
            }
            return RedirectToAction("EmptyPage", "Common", new { area = "System" });
        }

        //OTSTM2-485 Payment Panel reload
        [HttpGet]
        public ActionResult GetTSFRemittanceModel(int claimId)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(claimId);
            model.StatusName = model.StatusName != null && model.StatusName != string.Empty ? model.StatusName : "None";
            ClaimStatus status = (ClaimStatus)Enum.Parse(typeof(ClaimStatus), model.StatusName.Replace(" ", string.Empty), true);
            model.Status = status;

            //Fixed OTSTM2-955
            var securityResult = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RemittanceTSFRemittance);
            if (securityResult == SecurityResultType.EditSave || securityResult == SecurityResultType.Custom)
            {
                ViewBag.AllowEdit = true;
            }
            else
            {
                ViewBag.AllowEdit = false;
            }

            if (!model.IsPenaltyOverride && model.PeriodDate < new DateTime(2016, 7, 1) && model.Penalties > 0)
            {
                model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1})", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
            }
            else if (!model.IsPenaltyOverride && model.PeriodDate >= new DateTime(2016, 7, 1) && model.Penalties > 0 && model.Credit < model.TotalRemittancePayable)
            {
                model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1}) less Credit.", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
            }
            ViewBag.IsCreatedByStaff = model.IsCreatedByStaff;
            return PartialView("_TSFRemittanceStaffPayment", model);
        }

        [HttpGet]
        public JsonResult LoadCategoryByDate(int claimId, string date)
        {
            DateTime dt = Convert.ToDateTime(date);

            var ratelist = remittanceService.GetRateList(dt);
            var model = remittanceService.GetTSFRemittanceByClaimId(claimId);
            var rateListFiltered = from c in ratelist where !(from o in model.LineItems where ((o.CreditNegativeAdj > 0) && (o.NegativeAdjDate == dt)) select o.CategoryId).Contains(c.ID) select c;
            var CreditTires = new SelectList(rateListFiltered, "Id", "Name");
            return Json(CreditTires, JsonRequestBehavior.AllowGet);
        }
        private string GetPeriodStartDate(string periodStartDate)
        {
            if (SiteSettings.Instance.GetSettingValue("Settings.AllowAllUser") != null)
            {
                if (SiteSettings.Instance.GetSettingValue("Settings.AllowAllUser") == "false")
                {
                    if (SecurityContextHelper.CurrentVendor.CreatedDate < Convert.ToDateTime(SiteSettings.Instance.GetSettingValue("Settings.UserDate")))
                    {
                        DateTime dt = Convert.ToDateTime(SiteSettings.Instance.GetSettingValue("Settings.UserDate"));
                        periodStartDate = dt.Year + "-" + dt.Month + "-1";
                    }
                }
            }
            return periodStartDate;
        }

        public ActionResult GetClaimDetail(int claimID, int detId)
        {
            var model = remittanceService.GetRemittanceDetail(claimID, detId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DelClaimDetail(int claimID, int detailId)
        {
            remittanceService.DeleteRemittanceDetail(claimID, detailId);
            return RedirectToAction("Index");
        }

        //OTSTM2-158 server side check, shared by participant create/submit remittance and staff create remittance
        [HttpPost]
        public JsonResult RunCreateOrSubmitRules(DateTime selectedPeriodDate, int customerID)
        {
            var customer = remittanceService.GetCustomerByCustomerId(customerID);
            var activityApprovedDate=customer.ActiveStateChangeDate;
            if (!customer.IsActive)
            {
                if (!activityApprovedDate.HasValue ||  selectedPeriodDate.Year > activityApprovedDate.Value.Year ||   
                        (selectedPeriodDate.Year == activityApprovedDate.Value.Year && selectedPeriodDate.Month > activityApprovedDate.Value.Month)                        
                    )
                {
                    return Json(new { canCreateOrSubmit = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { canCreateOrSubmit = true }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (selectedPeriodDate.Date > DateTime.Today.Date)
                {
                    return Json(new { canCreateOrSubmit = false, reason = "Future Remittance can not be created. Please select a valid date." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { canCreateOrSubmit = true }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [ClaimsAuthorization(TreadMarksConstants.RemittanceTSFRemittance)]
        public ActionResult ParticipantIndex(int? id)
        {
            //Fixed vendor context invalid exception 
            if ((SecurityContextHelper.CurrentVendor != null) && (SecurityContextHelper.CurrentVendor.IsVendor))
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            if (string.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                ApplyUserSecurity();
                return RedirectToAction("Login", "Account", new { area = "", ReturnUrl = "Steward/TSFRemittances/StaffIndex/=" + id });
            }

            string periodStartDate = "2009-09-01";
            periodStartDate = GetPeriodStartDate(periodStartDate);
            ViewBag.PeriodStartDate = periodStartDate;

            if (SecurityContextHelper.CurrentUser != null)
            {
                ViewBag.IsActive = (SecurityContextHelper.CurrentVendor == null) ? false : SecurityContextHelper.CurrentVendor.IsActive;
                string sArea = string.Empty;
                if (id.HasValue && id != null)
                {
                    //var customerID = 1;
                    //var model = remittanceService.GetTSFRemittanceById(id.Value, customerID);
                    var model = remittanceService.GetTSFRemittanceByClaimId(id.Value);
                    if (model.RegistrationNumber != SecurityContextHelper.CurrentVendor.RegistrationNumber)
                    {
                        //Redirect to unauthorized page
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                    }
                    model.StatusName = model.StatusName != null && model.StatusName != string.Empty ? model.StatusName : "None";
                    ClaimStatus status = (ClaimStatus)Enum.Parse(typeof(ClaimStatus), model.StatusName.Replace(" ", string.Empty), true);
                    model.Status = status;
                    ViewBag.ngApp = "tsfParticipantClaimLib"; //OTSTM2-553

                    ////Permission for submit
                    ApplyParticipantUserSecurity();

                    ViewBag.IsCreatedByStaff = model.IsCreatedByStaff; //OTSTM2-56
                    DateTime dt = Convert.ToDateTime(model.PeriodDate);
                    if (model.SRemitsPerYears == false)
                    {
                        if (model.PeriodDate >= Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFMonthYearSelectionDate").Value))
                        {
                            if (!CheckDate(model.PeriodDate))
                            {
                                var TSFRateDateList = remittanceService.GetTSFRateDateList(dt);
                                model.TSFRateDateList = new SelectList(TSFRateDateList, "ShortName", "ShortName");
                            }
                        }

                    }
                    var ratelist = remittanceService.GetRateList(dt);
                    var rateListFiltered = from c in ratelist where !(from o in model.LineItems where o.Quantity > 0 select o.CategoryId).Contains(c.ID) select c;
                    var creditRateListFiltered = from c in ratelist where !(from o in model.LineItems where o.NegativeAdjustment > 0 select o.CategoryId).Contains(c.ID) select c;

                    model.Tires = new SelectList(rateListFiltered, "Id", "Name");
                    model.CreditTires = new SelectList(creditRateListFiltered, "Id", "Name");

                    if (model.LineItems != null)
                        model.LineItems = model.LineItems.OrderBy(x => x.CategoryId).ToList();
                    //Bind Adj.Reason
                    model.AdjustmentReasonTypeList = GetReasonlist();
                    if (id != null && id > 0)
                    {
                        if (model.SupportingDocuments == null)
                        {
                            model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Claims };
                        }
                        model.SupportingDocuments.ID = model.ID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetAttachments(model.ID).Count();
                        model.SupportingDocuments.ParticipantType = "Steward";
                        //model.SupportingDocuments.Status = model.Status;
                        model.SupportingDocuments.negativeAdj = model.SupportingDocOption;
                    }
                    if (!model.IsPenaltyOverride && model.PeriodDate < new DateTime(2016, 7, 1) && model.Penalties > 0)
                    {
                        model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1})", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
                    }
                    else if (!model.IsPenaltyOverride && model.PeriodDate >= new DateTime(2016, 7, 1) && model.Penalties > 0 && model.Credit < model.TotalRemittancePayable)
                    {
                        model.PenaltyTooltipText = string.Format("Penalty is equal to {0}% of Tire Counts Total (${1}) less Credit.", String.Format("{0:.##}", model.PenaltyRate), String.Format("{0:.00}", model.TotalRemittancePayable));
                    }

                    return View(model);
                }
                else
                {
                    if (id == null) { id = 0; }
                    var customerID = SecurityContextHelper.CurrentVendor.VendorId;
                    var model = remittanceService.GetTSFRemittanceById(id.Value, customerID);
                    DateTime dt = model.PeriodDate.HasValue ? model.PeriodDate.Value : DateTime.Now;
                    var ratelist = remittanceService.GetRateList(dt);
                    model.Tires = new SelectList(ratelist, "Id", "Name");
                    //Bind Adj.Reason
                    model.AdjustmentReasonTypeList = GetReasonlist();
                    return View(model);
                }
            }

            return RedirectToAction("EmptyPage", "Common", new { area = "System" });
        }

        [HttpPost]
        public ActionResult AddRemittance(TSFRemittanceModel model)
        {
            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                model.TSFRemittance.CreatedBy = SecurityContextHelper.CurrentUser.FirstName;
                model.TSFRemittance.ModifiedBy = SecurityContextHelper.CurrentUser.FirstName;
                model.SubmittedBy = SecurityContextHelper.CurrentUser.FirstName;

                //calculation for Tire Counts
                var rate = model.TSFRemittance.Rate;
                //OTSTM2-567
                if (rate == 0 && model.TSFRemittance.Quantity != 0)
                {
                    if (model.TSFRemittance.TSFRateDate != null)
                    {
                        rate = decimal.Round(remittanceService.GetRate((DateTime)model.TSFRemittance.TSFRateDate, model.TSFRemittance.CategoryId).ItemRate, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        rate = decimal.Round(remittanceService.GetRate((DateTime)model.PeriodDate, model.TSFRemittance.CategoryId).ItemRate, 2, MidpointRounding.AwayFromZero);
                    }
                }
                model.TSFRemittance.Rate = rate;
                var quantity = model.TSFRemittance.Quantity;
                var negativeAdj = model.TSFRemittance.NegativeAdjustment;
                var net = quantity - negativeAdj;
                if (model.PeriodDate < Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value))
                {
                    model.TSFRemittance.Net = net;
                    model.TSFRemittance.Total = net * rate;
                }
                else
                {
                    model.TSFRemittance.Total = quantity * rate;

                    //calculation for credit panel
                    var creditRate = model.TSFRemittance.CreditTSFRate;
                    //OTSTM2-567
                    if (creditRate == 0 && model.TSFRemittance.CreditNegativeAdj != 0)
                    {
                        creditRate = decimal.Round(remittanceService.GetRate((DateTime)model.TSFRemittance.NegativeAdjDate, model.TSFRemittance.CategoryId).ItemRate, 2, MidpointRounding.AwayFromZero);
                    }
                    model.TSFRemittance.CreditTSFRate = creditRate;
                    var creditNegAdj = model.TSFRemittance.CreditNegativeAdj;
                    model.TSFRemittance.CreditTSFDue = creditNegAdj * creditRate;

                }

                remittanceService.AddRemittanceDetail(model.TSFRemittance);

                if ((SecurityContextHelper.IsStaff()))//or is participant user
                {
                    return RedirectToAction("StaffIndex", new { id = model.TSFRemittance.ClaimID });
                }
                return RedirectToAction("ParticipantIndex", new { id = model.TSFRemittance.ClaimID });
            }
            return RedirectToAction("ParticipantIndex", new { id = model.TSFRemittance.ClaimID });
        }

        [HttpPost]
        public JsonResult IsDeleteVisible(int claimID)
        {
            //criteria: No data, not approved, valid permission
            var result = IsDeletable(claimID);
            return Json(new { result = result, status = true }, JsonRequestBehavior.AllowGet);
        }
        bool IsDeletable(int tsfClaimId)
        {
            return !remittanceService.HasData(tsfClaimId) && remittanceService.GetTSFRemittanceByClaimId(tsfClaimId).RecordState != "Approved" && (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RemittanceTSFRemittanceStatus) > CL.TMS.Security.SecurityResultType.ReadOnly);//need write permission on TSFRemittanceStatus
        }

        [HttpPost]
        public JsonResult DeleteTSFClaim(int claimID, bool bTestData = false)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(claimID);
            var result = IsDeletable(claimID) && remittanceService.RemoveTSFClaim(claimID);
            if (result)
            {
                string logMessage = string.Format("{0} deleted this remittance: Reg. # {1}, Period: {2}, TSFClaimId: {3}.", SecurityContextHelper.CurrentUser.FullName, model.RegistrationNumber, model.PayPeriod, claimID);
                LogManager.LogInfo(logMessage);
            }
            return Json(new { result = result, status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdatePaymentType(int claimID, string paymentType)
        {

            var result = remittanceService.UpdatePaymentType(claimID, paymentType, SecurityContextHelper.CurrentUser.UserName);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult UpdateCurrencyType(int claimID, string currencyType)
        {

            var result = remittanceService.UpdateCurrencyType(claimID, currencyType, SecurityContextHelper.CurrentUser.UserName);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult UpdatePenaltiesManually(int claimID, double penaltiesManually, bool? IsPenaltyOverride = false)
        {

            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                string modifiedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName;

                remittanceService.UpdatePenaltiesManually(claimID, penaltiesManually, modifiedBy, IsPenaltyOverride);

                if ((SecurityContextHelper.IsStaff()))//or is participant user
                {
                    return RedirectToAction("GetTSFRemittanceModel", new { claimId = claimID });
                }
                return RedirectToAction("ParticipantIndex", new { id = claimID });
            }
            return RedirectToAction("ParticipantIndex", new { id = claimID });
        }
        [HttpPost]
        public JsonResult UpdatemPenaltiesManuallyWithReturn(int claimID, double penaltiesManually, bool? IsPenaltyOverride = false)
        {

            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                string modifiedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName;

                remittanceService.UpdatePenaltiesManually(claimID, penaltiesManually, modifiedBy, IsPenaltyOverride);

                var model = remittanceService.GetTSFRemittanceByClaimId(claimID);
                var json = new
                {
                    TotalTSFDue = model.TotalTSFDue,
                    BalanceDue = model.BalanceDue,
                };
                if ((SecurityContextHelper.IsStaff()))//or is participant user
                {
                    return Json(json, JsonRequestBehavior.AllowGet);
                }
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateHST(int tsfClaimId, bool IsTaxApplicable = true)
        {

            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                string modifiedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName;

                remittanceService.UpdateHST(tsfClaimId, modifiedBy, IsTaxApplicable);

                if ((SecurityContextHelper.IsStaff()))//or is participant user
                {
                    return RedirectToAction("StaffIndex", new { id = tsfClaimId });
                }
                return RedirectToAction("ParticipantIndex", new { id = tsfClaimId });
            }
            return RedirectToAction("ParticipantIndex", new { id = tsfClaimId });
        }
        public ActionResult UpdateSupportingDocumentValue(int claimID, string docOptionValue)
        {

            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                string modifiedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName;

                remittanceService.UpdateSupportingDocumentValue(claimID, docOptionValue, modifiedBy);
            }
            return Json(new { success = true });
        }

        private void ApplyUserSecurity(string Resource = TreadMarksConstants.RemittanceTSFRemittance, ClaimStatus status = ClaimStatus.Unknown, TSFRemittanceModel TSFClaimSummary = null)
        {
            if (SecurityContextHelper.IsStaff())
            {
                //Fixed OTSTM2-955
                var isEditable = false;
                var securityResult = ResourceActionAuthorizationHandler.CheckUserSecurity(Resource);
                if (securityResult == SecurityResultType.EditSave || securityResult == SecurityResultType.Custom)
                {
                    isEditable = true;
                }

                ViewBag.AllowAddNew = ViewBag.AllowEdit = ViewBag.AllowDelete = isEditable;
                if (null != TSFClaimSummary)
                {
                    if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                    {
                        TSFClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                        TSFClaimSummary.ClaimStatus.IsClaimReadonly = true;
                        TSFClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    else
                    {
                        TSFClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;

                        if (!isEditable)
                        {
                            TSFClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                            TSFClaimSummary.ClaimStatus.IsClaimReadonly = true;
                        }
                    }
                }
            }
            else
            {
                ApplyParticipantUserSecurity();
            }
        }

        public JsonResult LoadTSFRemittancesPeriodBreadCrumb(int CustomerId)
        {
            var result = remittanceService.GetClaimPeriodForBreadCrumb(CustomerId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTSFPeriods()
        {
            var periodTypeID = (int)ClaimType.Steward;
            var result = remittanceService.GetClaimPeriods(periodTypeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region workflow
        public JsonResult GetListOfApplications(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"] != null ? Convert.ToInt16(Request["order[0][column]"]) : 0);
            var orderBy = Request[string.Format("columns[{0}][name]", sortColumnIndex)] != null ? Convert.ToString(Request[string.Format("columns[{0}][name]", sortColumnIndex)]) : "ApplicationSubmitDate";
            var sortDirection = Convert.ToString(Request["order[0][dir]"] != null ? Request["order[0][dir]"] : "asc");

            //OTSTM2-929
            var columnSearchText = new Dictionary<string, string>
            {
                {"RegNumber", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                {"Company", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                {"ReportingFrequency", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                {"Period", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                {"Submitted",Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                {"DepositDate", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                {"TotalReceivable", Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
                {"ChequeAmount", Request.QueryString.GetValues("columns[7][search][value]").FirstOrDefault()},
                {"BalanceDue", Request.QueryString.GetValues("columns[8][search][value]").FirstOrDefault()},
                {"Status", Request.QueryString.GetValues("columns[9][search][value]").FirstOrDefault()},
                {"AssignedTo",Request.QueryString.GetValues("columns[10][search][value]").FirstOrDefault()},
            };

            var alluserApplications = remittanceService.GetAllITSFRemittanceByFilter(param.Start, param.Length, searchText, orderBy, sortDirection, columnSearchText);

            //OTSTM2-929 comment out, because Submitted is converted to local time in Repository layer
            //alluserApplications.DTOCollection.ForEach(c => c.Submitted = ConvertTimeFromUtc(c.Submitted));

            var json = new
            {
                sEcho = param.Draw,
                iTotalRecords = alluserApplications.TotalRecords,
                iTotalDisplayRecords = alluserApplications.TotalRecords,
                aaData = alluserApplications.DTOCollection,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListOfApplicationsByCustomer(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"] != null ? Convert.ToInt16(Request["order[0][column]"]) : 0);
            var orderBy = Request[string.Format("columns[{0}][name]", sortColumnIndex)] != null ? Convert.ToString(Request[string.Format("columns[{0}][name]", sortColumnIndex)]) : "ApplicationSubmitDate";
            var sortDirection = Convert.ToString(Request["order[0][dir]"] != null ? Request["order[0][dir]"] : "asc");
            string RegistrationNumber = string.Empty;

            //OTSTM2-984
            var columnSearchText = new Dictionary<string, string>
            {
                {"Period", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                {"Status", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                { "TotalReceivable", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                {"Due", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},              
                {"Payment", Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                {"BalanceDue",Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                {"Submitted", Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
                {"SubmittedBy", Request.QueryString.GetValues("columns[7][search][value]").FirstOrDefault()},
            };

            if (SecurityContextHelper.CurrentVendor != null)
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }

            var alluserApplications = remittanceService.GetAllITSFRemittanceByFilterByCustomer(RegistrationNumber, param.Start, param.Length, searchText, orderBy, sortDirection, columnSearchText);

            //OTSTM2-984 comment out, because Submitted is converted to local time in Repository layer
            //alluserApplications.DTOCollection.ForEach(c => c.Submitted = ConvertTimeFromUtc(c.Submitted));

            var json = new
            {
                sEcho = param.Draw,
                iTotalRecords = alluserApplications.TotalRecords,
                iTotalDisplayRecords = alluserApplications.TotalRecords,
                aaData = alluserApplications.DTOCollection,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTireListForEdit(string PeriodDate)
        {
            DateTime dt = Convert.ToDateTime(PeriodDate);
            var ratelist = remittanceService.GetRateList(dt).ToList();
            var myResult = new
            {
                Tirelist = ratelist
            };
            return Json(myResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadTireListBasedOnTSFRateDate(string PeriodDate, int ClaimID)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(ClaimID);
            DateTime dt = Convert.ToDateTime(PeriodDate);
            var ratelist = remittanceService.GetRateList(dt).ToList();
            var rateListFiltered = from c in ratelist where !(from o in model.LineItems where o.TSFRateDate == dt select o.CategoryId).Contains(c.ID) select c;
            var myResult = new
            {
                Tirelist = rateListFiltered
            };
            return Json(myResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTireListOnPeriodBasis(TSFRemittanceModel model)
        {
            DateTime dt;
            if (!model.PeriodDate.HasValue || !DateTime.TryParse(model.PeriodDate.Value.ToString(),out dt)) {
                return Json(new { canCreateOrSubmit = false, reason = "Please selected a valid date." }, JsonRequestBehavior.AllowGet);
            }
            if (model.PeriodDate.Value.Date > DateTime.Today.Date)
            {
                return Json(new { canCreateOrSubmit = false, reason = "Future Remittance can not be created. Please select a valid date." }, JsonRequestBehavior.AllowGet);
            }

            //OTSTM2-21
            DateTime dtBefore = Convert.ToDateTime("2016-01-01");    
            var customer = remittanceService.GetCustomerByCustomerId(model.CustomerID);
            if (dt >= dtBefore)
            {
                if (customer.CustomerSettingHistoryList.Count > 0)
                {
                    var setting = customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == dt.Year).LastOrDefault();
                    if (setting != null)
                    {
                        if (setting.NewValue == "2x")
                        {
                            if (CheckDate(model.PeriodDate))
                            {
                                var result = new
                                {
                                    ID = -1,
                                    Year = dt.Year
                                };
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        //Frank confirm is using dt.addYears(1).year to find the oldsetting.
                        var oldSetting = customer.CustomerSettingHistoryList.Where(c => c.ChangeDate.Year == dt.AddYears(1).Year).FirstOrDefault();
                        if (oldSetting != null)
                        {
                            if (oldSetting.OldValue == "2x")
                            {
                                if (CheckDate(model.PeriodDate))
                                {
                                    var result = new
                                    {
                                        ID = -1,
                                        Year = dt.Year
                                    };
                                    return Json(result, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }

                }
                else //no data in new table, using the current logic, actually, this case(no data) only for 2016, before 2016, 12x, after 2016, 12x
                {
                    if (model.SRemitsPerYears == false)
                    {
                        if (dt.Year == DateTime.UtcNow.Year)
                        {
                            if (CheckDate(model.PeriodDate))
                            {
                                var result = new
                                {
                                    ID = -1,
                                    Year = dt.Year
                                };
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                }
            }

            var Tirelist = remittanceService.GetRateList(dt).ToList();

            if ((SecurityContextHelper.IsStaff()))//or is participant user
            {
                model.Istaff = true;
                model.SubmittedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;
                model.SubmittedByUserId = SecurityContextHelper.CurrentUser.Id;
            }
            else
            {
                model.Istaff = false;
            }
            bool isCreated = false; ;
            int TsfClaimID = remittanceService.AddTSFClaim(model, ref isCreated);
            if (isCreated)
            {
                //OTSTM2-553 Add Activity for staff created remittance (Status will
                //be Under View)
                AddActivity(TsfClaimID, ActivityTypeEnum.StaffSubmitted);
                //Publish activity event
                DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(TsfClaimID);
            }
            model.TSFRemittance.ClaimID = TsfClaimID;           
            model.Tires = new SelectList(Tirelist, "Id", "Name");
            var myResult = new
            {
                ID = TsfClaimID
            };
            return Json(myResult, JsonRequestBehavior.AllowGet);
        }

        private void AddActivity(int TsfClaimID, ActivityTypeEnum ActivityType, int InternalAdjustId = 0, string initiator = null, string initialName = null)
        {
            var activity = new Activity();
            activity.Initiator = SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = SecurityContextHelper.CurrentUser.UserName;
            activity.AssigneeName = SecurityContextHelper.CurrentUser.UserName;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.TsfClaimActivity;
            activity.ActivityArea = TreadMarksConstants.StewardType;
            activity.ObjectId = TsfClaimID;

            switch ((int)ActivityType)
            {
                case (int)ActivityTypeEnum.StaffSubmitted:
                    activity.Message = string.Format("{0} <strong>submitted</strong> the remittance.", activity.InitiatorName);
                    break;
                case (int)ActivityTypeEnum.AutoApproved:
                    //OTSTM2-993 Activity log on approval of Nil Remittances using 'System Admin' as initiator name
                    activity.InitiatorName = initialName;
                    activity.Initiator = initiator;
                    if (initiator.IndexOf("Admin") > -1)
                    {
                        activity.Message = string.Format("{0} <strong>approved</strong> this remittance.", initiator);
                    }
                    else
                    {
                        activity.Message = string.Format("{0} <strong>submitted</strong> the remittance.", initiator);
                    }
                    break;
                case (int)ActivityTypeEnum.BackToUnderReview:
                    activity.Message = string.Format("{0} sent this remittance <strong>back</strong> to <strong>{1}</strong>.", activity.InitiatorName, activity.AssigneeName);
                    break;
                case (int)ActivityTypeEnum.IntAdjRemoved:
                    var intAdj = remittanceService.GetClaimAdjustmentModalResult((int)TSFInternalAdjustmentType.Payment, InternalAdjustId);
                    activity.Message = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of <strong>{4}</strong>.",
                           SecurityContextHelper.CurrentUser.FullName,
                           intAdj.AdjustUser.FirstName + " " + intAdj.AdjustUser.LastName,
                           "Payment",
                           EnumHelper.GetEnumDescription(EnumHelper.ToEnum<TSFInternalAdjustmentPaymentType>(intAdj.PaymentType)).ToString(),
                           string.Format("{0:$#,##0.00}", intAdj.Amount));
                    break;
            }
            this.messageService.AddActivity(activity);
        }

        private bool CheckDate(DateTime? periodDate)
        {
            DateTime date = Convert.ToDateTime(periodDate);
            if (date.Month == 6 || date.Month == 12)
                return false;
            else
                return true;
        }

        public JsonResult RateByItemId(string PeriodDate, int ItemID)
        {
            DateTime dateValue, dt = new DateTime();
            //IReadOnlyList<RateListModel> rate;
            if (DateTime.TryParse(PeriodDate, out dateValue))
                dt = Convert.ToDateTime(PeriodDate);
            //rate = remittanceService.GetRateList(dt, ItemID);
            var rate = remittanceService.GetRate(dt, ItemID);
            return Json(rate, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsFirstTimePenalty(int CustomerID, int ClaimID)
        {
            //System.Diagnostics.Debug.
            // return false for legacy stewards and for 2009 remittances (handled in jquery)
            var customer = registrantService.GetVendorById(CustomerID);
            var isCreatedinNTM = (customer.CreatedDate.Value.Date >= Convert.ToDateTime(SiteSettings.Instance.GetSettingValue("Settings.UserDate")).Date);

            var isFirstTimePenalty = isCreatedinNTM && !remittanceService.IsAnyPastPenalty(CustomerID, ClaimID);

            if (isFirstTimePenalty)
            {
                AddWarningNote(ClaimID, TreadMarksConstants.FirstTimePenalty);
            }

            return Json(isFirstTimePenalty, JsonRequestBehavior.AllowGet);
        }


        private List<SelectListItem> GetReasonlist()
        {
            var reason = AppDefinitions.Instance.TypeDefinitions["AdjustmentReasonType"].ToList();
            var Reasonlist = new List<SelectListItem>();
            foreach (var data in reason)
            {
                Reasonlist.Add(new SelectListItem
                {
                    Value = Convert.ToString(data.DefinitionValue),
                    Text = data.Description
                });
            }

            return Reasonlist;
        }

        public ActionResult ExportToExcelForAssignment(string searchtext)
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];
            if (string.IsNullOrWhiteSpace(sortcolumn))
            {
                sortcolumn = "Submitted";
                sortdirection = "desc";
            }

            //OTSTM2-929
            var columnSearchText = new Dictionary<string, string>
            {
                {"RegNumber", Request.QueryString[3]},
                {"Company", Request.QueryString[4]},
                {"ReportingFrequency", Request.QueryString[5]},
                {"Period", Request.QueryString[6]},
                {"Submitted",Request.QueryString[7]},
                {"DepositDate", Request.QueryString[8]},
                {"TotalReceivable", Request.QueryString[9]},
                {"ChequeAmount", Request.QueryString[10]},
                {"BalanceDue", Request.QueryString[11]},
                {"Status", Request.QueryString[12]},
                {"AssignedTo",Request.QueryString[13]},
            };

            var applications = remittanceService.LoadRemittanceByRegNo(searchtext, sortcolumn, sortdirection, null, columnSearchText);
            var columnNames = new List<string>(){ //OTSTM2-188
                "RegNo", "Company", "ReportingFrequency", "Period", "Submitted","DepositDate","Total TSF Due($)","Payment($)","BalanceDue($)","Status", "AssignedTo"
            };

            var delegates = new List<Func<TSFRemittanceBriefModel, string>>()
            {
                u =>u.RegNo,
                u=> u.Company.ToString(),
                u=> u.ReportingFrequency.ToString(), //OTSTM2-188
                u => u.Period.ToString(),
                u=>u.Submitted==null?string.Empty:((DateTime)u.Submitted).ToString(TreadMarksConstants.DateFormat),
                u=>u.DepositDate==null?string.Empty:((DateTime)u.DepositDate).ToString(TreadMarksConstants.DateFormat), //OTSTM2-188
                u=>u.TotalReceivable.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-188
                u=>u.ChequeAmount.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-188
                u=>u.BalanceDue.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-188
                u=>u.Status.ToString(),
                 u=>u.AssignedTo.ToString(),

            };

            //OTSTM2-929 comment out, because Submitted is converted to local time in Repository layer, to be consistent with listing, DepositDate won't be converted
            //applications.ForEach(c =>
            //{
            //    c.Submitted = ConvertTimeFromUtc(c.Submitted);
            //    c.DepositDate = ConvertTimeFromUtc(c.DepositDate); //OTSTM2-188
            //});
            var csv = applications.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittanceAssignment-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittanceAssignment/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForTireCounts(int id)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(id);
            var columnNames = new List<string>();
            var delegates = new List<Func<TSFRemittanceItemModel, string>>();
            if (model.PeriodDate < Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFMonthYearSelectionDate").Value))
            {
                columnNames = new List<string>(){
                    "Class/Category", "Qty.", "Negative Adj.", "Net","Rate ($)", "Total ($)"
                };

                delegates = new List<Func<TSFRemittanceItemModel, string>>()
                {
                    u =>u.Category,
                    u=> u.Quantity.ToString(),
                    u => u.NegativeAdjustment.ToString(),
                    u=>u.Net.ToString(),
                    u=> u.Rate.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                    u=> u.Total.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423

                };
            }
            else if (model.PeriodDate >= Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFMonthYearSelectionDate").Value) && model.PeriodDate < Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value))
            {
                if (model.SRemitsPerYears == false)
                {
                    columnNames = new List<string>(){
                        "Month/Year","Class/Category", "Qty.", "Negative Adj.", "Net","Rate ($)", "Total ($)"
                    };

                    delegates = new List<Func<TSFRemittanceItemModel, string>>()
                    {
                        u =>u.TSFRateDateShortName,
                        u =>u.Category,
                        u =>u.Quantity.ToString(),
                        u =>u.NegativeAdjustment.ToString(),
                        u=>u.Net.ToString(),
                        u=> u.Rate.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                        u=> u.Total.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                    };
                }
                else
                {
                    columnNames = new List<string>(){
                        "Class/Category", "Qty.", "Negative Adj.", "Net","Rate ($)", "Total ($)"
                    };

                    delegates = new List<Func<TSFRemittanceItemModel, string>>()
                    {
                        u =>u.Category,
                        u=> u.Quantity.ToString(),
                        u=>u.NegativeAdjustment.ToString(),
                        u=>u.Net.ToString(),
                        u=> u.Rate.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                        u=> u.Total.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                    };
                }
            }
            else
            {
                if (model.SRemitsPerYears == false)
                {
                    columnNames = new List<string>(){
                        "Month/Year","Class/Category", "Qty.", "Rate ($)", "Total ($)"
                    };

                    delegates = new List<Func<TSFRemittanceItemModel, string>>()
                    {
                        u =>u.TSFRateDateShortName,
                        u =>u.Category,
                        u=> u.Quantity.ToString(),
                        u=> u.Rate.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                        u=> u.Total.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                    };
                }
                else
                {
                    columnNames = new List<string>(){
                        "Class/Category", "Qty.", "Rate ($)", "Total ($)"
                    };

                    delegates = new List<Func<TSFRemittanceItemModel, string>>()
                    {
                        u =>u.Category,
                        u=> u.Quantity.ToString(),
                        u=> u.Rate.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                        u=> u.Total.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423
                    };
                }
            }

            var csv = model.LineItems.OrderBy(m => m.CategoryId).ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            if (!(model.PeriodDate < Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value)))
            {
                csv = model.LineItems.Where(i => i.Quantity > 0).OrderBy(m => m.CategoryId).ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            }
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittanceTireCounts-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittanceAssignment/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForCredit(int id)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(id);
            var columnNames = new List<string>();
            var delegates = new List<Func<TSFRemittanceItemModel, string>>();

            columnNames = new List<string>()
                {
                    "Period Month/year", "Class/Category", "Negative Adj.", "Rate ($)", "Total ($)"
                };

            delegates = new List<Func<TSFRemittanceItemModel, string>>()
                {
                    u=>u.strNegativeAdjDate,
                    u=>u.Category,
                    u=>(-1 * u.CreditNegativeAdj).ToString("#,##0.00;(#,##0.00);0.00"),
                    u=>(-1 * u.CreditTSFRate).ToString("#,##0.00;(#,##0.00);0.00"),
                    u=>(-1 * u.CreditTSFDue).ToString("#,##0.00;(#,##0.00);0.00"),
                };

            var csv = model.LineItems.Where(i => i.CreditNegativeAdj > 0).OrderBy(m => m.NegativeAdjDate).ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittanceTireCountsCredit-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittanceAssignment/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForRemittance(string searchtext)
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];
            string RegistrationNumber = string.Empty;
            if (string.IsNullOrWhiteSpace(sortcolumn))
            {
                sortcolumn = "submitted";
                sortdirection = "desc";
            }
            //if (TempData["registrationNum"] != null)
            //{
            //    RegistrationNumber = TempData["registrationNum"].ToString();
            //    TempData.Keep("registrationNum");
            //}

            //OTSTM2-984
            var columnSearchText = new Dictionary<string, string>
            {
                {"Period", Request.QueryString[3]},
                {"Status", Request.QueryString[4]},
                {"TotalReceivable", Request.QueryString[5]},
                {"Due", Request.QueryString[6]},              
                {"Payment", Request.QueryString[7]},
                {"BalanceDue",Request.QueryString[8]},
                {"Submitted", Request.QueryString[9]},
                {"SubmittedBy", Request.QueryString[10]},
            };

            if (SecurityContextHelper.CurrentVendor != null)
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }
            else if (SecurityContextHelper.IsParticipantAdmin())
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }

            var applications = remittanceService.LoadRemittanceByRegNo(searchtext, sortcolumn, sortdirection, RegistrationNumber, columnSearchText);
            var columnNames = new List<string>(){
                "Period", "Status", "Subtotal($)", "Total TSF Due($)", "Payment($)","Balance Due($)", "Submitted","Submitted By"
            };

            var delegates = new List<Func<TSFRemittanceBriefModel, string>>()
            {
                u =>u.Period,
                u=> u.Status.ToString(),
                u => u.TotalReceivable.ToString("#,##0.00;(#,##0.00);0.00"),
                u=> u.Due.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423               
                u=> u.Payments != null ? u.Payments.ToString("#,##0.00;(#,##0.00);0.00"): string.Empty, //OTSTM2-423
                u=> u.BalanceDue.ToString("#,##0.00;(#,##0.00);0.00"), //OTSTM2-423

                u=>u.Submitted==null?string.Empty:((DateTime)u.Submitted).ToString(TreadMarksConstants.DateFormat),
                 u=>u.SubmittedBy,
            };

            //OTSTM2-984 comment out, because Submitted is converted to local time in Repository layer
            //applications.ForEach(c =>
            //{
            //    c.Submitted = ConvertTimeFromUtc(c.Submitted);
            //});

            var csv = applications.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittance-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittance/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForPayments(int remittancesId)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(remittancesId);
            decimal? balanceDue = 0;
            balanceDue = (model.BalanceDue);
            model.PaymentType = (model.PaymentType == "C") ? "Cheque" : ((model.PaymentType == "E") ? "EFT" : ((model.PaymentType == "O") ? "Online" : string.Empty));
            string IsPenaltyOverride = ((model.IsPenaltyOverride) ? "Penalty Override? Yes" : "Penalty Override? No") + " +";
            string IsTaxApplicable = ((model.IsTaxApplicable) ? "HST Apply? Yes" : "HST Apply? No") + " +";
            var TSFNegAdjSwitchDate = Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value);
            bool bAfterTSFNegAdjSwitchDate = model.PeriodDate >= TSFNegAdjSwitchDate;
            decimal subtotal = (decimal)(model.TotalRemittancePayable ?? 0) - (decimal)(model.Credit ?? 0) + (decimal)(model.InternalPaymentAdjustment ?? 0);
            decimal total = subtotal + (model.ApplicableTaxesHst ?? 0) + (model.Penalties ?? 0);
    
            var columnNames = new List<string>(){
                "", "Payments ($)", ""
            };
            string[,] payments;
            if (SecurityContextHelper.IsStaff())
            {
                payments = new string[16, 3] {
                    { "Tire Counts $", ((model.TotalRemittancePayable!=null)?Convert.ToDecimal(model.TotalRemittancePayable).ToString("#,###,##0.00"):""), "+" },
                    { bAfterTSFNegAdjSwitchDate ? "Credit" : null, bAfterTSFNegAdjSwitchDate ? ((model.Credit!=null) ? Convert.ToDecimal(model.Credit * -1).ToString("#,###,##0.00"):"") : string.Empty, bAfterTSFNegAdjSwitchDate ? "+" : string.Empty },
                    { "Payment Adjustments", ((model.InternalPaymentAdjustment!=null) ? Convert.ToDecimal(model.InternalPaymentAdjustment).ToString("#,###,##0.00") : "0.00"), "="},                   
                    { "Subtotal",Convert.ToDecimal(subtotal).ToString("#,###,##0.00") ,"+"},
                    { "HST", model.IsTaxApplicable ? Convert.ToDecimal(model.ApplicableTaxesHst ?? 0).ToString("#,###,##0.00") : "N/A", IsTaxApplicable},
                    { "Penalties", ((model.Penalties!=null)?Convert.ToDecimal(model.Penalties).ToString("#,###,##0.00"):""), IsPenaltyOverride },                
                    { "Total (TSF DUE)", Convert.ToDecimal(total).ToString("#,###,##0.00"), ""},
                    { "Submitted", ((model.SubmittedDate!=null)?Convert.ToDateTime(model.SubmittedDate).ToString(TreadMarksConstants.DateFormat):""), "" },
                    { "Payment Received", ((model.PayReceiptDate!=null)?Convert.ToDateTime(model.PayReceiptDate).ToString(TreadMarksConstants.DateFormat):""), "" },
                    { "Deposit", ((model.PayDepositDate!=null)?Convert.ToDateTime(model.PayDepositDate).ToString(TreadMarksConstants.DateFormat):""), "" },
                    { "PaymentType", model.PaymentType, "" },
                    { "CurrencyType", model.CurrencyType, "" },
                    { "Cheque/EFT #", (!string.IsNullOrEmpty(model.ChequeReferenceNumber)?model.ChequeReferenceNumber:""), "" },
                    { "Payment ($)", model.PaymentAmount!=null ? String.Format( "{0:#,##0.00}", model.PaymentAmount) : "0.00", "" },
                    { "Balance Due ($)", ((balanceDue!=null)?Convert.ToDecimal(balanceDue).ToString("#,###,##0.00"):""), "" },
                    { "Batch #", model.BatchNumber.ToString(), "" },
                };
            }
            else
            {
                payments = new string[13, 3] {
                    { "Tire Counts $", ((model.TotalRemittancePayable!=null)?Convert.ToDecimal(model.TotalRemittancePayable).ToString("#,###,##0.00"):""), "+" }, 
                    { bAfterTSFNegAdjSwitchDate ? "Credit" : null, bAfterTSFNegAdjSwitchDate ? ((model.Credit!=null) ? Convert.ToDecimal(model.Credit * -1).ToString("#,###,##0.00"):"") : string.Empty, bAfterTSFNegAdjSwitchDate ? "+" : string.Empty},
                    { "Payment Adjustments", ((model.InternalPaymentAdjustment!=null) ? Convert.ToDecimal(model.InternalPaymentAdjustment).ToString("#,###,##0.00") : "0.00"), "=" },
                    { "Subtotal",Convert.ToDecimal(subtotal).ToString("#,###,##0.00") ,"+"},
                    { "HST", model.IsTaxApplicable ? Convert.ToDecimal(model.ApplicableTaxesHst ?? 0).ToString("#,###,##0.00") : "N/A", IsTaxApplicable},
                    { "Penalties", ((model.Penalties!=null)?Convert.ToDecimal(model.Penalties).ToString("#,###,##0.00"):""), IsPenaltyOverride },                   
                    { "Total (TSF DUE)", Convert.ToDecimal(total).ToString("#,###,##0.00"), ""},
                    { "Submitted", ((model.SubmittedDate!=null)?Convert.ToDateTime(model.SubmittedDate).ToString(TreadMarksConstants.DateFormat):""), "" },
                    { "Payment Received", ((model.PayReceiptDate!=null)?Convert.ToDateTime(model.PayReceiptDate).ToString(TreadMarksConstants.DateFormat):""), "" },
                    { "PaymentType", model.PaymentType, "" },
                    { "Cheque/EFT #", (!string.IsNullOrEmpty(model.ChequeReferenceNumber)?model.ChequeReferenceNumber:""), "" },
                    { "Payment ($)", model.PaymentAmount!=null ? String.Format( "{0:#,##0.00}", model.PaymentAmount) : "0.00", "" },
                    { "Balance Due ($)", ((balanceDue!=null)?Convert.ToDecimal(balanceDue).ToString("#,###,##0.00"):""), "" },
                };
            }
            var csv = payments.ToCSV(",", columnNames.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittancePayments-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittanceAssignment/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForStatus(int remittancesId)
        {
            var model = remittanceService.GetTSFRemittanceByClaimId(remittancesId);
            var columnNames = new List<string>(){
                "Submitted", "Assigned", "Started","Cheque Due","Approved","Hold","On-hold","Off-hold","On-hold (Days)"
            };
            string[,] statusDetail;
            statusDetail = new string[1, 9] {
            {
                ((model.Submitted!=null)?model.Submitted.Value.ToLocalTime().ToString(TreadMarksConstants.DateFormat):""),
                ((model.Assigned!=null)?model.Assigned.Value.ToLocalTime().ToString(TreadMarksConstants.DateFormat):""),
                ((model.StartedDate!=null)?model.StartedDate.Value.ToLocalTime().ToString(TreadMarksConstants.DateFormat):""),
                ((model.PeriodDate!=null)?model.PeriodDate.Value.AddMonths(2).ToLocalTime().ToString(TreadMarksConstants.DateFormat):""),
                ((model.ApprovedDate!=null)?model.ApprovedDate.Value.ToLocalTime().ToString(TreadMarksConstants.DateFormat):""),
                "Off","","",""
             }
            };

            var csv = statusDetail.ToCSV(",", columnNames.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittanceStatus-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"tsfremittanceAssignment/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        //OTSTM2-56 "submitted" added
        [HttpPost]
        public JsonResult ApproveRemittance(int claimID, string status, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignuser = 0)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            string updatedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;
            decimal? balanceDue = this.remittanceService.SetRemittanceStatus(claimID, status, updatedBy, submitted, received, deposit, referenceNo.Trim(), amount, PaymentType, assignuser);


            return Json(new { status = MessageResource.ValidData, BalanceDue = string.Format("{0:n2}", balanceDue) }, JsonRequestBehavior.AllowGet);
        }

        //OTSTM2-360 Back to Participant Option for Approved 0$ remittance
        [HttpPost]
        public JsonResult SetRemittanceUnderReview(int claimID, string status, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            string updatedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;
            decimal? balanceDue = this.remittanceService.SetRemittanceStatus(claimID, status, updatedBy, submitted, received, deposit, referenceNo.Trim(), amount, PaymentType, SecurityContextHelper.CurrentUser.Id);

            //OTSTM2-553 Add activity for back to Under Review
            AddActivity(claimID, ActivityTypeEnum.BackToUnderReview);

            //Publish activity event
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(claimID);

            return Json(new { status = MessageResource.ValidData, BalanceDue = string.Format("{0:n2}", balanceDue) }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AutoApproveRemittance(int claimID, string status, string amount, long assignuser = 0)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            //Added check for variance
            var model = remittanceService.GetTSFRemittanceByClaimId(claimID);

            if (CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRemittanceVarianceAlert").Value == "1")
                AddVarianceNotes(model);

            string updatedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;
            this.remittanceService.SetRemittanceStatusAuto(claimID, status, updatedBy, amount, assignuser);


            //OTSTM2-993 Add activity for Participant submitted the remittance before AutoApproved.
            AddActivity(claimID, ActivityTypeEnum.AutoApproved, 0, "Participant", SecurityContextHelper.CurrentUser.FullName);
            //OTSTM2-553 Add activity for auto approval
            AddActivity(claimID, ActivityTypeEnum.AutoApproved, 0, "System Admin", "System Admin");

            //Publish activity event
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(claimID);

            var submitEmailModel = new ApplicationEmailModel()
            {
                BusinessLegalName = model.BusinessName,
                Address1 = model.Address.Address1,
                City = model.Address.City,
                ProvinceState = model.Address.Province,
                PostalCode = model.Address.PostalCode,
                Country = model.Address.Country,
                Email = model.PrimaryContact.Email,
                RegistrationNumber = model.RegistrationNumber,
                BusinessName = model.BusinessName,
                MonthYear = model.PeriodDate.Value.ToString("MMM, yyyy"),            
            };

            SendEmailOnSubmit("RemittanceSubmitted", submitEmailModel);

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int claimID, string status, long assignuser = 0, bool isAssigningToMe = false)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            string updatedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;

            ///back to participant
            if (string.Compare(status.ToString(), ClaimStatus.Open.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
            {

                var model = remittanceService.GetTSFRemittanceByClaimId(claimID);
                string Email = string.Empty;

                //OTSTM2-943
                if (model.PrimaryContact != null)
                {
                    Email = model.PrimaryContact.Email;
                }
                else
                {
                    Email = this.applicationInvitationService.GetEmailByApplicationId(model.ApplicationID);
                }

                if (!string.IsNullOrWhiteSpace(Email))
                {
                    var backToApplicantModel = new RemittanceEmailModel()
                    {
                        BusinessLegalName = model.BusinessName ?? "Applicant",
                        Email = Email,
                        Period = model.PeriodDate.Value.ToString("MMM yyyy"),
                    };

                    if (EmailContentHelper.IsEmailEnabled("RemittanceBacktoParticipant"))
                        this.SendEmailForBackToApplicant(backToApplicantModel);
                }
            }

            this.remittanceService.SetRemittanceStatus(claimID, status, updatedBy, assignuser, isAssigningToMe);


            /////start submit email (for submitted and approved status, add by Frank)
            if ((status == ClaimStatus.Submitted.ToString() || status == ClaimStatus.Approved.ToString()) && !SecurityContextHelper.IsStaff())
            {
                var model = remittanceService.GetTSFRemittanceByClaimId(claimID);

                if (status == ClaimStatus.Submitted.ToString())
                {
                    if (model.BalanceDue <= 0)
                    {
                        var warningNote = string.Format("[{0:D}, {1:HH:mm:ss}] Please note that the balance due is equal to or less than 0. Participant should have attached supporting documents for negative adjustments. Please review support.", DateTime.Now, DateTime.Now);
                        AddWarningNote(claimID, warningNote);
                    }

                    if (model.BalanceDue > 0 && model.Credit != null && Math.Abs(model.Credit.Value) >= 200)
                    {
                        var warningNote = string.Format("[{0:D}, {1:HH:mm:ss}] Please note that the credit value is greater than 199.99. Participant should have attached supporting documents for negative adjustments. Please review support.", DateTime.Now, DateTime.Now);
                        AddWarningNote(claimID, warningNote);
                    }

                    if (CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRemittanceVarianceAlert").Value == "1")
                        AddVarianceNotes(model);

                    //Participant 2016-08-11 they dont want it anymore on participant side
                    //OTSTM2-175
                    //var customer = registrantService.GetVendorById(model.CustomerID);
                    //var isCreatedinNTM = (customer.CreatedDate.Value.Date >= Convert.ToDateTime(SiteSettings.Instance.GetSettingValue("Settings.UserDate")).Date);           

                    //if (model.Penalties > 0 && isCreatedinNTM && model.PeriodDate.Value.Date.Year > 2009 && !remittanceService.IsAnyPastPenalty(model.CustomerID, claimID))
                    //{
                    //    AddWarningNote(claimID, "First time penalty.");
                    //}
                }

                var submitEmailModel = new ApplicationEmailModel()
                {
                    BusinessLegalName = model.BusinessName,
                    Address1 = model.Address.Address1,
                    City = model.Address.City,
                    ProvinceState = model.Address.Province,
                    PostalCode = model.Address.PostalCode,
                    Country = model.Address.Country,
                    Email = model.PrimaryContact.Email,
                    RegistrationNumber = model.RegistrationNumber,
                    BusinessName = model.BusinessName,
                    MonthYear = model.PeriodDate.Value.ToString("MMM, yyyy"),
                    BalanceDue = model.BalanceDue.Value
                };

                if (model.BalanceDue >= 0)
                {
                    SendEmailOnSubmit("RemittanceSubmitted", submitEmailModel);
                }
                else
                {
                    SendEmailOnSubmit("RemittanceSubmittedwithCreditDue", submitEmailModel);
                }                
            }
            /////end submit email
            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        private void AddVarianceNotes(TSFRemittanceModel model)
        {
            var lastApprovedTSFRemittance = remittanceService.GetLastApprovedTSFRemittance(model.CustomerID, model.ID);
            if (lastApprovedTSFRemittance != null)
            {
                //fixed the case where both last and current tsfdue is 0
                if (model.TotalTSFDue.Value == 0 && lastApprovedTSFRemittance.TotalTSFDue == 0)
                    return;

                //OTSTM2-1048 Variance note on $0 Remittance comparison 
                var warningNote = string.Empty;
                if (model.TotalTSFDue.Value == 0 || lastApprovedTSFRemittance.TotalTSFDue == 0)
                {
                    warningNote = string.Format("[{0:D}, {1:HH:mm:ss}] A variance has been detected because of a $0 comparison based on {2} submission.", DateTime.Now, DateTime.Now, lastApprovedTSFRemittance.Period.ShortName);
                    AddWarningNote(model.ID, warningNote);
                    return;
                }

                decimal diff = 0;
                //OTSTM2-418 case when last tsfdue is 0
                if (lastApprovedTSFRemittance.TotalTSFDue != 0)
                {
                    //OTSTM2-418 case when current tsfdue is 0
                    if (model.TotalTSFDue.Value != 0)
                        diff = ((model.TotalTSFDue.Value - lastApprovedTSFRemittance.TotalTSFDue) / lastApprovedTSFRemittance.TotalTSFDue);
                    else
                        diff = 0 - lastApprovedTSFRemittance.TotalTSFDue / 100;
                }
                else
                {
                    diff = (model.TotalTSFDue.Value - lastApprovedTSFRemittance.TotalTSFDue) / 100;
                }

                //OTSTM2-1007 the value comes from Appsetting table
                decimal remittanceVarianceThreshold = Convert.ToDecimal(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.RemittanceVarianceAlert").Value) / 100;

                //greater than equal to 30%
                if (model.TotalTSFDue.Value >= (lastApprovedTSFRemittance.TotalTSFDue) * (decimal)(1 + remittanceVarianceThreshold))
                {
                    warningNote = string.Format("[{0:D}, {1:HH:mm:ss}] A ({2}) greater variance was found based on {3} submission.", DateTime.Now, DateTime.Now, diff.ToString("p"), lastApprovedTSFRemittance.Period.ShortName);
                    AddWarningNote(model.ID, warningNote);
                }
                else if (model.TotalTSFDue.Value <= (lastApprovedTSFRemittance.TotalTSFDue) * (decimal)(1 - remittanceVarianceThreshold))
                {
                    warningNote = string.Format("[{0:D}, {1:HH:mm:ss}] A ({2}) less variance was found based on {3} submission.", DateTime.Now, DateTime.Now, diff.ToString("p"), lastApprovedTSFRemittance.Period.ShortName);
                    AddWarningNote(model.ID, warningNote);
                }
            }
        }

        private void AddWarningNote(int claimID, string warningNote)
        {
            var tsfclaimNote = new TSFRemittanceNote
            {
                TSFClaimId = claimID,
                UserId = null,
                Note = warningNote,
                CreatedDate = DateTime.UtcNow
            };

            remittanceService.AddTSFRemittanceNote(tsfclaimNote);
        }

        public void SendEmailOnSubmit(string emailName, ApplicationEmailModel model)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                        SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                        SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                        SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                        SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                        SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                        SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                        SiteSettings.Instance.GetSettingValue("Company.Email"),
                        Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));

            string emailBody, subject;
            bool isEmailEnabled = false;

            //move to appSetting
            //string htmlbody = "";
            if (model.BalanceDue >= 0)
            {
                //htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates\\SubmitTSFEmailTemplate.html");
                emailBody = EmailContentHelper.GetCompleteEmailByName(emailName);
                subject = EmailContentHelper.GetSubjectByName(emailName);
                isEmailEnabled = EmailContentHelper.IsEmailEnabled(emailName);
            }
            else
            {
                //htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates\\SubmitTSFEmail_BalanceLessThanZeroTemplate.html");
                emailBody = EmailContentHelper.GetCompleteEmailByName(emailName);
                subject = EmailContentHelper.GetSubjectByName(emailName);
                isEmailEnabled = EmailContentHelper.IsEmailEnabled(emailName);
            }

            if (isEmailEnabled)
            {
                //string emailBody = SystemIO.File.ReadAllText(htmlbody);

                emailBody = emailBody
                    .Replace(StewardApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                    .Replace(StewardApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.MonthYear, model.MonthYear)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.ConfirmationNumber, model.RegistrationNumber + DateTime.Now.ToString("MMddyy"))
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BalanceDue, string.Format("{0:C2}", model.BalanceDue))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = StewardApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Submitted {0} TSF Remittance", model.MonthYear);

                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, null, null, subject, emailBody, model, null, alternateViewHTML);
            }
        }

        public void SendEmailForBackToApplicant(RemittanceEmailModel emailModel)
        {
            #region
            string message = string.Empty;
            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.BackToApplicantRemittanceEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("RemittanceBacktoParticipant");
            string subject = EmailContentHelper.GetSubjectByName("RemittanceBacktoParticipant");

            string loginURL = string.Format("{0}{1}", SiteSettings.Instance.DomainName, "/Account/Login");
            emailBody = emailBody
                .Replace(RemittanceEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(StewardApplicationEmailTemplPlaceHolders.MonthYear, emailModel.Period)
                .Replace(RemittanceEmailTemplPlaceHolders.LoginURL, loginURL)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(RemittanceEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(RemittanceEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = RemittanceEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = RemittanceEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = RemittanceEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                SiteSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("TSF Remittance deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);
            #endregion
        }

        public ActionResult UpddateRemittance(TSFRemittanceModel model)
        {

            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {

                model.TSFRemittance.CreatedBy = SecurityContextHelper.CurrentUser.FirstName;
                model.TSFRemittance.ModifiedBy = SecurityContextHelper.CurrentUser.FirstName + ' ' + SecurityContextHelper.CurrentUser.LastName;
                model.SubmittedBy = SecurityContextHelper.CurrentUser.FirstName;

                bool isCreated = false; //no use here, only for the parameter change of the method
                remittanceService.AddTSFClaim(model, ref isCreated);

                if ((SecurityContextHelper.IsStaff()))//or is participant user
                {
                    return RedirectToAction("StaffIndex", new { id = model.TSFRemittance.ClaimID });
                }
                return RedirectToAction("ParticipantIndex", new { id = model.TSFRemittance.ClaimID });
            }
            return RedirectToAction("ParticipantIndex", new { id = model.TSFRemittance.ClaimID });
        }

        [HttpPost]
        public JsonResult UpddateRemittanceDetail(TSFRemittanceModel model)
        {
            bool isCreated = false; //no use here, only for the parameter change of the method
            int ID = remittanceService.AddTSFClaim(model, ref isCreated);
            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Activity
        [HttpPost]
        public JsonResult LoadAllActivities(DataGridModel param, int claimId, string temp)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }
            else
            {
                if (temp != null)
                {
                    searchText = temp.Trim();
                }
            }
            //var searchText = param.Search != null && param.Search.ContainsKey("value")
            //      ? param.Search["value"].Trim()
            //     : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadAllActivities(param.Start, param.Length, searchText, orderBy, sortDirection, claimId);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetAllActivitiesExport(string searchText)
        {
            var sortColumn = Request.QueryString[1];
            var sortDirection = Request.QueryString[2];
            var claimId = Request.QueryString[3];
            int cid = 0;
            int.TryParse(claimId, out cid);
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var result = messageService.GetAllActivitiesExport(cid, searchText, sortColumn, sortDirection);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All Remittance Activity Excel-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult TotalActivity(int claimId)
        {
            var count = messageService.TotalActivity(claimId);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Internal Adjument

        [HttpGet]
        public ActionResult GetListHandlerInternalAdjustment(DataGridModel param, int claimId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "AdjustmentDate"},
                {1, "AdjustmentType"},
                {2, "Note"},
                {3, "AdjustmentBy" }
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var internalAdjustments = remittanceService.LoadTSFClaimInternalAdjustments(param.Start, param.Length, searchText, orderBy, sortDirection, claimId, SecurityContextHelper.IsStaff());
            internalAdjustments.DTOCollection.ForEach(i =>
            {
                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
            });

            var json = new
            {
                draw = param.Draw,
                recordsTotal = internalAdjustments.TotalRecords,
                recordsFiltered = internalAdjustments.TotalRecords,
                data = internalAdjustments.DTOCollection
            };

            return new NewtonSoftJsonResult(json, false);
        }

        public JsonResult AddInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            var result = remittanceService.AddInternalAdjustment(claimId, modalResult);
            if (result)
            {
                return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            AddActivity(claimId, ActivityTypeEnum.IntAdjRemoved, internalAdjustmentId);
            remittanceService.RemoveInternalAdjustment(claimId, adjustmentType, internalAdjustmentId);
            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInternalAdjustment(int adjustmentType, int internalAdjustmentId)
        {
            var result = remittanceService.GetClaimAdjustmentModalResult(adjustmentType, internalAdjustmentId);
            return new NewtonSoftJsonResult(result);
        }

        public JsonResult EditInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            remittanceService.EditInternalAdjustment(claimId, modalResult);
            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcelInternalAdjustment(int claimId, string searchText = "")
        {
            var sortcolumn = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 1) ? Request.QueryString[1] : string.Empty;

            var columns = new List<string>();
            columns.Add("Date");
            columns.Add("Type");
            if (SecurityContextHelper.IsStaff())
            {
                columns.Add("Notes");
            }
            columns.Add("Added By");

            List<Func<TSFClaimInternalAdjustment, string>> delegates;
            if (SecurityContextHelper.IsStaff())
            {
                delegates = new List<Func<TSFClaimInternalAdjustment, string>>() { u => u.AdjustmentDate.ToString(TreadMarksConstants.DateFormat), u => u.AdjustmentType, u => u.Note, u => u.AdjustmentBy };
            }
            else
            {
                delegates = new List<Func<TSFClaimInternalAdjustment, string>>() { u => u.AdjustmentDate.ToString(TreadMarksConstants.DateFormat), u => u.AdjustmentType, u => u.AdjustmentBy };
            }

            var internalAdjustments = remittanceService.LoadTSFClaimInternalAdjustments(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId, SecurityContextHelper.IsStaff());

            internalAdjustments.DTOCollection.ForEach(i =>
            {
                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
                //i.Note = i.NotesAllText.Replace("<hr>", ";").Replace("\n"," ");
                i.Note = i.NotesAllText.Replace("<hr>", "; ");
            });

            string csv = internalAdjustments.DTOCollection.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("StewardRemittance-InternalAdjustment-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }


        #region Internal Adjustment Note
        [HttpGet]
        public ActionResult GetInternalNotesInternalAdjustment(int ClaimInternalPaymentAdjustId)
        {
            var result = remittanceService.GetInternalNotesInternalAdjustment(ClaimInternalPaymentAdjustId);
            result.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandlerInternalAdjustment(int ClaimInternalPaymentAdjustId, string notes)
        {
            var note = remittanceService.AddNotesHandlerInternalAdjustment(ClaimInternalPaymentAdjustId, notes);
            return new NewtonSoftJsonResult(note, false);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            var claimNotes = remittanceService.ExportToExcelInternalAdjustmentNotes(ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
                //c.Note = c.Note.Replace("\n", " ");
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion


        #endregion
    }
}