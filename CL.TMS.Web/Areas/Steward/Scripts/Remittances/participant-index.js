
//----Steward Remittance participant-index.jsSettings.TSFNegAdjSwitchDate
//RemittancesDetails();
$(function () {
   TSFRemittance();
    TireCount();
    $('#tdedit').css('display', 'none');
    $('#tdAdd').css('display', 'block');
    //$.fn.EnableDisableAddButton();
    var initialize = function () {
    }
    initialize();
    RunBusinessRules();
    TSFPanelGreenCheckSuppDoc();
    setExportUrl();

    $('#CreditRatePeriodDate').val('');

    if (Global.Remittance.PageIsReadonly) {
        $('#dpCreditRatePeriodDate').attr("style", "pointer-events: none; cursor: not-allowed;");
        $('.dropdown').addClass("disable-click");
        $('input[type="radio"]').addClass('disable-click');
        $('#fileUploadInput').addClass("disable-click");
        $('button[type="button"]').addClass('disable-click');
        $('button[type="button"]').prop("disabled", true);
        $('.form-control').prop("disabled", true);
        $('#txtGlobalSearch').removeAttr("disabled");
        $('#user-left-box').removeClass("disable-click");        
    }

    //OTSTM2-21
    var PeriodDate = $("#PeriodDate").val();
     if (PeriodDate != "") {
         var dateArray = PeriodDate.split(' ');
         var dt = new Date();
         var currentYear = dt.getFullYear();
         if (dateArray[1] != currentYear) {
             $("#StewardSetting").removeAttr("data-original-title");
            $("#StewardSetting").find(".btn").addClass("disabled").prop("disabled", true);
         }
     }

    var settings = {
        trigger: 'hover',
        multi: true,
        closeable: true,
        style: '',
        delay: { show: 300, hide: 800 },
        padding: true
    };

    // Class Category: Description
    var popoverClassCategoryContent = $('#popoverClassCategory').html(),
      popoverClassCategorySettings = {
          content: popoverClassCategoryContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.popoverClassCategory').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverClassCategorySettings));

    var penaltiesPopover = {
        content: $('#penalties-popover').data("popovertext"),
        width: 300,
        height: 60,
    };
    $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({}, settings, penaltiesPopover));

});
//function getURL()
//{

//    var url = "/Steward/TSFRemittances/ExportToExcelForTireCounts";//Global.DashBoard.ApplicationExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);

//    $('#exportTireCountsToExcel').attr('href', url);
//}
function setExportUrl() {
    var url = Global.ExportUrl.TireCountsExportUrl;
    var urlPayments = Global.ExportUrl.PaymentsExportUrl;
    $('#tireCountsExport').attr('href', url);
    $('#exportPayments').attr('href', urlPayments);
    url = Global.ExportUrl.CreditExportUrl;
    $('#creditExport').attr('href', url);
    }
function CloseEdit() {
    window.location.reload(true);
}

function RunBusinessRules() {
    
    $('#negativeAdj').toggle(false);

    //1 . Old Rule if there is any -ve adj in tire coutns panel, supp doc required
    if ($("#TSFRemittance_NegativeAdjustment").length != 0) {
        //$('#negativeAdj').toggle(false);
        $("#tblTireCount tr").each(function (e) {

            var negAdjValue = $(this).find("td").eq(2).find(".negAdj").html();

            //If june 2016 credit is not there but Period is there
            var dt1 = new Date($("#PeriodDate").val());
            var dt2 = new Date($('#tsfMonthYearSelectionDate').val());
            if (dt1.getUTCFullYear() == dt2.getUTCFullYear() && dt1.getUTCMonth() == dt2.getUTCMonth() && $("#SRemitsPerYears").val() == "False")
                negAdjValue = $(this).find("td").eq(3).find(".negAdj").html();


            if (negAdjValue != null && negAdjValue.indexOf("-") != -1) {
                $('#negativeAdj').toggle(true);
                return;
            }                       
        });
    }
    
    //absolute
    var credit = parseFloat($("#spnCredit").text().replace(',', '').replace('-', ''));
    
    //absolute
    //var tsfDue = parseFloat($("#spnTotal").text().replace(',', '').replace('-', ''));
    //OTSTM2-1315, the business rules should check against Tire counts now
    var totalRemittancePayable = $("#hdTotalRemittancePayable").val();
    var tsfDue = parseFloat(totalRemittancePayable.replace(',', '').replace('-', ''));
    
    //2. TSF Due > Credit, Credit > $199.99 (Steward is paying but credit is too high)  
    $("#tblCredit tr").each(function (e) {

        var negAdjValue = $(this).find("td").eq(2).find(".negAdj").html();
        if (negAdjValue != null && negAdjValue.indexOf("-") != -1 && tsfDue > credit && credit >= 200) {
            $('#negativeAdj').toggle(true);
            return;
        }        
    });

    //3. Credit > TSF Due (OTS needs to pay back to Steward)
    if (credit > tsfDue)
    {
        $('#negativeAdj').toggle(true);
        return;
    }

    //4. TSF Due > 0, Credit > 0, TSF Due = Credit  (Steward is not paying anything)
    if (tsfDue > 0 && credit > 0 && credit == tsfDue) {
        $('#negativeAdj').toggle(true);
        return;
    }
}

//Compares the checkmark image, if green then enable submit button
//Great piece of code ;)
function TSFSubmitButtonSuppDoc() {
    $(".greenCheckmark").each(function () {
        //OTSTM2-303 need to check both supporting doc and payment type
        var paymentCheckBox = $('input[type="radio"][name="paymentTypeGroup"]:checked').val();
        if ($(this).attr("src") == statuses[1] && ((paymentCheckBox != null) || ($('#submitRemittanceClaimBtn').text().indexOf('Submit $0') > -1))) {            
            //Permission for submit
            if (Global.Remittance.PageCanBeSubmitted) {
                $("#submitRemittanceClaimBtn").prop("disabled", false);
                Global.Remittance.bSubmitBtnGreen = true;
            }
        }
        else {
            //OTSTM2 - 303
            if (paymentCheckBox != null && Global.Remittance.PageCanBeSubmitted) {
                Global.Remittance.bSubmitBtnGreen = true;
            } else {
                Global.Remittance.bSubmitBtnGreen = false;
            }
            $("#submitRemittanceClaimBtn").prop("disabled", true);
            return false;
        }
    });
}

function TSFPanelGreenCheckSuppDoc() {

    var fileExists = $('#currUploadFiles').val();
    var fileRequired = false;
    $('#flagSupportingDocuments').attr('src', statuses[0]);
    $("#fmSupportingDoc input[type=radio]").each(function () {
        //var fileExists = $('#filesToUploadGrid tr').length;
        var tr = $(this).parents('tr');
        if (tr.css('display') != "none") {
            var radioValue = $(this).val();
            if (radioValue == 'optionupload' && $(this).is(':checked')) {
                fileRequired = true;
            }
        }
    });
    var isValid = (fileRequired && fileExists > 0) || !fileRequired;
    $('#flagSupportingDocuments').attr('src', isValid ? statuses[1] : statuses[0]);
    TSFSubmitButtonSuppDoc();
}

function TireCount() {

    var $select = $('#ddlTireList');

    //if ($("#divStatus").html() == "Submitted" || $("#divStatus").html() == "None" || $("#divStatus").html() == "Open")
    //{
    //    $("#totbalance").hide();
    //    $("#totamount").hide();

    //}
    //else {
    //    $("#totbalance").show();
    //    $("#totamount").hide();

    //}

    //Add for 2X
    var dt1 = new Date($("#PeriodDate").val());
    var dt2 = new Date($('#tsfMonthYearSelectionDate').val());
    var dt3 = new Date($('#tsfNegAdjSwitchDate').val()) //add for 2x June

    if ($("#divStatus").text() != "Unknown") {
        $("#PeriodDate").attr("disabled", true);
        $("#Calender").addClass("disable-click")
        $("#btnSection").removeClass("disable-click")
    }
    else {
        $("#PeriodDate").attr("disabled", false);
        $("#Calender").removeClass("disable-click")
        $("#btnSection").addClass("disable-click")
    }

    if ($("#divStatus").text() == "Unknown" || $("#divStatus").text() == "Under Review" || $("#divStatus").text() == "UnderReview" || $("#divStatus").text() == "Submitted" || $("#divStatus").text() == "Approved") {
        $("#TSFRemittance_CategoryId").prop("disabled", true);
        $("#Add").addClass("disable-click");
        $("#AddCredit").addClass("disable-click");
        $(".dropdown-menu-actions").addClass("disable-click");
        $('input[type="radio"][name="paymentTypeGroup"]').addClass("disable-click");
        $("#fileUploadDropzone").addClass("disable-click");//disable upload new file, but keep Download btn enable
    }
    else if (dt1>=dt2 && $("#SRemitsPerYears").val()=="False") {//Add for 2X
        $("#TSFRemittance_CategoryId").prop("disabled", true);
        $("#TSFRemittance_Quantity").prop("disabled", true);
        if (dt1<dt3) { //Add for 2X June
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);  //OTSTM2-457
        }
        else {
            $("#Credit_TSFRemittance_CategoryId").prop("disabled", true); //OTSTM2-457
            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
    }
    else {
        $("#TSFRemittance_CategoryId").removeProp("disabled");
        $("#TSFRemittance_Quantity").prop("disabled", true); //OTSTM2-377
        if (dt1 < dt3) { //OTSTM2-377
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
        else {
            $("#Credit_TSFRemittance_CategoryId").prop("disabled", true); //OTSTM2-457
            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
        $("#Add").removeClass("disable-click");
        $(".dropdown-menu-actions").removeClass("disable-click");
        $("#panelSupportingDocuments").removeClass("disable-click");
        $("#fileUploadDropzone").removeClass("disable-click");
    }
    if ($("#divStatus").text() == "Unknown") $("#divStatus").text("");
    $select.append('<option value="0">Select a Category</option>');
    $('#item').change(function () {
        var itemID = $(this).val();

        $.ajax({
            url: '/Steward/TSFRemittances/GetRemittanceRateByItemId',
            method: 'POST',
            dataType: "JSON",
            data: { itemID: itemID },
            complete: function (data) {
            }
        });

    });

    if ($('#chkterms').is(":checked")) {
        $('#btnsubmit').prop("disabled", false);
    }
    else { $('#btnsubmit').prop("disabled", true); }
}

function HideEditForm(ShowID, HideID) {

    $(ShowID).show()
    $(HideID).hide()

}

function TSFRemittance() {
    $("#td_Net").text("0.00");
    $("#td_Rate").text("0.00");
    $("#td_Total").text("0.00");

    $("#td_CreditRate").text("0.00");
    $("#td_CreditTotal").text("0.00");

    $('#dpPeriodDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'MM yyyy',
        maxDate: "0",
        minDate: new Date(2009, 09, 1),
        altFormat: 'yyyy-mm-dd',
        altField: '.set',
        startDate: $("#hfPeriodStartDate").val(),
        endDate: new Date(),
    });
    $('#dpPaymentReceivedDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'dd/mm/yyyy'

    });
    $('#dpPaymentDepositDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'dd/mm/yyyy'
    });

    var periodDate = new Date($("#PeriodDate").val());
    var y = periodDate.getFullYear();
    var m = periodDate.getMonth();
    var endDate = new Date(y, m + 1, 0);


    $('#dpCreditRatePeriodDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: "mm-yyyy",
        viewMode: "months",
        startView: 'year',
        minViewMode: "months",
        startDate: "2009-09-01",
        //endDate: formatDate(d)
        endDate:endDate
    });

    //Add for 2X
    $("#TSFRemittance_TSFRateDate").change(function () {
        //OTSTM2-457, enhance user experience, start--
        $("#TSFRemittance_Quantity").prop("disabled", true); 
        $("#TSFRemittance_Quantity").val('0');
        if ($('#TireCountsTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) { 
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).val('0');
            $.fn.showHideReasonDiv();
            $("#TSFRemittance_AdjustmentReasonType").val("");
        }
        $("#td_Rate").text("0.00");
        $.fn.allCalculations();  
        //--end--
        if ($("#TSFRemittance_TSFRateDate option:selected").val()!='') {
            var PeriodDate = $("#TSFRemittance_TSFRateDate option:selected").text();
            $.ajax({
                url: '/Steward/TSFRemittances/LoadTireListBasedOnTSFRateDate',
                dataType: 'json',
                type: 'POST',
                data: { PeriodDate: PeriodDate, ClaimID: $("#ClaimID").val() },
                success: function (result) {
                    $("#TSFRemittance_CategoryId").prop("disabled", false);
                    //$("#TSFRemittance_Quantity").prop("disabled", false); //OTSTM2-377 comment out
                    //$("#TSFRemittance_NegativeAdjustment").prop("disabled", false);//add for 2x June //OTSTM2-377 comment out
                    $('#TSFRemittance_CategoryId').empty();
                    var $select = $('#TSFRemittance_CategoryId');
                    $select.append('<option value="0">Select a Category</option>');

                    for (var i = 0; i < result.Tirelist.length; i++) {
                        $select.append(
                              $('<option/>', {
                                  value: result.Tirelist[i].ID,
                                  html: result.Tirelist[i].Name
                              })
                        );
                    }
                }
            });
        }
        else {
            $("#TSFRemittance_CategoryId").prop("disabled", true);
            $("#TSFRemittance_Quantity").prop("disabled", true);
            //$("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //add for 2x June
            $('#TSFRemittance_CategoryId').empty();
            var $select = $('#TSFRemittance_CategoryId');
            $select.append('<option value="0">Select a Category</option>');
            $("#TSFRemittance_Quantity").val('0');
            $("#td_Rate").text("0.00");
            $("#td_Total").text("0.00");
            $("#btnAdd").prop("disabled", true);
        }
        
    });

    $('#modalOpenRemittance').on('hidden.bs.modal', function () {
        console.log($("#PeriodDate").val());
        if (Global.Remittance.bResetDTpicker) {
            $("#PeriodDate").val('');
        }
    });

    $("#PeriodDate").change(function () {
        console.log($("#PeriodDate").val());
    
        if ($("#PeriodDate").val() == '') {
            return;
        }
        var pickupDateTime = $("#dpPeriodDate").data("datetimepicker").getDate().toISOString();
        $("#PeriodDate").attr("datedata", pickupDateTime);
      
        var PeriodDate = $(".set").val(); 
        $('#periodName').text(PeriodDate);
        $('#modalOpenRemittance').modal('show');
    });

    //OTSTM2-158 if reload is required after clicking OK
    //$(".close-inactive-steward-modal").on("click", function () {
    //    window.location.reload(true);
    //});

    //Bind Item Category Behalf of Selected period
    $("#btnYesOpenRemittance").click(function (event) {
        //OTSTM2-158 client side check, comment out
        //var activityApprovedDate = new Date(Global.Remittance.ActivityApprovedDate);
        //var activityYear = activityApprovedDate.getFullYear();
        //var activityMonth = activityApprovedDate.getMonth() + 1;
        //var selectedPeriodDate = $('#dpPeriodDate').data("datetimepicker").getDate();
        //var selectedPeriodYear = selectedPeriodDate.getFullYear();
        //var selectedPeriodMonth = selectedPeriodDate.getMonth() + 1;
        //if (Global.Remittance.ActiveStatus == "InActive") {
        //    if (selectedPeriodYear > activityYear) {
        //        $('#modalInactiveStewardParticipant').modal('show');
        //    }
        //    else if (selectedPeriodYear == activityYear && selectedPeriodMonth > activityMonth) {
        //        $('#modalInactiveStewardParticipant').modal('show');
        //    }
        //    else {
        //        createRemittance();
        //    }
        //}
        //else {
        //    createRemittance();
        //}
        
        //pass value to backend, do server side check
        var dt1 = new Date($("#PeriodDate").attr("datedata"));
        var selectedPeriodDate = dt1.toISOString();
        console.log(selectedPeriodDate)
        $.ajax({
            url: '/Steward/TSFRemittances/RunCreateOrSubmitRules',
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            data: JSON.stringify({ selectedPeriodDate: selectedPeriodDate, customerID: Global.ParticipantClaimsSummary.CustomerId }),
            success: function (result) {
                if (result.canCreateOrSubmit) {
                    createRemittance();
                }
                else {
                    if (result && result.reason) {
                        $('#createTSFRemittanceMsgStaff').text(result.reason);
                    }
                    $('#modalInactiveStewardStaff').modal('show');
                }
            }
        });
    });

    var createRemittance = function () {
        Global.Remittance.bResetDTpicker = false;//
        var dt1 = new Date($("#PeriodDate").attr("datedata"));
        var selectedPeriodDate = dt1.toISOString();
        var temp=$('#dpPeriodDate').data("datetimepicker").getDate().toISOString();
        var form = $('fmRemittanceTireCount')
        var data = form.serializeArray();
        data.push({ name: 'SRemitsPerYears', value: $("#SRemitsPerYears").val() });
        data.push({ name: 'PeriodDate', value: selectedPeriodDate });
        data.push({ name: 'Status', value: 'Open' });
        data.push({ name: 'CustomerID', value: $("#hdncustomerID").val() });
        data.push({ name: 'RegistrationNumber', value: $("#hdnregistrationNumber").val() });
        data.push({ name: 'IsCreatedByStaff', value: 'false' }); //OTSTM2-56

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        $.ajax({
            url: '/Steward/TSFRemittances/GetTireListOnPeriodBasis',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (result) {
                if (result.ID == -1) {
                    //alert('You can select June and December Period only in semiannually');
                    //TSF Remittance for the selected reporting period already exists, if you require assistance please contact OTS
                    //OTSTM2-21
                    var template = "You can select <strong>June</strong> and <strong>December</strong> only for <strong>" + result.Year + "</strong>.";
                    $('#modalCustomerSettingAlert').modal('show');
                    $('#modalCustomerSettingAlertBody').html(template);
                }
                else {
                    if (result && result.reason) {
                        console.log(result.reason);
                        $('#createTSFRemittanceMsg').text(result.reason);
                        $('#modalInactiveStewardParticipant').modal('show');
                    } else {
                        window.location.href = "/Steward/TSFRemittances/ParticipantIndex/" + result.ID;
                    }
                }

            },
            failure: function (result) {
                console.log(result);
            }
        });
    }

    /* Go back to summary page*/
    $('#lnkBacktoSummary').click(function (event) {
        var Id = $("#hdncustomerID").val();
        $.ajax({
            url: '/Steward/TSFRemittances/Index',
            method: 'GET',
            dataType: "JSON",
            data: { Id: Id },
            complete: function (result) {
            }
        });
    });
    //Bind rate behalf of selected Item and Effective Period
    $("#TSFRemittance_CategoryId").change(function () {

        if ($("#TSFRemittance_CategoryId").val() != "0" && $("#TSFRemittance_CategoryId").val() != "") { //OTSTM2-457 jqury error fixed
            var PeriodDate = $("#PeriodDate").val();
            if ($("#TSFRemittance_TSFRateDate").length != 0) {
                PeriodDate = $("#TSFRemittance_TSFRateDate option:selected").text();
            }
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'GET',
                dataType: "JSON",
                data: { PeriodDate: PeriodDate, ItemID: $("#TSFRemittance_CategoryId").val() },
                complete: function (result) {
                    //for (var i = 0; i < result.responseJSON.length; i++) {
                    //    var Rate = parseFloat(result.responseJSON[i].ItemRate).toFixed(2);
                    //    $("#td_Rate").text(Rate);
                    //    $.fn.allCalculations();
                    //    $.fn.showHideReasonDiv();
                    //    $.fn.EnableDisableAddButton();
                    //}
                    var Rate = parseFloat(result.responseJSON.ItemRate).toFixed(2);
                    $("#td_Rate").text(Rate);
                    $.fn.allCalculations();
                    $.fn.showHideReasonDiv();
                    $.fn.EnableDisableAddButton();

                    $("#TSFRemittance_Quantity").prop("disabled", false); //OTSTM2-377
                    if (!Global.Remittance.bShowCreditPanel) { //OTSTM2-377
                        $("#TSFRemittance_NegativeAdjustment").prop("disabled", false);
                    }
                }
            });

        }
        else {

            //OTSTM2-457, enhance user experience, start--
            $("#TSFRemittance_Quantity").prop("disabled", true);
            $("#TSFRemittance_Quantity").val('0');
            if ($('#TireCountsTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) {
                $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);
                $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).val('0');
                $("#TSFRemittance_AdjustmentReasonType").val("");
            }
            //--end            

            $("#td_Rate").text("0.00");
            $.fn.allCalculations();
            $.fn.showHideReasonDiv();
            $.fn.EnableDisableAddButton();
        }
    });

    $("#Credit_TSFRemittance_CategoryId").change(function () {
        if ($("#Credit_TSFRemittance_CategoryId").val() != "0") {
            var CreditRatePeriodDate = $("#CreditRatePeriodDate").val();
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'GET',
                dataType: "JSON",
                data: { PeriodDate: CreditRatePeriodDate, ItemID: $("#Credit_TSFRemittance_CategoryId").val() },
                complete: function (result) {
                    //for (var i = 0; i < result.responseJSON.length; i++) {
                    //    var Rate = parseFloat(result.responseJSON[i].ItemRate).toFixed(2);
                    //    $("#td_CreditRate").text(Rate);
                    //    //$.fn.allCalculations(); 
                    //    $.fn.CreditAllCalculations(); //OTSTM2-457 should be for credit
                    //    $.fn.showHideReasonDiv();
                    //    //$.fn.EnableDisableAddButton(); 
                    //    $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit
                    //}
                    var Rate = parseFloat(result.responseJSON.ItemRate).toFixed(2);
                    $("#td_CreditRate").text(Rate);
                    //$.fn.allCalculations(); 
                    $.fn.CreditAllCalculations(); //OTSTM2-457 should be for credit
                    $.fn.showHideReasonDiv();
                    //$.fn.EnableDisableAddButton(); 
                    $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit
                    $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", false); //OTSTM2-457
                }
            });
        }
        else {

            //OTSTM2-457, enhance user experience, start--
            $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
            $("#TSFRemittance_NegativeAdjustment").val('0');
            $("#TSFRemittance_AdjustmentReasonType").val("");
            $.fn.CreditAllCalculations();
            //--end            

            $("#td_CreditRate").text("0.00");
            //$.fn.allCalculations(); 
            $.fn.showHideReasonDiv(); //OTSTM2-457 should be for credit
            //$.fn.EnableDisableAddButton(); 
            $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit
        }
    });

    //calculation
    $(".Calculation").change(function () {
        $.fn.allCalculations();
        $.fn.showHideReasonDiv();
        $.fn.EnableDisableAddButton();
    });

    $(".CreditCalculation").change(function () {
        $.fn.CreditAllCalculations();
        $.fn.showHideReasonDiv();
        $.fn.CreditEnableDisableAddButton();
    });

    $("#TSFRemittance_AdjustmentReasonType").change(function () {
        $.fn.showHideDiv($(this).val(), ".reasonDesc", 4)
        if (!Global.Remittance.bShowCreditPanel) {//before effective date
            $.fn.EnableDisableAddButton();
        } else {
            $.fn.CreditEnableDisableAddButton();
        }

    });

    $('#TSFRemittance_OtherDesc').change(function () {
                $.fn.EnableDisableAddButton();
                });

    $(".drlOther").click(function () {
        //$.fn.showHideDiv($(this).val(), ".reasonDesc", 2)
        $.fn.showHideDiv(1, ".reasonDesc", 4);

    });

    $.fn.showHideDiv = function (val1, DivId, val2) {
        if (val1 != "" && val2 != "") {
            if (val1 == val2) {
                $(DivId).show();
            }
            else {
                $(DivId).hide();
            }
        }
        else {
            if (val1 > 0) {
                $(DivId).show();
            }
            else {
                $(DivId).hide();
            }
        }
    }


    //Edit method
    $(".edit").click(function (event) {
        event.preventDefault();
        var detId = $(this).attr("id");
        var categoryDate = $(this).closest('tr').children('td.creditPeriodDate').text();
        if (categoryDate == "") {
            categoryDate = $("#PeriodDate").val();
            }

        $.ajax({
            url: '/Steward/TSFRemittances/GetTireListForEdit',
            method: 'GET',
            dataType: "JSON",
                data: { PeriodDate: categoryDate },
            complete: function (result) {
                $('#TSFRemittance_CategoryId').empty();
                var $select = $('#TSFRemittance_CategoryId');
                $select.append('<option value="0">Select a Category</option>');

                for (var i = 0; i < result.responseJSON.Tirelist.length; i++) {
                    $select.append(
                              $('<option/>', {
                                  value: result.responseJSON.Tirelist[i].ID,
                                  html: result.responseJSON.Tirelist[i].Name
                              })
                     );
                }

                $('#Credit_TSFRemittance_CategoryId').empty();
                var $selectCredit = $('#Credit_TSFRemittance_CategoryId');
                $selectCredit.append('<option value="0">Select a Category</option>');

                for (var i = 0; i < result.responseJSON.Tirelist.length; i++) {
                    $selectCredit.append(
                              $('<option/>', {
                                  value: result.responseJSON.Tirelist[i].ID,
                                  html: result.responseJSON.Tirelist[i].Name
                              })
                     );
                }

                $.ajax({
                    url: '/Steward/TSFRemittances/GetClaimDetail',
                    method: 'GET',
                    dataType: "JSON",
                    data: { claimID: $("#ClaimID").val(), detId: detId },
                    complete: function (result) {
                        if (result.responseJSON.CreditNegativeAdj == 0) {//edit tire counts
                            //$("#td_Net").text("0.00");
                            $("#td_Rate").text("0.00");
                            $("#td_Total").text("0.00");
                            $("#ClaimDetailID").val(result.responseJSON.DetailID);
                            $("#TSFRemittance_TSFRateDate").val(result.responseJSON.TSFRateDateShortName);//add for 2x
                            $("#TSFRemittance_TSFRateDate").prop('disabled', true);//add for 2x
                            var cid = result.responseJSON.CategoryId.toString();
                            $("#TSFRemittance_CategoryId").val("" + cid + "");
                            $('#TSFRemittance_CategoryId').prop('disabled', true);
                            $("#TSFRemittance_Quantity").val(result.responseJSON.Quantity);
                            $("#TSFRemittance_Quantity").prop('disabled', false);
                            $("#TSFRemittance_NegativeAdjustment").val(result.responseJSON.NegativeAdjustment);
                            $("#TSFRemittance_NegativeAdjustment").prop('disabled', false);//add for 2x June
                            $("#td_Net").text(result.responseJSON.Net);//add for 2x June
                            var Rate = parseFloat(result.responseJSON.Rate).toFixed(2);
                            $("#td_Rate").text(Rate);
                            $("#td_Total").text(result.responseJSON.Total);
                            $('#TSFRemittance_Rate').val(Rate);
                            $('#TSFRemittance_Total').val(result.responseJSON.Total);
                            $('#TSFRemittance_Net').val(result.responseJSON.Net);
                            $('#tdedit').css('display', 'block');
                            $('#tdedit').prop('disabled', true);
                            $('#tdAdd').css('display', 'none');//otherdesc
                            $('#TSFRemittance_OtherDesc').val(result.responseJSON.OtherDesc);
                            BindAdjustmentReason(result.responseJSON.AdjustmentReasonType);
                        } else { //edit Credit
                            $("#td_CreditRate").text("0.00");
                            $("#td_CreditTotal").text("0.00");
                            $("#ClaimDetailID").val(result.responseJSON.DetailID);
                            var cid = result.responseJSON.CategoryId.toString();
                            $("#Credit_TSFRemittance_CategoryId").val("" + cid + "");
                            $('#Credit_TSFRemittance_CategoryId').prop('disabled', true);
                            $("#TSFRemittance_Quantity").val(result.responseJSON.Quantity);
                            $("#TSFRemittance_NegativeAdjustment").val(result.responseJSON.CreditNegativeAdj);
                            var Rate = parseFloat(result.responseJSON.Rate).toFixed(2);
                            $("#td_CreditRate").text(result.responseJSON.CreditTSFRate);
                            $("#td_CreditTotal").text(result.responseJSON.CreditTSFDue);
                            $('#TSFRemittance_Rate').val(Rate);
                            $('#TSFRemittance_Total').val(result.responseJSON.CreditTSFDue);
                            $('#tdeditCredit').css('display', 'block');
                            $('#tdeditCredit').prop('disabled', true);
                            $('#tdAddCredit').css('display', 'none');//otherdesc
                            $('#CreditRatePeriodDate').val(result.responseJSON.strNegativeAdjDate);
                            $('#CreditRatePeriodDate').prop('disabled', true);
                            $('#dpCreditRatePeriodDate').attr("style", "pointer-events: none; cursor: not-allowed;");
                            $('#TSFRemittance_OtherDesc').val(result.responseJSON.OtherDesc);
                            BindAdjustmentReason(result.responseJSON.AdjustmentReasonType);
                            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", false); //OTSTM2-457
                        }
                        $('.edit').addClass('disable-click').prop("disabled", false);//avoid other panel starting edit delete
                        $('.delete').addClass('disable-click').prop("disabled", false);
                    }
                });
            }
        });
    });

    $.fn.EnableDisableAddButton = function () {
        $("#btnAdd").prop("disabled", true);
        $("#btnupdate").prop("disabled", true);

        $("#tdAddCredit").prop("disabled", true);
        $("#btnAddCredit").prop("disabled", true);
        $("#btnupdateCredit").prop("disabled", true);

        if (!Global.Remittance.bShowCreditPanel)
        {
            //if ($('#TireCountTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) { //If there is NegativeAdjustment Column
            if ($('#TSFRemittance_AdjustmentReasonType').is(':visible')) {
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "")
                    return false;
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "4" && $("#TSFRemittance_OtherDesc").val().length <= 0)
                    return false;
            }
            else {
                if ($("#TSFRemittance_NegativeAdjustment").val() != "0") {
                    return false;
                }
            }
        }
        if (!Global.Remittance.bShowCreditPanel)//before effective date
        {
            if ($("#TSFRemittance_CategoryId").val() == "" || $("#TSFRemittance_CategoryId").val() == "0") {
                return false;
            }
            if (parseFloat($("#TSFRemittance_Quantity").val()) > 0 && $("#TSFRemittance_CategoryId").val() != "") {
                $("#btnAdd").prop("disabled", false);
                $("#btnupdate").prop("disabled", false);
            }
        }
        else {
            if ($("#TSFRemittance_CategoryId").val() == "" || $("#TSFRemittance_CategoryId").val() == "0") {
                return false;
            }
            if (parseFloat($("#TSFRemittance_Quantity").val()) > 0 && $("#TSFRemittance_CategoryId").val() != "") {

                $("#btnAdd").prop("disabled", false);
                $("#btnupdate").prop("disabled", false);
            }
            if ($("#Credit_TSFRemittance_CategoryId").val() == "" || $("#Credit_TSFRemittance_CategoryId").val() == "0") {
                return false;
            }
            if (parseFloat($("#TSFRemittance_NegativeAdjustment").val()) > 0 && $("#Credit_TSFRemittance_CategoryId").val() != "") {
                $("#tdAddCredit").prop("disabled", false);
                $("#btnAddCredit").prop("disabled", false);
                $("#btnupdateCredit").prop("disabled", false);
            }
        }

    }

    $.fn.CreditEnableDisableAddButton = function () {
        $("#btnAddCredit").prop("disabled", true);
        $("#btnupdate").prop("disabled", true);
        if ($("#TSFRemittance_NegativeAdjustment").length != 0) { //If there is NegativeAdjustment Column
            if ($('#TSFRemittance_AdjustmentReasonType').is(':visible')) {
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "")
                    return false;
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "4" && $("#TSFRemittance_OtherDesc").val().length <= 0)
                    return false;
            }
            else {
                if ($("#TSFRemittance_NegativeAdjustment").val() != "0") {
                    return false;
                }
            }
        }
        if ($("#Credit_TSFRemittance_CategoryId").val() == "" || $("#Credit_TSFRemittance_CategoryId").val() == "0") {
            return false;
        }
        if (parseFloat($("#TSFRemittance_NegativeAdjustment").val()) > 0 && $('#CreditRatePeriodDate').val() != '') {

            $("#btnAddCredit").prop("disabled", false);
            $("#btnupdateCredit").prop("disabled", false);
        }

    }

    $.fn.allCalculations = function () {
        var Quantity = parseInt($("#TSFRemittance_Quantity").val());
        if ($("#TSFRemittance_NegativeAdjustment").val() == "") {
            $("#TSFRemittance_NegativeAdjustment").val("0");
        }
        if (isNaN(Quantity)) {
            Quantity = 0;
        }

        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());

        if (isNaN(NegativeAdjustment)) {
            NegativeAdjustment = 0;
        }
        if (Quantity == 0) {
            $('#td_Total').text("0.00");
            $("#td_Net").text("0.00");
            $('#TSFRemittance_Total').val("0.00");
        }

        if (!Global.Remittance.bShowCreditPanel) {//before effective date
            if (!Global.Remittance.bShowCreditPanel) {
                if (Quantity <= NegativeAdjustment) {
                    if (!(Quantity == 0 && NegativeAdjustment == 0)) {
                        $('#modalQtyGreaterWarning').modal('show');
                    }
                    $("#TSFRemittance_NegativeAdjustment").val("0");
                    NegativeAdjustment = 0;
                }
            }
        }

        var sub = (Quantity - NegativeAdjustment);
        if(!Global.Remittance.bShowCreditPanel) { //add for 2x June
            $("#td_Net").text(sub);
            $('#TSFRemittance_Net').val(sub);
        }
        var Net = $("#td_Net").text();
        var Rate = parseFloat($("#td_Rate").text()).toFixed(2);
        var creditRate = parseFloat($("#td_CreditRate").text()).toFixed(2);

        var total = (Quantity * Rate).toFixed(2);

        if (!Global.Remittance.bShowCreditPanel) {
            total = (sub * Rate).toFixed(2);
        }

        $("#td_Total").text(addCommas(total));
        //$("#td_CreditTotal").text(addCommas(total));

        //var creditTotal = (NegativeAdjustment * creditRate).toFixed(2);
        $('#TSFRemittance_Rate').val(Rate);
        $('#TSFRemittance_Total').val(total);
        $('#TSFRemittance_Quantity').val(Quantity);

        //$('#td_CreditTotal').val(creditTotal);
    }

    $.fn.CreditAllCalculations = function () {
        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());

        if (isNaN(NegativeAdjustment)) {
            NegativeAdjustment = 0;
        }
        if (NegativeAdjustment == 0) {
            $('#td_CreditTotal').text("0.00");
        }
        var creditRate = parseFloat($("#td_CreditRate").text()).toFixed(2);

        var creditTotal = (NegativeAdjustment * creditRate).toFixed(2);
        $("#td_CreditTotal").text(addCommas(creditTotal));
        $('#TSFRemittance_NegativeAdjustment').val(NegativeAdjustment);

    }

    $.fn.showHideReasonDiv = function () {
        $("#tdnegativeAdj").hide();
        $(".reasonDesc").hide();
        if (parseInt($("#TSFRemittance_NegativeAdjustment").val()) > 0) {

            $("#tdnegativeAdj").show();

            if ($("#TSFRemittance_AdjustmentReasonType").val() == "4")
                $(".reasonDesc").show();
        }
    }

    $('.Calculation, .EditCalculation, .CreditCalculation').on('keydown', function (e) {

        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()

    });

    function BindAdjustmentReason(adjustmentval) {
        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());
        if (NegativeAdjustment > 0) {
            $('#tdnegativeAdj').removeAttr('style');
            $("#TSFRemittance_AdjustmentReasonType").val(adjustmentval);
            if (adjustmentval == 4) {
                $('.reasonDesc').show();
            }
            else {
                $('.reasonDesc').hide();
                $('.reasonDesc input').val('');
            }
        }
        else {
            $('#tdnegativeAdj').hide();;
        }
    }
    $("#TSFRemittance_Quantity").on("change", function () {
        $.fn.allCalculations();
        $.fn.showHideReasonDiv();
        $.fn.EnableDisableAddButton();
    });

    $('#negativeAdj input[name="SupportingDocuments.negativeAdj"]').click(function () {

        if ($(this).val() != '') {

            $.ajax({
                url: '/Steward/TSFRemittances/UpdateSupportingDocumentValue',
                dataType: 'json',
                type: 'POST',
                data: { claimID: $("#ClaimID").val(), docOptionValue: $(this).val() },
                success: function (data) {
                },
                failure: function (data) {
                    console.log(data);
                }
            });
        }
    });

    $('input[name="paymentTypeGroup"]').on('click', function () {
        $.ajax({
            url: '/Steward/TSFRemittances/UpdatePaymentType',
            dataType: 'json',
            data: { claimID: $("#ClaimID").val(), paymentType: this.value },
            type: 'POST',
            success: function (data) {
                if (data) {
                    if (data.Errors.length == 0) {
                        Global.Remittance.bSubmitBtnGreen = true;
                        //OTSTM2-303 need to check both supporting doc and payment type
                        //$('#submitRemittanceClaimBtn').removeAttr('disabled');
                        TSFSubmitButtonSuppDoc();

                    }
                }
            },
            failure: function (data) {
                console.log(data);
            }
        });
    });

    //Add by Frank
    $("#submitRemittanceClaimBtn").click(function () {
        if (this.innerHTML.trim() == "Submit $0 Remittance" && $('#hdCBRemittanceNilAutoApprove').val() == "1") {
            $("#btnsubmit").attr("DollarZeroFlag", "On");
        }
    });
   
    $("#btnsubmit").click(function (event) {       //OTSTM2-158 client side check, comment out
        //var activityApprovedDate = new Date(Global.Remittance.ActivityApprovedDate);
        //var activityYear = activityApprovedDate.getFullYear();
        //var activityMonth = activityApprovedDate.getMonth() + 1;
        //var selectedPeriodDate = $('#dpPeriodDate').data("datetimepicker").getDate();
        //var selectedPeriodYear = selectedPeriodDate.getFullYear();
        //var selectedPeriodMonth = selectedPeriodDate.getMonth() + 1;
        //if (Global.Remittance.ActiveStatus == "InActive") {
        //    if (selectedPeriodYear > activityYear) {
        //        $('#modalInactiveStewardParticipant').modal('show');
        //    }
        //    else if (selectedPeriodYear == activityYear && selectedPeriodMonth > activityMonth) {
        //        $('#modalInactiveStewardParticipant').modal('show');
        //    }
        //    else {
        //        submitRemittance();
        //    }
        //}
        //else {
        //    submitRemittance();
        //}
        
        //pass value to backend, do server side check
        var dt1 = new Date($("#PeriodDate").attr("datedata"));
        var selectedPeriodDate = dt1.toISOString();
        console.log(selectedPeriodDate)
        $.ajax({
            url: '/Steward/TSFRemittances/RunCreateOrSubmitRules',
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            data: JSON.stringify({ selectedPeriodDate: selectedPeriodDate, customerID: Global.ParticipantClaimsSummary.CustomerId }),
            success: function (result) {
                if (result.canCreateOrSubmit) {
                    submitRemittance();
                }
                else {
                    if (result && result.reason) {
                        $('#createTSFRemittanceMsgStaff').text(result.reason);
                    }
                    $('#modalInactiveStewardStaff').modal('show');
                }
            }
        });
    });

    var submitRemittance = function () {
        if ($("#btnsubmit").attr("DollarZeroFlag") == "On") {
            var payment = ($("#totamount>p").text() == "") ? "0.00" : $("#totamount>p").text();
            //var received = $("#PayReceiptDate").val();
            //var deposit = "";
            //var referenceNo = $("#ChequeReferenceNumber").text();
            //var PaymentType = $("input[name=paymentTypeGroup]:checked").val();
            $.ajax({
                url: '/Steward/TSFRemittances/AutoApproveRemittance',
                dataType: 'json',
                type: 'POST',
                //data: { claimID: $("#ClaimID").val(), status: "Approved", received: received, deposit: deposit, referenceNo: referenceNo, amount: payment, PaymentType: PaymentType },
                data: { claimID: $("#ClaimID").val(), status: "Approved", amount: payment },
                success: function (data) {
                    //window.location.reload(true);//OTSTM2-547
                    $("#modalSubmitRemittanceTwo").modal("show");
                },
                failure: function (data) {
                    console.log(data);
                }
            });
        }
        else {
            $.ajax({
                url: '/Steward/TSFRemittances/ChangeStatus',
                dataType: 'json',
                type: 'POST',
                data: { claimID: $("#ClaimID").val(), status: "Submitted" },
                success: function (data) {
                    //window.location.reload(true);//OTSTM2-547
                    $("#modalSubmitRemittanceTwo").modal("show");
                },
                failure: function (data) {
                    console.log(data);
                }
            });
        }
    }

    //Comment out by Frank
    //$("#btnsubmit").click(function (event) {
    //    $.ajax({
    //        url: '/Steward/TSFRemittances/ChangeStatus',
    //        dataType: 'json',
    //        type: 'POST',
    //        data: { claimID: $("#ClaimID").val(), status: "Submitted" },
    //        success: function (data) {
    //        },
    //        failure: function (data) {
    //            console.log(data);
    //        }
    //    });
    //});

    $("#modalSubmitRemittanceTwo .close").click(function (event) {
        window.location.reload();
    });
    $("#btnOk").click(function (event) {
        window.location.reload();
    });
    /*Remote Click*/

    $(".delete").click(function (event) {
        $("#hdnID").val($(this).attr("id"));
        var ItemID = $(this).parent().attr("class");
        $("#lblcategory").html($('#' + ItemID).html());

        $(this).attr('data-target', '#modalTireCountsRemove');


    });
    $("#remove").click(function (event) {
        event.preventDefault();
        $.ajax({
            url: '/Steward/TSFRemittances/DelClaimDetail',
            method: 'POST',
            dataType: "JSON",
            data: { claimID: $("#ClaimID").val(), detailId: $("#hdnID").val() },
            complete: function (result) {
                window.location.reload(true);

            }
        });
    });
    //For Upadte--------------------------------------------------------------------
    //Bind rate behalf of selected Item and Effective Period
    $(".ddlEditcategory").change(function () {
        if ($(".ddlEditcategory").val() != "0") {
            var PeriodDate = $("#PeriodDate").val();
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'POST',
                dataType: "JSON",
                data: { PeriodDate: PeriodDate, ItemID: $(".ddlEditcategory").val() },
                complete: function (result) {
                    for (var i = 0; i < result.responseJSON.length; i++) {

                        $("#edit_Rate").text(result.responseJSON[i].ItemRate);
                    }
                }
            });
        }
    });

    //Check terms and condtions

    $("#chkterms").change(function () {

        if ($(this).is(":checked")) {
            $("#btnsubmit").prop("disabled", false);

        } else {
            $("#btnsubmit").prop("disabled", true);

        }
    });

    $("#btnupdate").click(function () {
        //var form = $('fmRemittanceTireCount')
        //var data = form.serializeArray();
        //// data.push({ name: 'TSFRemittance.id', value: "16" })
        //data.push({ name: 'TSFRemittance.ClaimID', value: $("#ClaimID").val() });
        //data.push({ name: 'TSFRemittance.DetailID', value: $("#ClaimDetailID").val() });
        //data.push({ name: 'TSFRemittance.CategoryId', value: $(".ddlEditcategory").val() });
        //data.push({ name: 'TSFRemittance.Quantity', value: $("#TSFRemittance_Quantity").val() });
        //data.push({ name: 'TSFRemittance.NegativeAdjustment', value: $("#TSFRemittance_NegativeAdjustment").val() });
        //data.push({ name: 'TSFRemittance.Net', value: $('#TSFRemittance_Net').val() });
        //data.push({ name: 'TSFRemittance.Rate', value: $('#TSFRemittance_Rate').val() });
        //data.push({ name: 'TSFRemittance.Total', value: $('#TSFRemittance_Total').val() });

        //var len = data.length;
        //for (var i = 0; i < len; i++) {
        //    data[i].value = $.trim(data[i].value);
        //}
        //$.ajax({
        //    url: '/Steward/TSFRemittances/AddRemittance',
        //    dataType: 'json',
        //    type: 'POST',
        //    data: $.param(data),
        //    success: function (data) {
        //        window.location.reload(true);
        //    },
        //    failure: function (data) {
        //        console.log(data);
        //    }
        //});
    });

}

/* End Common Functions */

/* SUPPORTING DOCUMENT VALIDATION */

var validatorSupportingDocInfo = $('#fmSupportingDoc').validate({

    ignore: '.no-validate, [readonly=readonly]',

    onkeyup: function (element) {
        this.element(element);
    },
    invalidHandler: function (e, validator) {

    },
    errorPlacement: function (error, element) {

    },
    errorElement: 'p',
    errorClass: 'help-block',
    highlight: function (element) {

        var propertyName = $(element).attr('name');
        var index = panelLevelCheckErrorList.indexOf(propertyName);
        if (index > -1) {

            if ($(element).attr('name') == 'SupportingDocuments.negativeAdj') {
                $(element).closest('.form-group').removeClass('has-success').removeClass('has-required');
                $(element).closest('.form-group').addClass('has-error');
            }
        }
    },
    unhighlight: function (element) {
        var panelLevelCheckErrorList = []; //used for the panel check green tick box
        var propertyName = $(element).attr('name');
        var index = panelLevelCheckErrorList.indexOf(propertyName);
        if (index > -1) {
            if ($(element).attr('name') == 'SupportingDocuments.negativeAdj') {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required');
                $(element).closest('.form-group').addClass('has-success');
            }
        }
    },

    success: function (error) {
        error.remove();
    },
    failure: function () {
        //failure
    },
    rules: {
        'SupportingDocuments.negativeAdj': {
            inInvalidList: true
        }
    },
    messages: {
        'SupportingDocuments.negativeAdj': "Invalid Required Documents"
    }
});


var isreadonly = $('#hfIsReadOnly').text();
RemittancesDetails(isreadonly);
function RemittancesDetails(status) {
    if (status != 'undefined') {
        if (status == 'False') {
            $("input[type='text']").removeAttr("readonly");
            $(".CategoryId").removeAttr("readonly");
            $(".TSFRateDate").removeAttr("readonly");//Add for 2X
        }
    }
}
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function removeCommas(strComma) {
    var strArr = strComma.split(',');
    var strNew = '';
    for (var i = 0; i < strArr.length; i++) {
        strNew = strNew + strArr[i];
    }
    return strNew;
}

$("#CreditRatePeriodDate").on("change", function () {
    if ($("#CreditRatePeriodDate").attr('value') == $("#CreditRatePeriodDate").val()) {
        return; //month not changed.
    }
    $("#CreditRatePeriodDate").attr('value', $("#CreditRatePeriodDate").val());

    //if ($("#Credit_TSFRemittance_CategoryId").val() > 0) {
    var CreditRatePeriodDate = $("#CreditRatePeriodDate").val();

    //OTSTM2-457, enhance user experience, start--     
    $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
    $("#TSFRemittance_NegativeAdjustment").val('0');
    $.fn.showHideReasonDiv();
    $("#TSFRemittance_AdjustmentReasonType").val("");
    $("#td_CreditRate").text("0.00");
    $.fn.CreditAllCalculations();
    //--end--

    $.ajax({
        url: '/Steward/TSFRemittances/LoadCategoryByDate',
        method: 'GET',
        dataType: "JSON",
        data: { claimId: $('#ClaimID').val(), date: CreditRatePeriodDate },
        complete: function (result) {
            if (result.responseJSON != undefined) {             
                
                //$('#Credit_TSFRemittance_CategoryId').find('option').remove();
                //$('#Credit_TSFRemittance_CategoryId').append('Select Class/Category');
                //for (var i = 0; i < result.responseJSON.length; i++) {//rebuild category dropdown 
                //    var $select = $('#Credit_TSFRemittance_CategoryId');
                //    $select.append(
                //        $('<option/>', {
                //            value: result.responseJSON[i].Value,
                //            html: result.responseJSON[i].Text,
                //        })
                //     );                    
                //}


                //$("#Credit_TSFRemittance_CategoryId").change();

                //OTSTM2-457
                $("#TSFRemittance_NegativeAdjustment").prop("disabled", true); 
                $("#Credit_TSFRemittance_CategoryId").prop("disabled", false); 
                $('#Credit_TSFRemittance_CategoryId').empty();
                var $select = $('#Credit_TSFRemittance_CategoryId');
                $select.append('<option value="0">Select a Category</option>');

                for (var i = 0; i < result.responseJSON.length; i++) {
                    $select.append(
                            $('<option/>', {
                                value: result.responseJSON[i].Value,
                                html: result.responseJSON[i].Text,
                            })
                    );
                }
            }
        }
    });
    //}
});

$('#btnAddCredit').click(function () {
    var submitData = {
        ID: $("#ClaimID").val(),
        //CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
        TSFRemittance: {
            ClaimID: $('#ClaimID').val(),
            NegativeAdjDate: $('#CreditRatePeriodDate').val(),
            CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
            CreditNegativeAdj: $('#TSFRemittance_NegativeAdjustment').val(),
            CreditTSFRate: $('#td_CreditRate').text().replace(',', ''),
            CreditTSFDue: $('#td_CreditTotal').text().replace(',', ''),
            AdjustmentReasonType: $('#TSFRemittance_AdjustmentReasonType').val(),
            OtherDesc: $('#TSFRemittance_OtherDesc').val(),
        },
    }
    $(this).prop("disabled", true);

    $.ajax({
        url: '/Steward/TSFRemittances/AddRemittance',
        dataType: 'json',
        type: 'POST',
        data: submitData,
        success: function (data) {
            window.location.reload(true);
        },
        failure: function (data) {
            console.log(data);
        },
        complete : function (data) {
            window.location.reload(true);
        },
    });
});

$('#btnupdateCredit').click(function () {
    var submitData = {
        ID: $("#ClaimID").val(),
        //CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
        TSFRemittance: {
            DetailID: $('#ClaimDetailID').val(),
            ClaimID: $('#ClaimID').val(),
            NegativeAdjDate: $('#CreditRatePeriodDate').val(),
            CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
            CreditNegativeAdj: $('#TSFRemittance_NegativeAdjustment').val(),
            CreditTSFRate: $('#td_CreditRate').text().replace(',', ''),
            CreditTSFDue: $('#td_CreditTotal').text().replace(',', ''),
            AdjustmentReasonType: $('#TSFRemittance_AdjustmentReasonType').val(),
            OtherDesc: $('#TSFRemittance_OtherDesc').val(),
        },
    }

    $.ajax({
        url: '/Steward/TSFRemittances/AddRemittance',
        dataType: 'json',
        type: 'POST',
        data: submitData,
        success: function (data) {
            window.location.reload(true);
        },
        failure: function (data) {
            console.log(data);
        },
        complete : function (data) {
            window.location.reload(true);
        },
    });
});

$('#fmRemittanceTireCount').submit(function (ev) {
    $(this).append('<input type="hidden" name="TSFRemittance.ClaimID" id="TSFRemittance.ClaimID" value="' + $('#ClaimID').val() +'">');
    $(this).append('<input type="hidden" name="TSFRemittance.DetailID" id="TSFRemittance.DetailID" value="' + $('#ClaimDetailID').val() +'">');
    $(this).append('<input type="hidden" name="TSFRemittance.CategoryId" id="TSFRemittance.CategoryId" value="' + $('#TSFRemittance_CategoryId').val() + '">'); //OTSTM2-567
    $(this).append('<input type="hidden" name="TSFRemittance.TSFRateDate" id="TSFRemittance.TSFRateDate" value="' + $('#TSFRemittance_TSFRateDate').val() + '">'); //OTSTM2-567
    $('#btnAdd').prop("disabled", true);
    ev.preventDefault(); // to stop the form from submitting
    /* Validations go here */
    this.submit(); // If all the validations succeeded
});

var checkRequiredFields = function () {
    //part of OTSTM2-356
}

//OTSTM2-21
$("#modalCustomerSettingAlertOkBtn").click(function () {
    $("#PeriodDate").val("");
});