﻿"use strict";

var tsfCommonLib = angular.module("tsfCommonLib", ['ui.bootstrap', 'commonLib', 'datatables', 'datatables.scroller']);//angular-datatables.js; angular-datatables.scroller.js

//directives section
tsfCommonLib.directive("claimBreadCrumb", ['$http', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'TSFClaimSummaryBreadCrumb.html',
        scope: true,
        link: function (scope, elem, attr, ngModel) {
        },
        controller: function ($scope) {
            $http({
                url: Global.ParticipantClaimsSummary.LoadClaimPeriods,
                method: "GET",
                params: { CustomerId: Global.ParticipantClaimsSummary.CustomerId }
            }).then(function (result) {
                $scope.allClaimPeriods = result.data;
                $scope.numberOfClaimPeriods = result.data.length;
                for (var i = 0; i < result.data.length; i++) {
                    $scope.allClaimPeriods[i].Url = Global.ParticipantClaimsSummary.BreadCrumbUrl + $scope.allClaimPeriods[i].claimId;
                }
                $scope.countSelection = 1;
            });
            $scope.getClaimPeriod = function (count) {
                if (count == 1)
                    $scope.ClaimPeriods = $scope.allClaimPeriods.slice(count);
                else {
                    $scope.ClaimPeriods = $scope.allClaimPeriods;
                }
            };

            $scope.moreOnClick = function (event) {
                $scope.countSelection = $scope.numberOfClaimPeriods;
                event.stopPropagation();
            };

            $scope.$watch('countSelection', function (newVal, oldVal) {
                if (newVal != oldVal) {
                    $scope.getClaimPeriod(newVal);
                }
            }, true);
        }
    };
}]);

//service section
