﻿"use strict";

var tsfClaimLib = angular.module("tsfParticipantClaimLib", ['ui.bootstrap', 'commonLib', 'tsfCommonLib']);

tsfClaimLib.controller("tsfParticipantInternalAdjustmentController", ["$scope", "$filter", "$uibModal", "tsfclaimService", "$window", function ($scope, $filter, $uibModal, tsfclaimService, $window) {

    var initializeModalResult = function () {
        return {
            selectedItem: null,
            paymentType: 'Overall',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,
            internalNote: ''
        };
    }

    $scope.InternalAdjustTypes = [
       {
           name: 'Payment',
           id: '1'
       }
    ];
    

    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        tsfclaimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var viewItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var internalAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'InternalAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });           
        });
    }

    

    //Mail Date Picker
    $scope.datePicker = {
        status: false
    };

    $scope.openDatePicker = function ($event) {
        $scope.datePicker.status = true;
    };    

}]);

tsfClaimLib.controller('InternalAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, disableForReadOnlyStaff, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; 
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";     

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };      

}]);


//service section
tsfClaimLib.factory('tsfclaimService', ["$http", function ($http) {

    var factory = {};   

    factory.getInternalAdjustment = function (adjustmentType, internalAdjustmentId) {
        var req = {
            adjustmentType: adjustmentType,
            internalAdjustmentId: internalAdjustmentId
        }
        var submitVal = {
            url: Global.Remittance.GetInternalAdjustmentUrl,
                method: "POST",
                    data: JSON.stringify(req)
            }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    return factory;
}]);
