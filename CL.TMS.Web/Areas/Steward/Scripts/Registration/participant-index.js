﻿//Steward participant-index.js

$(function () {

    BusinessLocation();
    ContactInformation();
   
    TireDetails();
    StewardDetails();
    TermsAndConditions();

    var initialize = function () {
        titleCase();
        //restrictOnlyNumbers();
        //DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        InitAutoSave();
    }
    initialize();

    $('#submitApplicationBtn').on('click', function () {
        var Id = $("[name='ID']").val();

        $.ajax({
            url: '/Steward/Registration/SubmitApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: Id, tokenId: Global.ParticipantIndex.Resources.TokenId },
            complete: function () {
                window.location.reload();
            }
        });

    });

    //fix backspace error for checkboxes
    $('input[type="checkbox"]').keydown(function (e) {
        if (e.keyCode === 8) {
            return false;
        }
    });



});

function BusinessLocation() {
    var toggleMailingAddress = function () {
       
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    $('#MailingAddressSameAsBusiness').click(function () {

        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {
           
            CopyMailingAddress();

        }
        else {
           
            ClearMailingAddress();

        }
        toggleMailingAddress();
    });

    toggleMailingAddress();
}

function CopyMailingAddress() {

 
    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('input[name="BusinessLocation.MailingAddress.City"]').val($('#BusinessLocation_BusinessAddress_City').val())
    $('input[name="BusinessLocation.MailingAddress.Province"]').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('select[name="BusinessLocation.MailingAddress.Country"]').val($('#BusinessLocation_BusinessAddress_Country').val())


}

function ClearMailingAddress() {


    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val('')
    $('input[name="BusinessLocation.MailingAddress.City"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Province"]').val('')
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;

}

function ContactInformation() {
    $('.isContactAddressSameAsBusinessAddress').on('click', function () {
        contactAddressSameAs();
    });

    //#region
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        contactTemplate = "<div class='template" + i + "'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }
    $('#btnAddContact').on('click', function () {
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
        titleCase();
    });

    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {

            currAddCount = contactTemplateCount++;
            temp = "<div class='ctemp'><div class='template" + currAddCount + "'>" + temp + '</div></div><hr>';
            $(temp).appendTo('#divContactTemplate');

            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }
    
    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            /*
                         if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
            
                             if ($(this).is(":checked")) {
                                 $(this).closest('.row').next('.contactAddressDifferent').hide();
                             }
                             else {
                                 $(this).closest('.row').next('.contactAddressDifferent').show();
                             }
                         }
            */
        });
    }

    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }
    primaryContactCheckName();

}

function TireDetails() {
    $('input.numberinput').on('change', function () {
        var number = Number(this.value);
        if (number) {
            if (0.0 > number) {
                this.value = '0';
            }
        }
        else {
            this.value = '0';
        }
    });

}

function StewardDetails() {
 
    //applies to radiobutton for StewardDetails.StewardDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });


    $('#StewardDetails_StewardDetailsVendor_WSIBNumber').on('focusout', function () {
        DisplayWSIBDocumentOption();
    });

    $('#StewardDetails_StewardDetailsVendor_CommercialLiabHSTNumber').on('focusout', function () {
        DisplayHSTDocumentOption();
    });

    $('#StewardDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });

    $('input[type="radio"]').click(function () {
        // For Steward Cvor detail.
        if ($(this).attr('id') == 'StewardDetails_StewardDetailsVendor_HIsGVWR') {
            if ($(this).val() == 'True') {
                $('#CVORNumberSteward').show();
                $('#CVORExpiryDateSteward').show();
            }
            else {
                $('#CVORNumberSteward').hide();
                $('#CVORExpiryDateSteward').hide();
            }
        }
        //If Steward have 1 or more employee.
        if ($(this).attr('id') == 'StewardDetails_StewardDetailsVendor_HasMoreThanOneEmp') {
            if ($(this).val() == 'True') {
                $('#WSIBNumberSteward').show();
            }
            else {
                $('#WSIBNumberSteward').hide();
            }
        }
    });

    var disabledHSTTextBox = function () {
        if (!($('#StewardDetails_IsTaxExempt').attr('disabled') == 'disabled')) {

            if ($('#StewardDetails_IsTaxExempt').is(':checked')) {
                $('#StewardDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
            }
            else {
                $('#StewardDetails_CommercialLiabHstNumber').attr('readonly', false);
            }
        }
    }


    $('#StewardDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });
    disabledHSTTextBox();
    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    $('#dpBusinessStartDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpInsuranceExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });
}

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
}


/* Common Functions */

var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
        //$(this).closest('.row').next('.contactAddressDifferent').find('input:checkbox').removeAttr('checked')
    });
}

//capitalize first letter of address
var titleCase = function () {
    $('.title-case').on('keyup', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}

function processJson() {    
    var myform = $('#MainId');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var data = $('form').serializeArray();
    data = data.concat(
        $('#MainId input[type=checkbox]:not(:checked)').map(
            function () {
                return { "name": this.name, "value": this.value }
            }).get());
    data = data.concat(
        $('#MainId input[type=radio]:not(:checked)').map(
            function () {
                return { "name": this.name, "value": null }
            }).get());
    //data.push({ name: 'ID', value: $('#hdnRegId').val() });

    $('#item_checkbox input[type="checkbox"]').each(function () {

        if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
            data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
        }
    });

    if ($('#MailingAddressSameAsBusiness').is(':checked')) {
        data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
    }
    disabled.attr('disabled', 'disabled');

    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant') {
        //insert current invalid forms back into model
        var tmpList = Global.ParticipantIndex.Model.InvalidFormFields;
        for (var i = 0; i < tmpList.length; i++) {
            data.push({ name: 'InvalidFormFields[]', value: tmpList[i] });
        }
    }

    if (Global.ParticipantIndex.Model.Status == 'Open') {
        var len = data.length;
        for (var j = 0; j < len; j++) {
            data.push({ name: 'InvalidFormFields[]', value: data[j].name });
        }
    }
    //remove leading and trailing space
    var len = data.length;
    for (var i = 0; i < len; i++) {

        data[i].value = $.trim(data[i].value);
    }

   xhr = $.ajax({
        url: '/Steward/Registration/SaveModel',
        dataType: 'json',
        type: 'POST',
        data: $.param(data),
        success: function (result) {
            //initclass();
        },
        error: function (result) {
        }
   });
   //abortAjax(xhr);
}

function abortAjax(xhr) {
    if(xhr && xhr.readystate != 4){
        xhr.initclass();
    }
}

var certificateOfApproval = function () {
    var allSortYards = 0;
    var capp = $("input[data-maxstorage]");
    capp.each(function () {

        $(this).attr('data-maxstorage', $(this).val());

        if (parseFloat($(this).attr('data-maxstorage')) > 0) {
            allSortYards++;
        }
        if (parseFloat($(this).attr('data-maxstorage')) < 50) {
            var certApp = $(this).parents('.row').find('.COA');
            $(certApp).find('input:text').each(function () {
                $(this).val('');
            });
            $(this).parents('.row').find('.COA').hide();
        }
        else {
            $(this).parents('.row').find('.COA').show();
        }
    });
    $('#SortYardCount').text(allSortYards);
}

var totalStorage = function () {
    var maxStorageCap = 0;
    var inputs = $("input[data-maxstorage]");
    inputs.each(function () {
        if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
            maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        }
    });
}

//function DisplayCVORDocumentOption() {
//    var show = ($('#StewardDetails_StewardDetailsVendor_CVORNumber').val().length > 0);
//    if (show) {
//        $('#cVORDocument').show();
//    }
//    else {
//        $('#cVORDocument').hide();
//    }
//}

function DisplayWSIBDocumentOption() {
    //var show = ($('#StewardDetails_StewardDetailsVendor_WSIBNumber').val().length > 0);
    var show = ($('#StewardDetails_HasMoreThanOneEmp:checked').val() == 'True');

    if (show) {
        $('#wSIBDocument').show();
    }
    else {
        $('#wSIBDocument').hide();
    }
}

function DisplayHSTDocumentOption() {
    function DisplayHSTDocumentOption() {
        var show = !$('#HaulerDetails_IsTaxExempt').is(':checked');
        $('#hSTDocument').toggle(show);
        PanelGreenCheckSuppDoc();
    }
}

function _isEmpty(value) {
    return (value == null || value.length === 0);
}

function InitAutoSave() {
    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        $('#MainId :input').eq(0).trigger('change');
    });


    $('#MainId :input').on('change', function () {
        var appsteward = $(this);
        $.when(
        appsteward.focusout()).then(function () {
            //certificateOfApproval();
            processJson();
            //totalStorage();
            contactAddressSameAs();

        });
    });

    $('#MainId').on('change', '.ctemp', function () {

        var ctemp = $(this);
        $.when(
        ctemp.focusout()).then(function () {
            certificateOfApproval();
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });


}

/* End Common Functions */

/* Gabe UI styles */
(function () {

    // Popover Setup
    var settings = {
        trigger: 'hover',
        //title:'Pop Title',
        //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
        //width:300,            
        multi: true,
        closeable: true,
        style: '',
        delay: { show: 300, hide: 800 },
        padding: true
    };


    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
      businessLocationLBNSettings = {
          content: businessLocationLBNContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
      businessLocationBONSettings = {
          content: businessLocationBONContent,
          width: 300,
          height: 100,
      };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationBONSettings));

    // Sort Yard Details: All Sort Yards Capacity
    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
      sortYardDetailsASYCSettings = {
          content: sortYardDetailsASYCContent,
          width: 300,
          height: 100,
      };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsASYCSettings));

    // Sort Yard Details: This Sort Yard Capacity
    var sortYardDetailsTSYCContent = $('#sortYardDetailsTSYC').html(),
      sortYardDetailsTSYCSettings = {
          content: sortYardDetailsTSYCContent,
          width: 300,
          height: 100,
      };
    var popLargeTSYC = $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsTSYCSettings));



    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
      tiredetailsPopoverPLTSettings = {
          content: tiredetailsPopoverPLTContent,
          width: 500,
          height: 600,
      };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
      tiredetailsPopoverMTSettings = {
          content: tiredetailsPopoverMTContent,
          width: 380,
          height: 155,
      };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverAGLSSettings));
    // Tire Details: Pneumatic Tires (OTR) 
    var tiredetailsContent = $('#tiredetailsPopoverOTR').html(),
      tiredetailsSettings = {
          content: tiredetailsContent,
          width: 380,
          height: 109,
      };
    var popLargeMT = $('.tiredetailsPopoverOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));
   // Solid & Resilient Tires (OTR)
    var tiredetailsContent = $('#tiredetailsPopoverOTRS').html(),
  tiredetailsSettings = {
      content: tiredetailsContent,
      width: 380,
      height: 150,
  };
    var popLargeMT = $('.tiredetailsPopoverOTRS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    //Steward Details

    var tiredetailsContent = $('#StewardOriginalEquipmentManufactureOEM').html(),
tiredetailsSettings = {
    content: tiredetailsContent,
    width: 380,
    height: 66,
};
    var popLargeMT = $('.StewardOriginalEquipmentManufactureOEM').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    var tiredetailsContent = $('#StewardTireManufactureBrandOwner').html(),
tiredetailsSettings = {
    content: tiredetailsContent,
    width: 380,
    height: 230,
};
    var popLargeMT = $('.StewardTireManufactureBrandOwner').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));



    var tiredetailsContent = $('#StewardFirstImporter').html(),
tiredetailsSettings = {
    content: tiredetailsContent,
    width: 380,
    height: 150,
};
    var popLargeMT = $('.StewardFirstImporter').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    // Tire Details & Service Area: Zone 1
    var serviceAreaZone1Content = $('#serviceAreaZone1').html(),
      serviceAreaZone1Settings = {
          content: serviceAreaZone1Content,
          width: 674,
          height: 359,
      };
    var popLargeVOCH = $('.serviceAreaZone1').webuiPopover('destroy').webuiPopover($.extend({}, settings, serviceAreaZone1Settings));





})();

// Script; Expand on checkbox deselect
$('.isMailingAddressDifferent').click(function () {
    $(".mailingAddressDifferent").toggle(this.unchecked);
});
$('.isContactAddressDifferent').click(function () {
    $(".contactAddressDifferent").toggle(this.unchecked);
});
$('.isBankTransferNotficationPrimaryContact').click(function () {
    $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
});
/* End Gabe UI Styles */