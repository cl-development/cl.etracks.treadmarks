﻿
$(function () {
    "use strict";
    BusinessLocation();
    ContactInformation();
    TireDetails();
    StewardDetails();

    AuditGroupSelection();
    SupportingDocuments();
    TermsAndConditions();

    //Sremit section
    $('.btn-ram').on('click', function () {

        var ColName = $(this).attr("id");
        var ColValue = $(this).val();

        switch (ColName) {
            case 'SMOEOn':
                Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + 'changed'.strongMe() + "participant's MOE from" + 'Off'.strongMe() + 'to' + 'On'.strongMe(); }
                Global.CommonActivity.ActivitiesLog.hasAct = true;
                break;
            case 'SMOEOff':
                Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + 'changed'.strongMe() + "participant's MOE from" + 'On'.strongMe() + 'to' + 'Off'.strongMe(); }
                Global.CommonActivity.ActivitiesLog.hasAct = true;
                break;
            case 'SRemitsPerYear2x':
                Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Remits per year from" + '12x'.strongMe() + 'to' + '2x'.strongMe(); }
                Global.CommonActivity.ActivitiesLog.hasAct = true;
                break;
            case 'SRemitsPerYear12x':
                Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Remits per year from" + '2x'.strongMe() + 'to' + '12x'.strongMe(); }
                Global.CommonActivity.ActivitiesLog.hasAct = true;
                break;
            default:
                break;
        }

        var jsonObj = { ID: ColValue, vendorId: $('#VendorID').val(), PropName: ColName };
        if (jsonObj != null && typeof jsonObj != 'undefined') {
            $.ajax({
                url: '/Steward/Registration/SaveItem',
                dataType: 'json',
                type: 'POST',
                data: jsonObj,
                success: function (data) {
                    if (Global.CommonActivity.ActivitiesLog.hasAct && Global.CommonActivity.ApplicationStatus != 'Open' && Global.CommonActivity.ApplicationStatus != 'None') {
                        addActivityLog(Global.CommonActivity.ActivityArea,null);
                    }
                    window.location.reload(true);
                },
                failure: function (data) {
                    console.log(data);
                },
            });
        } else {
            donotReload = true;
            alert('Please check Data validation.');
        }
    });

    var initialize = function () {
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        contactAddressSameAs();
        ApplicationCommon.setContactAddressCountryDropDown();
        restrictOnlyNumbers();
        InitAutoSave();
        titleCase();
        setExportUrl();
        //fix backspace error for checkboxes
        $('input[type="checkbox"]').keydown(function (e) {
            if (e.keyCode === 8) {
                return false;
            }
        });
    }
    initialize();
});

function setExportUrl() {
    var url = Global.StaffIndex.ExportToPdfUrl;
    $('#exportToPdf').attr('href', url);

}

function BusinessLocation() {
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    $('#MailingAddressSameAsBusiness').click(function () {

        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {

            CopyMailingAddress();

        }
        else {
            ClearMailingAddress();

        }

        toggleMailingAddress();
    });
    toggleMailingAddress();
}

function CopyMailingAddress() {

    $('#BusinessLocation_MailingAddress_AddressLine1').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('#BusinessLocation_MailingAddress_City').val($('#BusinessLocation_BusinessAddress_City').val())
    $('#BusinessLocation_MailingAddress_Province').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('#BusinessLocation_MailingAddress_AddressLine2').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('#BusinessLocation_MailingAddress_Postal').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('#BusinessLocation_MailingAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val())


}

function ClearMailingAddress() {

    $('#BusinessLocation_MailingAddress_AddressLine1').val('')
    $('#BusinessLocation_MailingAddress_City').val('')
    $('#BusinessLocation_MailingAddress_Province').val('')
    $('#BusinessLocation_MailingAddress_AddressLine2').val('')
    $('#BusinessLocation_MailingAddress_Postal').val('')
    $('#BusinessLocation_MailingAddress_Country').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;

}

function ContactInformation() {
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();
    if (temp != undefined) {
        temp = temp.replace(/value=\".+\"/g, "value=''"); //OTSTM2-50 remove all default value of input
    }
    
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        var sectionname = $("#contactTemplate" + i).data('sectionname').replace('Contact 0', 'Primary Contact');
        contactTemplate = "<div class='template" + i + " parent' data-sectionname='" + sectionname + "'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    var initCheckboxClearField = function () {
        $('input[type="checkbox"]').on('click', function () {
            clearField(this);
        });
    }

    $('#btnAddContact').on('click', function () {
        initCheckboxClearField();
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
        var logName = BuildActivityLogContent(this);
        saveActivies(logName);
    });
    $('.isContactAddressDifferent').click(function () {
        $(".contactAddressDifferent").toggle(this.unchecked);
    });
    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            var sectionname = $("#contactTemplate0").data('sectionname').replace('Contact 0', 'Contact ' + (contactTemplateCount));
            currAddCount = contactTemplateCount++;
            tempDiv = "<div class='ctemp'><div class='template" + currAddCount + " parent' data-sectionname='" + sectionname + "'>" + temp + '</div></div><hr>'; //OTSTM2-50 change temp to tempDiv to avoid multiple <hr> after removing and adding
            $(tempDiv).appendTo('#divContactTemplate');
            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }

            //Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + 'added'.strongMe() + 'the' + sectionname.strongMe() + 'in' + 'Contact Information'.strongMe() + 'panel'; }
            //addActivityLog(Global.CommonActivity.ActivityArea, null);
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }
    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }

    $(".dropdownCountry").change(function () {
        var optionVal = $('option:selected', this).text();
        $(this).prev('input').val(optionVal);
    });

    primaryContactCheckName();

    //OTSTM2-50 adding for new added contact autosave
    $(document).on("change", ".ctemp :input", function (evt) {
        var logName=BuildActivityLogContent(this);
        //var IsStewardAutoSaveDisabled = Global.StaffIndex.Security.IsStewardAutoSaveDisabled.toLowerCase() == 'true';

        evt.stopImmediatePropagation(); //avoid being fired twice

        //save textbox, checkbox or select option value to the template, keep value for switch from contact2 to contact1
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            var value = ctemp.is(':checked');
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;
            }
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
            this.defaultChecked = value;
        }
        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country") >= 0) {
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }
        $.when(
        ctemp.focusout()).then(function () {   
            saveActivies(logName);
            contactAddressSameAs();
        });
    });

    //OTSTM2-50 
    $(document).on("click", ".btnRemoveContact", function (evt) {
        var logName = BuildActivityLogContent(this);
        evt.stopImmediatePropagation();
        var contactSection = $(this).parent().parent();
        var fullName = contactSection.find('input[data-fieldname="First Name"]').val() + ' ' + contactSection.find('input[data-fieldname="Last Name"]').val();
        if (Global.StaffIndex.Settings.IsApprovedApplication) { //for registered application
            clearAllInput(contactSection);

            contactTemplateCount--;
            if (contactSection.parent().hasClass("ctemp")) { //for new added contact without refresh page
                if (contactSection.hasClass("template1") && $(".template2").length > 0) { //remove contact1, switch contact2 to contact1
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");
                    $(".template1").attr("data-sectionname", "Contact 1");
                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                }
                else { //remove contact2 or remove contact1 (no contact2)
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                }
            }
            else { //for existing contact
                if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                    contactSection.next().remove();
                    contactSection.remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");
                    $(".template1").attr("data-sectionname", "Contact 1");
                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                    //to remember selected country when switching from contact2 to contact1
                    var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                    switch (selectedValue) {
                        case "Canada":
                            $(".template1").find("select option:contains('Canada')").prop('selected', true);
                            break;
                        case "USA":
                            $(".template1").find("select option:contains('USA')").prop('selected', true);
                            break;
                        case "Other":
                            $(".template1").find("select option:contains('Other')").prop('selected', true);
                            break;
                        case "":
                            $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                            break;
                    }
                }
                else {
                    contactSection.next().remove();
                    contactSection.remove();
                }
            }
            $('#btnAddContact').closest('div.row').show();
            saveActivies(logName);
        }
        else { //for unregistered application
            contactSection.addClass("contactToBeDeleted"); //add a temporary class to remember the contact that to be removed
            $('#modalRemoveContactYesBtn').attr('fullname', fullName);
            $("#modalRemoveContactConfirmation").modal("show");

            var currentContactClassName = contactSection.attr("class");
            var index = currentContactClassName.indexOf("template");
            var value = currentContactClassName.charAt(index + 8);
            var msg = "Are you sure you want to remove Contact " + value + "?";
            $('#modalRemoveContactConfirmation').on('shown.bs.modal', function () {
                $("#contactRemovingMsg").text(msg);
            });
        }
    });

    //remove above temporarily added class when click "Cancel" button in Modal
    $('#modalRemoveContactConfirmation').on('hidden.bs.modal', function () {
        $(".contactToBeDeleted").removeClass("contactToBeDeleted");
    });

    $(document).on("click", "#modalRemoveContactYesBtn", function (evt) {

        var fuleName = $(this).attr('fullname');
        var contactSection = $(".contactToBeDeleted");
        clearAllInput(contactSection);

        contactTemplateCount--;
        if (contactSection.parent().hasClass("ctemp")) {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");
                $(".template1").attr("data-sectionname", "Contact 1");
                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

            }
            else {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
            }
        }
        else {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.next().remove();
                contactSection.remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");
                $(".template1").attr('data-fieldname', 'Contact 1');
                $(".template1").attr("data-sectionname", "Contact 1");
                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

                var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                switch (selectedValue) {
                    case "Canada":
                        $(".template1").find("select option:contains('Canada')").prop('selected', true);
                        break;
                    case "USA":
                        $(".template1").find("select option:contains('USA')").prop('selected', true);
                        break;
                    case "Other":
                        $(".template1").find("select option:contains('Other')").prop('selected', true);
                        break;
                    case "":
                        $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                        break;
                }
            }
            else {
                contactSection.next().remove();
                contactSection.remove();
            }
        }
        $('#btnAddContact').closest('div.row').show();
        //processJson(); //to autosave
        logName = "remove contact from contact information panel";
        saveActivies(logName);  
    });

    var clearAllInput = function (element) {
        element.find("input[type=text]").val("");
        element.find("input[type=checkbox]").prop("checked", false);
        element.find("select").find('option').removeAttr('selected');
        element.find("input[type=hidden]").val("0");
    }

    var replaceHtml = function (html) {
        var index = html.indexOf("ContactInformationList");
        var value = html.charAt(index + 23);
        var replaceValue = value - 1;
        var rp1 = "ContactInformationList_" + replaceValue;
        var rp2 = "ContactInformationList[" + replaceValue;
        var html = html.replace(/ContactInformationList_\d/g, rp1).replace(/ContactInformationList\[\d/g, rp2);
        return html;
    }
}

function TireDetails() {
    $('input.numberinput').on('change', function () {
        var number = Number(this.value);
        if (number) {
            if (0.0 > number) {
                this.value = '0';
            }
        }
        else {
            this.value = '0';
        }
    });
}

function StewardDetails() {

    //event handler
    $('.audit-btn').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })

    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    $('#StewardDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });

    var disabledHSTTextBox = function () {
        if (!($('#StewardDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#StewardDetails_IsTaxExempt').is(':checked'))
                $('#StewardDetails_CommercialLiabHstNumber').val('').attr('disabled', 'disabled');
            else
                $('#StewardDetails_CommercialLiabHstNumber').removeAttr('disabled');
        }
    }
    $('#StewardDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });
    disabledHSTTextBox();
    
    var calendar_readonly = Global.StaffIndex.Security.StewardDetailsCalendarReadonly.toLowerCase() == 'true';

    if (!calendar_readonly) {
        $('#dpBusinessStartDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: Global.StaffIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
        });

        $('#dpInsuranceExpiryDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: Global.StaffIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
        });
    }
}

function SupportingDocuments() {
    $("div #panelSupportingDocuments [type=checkbox]").each(function () {
        $(this).attr('name', $(this).attr('id'));
    });
};

function TermsAndConditions() {
    //code here
}

function AuditGroupSelection() {

    $('#listOfStaff').empty().append('<option value="0" selected="selected"></option>');
    $("#modalAssignAssignBtn").attr("disabled", "disabled");

    $.ajax({
        url: '/Steward/Registration/GetUsersInAuditGroup',
        method: 'GET',
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, data) {
                $('#listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')
            });
        },
        failure: function (data) {

        },
    });
}

var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
    });
}
var saveActivies = function (logName) {
    switch (Global.StaffIndex.Settings.status) {
        case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
        case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
        case Global.StaffIndex.Settings.ApplicationStatus.Approved:
        case Global.StaffIndex.Settings.ApplicationStatus.Completed:
            if (Global.CommonActivity.ActivitiesLog.hasAct) {
                Global.CommonActivity.LogHistory.push({
                    name: logName, value: Global.CommonActivity.ActivitiesLog.actLogStr(), actObj: Global.CommonActivity.ActivitiesLog
                });
                Global.CommonActivity.ActivitiesLog.hasAct = false;
            }
            break;
        default:
            if (Global.StaffIndex.Security.IsStewardAutoSaveDisabled.toLowerCase() != 'true') {
                processJson();
            }
            else {
                $('#modalSaveError').modal('toggle')
            }
            break;
    }
}
var handleBusinessAddressCheckboxForContact = function () {

    $('.ContactAddressSameAsBusinessAddress').each(function () {
        var isChecked = $(this).is(':checked');
        $(this).next('input[type="hidden"]').val(isChecked);
    });

}

function InitAutoSave() {

    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        clearField(this);
        $('#MainId :input').eq(0).trigger('change');
    });

    $(document).on("change", "#MainId :input:not(#inactivedropdown):not(#activedropdown)", function (evt) { //OTSTM2-50 change the "Change" event
        var logName = BuildActivityLogContent(this);

        evt.stopImmediatePropagation(); //OTSTM2-50

       // var IsStewardAutoSaveDisabled = Global.StaffIndex.Security.IsStewardAutoSaveDisabled.toLowerCase() == 'true';

        var appsteward = $(this);
        if (appsteward.is(':checkbox')) {
            clearField(appsteward);
        }

        //save textbox, checkbox or select option value to the template, keep value for switch from contact2 to contact1
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            if (ctemp[0].name != "TireDetails.TireItem") {
                var value = ctemp.is(':checked');
            }
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;
            }
            if (ctemp[0].name != "TireDetails.TireItem") {
                this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
                this.defaultChecked = value;
            }
        }

        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country") >= 0) { //OTSTM2-899 add condition "!ctemp.hasClass("primary-business-activity-dropdown")"
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }

        $.when(
        appsteward.focusout()).then(function () {
            saveActivies(logName);
            contactAddressSameAs();
        });
    });
};

var processJson = function () {
    
    if ($('#status').val() != 'Approved') {
        var form = $('form');

        //enable disabled controls which have classes and reenable them after        
        var tmpEnabled = form.find('input.sort-yard-address-disabled,select.sort-yard-address-disabled').removeAttr('disabled');
        var disabled = form.find(':input:disabled').removeAttr('disabled');

        var data = form.serializeArray();

        tmpEnabled = tmpEnabled.attr('disabled', 'disabled');
        disabled.attr('disabled', 'disabled');

        data = data.concat(
            $('#MainId input[type=checkbox]:not(:checked)').map(
                function () {
                    return { "name": this.name, "value": this.value }
                }).get());

        data = data.concat(
            $('#MainId input[type=radio]:not(:checked)').map(
                function () {
                    return { "name": this.name, "value": null }
                }).get());

        $('input[type="checkbox"]').each(function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }
                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }

        //remove leading and trailing space
        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        var IsStewardAutoSaveDisabled = Global.StaffIndex.Security.IsStewardAutoSaveDisabled.toLowerCase() == 'true';
        if (!IsStewardAutoSaveDisabled) {
            $.ajax({
                url: '/Steward/Registration/SaveModel',
                dataType: 'json',
                type: 'POST',
                data: $.param(data),
                success: function (result) {
                    if (Global.CommonActivity.ActivitiesLog.hasAct && Global.CommonActivity.ApplicationStatus != 'Open' && Global.CommonActivity.ApplicationStatus != 'None') {
                        addActivityLog(Global.CommonActivity.ActivityArea, null);
                    }
                    if (result != 'Success')
                        console.log('Failuer in Auto Save: ' + result);
                },
                error: function (result) {
                }
            });
        }
        else {
            $('#modalSaveError').modal('toggle')
        }
    }
}

/* COMMON FUNCTIONS */

var restrictOnlyNumbers = function () {
    $(".only-numbers").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    });
}
function DisplayWSIBDocumentOption() {
    var show = ($('#StewardDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
    $('#pnlWSIB').toggle(show);
}
function DisplayHSTDocumentOption() {
    var show = !$('#StewardDetails_IsTaxExempt').is(':checked');

    //$('#hSTDocument').toggle(show);
    if (show) {
        $('#hSTDocument').show();
        $('#StewardDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
        $('#StewardDetails_CommercialLiabHstNumber').attr('readonly', false);
    }
    else {

        $('#hSTDocument').hide();
        $('#StewardDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
        $('#StewardDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
    }
}
function _isEmpty(value) {
    return (value == null || value.length === 0);
}
var clearField = function (element) {
    if (!$(element).is(':checked')) {
        var nameAttr = $(element).attr('data-att-chkbox');
        if (nameAttr) {
            if ($('input[name="' + nameAttr + '"]').length > 0) {
                var control = $('input[name="' + nameAttr + '"]');

                if (control.attr('type') == 'radio') {
                    control.prop('checked', false);
                }
                else if (control.attr('type') == 'checkbox') {
                    control.prop('checked', false);
                }


                else {
                    control.val('');
                }
            }
            else if ($('select[name="' + nameAttr + '"]').length > 0) {
                $('select[name="' + nameAttr + '"]').find('option').removeAttr('selected');
            }
            else if ($('textarea[name="' + nameAttr + '"]').length > 0)
            {
                $('textarea[name="' + nameAttr + '"]').val('');
            }
        }
    }
}

function GetValidationErrorMessages() {
    var validationErrorMessages = [];

    var validationCheckBoxes = $('INPUT.validation-checkbox:checkbox:not(:checked)');
    $.each(validationCheckBoxes, function () {
        var validationMessage = $(this).data('validate-message');
        validationErrorMessages.push(validationMessage);
    });
    var additionalReasons = $('#modelDenyAdditionalReasonInput').val();
    if (additionalReasons.length != '' && additionalReasons.length > 0) {
        validationErrorMessages.push(additionalReasons);
    }

    var uploadedFilesValidationMessage = $('.upload-file-selection');
    $.each(uploadedFilesValidationMessage, function () {
        var validationMessage = $(this).attr('data-uploadedFile-validation-message');
        if (validationMessage != undefined && validationMessage != '') {
            validationErrorMessages.push(validationMessage);
        }
    });

    return validationErrorMessages;
}
function titleCase() {
    $('.title-case').on('keypress', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}

/* END COMMON FUNCTIONS */

/* WORKFLOW CODE */
$(function () {
    $(window).load(function () {
        $(document).ready(function () {

            $('#modalDeny').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApplicant').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOnhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOffhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalAssign').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApproveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalResend').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalSaveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalCancelApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            //ActiveInactive

            $('#switchInactiveToActive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#switchActiveToInactive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });
        });
    });

    var RegistrantStatus;
    var approvedActiveDate = $('.RegistrantActiveStatusApprovedDate').val();
    var approvedInActiveDate = $('.RegistrantInActiveStatusApprovedDate').val();

    $('#datetimepickerToInactive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedInActiveDate
    });
    $('#datetimepickerToActive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedActiveDate
    });

    if ($('#activedropdown option:selected').index() == 6) {
        $('#OtherActiveStatus').show();
    }

    if ($('#inactivedropdown option:selected').index() == 11) {
        $('#OtherInActiveStatus').show();
    }

    $('#modalDeactivatebtn').prop('disabled', false);
    if ($('#inactivedropdown option:selected').index() == 0 || $('#InActiveRegistrantStatusChangeEffectiveDate').val() == '') {
        $('#modalDeactivatebtn').prop('disabled', true);
    }
    else {
        if (($('#inactivedropdown option:selected').index() == 11) && ($('#OtherInActiveStatus').val() == '' &&
            $('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')) {
            $('#modalDeactivatebtn').prop('disabled', true);
        }
    }

    //dropdown list change
    $("#activedropdown").change(function () {
        if (this.selectedIndex == 6) {
            $('#OtherActiveStatus').show();
        }
        else {
            $('#OtherActiveStatus').hide();
            $('#OtherActiveStatus').val('');
        }
    });

    $("#inactivedropdown").change(function () {
        if (this.selectedIndex == 0)
            $('#modalDeactivatebtn').prop('disabled', true);
        else if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
            $('#modalDeactivatebtn').prop('disabled', true);
        else if (this.selectedIndex == 11 && $('#OtherInActiveStatus').val() == '')
            $('#modalDeactivatebtn').prop('disabled', true);
        else {
            $('#modalDeactivatebtn').prop('disabled', false);
        }

        if (this.selectedIndex == 11) {
            $('#OtherInActiveStatus').show();
        }
        else {
            $('#OtherInActiveStatus').hide();
            $('#OtherInActiveStatus').val('');
        }
    });

    $('#OtherInActiveStatus').on('change', function () {
        var OtherInActiveStatus = $(this);
        $.when(
        OtherInActiveStatus.focusout()).then(function () {
            if ($('#OtherInActiveStatus').val() == '')
                $('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
                $('#modalDeactivatebtn').prop('disabled', true);
            else
                $('#modalDeactivatebtn').prop('disabled', false);
        });
    });

    if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $('#RegistrantStatusChangeEffectiveDate').val());
    }
    if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $('#InActiveRegistrantStatusChangeEffectiveDate').val());
    }

    $('#RegistrantStatusChangeEffectiveDate').on('change', function () {
        var RegistrantStatusChangeEffectiveDate = $(this);
        $.when(
            RegistrantStatusChangeEffectiveDate.focusout()).then(function () {
                $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $(this).val());
            });
    });

    $('#InActiveRegistrantStatusChangeEffectiveDate').on('change', function () {
        var InActiveEffectiveDate = $(this);
        $.when(
        InActiveEffectiveDate.focusout()).then(function () {
            $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $(this).val());

            if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
                $('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#inactivedropdown option:selected').index() == 0)
                $('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#inactivedropdown option:selected').index() == 0 && $('#OtherInActiveStatus').val() == '')
                $('#modalDeactivatebtn').prop('disabled', true);
            else
                $('#modalDeactivatebtn').prop('disabled', false);
        });
    });

    $('#modalActivateBtn').on('click', function () {

        //check if date and reason are filled in
        if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {

            var jsonObj;

            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = d.getFullYear() + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                (day < 10 ? '0' : '') + day;

            Global.CommonActivity.Message = GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Application status from" + 'Inactive'.strongMe() + 'to' + 'Active'.strongMe();
            jsonObj = activeInactiveProcess("Activate");

            if (jsonObj != null && typeof jsonObj != 'undefined') {
                $.ajax({
                    url: '/System/Common/ActiveInactiveCustomer',
                    dataType: 'json',
                    type: 'POST',
                    data: { customerActiveHistory: jsonObj, Activity: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, ActivityType: Global.CommonActivity.ActivityType, Message: Global.CommonActivity.Message } },
                    success: function (data) {
                        if (data.isValid) {
                            location.reload();
                        }
                    },
                    failure: function (data) {

                    },
                });
            }
        }
    });

    $('#modalDeactivatebtn').on('click', function () {

        if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0 && $("#inactivedropdown").val().length > 0) {
            if ($('#inactivedropdown option:selected').index() != 0) {
                var jsonObj;
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day;

                Global.CommonActivity.Message = GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Application status from" + 'Active'.strongMe() + 'to' + 'Inactive'.strongMe();
                jsonObj = activeInactiveProcess("Deactivate");

                if (jsonObj != null && typeof jsonObj != 'undefined') {
                    $.ajax({
                        url: '/System/Common/ActiveInactiveCustomer',
                        dataType: 'json',
                        type: 'POST',
                        data: { customerActiveHistory: jsonObj, Activity: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, ActivityType: Global.CommonActivity.ActivityType, Message: Global.CommonActivity.Message}},
                        success: function (data) {
                            if (data.isValid) {
                                location.reload();
                            }
                        },
                        failure: function (data) {

                        },
                    });
                }
            }
        }
    });

    var activeInactiveProcess = function (activeStatus) {
        item = {};
        item['CustomerID'] = $('.VendorID').val();

        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;

        switch (activeStatus) {
            case "Activate":
                item["Reason"] = $("#activedropdown").val();
                item["ActiveStateChangeDate"] = combineDateAndTime($('#RegistrantStatusChangeEffectiveDate').val());
                item["ActiveState"] = true;
                item["OtherReason"] = $('#OtherActiveStatus').val();
                item["CreateDate"] = combineDateAndTime(output);
                break;
            case "Deactivate":
                item["Reason"] = $("#inactivedropdown").val();
                item["ActiveStateChangeDate"] = combineDateAndTime($('#InActiveRegistrantStatusChangeEffectiveDate').val());
                item["ActiveState"] = false;
                item["Description"] = $('#NewRegistrationNumber').val();
                item["OtherReason"] = $('#OtherInActiveStatus').val();
                item["CreateDate"] = combineDateAndTime(output);
                break;
            case "ActivateOnlyDate":
                item["ActiveStateChangeDate"] = $('#RegistrantStatusChangeEffectiveDate').val();
                item["ActiveState"] = 1;
                break;
        }
        return item;
    }

    var combineDateAndTime = function (dateString) {

        var currDate = new Date();
        var timeString = currDate.getHours() + ':' + currDate.getMinutes() + ':' + currDate.getSeconds();

        var datetime = new Date(dateString + ' ' + timeString);

        // format the output
        var month = datetime.getMonth() + 1;
        var day = datetime.getDate();
        var year = datetime.getFullYear();

        var hour = datetime.getHours();
        if (hour < 10)
            hour = "0" + hour;

        var min = datetime.getMinutes();
        if (min < 10)
            min = "0" + min;

        var sec = datetime.getSeconds();
        if (sec < 10)
            sec = "0" + sec;

        var dateTimeString = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;

        return dateTimeString;
    };

    var checkReviewCheckBoxForApproveAndDenyList = function () {

        //check if any same as mailing address for business location
        if ($('input.isMailingAddressDifferent').is(':checked')) {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //check if any same as mailing address for contact information
        if ($('input.isContactAddressSameAsBusinessAddress').is(':checked')) {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for each sort yard details COA Capacity tonnes whether certificate of approval appears
        $('.COA input[type=checkbox]').each(function () {
            if ($(this).closest('.COA').css('display') == "none") {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            } else {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            }
        });

        //for Steward details GVWR
        //if display is none and add that attribute
        if ($('#pnlCVOR').css('display') == "none") {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for Steward details WSIB
        //if display is none and add that attribute
        if ($('#pnlWSIB').css('display') == "none") {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }
    }

    var GetValidationErrorMessages = function () {
        var validationErrorMessages = [];
        checkReviewCheckBoxForApproveAndDenyList();
        var validationCheckBoxes = $('INPUT.validation-checkbox:checkbox:not(:checked)');
        $.each(validationCheckBoxes, function () {
            var validationMessage = $(this).data('validate-message');
            validationErrorMessages.push(validationMessage);
        });
        var additionalReasons = $('#modelDenyAdditionalReasonInput').val();
        if (additionalReasons.length != '' && additionalReasons.length > 0) {
            validationErrorMessages.push(additionalReasons);
        }

        var uploadedFilesValidationMessage = $('.upload-file-selection');
        $.each(uploadedFilesValidationMessage, function () {
            var validationMessage = $(this).attr('data-uploadedFile-validation-message');
            if (validationMessage != undefined && validationMessage != '') {
                validationErrorMessages.push(validationMessage);
            }
        });

        return validationErrorMessages;
    }

    var reasonList;

    $('#modalDeny').on('shown.bs.modal', function () {
        $('#denyReasonsList').empty();
        $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
        reasonList = GetValidationErrorMessages();
        $.each(reasonList, function (index) {
            $('#denyReasonsList').append('<li>' + reasonList[index] + '</li>');
        });
        if (reasonList.length > 0) {
            $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
        } else {
            var comment = $.trim($('#modelDenyAdditionalReasonInput').val());
            if (comment.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            };
        };
    });

    $('#modelDenyAdditionalReasonInput').on('change', function () {
        if (reasonList.length <= 0) {
            if (this.value.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            } else {
                $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
            };
        };
    });

    $('#listOfStaff').on('change', function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#modalAssignAssignBtn").removeAttr("disabled");
            $("#modalAssignAssignBtn").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#modalAssignAssignBtn").attr("disabled", "disabled");
        }
    });

    $('.workflow').on('click', function () {
        var status = $(this).attr('data-status');
        var userID = 0;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }
        if (status != "") {
            if (status.toLowerCase() == 'backtoapplicant') {
                //as per OTSTM-864 clear all fields that have their checkbox unchecked 

                $('input[type="checkbox"]:not(:checked)').each(function () {
                    clearField(this);
                });

                $('#hdnExplicitSave').trigger('click');//saves data                
            }
            //let model save first then change status
            setTimeout(function () {
                $.ajax({
                    url: '/Steward/Registration/ChangeStatus',
                    method: 'POST',
                    dataType: "JSON",
                    data: { applicationId: $('#applicationId').val(), status: status, userID: userID },
                    success: function (data) {
                        window.location.reload(true);
                    },
                });
            }, 1000);
        }
    });

    $('#modalApproveApproveBtn').on('click', function () {

        $.ajax({
            url: '/Steward/Registration/ApproveApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val() },
            success: function (data) {
                if (data.isValid) {
                    var url = Global.StaffIndex.Settings.afterApproveRedirectUrl.replace('111', data.customerID);
                    window.location = url;
                }
            },
            failure: function (data) {                
            }
        });
    });

    $('#approveBtn').on('click', function () {
        //check if any fields are checked
        $('#approveBtn').removeAttr("data-target");

        checkReviewCheckBoxForApproveAndDenyList();
        var isAllValidationChecksCompleted = true;

        $('input:checkbox.validation-checkbox.no-validate').each(function () {
            if (!this.checked && !this.hasAttribute('approve-validation')) {
                isAllValidationChecksCompleted = false;
            }
        });

        if (isAllValidationChecksCompleted &&
            $('#fmBusinessLocation').valid() &&
            $('#fmContactInfo').valid() &&          
            $('#fmStewardDetails').valid() &&
            $('#fmTermsAndConditions').valid() &&
            $('#fmTireDetails').valid()) {
            $(this).attr("data-target", "#modalApproveApplication");
        }
        else {
           // alert('Validation error. One or more fields are not valid.');
            $('#modalApproveValidateWarning').modal('show');
        }
       
    });

    $('#modalDenyDenyApplicationBtn').on('click', function () {
       $.ajax({
            url: '/Steward/Registration/DenyApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val(), validationErrorMessages: GetValidationErrorMessages() },
            failure: function (data) {
            },
        });
    });

    $('#modalResendYesBtn').on('click', function () {

        //added
        switch (Global.StaffIndex.Settings.status) {
            case Global.StaffIndex.Settings.ApplicationStatus.Approved:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
            case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                var url = '/Steward/Registration/ResendApproveCustomer'
                var data = { customerId: Global.StaffIndex.Settings.vendorID, applicationId: $('#applicationId').val() };
                break;
            default:
                //var url = '/Steward/Registration/ResendApproveApplication'
                //var data = { applicationId: $('#applicationId').val()};
                break;
        }

        $.ajax({
            url: url,
            method: 'GET',
            dataType: "JSON",
            data: data,
            failure: function (data) {
                if (data.isValid) {
                    window.location.reload(true);
                }
            },
            success: function (data) {
                if (data.isValid) {
                    window.location.reload(true);
                }
            },
        });
    });

    $('#modalSaveDBYesBtn').on('click', function () {
        
        handleBusinessAddressCheckboxForContact();

        var form = $('form');

        //var tmpEnabled = form.find('input.sort-yard-address-disabled,select.sort-yard-address-disabled').removeAttr('disabled');
        var disabled = form.find(':input:disabled').removeAttr('disabled'); //for saving when all fields are disabled
        
        var data = form.serializeArray();
        //tmpEnabled = tmpEnabled.attr('disabled', 'disabled');
        disabled.attr('disabled', 'disabled');
        
        data = data.concat(
            $('#MainId input[type=checkbox]:not(:checked)').map(
                function () {
                    return { "name": this.name, "value": this.value }
                }).get());

        data = data.concat(
            $('#MainId input[type=radio]:not(:checked)').map(
                function () {
                    return { "name": this.name, "value": null }
                }).get());

        $('input[type="checkbox"]').each(function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }
                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }

        $.ajax({
            url: '/Steward/Registration/SaveModelDB',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (result) {
                if (result.isValid) {
                    var temp = [];
                    $.each(Global.CommonActivity.LogHistory, function (index, item) {
                        temp.push({ name: item.name, value: item.value });
                    });
                    addActivityLog(Global.CommonActivity.ActivityArea,temp);
                    window.location.reload(true);
                }
                else {
                    $('#save-error-text').html(result);
                    $('#modalSaveError').modal('toggle');
                }
            },
            error: function (result) {
            }
        });
    });

    $('#saveBtn').on('click', function () {

        $('#saveBtn').removeAttr("data-target");

        var IsStewardAutoSaveDisabled = Global.StaffIndex.Security.IsStewardAutoSaveDisabled.toLowerCase() == 'true';

        if (!IsStewardAutoSaveDisabled) {
            //check if any fields are checked
            if ($('#fmBusinessLocation').valid() &&
                $('#fmContactInfo').valid() &&
                $('#fmStewardDetails').valid() &&
                $('#fmTermsAndConditions').valid() &&
                $('#fmTireDetails').valid()) {
                $(this).attr("data-target", "#modalSaveApplication");
            }
            else {
                // alert('Validation error. One or more fields are not valid.');
                $('#modalApproveValidateWarning').modal('show');
            }
        }
        else {
            $('#modalSaveError').modal('toggle')
        }
    });
});

/* END WORKFLOW CODE */


/* GABE STYLES */

(function () {
    // Popover Setup
    var settings = {
        trigger: 'hover',
        //title:'Pop Title',
        //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
        //width:300,
        multi: true,
        closeable: true,
        style: '',
        delay: { show: 300, hide: 800 },
        padding: true
    };

    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
      businessLocationLBNSettings = {
          content: businessLocationLBNContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
      businessLocationBONSettings = {
          content: businessLocationBONContent,
          width: 300,
          height: 100,
      };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, settings, businessLocationBONSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
      tiredetailsPopoverPLTSettings = {
          content: tiredetailsPopoverPLTContent,
          width: 500,
          height: 600,
      };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
      tiredetailsPopoverMTSettings = {
          content: tiredetailsPopoverMTContent,
          width: 380,
          height: 155,
      };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverAGLSSettings));

    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
      tiredetailsPopoverINDSettings = {
          content: tiredetailsPopoverINDContent,
          width: 360,
          height: 120,
      };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverINDSettings));

    // Tire Details: Pneumatic Tires (OTR) 
    var tiredetailsContent = $('#tiredetailsPopoverOTR').html(),
    tiredetailsSettings = {
        content: tiredetailsContent,
        width: 380,
        height: 100,
    };
    var popLargeMT = $('.tiredetailsPopoverOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));
    // Solid & Resilient Tires (OTR)
    var tiredetailsContent = $('#tiredetailsPopoverOTRS').html(),
    tiredetailsSettings = {
        content: tiredetailsContent,
        width: 380,
        height: 150,
    };
    var popLargeMT = $('.tiredetailsPopoverOTRS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    //Steward Details

    var tiredetailsContent = $('#StewardOriginalEquipmentManufactureOEM').html(),
    tiredetailsSettings = {
        content: tiredetailsContent,
        width: 380,
        height: 66,
    };
    var popLargeMT = $('.StewardOriginalEquipmentManufactureOEM').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    var tiredetailsContent = $('#StewardTireManufactureBrandOwner').html(),
    tiredetailsSettings = {
        content: tiredetailsContent,
        width: 508,
        height: 230,
    };
    var popLargeMT = $('.StewardTireManufactureBrandOwner').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    var tiredetailsContent = $('#StewardFirstImporter').html(),
    tiredetailsSettings = {
        content: tiredetailsContent,
        width: 380,
        height: 150,
    };
    var popLargeMT = $('.StewardFirstImporter').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBL').html(),
      supportingDocumentsAIMBLSettings = {
          content: supportingDocumentsAIMBLContent,
          width: 500,
          height: 140,
      };
    var popLargeAIMBL = $('.supportingDocumentsAIMBL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLI').html(),
      supportingDocumentsCLISettings = {
          content: supportingDocumentsCLIContent,
          width: 500,
          height: 260,
      };
    var popLargeCLI = $('.supportingDocumentsCLI').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCLISettings));

    // Supporitng Documents: CVOR Abstract Level II
    var supportingDocumentsCVORContent = $('#supportingDocumentsCVOR').html(),
      supportingDocumentsCVORSettings = {
          content: supportingDocumentsCVORContent,
          width: 460,
          height: 170,
      };
    var popLargeCVOR = $('.supportingDocumentsCVOR').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCVORSettings));

    // Supporitng Documents: Processor Relationship Letter
    var supportingDocumentsPRLContent = $('#supportingDocumentsPRL').html(),
      supportingDocumentsPRLSettings = {
          content: supportingDocumentsPRLContent,
          width: 460,
          height: 105,
      };
    var popLargePRL = $('.supportingDocumentsPRL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsPRLSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCH').html(),
      supportingDocumentsVOCHSettings = {
          content: supportingDocumentsVOCHContent,
          width: 674,
          height: 359,
      };
    var popLargeVOCH = $('.supportingDocumentsVOCH').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsVOCHSettings));

    // Popover: Participant Information
    var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
        popoverParticipantInfoSettings = {
            content: popoverParticipantInfoContent,
            width: 270,
        };
    var popLargeLBN = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));

    $('#MainId :input').each(function () {
        this.defaultValue = this.value;
        if (this.type == 'select-one') {
            this.defaultValue = this.options[this.selectedIndex].text;
        }
        if (this.type == 'radio' && this.checked) {
            if (this.dataset.fieldname == "Are you currently registered with RPRA?" || this.dataset.fieldname == "Please select your Steward Type.") {
                $(this.closest("div.form-group")).attr('data-reportval', this.value);
            }           
            else {
                $(this.closest('tbody > tr')).attr('data-reportval', this.value);
            }            
        }
    })  
})();

/* GABE STYLES */

//OTSTM2-630 keep the original selection in contact panel dropdown
$(document).ready(function () {
    $(".dropdownCountry").each(function () {
        if (this.selectedIndex == -1) {
            this.selectedIndex = 0;
        }
        this.defaultValue = this.options[this.selectedIndex].text;
    })
});