﻿using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.ProcessorServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Claims.Export;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.ConfigurationsServices;

namespace CL.TMS.Web.Areas.Processor.Controllers
{
    public class ClaimsController : ClaimBaseController
    {
        #region Properties

        private IClaimService claimService;
        private IFileUploadService fileUploadService;
        private IProcessorClaimService processorClaimService;
        private IConfigurationsServices configurationService;

        #endregion

        #region Constructor

        public ClaimsController(IProcessorClaimService processorClaimService, IClaimService claimService, IFileUploadService fileUploadService, IConfigurationsServices configurationService)
        {
            this.processorClaimService = processorClaimService;
            this.claimService = claimService;
            this.fileUploadService = fileUploadService;
            this.configurationService = configurationService;
        }
        #endregion

        #region Action Methods

        // GET: /Processor/Claims/
        public ActionResult Index()
        {
            if (SecurityContextHelper.CurrentVendor == null || SecurityContextHelper.CurrentVendor.VendorType != 4)
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;
            ViewBag.SelectedMenu = "Claims";

            if (!SecurityContextHelper.IsStaff())
            {
                return View("ParticipantClaimIndex");
            }
            else
            {
                return View("StaffClaimIndex");
            }
        }

        [ClaimsAuthorization(TreadMarksConstants.ClaimsProcessorClaimSummary)]
        public ActionResult ClaimSummary(int claimId, int? vId = null)
        {
            if (null == SecurityContextHelper.CurrentVendor && vId == null)//session time out
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            else if (vId != null)
            {
                var vendor = claimService.GetVendorById(vId.Value);
                if (vendor != null)
                {
                    //Keep it in session
                    //Vendor Context Check
                    if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 4)
                        Session["SelectedVendor"] = vendor;
                    else
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
                else
                    return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.ngApp = "processorClaimApp";
            ViewBag.ClaimsId = claimId;
            //OTSTM2-551 only claim summary page for staff has activity panel (can get real time activity)
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.HasActivityPanel = 1;
            }
            ViewBag.ClaimsStatus = claimService.GetClaimStatus(claimId);
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = false;
            ViewBag.AllowEdit = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary) == SecurityResultType.EditSave);

            var vendorReference = claimService.GetVendorReference(claimId);
            if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 4)
            {
                if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                {
                    return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }

            return View(SecurityContextHelper.IsStaff() ? "StaffClaimSummary" : "ParticipantClaimSummary");
        }

        [HttpGet]
        public ActionResult ClaimDetails(int claimId)
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            var result = processorClaimService.LoadProcessorClaimSummary(vendorId, claimId);
            if (SecurityContextHelper.IsStaff())
            {
                PopulateTransationUrlForStatusPanel(result.ClaimStatus, "Processor", claimId);
            }
            result.ClaimCommonModel.ClaimPeriodName = result.ClaimCommonModel.ClaimPeriod.ShortName;
            result.ClaimCommonModel.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;

            if (null != result.ClaimStatus)
            {
                result.ClaimStatus.Submitted = ConvertTimeFromUtc(result.ClaimStatus.Submitted);
                result.ClaimStatus.Assigned = ConvertTimeFromUtc(result.ClaimStatus.Assigned);
                result.ClaimStatus.Started = ConvertTimeFromUtc(result.ClaimStatus.Started);
                result.ClaimStatus.ReviewDue = ConvertTimeFromUtc(result.ClaimStatus.ReviewDue);
                result.ClaimStatus.Approved = ConvertTimeFromUtc(result.ClaimStatus.Approved);
                result.ClaimStatus.Reviewed = ConvertTimeFromUtc(result.ClaimStatus.Reviewed);
                result.ClaimStatus.ClaimOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOnholdDate);
                result.ClaimStatus.ClaimOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOffholdDate);
                result.ClaimStatus.AuditOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOnholdDate);
                result.ClaimStatus.AuditOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOffholdDate);
            }

            return new NewtonSoftJsonResult(result, false);
        }
        #endregion

        [HttpGet]
        public ActionResult LoadPIRatesByTransactionID(int rateTransactionID)
        {
            RateDetailsVM result = processorClaimService.LoadPIRatesByTransactionID(rateTransactionID);
            result.notes.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        #region Submit Button
        [HttpPost]
        public JsonResult SubmitClaimCheck(ProcessorSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data", Type = "" }, JsonRequestBehavior.AllowGet);
                }

                var updatedSubmitClaimModel = processorClaimService.CheckClaimSubmitBusinessRule(submitClaimModel);

                if (updatedSubmitClaimModel.Errors.Count > 0)
                {
                    //handle errors
                    var result = new { status = "Errors", Type = SubmitBusinessRuleType.Error.ToString(), Message = updatedSubmitClaimModel.Errors.First() };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                if (updatedSubmitClaimModel.Warnings.Count > 0)
                {
                    //handle warnings
                    return Json(new { status = "Warnings", Type = SubmitBusinessRuleType.Warning.ToString(), Warnings = updatedSubmitClaimModel.Warnings }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = "Valid Data", Type = SubmitBusinessRuleType.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult FinalSubmitClaim(int claimId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }
                this.claimService.SubmitClaim(claimId, false);

                return Json(new { status = "Valid Data" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion
    }
}