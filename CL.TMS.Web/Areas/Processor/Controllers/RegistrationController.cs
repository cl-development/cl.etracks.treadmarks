﻿using System;
using System.Collections.Generic;
using System.Linq;
using MicrosoftSystem = System.Web;
using MicrosoftIO = System.IO;
using System.Web.Mvc;
using CL.TMS.Common;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.UI.Common.Helper;
using CL.TMS.Common.Helper;
using System.IO;
using SystemIO = System.IO;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.Framework.Common;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Email;
using CL.TMS.ServiceContracts.RegistrantServices;
using Microsoft.Ajax.Utilities;
using System.Diagnostics;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.Security.Authorization;
using CL.TMS.UI.Common.Security;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Configuration;
using CL.TMS.ExceptionHandling;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using MicrosoftWeb = System.Web;

namespace CL.TMS.Web.Areas.Processor.Controllers
{
    public class RegistrationController : BaseController
    {
        #region Services

        private IProcessorRegistrationService ProcessorRegistrationService;
        private IProcessorRegistrationInvitationService registrationInvitationService; //IApplicationInvitationService applicationInvitationService;
        private IApplicationService applicationService; //IApplicationService applicationService;
        private IUserService userService;
        private IRegistrantService registrantService;
        private IFileUploadService fileUploadService;
        private ICompanyBrandingServices companyBrandingService;
        private string uploadRepositoryPath;
        private string fileUploadPreviewDomainName;
        private string inviteUrl;

        #endregion

        #region Contructors

        public RegistrationController(IProcessorRegistrationService ProcessorRegistrationService, IProcessorRegistrationInvitationService applicationInvitationService,
            IUserService userService, IApplicationService applicationService, IRegistrantService registrantService, IFileUploadService fileUploadService, ICompanyBrandingServices companyBrandingService)
        {
            this.ProcessorRegistrationService = ProcessorRegistrationService;
            this.registrationInvitationService = applicationInvitationService;
            this.userService = userService;
            this.applicationService = applicationService;
            this.registrantService = registrantService;
            this.fileUploadService = fileUploadService;
            this.companyBrandingService = companyBrandingService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        #endregion

        #region Action Methods

        [AllowAnonymous]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsProcessorApplication)]
        [HttpGet]
        public ActionResult Index(Guid id)
        {
            ProcessorRegistrationModel model = ProcessorRegistrationService.GetByTokenId(id);

            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }
            int appID = registrationInvitationService.GetByTokenID(id).ApplicationID.Value;
            ApplicationStatusEnum status = (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), applicationService.GetByApplicationID(appID).Status);

            if ((status == ApplicationStatusEnum.Open) || (status == ApplicationStatusEnum.BackToApplicant))
            {
                if ((model.ExpireDate != null) && (model.ExpireDate < DateTime.UtcNow))
                {
                    return RedirectToAction("InvitationExpired", "AppInvitation", new { area = "System" });
                }
            }

            if (status == ApplicationStatusEnum.Approved
                || status == ApplicationStatusEnum.Completed
                || status == ApplicationStatusEnum.BankInformationSubmitted
                || status == ApplicationStatusEnum.BankInformationApproved)
            {
                return RedirectToAction("RegistrationIndex", "Registration", new { vId = model.VendorID });
            }
            else
            {
                #region read from form object
                if (status == ApplicationStatusEnum.BankInformationSubmitted || status == ApplicationStatusEnum.BankInformationApproved)
                {
                    var BankInfo = registrantService.GetBankInformation(model.VendorID);

                    if (BankInfo.ReviewStatus == BankInfoReviewStatus.Submitted.ToString() || BankInfo.ReviewStatus == BankInfoReviewStatus.Approved.ToString())
                    {
                        model.BankingInformation = AutoMapper.Mapper.Map<BankingInformationRegistrationModel>(BankInfo);
                    }
                }

                #region prepare Model
                if ((null == model.ContactInformationList) || (model.ContactInformationList.Count == 0))
                {
                    model.ContactInformationList = new List<ContactRegistrationModel>();
                    model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
                }
                if ((null == model.SortYardDetails) || (model.SortYardDetails.Count == 0))
                {
                    model.SortYardDetails.Add(new ProcessorSortYardDetailsRegistrationModel() { SortYardAddress = new SortYardAddressModel() });
                }

                if (model.TermsAndConditions == null)
                {
                    model.TermsAndConditions = new TermsAndConditionsRegistrationModel();
                }
                TempData["SubTypes"] = AppDefinitions.Instance.TypeDefinitions["ProcessorProdType"].ToList();
                model.RegistrantActiveStatus = model.RegistrantActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantActiveStatus;
                model.RegistrantInActiveStatus = model.RegistrantInActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantInActiveStatus;

                if (model.SupportingDocuments == null)
                {
                    model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
                }
                switch (status)
                {
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                        model.SupportingDocuments.ID = model.VendorID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetVendorAttachments(model.VendorID).Count();
                        model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Registrant;
                        break;
                    default:
                        model.SupportingDocuments.ID = model.ID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetAttachments(model.ID).Count();
                        break;
                }

                if (model.TireDetails == null)
                {
                    model.TireDetails = ProcessorRegistrationService.GetAllItemsList().TireDetails;
                }
                if (model.TireDetails.ProductsProduceType == null)
                {
                    model.TireDetails.ProductsProduceType = ProcessorRegistrationService.GetAllProcessorProduced().TireDetails.ProductsProduceType;
                }

                model.TireDetails.Status = model.Status;
                model.BusinessLocation.Status = model.Status;

                foreach (var contact in model.ContactInformationList)
                {
                    contact.Status = model.Status;
                }
                foreach (var contact in model.SortYardDetails)
                {
                    contact.Status = model.Status;
                }
                model.ProcessorDetails.Status = model.Status;
                model.TermsAndConditions.Status = model.Status;
                model.TermsAndConditions.ParticipantType = "Processor";
                //OTSTM2-973 TermsAndConditions
                model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

                model.SupportingDocuments.ParticipantType = "Processor";
                model.SupportingDocuments.Status = model.Status;


                #endregion


                var user = TMSContext.TMSPrincipal;

                if (user.Identity.IsAuthenticated)
                {
                    if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                    {
                        if (model.VendorID != 0 &&
                            (status == ApplicationStatusEnum.Approved
                            || status == ApplicationStatusEnum.Completed
                            || status == ApplicationStatusEnum.BankInformationSubmitted
                            || status == ApplicationStatusEnum.BankInformationApproved
                            ))
                        {
                            var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForVendor(model.VendorID);
                            var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, false);
                            var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, true);

                            model.ActiveInactive.RegistrantActive = previousActiveHistory;
                            model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                            model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                            if (model.ActiveInactive.RegistrantInactive == null)
                            {
                                model.ActiveInactive.RegistrantInactive = new VendorActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }
                            if (model.ActiveInactive.RegistrantActive == null)
                            {
                                model.ActiveInactive.RegistrantActive = new VendorActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }

                            model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                            model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                            model.ActiveInactive.Status = model.Status;
                        }

                        return View("StaffIndex", model);
                    }
                    return View("ParticipantIndex", model);
                }

                switch (status)
                {
                    case ApplicationStatusEnum.Open:
                    case ApplicationStatusEnum.BackToApplicant:
                        break;
                    case ApplicationStatusEnum.Submitted:
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                    case ApplicationStatusEnum.OnHold:
                    case ApplicationStatusEnum.Denied:
                    case ApplicationStatusEnum.Assigned:
                    default:
                        return RedirectToAction("ApplicationSubmittedPage", "Default", new { area = "" });
                }

                return View("ParticipantIndex", model);
                #endregion
            }
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsProcessorApplication)]
        [HttpGet]
        public ActionResult RegistrationIndex(int vId)
        {
            ProcessorRegistrationModel model = ProcessorRegistrationService.GetApprovedApplication(vId);
            model.VendorID = vId;
            ViewBag.SelectedMenu = "Applications";
            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }

            //OTSTM2-427: set up global search  
            var vendor = registrantService.GetVendorById(vId);
            if (vendor != null)
            {
                //Keep it in session
                //Vendor Context Check
                if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 4)
                {
                    Session["SelectedVendor"] = vendor;
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            if (model.Status == ApplicationStatusEnum.BankInformationSubmitted || model.Status == ApplicationStatusEnum.BankInformationApproved)
            {
                var BankInfo = registrantService.GetBankInformation(vId);

                if (BankInfo.ReviewStatus == BankInfoReviewStatus.Submitted.ToString() || BankInfo.ReviewStatus == BankInfoReviewStatus.Approved.ToString())
                {
                    model.BankingInformation = AutoMapper.Mapper.Map<BankingInformationRegistrationModel>(BankInfo);
                }
            }
            #region prepare Model
            if ((null == model.ContactInformationList) || (model.ContactInformationList.Count == 0))
            {
                model.ContactInformationList = new List<ContactRegistrationModel>();
                model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
            }
            if ((null == model.SortYardDetails) || (model.SortYardDetails.Count == 0))
            {
                model.SortYardDetails.Add(new ProcessorSortYardDetailsRegistrationModel() { SortYardAddress = new SortYardAddressModel() });
            }

            if (model.TermsAndConditions == null)
            {
                model.TermsAndConditions = new TermsAndConditionsRegistrationModel();
            }
            TempData["SubTypes"] = AppDefinitions.Instance.TypeDefinitions["ProcessorProdType"].ToList();
            model.RegistrantActiveStatus = model.RegistrantActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantActiveStatus;
            model.RegistrantInActiveStatus = model.RegistrantInActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantInActiveStatus;

            if (model.SupportingDocuments == null)
            {
                model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Registrant };
            }
            model.SupportingDocuments.ID = vId;
            model.SupportingDocuments.CountOfUploadedDocuments = GetVendorAttachments(vId).Count();
            model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Registrant;

            if (model.TireDetails == null)
            {
                model.TireDetails = ProcessorRegistrationService.GetAllItemsList().TireDetails;
            }
            if (model.TireDetails.ProductsProduceType == null)
            {
                model.TireDetails.ProductsProduceType = ProcessorRegistrationService.GetAllProcessorProduced().TireDetails.ProductsProduceType;
            }
            model.TireDetails.Status = model.Status;
            model.BusinessLocation.Status = model.Status;
            foreach (var contact in model.ContactInformationList)
            {
                contact.Status = model.Status;
            }
            foreach (var contact in model.SortYardDetails)
            {
                contact.Status = model.Status;
            }
            model.ProcessorDetails.Status = model.Status;
            model.TermsAndConditions.Status = model.Status;
            model.TermsAndConditions.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("processor") > -1).Name;
            //OTSTM2-973 TermsAndConditions
            model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

            model.SupportingDocuments.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("processor") > -1).Name;
            model.SupportingDocuments.Status = model.Status;


            #endregion


            var user = TMSContext.TMSPrincipal;

            if (user.Identity.IsAuthenticated)
            {
                if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                {
                    var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForVendor(model.VendorID);
                    var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, false);
                    var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForVendor(model.VendorID, true);

                    model.ActiveInactive.RegistrantActive = previousActiveHistory;
                    model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                    model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                    if (model.ActiveInactive.RegistrantInactive == null)
                    {
                        model.ActiveInactive.RegistrantInactive = new VendorActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }
                    if (model.ActiveInactive.RegistrantActive == null)
                    {
                        model.ActiveInactive.RegistrantActive = new VendorActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }

                    model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                    model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                    model.ActiveInactive.Status = model.Status;

                    //Get Group Name (effective) from vendor reference
                    model.GroupName = vendor.GroupName;

                    return View("StaffIndex", model);
                }
                return View("ParticipantIndex", model);
            }

            switch (model.Status)
            {
                case ApplicationStatusEnum.Open:
                case ApplicationStatusEnum.BackToApplicant:
                    break;
                case ApplicationStatusEnum.Submitted:
                case ApplicationStatusEnum.Approved:
                case ApplicationStatusEnum.BankInformationSubmitted:
                case ApplicationStatusEnum.BankInformationApproved:
                case ApplicationStatusEnum.Completed:
                case ApplicationStatusEnum.OnHold:
                case ApplicationStatusEnum.Denied:
                case ApplicationStatusEnum.Assigned:
                default:
                    return RedirectToAction("ApplicationSubmittedPage", "Default", new { area = "" });
            }

            return View("ParticipantIndex", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SubmitApplication(int applicationId, Guid tokenId)
        {
            ApplicationInvitation appInvitation = registrationInvitationService.GetByTokenID(tokenId);
            if (appInvitation == null)
            {
                return new HttpStatusCodeResult(401);
            }

            this.ProcessorRegistrationService.SetApplicationStatus(applicationId, ApplicationStatusEnum.Submitted, null, null);

            return new HttpStatusCodeResult(200);
        }

        [AllowAnonymous]
        public ActionResult NotFound()
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            return View();

        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SaveModel(ProcessorRegistrationModel model)
        {
            try
            {
                var IsProcessorAutoSaveDisabled = false;

                if (SecurityContextHelper.IsStaff())
                {
                    var user = TMSContext.TMSPrincipal;
                    var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Processor")).ToList();
                    IsProcessorAutoSaveDisabled = ApplicationHelper.IsProcessorAutoSaveDisabled(rolePermissionsClaims);
                }

                if (!IsProcessorAutoSaveDisabled)
                {
                    PopulateTireDetails(model);

                    //OTSTM2-50 fix existing issue that check the following checkbox at the first time cannot be remembered after refreshing page
                    model.ContactInformationList.ToList().ForEach(c =>
                    {
                        if (c.ContactAddressSameAsBusinessAddress == false && c.ContactInformation.ContactAddressSameAsBusiness == true)
                        {
                            c.ContactAddressSameAsBusinessAddress = true;
                            c.ContactInformation.ContactAddressSameAsBusiness = false;
                        }
                    });

                    if (model.VendorID != 0)
                    {
                        foreach (var contactInformation in model.ContactInformationList)
                        {
                            if (contactInformation.ContactInformation.ID == 0)
                            {
                                contactInformation.ContactAddressSameAsBusinessAddress = contactInformation.ContactInformation.ContactAddressSameAsBusiness;
                            }
                        }
                    }

                    model.UserIPAddress = ("::1" == MicrosoftSystem.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftSystem.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow; //Use UTC time to avoid DateTime Serialze issue in JavaScriptSerializer().Serialize()
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI

                    int appID = model.ID;
                    string updatedBy = MicrosoftSystem.HttpContext.Current.User.Identity.Name;

                    ProcessorRegistrationService.UpdateUserExistsinApplication(model, appID, updatedBy);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #region WorkFlow
        [HttpPost]
        public JsonResult ChangeStatus(string applicationId, string status, long userId = 0)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            if (string.Compare(status, ApplicationStatusEnum.Approved.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            int applicationID;
            int.TryParse(applicationId, out applicationID);

            if (applicationID == 0) { throw new ArgumentException("applicationId"); }

            ApplicationStatusEnum applicationStatus;
            Enum.TryParse<ApplicationStatusEnum>(status, true, out applicationStatus);

            var user = TMSContext.TMSPrincipal.Identity;
            string updatedBy = user.Name;

            //if its backtoApplicant send an email
            if (string.Compare(applicationStatus.ToString(), ApplicationStatusEnum.BackToApplicant.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
            {
                var ProcessorApplicationModel = this.ProcessorRegistrationService.GetFormObject(applicationID);
                string Email = this.registrationInvitationService.GetEmailByApplicationId(applicationID);

                var backToApplicantProcessorEmailModel = new ApplicationEmailModel()
                {
                    BusinessLegalName = ProcessorApplicationModel.BusinessLocation.BusinessName,
                    BusinessName = ProcessorApplicationModel.BusinessLocation.OperatingName,
                    Address1 = ProcessorApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                    City = ProcessorApplicationModel.BusinessLocation.BusinessAddress.City,
                    ProvinceState = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Province,
                    PostalCode = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Postal,
                    Country = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Country,
                    Email = Email,
                };

                if (EmailContentHelper.IsEmailEnabled("NewApplicationBacktoApplicant"))
                    this.ProcessorRegistrationService.SendEmailForBackToApplicant(applicationID, backToApplicantProcessorEmailModel);
            }

            this.ProcessorRegistrationService.SetApplicationStatus(applicationID, applicationStatus, null, updatedBy, userId);

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DenyApplication(ProcessorApplicationDenyModel ProcessorApplicationDenyModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Processor")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsProcessorRouting = SecurityContextHelper.ProcessorApplicationsWorkflow != null ? SecurityContextHelper.ProcessorApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsProcessorRouting)
            {
                int applicationID;
                int.TryParse(ProcessorApplicationDenyModel.applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }
                string denyReasons = string.Join(",", ProcessorApplicationDenyModel.validationErrorMessages);
                //TODO: Check for call centre role
                var user = TMSContext.TMSPrincipal.Identity;
                string updatedBy = user.Name;
                this.ProcessorRegistrationService.SetApplicationStatus(applicationID, ApplicationStatusEnum.Denied, denyReasons, updatedBy);

                var ProcessorApplicationModel = this.ProcessorRegistrationService.GetFormObject(applicationID);
                string Email = this.registrationInvitationService.GetEmailByApplicationId(applicationID);
                var approvedProcessorApplicationModel = new ApplicationEmailModel()
                {
                    BusinessLegalName = ProcessorApplicationModel.BusinessLocation.BusinessName,
                    BusinessName = ProcessorApplicationModel.BusinessLocation.OperatingName,
                    Address1 = ProcessorApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                    City = ProcessorApplicationModel.BusinessLocation.BusinessAddress.City,
                    ProvinceState = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Province,
                    PostalCode = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Postal,
                    Country = ProcessorApplicationModel.BusinessLocation.BusinessAddress.Country,
                    Email = Email,
                };

                string message = "";
                if (EmailContentHelper.IsEmailEnabled("NewApplicationDenied"))
                {
                    message = ComposeAndSendDenyEMail(approvedProcessorApplicationModel, ProcessorApplicationDenyModel.validationErrorMessages);
                }
                else
                    message = "This email is disabled. Please contact us for assistance.";


                return Json(message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        private string ComposeAndSendDenyEMail(ApplicationEmailModel approvedApplicationModel, string[] validationErrorMessages)
        {
            string message = string.Empty;

            try
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.DenyHaulerApplicationEmailTemplLocation"));
                //string emailBody = SystemIO.File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationDenied");
                string subject = EmailContentHelper.GetSubjectByName("NewApplicationDenied");

                emailBody = emailBody
                    .Replace(ProcessorApplicationEmailTemplPlaceHolders.ApplicationType, "Processor")
                    .Replace(ProcessorApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessLegalName, approvedApplicationModel.BusinessLegalName)
                    .Replace(ProcessorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                if (validationErrorMessages != null && validationErrorMessages.Length > 0)
                {
                    var validationErrorMessagesHtmlBlock = new StringBuilder();
                    foreach (var errorMessage in validationErrorMessages)
                    {
                        string validationErrorMessageHtml = string.Format("<p style='color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;'>{0}</p>", errorMessage);
                        validationErrorMessagesHtmlBlock.Append(validationErrorMessageHtml);
                    }

                    emailBody = emailBody.Replace(ProcessorApplicationEmailTemplPlaceHolders.ValidationErrorMessages, validationErrorMessagesHtmlBlock.ToString());
                }
                else
                {
                    emailBody = emailBody.Replace(ProcessorApplicationEmailTemplPlaceHolders.ValidationErrorMessages, string.Empty);
                }

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = ProcessorApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = ProcessorApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = ProcessorApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Boolean.Parse(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                //string subject = MessageResource.ProcessorDeclinedSubject;

                bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();
                //OTSTM2-132 BCC value update
                email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), approvedApplicationModel.Email, null, bSendBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

                message = string.Format("Processor application deny email was successfully sent to {0} at email {1}", approvedApplicationModel.BusinessLegalName, approvedApplicationModel.Email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                message = ex.Message;
            }

            return message;
        }

        [HttpPost]
        public JsonResult ApproveApplication(string applicationId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int selectedVendorGroupId = 0;
            int.TryParse(MicrosoftWeb.HttpContext.Current.Request.Params["SelectedVendorGroupId"], out selectedVendorGroupId);

            int rateGroupId = 0;
            int.TryParse(MicrosoftWeb.HttpContext.Current.Request.Params["EffectiveRateGroupId"], out rateGroupId);

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Processor")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsProcessorRouting = SecurityContextHelper.ProcessorApplicationsWorkflow != null ? SecurityContextHelper.ProcessorApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsProcessorRouting && (selectedVendorGroupId != 0) && (rateGroupId != 0))
            {

                int applicationID;
                int.TryParse(applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }

                var applicationModel = this.ProcessorRegistrationService.GetFormObject(applicationID);

                applicationModel.SelectedVendorGroupId = selectedVendorGroupId;
                applicationModel.rateGroupId = rateGroupId;
                applicationModel.approvedByUserId = SecurityContextHelper.CurrentUser.Id;

                var vendorId = this.ProcessorRegistrationService.ApproveApplication(applicationModel, applicationID);

                ApplicationEmailModel approvedApplicationModel = this.ProcessorRegistrationService.GetApprovedProcessorApplicantInformation(applicationID);

                //Create User invitation first to Get invitation token
                approvedApplicationModel.InvitationToken = CreateUserInvitation(approvedApplicationModel, vendorId, applicationID);


                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationProcessor"))
                {
                    bool result = ComposeAndSendApplicationApprovalEmail(approvedApplicationModel);
                }

                return Json(new { status = MessageResource.ValidData, isValid = true, vendorID = vendorId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ResendApproveRegistrant(int vendorId)
        {
            if (vendorId == 0)
            {
                throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
            }
            ApplicationEmailModel approvedApplicationModel = this.registrantService.GetApprovedRegistrantInformation(vendorId);

            if (approvedApplicationModel != null)
            {
                var invitation = userService.FindInvitation(vendorId, approvedApplicationModel.Email, true);
                var expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
                if (invitation != null)
                {
                    invitation.FirstName = approvedApplicationModel.PrimaryContactFirstName;
                    invitation.LastName = approvedApplicationModel.PrimaryContactLastName;
                    invitation.ExpireDate = expireDate;
                    userService.UpdateInvitation(invitation);
                    approvedApplicationModel.InvitationToken = invitation.InviteGUID;
                }
                else
                {
                    var userIp = Request.UserHostAddress;
                    var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));
                    var vendor = registrantService.GetSingleVendorByID(vendorId);
                    string reditectURL = string.Empty;
                    if (vendor.RegistrantStatus == "Approved")
                        reditectURL = "/System/FinancialIntegration/BankingInformation/" + vendorId;

                    var newinvitation = ModelAdapter.ToInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, reditectURL, vendorId, 0);
                    userService.CreateParticipantDefaultUserInvitation(newinvitation);
                    approvedApplicationModel.InvitationToken = newinvitation.InviteGUID;
                }

                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationProcessor"))
                {
                    bool result = ComposeAndSendApplicationApprovalEmail(approvedApplicationModel);
                    if (!result)
                        throw new Exception("Email failed to send");
                }

                return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //  Update save in DB 
        public ActionResult SaveModelDB(ProcessorRegistrationModel model)
        {
            try
            {
                var user = TMSContext.TMSPrincipal;
                var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Processor")).ToList();
                var IsProcessorAutoSaveDisabled = ApplicationHelper.IsProcessorAutoSaveDisabled(rolePermissionsClaims);

                if (!IsProcessorAutoSaveDisabled)
                {
                    PopulateTireDetails(model);

                    if (model.VendorID != 0)
                    {
                        model.ContactInformationList.ToList().ForEach(c =>
                        {
                            c.isEmptyContact = false;
                            if (c.ContactInformation.ID == 0)
                            {
                                c.ContactAddressSameAsBusinessAddress = c.ContactInformation.ContactAddressSameAsBusiness;

                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (c.ContactAddressSameAsBusinessAddress)
                                    {
                                        model.ContactInformationList.Remove(c);
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            model.ContactInformationList.Remove(c);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (!c.ContactAddressSameAsBusinessAddress)
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            c.isEmptyContact = true;
                                        }
                                    }
                                }
                            }
                        });
                    }

                    model.UserIPAddress = ("::1" == MicrosoftSystem.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftSystem.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow;
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI

                    var vendorId = this.ProcessorRegistrationService.UpdateApproveApplication(model, model.VendorID);
                    return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private Guid CreateUserInvitation(ApplicationEmailModel approvedApplicationModel, int vendorId, int applicationId)
        {
            var userIp = Request.UserHostAddress;
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            string ReditectURL = "/System/FinancialIntegration/BankingInformation/" + vendorId;

            var invitation = ModelAdapter.ToInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, ReditectURL, vendorId, applicationId);

            userService.CreateParticipantDefaultUserInvitation(invitation);
            return invitation.InviteGUID;
        }

        #endregion

        #endregion

        #region Private Helper Methods
        private void PopulateTireDetails(ProcessorRegistrationModel model)
        {
            #region TireDetailsItemType
            if (model.TireDetails != null)
            {
                if (model.TireDetails.TireDetailsItemType != null)
                {
                    model.TireDetails.TireDetailsItemType = GetTireDetailsItemType(model).ToList();
                }
                else
                {
                    model.TireDetails.TireDetailsItemType = new List<ProcessorTireDetailsItemTypeModel>();
                }
            }
            else
            {
                model.TireDetails = new ProcessorTireDetailsRegistrationModel()
                {
                    TireDetailsItemType = new List<ProcessorTireDetailsItemTypeModel>()
                };
            }
            #endregion

            model.TireDetails.TireDetailsItemType = GetTireDetailsItemType(model).ToList();

            if (model.TireDetails.ProductsProduceType != null)
            {
                model.TireDetails.ProductsProduceType = GetProductProcessDetailsItemType(model).ToList();
            }
            else
            {
                model.TireDetails.ProductsProduceType = new List<ProcessorTireDetailsItemTypeModel>();
            }

            model.TireDetails.ProductsProduceType = GetProductProcessDetailsItemType(model).ToList();
        }

        private IEnumerable<AttachmentModel> GetAttachments(int applicationId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (applicationId > 0)
            {
                attachments = this.fileUploadService.GetApplicationAttachments(applicationId);
            }
            return attachments;
        }

        private IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (vendorId > 0)
            {
                attachments = this.fileUploadService.GetVendorAttachments(vendorId);
            }
            return attachments;
        }

        private void DeleteAttachment(int applicationId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadService.GetApplicationAttachment(applicationId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileFullPath = Path.Combine(uploadRepositoryPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadService.RemoveApplicationAttachment(applicationId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }

        private IList<ProcessorTireDetailsItemTypeModel> GetTireDetailsItemType(ProcessorRegistrationModel model)
        {
            //This method formats the TireDetailsItems list and creates an item model to populate
            IList<string> tiredetailsItemType = model.TireDetailsItemTypeString;
            var tireDetailsResult = new List<ProcessorTireDetailsItemTypeModel>();
            string name, shortname, datamyattri;
            int id;
            bool isChecked;

            foreach (string item in tiredetailsItemType)
            {
                string[] split = item.Split(':');
                datamyattri = split[0];
                Boolean.TryParse(split[1], out isChecked);
                Int32.TryParse(split[split.Length - 1], out id);

                if (datamyattri.Contains('-'))
                {
                    //datamyattri tries to split by - for name and shortname and get the final portion
                    string[] namesplit = datamyattri.Split('-');
                    name = namesplit[namesplit.Length - 1];
                    shortname = namesplit[0];

                    tireDetailsResult.Add(new ProcessorTireDetailsItemTypeModel() { Name = name, ShortName = shortname, isChecked = isChecked, ID = id });
                }
            }
            return tireDetailsResult;
        }
        private IList<ProcessorTireDetailsItemTypeModel> GetProductProcessDetailsItemType(ProcessorRegistrationModel model)
        {
            //This method formats the TireDetailsItems list and creates an item model to populate
            IList<string> tiredetailsItemType = model.ProductsProduceTypeString;
            var tireDetailsResult = new List<ProcessorTireDetailsItemTypeModel>();
            string name, shortname, datamyattri;
            int id;
            bool isChecked;

            foreach (string item in tiredetailsItemType)
            {
                string[] split = item.Split(':');
                datamyattri = split[0];
                Boolean.TryParse(split[1], out isChecked);
                Int32.TryParse(split[split.Length - 1], out id);

                if (datamyattri.Contains('-'))
                {
                    //datamyattri tries to split by - for name and shortname and get the final portion
                    string[] namesplit = datamyattri.Split('~');
                    name = namesplit[namesplit.Length - 1];
                    shortname = namesplit[0];

                    tireDetailsResult.Add(new ProcessorTireDetailsItemTypeModel() { Name = name, ShortName = shortname, isChecked = isChecked, ID = id });
                }
                else
                {
                    string[] namesplit = datamyattri.Split('~');
                    name = "";
                    shortname = namesplit[0];
                    tireDetailsResult.Add(new ProcessorTireDetailsItemTypeModel() { Name = name, ShortName = shortname, isChecked = isChecked, ID = id });

                }
            }
            return tireDetailsResult;
        }

        private bool ComposeAndSendApplicationApprovalEmail(ApplicationEmailModel model)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(model.Email);
            MailMessage message = new MailMessage(from, to) { };

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.ProcessorApplicationApprovedEmailTempl"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("ApprovedApplicationProcessor");
            string subject = EmailContentHelper.GetSubjectByName("ApprovedApplicationProcessor");

            if ("loginpage" != this.inviteUrl)//No existing Invitation
            {
                this.inviteUrl = string.Format("{0}{1}{2}", SiteSettings.Instance.DomainName, "/System/Invitation/AcceptInvitation/", model.InvitationToken);
            }
            else
            {
                this.inviteUrl = string.Format("{0}", SiteSettings.Instance.DomainName);
            }
            DateTime expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));


            emailBody = emailBody
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.AcceptInvitationUrl, this.inviteUrl)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.InvitationExpirationDateTime, expireDate.ToString(TreadMarksConstants.DateFormat) + " at " + expireDate.ToString("hh:mm tt"))
                .Replace(ProcessorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = ProcessorApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = ProcessorApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = ProcessorApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            //string subject = MessageResource.ProcessorApplicationApprovedEmailSubject;

            bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();
            SiteSettings.Instance.GetSettingValue("Invitation.WelcomeSubject");
            Task.Factory.StartNew(() =>
            {
                //OTSTM2-132 BCC value update
                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, null, bSendBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, model, null, alternateViewHTML);
            });
            return true;
        }

        private void InitializeClientMessages()
        {
            //BUSINESS INFORMATION VALIDATION
            ViewBag.ValidationMsgInvalidBusinessName = MessageResource.ValidationMsgInvalidBusinessName;
            ViewBag.ValidationMsgInvalidOperatingName = MessageResource.ValidationMsgInvalidOperatingName;
            ViewBag.ValidationMsgInvalidAddressLine1 = MessageResource.ValidationMsgInvalidAddressLine1;
            ViewBag.ValidationMsgInvalidCity = MessageResource.ValidationMsgInvalidCity;
            ViewBag.ValidationMsgInvalidProvinceState = MessageResource.ValidationMsgInvalidProvinceState;

            ViewBag.ValidationMsgInvalidAddressLine2 = MessageResource.ValidationMsgInvalidAddressLine2;
            ViewBag.ValidationMsgInvalidPostalCode = MessageResource.ValidationMsgInvalidPostalCode;
            ViewBag.ValidationMsgInvalidCountry = MessageResource.ValidationMsgInvalidCountry;
            ViewBag.ValidationMsgInvalidExt = MessageResource.ValidationMsgInvalidExt;
            ViewBag.ValidationMsgInvalidEmail = MessageResource.ValidationMsgInvalidEmail;

            ViewBag.ValidationMsgInvalidPrimaryContactName = MessageResource.ValidationMsgInvalidPrimaryContactName;
            ViewBag.ValidationMsgInvalidPosition = MessageResource.ValidationMsgInvalidPosition;
            ViewBag.ValidationMsgInvalidPhone = MessageResource.ValidationMsgInvalidPhone;
            ViewBag.ValidationMsgExpectedFormat = MessageResource.ValidationMsgExpectedFormat;
            ViewBag.ValidationMsgInvalidCapacity = MessageResource.ValidationMsgInvalidCapacity;

            ViewBag.ValidationMsgInvalidCertificateOfApproval = MessageResource.ValidationMsgInvalidCertificateOfApproval;
            ViewBag.ValidationMsgInvalidBusinessStartDate = MessageResource.ValidationMsgInvalidBusinessStartDate;
            ViewBag.ValidationMsgInvalidOntarioBusinessNumber = MessageResource.ValidationMsgInvalidOntarioBusinessNumber;
            ViewBag.ValidationMsgInvalidHSTRegistrationNumber = MessageResource.ValidationMsgInvalidHSTRegistrationNumber;
            ViewBag.ValidationMsgInvalidComercialLiabilityInsurance = MessageResource.ValidationMsgInvalidComercialLiabilityInsurance;

            ViewBag.ValidationMsgInvalidExpiryDate = MessageResource.ValidationMsgInvalidExpiryDate;
            ViewBag.ValidationMsgInvalidCVORNumber = MessageResource.ValidationMsgInvalidCVORNumber;
            ViewBag.ValidationMsgInvalidWSIPNumber = MessageResource.ValidationMsgInvalidWSIPNumber;
            ViewBag.ValidationMsgInvalidFirstName = MessageResource.ValidationMsgInvalidFirstName;
            ViewBag.ValidationMsgInvalidLastName = MessageResource.ValidationMsgInvalidLastName;

            ViewBag.ValidationMsgInvalidFullName = MessageResource.ValidationMsgInvalidFullName;
            ViewBag.ValidationMsgMustBeCheckedForSubmission = MessageResource.ValidationMsgMustBeCheckedForSubmission;
            ViewBag.ValidationMsgInvalidTireType = MessageResource.ValidationMsgInvalidTireType;
            ViewBag.ValidationMsgInvalidProcessor = MessageResource.ValidationMsgInvalidProcessor;
        }

        #endregion
    }
}