﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.Security.Authorization;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using System.Diagnostics;

using CL.TMS.Common.Extension;
using CL.TMS.Framework.DTO;
using CL.TMS.Security;
using CL.TMS.UI.Common.Security;
using CL.TMS.ServiceContracts.ConfigurationsServices;

namespace CL.TMS.Web.Areas.Processor.Controllers
{
    public class TransactionController : BaseController
    {
        #region Properties
        private ITransactionService transactionService;
        private IRegistrantService registrantService;
        private IClaimService claimService;
        private IConfigurationsServices configurationsServices;
        #endregion


        #region Constructor
        public TransactionController(ITransactionService transactionService, IRegistrantService registrantService, IClaimService claimService, IConfigurationsServices configurationsServices)
        {
            this.transactionService = transactionService;
            this.registrantService = registrantService;
            this.claimService = claimService;
            this.configurationsServices = configurationsServices;
        }
        #endregion


        #region Action Methods
        public ActionResult Index(int id, string type, int direction)
        {
            ViewBag.ngApp = "TransactionApp";
            ViewBag.VendorCode = TreadMarksConstants.Processor;

            var claimStatus = this.claimService.GetClaimStatus(id);

            ViewBag.ClaimId = id;
            ViewBag.TypeId = type;
            ViewBag.Direction = direction;

            if (null != SecurityContextHelper.CurrentVendor)
            {
                ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;

                var vendorReference = claimService.GetVendorReference(id);
                if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 4)
                {
                    if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                    {
                        return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                    }
                    else
                    {
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                    }
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = (SecurityContextHelper.IsStaff() && claimStatus == ClaimStatus.UnderReview);

            //OTSTM2-155
            bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryOutbound) == SecurityResultType.EditSave);
            switch (type)
            {
                case TreadMarksConstants.PTR:
                    bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                    break;

                case TreadMarksConstants.PIT:
                    if (direction == 1) bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                    break;
                default:
                    break;
            };

            if (SecurityContextHelper.IsStaff()) ViewBag.AllowAddTransaction = (this.claimService.IsClaimAllowAddNewTransaction(id) && bWritePermission);
            else ViewBag.AllowAddTransaction = (bWritePermission && claimStatus == ClaimStatus.Open);

            ViewBag.ClaimPeriod = this.claimService.GetClaimPeriod(id).ShortName;
            ViewBag.AllowEdit = bWritePermission;

            switch (type)
            {
                case TreadMarksConstants.PTR:
                    ViewBag.TransactionType = TreadMarksConstants.PTR;
                    ViewBag.HeaderName = "PTR Transactions";
                    return View("PTRTransactions");

                case TreadMarksConstants.PIT:
                    ViewBag.TransactionType = TreadMarksConstants.PIT;
                    ViewBag.HeaderName = (direction == 1) ? "Inbound PIT Transactions" : "Outbound PIT Transactions";

                    if (direction == 1) return View("PITTInBoundTransactions");
                    else
                    {
                        // Only staff can add papaer form in Outbound 
                        ViewBag.AllowAddTransaction = (SecurityContextHelper.IsStaff() && ViewBag.AllowAddTransaction);
                        return View("PITTOutBoundTransactions");
                    }

                case TreadMarksConstants.SPS:
                    ViewBag.TransactionType = TreadMarksConstants.SPS;
                    ViewBag.HeaderName = "SPS Transactions";
                    return View("SPSTransactions");

                case TreadMarksConstants.DOR:
                    ViewBag.TransactionType = TreadMarksConstants.DOR;
                    ViewBag.HeaderName = "DOR Transactions";
                    return View("DORTransactions");

                default:
                    return RedirectToAction("Index", "Claims", new { area = "Processor" });
            };
        }
        #endregion


        #region Service Methods

        #region Paper Form Service Methods
        [HttpPost]
        public NewtonSoftJsonResult initializePaperForm(int id, string type, int direction)
        {
            var claimPeriod = this.claimService.GetClaimPeriod(id);

            var processorClaimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
            var paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;
            var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
            var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;
            var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(SecurityContextHelper.CurrentVendor.Address.PostalCode, 2);
            Rate OnRoadRate, OffRoadRate;

            IProcessorTransactionViewModel paperFormModel = new ProcessorTransactionViewModel();

            //RateGroupRate changes
            var rateGroupRates = configurationsServices.LoadRateGroupRates(claimPeriod.StartDate, claimPeriod.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsServices.LoadVendorRateGroups((int)c));
            });
            var deliveryGroup = vendorRateGroups.FirstOrDefault(q => q.VendorId == SecurityContextHelper.CurrentVendor.VendorId);
            var onRoadRate = 0m;
            var offRoadRate = 0m;
            if (deliveryGroup != null)
            {
                var ptrItemOnRoadRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 1 && r.ProcessorGroupId == deliveryGroup.GroupId);
                onRoadRate = ptrItemOnRoadRate != null ? ptrItemOnRoadRate.Rate : 0;
                var ptrItemOffRoadRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 2 && r.ProcessorGroupId == deliveryGroup.GroupId);
                offRoadRate = ptrItemOffRoadRate != null ? ptrItemOffRoadRate.Rate : 0;
            }

            switch (type)
            {
                case TreadMarksConstants.PTR:
                    paperFormModel = new ProcessorPTRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.PTR)));

                    var vmPTR = paperFormModel as ProcessorPTRTransactionViewModel;

                    vmPTR.RateDetail.OnRoadRate = onRoadRate;
                    vmPTR.RateDetail.OffRoadRate = offRoadRate;
                    break;

                case TreadMarksConstants.PIT:
                    paperFormModel = new ProcessorPITTransactionViewModel();
                    paperFormModel.initialize();

                    var vmPIT = paperFormModel as ProcessorPITTransactionViewModel;
                    vmPIT.IsInbound = (direction == 1);
                    vmPIT.ProductList.AddRange(TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue, SecurityContextHelper.CurrentVendor.VendorId, claimPeriod.StartDate, claimPeriod.EndDate));
                    break;

                case TreadMarksConstants.SPS:
                    paperFormModel = new ProcessorSPSTransactionViewModel();
                    paperFormModel.initialize();

                    var vmSPS = paperFormModel as ProcessorSPSTransactionViewModel;
                    vmSPS.CountryList.AddRange(DataLoader.GetCountriesByIso3166());
                    vmSPS.ProductList.AddRange(TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue, SecurityContextHelper.CurrentVendor.VendorId, claimPeriod.StartDate, claimPeriod.EndDate));
                    break;

                case TreadMarksConstants.DOR:
                    paperFormModel = new ProcessorDORTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.DOR)));

                    var vmDOR = paperFormModel as ProcessorDORTransactionViewModel;
                    vmDOR.CountryList.AddRange(DataLoader.GetCountriesByIso3166());
                    vmDOR.WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg, TreadMarksConstants.Ton };
                    vmDOR.MaterialTypeList = DataLoader.MaterialTypeList;
                    vmDOR.DispositionReasonList = DataLoader.DispositionReasonList;

                    if (onRoadRate > 0)
                    {
                        var rateInTon = Math.Round(DataConversion.ConvertTonToKg((double)onRoadRate), 3, MidpointRounding.AwayFromZero);
                        vmDOR.RateDetail.OnRoadRate = rateInTon;
                        vmDOR.ProductDetail.OnRoadRate = rateInTon;
                    }

                    if (offRoadRate > 0)
                    {
                        var rateInTon = Math.Round(DataConversion.ConvertTonToKg((double)offRoadRate), 3, MidpointRounding.AwayFromZero);
                        vmDOR.RateDetail.OffRoadRate = rateInTon;
                        vmDOR.ProductDetail.OffRoadRate = rateInTon;
                    }
                    break;

                default:
                    return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            };

            paperFormModel.CurrentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
            paperFormModel.ClaimId = id;

            // Form date related setting            
            TransactionHelper.initializeFormDate(paperFormModel.FormDate, claimPeriod, ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary));
            paperFormModel.FormDate.DisabledDateRangeList = new List<TransactionDisabledDateRangeViewModel>();
            paperFormModel.FormDate.DisabledDateRangeList.AddRange(transactionService.GetVendorInactiveDateRanges(SecurityContextHelper.CurrentVendor.VendorId));


            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = paperFormModel } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionPTR(ProcessorPTRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                transactionDataModel.Outbound = registrantService.GetVendorByNumber(transactionDataModel.HaulerNumber.ToString());

                var creationNotification = transactionService.AddProcessorPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Outbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionPIT(ProcessorPITTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                if (transactionDataModel.IsInbound)
                {                    
                    transactionDataModel.Inbound = SecurityContextHelper.CurrentVendor;
                    transactionDataModel.Outbound = registrantService.GetVendorByNumber(transactionDataModel.ProcessorNumber.ToString());
                }
                else
                {
                    transactionDataModel.Inbound = registrantService.GetVendorByNumber(transactionDataModel.ProcessorNumber.ToString());
                    transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;
                }

                //OTSTM2-1198 over-writing the itemrate for product selected from UI as it should use outbound processor's rates
                var claimPeriod = this.claimService.GetClaimPeriod(transactionDataModel.ClaimId);
                decimal ItemRate = 0.00M;
                var specificRate = DataLoader.ProcessorSpecificRates.FirstOrDefault(c => c.ItemID == transactionDataModel.ProductDetail.ProductTypeId && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= claimPeriod.StartDate.Date && c.EffectiveEndDate >= claimPeriod.EndDate.Date && c.VendorID == transactionDataModel.Outbound.VendorId);
                if (specificRate != null)
                {
                    ItemRate = specificRate.ItemRate;
                }
                else
                {
                    Rate globalRate;
                    globalRate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == transactionDataModel.ProductDetail.ProductTypeId && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= claimPeriod.StartDate.Date && c.EffectiveEndDate >= claimPeriod.EndDate.Date);
                    ItemRate = globalRate != null ? globalRate.ItemRate : 0.00M;
                }
                transactionDataModel.ProductDetail.ProductRate = ItemRate;

                var creationNotification = transactionService.AddProcessorPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.IsInbound && transactionDataModel.Outbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);
                if (!transactionDataModel.IsInbound && transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionSPS(ProcessorSPSTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = (transactionDataModel.SoldTo == "registered" && transactionDataModel.RPMNumber.HasValue) ? registrantService.GetVendorByNumber(transactionDataModel.RPMNumber.Value.ToString()) : null;
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                var creationNotification = transactionService.AddProcessorPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionDOR(ProcessorDORTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = null;
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                transactionService.AddProcessorPapeFormTransaction(transactionDataModel);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }
        #endregion

        #region List Handler Service Methods
        public ActionResult GetTransactionListHandler(DataGridModelExtend param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();

                    switch (transactionType)
                    {
                        case TreadMarksConstants.SPS:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "VendorNumber");

                            columns.Add(5, "VendorBusinessName");
                            columns.Add(6, "Destination");
                            columns.Add(7, "InvoiceNbr");
                            columns.Add(8, "ProductType");
                            columns.Add(9, "MeshRange");
                            columns.Add(10, "ScaleWeight");

                            columns.Add(11, "Rate");
                            columns.Add(12, "AmountDollar");
                            break;


                        case TreadMarksConstants.PTR:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "Badge");
                            columns.Add(2, "TransactionDate");
                            columns.Add(3, "TransactionFriendlyId");
                            columns.Add(4, "VendorNumber");

                            columns.Add(5, "Adjustment");
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, "ScaleTicketNbr");
                            columns.Add(9, "ScaleWeight");

                            columns.Add(10, "EstWeight");
                            columns.Add(11, "Variance");
                            columns.Add(12, "AmountDollar");
                            break;

                        case TreadMarksConstants.DOR:
                            columns.Add(0, "TransactionFriendlyId");
                            columns.Add(1, "ReviewStatus");
                            columns.Add(2, "Adjustment");
                            columns.Add(3, "TransactionDate");
                            columns.Add(4, "VendorBusinessName");

                            columns.Add(5, "Destination");
                            columns.Add(6, "MaterialType");
                            columns.Add(7, "DispositionReason");
                            columns.Add(8, "ScaleTicketNbr");
                            columns.Add(9, "ScaleWeight");

                            columns.Add(10, "AmountDollar");
                            break;

                        case TreadMarksConstants.PIT:
                            if (param.DataSubType.ToLower().Contains("inbound"))
                            {
                                columns.Add(0, "DeviceName");
                                columns.Add(1, "Badge");
                                columns.Add(2, "TransactionFriendlyId");
                                columns.Add(3, "ReviewStatus");
                                columns.Add(4, "Adjustment");

                                columns.Add(5, "TransactionDate");
                                columns.Add(6, "VendorNumber");
                                columns.Add(7, "VendorBusinessName");
                                columns.Add(8, "ProductType");
                                columns.Add(9, "ScaleTicketNbr");

                                columns.Add(10, "ScaleWeight");
                            }
                            else
                            {
                                columns.Add(0, "DeviceName");
                                columns.Add(1, "Badge");
                                columns.Add(2, "TransactionFriendlyId");
                                columns.Add(3, "ReviewStatus");
                                columns.Add(4, "Adjustment");

                                columns.Add(5, "TransactionDate");
                                columns.Add(6, "VendorNumber");
                                columns.Add(7, "VendorBusinessName");
                                columns.Add(8, "ProductType");
                                columns.Add(9, "ScaleTicketNbr");

                                columns.Add(10, "ScaleWeight");
                                columns.Add(11, "Rate");
                                columns.Add(12, "AmountDollar");
                            }
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    //var transactions = transactionService.LoadProcessorVendorTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);
                    PaginationDTO<ProcessorCommonTransactionListViewModel, int> transactions = new PaginationDTO<ProcessorCommonTransactionListViewModel, int>();

                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryOutbound) == SecurityResultType.EditSave);

                    switch (transactionType)
                    {
                        case TreadMarksConstants.PTR:
                            transactions = transactionService.LoadProcessorPTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.DOR:
                            transactions = transactionService.LoadProcessorDORTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.PIT:
                            transactions = transactionService.LoadProcessorPITTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId, vendor.VendorId, param.DataSubType);
                            if (param.DataSubType.Contains("inbound"))
                            {
                                bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                            }
                            break;
                        case TreadMarksConstants.SPS:
                            transactions = transactionService.LoadProcessorSPSTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }


                    var json = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json, false);
                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData, error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetProcessorStaffTransactionsList(DataGridModelExtend param)
        {
            //var allTransactions = this.transactionService.LoadAllTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, VendorId);

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    columns = new Dictionary<int, string>();
                    switch (transactionType)
                    {
                        case TreadMarksConstants.DOR:
                            columns.Add(0, "TransactionFriendlyId");
                            columns.Add(1, "ReviewStatus");
                            columns.Add(2, "Adjustment");
                            columns.Add(3, "Notes");
                            columns.Add(4, "TransactionDate");

                            columns.Add(5, "VendorBusinessName");
                            columns.Add(6, "Destination");
                            columns.Add(7, "MaterialTypeName");
                            columns.Add(8, "DispositionReason");
                            columns.Add(9, "ScaleTicketNbr");

                            columns.Add(10, "ScaleWeight");
                            columns.Add(11, "AmountDollar");
                            break;

                        case TreadMarksConstants.PIT:
                            if (param.DataSubType.ToLower().Contains("inbound"))
                            {
                                columns.Add(0, "DeviceName");
                                columns.Add(1, "TransactionFriendlyId");
                                columns.Add(2, "ReviewStatus");
                                columns.Add(3, "Adjustment");
                                columns.Add(4, "Notes");

                                columns.Add(5, "TransactionDate");
                                columns.Add(6, "VendorNumber");
                                columns.Add(7, "ProductType");
                                columns.Add(8, "ScaleTicketNbr");
                                columns.Add(9, "ScaleWeight");
                            }
                            else
                            {
                                columns.Add(0, "DeviceName");
                                columns.Add(1, "TransactionFriendlyId");
                                columns.Add(2, "ReviewStatus");
                                columns.Add(3, "Adjustment");
                                columns.Add(4, "Notes");

                                columns.Add(5, "TransactionDate");
                                columns.Add(6, "VendorNumber");
                                columns.Add(7, "ProductType");
                                columns.Add(8, "ScaleTicketNbr");
                                columns.Add(9, "ScaleWeight");

                                columns.Add(10, "Rate");
                                columns.Add(11, "AmountDollar");
                            }
                            break;

                        case TreadMarksConstants.PTR:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "TransactionDate");
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, TreadMarksConstants.PLT);
                            columns.Add(8, TreadMarksConstants.MT);
                            columns.Add(9, TreadMarksConstants.AGLS);

                            columns.Add(10, TreadMarksConstants.IND);
                            columns.Add(11, TreadMarksConstants.SOTR);
                            columns.Add(12, TreadMarksConstants.MOTR);
                            columns.Add(13, TreadMarksConstants.LOTR);
                            columns.Add(14, TreadMarksConstants.GOTR);

                            columns.Add(15, "ScaleWeight");
                            columns.Add(16, "EstWeight");
                            columns.Add(17, "Variance");
                            columns.Add(18, "AmountDollar");
                            break;

                        case TreadMarksConstants.SPS:
                            columns.Add(0, "InvoiceDate");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "VendorNumber");
                            columns.Add(6, "VendorBusinessName");
                            columns.Add(7, "Destination");
                            columns.Add(8, "InvoiceNbr");
                            columns.Add(9, "ProductType");
                            columns.Add(10, "MeshRange");

                            columns.Add(11, "ScaleWeight");
                            columns.Add(12, "Rate");
                            columns.Add(13, "AmountDollar");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    //var transactions = transactionService.LoadProcessorStaffTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);

                    PaginationDTO<ProcessorCommonTransactionListViewModel, int> transactions = new PaginationDTO<ProcessorCommonTransactionListViewModel, int>();

                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryOutbound) == SecurityResultType.EditSave);
                    switch (transactionType)
                    {
                        case TreadMarksConstants.PTR:
                            transactions = transactionService.LoadProcessorStaffPTRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                            break;
                        case TreadMarksConstants.DOR:
                            transactions = transactionService.LoadProcessorStaffDORTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.PIT:
                            transactions = transactionService.LoadProcessorStaffPITTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId, vendor.VendorId, param.DataSubType);
                            if (param.DataSubType.Contains("inbound"))
                            {
                                bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryInbound) == SecurityResultType.EditSave);
                            }
                            break;
                        case TreadMarksConstants.SPS:
                            transactions = transactionService.LoadProcessorStaffSPSTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }

                    //OTSTM2-1064
                    var cBTotalTireCount = Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBTotalTireCount").Value);
                    var totalTireCountPTR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountPTR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountPTR").Value);
                    var totalTireCountDOR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOR").Value);

                    transactions.DTOCollection.ForEach(c =>
                    {
                        c.SetFlagQtyOverPTR(cBTotalTireCount, totalTireCountPTR);
                        c.SetFlagQtyOverDOR(cBTotalTireCount, totalTireCountDOR);
                    });

                    var json1 = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };

                    return new NewtonSoftJsonResult(json1, false);
                }
            }
            return Json(new { status = "", error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export to CSV Service Methods
        public ActionResult ExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            List<Func<ProcessorCommonTransactionListViewModel, string>> delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.PTR:
                    columns.Add("iPad/Form");
                    columns.Add("Badge");
                    columns.Add("Transaction Date");
                    columns.Add("PTR #");
                    columns.Add("Review Status");

                    columns.Add("Adjusted By");
                    columns.Add("Hauler");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);

                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Business Name");
                    columns.Add("Scale Ticket#");
                    columns.Add("Acutal(KG)");

                    columns.Add("Est.(KG)");
                    columns.Add("Var.(%)");
                    columns.Add("Amount.($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge,
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),

                        u => u.Adjustment.ToString(),
                        u => u.VendorNumber,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),

                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.VendorBusinessName,
                        u => u.ScaleTicketNbr.ToString(),
                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),

                        u => u.EstWeight.ToString(),
                        u => Math.Round((u.Variance ?? 0), 2, MidpointRounding.AwayFromZero).ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                case TreadMarksConstants.SPS:
                    columns.Add("Invoice Date");
                    columns.Add("SPS #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("RPM #");

                    columns.Add("Business Name");
                    columns.Add("Destination");
                    columns.Add("Invoice #");
                    columns.Add("Product Type");
                    columns.Add("Mesh Range");
                    columns.Add("Actual(t)");

                    columns.Add("Rate(t)");
                    columns.Add("Amount($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.InvoiceDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.VendorNumber,

                        u => u.VendorBusinessName,
                        u => u.Destination,
                        u => u.InvoiceNbr,
                        u => u.ProductType,
                        u => u.MeshRange,
                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),

                        u => u.Rate.ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                case TreadMarksConstants.PIT:
                    if (dataSubType.ToLower().Contains("inbound"))
                    {
                        columns.Add("iPad/Form");
                        columns.Add("Badge");
                        columns.Add("PIT #");
                        columns.Add("Review Status");
                        columns.Add("Adjusted By");

                        columns.Add("Date");
                        columns.Add("Processor");
                        columns.Add("Business Name");
                        columns.Add("Product Type");
                        columns.Add("Scale Ticket#");

                        columns.Add("Actual(t)");

                        delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                        {
                            u => u.DeviceName.ToString(),
                            u => u.Badge.ToString(),
                            u => u.TransactionFriendlyId.ToString(),
                            u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                            u => u.Adjustment.ToString(),

                            u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                            u => u.VendorNumber,
                            u => u.VendorBusinessName,
                            u => u.ProductType,
                            u => u.ScaleTicketNbr,

                            u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        };
                    }
                    else
                    {
                        columns.Add("iPad/Form");
                        columns.Add("Badge");
                        columns.Add("PIT #");
                        columns.Add("Review Status");
                        columns.Add("Adjusted By");

                        columns.Add("Date");
                        columns.Add("Processor");
                        columns.Add("Business Name");
                        columns.Add("Product Type");
                        columns.Add("Scale Ticket#");

                        columns.Add("Actual(t)");
                        columns.Add("Rate($/t)");
                        columns.Add("Amount($)");
                        delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                        {
                            u => u.DeviceName.ToString(),
                            u => u.Badge.ToString(),
                            u => u.TransactionFriendlyId.ToString(),
                            u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                            u => u.Adjustment.ToString(),
                            //u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                            u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                            u => u.VendorNumber,
                            u => u.VendorBusinessName,
                            u => u.ProductType,
                            u => u.ScaleTicketNbr,

                            u => Math.Round(u.ScaleWeight ?? 0,2, MidpointRounding.AwayFromZero).ToString(),
                            u => u.Rate.ToString(),
                            u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                        };
                    }
                    break;

                case TreadMarksConstants.DOR:
                    columns.Add("DOR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Transaction Date");
                    columns.Add("Reg.#/Business Name");

                    columns.Add("Destination");
                    columns.Add("Material Type");
                    columns.Add("Reason");
                    columns.Add("Scale Ticket#");
                    columns.Add("Actual(t)");

                    columns.Add("Amount($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorBusinessName,

                        u => u.Destination,
                        u => u.MaterialTypeName,
                        u => u.DispositionReason,
                        u => u.ScaleTicketNbr.ToString(),
                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                default:
                    break;
            };
            #endregion

            List<ProcessorCommonTransactionListViewModel> transactions = new List<ProcessorCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.PTR:
                    transactions = transactionService.LoadProcessorPTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.DOR:
                    transactions = transactionService.LoadProcessorDORTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.PIT:
                    transactions = transactionService.LoadProcessorPITTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId, vendor.VendorId, dataSubType).DTOCollection;
                    break;
                case TreadMarksConstants.SPS:
                    transactions = transactionService.LoadProcessorSPSTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }

            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult StaffExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            int vendorId = (null != vendor) ? vendor.VendorId : 0;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            List<Func<ProcessorCommonTransactionListViewModel, string>> delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.PTR:
                    columns.Add("iPad/Form");
                    columns.Add("PTR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");

                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);

                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);

                    columns.Add("Acutal(KG)");
                    columns.Add("Est.(KG)");
                    columns.Add("Var.(%)");
                    columns.Add("Amount.($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),

                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),

                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        u => u.EstWeight.ToString(),
                        u => Math.Round((u.Variance ?? 0), 2, MidpointRounding.AwayFromZero).ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                case TreadMarksConstants.SPS:
                    columns.Add("Invoice Date");
                    columns.Add("SPS #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");

                    columns.Add("RPM #");
                    columns.Add("Business Name");
                    columns.Add("Destination");
                    columns.Add("Invoice #");
                    columns.Add("Product Type");
                    columns.Add("Mesh Range");

                    columns.Add("Actual(t)");
                    columns.Add("Rate(t)");
                    columns.Add("Amount($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.InvoiceDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                        u => u.VendorNumber,
                        u => u.VendorBusinessName,
                        u => u.Destination,
                        u => u.InvoiceNbr,
                        u => u.ProductType,
                        u => u.MeshRange,

                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        u => u.Rate.ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                case TreadMarksConstants.PIT:
                    if (dataSubType.ToLower().Contains("inbound"))
                    {
                        columns.Add("iPad/Form");
                        columns.Add("PIT #");
                        columns.Add("Review Status");
                        columns.Add("Adjusted By");
                        columns.Add("Notes");

                        columns.Add("Date");
                        columns.Add("Processor");
                        columns.Add("Product Type");
                        columns.Add("Scale Ticket#");
                        columns.Add("Actual(t)");

                        delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                        {
                            u => u.DeviceName.ToString(),
                            u => u.TransactionFriendlyId.ToString(),
                            u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                            u => u.Adjustment.ToString(),
                            u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                            u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                            u => u.VendorNumber,
                            u => u.ProductType,
                            u => u.ScaleTicketNbr,
                            u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        };
                    }
                    else
                    {
                        columns.Add("iPad/Form");
                        columns.Add("PIT #");
                        columns.Add("Review Status");
                        columns.Add("Adjusted By");
                        columns.Add("Notes");

                        columns.Add("Date");
                        columns.Add("Processor");
                        columns.Add("Product Type");
                        columns.Add("Scale Ticket#");
                        columns.Add("Actual(t)");

                        columns.Add("Rate($/t)");
                        columns.Add("Amount($)");
                        delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                        {
                            u => u.DeviceName.ToString(),
                            u => u.TransactionFriendlyId.ToString(),
                            u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                            u => u.Adjustment.ToString(),
                            u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                            u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                            u => u.VendorNumber,
                            u => u.ProductType,
                            u => u.ScaleTicketNbr,
                            u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),

                            u => u.Rate.ToString(),
                            u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                        };
                    }
                    break;

                case TreadMarksConstants.DOR:
                    columns.Add("DOR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");

                    columns.Add("Reg.#/Business Name");
                    columns.Add("Destination");
                    columns.Add("Material Type");
                    columns.Add("Reason");
                    columns.Add("Scale Ticket#");

                    columns.Add("Actual(t)");
                    columns.Add("Amount($)");

                    delegates = new List<Func<ProcessorCommonTransactionListViewModel, string>>()
                    {
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),

                        u => u.VendorBusinessName,
                        u => u.Destination,
                        u => u.MaterialTypeName,
                        u => u.DispositionReason,
                        u => u.ScaleTicketNbr.ToString(),

                        u => Math.Round(u.ScaleWeight ?? 0, 4, MidpointRounding.AwayFromZero).ToString(),
                        u => String.Format( "{0:#,##0.##}", u.AmountDollar)
                    };
                    break;

                default:
                    break;
            };
            #endregion

            List<ProcessorCommonTransactionListViewModel> transactions = new List<ProcessorCommonTransactionListViewModel>();

            switch (transactionType)
            {
                case TreadMarksConstants.PTR:
                    transactions = transactionService.LoadProcessorStaffPTRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.DOR:
                    transactions = transactionService.LoadProcessorStaffDORTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.PIT:
                    transactions = transactionService.LoadProcessorStaffPITTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId, vendor.VendorId, dataSubType).DTOCollection;
                    break;
                case TreadMarksConstants.SPS:
                    transactions = transactionService.LoadProcessorStaffSPSTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }

            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #endregion
    }
}