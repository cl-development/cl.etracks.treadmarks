﻿$(function () {
    var pageSize = 5;
    //Define data table
    var table = $('#tblProcessorTransactionAdjusList').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.ParticipantTransactionAdjustmentSummary.TransAdjustmentHandleUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY: 300,
        scrollCollapse: false,
        searching: true,
        ordering: true,
        order: [[0, "asc"]],

        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },

        columns: [
                 {
                     name: "TransactionType",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.TransactionType + "</span>";
                     }
                 },
                 {
                     name: "TransactionNumber",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.TransactionNumber + "</span>";
                     }
                 },
                 {
                     name: "TransactionDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         if (data.TransactionDate != null && data.TransactionDate !== undefined) {
                             return dateTimeConvert(data.TransactionDate);
                         } else {
                             return '';
                         }
                     }
                 },
                 {
                     name: "StartedBy",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.StartedBy + "</span  >";
                     }
                 },
                 {
                     name: "AdjustmentDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         if (data.AdjustmentDate != null && data.AdjustmentDate !== undefined) {
                             return dateTimeConvert(data.AdjustmentDate);
                         } else {
                             return '';
                         }
                     }
                 },
                 {
                     name: "Status",
                     className: "td-center",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.Status + "</span>";
                     }
                 },
                 {
                     name: "Actions",
                     data: null,
                     className: 'td-center',
                     orderable: false,
                     render: function (data, type, full, meta) {
                         var content = "<div class='btn-group dropdown-menu-actions'><a href='javascript:void(0)' data-toggle='popover'" +
                             " id='actions" + meta.row + "' data-original-title='' title=''><i class='glyphicon glyphicon-cog'></i></a><div class='popover-menu'><ul>";
                         if (data.Status == "Requested") {
                             content += " <li><a href='#' data-toggle='modal' data-target='#modalTransAdjustmentsRecallConfirm' data-rowId='" + meta.row + "'><i class='fa fa-reply'></i> Recall</a></li>" +
                                 " </ul> </div> </div>";
                         } else if (data.Status == "Underrevied") {
                             content += "<li><a href='#' data-toggle='modal' data-target='#modalEditUser' data-rowId='" + meta.row + "'><i class='fa fa-check'></i> Accept</a></li>" +
                                 " <li><a href='#' data-toggle='modal' data-target='#modalInactivateUser' data-rowId='" + meta.row + "'><i class='fa fa-times'></i> Reject</a></li>" +
                                 " </ul> </div> </div>";
                         } else {
                             content = "";
                         }
                         return content;
                     }
                 },
                 {
                     name: "TransactionAdjustmentId",
                     data: null,
                     className: 'display-none',
                     render: function (data, type, full, meta) {
                         return "<span>" + data.TransactionAdjustmentId + "</span>";
                     }
                 }
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#transactionAdjustFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= pageSize) ? $('#viewmore').css('visibility', 'visible') : $('#viewmore').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#transactionAdjustSearch').val();
            var url = Global.ParticipantTransactionAdjustmentSummary.TransAdjustmentExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#exportTransactionAdj').attr('href', url);
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    $('#viewmore').on('click', function () {
        $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('#transactionAdjustSearch').on('keyup', function () {
        table.search(this.value).draw(false);
    });

    $('#transactionAdjustRemove').on('click', function () {
        table.search("").draw(false);
        $('#transactionAdjustFound').hide();
    });

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    };
});