﻿'use strict';
processorClaimApp.factory('processorClaimService', ["$http", function ($http) {
    var factory = {};

    factory.getClaimDetails = function () {
        return $http.get(Global.ParticipantClaimsSummary.ClaimDetailsUrl)
            .then(function (result) {
                return result.data;
            });
    }
    
    factory.loadPIRatesByTransactionID = function (rateTransactionID) {
        return $http({
            method: 'GET',
            url: Global.ParticipantClaimsSummary.LoadPIRatesByTransactionIDUrl,
            params: {
                rateTransactionID: rateTransactionID                
            }
        });
    }

    factory.submitClaimCheck = function () {
        var submitClaimModel = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
        };

        var submitVal = {
            url: Global.ParticipantClaimsSummary.SubmitClaimCheckUrl,
            method: "POST",
            data: JSON.stringify(submitClaimModel)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.FinalSubmitClaim = function () {
        var req = { claimId: Global.ParticipantClaimsSummary.ClaimsID }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.FinalSubmitClaimUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.exportList = function (data, url) {
        var submitVal = {
            url: url,
            method: "POST",
            data: JSON.stringify(data)
        }
        return $http(submitVal);
    }

    factory.getProcessorTopInfoPickupRateGroupDetail = function (data) {
        var submitVal = {
            url: Global.ParticipantClaimsSummary.VendorRateInfoUrl,
            method: "GET",
            params: data,
        }
        return $http(submitVal);

        return $http.get(Global.ParticipantClaimsSummary.VendorRateInfoUrl)
            .then(function (result) {
                return result.data;
            });
    }

    return factory;
}]);