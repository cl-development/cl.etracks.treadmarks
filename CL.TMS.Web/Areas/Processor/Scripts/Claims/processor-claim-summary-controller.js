﻿'use strict';
processorClaimApp.controller("processorClaimSummaryController", ["$scope", "$filter", "processorClaimService", '$uibModal', 'claimService', function ($scope, $filter, processorClaimService, $uibModal, claimService) {
    $scope.dataloaded = false;
    $scope.submitBtn = false;

    processorClaimService.getClaimDetails().then(function (data) {
        $scope.claimPeriod = data.ClaimCommonModel.ClaimPeriod;
        $scope.vendorId = data.ClaimCommonModel.VendorId;

        //populating claim details
        $scope.ProcessorClaimSummary = data;

        $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
        $scope.disableAddTransAdjBtn = ((data.ClaimCommonModel.StatusString == "Approved") || $scope.PageIsReadonly);
        
        //Submit Info
        $scope.submitInfo = {
            periodShortName: data.ClaimCommonModel.ClaimPeriodName,
            registrationNum: data.ClaimCommonModel.RegistrationNumber
        };
        $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
        $scope.dataloaded = true;

        //open specific rate modal
        $scope.openRateDetailsModal = function () {
            $scope.rateTransactionID = data.AssociatedRateTransactionId;
            var rateDetailModal = $uibModal.open({
                templateUrl: 'modelSpecificRate.html',
                controller: 'loadRateCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    rateTransactionID: function () { return $scope.rateTransactionID; }
                }
            });
        }

        $scope.HasSpecificRate = data.HasSpecificRate;

    });
    var refreshPanels = function () {
        //call claim service to refresh panel
        processorClaimService.getClaimDetails().then(function (data) {
            //populating claim details
            $scope.ProcessorClaimSummary = data;
        });
    }
    $scope.InternalAdjustTypes = [
    {
        name: 'Weight',
        id: '1'
    },
    {
        name: 'Tire Counts',
        id: '2'
    },
    {
        name: 'Payment',
        id: '4'
    }
    ];
    //view internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        var viewItem = $filter('filter')($scope.InternalAdjustTypes, { name: internalAdjustType })[0];
        claimService.getInternalAdjustment(viewItem.id, internalAdjustId).then(function (data) {
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'PEligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (modalResult) {
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'PEligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }


    var category = 1;
    $scope.loadTopInfoBar = function () {
        var isSpecific = true;
        var addNewRateModal = $uibModal.open({
            templateUrl: "processor-modal-load-top-info-bar.html",
            controller: 'loadTopInfoCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                category: function () {
                    return $scope.category;
                },
                vendorId: function () {
                    return $scope.vendorId;
                },
                claimPeriod: function () {
                    return $scope.claimPeriod;
                }
            }
        });
    }


}]);

//Specific Rate rate Controller
processorClaimApp.controller('loadRateCtrl', ['$scope', '$uibModalInstance', 'rateTransactionID', 'processorClaimService', function ($scope, $uibModalInstance, rateTransactionID, processorClaimService) {

    $scope.isView = true;
    $scope.rateTransactionID = rateTransactionID;
    
    processorClaimService.loadPIRatesByTransactionID(rateTransactionID).then(function (data) {
        $scope.rateVM = rateViewModel(data.data);
        $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
    });
    
    function rateViewModel(data) {
        var result = {};
        var prefex = '$';
        if (typeof (data) === 'object') {//session time out handling
            $.each(data, function (name, value) {
                if (value instanceof Object && name != "notes") {
                    var copy = {};
                    $.each(value, function (itemname, itemvalue) {
                        if (typeof itemvalue !== 'object') {
                            if (typeof itemvalue === 'number') {
                                copy[itemname] = prefex + itemvalue.toFixed(data.decimalsize).toString();
                            } else if (typeof itemvalue === 'string') {
                                copy[itemname] = itemvalue;
                            }
                        }
                    })
                    result[name] = copy;
                } else {
                    result[name] = value;
                }
            });
            return result;
        } else {
            console.log('session time out?');
            return false;//return Boolean instead of object[null]
        }
    };

    $scope.cancel = function () {
        if (typeof ($scope.rateVM) === 'object') {//session time out handling

        }
        $uibModalInstance.dismiss('cancel');
    };    
}]);
processorClaimApp.controller('PEligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;

    $scope.confirm = function () {
        $scope.modalResult.selectedItem = $scope.selectedItem;
        $uibModalInstance.close($scope.modalResult);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

processorClaimApp.controller('PEligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

processorClaimApp.controller('ErrorModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'errorMessage', function ($rootScope, $scope, $http, $uibModalInstance, errorMessage) {
    $scope.errorMessage = errorMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

processorClaimApp.controller('WarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

processorClaimApp.controller('ModalSubmitOneModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'processorClaimService', function ($rootScope, $scope, $http, $uibModalInstance, $window, processorClaimService) {

    $scope.finalSubmitClaim = function () {
        processorClaimService.FinalSubmitClaim().then(function (data) {
            if (data.status == "Valid Data") {
                $scope.submitBtn = false;
                $uibModalInstance.close($scope.submitBtn);
                //$window.location.reload();
            }
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

processorClaimApp.controller('ModalSubmitTwoModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'submitInfo', '$window', function ($rootScope, $scope, $http, $uibModalInstance, submitInfo, $window) {

    $scope.submitInfo = submitInfo;

    $scope.confirm = function () {
        $uibModalInstance.close();
        $window.location.reload();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Directives
processorClaimApp.directive('submitClaimcheck', ['processorClaimService', '$uibModal', function (processorClaimService, $uibModal) {
    return {
        restrict: 'E',
        templateUrl: 'claimSubmit.html',
        scope: {
            model: "=model",
            submitInfo: "=submitInfo",
            submitBtn: "=submitBtn",
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: ['$scope', function ($scope) {
            if ($scope.model.ClaimCommonModel.StatusString == "Open")
                $scope.submitBtn = true;
            $scope.submitClaimCheck = function () {
                processorClaimService.submitClaimCheck().then(function (data) {

                    if (data.status != "Invalid data") {
                        if (data.Type == "Error") {
                            var errorInstance = $uibModal.open({
                                templateUrl: 'ErrorModal.html',
                                controller: 'ErrorModalCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    errorMessage: function () {
                                        return data.Message;
                                    }
                                }
                            });
                        }
                        if (data.Type == "Warning") {
                             var warningInstance = $uibModal.open({
                                templateUrl: 'WarningModal.html',
                                controller: 'WarningModalCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    warningMessage: function () {
                                        return data.Warnings;
                                    }
                                }
                            });

                            warningInstance.result.then(function () {

                                var submitOneInstance = $uibModal.open({
                                    templateUrl: 'ModalSubmitOne.html',
                                    controller: 'ModalSubmitOneModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static'
                                });
                                submitOneInstance.result.then(function (data) {
                                    $scope.submitBtn = data;

                                    var submitTwoInstance = $uibModal.open({
                                        templateUrl: 'ModalSubmitTwo.html',
                                        controller: 'ModalSubmitTwoModalCtrl',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            submitInfo: function () {
                                                return $scope.submitInfo;
                                            }
                                        }
                                    });
                                    submitOneInstance.result.then(function (data) {

                                    });
                                });
                            });
                        }
                        if (data.Type == "Success") {
                            var submitOneInstance = $uibModal.open({
                                templateUrl: 'ModalSubmitOne.html',
                                controller: 'ModalSubmitOneModalCtrl',
                                size: 'lg',
                                backdrop: 'static'
                            });
                            submitOneInstance.result.then(function (data) {
                                $scope.submitBtn = data;

                                var submitTwoInstance = $uibModal.open({
                                    templateUrl: 'ModalSubmitTwo.html',
                                    controller: 'ModalSubmitTwoModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        submitInfo: function () {
                                            return $scope.submitInfo;
                                        }
                                    }
                                });
                                submitOneInstance.result.then(function (data) {

                                });
                            });
                        }
                    }
                    else {
                        $scope.submitBtn = true;
                    }
                });
            }
        }]
    }
}]);

processorClaimApp.controller('loadTopInfoCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'vendorId', 'claimPeriod', 'processorClaimService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, category, vendorId, claimPeriod, processorClaimService, $rootScope) {
    var params = {
        vendorId: vendorId,
        claimPeriodStart: claimPeriod.StartDate,
        claimPeriodEnd: claimPeriod.EndDate,
        rateGroupCategory: 'DeliveryGroup',
    };

    processorClaimService.getProcessorTopInfoPickupRateGroupDetail(params).then(function (result) {
        $scope.vm = result;
        console.log($scope.vm.data);
        $scope.vm.isView = true;
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };
}]);
