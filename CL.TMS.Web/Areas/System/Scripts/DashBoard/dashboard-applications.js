﻿function redrawInit(options) {

    $('#assgnToMeApplicationID').val(options.assignToMeId);

    $('#btnAssignToMe').data('assignuser', options.curentUserId)
    $('#btnAssignToMe').data('assgnmeurl', options.assignToMeUrl)

    $('#assignCompanyName3').text(options.assignToMeCompanyName);

    $('#BtnAssignToMeOK').data('assignuser', options.curentUserId)

    return {
        assignToMeUrl: options.assignToMeUrl,
        assignToMeId: options.assignToMeId,
        assignToMeCompanyName: options.assignToMeCompanyName,
        curentUserId: options.curentUserId,
    }
}

$(function () {
    _PageSize = 7; //OTSTM2-982
    _txtSearchAppId = 'SearchApplications';//id of search textbox
    _txtSearchRegisId = 'SearchRegistrant';
    _exportAppId = 'exportApplicationToExcel'; //id of export
    _exportTransId = 'exportRegistrantToExcel';
    _modalId = 'myModal';
    _pageX = 0;
    _pageY = 0;
    _cancelMouseClick = false;
    currentRowData = {};

    //OTSTM2-982
    $('#tblApplicationlist tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchText' + idText + '"/>');
    });

    //OTSTM2-931
    $('#tblRegistrantlist tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchTextRg' + idText + '"/>');
    });

    if (localStorage.getItem("error") === null) {
        $('#ajaxstatus').text('');
    } else {
        $('#ajaxstatus').text(localStorage.getItem("error"));
    }

    var tableApplication = $('#tblApplicationlist').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: "GetListOfApplications",
        deferRender: true,
        dom: "rtiS",
        scrollY: 260,
        scrollX: true,
        "sScrollX": "100%",
        "sScrollXInner": "110%",
        scrollCollapse: true,
        searching: true,
        ordering: true,
        order: [[4, "asc"]],
        "columns": [
             {
                 sName: "ApplicationSubmitDate",
                 width: "12%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return DatetimeRender(data.ApplicationSubmitDate, data.Activated, true);
                 }
             },
             {
                 sName: "ParticipantType",
                 data: null,
                 width: "8%",
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.ParticipantType, data.Activated);
                 }
             },
             {
                 sName: "Email",
                 data: null,
                 width: "17%",
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Email);
                 }
             },
             {
                 sName: "BusinessName",
                 width: "15%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.BusinessName, data.Activated);
                 }
             },
            {
                sName: "ContactName",
                width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return GeneralColumnRender(data.ContactName, data.Activated);
                }

            },
             {
                 className: "td-center",
                 sName: "Status",
                 width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return ApplicationStatusRender(data.Status, data.Activated);
                 }
             },
            {
                sName: "assignedToName",
                width: "12%",
                data: null,
                render: function (data, type, full, meta) {
                    return GeneralColumnRender(data.assignedToName, data.Activated);
                }
            },
            {
                width: "11%",
                className: "td-center",
                data: null,
                orderable: false,
                render: function (data, type, full, meta) {
                    return ApplicationActionRender(data.Status, data.ID, data.TokenID, data.Activated, data.ParticipantType, meta);
                }
            },
            {
                visible: false,
                data: null,
                render: function (data, type, full, meta) {
                    return GeneralColumnRender(data.ID, data.Activated);
                }
            },
             {
                 visible: false,
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.TokenID, data.Activated);
                 }
             },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');

            //OTSTM2-982
            tableApplication.columns().eq(0).each(function (index) {
                var column = tableApplication.column(index);
                var that = column;
                $('input', column.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#txtFoundApplications').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= _PageSize) ? $('#ApplicationsViewMore').css('visibility', 'visible') : $('#ApplicationsViewMore').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#SearchApplications').val();

            //OTSTM2-982
            if ($("#ApplicationsViewMore").is(':visible')) {
                $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
            }
            else {
                if (settings.fnDisplayEnd() >= _PageSize) {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'scroll' });
                }
                else {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                }
            }

            if ($("#searchTextDate").val() || $("#searchTextType").val() || $("#searchTextEmailAddress").val() || $("#searchTextCompany").val() || $("#searchTextContact").val()
                || $("#searchTextStatus").val() || $("#searchTextAssignedTo").val() || $("#SearchApplications").val()) {

                $("#txtFoundApplications").css('display', 'block');
            }
            else {
                $("#txtFoundApplications").css('display', 'none');
            }

            var url = Global.DashBoard.ApplicationExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)
                .replace('-st-date', $("#searchTextDate").val()).replace('-st-type', $("#searchTextType").val()).replace('-st-emailaddress', $("#searchTextEmailAddress").val())
                .replace('-st-company', $("#searchTextCompany").val()).replace('-st-contact', $("#searchTextContact").val()).replace('-st-status', $("#searchTextStatus").val())
                .replace('-st-assignedto', $("#searchTextAssignedTo").val()); //OTSTM2-982

            $('#exportApplicationToExcel').attr('href', url);
        },
        scroller: {
            displayBuffer: 100,
            rowHeight: 40,
            serverWait: 100,
            loadingIndicator: false,
        },
        createdRow: function (row, data, index) {
            $(row).attr('bankinfo-id', data.BankInfoId);

            switch (data.ParticipantType) {
                case 'Steward':
                    $(row).addClass(Global.DashBoard.AccessPermission.StewardApplication);
                    break;
                case 'Collector':
                    $(row).addClass(Global.DashBoard.AccessPermission.CollectorApplication);
                    break;
                case 'Hauler':
                    $(row).addClass(Global.DashBoard.AccessPermission.HaulerApplication);
                    break;
                case 'Processor':
                    $(row).addClass(Global.DashBoard.AccessPermission.ProcessorApplication);
                    break;
                case 'RPM':
                    $(row).addClass(Global.DashBoard.AccessPermission.RPMApplication);
                    break;
                default:
                    break;
            }
        },
    });

    tableApplication.on("draw.dt", function () {

        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    tableApplication.on('error.dt', function (e, settings, techNote, message) {
        console.log('settings.jqXHR.statusText:', settings.jqXHR.statusText + '\r\nsettings.jqXHR.status', settings.jqXHR.status);
        window.location.href = "/System/Common/EmptyPage";
    });

    var tableRegistrant = $('#tblRegistrantlist').DataTable({
        paging: true,
        info: false,
        processing: true,
        serverSide: true,
        ajax: "GetListOfRegistrants",
        deferRender: true,
        dom: "rtiS",
        scrollY: 260,
        scrollX: true,
        "sScrollX": "100%",
        "sScrollXInner": "110%",
        scrollCollapse: true,
        searching: true,
        ordering: true,
        order: [[0, "desc"]],
        autoWidth: true,
        "columns": [
             {
                 sName: "Number",
                 width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return RegNumberColumnRender(data.Number, data.Activated);
                 }
             },
             {
                 sName: "BusinessName",
                 width: "23%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.BusinessName, data.Activated);
                 }
             },
             {
                 sName: "Contact",
                 width: "22%",
                 data: null,
                 render: function (data, type, full, meta) {
                     if (data.ContactString !== null) {

                         return GeneralColumnRender(data.Contact, data.Activated);
                     } else {
                         return GeneralColumnRender("Missing Addr Info", data.Activated);
                     }
                 }

             },
             {
                 sName: "CreatedDate",
                 data: null,
                 width: "13%",
                 render: function (data, type, full, meta) {
                     return DatetimeRender(data.CreatedDate, data.Activated, false);
                 }

             },
             {
                 width: "10%",
                 className: "td-center",
                 sName: "Status",
                 data: null,
                 render: function (data, type, full, meta) {
                     return RegistrangStatusRender(data.Status, data.Activated);
                 }
             },
             {
                 width: "10%",
                 className: "td-center",
                 sName: "BankInfoReviewStatus",
                 data: null,
                 render: function (data, type, full, meta) {
                     if (data.BankInfoReviewStatus !== null)
                         return BankInfoStatusRender(data.BankInfoReviewStatus, data.Activated);
                     else
                         return '';
                 }
             },
             {
                 width: "12%",
                 className: "td-center",
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return BankActionRender(data.BankInfoReviewStatus, data.ID, data.TokenID, data.ParticipantType, meta);
                 }
             },
             {
                 sName: "BankInfoId",
                 visible: false,
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Contact, data.Activated);
                 }
             },
             {
                 sName: "TokenID",
                 visible: false,
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Contact, data.Activated);
                 }
             },
             {
                 sName: "ParticipantType",
                 visible: false,
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Contact, data.Activated);
                 }
             },
             {
                 sName: "VendorID",
                 visible: false,
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Contact, data.Activated);
                 }
             },

        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');

            //OTSTM2-931
            tableRegistrant.columns().eq(0).each(function (index) {
                var column = tableRegistrant.column(index);
                var that = column;
                $('input', column.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                                .search(this.value)
                                .draw();
                    }
                });
            });
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#txtFoundRegistrant').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= _PageSize) ? $('#RegistrantViewMore').css('visibility', 'visible') : $('#RegistrantViewMore').css('visibility', 'hidden'); //OTSTM2-931
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#SearchRegistrant').val();

            //OTSTM2-931
            if ($("#RegistrantViewMore").is(':visible')) {
                $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
            }
            else {
                if (settings.fnDisplayEnd() >= _PageSize) {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'scroll' });
                }
                else {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                }
            }

            if ($("#searchTextRgReg").val() || $("#searchTextRgCompany").val() || $("#searchTextRgContact").val() || $("#searchTextRgApprovedDate").val() || $("#searchTextRgStatus").val()
                || $("#searchTextRgBankStatus").val() || $('#SearchRegistrant').val()) {

                $("#txtFoundRegistrant").css('display', 'block');
            }
            else {
                $("#txtFoundRegistrant").css('display', 'none');
            }

            var url = Global.DashBoard.RegistrantExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)
            .replace('-st-reg', $("#searchTextRgReg").val()).replace('-st-company', $("#searchTextRgCompany").val()).replace('-st-contact', $("#searchTextRgContact").val())
            .replace('-st-approveddate', $("#searchTextRgApprovedDate").val()).replace('-st-status', $("#searchTextRgStatus").val()).replace('-st-bankstatus', $("#searchTextRgBankStatus").val()); //OTSTM2-931
            $('#exportRegistrantToExcel').attr('href', url);
        },
        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: true
        },
        createdRow: function (row, data, index) {
            $(row).attr('bankinfo-id', data.BankInfoId);
        },
    });

    tableRegistrant.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    tableRegistrant.on('error.dt', function (e, settings, techNote, message) {
        console.log('settings.jqXHR.statusText:', settings.jqXHR.statusText + '\r\nsettings.jqXHR.status', settings.jqXHR.status);
        window.location.href = "/System/Common/EmptyPage";
    });

    $(document).on('click', '.load-bankdetail', function () {
        var scope = angular.element(document.getElementById("DashboardRegistrant")).scope();
        var rowId = this.attributes['data-rowId'].value;
        var isview = this.attributes['data-isview'].value;
        var rowData = tableRegistrant.row(rowId).data();
        scope.rowData = rowData;
        console.log(isview);
        //OTSTM2-573
        scope.table = {
            registrantTable: tableRegistrant,
            searchVal: $('#SearchRegistrant').val()
        }
       
        if (isview == 'true') {
            scope.viewBankDetail(rowData.BankInfoId);
        } else {
            scope.openBankDetail(rowData.BankInfoId); 
        }
    });

    $('#tblApplicationlist tbody').on('click', 'tr', function (e) {
        if (_cancelMouseClick) {
            _cancelMouseClick = false;
            return;
        } var data = tableApplication.row(this).data();
        var nodeName = e.target.nodeName;
        if ((nodeName == "I") || ((nodeName == "A"))) {
            $('.assignCompanyName').text(data.Email);
            $('.unAssignUserName').text(data.assignedToName);

            if ((!!data.TokenID) && (!!data.ID)) {
            };
            $('#selectedName').val(data.BusinessName);
            $('#selectedApplicationToken').val(data.TokenID);
            $('#selectedApplicationID').val(data.ID);
            $('#assignActionURL').val("/" + data.ParticipantType + "/Registration/ChangeStatus");
            $('#assignVendorType').val(data.ParticipantType);
            if (this.className.includes('NoAccess')) {
                //console.log('NoAccess to this Application type');
            }

            return;//and open modal dialog
        };
        if ((!!data.TokenID) && (!!data.ID)) {
            switch (data.Status.toLowerCase()) {
                case "submitted":
                case "under review":
                case "assigned":
                    break;
                default:
                    break;
            }
        } else {
            alert("empty GUID/ID");
        };

        if ((!!data.TokenID) && (!!data.ID)) {

            switch (data.ParticipantType) {
                case "Collector":
                case "Hauler":
                case "Steward":
                case "Processor":
                case "RPM":
                    var url = "/" + data.ParticipantType + "/Registration/index/" + data.TokenID;
                    //window.open(url, "_self");
                    window.open(url);
                    break;
                default:
                    alert("Participant type [" + data.ParticipantType + "] is not implemented yet.");
                    break;
            }
        } else {
            alert("empty GUID/ID");
        }
    });

    $('#tblRegistrantlist tbody').on('click', 'tr', function (e) {
        if (_cancelMouseClick) {
            _cancelMouseClick = false;
            return;
        } var data = tableRegistrant.row(this).data();
        currentRowData = data;
        var nodeName = e.target.nodeName;

        if ((nodeName == "I") || ((nodeName == "A"))) {
            $('.assignCompanyName').text(data.BusinessName);
            if ((!!data.TokenID) && (!!data.ID)) {
            };
            $('#selectedName').val(data.BusinessName);
            $('#selectedApplicationToken').val(data.TokenID);
            $('#selectedApplicationID').val(data.ID);
            $('#assignActionURL').val("/" + data.ParticipantType + "/Registration/ChangeStatus");
            return;//and open modal dialog
        };

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tableRegistrant.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            //return;
        };

        var url = '';
        switch (data.ParticipantType) {
            case 1:
                url = Global.Settings.StewardApproveRedirectUrl.replace('111', data.VendorID);
                window.open(url);
                break;
            case 2:
                url = Global.Settings.CollectorApproveRedirectUrl.replace('111', data.VendorID);
                window.open(url);
                break;
            case 3:
                url = Global.Settings.HaulerApproveRedirectUrl.replace('111', data.VendorID);
                window.open(url);
                break;
            case 4:
                url = Global.Settings.ProcessorApproveRedirectUrl.replace('111', data.VendorID);
                window.open(url);
                break;
            case 5:
                url = Global.Settings.RPMApproveRedirectUrl.replace('111', data.VendorID);
                window.open(url);
                break;
            default:
                alert("Participant type [" + data.ParticipantType + "] is not implemented yet.");
                break;
        }
    });

    $(window).load(function () {
        //$("#tblRegistrantlist").parents(".dataTables_scrollBody").css("overflow", "hidden");
        //$("#tblApplicationlist").parents(".dataTables_scrollBody").css("overflow", "hidden");
        $("#tblRegistrantlist").parents(".dataTables_scrollBody").css("overflow-x", "auto");
        $("#tblApplicationlist").parents(".dataTables_scrollBody").css("overflow-x", "auto");
    });

    $("#RegistrantViewMore").on("click", function () {
        $("#tblRegistrantlist_wrapper").find(".dataTables_scrollBody").css("overflow", "auto").css("overflow-y", "auto");  //OTSTM2-949 "overflow-y: auto" for IE11
        $("#tblRegistrantlist_wrapper").find(".dataTables_scrollHead").removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $("#tblRegistrantlist_wrapper").find('.dataTables_scrollFoot').removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-931
        $(this).hide();
    });

    $("#ApplicationsViewMore").on("click", function () {
        $("#tblApplicationlist_wrapper").find(".dataTables_scrollBody").css("overflow", "auto").css("overflow-y", "auto");  //OTSTM2-949 "overflow-y: auto" for IE11
        $("#tblApplicationlist_wrapper").find(".dataTables_scrollHead").removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $("#tblApplicationlist_wrapper").find('.dataTables_scrollFoot').removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-982
        $(this).hide();
    });

    $("#exportRegistrantToExcel").on("click", function (e) {
        window.location.href = $(this).attr('href');
        e.stopPropagation();
    });

    $("#exportApplicationToExcel").on("click", function (e) {
        window.location.href = $(this).attr('href');
        e.stopPropagation();
    });

    //============= Assign To Me ================================
    function loadFirstApplication() {
        return $.ajax({
            url: '/System/Dashboard/GetFirstSubmitApplication',
            method: 'GET',
            dataType: "JSON"
        });
    }

    $('#modalAssingToMe').on('show.bs.modal', function () {

        var status = $('#BtnAssignToMeOK').attr('data-status');

        loadFirstApplication().done(function (data) {
            if (data) {

                var assignToMeBtnResult = {
                    assignToMeUrl: "/" + data.ParticipantType + "/Registration/ChangeStatus/",
                    assignToMeId: data.ID,
                    assignToMeCompanyName: data.BusinessName,
                    curentUserId: data.CurrentUser.Id,
                    assgnedUserName: data.CurrentUser.FullName,
                }

                $.ajax({
                    url: assignToMeBtnResult.assignToMeUrl,
                    method: 'POST',
                    dataType: "JSON",
                    data: {
                        applicationId: assignToMeBtnResult.assignToMeId,
                        status: status,
                        userID: assignToMeBtnResult.curentUserId,
                        assgnedUserName: assignToMeBtnResult.assgnedUserName,
                    },
                    success: function (result) {
                        $('#assignToMeTitle').html('Assigned');
                        $('#SuccessAssignToMe').show();
                        $('#NoDataassignToMe').hide();
                        $('#assignToMeCompanyName').text(assignToMeBtnResult.assignToMeCompanyName);
                    }
                });
            }
        }).fail(function () {
            $('#assignToMeTitle').html('Warning');
            $('#SuccessAssignToMe').hide();
            $('#NoDataassignToMe').show();
        })

    });

    $('#BtnAssignToMeOK').on("click", function () {
        tableApplication.search($('#SearchApplications').val()).draw(false);
    });
    //==================================================================

    //============= Assign/Re-Assign To User Un-Assign==================
    function RegistrationChangeStatus(currentData) {
        var url = $('#assignActionURL').val();
        var ApplicationID = $('#selectedApplicationID').val();
        var userID = 0;
        var userName;
        if ((typeof $(currentData).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(currentData).attr('data-assignuser'));
            userName = $(currentData).data('assignusername');
            if (currentData.id === 'Btn-Un-Assign') {
                userName = $('.unAssignUserName').text();
            }
            if (currentData.id === 'Btn-Re-Assign') {
                userName = $("#Btn-Re-Assign").attr("data-assignusername");
            }
        }
        var status = $(currentData).attr('data-status');

        $.ajax({
            url: url,
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: ApplicationID, status: status, userID: userID, assgnedUserName: userName },
            success: function (data) {
                tableApplication.search($('#SearchApplications').val()).draw(false);
            },
            failure: function (data) {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.status == 404) {
                }
            },
        });
    };

    $('#Btn-Un-Assign').on('click', function () {
        RegistrationChangeStatus(this);
    });

    $('#Btn-Re-Assign').on('click', function () {
        RegistrationChangeStatus(this);
    });

    $('#Btn-Assign').on('click', function () {
        RegistrationChangeStatus(this);
    });

    $('#Btn-Re-Send').on('click', function () {
        var url = ("/System/DashBoard/ResendAppInvitationEmail");

        $.ajax({
            url: url,
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#selectedApplicationID').val() },
            success: function (data) {
                tableApplication.search($('#SearchApplications').val()).draw(false);
            },
            failure: function (data) {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.status == 404) {
                }
            },
        });
    });
    //==================================================================

    function RegNumberColumnRender(iPadNumber, activated) {

        var span = ""
        if (activated) {
            span = "<span>" + iPadNumber + "</span>";
        }
        else {
            span = "<span>" + iPadNumber + "</span>";
        }
        return span;
    }

    function RegistrangStatusRender(StatusName, activated) {
        var span = ""
        if (("1" == StatusName.toLowerCase()) || ("active" == StatusName.toLowerCase())) {
            span = '<span>' + '<div class="panel-table-status color-tm-green-bg">Active</div>' + '</span>';
        }
        else {
            span = '<span>' + '<div class="panel-table-status color-tm-red-bg">Inactive</div>' + '</span>';
        }
        return span;
    }

    function BankInfoStatusRender(StatusName, activated) {
        var span = "";

        switch (StatusName) {
            case 'Submitted':
                span = '<span>' + '<div class="panel-table-status color-tm-orange-bg">' + StatusName + '</div>' + '</span>';
                break;
            case 'Open':
                span = '<span>' + '<div class="panel-table-status color-tm-blue-bg">' + StatusName + '</div>' + '</span>';
                break;
            case 'Approved':
                span = '<span>' + '<div class="panel-table-status color-tm-green-bg">' + StatusName + '</div>' + '</span>';
                break;
            default:
                return '';
        }
        return span;
    }

    function ApplicationStatusRender(StatusName, activated) {

        var span = ""
        if (StatusName == null) return span;
        switch (StatusName.toLowerCase()) {
            case "submitted":
                span = '<span>' + '<div class="panel-table-status color-tm-orange-bg">Submitted</div>' + '</span>';
                break;

            case "reassigntorep":
            case "onhold":
            case "offhold":
            case "assigned":
            case "under review":
                span = '<span>' + '<div class="panel-table-status color-tm-yellow-bg">Under Review</div>' + '</span>';
                break;
            case "open":
            case "backtoapplicant":
                span = '<span>' + '<div class="panel-table-status color-tm-blue-bg">Open</div>' + '</span>';
                break;
            case "rejected":
            case "denied":
                span = '<span>' + '<div class="panel-table-status color-tm-red-bg">Rejected</div>' + '</span>';
                break;
            case "approved":
            case "completed":
            case "bankinformationsubmitted":
            case "bankinformationapproved":
            case "resend":
                span = '<span>' + '<div class="panel-table-status color-tm-green-bg">Approve</div>' + '</span>';
                break;
            default:
                span = '<span>' + '<div class="panel-table-status color-tm-gray-bg">' + StatusName + '</div>' + '</span>';
                break;
                //default code block
        }
        return span;
    }

    function pad(num) {
        num = "0" + num;
        return num.slice(-2);
    }

    function DatetimeRender(value, activated, isApplication) {
        if (value == null) return "<span></span>";
        var str, year, month, day, hour, minute, d, finalDate;
        str = value.replace(/\D/g, "");
        if ('2208970800000' == str) {
            return '1900-1-1';
        }
        d = new Date(parseInt(str));

        year = d.getFullYear();
        month = pad(d.getMonth() + 1);
        day = pad(d.getDate());
        hour = pad(d.getHours());
        minutes = pad(d.getMinutes());
        var ampm = hour >= 12 ? 'PM' : 'AM';
        hour = hour % 12;
        hour = hour ? hour : 12;

        if (isApplication) {
            finalDate = year + "-" + month + "-" + day + ' ' + hour + ':' + minutes + ' ' + ampm;
        }
        else {
            finalDate = year + "-" + month + "-" + day;
        }
        var span = ""
        span = "<span>" + finalDate + "</span>";
        return span;
    }

    function GeneralColumnRender(Value, activated) {

        var span = "<span>"

        if (Value == "" || Value == null) {
            span = "<span style=\"color:#b1b1b1\">";
        }
        else {
            span = span + Value + "</span>";
        }

        return span;
    }

    function ApplicationActionRender(StatusName, ID, TokenID, isActivated, ParticipantType, meta) {
        var content = "";
        if (StatusName == null) return content;
        if (!Global.DashBoard.ViewOption.showApplicationActionGear) return content;
        switch (ParticipantType) {
            case 'Steward':
                if (!Global.DashBoard.UserApplicationWorkflow.Steward.ActionGear) return content;
                break;
            case 'Collector':
                if (!Global.DashBoard.UserApplicationWorkflow.Collector.ActionGear) return content;
                break;
            case 'Hauler':
                if (!Global.DashBoard.UserApplicationWorkflow.Hauler.ActionGear) return content;
                break;
            case 'Processor':
                if (!Global.DashBoard.UserApplicationWorkflow.Processor.ActionGear) return content;
                break;
            case 'RPM':
                if (!Global.DashBoard.UserApplicationWorkflow.RPM.ActionGear) return content;
                break;
            default:
                break;
        }
        switch (StatusName.toLowerCase()) {
            case "submitted":
                content = '<div class="btn-group dropdown-menu-actions">' +
                '<a href="javascript:void(0)" data-trigger="focus" tabindex="' + ID + '" data-toggle="popover" id="actions"' + meta.row + '" data-original-title="" title="">' +
                '<i class="glyphicon glyphicon-cog"></i></a>' +
                '<div class="popover-menu"> <ul> <li><a data-target="#modalAssign" data-toggle="modal" href="#"><i class="fa fa-user"></i>' +
                ' Assign' +
                '</a></li></ul></div></div>';
                break;
            case "reassigntorep":
            case "onhold":
            case "offhold":
            case "under review":
            case "assigned":
                content = '<div class="btn-group dropdown-menu-actions">' +
                '<a href="javascript:void(0)" data-trigger="focus" tabindex="' + ID + '" data-toggle="popover" id="actions"' + meta.row + '" data-original-title="" title="">' +
                '<i class="glyphicon glyphicon-cog"></i></a>' +
                '<div class="popover-menu"><ul><li><a data-target="#modalReAssign" data-toggle="modal" href="#"><i class="fa fa-user"></i> Re-Assign</a></li>' +
                '<li><a data-target="#modalUnAssign" data-toggle="modal" href="#"><i class="fa fa-minus-square-o"></i> Un-Assign</a></li>' +
                '</ul></div></div>';
                break;
            case "open":
            case "backtoapplicant":
                content = '<div class="btn-group dropdown-menu-actions">' +
                '<a href="javascript:void(0)" data-trigger="focus" tabindex="' + ID + '" data-toggle="popover" id="actions"' + meta.row + '" data-original-title="" title="">' +
                '<i class="glyphicon glyphicon-cog"></i></a>' +
                '<div class="popover-menu"> <ul> <li><a data-target="#modalResendInvitation" data-toggle="modal" href="#"><i class="fa fa-envelope"></i>' +
                ' Re-send Application Invite' +
                '</a></li></ul></div></div>';
                break;
            case "rejected":
            case "denied":
                break;
            case "approved":
            case "bankinformationsubmitted":
            case "bankinformationapproved":
            case "resend":
            case "completed":
                break;
            default:
                break;
        }
        return content;
    }

    function BankActionRender(StatusName, ID, TokenID, ParticipantType, meta) {
        if (!Global.DashBoard.ViewOption.bShowBankAction) return "";
        switch (ParticipantType) {
            case 1://'Steward':
                if (Global.DashBoard.AccessPermission.StewardApplication == 'NoAccess') {
                    return "";
                }
                break;
            case 2://'Collector':
                if (Global.DashBoard.AccessPermission.CollectorApplication == 'NoAccess') {
                    return "";
                }
                break;
            case 3://'Hauler':
                if (Global.DashBoard.AccessPermission.HaulerApplication == 'NoAccess') {
                    return "";
                }
                break;
            case 4://'Processor':
                if (Global.DashBoard.AccessPermission.ProcessorApplication == 'NoAccess') {
                    return "";
                }
                break;
            case 5://'RPM':
                if (Global.DashBoard.AccessPermission.RPMApplication == 'NoAccess') {
                    return "";
                }
                break;
            default:
                break;
        };

        var content = "";
        if (StatusName == null) return content;
        switch (StatusName) {
            case "Submitted":
                var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex='" + ID + "'" : "";
                content = '<div class="btn-group dropdown-menu-actions">' +
                    '<a href="javascript:void(0)" data-trigger="focus" tabindex="' + ID + '" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                    '<i class="glyphicon glyphicon-cog"></i></a>';
                if (Global.DashBoard.ViewOption.bEditBankinfo) {
                    content += '<div class="popover-menu"><ul><li><a class="load-bankdetail" href="#" data-isview="false" data-rowId="' + meta.row + '">';
                } else if (Global.DashBoard.ViewOption.viewBankinfo) {
                    content += '<div class="popover-menu"><ul><li><a class="load-bankdetail" href="#" data-isview="true" data-rowId="' + meta.row + '">';
                }
                content += '<i class="fa fa-pencil-square-o"></i> Review</a></li></ul></div></div>';
                break;
            case "Approved":
                content = '<div class="btn-group dropdown-menu-actions">' +
                    '<a href="javascript:void(0)" data-trigger="focus" tabindex="' + ID + '" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                    '<i class="glyphicon glyphicon-cog"></i></a>' +
                    '<div class="popover-menu"><ul>';
                if (Global.DashBoard.ViewOption.bEditBankinfo) {
                    content += '<li><a class="load-bankdetail" href="#" data-isview="true" data-rowId="' + meta.row + '"><i class="glyphicon fa fa-external-link" aria-hidden="true"></i>View</a></li>' +
                    '<li><a class="load-bankdetail" href="#" data-isview="false" data-rowId="' + meta.row + '"><i class="glyphicon fa fa-pencil-square-o"></i>Edit</a></li>';
                } else if (Global.DashBoard.ViewOption.viewBankinfo) {
                    content += '<li><a class="load-bankdetail" href="#" data-isview="true" data-rowId="' + meta.row + '"><i class="glyphicon fa fa-external-link" aria-hidden="true"></i>View</a></li>';
                }
                content += '</ul></div></div>';
                break;
            default:
                break;
        }
        return content;
    }

    $('#SearchRegistrant').on('keypress', function (e) {
        if (e.keyCode === 13) {
            var str = $(this).val();
            if (str.length >= 1) {
                $('#txtFoundRegistrant').siblings('.remove-icon').show();
                tableRegistrant.search(str).draw(false);
            }
            else {
                $('#txtFoundRegistrant').siblings('.remove-icon').hide();
                tableRegistrant.search(str).draw(false);
            }
        }
    });

    $('#SearchApplications').on('keyup', function (e) {
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36)) //End Home
        { return; }

        var str = $(this).val();
        if (str.length >= 1) {
            $('#txtFoundApplications').siblings('.remove-icon').show();
            tableApplication.search(str).draw(false);
        }
        else {
            $('#txtFoundApplications').siblings('.remove-icon').hide(); //OTSTM2-877, fix the jquery error of extra "sibling" when clearing search area
            tableApplication.search(str).draw(false);
        }
    });

    $('#glyphicon-search-App').on('click', function () {
        var searchValue = $('#SearchApplications').val();
        tableApplication.search(searchValue).draw();
    });

    $('#glyphicon-remove-App').on('click', function () {
        tableApplication.search("").draw();
    });

    $('#glyphicon-search-Reg').on('click', function () {
        var searchValue = $('#SearchRegistrant').val();
        tableRegistrant.search(searchValue).draw();
    });

    $('#glyphicon-remove-Reg').on('click', function () {
        tableRegistrant.search("").draw();
    });

    $.ajax({
        url: '/System/Common/GetUsersInAuditGroup',
        method: 'GET',
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, data) {
                $('.listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')//value:ID text:name text
            });
            $('.listOfStaff').prop('selectedIndex', 0);
        },
        failure: function (data) {
        },
    });

    $("#listOfStaffAssign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Assign").removeAttr("disabled");
            $("#Btn-Assign").attr("data-assignuser", selectedStaffID);
            $("#Btn-Assign").attr("data-assignusername", $("#listOfStaffAssign option:selected").text());
        }
        else {
            $("#Btn-Assign").attr("disabled", "disabled");
        }
    });

    $("#listOfStaffRe-Assign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Re-Assign").removeAttr("disabled");
            $("#Btn-Re-Assign").attr("data-assignuser", selectedStaffID);
            $("#Btn-Re-Assign").attr("data-assignusername", $("#listOfStaffRe-Assign option:selected").text());
        }
        else {
            $("#Btn-Re-Assign").attr("disabled", "disabled");
        }
    });

    $('#modalReAssign').on('shown.bs.modal', function () {
        loadUsers();
        var strUser = $("#listOfStaffRe-Assign option:selected").text() + ' ' + $("#listOfStaffRe-Assign option:selected").val();
        $("#Btn-Re-Assign").attr("data-assignuser", $("#listOfStaffRe-Assign option:selected").val());
        $("#Btn-Re-Assign").attr("data-assignusername", $("#listOfStaffRe-Assign option:selected").text());
    });

    $('#modalAssign').on('shown.bs.modal', function () {
        loadUsers();
        $("#Btn-Assign").attr("data-assignuser", $("#listOfStaffAssign option:selected").val());
        $("#Btn-Assign").attr("data-assignusername", $("#listOfStaffAssign option:selected").text());
    });

    $('table').on('mousedown', 'tr', function (e) {
        _pageX = e.pageX;
        _pageY = e.pageY;
    });

    $('table').on('mouseup', 'tr', function (e) {
        _pageX = e.pageX - _pageX;
        _pageY = e.pageY - _pageY;
        if ((Math.abs(_pageX) > 5) || (Math.abs(_pageY) > 5)) {
            _cancelMouseClick = true;
        }
    });

    function loadUsers() {
        $('.listOfStaff').html("");
        $.ajax({
            url: '/System/Common/GetUsersByApplicationsWorkflow',
            method: 'GET',
            dataType: "JSON",
            data: { accountName: $('#assignVendorType').val() },
            success: function (data) {
                $.each(data, function (index, data) {
                    $('.listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')//value:ID text:name text
                });
                $('.listOfStaff').prop('selectedIndex', 0);
                $("#Btn-Assign").attr("data-assignuser", $('#listOfStaffAssign').val());
                $("#Btn-Assign").attr("data-assignusername", $('#listOfStaffAssign option:selected').text());
                $("#Btn-Re-Assign").attr("data-assignuser", $('#listOfStaffAssign').val());
                $("#Btn-Re-Assign").attr("data-assignusername", $('#listOfStaffAssign option:selected').text());
            },
            failure: function (data) {
            },
        });

    }
})