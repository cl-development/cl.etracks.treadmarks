﻿'use strict';
var validMsg = {
    clean:"clean",
    isRequired: "Required",
    percentOver100: "The variance cannot be greater than 100%",
    percentZero: "The variance cannot be equal to 0%",
    invalidInput: "Invalid Input",
    collectorAllChk: "Expected Input: Approver 2 > Approver 1 > Automatic approval Threshold",
    collectorOnly1n2Chk: "Expected Input: Approver 2 > Approver 1",
    collector1nAutoChk: "Expected Input: Approver 1 > Automatic approval Threshold",
    collectorOnly2Chk: "Approver 1's check-box must be checked",
    noneMsg:""
}
/**********************
 * System Thresholds App
 **********************/
var systemThresholdsApp = angular.module("systemThresholdsApp",
    ["commonLib", "ui.bootstrap", "systemThresholdsApp.directives", "systemThresholdsApp.controllers", "systemThresholdsApp.services", "ngMessages"]);

/***********************
 * Controllers Section *
 ***********************/
var controllers = angular.module('systemThresholdsApp.controllers', []);
// main controllers
controllers.controller('accountThresholdsController', ['$rootScope', '$scope', '$http', '$uibModal', 'systemThresholdsAppServices', function ($rootScope, $scope, $http, $uibModal, systemThresholdsAppServices) {

    $scope.valueRequire = 'Required';
    $scope.valueMustEqualOrGreaterThan = validMsg.collectorOnly1n2Chk;
    $scope.customValidationRequireCB = validMsg.collectorOnly2Chk;
    $scope.CollectorvalueMustEqualOrGreaterThan = validMsg.noneMsg;
    $scope.valueBetweenZeroHundred = validMsg.noneMsg;

    $scope.vm = {};
    $scope.old = {};
    $scope.disableSaveBtn = false;

    systemThresholdsAppServices.getDetails().then(function (data) {
        $scope.vm = viewModel(data.data);
        $scope.old = viewModel(data.data);
        $scope.freshnote = true;
    });

    //base on security determining isReadonly or not.
    $scope.isView = Global.AccountThresholds.Security.AccountThresholds === Global.AccountThresholds.Security.Constants.ReadOnly;
   
    $scope.submitContent = function () {
        //valid check
        var msg = validCollectorCheck($scope.vm.items);
        if (msg != validMsg.noneMsg || $scope.disableSaveBtn || !(angular.equals({}, $scope.SysThresholdForm.$error))) {
            if (msg !== validMsg.collectorOnly2Chk) {
                $(".collector").children("span").html(msg);
            }
            $('html, body').animate({
                scrollTop: $("#fixedClaimHeader").offset().top-680
            }, 500);
            return;
        };
        
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                header: function () {
                    return Global.AccountThresholds.ConfirmHeader;
                },
                confirmMessage: function () {
                    return Global.AccountThresholds.ConfirmHtml.replace("AAA", $scope.vm.applicationName);
                }
            }
        });

        confirmModal.result.then(function (confirm) { //confirm  
            var data = postModel($scope.vm);
            systemThresholdsAppServices.saveDetails(data).then(function (response) {
                if (response && response.data && response.data.status) {
                    $scope.freshnote = true;
                    $scope.old = viewModel(response.data.data);
                    msg = validMsg.noneMsg;
                } else {
                    var failModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'failCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () {
                                return Global.AccountThresholds.FailHeader;
                            },
                            failResult: function () {
                                return response.data.status ? Global.AccountThresholds.FailHtml : response.data.statusMsg;
                            }
                        }
                    });
                }
            });
        }, function (cancel) {
            console.log('non-save');
        });
    }

    // internal notes  
    $scope.freshnote = false;
    $scope.addUrl = Global.AccountThresholds.AddNoteUrl;
    $scope.loadUrl = Global.AccountThresholds.LoadNotesUrl;
    $scope.exportUrl = Global.AccountThresholds.ExportNotesUrl;

    $scope.cancelBtn = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                header: function () {
                    return Global.AccountThresholds.CancelHeader;
                },
                confirmMessage: function () {
                    return Global.AccountThresholds.CancelHtml;
                }
            }
        });
        cancelModal.result.then(function (cancel) {
            systemThresholdsAppServices.getDetails().then(function (data) {
                $scope.vm = viewModel(data.data);
                $scope.freshnote = true;
                $scope.disableSaveBtn = showValidinfo(validMsg.clean, validMsg.noneMsg);
            });           
        }, function (uncancel) {
            $scope.disableSaveBtn = showValidinfo(validMsg.clean, validMsg.noneMsg);
        });
    }

    $scope.isChecked = function (item, isChecked) {
        if (!isChecked) {
            $scope.vm.items[item].value = $scope.old.items[item].value;           
        } 
        showValidinfo(validMsg.clean, validMsg.noneMsg);
    }

    //for popover
    var collectorClaimSetting = {
        content: Global.Settings.collectorClaimsContent
    };

    $('.fa-question-circle').webuiPopover('destroy')
        .webuiPopover($.extend({}, Global.Settings.popover, collectorClaimSetting));

    $scope.valueMustEqualOrGreater = function (a, b, filedName1, filedName2, ruleName, isRuleEnabled) {
        if (typeof (a) === 'undefined' || typeof (b) === 'undefined') {
            return;
        }

        if (typeof (isRuleEnabled) === 'undefined' || !isRuleEnabled) {
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, true);
            }
            if ($scope.SysThresholdForm[filedName2]) {
                $scope.SysThresholdForm[filedName2].$setValidity(ruleName, true);
            }
            return true;
        }

        if ($scope.SysThresholdForm[filedName1]) {
            //$scope.SysThresholdForm[filedName1].$setDirty();
        }
        if ($scope.SysThresholdForm[filedName2]) {
            //$scope.SysThresholdForm[filedName2].$setDirty();
        }
        if (parseInt(b.value) <= parseInt(a.value)) {//set error:
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, false);
                }
            if ($scope.SysThresholdForm[filedName2]) {
                $scope.SysThresholdForm[filedName2].$setValidity(ruleName, false);
                }
        } else {
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, true);
            }
            if ($scope.SysThresholdForm[filedName2]) {
                $scope.SysThresholdForm[filedName2].$setValidity(ruleName, true);
            }
        }
    };
    
    $scope.RequireCB = function (a,b, filedName1, filedName2, ruleName) {
        if (typeof (a) === 'undefined' || typeof (b) === 'undefined' || typeof (a.$viewValue) === 'undefined' || typeof (b.$viewValue) === 'undefined') {
            return;
        }
        if (!a.$viewValue && b.$viewValue) {//set validation error:
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, false);
            }
            if ($scope.SysThresholdForm[filedName2]) {
                $scope.SysThresholdForm[filedName2].$setValidity(ruleName, false);
            }
            return false;

        } else {//clear validation error:
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, true);
            }
            if ($scope.SysThresholdForm[filedName2]) {
                $scope.SysThresholdForm[filedName2].$setValidity(ruleName, true);
            }
            return true;
        }
    };

    $scope.CollectorCustomValidationEqualOrGreater = function (cbApprover2, valApprover2, cbApprover1, valApprover1, cbAutoApprove, valAutoApprove) {
        if (typeof (cbAutoApprove) === 'undefined' || typeof (valAutoApprove) === 'undefined' || typeof (cbApprover1) === 'undefined' || typeof (valApprover1) === 'undefined' || typeof (cbApprover2) === 'undefined' || typeof (valApprover2) === 'undefined'
            || typeof (cbAutoApprove.$modelValue) === 'undefined' || typeof (valAutoApprove.$modelValue) === 'undefined' || typeof (cbApprover1.$modelValue) === 'undefined' || typeof (valApprover1.$modelValue) === 'undefined' || typeof (cbApprover2.$modelValue) === 'undefined' || typeof (valApprover2.$modelValue) === 'undefined'
            || isNaN(valAutoApprove.$modelValue) || isNaN(valApprover1.$modelValue) || isNaN(valApprover2.$modelValue)) {
            return;
        }
        $scope.CollectorvalueMustEqualOrGreaterThan == validMsg.noneMsg;
        if (cbAutoApprove.$modelValue && cbApprover1.$modelValue && cbApprover2.$modelValue) {
            $scope.CollectorvalueMustEqualOrGreaterThan = parseInt(valApprover2.$modelValue) > parseInt(valApprover1.$modelValue) &&
                parseInt(valApprover1.$modelValue) > parseInt(valAutoApprove.$modelValue) ? validMsg.noneMsg : validMsg.collectorAllChk;
        } else if (cbApprover1.$modelValue && cbApprover2.$modelValue) {
            $scope.CollectorvalueMustEqualOrGreaterThan = parseInt(valApprover2.$modelValue) > parseInt(valApprover1.$modelValue) ? validMsg.noneMsg : validMsg.collectorOnly1n2Chk;
        } else if (cbAutoApprove.$modelValue && cbApprover1.$modelValue) {
            $scope.CollectorvalueMustEqualOrGreaterThan = parseInt(valApprover1.$modelValue) > parseInt(valAutoApprove.$modelValue) ? validMsg.noneMsg : validMsg.collector1nAutoChk;
        } else if (cbApprover2.$modelValue) {
            //$scope.CollectorvalueMustEqualOrGreaterThan = validMsg.collectorOnly2Chk;
            //this rule check already applied in RequireCB

            if (!cbApprover1.$modelValue) {
                $scope.CollectorvalueMustEqualOrGreaterThan = validMsg.noneMsg;
            }
        } else if (cbApprover1.$modelValue) {//only cbApprover1 is checked, this is a legal status
            $scope.CollectorvalueMustEqualOrGreaterThan = validMsg.noneMsg;
        }
        if ($scope.CollectorvalueMustEqualOrGreaterThan != validMsg.noneMsg) {
            switch ($scope.CollectorvalueMustEqualOrGreaterThan) {
                case validMsg.collectorAllChk:
                    $scope.SysThresholdForm.CollectorApprover1ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    $scope.SysThresholdForm.CollectorApprover2ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    $scope.SysThresholdForm.CollectorAutoApproveAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    break;
                case validMsg.collectorOnly1n2Chk:
                    $scope.SysThresholdForm.CollectorApprover1ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    $scope.SysThresholdForm.CollectorApprover2ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    break;
                case validMsg.collector1nAutoChk:
                    $scope.SysThresholdForm.CollectorApprover1ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    $scope.SysThresholdForm.CollectorAutoApproveAmount.$setValidity('CollectorCustomValidationEqualOrGreater', false);
                    break;
                default:
                    break;
            }
        }else
        {
            $scope.SysThresholdForm.CollectorApprover1ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', true);
            $scope.SysThresholdForm.CollectorApprover2ReqdAmount.$setValidity('CollectorCustomValidationEqualOrGreater', true);
            $scope.SysThresholdForm.CollectorAutoApproveAmount.$setValidity('CollectorCustomValidationEqualOrGreater', true);
        }
    };
    
    $scope.percentInteger = function (inputVal, filedName1, ruleName) {
        if (typeof (inputVal) === 'undefined') {
            return;
        }
        if (inputVal == 0) {//set validation error:
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, false);
                $scope.valueBetweenZeroHundred = validMsg.percentZero;
            }
        } else if (inputVal > 100) {//clear validation error:
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, false);
                $scope.valueBetweenZeroHundred = validMsg.percentOver100;
            }
        } else {
            if ($scope.SysThresholdForm[filedName1]) {
                $scope.SysThresholdForm[filedName1].$setValidity(ruleName, true);
                $scope.valueBetweenZeroHundred = validMsg.noneMsg;
            }
        }
    };

    }]);

//Transaction Thresholds Controller
controllers.controller('transactionThresholdController', ['$rootScope', '$scope', '$http', '$uibModal', 'systemThresholdsAppServices', '$window', function ($rootScope, $scope, $http, $uibModal, systemThresholdsAppServices, $window) {

    //base on security determining isReadonly or not.
    $scope.isView = Global.TransactionThresholds.Security.transactionThresholds === Global.TransactionThresholds.Security.Constants.ReadOnly;

    $scope.transactionThresholdsValidityOutput = false;

    $scope.submitContent = function () {
        transactionThresholdsValidityCheck();
        if ($scope.transactionThresholdsValidityOutput) {
            $scope.transactionThresholdsValidityOutput = false;
            //alert('Validation error. One or more fields are not valid.');
            window.scrollTo(0, 0);
            return false;
        }
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmTransCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                confirmMessage: function () {
                    return Global.TransactionThresholds.ConfirmHtml;
                }
            }
        });

        confirmModal.result.then(function (e) {
            var vm = $scope.transactionThresholdsViewModel;
            systemThresholdsAppServices.updateTransactionThresholds(vm).then(function (response) {
                if (response.data.status) {
                    //$window.location.reload();
                    $scope.transactionThresholdsViewModelOriginal = response.data.data;
                }
                else {
                    transactionThresholdsValidityCheck();
                    console.log(response.data.statusMsg);
                    alert(response.data.statusMsg);
                }
            });
        });
    }

    $scope.cancelChange = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'cancel-modal.html',
            controller: 'cancelCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                cancelMessage: function () {
                    return Global.TransactionThresholds.CancelHtml;
                }
            }
        });

        cancelModal.result.then(function (e) {
            systemThresholdsAppServices.loadTransactionThresholds().then(function(data) {
                $scope.transactionThresholdsViewModel = data.data;              
            });
        });
    }  

    function transactionThresholdsValidityCheck() {
        if ($scope.transactionThresholdsViewModel.Threshold_CBIndividualTireCount) {
            if (parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountPLT) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMT) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountAGLS) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountIND) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountSOTR) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMOTR) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountLOTR) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountGOTR) >= 0) {
                    $scope.disableIndividualErrorMessage = false;
            }
            else {
                    $scope.disableIndividualErrorMessage = true;
    }
        }

        if ($scope.transactionThresholdsViewModel.Threshold_CBTotalTireCount) {
            if ($scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOT
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountHIT || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountPTR
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountRTR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountSTC
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountTCR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountUCR) {
                $scope.disableTotalErrorMessage = false;
            }
            else {
                $scope.disableTotalErrorMessage = true;
            }
        }

        if ($scope.disableIndividualErrorMessage || $scope.disableTotalErrorMessage) {
            $scope.transactionThresholdsValidityOutput = true;
        }       
    }    
}]);

//common Controllers
controllers.controller('successCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'systemThresholdsAppServices', 'successResult', 'header', function ($scope, $uibModal, $uibModalInstance, systemThresholdsAppServices, successResult, header) {
    $scope.sucessMessage = successResult;
    $scope.successHeader = header;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.$root.freshnote = true;
    };
}]);
controllers.controller('failCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', function ($scope, $uibModal, $uibModalInstance, failResult, header) {
    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('confirmCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'header', 'confirmMessage', function ($scope, $uibModal, $uibModalInstance,header, confirmMessage) {
    $scope.confirmMessage = confirmMessage;
    $scope.confirmHeader = header;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
    };
}]);
controllers.controller('cancelCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'cancelMessage', '$window', function ($scope, $uibModal, $uibModalInstance, cancelMessage, $window) {
    $scope.cancelMessage = cancelMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
        //$window.location.reload();
    };
}]);
controllers.controller('confirmTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'confirmMessage', function ($scope, $uibModal, $uibModalInstance, confirmMessage) {
    $scope.confirmMessage = confirmMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
    };
}]);
/********************
 * Services Section
 ********************/
var services = angular.module('systemThresholdsApp.services', []);
services.factory('systemThresholdsAppServices', ["$http", function ($http) {
    var factory = {};

    factory.saveDetails = function (data) {
        var submitVal = {
            url: Global.AccountThresholds.SaveDetailsUrl,
            method: "POST",
            data: JSON.stringify({vm: data })
        }
        return $http(submitVal);
    }

    factory.getDetails = function () {
        return $http({
            method: 'GET',
            url: Global.AccountThresholds.LoadDetailsUrl,
            params: {         
            }
        });
    }

    factory.loadNotes = function (applicationTypeID) {
        return $http({
            method: 'GET',
            url: Global.AccountThresholds.LoadNotesUrl,
            params: {
                applicationTypeID: applicationTypeID,
            }
        });
    }

    factory.loadTransactionThresholds = function () {
        return $http({
            method: 'GET',
            url: Global.TransactionThresholds.LoadTransactionThresholdsUrl,
            params: {}
        });
    }

    factory.updateTransactionThresholds = function (vm) {
        var submitVal = {
            url: Global.TransactionThresholds.UpdateTransactionThresholdsUrl,
            method: "POST",
            data: {
                transactionThresholdsVM: vm
            }
        }
        return $http(submitVal);
    }
    return factory;
}]);
/**
 * View Model
 */
function viewModel(datas) {
    var result = {
        items: {},
        decimailsize: datas.decimalsize,
        applicationTypeID: datas.applicationTypeID,
        note: datas.note,        
        notes: {}
    };
    $.each(datas.Items, function (index, data) {
        if (data instanceof Object) {
            var name = data.ItemName.split(".")[1] ? data.ItemName.split(".")[1] : data.ItemName.split(".")[0];
            var value = name.indexOf("CB") > -1 ? data.ItemValue == "1" : data.ItemValue;
            result.items[name] = {
                value: value,
                id: data.ItemID,
            };
        }
    });
    return result;
};

function postModel(datas) {
    var result = {
        Items: [],
        applicationTypeID: datas.applicationTypeID,
        note: datas.note
    };
    $.each(datas.items, function (name, data) {
        var value = data.value;
        if (name.indexOf("CB") > -1) {
            value= data.value ? "1" : "0";
        }
        var item = {
            ItemID:data.id,
            ItemName: name,
            ItemValue: value
        }      
        result.Items.push(item);
    });
    return result;
}

function validCollectorCheck(item) {
    var msg = validMsg.noneMsg;
    if (item.CBCollectorAutoApprove.value && item.CBCollectorApprover1Reqd.value && item.CBCollectorApprover2Reqd.value) {
        msg = parseInt(item.CollectorApprover2ReqdAmount.value) > parseInt(item.CollectorApprover1ReqdAmount.value) &&
            parseInt(item.CollectorApprover1ReqdAmount.value) > parseInt(item.CollectorAutoApproveAmount.value) ? validMsg.noneMsg : validMsg.collectorAllChk;
    } else if (item.CBCollectorApprover1Reqd.value && item.CBCollectorApprover2Reqd.value) {
        msg = parseInt(item.CollectorApprover2ReqdAmount.value) > parseInt(item.CollectorApprover1ReqdAmount.value) ? validMsg.noneMsg : validMsg.collectorOnly1n2Chk;
    } else if (item.CBCollectorAutoApprove.value && item.CBCollectorApprover1Reqd.value) {
        msg = parseInt(item.CollectorApprover1ReqdAmount.value) > parseInt(item.CollectorAutoApproveAmount.value) ? validMsg.noneMsg : validMsg.collector1nAutoChk;
    } else if (item.CBCollectorApprover2Reqd.value) {
        msg = validMsg.collectorOnly2Chk;
    }   
    return msg;
}

function showValidinfo(element, msg) {
    if (typeof element == 'string' && element == validMsg.clean) {
        $("div.valid-info").children("span").html(msg);
    } else {
        $(element).parents("div.form-inline").children("div.valid-info").children("span").html(msg);
    }
    if (msg !=validMsg.noneMsg){
        $('html, body').animate({
            scrollTop: $("#fixedClaimHeader").offset().top - 680
        }, 500);
    }
    return msg != validMsg.noneMsg;
}

/*********************
 * Directives Section
 *********************/
var directives = angular.module('systemThresholdsApp.directives', []);

directives.directive('validNumber', function () {
    return {
        require: '?ngModel',
        scope: {
            theprefex: '@',
            decimalSize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {
            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 12;
            var zero = "00000";
            var scale, prefex;        

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '0';
                }
                var clean = val.replace(/[^0-9]/g, '');//val.replace(/[^0-9\.]/g, ''); don't accept dot, since decimal is not allowed.
                var decimalCheck = clean.split('.');
                scale=scope.decimalSize ? parseInt(scope.decimalSize) : 0;
                prefex = scope.theprefex == "%" || scope.theprefex == "none" ? '' : '$';
                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(prefex + clean);
                    ngModelCtrl.$render();
                }
                return clean != "" ? prefex + clean : clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            }).bind('blur', function (e) {
                e.preventDefault();
                scale = scope.decimalSize ? parseInt(scope.decimalSize) : 0;
                scope.$parent.disableSaveBtn = showValidinfo(element, validMsg.noneMsg);
                var elementValue = element.val() ? element.val().split('$').length > 1 ? element.val().split('$')[1] : element.val().split('$')[0] : '';
                if (elementValue == '' || elementValue == '-' || elementValue.indexOf('-') > -1) {                 
                    //scope.$parent.disableSaveBtn = showValidinfo(element, validMsg.isRequired);
                } else {
                    var splitValue = elementValue.split('.');
                    if (splitValue.length > 1 && splitValue[1].length > scale) {
                        elementValue = splitValue[0] + '.' + splitValue[1].substr(0, scale);
                    }
                    var fixedValue = parseFloat(elementValue).toFixed(scale).toString();
                    if (scope.theprefex) {
                        //if (scope.theprefex == "%" ) {
                        //    if (parseFloat(elementValue) > 100) {
                        //        scope.$parent.disableSaveBtn = showValidinfo(element, validMsg.percentOver100);
                        //    } else if (parseFloat(elementValue) == 0) {
                        //        scope.$parent.disableSaveBtn = showValidinfo(element, validMsg.percentZero);
                        //    }
                        //}
                    } else {
                        fixedValue = prefex + fixedValue;
                    }
                    ngModelCtrl.$setViewValue(fixedValue);
                    //check collector
                    //var msg = validCollectorCheck(scope.$parent.vm.items);                  
                    //if (msg !== validMsg.collectorOnly2Chk) {
                    //    $(".collector").children("span").html(msg);
                    //}
                }
                ngModelCtrl.$render();
                scope.$apply();               
            });
        }
    }
});

directives.directive('validCheckbox', function () {
    return {
        require: '?ngModel',
        scope: {},
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('change', function (e) {
                e.preventDefault();
                //var filteredNames = Object.keys(scope.$parent.vm.items).filter((name) => /Collector/.test(name));
                var msg = validCollectorCheck(scope.$parent.vm.items);
                if (msg != validMsg.noneMsg) {
                    $(".collector").children("span").html(msg);
                };
            });
        }
    }
});


directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

directives.directive('tablerowpopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

directives.directive('internalNotes', ['$compile', '$window', '$timeout', '$http', '$filter', function ($compile, $window, $timeout, $http, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@',
            applicationTypeId: '@',
            isView: '@',
            reloaddata: '@',
            exportName:'@'
        },
        link: function (scope, elem, attr, ngModel) {
            scope.noteTDWidthpixel = 0;
            scope.elem = elem;
            elem.find("textarea").prop('disabled', scope.isView == "true");
            elem.find("button").prop('disabled', scope.isView == "true");

            scope.focus = function () {
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            //internal notes search
            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                    scope.allInternalNotes = $filter('filter')(scope.$parent.vm.notes, scope.searchNote);
                    scope.internalNoteLength = scope.allInternalNotes.length;
                } else {
                    scope.allInternalNotes = scope.$parent.vm.notes;
                }
            });

            //view more
            scope.viewMoreNotesClick = function ($event) {
                scope.quantity = scope.allInternalNotes.length;
                scope.viewMore = false;
                scope.scroller = true;
                $("#internalNoteBody").height("175");
                $(".spaceRow").show();
            }

            //internal notes remove icon
            scope.removeIcon = function () {
                scope.searchNote = '';
                scope.removeIconStyle = { 'display': 'none' };
                scope.foundStyle = { 'display': 'none' };
                scope.searchStyle = '';
            }
            //internal notes sorting
            scope.noteSorting = function (sortType) {
                scope.sortType = sortType;
                scope.sortReverse = !scope.sortReverse;
                scope.direction = scope.sortReverse ? "-" + sortType : "+" + sortType;
                scope.allInternalNotes = $filter('orderBy')(scope.allInternalNotes, scope.direction);
            };
            scope.add = function () {
                if (scope.textAreaInput) {
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            applicationTypeID: scope.applicationTypeId,
                            notes: scope.textAreaInput
                        })
                    }).success(function (result) {
                        $http({
                            url: scope.loadUrl,
                            method: "GET",
                            params: {
                                applicationTypeID: scope.applicationTypeId,
                            },
                        }).success(function (result) {
                            scope.textAreaInput = '';
                            scope.allInternalNotes = result;
                        })
                    })
                }
            };

            scope.stripNotes = function (note) {
                var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one //.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                var trimWidth = 0;
                var bestFitNbr = 50;
                var shortNote = '';
                if (noteWidth + 15 >= scope.noteTDWidthpixel) {
                    while (trimWidth < scope.noteTDWidthpixel - 15) {
                        bestFitNbr += 2;
                        shortNote = noteTrimedStr.substr(0, bestFitNbr);//incrase str size and try again
                        trimWidth = getTextWidth(shortNote, "0.875em Open Sans");
                    }
                    return shortNote + "...";
                }
                else {
                    return note;
                }
            };

            scope.checkNoteSize = function (note) {
                if (note != undefined) {
                    var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    var tdAddedBy = document.getElementById('tdAddedBy');
                    if (hasScrollBar(internalNoteBody)) {
                        $(tdAddedBy).css("margin-left", "-20px");
                        return (scope.noteTDWidthpixel <= noteWidth + 32);
                    }
                    else {
                        return (scope.noteTDWidthpixel <= noteWidth + 6);
                    }
                }
                else {
                    return false;
                }
            };

            function clickInsidePopover(e) {
                if ((e == null) || (e == undefined)) {
                    return false;
                } else if ((e.className === 'popover-content') || (e.className === 'popover-title') || (e.className.startsWith('popover fade'))) {
                    return true;
                }
                else {
                    return clickInsidePopover(e.parentElement);
                }
            }

            function getTextWidth(text, font) {
                var canvas = document.getElementById('noteCanvas');
                var context = canvas.getContext("2d");
                context.font = font;
                var metrics = context.measureText(text);
                return Math.round(metrics.width);
            }

            //OTSTM2-845 check if vertical scrollbar appears
            function hasScrollBar(obj) {
                return $(obj).get(0).scrollHeight > $(obj).height();
            }

            angular.element($window).bind('resize', function () {
                var length = scope.allInternalNotes ? scope.allInternalNotes.length : 0;
                for (var i = 0; i < length; i++) {
                    if (!scope.allInternalNotes[i].Note) { continue };
                    var noteTrimedStr = scope.allInternalNotes[i].Note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    var trimWidth = 0;
                    var bestFitNbr = 50;
                    var shortNote = scope.stripNotes(scope.allInternalNotes[i].Note);
                    scope.noteTDWidthpixel = 0;
                    var $div = $("<td ng-if=\"checkNoteSize(note.Note)\" width=\"75%\" style=\"cursor: pointer\" class=\"internal-note{{$index}}\"><span internal-note-hover-popover data-note=\"{{note.Note}}\">"+shortNote+"</span></td>");
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    if (hasScrollBar(internalNoteBody)) {
                        if (scope.noteTDWidthpixel <= noteWidth + 32) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                scope.$apply();
                            }
                        }
                    }
                    else {
                        if (scope.noteTDWidthpixel <= noteWidth + 6) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                scope.$apply();
                            }
                        }
                    }
                }
                scope.$digest();
            });

            angular.element("#exportInternalNote").bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                var submitVal = {
                    url: scope.exportUrl,
                    method: "GET",
                    params: {
                        applicationTypeID: scope.applicationTypeId,
                        sortReverse: scope.sortReverse,
                        sortcolumn: scope.sortColumn,
                        searchText: scope.searchNote,
                    },
                }
                return $http(submitVal).then(function (result) {
                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = scope.exportName+"-Internal-Note-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = '<a title="Export to CSV" href="javascript:void(0);" style="display: none;">';
                        var body = $(document.body).append(anchor);
                        $(anchor).attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();
                        $(anchor).remove();
                    }
                });
            });

        },
        controller: function ($scope, $rootScope) {  
            $scope.$watch('reloaddata', function () {
                if ($scope.reloaddata == "true") {
                    $http({
                        url: $scope.loadUrl,
                        method: "GET",
                        params: {
                            applicationTypeID: $scope.applicationTypeId,
                        },
                    }).then(function (result) {
                        $scope.allInternalNotes = result.data;
                        $scope.$parent.vm.notes = result.data;
                        $scope.$parent.freshnote = false;
                        $scope.sortType = 'AddedOn';
                        $scope.textAreaInput = '';
                        $scope.sortReverse = true;
                        $scope.searchNote = '';
                        $scope.quantity = 5;
                        $scope.showMore = true;
                    });
                }
            });
            $scope.$watch('textAreaInput', function (text) {
                $scope.$parent.vm.note = text;
            });
        }
    };
}]);

directives.directive('internalNoteHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var header = '<span><strong>Internal Notes</strong></span>' +
                    '<button type="button" class="close" ng-click="closeMe()")>'+
                    '<span aria-hidden="true">×</span></button>';
            var content = '<div style="overflow: auto; height: 300px; padding:true;">' + toHtml(attrs.note); + '</div>';
            $(element).popover({
                container: 'div.internal-wrap',
                content: function () {
                    return $compile(content)(scope);
                },
                placement: function (target, conts) {
                    $("div.popover.fade.in").popover("hide");
                    $(target).css({"width": "500px", "max-width": "500px", "cursor": "default" });
                    return "top";
                }, 
                title: function () {
                    return $compile(header)(scope);
                },
                html: true
            });
            scope.closeMe = function () {
                $(element).popover('hide');
            }  
            function toHtml(data) {               
                var res = data.split('\n');
                var result = "";              
                for (var i = 0; i < res.length; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }
        },
        controller: function ($scope, $element) {
        }
    };
});

//Transaction Thresholds Directive
directives.directive('transactionThresholdsPanel', ['$rootScope', 'systemThresholdsAppServices', function ($rootScope, systemThresholdsAppServices) {
    return {
        restrict: 'E',
        templateUrl: 'transaction-threshold-panel.html',
        scope: {
            transactionThresholdsViewModel: "=",
            disableTotalErrorMessage: "=",
            transactionThresholdsViewModelOriginal: "=",
            isView: "="
        },

        link: function (scope, el, attrs) {           
            systemThresholdsAppServices.loadTransactionThresholds().then(function (data) {
                scope.transactionThresholdsViewModel = data.data;
                scope.transactionThresholdsViewModelOriginal = angular.copy(scope.transactionThresholdsViewModel);
            });
        },
        controller: function ($scope) {

            //for popover
            var IndividualPopover = {
                content: Global.Settings.individualContent
            };

            var TotalPopover = {
                content: Global.Settings.totalContent
            };

            var autoApprovalPopover = {
                content: Global.Settings.autoApprovalTooltip
            };

            $('#individual-title-question-mark').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, IndividualPopover));
            $('#total-title-question-mark').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, TotalPopover));
            $('#autoapproval-title-question-mark').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, autoApprovalPopover));

            //check box - individual
            $scope.allIndividualTireCountsDisabled = false;
            $scope.disableIndividualErrorMessage = false;
            $scope.$watch('transactionThresholdsViewModel.Threshold_CBIndividualTireCount', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && $scope.transactionThresholdsViewModelOriginal && parseInt(oldVal) != parseInt(newVal)) {
                    if ($scope.transactionThresholdsViewModel.Threshold_CBIndividualTireCount) {
                        $scope.allIndividualTireCountsDisabled = false;
                        individualValidationCheck();                        
                    }
                    else {                       
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountPLT = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountPLT;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMT = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountMT;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountAGLS = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountAGLS;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountIND = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountIND;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountSOTR = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountSOTR;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMOTR = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountMOTR;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountLOTR = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountLOTR;
                        $scope.transactionThresholdsViewModel.Threshold_IndividualTireCountGOTR = $scope.transactionThresholdsViewModelOriginal.Threshold_IndividualTireCountGOTR;
                        $scope.allIndividualTireCountsDisabled = true;
                    }
                }
            }, true);

            //validation - individual
            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountPLT', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                   
                }            
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountMT', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                   
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountAGLS', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                    
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountIND', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                    
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountSOTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                   
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountMOTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                    
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountLOTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                    
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_IndividualTireCountGOTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    individualValidationCheck();                    
                }
            });

            function individualValidationCheck() {
                if (parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountPLT) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMT) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountAGLS) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountIND) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountSOTR) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountMOTR) >= 0
                        || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountLOTR) >= 0 || parseInt($scope.transactionThresholdsViewModel.Threshold_IndividualTireCountGOTR) >= 0) {
                    $scope.disableIndividualErrorMessage = false;
                }
                else {
                    $scope.disableIndividualErrorMessage = true;
                }
            }

            //check box - total
            $scope.allTotalTireCountsDisabled = false;
            $scope.disableTotalErrorMessage = false;
            $scope.$watch('transactionThresholdsViewModel.Threshold_CBTotalTireCount', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && $scope.transactionThresholdsViewModelOriginal && parseInt(oldVal) != parseInt(newVal)) {
                    if ($scope.transactionThresholdsViewModel.Threshold_CBTotalTireCount) {
                        $scope.allTotalTireCountsDisabled = false;
                        totalValidationCheck();
                    }
                    else {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOR = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountDOR;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOT = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountDOT;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountHIT = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountHIT;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountPTR = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountPTR;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountRTR = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountRTR;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountSTC = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountSTC;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountTCR = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountTCR;
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountUCR = $scope.transactionThresholdsViewModelOriginal.Threshold_TotalTireCountUCR;
                        $scope.allTotalTireCountsDisabled = true;
                    }
                }
            }, true);

            //validation - total
            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountDOR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOR = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountDOT', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOT = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountHIT', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountHIT = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountPTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountPTR = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountRTR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountRTR = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountSTC', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountSTC = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountTCR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountTCR = null;
                    }
                    totalValidationCheck();
                }
            });

            $scope.$watch('transactionThresholdsViewModel.Threshold_TotalTireCountUCR', function (newVal, oldVal) {
                if ($scope.transactionThresholdsViewModel && parseInt(oldVal) != parseInt(newVal)) {
                    if (newVal == 0) {
                        $scope.transactionThresholdsViewModel.Threshold_TotalTireCountUCR = null;
                    }
                    totalValidationCheck();
                }
            });

            function totalValidationCheck() {
                if ($scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountDOT
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountHIT || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountPTR
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountRTR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountSTC
                            || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountTCR || $scope.transactionThresholdsViewModel.Threshold_TotalTireCountUCR) {
                    $scope.disableTotalErrorMessage = false;
                }
                else {
                    $scope.disableTotalErrorMessage = true;
                }
            }
        }
    };
}]);


$(function () {   
    $(':input[type="text"]').keyup(function (e) {             
        if (this.value.length > 1 && parseInt(this.value) !== 0) {
            this.value = this.value.replace(/^0+/, ''); //ignore leading zeros
        }
        else if (this.value.length > 1 && parseInt(this.value) == 0) {
            this.value = this.value.slice(0, 1); //change multiple zeros to one zero
        }                
    });
});
