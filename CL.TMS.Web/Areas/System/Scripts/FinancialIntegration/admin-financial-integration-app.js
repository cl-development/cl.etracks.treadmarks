﻿'use strict';

var Security = {
    isReadonly: function (resource) {
        return resource === Global.FI.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.FI.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.FI.Security.Constants.NoAccess
    },
}

//FinancialIntegration App
var financialIntegrationApp = angular.module("adminFinancialIntegrationApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller","financialIntegration.directives", "financialIntegration.controllers", "financialIntegration.services"]);

//Controllers Section
var controllers = angular.module('financialIntegration.controllers', []);
controllers.controller('financialIntegrationController', ['$rootScope', '$scope', '$http', '$uibModal', 'financialIntegrationServices', function ($rootScope, $scope, $http, $uibModal,financialIntegrationServices) {
    $scope.vm = {}; 
    $scope.categoryID = $("#adminFICategoryList").val();
    $scope.isView = Security.isReadonly(Global.FI.Security.AdminFinancialIntegration);
    $("input.form-control").attr("style", "width:95%");
    if ($scope.isView) {
        $("input.form-control").attr("disabled", "disabled");
    }
    financialIntegrationServices.getData(0).then(function (data) {
        $scope.vm = data.data;
        console.log('$scope.vm', $scope.vm);
        //initial tooltips  
        $.each($('.fa-question-circle.steward'), function (index, item) {
            var stewardSettings = {            
                content: $scope.vm.stewardTooltip[index].ItemName
            };
            $(item).webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, stewardSettings));
        });
        $('.fa-question-circle.processor').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, { content: 'Processor TI Tax Distributions are set in the Tax Distributions - Transportation Incentives section of Hauler - Transportation Premiums.' }));
    });
    $("#adminFICategoryList").on("change", function () {
        var selectedValue = $(this).find("option:selected").val();
        $("div[id*='panel-']").hide();
        $("#panel-" + selectedValue).show();
        $scope.categoryID = selectedValue;
        financialIntegrationServices.getData(0).then(function (data) {
            $scope.vm = data.data
        });
    });
    //showValidinfo("", null);
    $scope.saveBtn = function () {
        var validInfo = validCheck($scope.vm, $scope.categoryID);
        if (!validInfo) {
            $([document.documentElement, document.body]).animate({scrollTop: $("span.valid-info:visible").offset().top - 200}, 500);
            return;
        }
        var confirmSaveModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmMessage = Global.FI.ConfirmSaveHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
            }
        });

        confirmSaveModal.result.then(function () {
            var data = $scope.vm; //.appSettingsVM;
            var categoryId = $scope.categoryID;
            financialIntegrationServices.updateData(data, categoryId).then(function (response) {
                if (response && response.data) {
                    //show error?
                }
                console.log(response, response.data);
            });
        });
    };

    $scope.cancelBtn = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmMessage = Global.FI.ConfirmCancelHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();                   
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: { 
            }
        });
        cancelModal.result.then(function (cancel) {          
            financialIntegrationServices.getData(0).then(function (data) {                
                $scope.vm = data.data
            });
            $("html, body").animate({ scrollTop: -180 }, 500);
        });
    };
}]);
function validCheck(vm, categoryID) {
    var tempData;  
    switch (categoryID) {
        case "1":
            tempData = vm.steward;          
            break;
        case "2":
            tempData = vm.collector;
          
            break;
        case "3":
            tempData = vm.hauler;
       
            break;
        case "4":
            tempData = vm.processor;
        
            break;
        case "5":
            tempData=vm.rpm;         
            break;
    }
    var result = true;
    $.each(tempData, function (index, item) {
        if (!item.AccountNumber || item.AccountNumber.length == 0) {
            result = false;
            return false;
        }
    })    
    return result;
}

//Services Section
var services = angular.module('financialIntegration.services', []);
services.factory('financialIntegrationServices', ["$http", function ($http) {
    var factory = {};

    factory.getData = function (categoryID) {
        return $http({
            method: 'GET',
            url: Global.FI.GetDataUrl,
            params: {
                categoryID: categoryID,
            }
        });
    }

    factory.updateData = function (data,id) {
        var submitVal = {
            url: Global.FI.UpdateDataUrl,
            method: "POST",
            data: { vm: data ,categoryID:id}
        }
        return $http(submitVal);
    }
    return factory;
}]);

//Directives Section
var directives = angular.module('financialIntegration.directives', []);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);
directives.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {

            var content = el[0].nextElementSibling.innerHTML;

            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);
directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;
            
            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",                
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});
directives.directive('validNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            //Note:  set the account number as free text as well as we discussed.
            //function inputText(formInput) {
            //    if (formInput  ) {
            //        var transformInput = formInput.replace(/[^0-9-]/g, ''); //allow number and dash only 
            //        transformInput = transformInput.substr(0, 1) === '-' ? '' : transformInput; // dash can not be the first character
            //        transformInput = transformInput.indexOf("--") > 0 ? transformInput.replace("--", "-") : transformInput; // not accept continue dash
            //        if (transformInput !== formInput) {                        
            //            ngModelCtrl.$setViewValue(transformInput);
            //            ngModelCtrl.$render();
            //        }
            //        return transformInput;
            //    }
            //    return '';
            //}
            //ngModelCtrl.$parsers.push(inputText);
        }
    };
});
function dateTimeConvert(data) {

    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
    //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

    return {
        date: d,
        time: curr_time
    }
}
