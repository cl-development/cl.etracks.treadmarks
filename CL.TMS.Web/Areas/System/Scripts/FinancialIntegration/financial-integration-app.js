﻿'use strict';

var Security = {
    isReadonly: function (resource) {
        return resource === Global.GP.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.GP.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.GP.Security.Constants.NoAccess
    },
    checkUserSecurity: function (type) {
        switch (type) {
            case "R":
                return Global.GP.Security.ParticipantManager
                break;
            case "S":
                return Global.GP.Security.StewardManager
                break;
            case "C":
                return Global.GP.Security.CollectorManager
                break;
            case "H":
                return Global.GP.Security.HaulerManager
                break;
            case "P":
                return Global.GP.Security.ProcessorManager
                break;
            case "M":
                return Global.GP.Security.RPMManager
                break;
        }
    }
}

//FinancialIntegration App
var financialIntegrationApp = angular.module("financialIntegrationApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller","financialIntegration.directives", "financialIntegration.controllers", "financialIntegration.services"]);

//Controllers Section
var controllers = angular.module('financialIntegration.controllers', []);
controllers.controller('financialIntegrationController', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);
controllers.controller('GPSecondModalCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'batchNumber', 'type', function ($scope, $http, $uibModal, $uibModalInstance, batchNumber, type) {
    $scope.batchNumber = batchNumber;
    $scope.type = type;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Common Controllers
controllers.controller('GpSuccessCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'successResult', 'header', 'body', function ($scope, $uibModal, $uibModalInstance, successResult, header, body) {

    $scope.sucessMessage = body;
    $scope.successHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('GpFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', 'isExport', function ($scope, $uibModal, $uibModalInstance, failResult, header, isExport) {

    if (!isExport)
    {
        $scope.failResult = failResult;
        $scope.failHeader = header;

        if ($scope.failResult.Errors.length > 0) {
            $scope.message = $scope.failResult.Errors[0];
        }
        else {
            $scope.message = $scope.failResult.Warnings[0];
        }
    }
    else {
        $scope.message = failResult;
        $scope.failHeader = header;
    }
    

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');        
    };
}]);

//Create Batch Panel Controller
controllers.controller('CreateBatchCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'type', 'panelName', 'financialIntegrationServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, type, panelName, financialIntegrationServices, $rootScope) {

    $scope.type = type;

    $scope.disableBtn = false;

    $scope.confirmMessage = Global.GP.CreateBatchConfirmHtml.replace("AAA", panelName);
    
    $scope.confirm = function () {
        $scope.disableBtn = true;
        financialIntegrationServices.createBatch($scope.type).then(function (response) {
            if (response.data.status) {
                $scope.disableBtn = false;
                $rootScope.$emit('PANEL_VIEW_UPDATED', "CreateBatch");
                $uibModalInstance.close(response.data.result);
            }
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//Post Batch Panel Controller
controllers.controller('PostBatchCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'batchNum', 'panelName', 'type',
    'financialIntegrationServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, batchNum, panelName, type, financialIntegrationServices, $rootScope) {

    $scope.disableBtn = false;
    $scope.confirmMessage = Global.GP.PostBatchConfirmHtml.replace("BatchNum", batchNum).replace("Type", type).replace("PanelName", panelName);
    
    $scope.confirm = function () {
        $scope.disableBtn = true;
        financialIntegrationServices.postBatch(batchNum, type).then(function (response) {
            if (response.data.status) {
                $scope.disableBtn = true;
                $rootScope.$emit('PANEL_VIEW_UPDATED', "PostBatch");
                $uibModalInstance.close(response.data.result);
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//On Hold Modal Transaction Controller
controllers.controller('OnHoldTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'gpTransNum', 'gpBatchEntyId', 'financialIntegrationServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, gpTransNum, gpBatchEntyId, financialIntegrationServices, $rootScope) {

    $scope.disableBtn = false;
    $scope.confirmMessage = Global.GP.OnHoldTransactionConfirmHtml.replace("TransNum", gpTransNum);

    $scope.confirm = function () {
        $scope.disableBtn = true;
        financialIntegrationServices.putOnHold(gpBatchEntyId).then(function (response) {
            if (response.data.status) {
                $scope.disableBtn = true;
                $rootScope.$emit('MODAL_VIEW_UPDATED', "OnHold");
                $rootScope.$emit('PANEL_VIEW_UPDATED', "OnHold");
                $uibModalInstance.close(response.data.result);
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//Off Hold Modal Transaction Controller
controllers.controller('OffHoldTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'gpTransNum', 'gpBatchEntyId', 'financialIntegrationServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, gpTransNum, gpBatchEntyId, financialIntegrationServices, $rootScope) {

    $scope.disableBtn = false;
    $scope.confirmMessage = Global.GP.OffHoldTransactionConfirmHtml.replace("TransNum", gpTransNum);

    $scope.confirm = function () {
        financialIntegrationServices.putOffHold(gpBatchEntyId).then(function (response) {
            $scope.disableBtn = true;
            if (response.data.status) {
                $scope.disableBtn = true;
                $rootScope.$emit('MODAL_VIEW_UPDATED', "OffHold");
                $rootScope.$emit('PANEL_VIEW_UPDATED', "OffHold");

                $uibModalInstance.close(response.data.result);
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//Remove Modal Transaction Controller
controllers.controller('RemoveTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'gpTransNum', 'gpBatchEntyId', 'gpType', 'financialIntegrationServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, gpTransNum, gpBatchEntyId, gpType, financialIntegrationServices, $rootScope) {

    $scope.disableBtn = false;
    $scope.confirmMessage = Global.GP.RemoveTransactionConfirmHtml.replace("TransNum", gpTransNum);

    switch (gpType)
    {
        case "C":
        case "P":
        case "H":
        case "M":
            $scope.note = Global.GP.RemoveNoteTypeHPCR;
            break;
        case "S":
            $scope.note = Global.GP.RemoveNoteTypeS;
            break;
        default:
            $scope.note = "";
    }

    $scope.confirm = function () {
        financialIntegrationServices.removeTransaction(gpBatchEntyId).then(function (response) {
            $scope.disableBtn = true;
            if (response.data.status) {
                $scope.disableBtn = true;
                $rootScope.$emit('MODAL_VIEW_UPDATED', "Remove");
                $rootScope.$emit('PANEL_VIEW_UPDATED', "Remove");
                $uibModalInstance.close(response.data.result);
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Services Section
var services = angular.module('financialIntegration.services', []);
services.factory('financialIntegrationServices', ["$http", function ($http) {
    var factory = {};

    factory.anchorCss = function () {
        return {
            "display": "block",
            "padding": "3px 20px",
            "clear": "both",
            "font-weight": "400",
            "line-height": "1.42857143",
            "color": "#000",
            "white-space": "nowrap"
        }
    }

    factory.createBatch = function (type) {        
        var submitVal = {
            url: Global.GP.CreateBatchUrl,
            method: "POST",
            data: { type: type }
        }
        return $http(submitVal);
    }

    factory.postBatch = function (batchNum, type) {

        var IsPost = false;
        var IsRePost = false;
        if (type === "Re-Post") {
            IsPost = false;
            IsRePost = true;
        }
        if (type === "Post") {
            IsPost = true;
            IsRePost = false;
        }

        var submitVal = {
            url: Global.GP.PostBatchUrl,
            method: "POST",
            data: { batchId: batchNum, IsPost: IsPost, IsRepost: IsRePost }
        }
        return $http(submitVal);
    }

    factory.putOnHold = function (gpTransNum) {
        var submitVal = {
            url: Global.GP.OnHoldTransactionUrl,
            method: "POST",
            data: { gpiBatchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.putOffHold = function (gpTransNum) {
        var submitVal = {
            url: Global.GP.OffHoldTransactionUrl,
            method: "POST",
            data: { gpiBatchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.removeTransaction = function (gpTransNum) {
        var submitVal = {
            url: Global.GP.RemoveTransactionUrl,
            method: "POST",
            data: { batchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.checkExport = function (batchId) {
        var submitVal = {
            url: Global.GP.CheckExportUrl,
            method: "POST",
            data: { batchId: batchId }
        }
        return $http(submitVal);
    }

    factory.getExport = function (batchId, panelName) {        
        return $http({
            method: 'GET',
            url: Global.GP.ExportUrl,
            params: {
                batchId: batchId,
                panelName: panelName
            }
        });
    }

    return factory;
}]);

//Directives Section
var directives = angular.module('financialIntegration.directives', []);
directives.directive('gpPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', function (DTOptionsBuilder, DTColumnBuilder) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'gp-panel-template.html',
        scope: {
            gpUrl: '@',
            type: '@',
            header: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {            
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {

                $scope.dtInstance = {};

                $scope.chevronId = $scope.header.replace(/\s/g, '');
                                        
                $rootScope.$on('PANEL_VIEW_UPDATED', function (e, data) {
                    $scope.dtInstance.DataTable.draw();
                });
            
                $scope.dtColumns = [
                        DTColumnBuilder.newColumn("BatchNum", "Batch #").withOption('name', 'BatchNum'),
                        DTColumnBuilder.newColumn("Count", "Count").withOption('name', 'Count'),
                        DTColumnBuilder.newColumn("Modified", "Modified").withOption('name', 'Modified').renderWith(function (data, type, full, meta) {
                            var date = dateTimeConvert(data);
                            return '<div title="' + date.time + '">' + date.date + '</div>';
                        }),
                        DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').renderWith(function (data, type, full, meta) {
                            switch (data.replace(" ", "")) {
                                case "Extracted":
                                    return '<div class="panel-table-status color-tm-yellow-bg" title="Extracted">Extracted</div>';
                                case "Queued":
                                    return '<div class="panel-table-status color-tm-orange-bg" title="Posted (Queued)">Queued</div>';
                                case "FailedStaging":
                                case "Failed":
                                    return '<div class="panel-table-status color-tm-red-bg" title="Failed (Import)">Failed</div>';
                                case "Posted":
                                    return '<div class="panel-table-status color-tm-green-bg" title="Posted (Imported)">Posted</div>';
                                case "OnHold":
                                    return '<div class="panel-table-status color-tm-gray-bg" title="On-Hold">On-Hold</div>';
                                case "Unknown":
                                    return '<div class="panel-table-status color-tm-red-bg" title="Unknown">Unknown</div>';
                                default:
                                    return data;
                            }
                        }),
                        DTColumnBuilder.newColumn("Message", "Message").withOption('name', 'Message').renderWith(function (data, type, full, meta) {
                            if (data) {
                                var html = $compile($templateCache.get('messagePopover.html').replace('Message',data))($scope);
                                return html[0].outerHTML;
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        .renderWith(actionsHtml)
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.gpUrl,
                    type: "POST",
                    error: function(xhr, error, thrown) {
                        console.log('xhr, error:', xhr, error);
                        window.location.reload(true);
                    }
                }).withOption('processing', true)
                    .withOption('responsive', true).withOption('bAutoWidth', false)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', [0, 'desc'])
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('initComplete', function (settings, result) {
                        $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                        $scope.$apply();
                    })
                    .withOption('drawCallback', function (settings) {
                        $scope.found = 'Found ' + settings.fnRecordsDisplay();
                        $scope.$apply();
                    });
                
                //open second modal
                $scope.openSecondModal = function (data) {
                    var secondGPModal = $uibModal.open({
                        templateUrl: 'gp-second-modal-template.html',
                        controller: 'GPSecondModalCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            batchNumber: function () { return data; },
                            type: function () { return $scope.type; }
                        }
                    });
                }

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    $scope.dtOptions.withScroller()
                                   .withOption('deferRender', true)
                                   .withOption('scrollY', 250);
                }
            
                //search
                $scope.search = function (dtInstance) {

                    if ($scope.searchText.length >= 1)
                    {
                        $scope.foundStyle = { 'display': 'block' };
                        $scope.removeIconStyle = { 'display': 'block' };
                        $scope.searchStyle = { 'visibility': 'visible', 'width':'190px' };
                        dtInstance.DataTable.search($scope.searchText).draw(false);
                    }
                    else
                    {
                        $scope.foundStyle = { 'display': 'none' };
                        $scope.removeIconStyle = { 'display': 'none' };
                        $scope.searchStyle = '';
                        dtInstance.DataTable.search($scope.searchText).draw(false);
                    }
                }

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                            
                $scope._pageX = 0;
                $scope._pageY = 0;
                $scope._cancelMouseClick = false;

                $('table').on('mousedown', 'tr', function (e) {
                    $scope._pageX = e.pageX;
                    $scope._pageY = e.pageY;
                });
                $('table').on('mouseup', 'tr', function (e) {
                    $scope._pageX = e.pageX - $scope._pageX;
                    $scope._pageY = e.pageY - $scope._pageY;
                    if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                        $scope._cancelMouseClick = true;
                    }
                });
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function () {
                        console.log('$scope._cancelMouseClick:', $scope._cancelMouseClick);
                        if ($scope._cancelMouseClick) {
                            $scope._cancelMouseClick = false;
                            return;
                        }
                        $scope.$apply(function () {
                            $scope.openSecondModal(aData.BatchNum);
                        });
                    });
                    return nRow;
                }

                function actionsHtml(data, type, full, meta) {
                    switch (data.Status.replace(" ", "")) {
                        case "Extracted":
                            var html = $compile($templateCache.get('panel-extracted.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        case "Queued":
                            var html = $compile($templateCache.get('panel-queued.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        case "Failed":
                        case "FailedStaging":
                            var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        case "Posted":
                            var html = $compile($templateCache.get('panel-posted.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        case "OnHold":
                            var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        case "Unknown":
                            var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.BatchNum).replace(/panelName/g, $scope.panelName))($scope);
                            return html[0].outerHTML;

                        default:
                            return "";
                    }
                }
        }
    }
}]);
directives.directive('gpModalView', ['DTOptionsBuilder', 'DTColumnBuilder', function (DTOptionsBuilder, DTColumnBuilder) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'gp-modal-template.html',
        scope: {
            gpUrl: '@',
            batchNum: '@',
            type: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.modalCss = {
                "width": "800px",
                "min-width": "800px"
            };
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {

            $scope.dtInstance = {};

            $scope.viewMore = true;

            $rootScope.$on('MODAL_VIEW_UPDATED', function (e, data) {
                $scope.dtInstance.DataTable.draw();
            });

            $scope.dtColumns = [
                        DTColumnBuilder.newColumn("GPTransactionNum", "Transaction #").withOption('name', 'GPTransactionNum'),
                        DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').renderWith(function (data, type, full, meta) {
                            switch (data.replace(" ", "")) {
                                case "Extracted":
                                    return '<div class="panel-table-status color-tm-yellow-bg" title="Extracted">Extracted</div>';
                                case "Queued":
                                    return '<div class="panel-table-status color-tm-orange-bg" title="Posted (Queued)">Queued</div>';
                                case "FailedStaging":
                                case "Failed":
                                    return '<div class="panel-table-status color-tm-red-bg" title="Failed (Import)">Failed</div>';
                                case "Posted":
                                    return '<div class="panel-table-status color-tm-green-bg" title="Posted (Imported)">Posted</div>';
                                case "OnHold":
                                    return '<div class="panel-table-status color-tm-gray-bg" title="On-Hold">On-Hold</div>';
                                case "Unknown":
                                    return '<div class="panel-table-status color-tm-red-bg" title="Unknown">Unknown</div>';
                                default:
                                    return data;
                            }
                        }),
                        DTColumnBuilder.newColumn("Message", "Message").withOption('name', 'Message').renderWith(function (data, type, full, meta) {
                            if (data) {
                                var html = $compile($templateCache.get('messagePopover.html').replace('Message', data))($scope);
                                return html[0].outerHTML;
                                //return '<label data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' + data + '"><i class="fa fa-comment color-tm-grey"></i></label>'
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        .renderWith(actionsHtml)
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.gpUrl.replace('AAA', $scope.batchNum),
                type: "POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                    .withOption('responsive', true).withOption('bAutoWidth', false)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', [0, 'desc'])
                    .withOption('lengthChange', false)                
                    .withDisplayLength(5)
                    .withDOM('tr')                    
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('initComplete', function (settings, result) {
                        $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                        $scope.$apply();
                    })
                    .withOption('drawCallback', function (settings) {
                        $scope.found = 'Found ' + settings.fnRecordsDisplay();
                        $scope.$apply();
                    });

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            function actionsHtml(data, type, full, meta) {
                switch (data.Status.replace(" ", "")) {
                    case "Extracted":
                        var html = $compile($templateCache.get('modal-extracted.html')
                            .replace('TransactionNumber', data.GPTransactionNum)
                            .replace("BatchEntryId", data.GpiBatchEntryId))($scope);
                        return html[0].outerHTML;
                    case "OnHold":
                        var html = $compile($templateCache.get('modal-hold.html')
                            .replace(/TransactionNumber/g, data.GPTransactionNum)
                            .replace(/BatchEntryId/g, data.GpiBatchEntryId)
                            .replace('Type', $scope.type))($scope);
                        return html[0].outerHTML;

                    default:
                        return "";
                }
            }
        }
    }
}]);
directives.directive('createBatch', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-create-batch-btn.html',
        scope: {
            type: '=',
            panelName: '='
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $rootScope) {

            $scope.IsReadOnly = false;
            if ($scope.$parent.type) {
                var resource = Security.checkUserSecurity($scope.$parent.type);
                $scope.IsReadOnly = Security.isReadonly(resource);
            }

            $scope.createBatch = function () {
                var firstCreateBatchModal = $uibModal.open({
                    templateUrl: 'gp-confirm-batch-modal.html',
                    controller: 'CreateBatchCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        type: function () {
                            return $scope.type;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        }
                    }
                });
                firstCreateBatchModal.result.then(function (data) {
                    $scope.createBatchResult = data;

                    if (data.Warnings.length > 0 || data.Errors.length > 0) {
                        var createBatchFail = $uibModal.open({
                            templateUrl: 'gp-fail-modal.html',
                            controller: 'GpFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                failResult: function () {
                                    return $scope.createBatchResult;
                                },
                                header: function () {
                                    return Global.GP.CreateBatchFailHeader;
                                },
                                isExport: function () {
                                    return false;
                                }
                            }
                        });
                        createBatchFail.result.then(function (data) {});
                    }
                    else {
                        var createBatchSuccessful = $uibModal.open({
                            templateUrl: 'gp-successful-modal.html',
                            controller: 'GpSuccessCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                successResult: function () {
                                    return $scope.createBatchResult;
                                },
                                header: function () {
                                    return Global.GP.CreateBatchSuccessHeader
                                },
                                body: function () {
                                    return Global.GP.CreateBatchSuccessHtml.replace("BatchNum", $scope.createBatchResult.ID).replace("PanelName", $scope.panelName);
                                }
                            }
                        });
                        createBatchSuccessful.result.then(function (data) {});
                    }
                });
            }
        }
    }
}]);
directives.directive('postBatch', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-post-batch-btn.html',
        scope: {
            batchNum: '@',
            type: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = financialIntegrationServices.anchorCss();

            if (scope.IsReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }            
        },
        controller: function ($scope, $uibModal) {

            $scope.IsReadOnly = false;
            if ($scope.$parent.type) {
                var resource = Security.checkUserSecurity($scope.$parent.type);
                $scope.IsReadOnly = Security.isReadonly(resource);
            }

            $scope.postBatch = function () {
                var postBatchModal = $uibModal.open({
                    templateUrl: 'gp-confirm-batch-modal.html',
                    controller: 'PostBatchCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        batchNum: function () {
                            return $scope.batchNum;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        type: function () {
                            return $scope.type;
                        }
                    }
                });
                postBatchModal.result.then(function (data) {
                    $scope.postBatchResult = data;

                    if (data.Warnings.length > 0 || data.Errors.length > 0) {
                        var postBatchFail = $uibModal.open({
                            templateUrl: 'gp-fail-modal.html',
                            controller: 'GpFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                failResult: function () {
                                    return $scope.postBatchResult;
                                },
                                header: function () {
                                    return Global.GP.PostBatchFailHeader;
                                },
                                isExport: function () {
                                    return false;
                                }
                            }
                        });
                        postBatchFail.result.then(function (data) { });
                    }
                    else {
                        var postBatchSuccessful = $uibModal.open({
                            templateUrl: 'gp-successful-modal.html',
                            controller: 'GpSuccessCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                successResult: function () {
                                    return $scope.postBatchResult;
                                },
                                header: function () {
                                    return Global.GP.PostBatchSuccessHeader
                                },
                                body: function () {
                                    return Global.GP.PostBatchSuccessHtml.replace("BatchNum", $scope.postBatchResult.ID).replace("PanelName", $scope.panelName);
                                }
                            }
                        });
                        postBatchSuccessful.result.then(function (data) { });
                    }
                });
            }
        }
    }
}]);
directives.directive('onHoldTransaction', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-on-hold-btn.html',
        scope: {
            transactionNum: '@',
            gpiBatchentryId: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = financialIntegrationServices.anchorCss();

            if (scope.IsReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            $scope.IsReadOnly = false;
            if ($scope.$parent.type) {
                var resource = Security.checkUserSecurity($scope.$parent.type);
                $scope.IsReadOnly = Security.isReadonly(resource);
            }

            $scope.onHold = function () {
                var onHoldTransactionModal = $uibModal.open({
                    templateUrl: 'gp-confirm-batch-modal.html',
                    controller: 'OnHoldTransCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        gpTransNum: function () {
                            return $scope.transactionNum;
                        },
                        gpBatchEntyId: function () {
                            return $scope.gpiBatchentryId;
                        }
                    }
                });
                onHoldTransactionModal.result.then(function (data) {
                    $scope.onHoldGpTransResult = data;

                    if (data.Warnings.length > 0 || data.Errors.length > 0) {
                        var onHoldGpTransFail = $uibModal.open({
                            templateUrl: 'gp-fail-modal.html',
                            controller: 'GpFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                failResult: function () {
                                    return $scope.onHoldGpTransResult;
                                },
                                header: function () {
                                    return Global.GP.OnHoldTransactionFailHeader;
                                },
                                isExport: function () {
                                    return false;
                                }
                            }
                        });
                        onHoldGpTransFail.result.then(function (data) { });
                    }
                });
            }
        }
    }
}]);
directives.directive('offHoldTransaction', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-off-hold-btn.html',
        scope: {
            transactionNum: '@',
            gpiBatchentryId: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = financialIntegrationServices.anchorCss();

            if (scope.IsReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {

            $scope.IsReadOnly = false;
            if ($scope.$parent.type) {
                var resource = Security.checkUserSecurity($scope.$parent.type);
                $scope.IsReadOnly = Security.isReadonly(resource);
            }

            $scope.offHold = function () {
                var offHoldTransactionModal = $uibModal.open({
                    templateUrl: 'gp-confirm-batch-modal.html',
                    controller: 'OffHoldTransCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        gpTransNum: function () {
                            return $scope.transactionNum;
                        },
                        gpBatchEntyId: function () {
                            return $scope.gpiBatchentryId;
                        }
                    }
                });

                offHoldTransactionModal.result.then(function (data) {
                    $scope.offHoldGpTransResult = data;

                    if (data.Warnings.length > 0 || data.Errors.length > 0) {
                        var offHoldGpTransFail = $uibModal.open({
                            templateUrl: 'gp-fail-modal.html',
                            controller: 'GpFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                failResult: function () {
                                    return $scope.offHoldGpTransResult;
                                },
                                header: function () {
                                    return Global.GP.OffHoldTransactionFailHeader;
                                },
                                isExport: function () {
                                    return false;
                                }
                            }
                        });
                        onHoldGpTransFail.result.then(function (data) { });
                    }
                });
            }
        }
    }
}]);
directives.directive('removeTransaction', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-remove-btn.html',
        scope: {
            transactionNum: '@',
            gpiBatchentryId: '@',
            type: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = financialIntegrationServices.anchorCss();

            if (scope.IsReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {

            $scope.IsReadOnly = false;
            if ($scope.$parent.type) {
                var resource = Security.checkUserSecurity($scope.$parent.type);
                $scope.IsReadOnly = Security.isReadonly(resource);
            }

            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'gp-confirm-batch-modal.html',
                    controller: 'RemoveTransCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        gpTransNum: function () {
                            return $scope.transactionNum;
                        },
                        gpBatchEntyId: function () {
                            return $scope.gpiBatchentryId;
                        },
                        gpType: function () {
                            return $scope.type;
                        }
                    }
                });

                removeTransactionModal.result.then(function (data) {
                    $scope.removeGpTransResult = data;

                    if (data.Warnings.length > 0 || data.Errors.length > 0) {
                        var removeGpTransFail = $uibModal.open({
                            templateUrl: 'gp-fail-modal.html',
                            controller: 'GpFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                failResult: function () {
                                    return $scope.removeGpTransResult;
                                },
                                header: function () {
                                    return Global.GP.RemoveTransactionFailHeader;
                                },
                                isExport: function () {
                                    return false;
                                }
                            }
                        });
                        removeGpTransFail.result.then(function (data) { });
                    }
                });
            }
        }
    }
}]);
directives.directive('exportBatch', ['financialIntegrationServices', function (financialIntegrationServices) {
    return {
        restrict: 'E',
        templateUrl: 'gp-export-batch-btn.html',
        scope: {
            batchNum: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = financialIntegrationServices.anchorCss();
        },
        controller: function ($scope, $uibModal) {
            $scope.exportBatch = function () {

                window.location = Global.GP.ExportUrl + "?batchId=" + $scope.batchNum + "&panelName=" + $scope.panelName;
            }
        }
    }
}]);
directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);
directives.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {

            var content = el[0].nextElementSibling.innerHTML;

            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);
directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;
            
            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",                
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

//OTSMTM2-745
directives.directive('gpActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', function claimActivityPanel($window, DTOptionsBuilder, DTColumnBuilder, $timeout) {
    return {
        scope: {
            snpUrl: '@',
        },
        restrict: 'E',
        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));
                    }
                }
                scope.$digest();
            });
            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '300',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }
            }
        },

        templateUrl: 'gpActivityPanel',
        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {

            $scope.dtInstance = {};

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width', '15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date + ' ' + date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.snpUrl,
                global: false,
                type: "POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order', [0, 'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    var direction = settings.aLastSort[0].dir;
                    var searchValue = $('#searchActivity').val();
                    
                    var url = "/System/FinancialIntegration/GetAllGPActivitiesExport?searchText=CCC&sortColumn=AAA&sortDirection=BBB";
                    url = url.replace('AAA', sortColumn).replace('BBB', direction).replace('CCC', searchValue);
                    $('#exportActivities').attr('href', url);
                    $scope.$apply();
                });

            $scope.url = $scope.snpUrl; //keep original snpUrl

            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false
            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };
                var searchValue = $('#searchActivity').val();
                if (searchValue) { //change original snpUrl by appending searchValue, because searchValue cannot be passed to backend by DataGridModel param if "More" button is clicked (maybe a bug of angular datatables)
                    var url = $scope.snpUrl;
                    url = url + "&temp=" + searchValue;
                    $scope.snpUrl = url;
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 190)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc: "data",
                                    url: $scope.snpUrl,
                                    global: false, // this makes sure ajaxStart is not triggered
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);

                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            $scope.updateGpActivityPanel = function(){
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    $scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.snpUrl,
                                   global: false,
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '300',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }

            
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace

function dateTimeConvert(data) {

    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
    //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

    return {
        date: d,
        time: curr_time
    }
}
