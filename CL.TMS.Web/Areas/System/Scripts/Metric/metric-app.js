﻿"use strict";
// app
var metricApp = angular.module("metricApp", ["metric.services", "metric.directives", "metric.controllers", "metric.filter"]);

//*********service section *******************
var services = angular.module("metric.services", []);
services.factory("productService", ["$http", function ($http) {
    var factory = {};

    factory.getDetailsByID = function (data) {
        var submitVal = {
            url: Global.Metric.LoadDetailsByIDUrl,
            method: "POST",
            data: { paramModel: data }
        }
        return $http(submitVal);
    }
    factory.getClaimPeriods = function () {
        var submitVal = {
            url: Global.Metric.getClaimPeriodsUrl,
            method: "GET",
            params: {}
        }
        return $http(submitVal);
    }
    factory.numberWithCommas = (x, pref) => {
        return pref + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // matrix transpose for row with column
    function transpose(matrix) {
        return Object.keys(matrix[0])
            .map(colNumber => matrix.map(rowNumber => rowNumber[colNumber]));
    }
    // init datetime picker
    factory.triggerDateTimePicker = function () {
        var now =moment().add(-2, 'months');
        var thisMonth = now.toISOString().split("T")[0];
        var startMonth = Global.Metric.StartMonth;
        $('#topTenStartDatePicker').datetimepicker({
            minView: 3,
            showOn: 'focus',
            autoclose: true,
            format: "M yyyy",
            viewMode: "year",
            startView: 'year',
            altField: '.set',
            minViewMode: "months",
            startDate: startMonth,
            endDate: thisMonth,
            minDate: startMonth,
        });
        $('#topTenStartDate').css("background-color", "#1EABF1");
        $('#topTenStartDate').css("color", "white");
        $('#topTenStartDate').css("font-size", "large");
        $("#topTenStartDate").attr("readonly", true);
    }
    // processor data
    factory.ytdDollorVM = function (data) {
        var result = {
            dollar: null,
            total: null
        }
        result.dollar = {
            xkey: "Year",
            ykeys: ["PLT", "MT", "OTR"],
            labels: ["PLT", "MT", "OTR", "Total"],
            data: [],
            colors: ["#E84E40", "#FFC107", "#03A9F4", "#9C27B0"],
            isCurrency: true,
            angle: 0,
            ymax: 0
        };
        result.total = {
            xkey: "Year1",
            ykeys: ["Total"],
            labels: ["Total"],
            data: [],
            colors: ["#9C27B0"],
            isCurrency: true,
            angle: 35,
            ymax: 0
        };
        if (typeof (data) === 'object') {
            var tempMax = 0;
            $.each(data, function (i, item) {
                var temp = Math.round(item.PLT + item.MT + item.OTR);
                if (Math.abs(temp) > tempMax) {
                    tempMax = temp;
                }
                result.total.data.push({
                    Total: temp,
                    Year1: "" + item.Year
                })
            });
            var exMax = tempMax.toExponential(1).split('e+');
            var ruMax = Math.ceil(parseFloat(exMax[0]) * 10 / 4);
            for (var i = 0; i < exMax[1]; i++) {
                ruMax = ruMax * 10;
            }
            result.total.ymax = ruMax * 4/10;
            tempMax = 0;
            $.each(data, function (i, item) {
                var temp = Math.max(item.PLT, item.MT, item.OTR);
                if (Math.abs(temp) > tempMax) {
                    tempMax = temp;
                }
                result.dollar.data.push(
                    {
                        PLT: Math.round(item.PLT),
                        MT: Math.round(item.MT),
                        OTR: Math.round(item.OTR),
                        Year: item.Year + " Dollars"
                    }
                );
            });
            exMax = tempMax.toExponential(1).split('e+');
            ruMax = Math.ceil(parseFloat(exMax[0]) * 10 / 4);
            for (var i = 0; i < exMax[1]; i++) {
                ruMax = ruMax * 10;
            }
            result.dollar.ymax = ruMax * 4/10;
        }
        return result;
    };
    factory.ytdUnitVM = function (data) {
        var result = {
            unit: null,
            total: null
        }
        result.unit = {
            xkey: "Year",
            ykeys: ["PLT", "MT", "OTR"],
            labels: ["PLT", "MT", "OTR", "Total"],
            data: [],
            colors: ["#E84E40", "#FFC107", "#03A9F4", "#9C27B0"],
            isCurrency: false,
            angle: 0,
            ymax: 0
        };
        result.total = {
            xkey: "Year1",
            ykeys: ["Total"],
            labels: ["Total"],
            data: [],
            colors: ["#9C27B0"],
            isCurrency: false,
            angle: 35,
            ymax: 0
        };
        if (typeof (data) === 'object') {
            var tempMax = 0;
            $.each(data, function (i, item) {
                var temp = Math.round(item.PLT + item.MT + item.OTR);
                if (Math.abs(temp) > tempMax) {
                    tempMax = temp;
                }
                result.total.data.push({
                    Total: temp,
                    Year1: "" + item.Year
                })
            });

            var exMax = tempMax.toExponential(1).split('e+');
            var ruMax = Math.ceil(parseFloat(exMax[0]) * 10 / 4);
            for (var i = 0; i < exMax[1]; i++) {
                ruMax = ruMax * 10;
            }
            result.total.ymax = ruMax * 4/10;
            tempMax = 0;
            $.each(data, function (i, item) {
                var temp = Math.max(item.PLT, item.MT, item.OTR);
                if (temp > tempMax) {
                    tempMax = temp;
                }
                result.unit.data.push(
                    {
                        PLT: Math.round(item.PLT),
                        MT: Math.round(item.MT),
                        OTR: Math.round(item.OTR),
                        Year: item.Year + " Units"
                    }
                );
            });

            exMax = tempMax.toExponential(1).split('e+');
            ruMax = Math.ceil(parseFloat(exMax[0]) * 10 / 4);
            for (var i = 0; i < exMax[1]; i++) {
                ruMax = ruMax * 10;
            }
            result.unit.ymax = ruMax * 4/10;
        }
        return result;
    };
    factory.tsfStatusVM = function (data) {
        var result = {
            barData: [],
            lineData: [],
            years: [],
            isStartYear: true,
            colors: ["#e84e40", "#03A9F4"],
            labels: [],
            month: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        }
        $.each(data, function (i, item) {
            if (result.years.indexOf(item.Year) < 0) {
                result.isStartYear = result.years.length == 0;
                result.years.push(item.Year);
                result.labels.push("" + item.Year + " Revenues");
            }
            if (result.isStartYear) {
                result.lineData.push([gd(result.years[0], item.Month), Math.round(item.Revenue)]);
            } else {
                result.barData.push([gd(result.years[0], item.Month), Math.round(item.Revenue)]);
            }
        });
        return result;
    };
    factory.paymentMethodVM = function (data) {
        var result = {
            xkey: "label",
            ykeys: [],
            labels: [],
            isFirst: true,
            data: [],
            tableData: [],
            colors: ["#01B0F0", "#FFD966", "#01AF50", "#3D3D3D"]
        };
        if (typeof (data) === 'object') {
            $.each(data, function (i, item) {
                item.PLT = Math.round(item.PLT);
                item.MT = Math.round(item.MT);
                item.OTR = Math.round(item.OTR);
                item["Total"] = item.PLT + item.MT + item.OTR;
            });

            result.tableData = data.slice();

            for (var k = 0; k < revData.length; k++) {
                var row = {};
                for (var i = 0; i < revData[k].length; i++) {
                    if (k == 0) {
                        result.labels.push("" + revData[k][i]);
                        result.ykeys.push("y" + revData[k][i]);
                    } else {
                        row["label"] = revKeys[k - 1];
                        row[result.ykeys[i]] = revData[k][i];
                    }
                }
                if (k == 0) {
                    result.labels.push("Total");
                } else {
                    result.data.push(row);
                }
            }
        }
        return result;
    };
    factory.topTenTablesVM = function (data) {
        var result = [];
        $.each(data, function (index, item) {
            var row = item;
            row.ChangePercentage = row.ChangePercentage.toFixed(2);
            row["StewardSubType"] = Global.Metric.StrewardSubType[item.StewardSubTypeID - 1];
            result.push(row);
        });
        return result;
    }
    factory.topTenChartsVM = function (data,startDate) {
        var lastYear = moment(startDate).year(), tempMax = 0;
        var result = {
            xkey: "Year",
            ykeys: ["Total"],
            labels: ["Total"],
            data: [
                { Year: lastYear+"", Total: 0 },
                { Year: lastYear-1+"", Total: 0 },
                { Year: lastYear-2+"", Total: 0 },
            ],
            colors: ["#9C27B0"],
            isCurrency: true,
            angle: 35,
            ymax: 0
        };
        if (typeof (data) === 'object') {        
            $.each(data, function (i, item) {               
                var tempTotal = Math.round(item.Total);
                if (tempTotal > tempMax) {
                    tempMax = tempTotal;
                }
                var tempData = result.data.find(function (ite) { return ite.Year == item.Year });
                if (Global.Metric.StartMonth == "2016-04-01") {
                    if (moment(startDate).month() < 3) {
                        tempData.Total = item.Year > 2016 ? tempTotal : 0;
                    } else {
                        tempData.Total = item.Year >= 2016 ? tempTotal : 0;
                    }
                } else {
                    tempData.Total = tempTotal;
                }               
            });
            var exMax = tempMax.toExponential(1).split('e+');
            var ruMax = Math.ceil(parseFloat(exMax[0]) * 10 / 4);
            for (var i = 0; i < exMax[1]; i++) {
                ruMax = ruMax * 10;
            }
            result.ymax = ruMax * 4 / 10;
            if (tempMax == 0) {
                result.data = [];
            }
        }
        return result;
    }
    //morris bar
    factory.morrisBar = function (barData, element) {
        var config = {
            data: barData.data,
            element: element,
            xkey: barData.xkey,
            ykeys: barData.ykeys,
            labels: barData.labels,
            fillOpacity: 0.6,
            xLabelAngle: barData.angle,
            hideHover: "no",
            hoverCallback: function (index, options, content) {
                var data = options.data[index];
                return (content);
            },
            yLabelFormat: function (y) {
                return barData.isCurrency ? factory.numberWithCommas(y, "$") : factory.numberWithCommas(y, "");
            },
            ymax: barData.ymax,
            gridTextColor: "black",
            gridTextWeight: "normal",
            gridTextSize: 12,
            barColors: barData.colors,
            behaveLikeLine: true,
            resize: true,
            pointFillColors: ["#ffffff"],
            pointStrokeColors: ["purple"],
            lineColors: ["gray", "red"]
        },
        bar = Morris.Bar(config);
        return bar;
    }
    // graph Chart for #721 two years revenue compares
    function gd(year, month) { return new Date(year, month - 1, 1).getTime(); }
    factory.plotGraphChart = function (graphData, element) {
        var config = [{
            data: graphData.barData,
            bars: {
                show: true,
                barWidth: 24 * 60 * 60 * 12000,
                lineWidth: 1,
                fill: 1,
                align: "center"
            },
            label: graphData.labels[1]
        },
            {
                data: graphData.lineData,
                color: graphData.colors[0],
                lines: {
                    show: true,
                    lineWidth: 3,
                },
                points: {
                    //fillColor: "#e84e40",
                    fillColor: "#ffffff",
                    pointWidth: 1,
                    show: true
                },
                label: graphData.labels[0]
            }],
            previousPoint = null;

        var plotGraph = $.plot(("#" + element), config, {
            colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
            grid: {
                tickColor: "#f2f2f2",
                borderWidth: 0,
                hoverable: true,
                clickable: true
            },
            legend: {
                noColumns: 1,
                container: $("#legendContainer"),
                display: false
            },
            shadowSize: 0,
            yaxis: {
                tickFormatter: function numberWithCommas(x) {
                    return "$" + x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
                },
            },
            xaxis: {
                mode: "time",
                tickSize: [1, "month"],
                tickLength: 0,
                axisLabelUseCanvas: true,
            }
        });

        $(window).resize(function () {
            $.plot(("#" + element), config, {
                colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
                grid: {
                    tickColor: "#f2f2f2",
                    borderWidth: 0,
                    hoverable: true,
                    clickable: true
                },
                legend: {
                    noColumns: 1,
                    container: $("#legendContainer"),
                    display: false
                },
                shadowSize: 0,
                yaxis: {
                    tickFormatter: function numberWithCommas(x) {
                        return "$" + x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
                    },
                },
                xaxis: {
                    mode: "time",
                    tickSize: [1, "month"],
                    tickLength: 0,
                    axisLabelUseCanvas: true,
                }
            });
        });

        $("#" + element).bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $("#flot-tooltip").remove();
                    showTooltip(item.pageX, item.pageY, item.dataIndex, graphData);
                }
            }
            else {
                $("#flot-tooltip").remove();
                previousPoint = [0, 0, 0];
            }
        });

        function showTooltip(x, y, index, graphData) {
            var temp = '<div id="flot-tooltip"><b class="text-c">' + graphData.month[index] + "</b><br/><b>";
            if (graphData.lineData[index]) {
                temp += graphData.years[0] + '  Revenues: </b><i>' + factory.numberWithCommas(graphData.lineData[index][1], "$") + '</i>';
            }
            if (graphData.barData[index]) {
                temp += '<br/><b>' + graphData.years[1] + '  Revenues: </b><i>' + factory.numberWithCommas(graphData.barData[index][1], "$") + '</i>';
            }
            temp += '</div>';
            $(temp).css({
                top: y + 5,
                left: x + 20
            }).appendTo("body").fadeIn(200);
        };
    }
    return factory;
}]);

//**********directives section***************
var directives = angular.module("metric.directives", []);

directives.directive("metricYtdDollarPanel", ["$window", "$timeout", "productService",
    "$http", "$compile", "$templateCache", "$rootScope", "$uibModal",
    function ($window, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {},
            restrict: "E",
            templateUrl: "metric-ytd-dollar-panel.html",
            link: function (scope, elem, attr, ctrl) { },
            controller: ["$scope", function ($scope) {
                $scope.vm = {};
                var paramModel = {
                    CategoryID: Global.Metric.Category.YearToDateTSFDollar,
                    StartDate: "",
                };
                productService.getDetailsByID(paramModel).then(function (result) {
                    if (result && result.data && result.data.result) {
                        $scope.vm = productService.ytdDollorVM(result.data.result.ytdVMs);
                        productService.morrisBar($scope.vm.dollar, "barchart-dollar");
                        productService.morrisBar($scope.vm.total, "barchart-dollar-total");
                    }
                });
            }]
        }
    }
]);
directives.directive("metricYtdUnitPanel", ["$window", "$timeout", "productService",
    "$http", "$compile", "$templateCache", "$rootScope", "$uibModal",
    function ($window, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {},
            restrict: "E",
            templateUrl: "metric-ytd-unit-panel.html",
            link: function (scope, elem, attr, ctrl) { },
            controller: ["$scope", function ($scope) {
                $scope.vm = {};
                var paramModel = {
                    CategoryID: Global.Metric.Category.YearToDateTSFUnits,
                    StartDate: "",
                };

                productService.getDetailsByID(paramModel).then(function (result) {
                    if (result && result.data && result.data.result) {
                        $scope.vm = productService.ytdUnitVM(result.data.result.ytdVMs);
                        productService.morrisBar($scope.vm.unit, "barchart-unit");
                        productService.morrisBar($scope.vm.total, "barchart-unit-total");
                    }
                });
            }]
        }
    }
]);
directives.directive("metricTsfStatusPanel", ["$window", "$timeout", "productService",
    "$http", "$compile", "$templateCache", "$rootScope", "$uibModal",
    function ($window, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {},
            restrict: "E",
            templateUrl: "metric-tsf-status-panel.html",
            link: function (scope, elem, attr, ctrl) { },
            controller: ["$scope", function ($scope) {
                var paramModel = {
                    CategoryID: Global.Metric.Category.StewardTSFStatusOverview,
                    StartDate: "",
                    EndDate: ""
                };
                productService.getDetailsByID(paramModel).then(function (result) {
                    if (result && result.data && result.data.result) {
                        $scope.vm = productService.tsfStatusVM(result.data.result.tsfStatusVMs);
                        var tempTotal = 0; $scope.vm.TimeClass = {};
                        $.each(result.data.result.tsfStatusTimeClassVMs, function (i, item) {
                            tempTotal += item.Amount;
                            $scope.vm.TimeClass[item.TimeClass] = item.Amount;
                        });
                        $scope.vm.TimeClass["Total"] = tempTotal;
                        productService.plotGraphChart($scope.vm, "graphchart-status");
                    }
                });
            }]
        }
    }
]);
directives.directive("metricPaymentMethodPanel", ["$window", "$timeout", "productService",
    "$http", "$compile", "$templateCache", "$rootScope", "$uibModal",
    function ($window, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {},
            restrict: "E",
            templateUrl: "metric-payment-method-panel.html",
            link: function (scope, elem, attr, ctrl) { },
            controller: ["$scope", function ($scope) {
                $scope.vm = {};
                var paramModel = {
                    CategoryID: Global.Metric.Category.StewardPaymentMethods,
                    StartDate: "",
                    EndDate: ""
                };
            }]
        }
    }
]);
directives.directive("metricTopTenStewardsPanel", ["$window", "$timeout", "productService",
    "$http", "$compile", "$templateCache", "$rootScope", "$uibModal",
    function ($window, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {},
            restrict: "E",
            templateUrl: "metric-top-ten-stewards-panel.html",
            link: function (scope, elem, attr, ctrl) {
                var thisMonth =moment().add(-2, 'months').format('MMM YYYY');
                var paramModel = {
                    CategoryID: Global.Metric.Category.TopTenStewards,
                    StartDate: thisMonth
                };
                scope.noData = false;
                productService.getDetailsByID(paramModel).then(function (result) {
                    if (result && result.data && result.data.result) {
                        scope.vm.topTenTables = productService.topTenTablesVM(result.data.result.topTenTableVMs);
                        scope.vm.topTenCharts = productService.topTenChartsVM(result.data.result.topTenChartVMs,result.data.result.startDate);
                        if (scope.vm.topTenCharts.data.length > 0) {
                            productService.morrisBar(scope.vm.topTenCharts, "barchart-top-ten");
                        }
                        if ( scope.vm.topTenTables.length==0){
                            scope.noData = true;
                        }
                    }
                });
            },
            controller: function ($scope, $rootScope) {
                var thisMonth =moment().add(-2, 'months').format('MMM YYYY');
                $scope.vm = { startDate: thisMonth };
                productService.triggerDateTimePicker();
                $("#topTenStartDatePicker").on("change", function (e) {
                    var inputDate = moment($scope.vm.startDate,"MMM YYYY");
                    var thisMonth =moment().add(-2, 'months');
                    var startMonth = moment(Global.Metric.StartMonth,"YYYY-MM-DD");
                    if (!inputDate.isValid() || inputDate.diff(thisMonth,"months") >= 0 || inputDate.diff(startMonth,"months") < 0) {
                        $scope.vm.startDate = thisMonth.format('MMM YYYY');
                    }
                    var paramModel = {
                        CategoryID: Global.Metric.Category.TopTenStewards,
                        StartDate: $scope.vm.startDate
                    };
                    $scope.noData = false;
                    productService.getDetailsByID(paramModel).then(function (result) {
                        if (result && result.data && result.data.result) {
                            $("#barchart-top-ten").html("");
                            $scope.vm.topTenTables = productService.topTenTablesVM(result.data.result.topTenTableVMs);
                            $scope.vm.topTenCharts = productService.topTenChartsVM(result.data.result.topTenChartVMs, result.data.result.startDate);
                            if ($scope.vm.topTenCharts.data.length > 0) {
                                productService.morrisBar($scope.vm.topTenCharts, "barchart-top-ten");
                            }
                            if ($scope.vm.topTenTables.length == 0) {
                                $scope.noData = true;
                            }
                        }
                    });
                });
            }
        }
    }
]);

//for show action gear pop-up
directives.directive("tablerowpopover", ["$compile", function ($compile) {

    return {
        restrict: "A",
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: "bottom",
                container: "body",
                template: "<div class='popover' role='tooltip'><div class='popover-content'></div></div>",
                html: true
            });
        }
    }
}]);
directives.directive("htmlMessage", ["$compile", function ($compile) {
    return {
        restrict: "E",
        scope: {
            message: "="
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

//***************controller section**************
var controllers = angular.module("metric.controllers", []);
controllers.controller("metricController", ["$rootScope", "$scope", "$http", "$uibModal", "productService", function ($rootScope, $scope, $http, $uibModal, productService) {

}]);

//****************filter section
var filters = angular.module("metric.filter", []);
filters.filter("absChange", function () {
    return function (x) {
        return Math.abs(x).toFixed(0);
    };
});
