﻿var DOTTransactionApp = angular.module('DOTTransactionApp', ['DOTTransaction.directives', 'DOTTransaction.controllers', 'ui.bootstrap']);

var dotTransControllers = angular.module('DOTTransaction.controllers', []);
dotTransControllers.controller('TRCtrl', ['$rootScope', '$scope', function ($rootScope, $scope) {
    $scope.msg = 'Hello World';
}]);

var dotTransDirectives = angular.module('DOTTransaction.directives', []);

dotTransDirectives.directive('sortYardCapacity', function () {
    return {
        restrict: "C",
        scope: {
        },
        link: function (scope, element) {
            element.change(function (e) {
                scope.manageDocument(element.val());
            });
        },
        controller: function ($rootScope, $scope) {
            $scope.manageDocument = function (maxVal) {
                alert(maxVal);
            }
        }
    };
});