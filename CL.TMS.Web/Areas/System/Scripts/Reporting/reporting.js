﻿(function () {
    $('#dpStartingDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'MM dd yyyy',
        maxDate: '+0m +0w',//$.now(),
        minDate: new Date(2009, 09, 1),
        altFormat: 'dd/mm/yyyy',
        altField: '.set',
    });

    $('#dpEndingDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'MM dd yyyy',
        maxDate: '0',
        minDate: new Date(2009, 09, 1),
        altFormat: 'dd/mm/yyyy',
        altField: '.set',
    });
    
    $('input[name="report-page-range"]').change(function () {
        if (this.value == 'All') {
            $('.page-index').attr('disabled', 'disabled');
        } else {
            $('.page-index').removeAttr('disabled');
        }
    });
})();