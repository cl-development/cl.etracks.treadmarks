﻿$(function () {

    //Permission for Add User button //OTSTM2-882
    if (Global.InvitationIndex.AdminUserPageReadOnly.toLowerCase() == 'true' || Global.InvitationIndex.Permission.DisableActionGearEditAndAddBtn) {
        $("#addUserBtn").addClass("disabled").prop("disabled", true);
    }

    var hasRoleSelected=function()
    {
        var isSelected=false;
        var addRoleElements = $('input:checkbox[class=ckAddRole]');
        for (var i = 0; i < addRoleElements.length; i++)
        {
            var element = addRoleElements.eq(i);
            if (element.is(":checked"))
            {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }
    
    var hasClaimRoleSelected = function () {
        var isSelected = false;
        var addRoleElements = $('input:checkbox[class=ckClaimsRole]');
        for (var i = 0; i < addRoleElements.length; i++) {
            var element = addRoleElements.eq(i);
            if (element.is(":checked")) {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }

    var hasClaimRoutingSelected = function () {
        var isSelected = false;
        var addRoleElements = $('input:checkbox[class=ckClaimsRouting]');
        for (var i = 0; i < addRoleElements.length; i++) {
            var element = addRoleElements.eq(i);
            if (element.is(":checked")) {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }

    $('.ckAddRole').click(function () {
       
        var isAddRoleSelected = hasRoleSelected();
        var isClaimRoleSelected = hasClaimRoleSelected();
        var isClaimRoutingSelected = hasClaimRoutingSelected();
        if ((!isAddRoleSelected) && (!isClaimRoleSelected)) {
            $('#btnadduser').prop('disabled', true);
            return;
        }
        if ((isAddRoleSelected) && (!isClaimRoleSelected) && (isClaimRoutingSelected))
        {
            $('#btnadduser').prop('disabled', true);
            return;
        }
        if ((isAddRoleSelected) && (isClaimRoleSelected) && (!isClaimRoutingSelected)) {
            $('#btnadduser').prop('disabled', true);
            return;
        }
        $('#btnadduser').prop('disabled', false);
    });
    
    var ClaimRoleChecking=function()
    {
        var isClaimRoleSelected = hasClaimRoleSelected();
        var isClaimRoutingSelected = hasClaimRoutingSelected();
        var isAddRoleSelected = hasRoleSelected();
        if ((isClaimRoleSelected) && (!isClaimRoutingSelected)) {
            $('#btnadduser').prop('disabled', true);
            return;
        }
        if ((!isClaimRoleSelected) && (isClaimRoutingSelected)) {
            $('#btnadduser').prop('disabled', true);
            return;
        }
        
        if ((!isClaimRoleSelected) && (!isAddRoleSelected)) {
            $('#btnadduser').prop('disabled', true);
            return;
        }

        $('#btnadduser').prop('disabled', false);
    }


    $('.ckClaimsRole').click(function () {
        ClaimRoleChecking();
    });

    $('.ckClaimsRouting').click(function () {
        ClaimRoleChecking();
    });

    //Editing user
    var hasEditRoleSelected = function () {
        var isSelected = false;
        var addRoleElements = $('input:checkbox[class=ckEditRole]');
        for (var i = 0; i < addRoleElements.length; i++) {
            var element = addRoleElements.eq(i);
            if (element.is(":checked")) {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }

    var hasEditClaimRoleSelected = function () {
        var isSelected = false;
        var addRoleElements = $('input:checkbox[class=ckEditClaimsRole]');
        for (var i = 0; i < addRoleElements.length; i++) {
            var element = addRoleElements.eq(i);
            if (element.is(":checked")) {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }

    var hasEditClaimRoutingSelected = function () {
        var isSelected = false;
        var addRoleElements = $('input:checkbox[class=ckEditClaimsRouting]');
        for (var i = 0; i < addRoleElements.length; i++) {
            var element = addRoleElements.eq(i);
            if (element.is(":checked")) {
                isSelected = true;
                break;
            }
        }
        return isSelected;
    }

    $('.ckEditRole').click(function () {
        var isAddRoleSelected = hasEditRoleSelected();
        var isClaimRoleSelected = hasEditClaimRoleSelected();
        var isClaimRoutingSelected = hasEditClaimRoutingSelected();
        if ((!isAddRoleSelected) && (!isClaimRoleSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }
        if ((isAddRoleSelected) && (!isClaimRoleSelected) && (isClaimRoutingSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }
        if ((isAddRoleSelected) && (isClaimRoleSelected) && (!isClaimRoutingSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }
        $('#btneditusernext').prop('disabled', false);
    });

    var EditClaimRoleChecking = function () {
        var isClaimRoleSelected = hasEditClaimRoleSelected();
        var isClaimRoutingSelected = hasEditClaimRoutingSelected();
        var isAddRoleSelected = hasEditRoleSelected();
        if ((isClaimRoleSelected) && (!isClaimRoutingSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }
        if ((!isClaimRoleSelected) && (isClaimRoutingSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }

        if ((!isClaimRoleSelected) && (!isAddRoleSelected)) {
            $('#btneditusernext').prop('disabled', true);
            return;
        }

        $('#btneditusernext').prop('disabled', false);
    }

    $('.ckEditClaimsRole').click(function () {
        EditClaimRoleChecking();
    });

    $('.ckEditClaimsRouting').click(function () {
        EditClaimRoleChecking();
    });

    var checkM = true;
    function CheckEmailExistance() {
        var email = $('#emailaddress-input').val();
        var req = { emailAddress: email }
        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.CheckEmailUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            traditional: true,
            success: function (data) {
                if (data.status !== '') {
                    $('#validemail-input').text(data.status);
                    checkM = false;
                    return false;
                }
                else {

                    checkM = true;
                    return true;
                }
            },
            error: function (xhr) {
                //  alert(xhr.status + ' ' + xhr.statusText);
            }
        });
    };



    $('#modalAddUser').on('show.bs.modal', function() {
        clear();        
    });

    $('#modalAddNewUser').on('show.bs.modal', function () {
        clear();        

        $.ajax({
            url: '/System/Invitation/NewStaffUser',
            method: 'GET',
            dataType: "JSON",
            //data: { claimId: $('#ClaimID').val(), date: CreditRatePeriodDate },
            complete: function (result) {

                $('#multiselect_to').empty();
                
                $('#multiselect').empty();
                var listAvailableRoles = $('#multiselect');
                //select.append('<option value="0">None</option>');                
                //alert(JSON.stringify(result.responseJSON.AvailableRoles.length));
                
                //populate Available roles
                for (var i = 0; i < result.responseJSON.AvailableRoles.length; i++) {
                    listAvailableRoles.append(
                            $('<option/>', {
                                value: result.responseJSON.AvailableRoles[i].Id,
                                html: result.responseJSON.AvailableRoles[i].RoleName,
                                Title: result.responseJSON.AvailableRoles[i].RoleName
                            })
                    );
                }

                //populate Workflow dropdown lists
                var ddlStewardWorkflow = $('#ddlStewardWorkflow');
                var ddlCollectorWorkflow = $('#ddlCollectorWorkflow');
                var ddlHaulerWorkflow = $('#ddlHaulerWorkflow');
                var ddlProcessorWorkflow = $('#ddlProcessorWorkflow');
                var ddlRPMWorkflow = $('#ddlRPMWorkflow');

                $('#ddlStewardWorkflow').empty();
                $('#ddlCollectorWorkflow').empty();
                $('#ddlHaulerWorkflow').empty();
                $('#ddlProcessorWorkflow').empty();
                $('#ddlRPMWorkflow').empty();


                for (var i = 0; i < result.responseJSON.StewardWorkflowList.length; i++) {
                    ddlStewardWorkflow.append(
                            $('<option/>', {
                                value: i,
                                html: result.responseJSON.StewardWorkflowList[i],
                                title: result.responseJSON.StewardWorkflowList[i],
                            })
                    );
                }

                for (var i = 0; i < result.responseJSON.WorkflowList.length; i++) {
                
                    ddlCollectorWorkflow.append(
                            $('<option/>', {
                                value: i,
                                html: result.responseJSON.WorkflowList[i],
                                title: result.responseJSON.WorkflowList[i],
                            })
                    );

                    ddlHaulerWorkflow.append(
                            $('<option/>', {
                                value: i,
                                html: result.responseJSON.WorkflowList[i],
                                title: result.responseJSON.WorkflowList[i],
                            })
                    );

                    ddlProcessorWorkflow.append(
                            $('<option/>', {
                                value: i,
                                html: result.responseJSON.WorkflowList[i],
                                title: result.responseJSON.WorkflowList[i],
                            })
                    );

                    ddlRPMWorkflow.append(
                            $('<option/>', {
                                value: i,
                                html: result.responseJSON.WorkflowList[i],
                                title: result.responseJSON.WorkflowList[i],
                            })
                    );
                }
            }
        });
        //Apply permission for adding
        var rolesPermission = Global.InvitationIndex.Permission.RolesPermission;
        var routingWorkflowClaimsPermission = Global.InvitationIndex.Permission.RoutingWorkflowClaimsPermission;
        var routingWorkflowRemittancesPermission = Global.InvitationIndex.Permission.RoutingWorkflowRemittancesPermission
        var routingWorkflowApplicationsPermission = Global.InvitationIndex.Permission.RoutingWorkflowApplicationsPermission
        if (rolesPermission != "EditSave" && routingWorkflowClaimsPermission != "EditSave" && routingWorkflowRemittancesPermission != "EditSave" && routingWorkflowApplicationsPermission != "EditSave") {
            $("#firstname-input").addClass("disabled").prop("disabled", true);
            $("#lastname-input").addClass("disabled").prop("disabled", true);
            $("#emailaddress-input").addClass("disabled").prop("disabled", true);
            $("#btnadduser").addClass("disabled").prop("disabled", true);
        }
        if (rolesPermission == "ReadOnly") {
            $("#multiselect_rightSelected").css("pointer-events", "none");
            $("#multiselect_rightSelected > i").css("color", "lightblue");
            $("#multiselect_leftSelected").css("pointer-events", "none");
            $("#multiselect_leftSelected > i").css("color", "lightblue");
            $("#multiselect").addClass("disabled").prop("disabled", true);
            $("#multiselect_to").addClass("disabled").prop("disabled", true);           
        }
        if (routingWorkflowClaimsPermission == "ReadOnly") {           
            $("#ddlCollectorWorkflow").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelCollector = $("#ddlCollectorWorkflow option:selected");
            $("#ddlCollectorWorkflow").change(function () {
                lastSelCollector.prop("selected", true);
            });
            $("#ddlCollectorWorkflow").click(function () {
                lastSelCollector = $("#ddlCollectorWorkflow option:selected");
            });
            $("#ddlHaulerWorkflow").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelHauler = $("#ddlHaulerWorkflow option:selected");
            $("#ddlHaulerWorkflow").change(function () {
                lastSelHauler.prop("selected", true);
            });
            $("#ddlHaulerWorkflow").click(function () {
                lastSelHauler = $("#ddlHaulerWorkflow option:selected");
            });
            $("#ddlProcessorWorkflow").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelProcessor = $("#ddlProcessorWorkflow option:selected");
            $("#ddlProcessorWorkflow").change(function () {
                lastSelProcessor.prop("selected", true);
            });
            $("#ddlProcessorWorkflow").click(function () {
                lastSelProcessor = $("#ddlProcessorWorkflow option:selected");
            });
            $("#ddlRPMWorkflow").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelRPM = $("#ddlRPMWorkflow option:selected");
            $("#ddlRPMWorkflow").change(function () {
                lastSelRPM.prop("selected", true);
            });
            $("#ddlRPMWorkflow").click(function () {
                lastSelRPM = $("#ddlRPMWorkflow option:selected");
            });

            $("#cbClaimsCollector").addClass("disabled").prop("disabled", true);
            $("#cbClaimsHauler").addClass("disabled").prop("disabled", true);
            $("#cbClaimsProcessor").addClass("disabled").prop("disabled", true);
            $("#cbClaimsRPM").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowRemittancesPermission == "ReadOnly") {            
            $("#ddlStewardWorkflow").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelRemittance = $("#ddlStewardWorkflow option:selected");
            $("#ddlStewardWorkflow").change(function () {
                lastSelRemittance.prop("selected", true);
            });
            $("#ddlStewardWorkflow").click(function () {
                lastSelRemittance = $("#ddlStewardWorkflow option:selected");
            });
            $("#cbClaimsSteward").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowApplicationsPermission == "ReadOnly") {
            $("#cbApplicationsRoutingSteward").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionSteward").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingCollector").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionCollector").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingHauler").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionHauler").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingProcessor").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionProcessor").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingRPM").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionRPM").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowClaimsPermission == "NoAccess" && routingWorkflowRemittancesPermission == "NoAccess") {
            $("#modalAddNewUser").find(".verticalLine").removeClass("verticalLine");
        }
    });

    
    jQuery('.error').hide(); // Hide Warning Label. 

    $("#multiselect_to").bind("DOMNodeInserted ", function () {
        jQuery("label#select_error").hide(); // show Warning         
    });
    
    $("#multiselectEdit_to").bind("DOMNodeInserted ", function () {
        jQuery("label#selectEdit_error").hide(); // show Warning         
    });
    
    $('#btnadduser').on("click", function () {
        var form = document.forms['frminvitation'];
        //var trigger = $(event.relatedTarget); //comment out for OTSTM2-1045, because it does not work on Firefox
        jQuery.ajaxSetup({ async: false });

        if (!$(form).valid()) {
            if ($("#hasRolePanel").length) { //check if empty only when there is Role panel
                if ($('#multiselect_to option').length == 0) {
                    jQuery("label#select_error").show(); // show Warning 
                    jQuery("select#multiselectEdit_to").focus();  // Focus the select box      
                }  
            }                 
            return false;
        }        

        //if form is valid then check if email is unique
        var checkEmail = CheckEmailExistance();
        if (checkM == false) {
            return false;
        }

        //Assigned roles list if empty
        if ($("#hasRolePanel").length) { //check if empty only when there is Role panel
            if ($('#multiselect_to option').length == 0) {
                jQuery("label#select_error").show(); // show Warning 
                jQuery("select#multiselectEdit_to").focus();  // Focus the select box      
                return false;
            }
        }
        
       
        if ($(form).valid()) {
            if ($("#hasRolePanel").length && $("#hasStewardWorkflowPanel").length && $("#hasClaimWorkflowPanel").length && $("#hasApplicationPanel").length) {
                $('#modalAddNewUser').modal('hide');
                $('#modalAddUserSendInviteConfirm').modal('show');
            }
            else {
                $('#modalAddNewUser').modal('hide');
                $('#modalWarningMessage').modal('show');
            }          
            return true;
        };       
              
        return false;
    });

    $('#modalAddUserSendInviteConfirm').on('show.bs.modal', function (event) {
        var firstname = $("#firstname-input").val() + ' ' + $("#lastname-input").val();
        $(this).find('#dynamicname').text(firstname);
        $(this).find('#dynamicemail').text($("#emailaddress-input").val());
    });

    $('#btncanceluser').on("click", function () {
        clear();       
    });

    function clear() {
        jQuery("label#select_error").hide(); // show Warning         
        jQuery("label#selectEdit_error").hide(); // show Warning         

        $('#firstname-input').val('');
        $('#lastname-input').val('');
        $('#emailaddress-input').val('');
        $('#status').text('');
        $('#validfirstname-input').text('');
        $('#validlastname-input').text('');
        $('#validemail-input').text('');

        $('#validfirstnameEdit-input').text('');
        $('#validlastnameEdit-input').text('');

        $('input:checkbox[class=ckAddRole]').each(function () {
            $(this).prop('checked', false);
        });
        $('input:checkbox[class=ckClaimsRole]').each(function () {
            $(this).prop('checked', false);
        });
        $('input:checkbox[class=ckClaimsRouting]').each(function () {
            $(this).prop('checked', false);
        });

        $('#cbClaimsSteward').prop('checked', false);
        $('#cbClaimsCollector').prop('checked', false);
        $('#cbClaimsHauler').prop('checked', false);
        $('#cbClaimsProcessor').prop('checked', false);
        $('#cbClaimsRPM').prop('checked', false);

        $('#cbApplicationsRoutingSteward').prop('checked', false);
        $('#cbApplicationsActionSteward').prop('checked', false);

        $('#cbApplicationsRoutingCollector').prop('checked', false);
        $('#cbApplicationsActionCollector').prop('checked', false);

        $('#cbApplicationsRoutingHauler').prop('checked', false);
        $('#cbApplicationsActionHauler').prop('checked', false);

        $('#cbApplicationsRoutingProcessor').prop('checked', false);
        $('#cbApplicationsActionProcessor').prop('checked', false);

        $('#cbApplicationsRoutingRPM').prop('checked', false);
        $('#cbApplicationsActionRPM').prop('checked', false);
    }

    //User list datatable
    $('#btnadduserconfirm').on("click", function () {

        var firstname = $('#firstname-input').val();
        var lastname = $('#lastname-input').val();
        var email = $('#emailaddress-input').val();
      
        var AssignedRolesList = [];
        $('#multiselect_to option').each(function () {
            AssignedRolesList.push({
                Id: this.value,
                RoleName: this.text
            });
        });

        var req = {
            firstName: firstname, lastName: lastname, email: email, AssignedRoles: AssignedRolesList,

            StewardClaimsWorkflow: { Workflow: $("#ddlStewardWorkflow option:selected").text(), ActionGear: $('#cbClaimsSteward').is(':checked') },
            CollectorClaimsWorkflow: { Workflow: $("#ddlCollectorWorkflow option:selected").text(), ActionGear: $('#cbClaimsCollector').is(':checked') },
            HaulerClaimsWorkflow: { Workflow: $("#ddlHaulerWorkflow option:selected").text(), ActionGear: $('#cbClaimsHauler').is(':checked') },
            ProcessorClaimsWorkflow: { Workflow: $("#ddlProcessorWorkflow option:selected").text(), ActionGear: $('#cbClaimsProcessor').is(':checked') },
            RPMClaimsWorkflow: { Workflow: $("#ddlRPMWorkflow option:selected").text(), ActionGear: $('#cbClaimsRPM').is(':checked') },

            StewardApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingSteward').is(':checked'), ActionGear: $('#cbApplicationsActionSteward').is(':checked') },
            CollectorApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingCollector').is(':checked'), ActionGear: $('#cbApplicationsActionCollector').is(':checked') },
            HaulerApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingHauler').is(':checked'), ActionGear: $('#cbApplicationsActionHauler').is(':checked') },
            ProcessorApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingProcessor').is(':checked'), ActionGear: $('#cbApplicationsActionProcessor').is(':checked') },
            RPMApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingRPM').is(':checked'), ActionGear: $('#cbApplicationsActionRPM').is(':checked') }

        }

        var form = document.forms['frminvitation'];
        if (!$(form).valid()) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.SaveNewStaffUserUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    return false;
                }
                else {
                    clear();
                    window.location.reload(true);
                }
            },
            error: function (xhr) {
                //  alert(xhr.status + ' ' + xhr.statusText);
                window.location.reload(true);
                clear();
            }
        });
    });

    var pageReadonly = function () {
        //OTSTM2-882
        //var isPageReadonly = Global.InvitationIndex.PageReadOnly.toLowerCase();
        var isPageReadonly = Global.InvitationIndex.AdminUserPageReadOnly.toLowerCase();
        return isPageReadonly == 'true';
    }

    //global variable
    var selectedUser;
    var pageSize = 7; //OTSTM2-1013, fix "More" existing issue
    //Define data table
    var table = $('#tbluserlist').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.InvitationIndex.GetListHandlerUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY: 300,
        "sScrollX": "100%",
        "sScrollXInner": "110%",
        scrollCollapse: true,
        searching: true,
        ordering: true,
        order: [[4, "desc"]],
        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },

        columns: [
            {
                name: "firstName",
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + data.FirstName + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData,td)
                }
            },
            {
                name: "lastName",
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + data.LastName + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                className:'inviteEmail',
                name: "inviteEmail",
                data: null,
                render: function (data, type, full, meta) {
                    return  data.InviteEmail;
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                name: "invitedDate", //OTSTM2-1013, for existing issue, consistent with back end name
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + dateTimeConvert(data.InviteSendDate) + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                name: "displayStatus", //OTSTM2-1013, for existing issue, consistent with back end name
                className: "td-center",
                orderable: true,
                data: null,
                render: function (data, type, full, meta) {
                    if (data.DisplayStatus == "Inactive") {
                        return "<div class='panel-table-status color-tm-red-bg'>Inactive</div>";
                    }
                    if (data.DisplayStatus == "Active") {
                        return "<div class='panel-table-status color-tm-green-bg'>Active</div>";
                    }
                    if (data.DisplayStatus == "Invite Expired") {
                        return "<div class='panel-table-status color-tm-gray-light-bg'>Invite Expired</div>";
                    }
                    if (data.DisplayStatus == "Invite Sent") {
                        return "<div class='panel-table-status color-tm-blue-bg'>Invite Sent</div>";
                    }
                    //OTSTM2-1013
                    if (data.DisplayStatus == "Locked") {
                        return "<div class='panel-table-status color-tm-yellow-bg'>Locked</div>";
                    }
                    return "<div></div>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                className: "td-center",
                orderable: false,
                data: null,

                render: function (data, type, full, meta) {                   

                    if (!pageReadonly()) //Permission for Action Gear
                    {
                        //fix for safari
                        var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex='" + data.ID + "'" : "";
                        var content = "<div class='btn-group dropdown-menu-actions'><a href='javascript:void(0)' tabindex='" + data.ID + "' data-trigger='focus' data-toggle='popover'" +
                            " id='actions" + meta.row + "' data-original-title='' title=''><i class='glyphicon glyphicon-cog'></i></a><div class='popover-menu'><ul>";
                        if ((data.DisplayStatus == "Invite Sent") || (data.DisplayStatus == "Invite Expired")) {

                            content += " <li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalAddUserResendInviteConfirm' data-rowId='" + meta.row + "'><i class='fa fa-envelope-o'></i> Re-send Invite</a></li>" +
                                " </ul> </div> </div>";
                        }
                        if (data.DisplayStatus == "Active") {
                            content += "<li class='editOption'><a href='javascript:void(0)' data-toggle='modal' data-target='#modalEditUserNew' data-rowId='" + meta.row + "'><i class='fa fa-pencil-square-o'></i> Edit </a></li>" +
                                " <li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalInactivateUser' data-rowId='" + meta.row + "'><i class='fa fa-times'></i> Inactivate</a></li>" +
                                " </ul> </div> </div>";
                        }
                        if (data.DisplayStatus == "Inactive") {
                            content += "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalActivateUser' data-rowId='" + meta.row + "'><i class='fa fa-check'></i> Activate</a></li>" +
                              " </ul> </div> </div>";
                        }
                        //OTSTM2-1013
                        if (data.DisplayStatus == "Locked") {
                            content += "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalUnlockUser' data-rowId='" + meta.row + "'><i class='fa fa-unlock-alt'></i> Unlock</a></li>" +
                              " </ul> </div> </div>";
                        }
                        return content;
                    }
                    else
                    {
                        return "";
                    }
                }
            },
            {
                data: null,
                className: "display-none",
                render: function (data, type, full, meta) {
                    return data.RowversionString;
                }
            }
        ],
        initComplete: function (settings,json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#txtFoundRecords').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() > pageSize) ? $('#viewmore').css('visibility', 'visible') : $('#viewmore').css('visibility', 'hidden'); //OTSTM2-1013, for existing issue, cannot be ">="
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#Search').val();
            var url = Global.InvitationIndex.ExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----',searchValue);
            $('#export').attr('href', url);


        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });


        //disable edit option in Action Gear when Admin/Users-Add/Edit New User is No Access
        if (Global.InvitationIndex.Permission.DisableActionGearEditAndAddBtn) {
            $(".editOption").addClass("avoid-clicks");
        }
    });

    function statusClickEvent(data,td) {
        if (data.DisplayStatus.toLowerCase() == "active" || data.DisplayStatus.toLowerCase() == "inactive" || data.DisplayStatus.toLowerCase() == "locked") {
            $(td).prop("style", "cursor:pointer");
            $(td).on("click", function (event) {
                fillUserData(data.UserEmail);
                $("#modalViewUser").modal("show");
            });
        }
    }

    $('#viewmore').on('click', function () {
        $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $(this).hide();
    });

    ///////////

    $('#Search').on('keydown', function (e) {
        if (e.keyCode == 13) //End Home
        { e.preventDefault(); }
    });

    var myTimer;

    $('#Search').on('keyup', function (e) {
        //$('#export').attr('href', url);
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36) || (e.keyCode === 13)) //End Home
        { return; }
        var str = $(this).val();

        clearTimeout(myTimer);
        myTimer = setTimeout(searchIt, 500, str);
    });

    var searchIt = function (str) {
        if (str.length >= 1) {
            $("#Search").css('visibility', 'visible'); //OTSTM2-1013, $(this) is not correct selector
            $('#txtFoundRecords').css('display', 'block');
            $('#txtFoundRecords').siblings('.remove-icon').show();
            table.search(str).draw(false);
        }
        else {
            $('#txtFoundRecords').siblings('.remove-icon').hide();
            $('#txtFoundRecords').css('display', 'none');
            table.search(str).draw(false);
        }
    }

    $('#glyphicon-search-Rem').on('click', function () {
        var searchValue = $('#Search').val();
        table.search(searchValue).draw(false);
    });
    //on clear field click
    $('.remove-icon').click(function () {
        $(this).siblings("input").val("");
        $(this).hide();
        $('#txtFoundRecords').css('display', 'none');
        table.search("").draw(false);
    });

    //populate message to user
    $('#modalAddUserResendInviteConfirm').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedUser = table.row(rowId).data();
        var fullName = selectedUser.FirstName + ' ' + selectedUser.LastName;
        $(this).find('#spanName').text(fullName);
        $(this).find('#spanEmail').text(selectedUser.InviteEmail);
    });

    //send email to user	
    $('#btnResendInvite').on("click", function () {
        var email = $('#spanEmail').text();
        var req = { email: email }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.ResendInviteUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    $('#ajaxstatus').text(data.status);
                }
                else
                {
                    //window.location.reload(true);
                    table.search($('#Search').val()).draw(false); //OTSTM2-573
                }
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });
    
    //Edit user New
    $('#modalEditUserNew').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedUser = table.row(rowId).data();
        var email = selectedUser.InviteEmail;

        $.ajax({
            type: 'GET',
            url: Global.InvitationIndex.LoadStaffUserDetailsUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { emailAddress: [email] },
            traditional: true,
            success: function (result) {
                if (result.Status !== '') {
                    $('#edit_firstname-input').val(result.FirstName);
                    $('#edit_lastname-input').val(result.LastName);
                    $('#editEmailText').text(result.Email);
                 
                    ////////////

                    $('#multiselectEdit').empty();
                    var listAvailableRoles = $('#multiselectEdit');

                    $('#multiselectEdit_to').empty();
                    var listAssignedRoles = $('#multiselectEdit_to');
                    
                    //select.append('<option value="0">None</option>');                
                    //alert(JSON.stringify(result.responseJSON.AvailableRoles.length));

                    //populate Available roles
                    for (var i = 0; i < result.AvailableRoles.length; i++) {
                        listAvailableRoles.append(
                                $('<option/>', {
                                    value: result.AvailableRoles[i].Id,
                                    html: result.AvailableRoles[i].RoleName,
                                    Title: result.AvailableRoles[i].RoleName
                                })
                        );
                    }
                    
                    //populate Assigned roles
                    for (var i = 0; i < result.AssignedRoles.length; i++) {
                        listAssignedRoles.append(
                                $('<option/>', {
                                    value: result.AssignedRoles[i].Id,
                                    html: result.AssignedRoles[i].RoleName,
                                    Title: result.AssignedRoles[i].RoleName
                                })
                        );
                    }

                    //populate Workflow dropdown lists
                    var ddlStewardWorkflow = $('#ddlStewardWorkflowEdit');
                    var ddlCollectorWorkflow = $('#ddlCollectorWorkflowEdit');
                    var ddlHaulerWorkflow = $('#ddlHaulerWorkflowEdit');
                    var ddlProcessorWorkflow = $('#ddlProcessorWorkflowEdit');
                    var ddlRPMWorkflow = $('#ddlRPMWorkflowEdit');

                    $('#ddlStewardWorkflowEdit').empty();
                    $('#ddlCollectorWorkflowEdit').empty();
                    $('#ddlHaulerWorkflowEdit').empty();
                    $('#ddlProcessorWorkflowEdit').empty();
                    $('#ddlRPMWorkflowEdit').empty();

                    for (var i = 0; i < result.StewardWorkflowList.length; i++) {
                        ddlStewardWorkflow.append(
                                $('<option/>', {
                                    value: result.StewardWorkflowList[i],
                                    html: result.StewardWorkflowList[i],
                                    title: result.StewardWorkflowList[i],
                                })
                        );
                    }

                    for (var i = 0; i < result.WorkflowList.length; i++) {                        

                        ddlCollectorWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlHaulerWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlProcessorWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlRPMWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );
                    }

                 
                    $("#ddlStewardWorkflowEdit").val(result.StewardClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlCollectorWorkflowEdit").val(result.CollectorClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlHaulerWorkflowEdit").val(result.HaulerClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlProcessorWorkflowEdit").val(result.ProcessorClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlRPMWorkflowEdit").val(result.RPMClaimsWorkflow.Workflow).attr("selected", "selected");;

                    $('#cbClaimsStewardEdit').prop('checked', result.StewardClaimsWorkflow.ActionGear);
                    $('#cbClaimsCollectorEdit').prop('checked', result.CollectorClaimsWorkflow.ActionGear);
                    $('#cbClaimsHaulerEdit').prop('checked', result.HaulerClaimsWorkflow.ActionGear);
                    $('#cbClaimsProcessorEdit').prop('checked', result.ProcessorClaimsWorkflow.ActionGear);
                    $('#cbClaimsRPMEdit').prop('checked', result.RPMClaimsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingStewardEdit').prop('checked', result.StewardApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionStewardEdit').prop('checked', result.StewardApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingCollectorEdit').prop('checked', result.CollectorApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionCollectorEdit').prop('checked', result.CollectorApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingHaulerEdit').prop('checked', result.HaulerApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionHaulerEdit').prop('checked', result.HaulerApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingProcessorEdit').prop('checked', result.ProcessorApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionProcessorEdit').prop('checked', result.ProcessorApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingRPMEdit').prop('checked', result.RPMApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionRPMEdit').prop('checked', result.RPMApplicationsWorkflow.ActionGear);
                    selectedUser = result;
                }
            },
            error: function (result) {
            }
        });
        //Apply permission for editing
        var rolesPermission = Global.InvitationIndex.Permission.RolesPermission;
        var routingWorkflowClaimsPermission = Global.InvitationIndex.Permission.RoutingWorkflowClaimsPermission;
        var routingWorkflowRemittancesPermission = Global.InvitationIndex.Permission.RoutingWorkflowRemittancesPermission
        var routingWorkflowApplicationsPermission = Global.InvitationIndex.Permission.RoutingWorkflowApplicationsPermission
        if (rolesPermission != "EditSave" && routingWorkflowClaimsPermission != "EditSave" && routingWorkflowRemittancesPermission != "EditSave" && routingWorkflowApplicationsPermission != "EditSave") {
            $("#edit_firstname-input").addClass("disabled").prop("disabled", true);
            $("#edit_lastname-input").addClass("disabled").prop("disabled", true);            
            $("#btneditusernext").addClass("disabled").prop("disabled", true);
        }
        if (rolesPermission == "ReadOnly") {
            $("#multiselectEdit_rightSelected").css("pointer-events", "none");
            $("#multiselectEdit_rightSelected > i").css("color", "lightblue");
            $("#multiselectEdit_leftSelected").css("pointer-events", "none");
            $("#multiselectEdit_leftSelected > i").css("color", "lightblue");
            $("#multiselectEdit").addClass("disabled").prop("disabled", true);
            $("#multiselectEdit_to").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowClaimsPermission == "ReadOnly") {           
            $("#ddlCollectorWorkflowEdit").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelCollector = $("#ddlCollectorWorkflowEdit option:selected");
            $("#ddlCollectorWorkflowEdit").change(function () {
                lastSelCollector.prop("selected", true);
            });
            $("#ddlCollectorWorkflowEdit").click(function () {
                lastSelCollector = $("#ddlCollectorWorkflowEdit option:selected");
            });
            $("#ddlHaulerWorkflowEdit").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelHauler = $("#ddlHaulerWorkflowEdit option:selected");
            $("#ddlHaulerWorkflowEdit").change(function () {
                lastSelHauler.prop("selected", true);
            });
            $("#ddlHaulerWorkflowEdit").click(function () {
                lastSelHauler = $("#ddlHaulerWorkflowEdit option:selected");
            });
            $("#ddlProcessorWorkflowEdit").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelProcessor = $("#ddlProcessorWorkflowEdit option:selected");
            $("#ddlProcessorWorkflowEdit").change(function () {
                lastSelProcessor.prop("selected", true);
            });
            $("#ddlProcessorWorkflowEdit").click(function () {
                lastSelProcessor = $("#ddlProcessorWorkflowEdit option:selected");
            });
            $("#ddlRPMWorkflowEdit").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelRPM = $("#ddlRPMWorkflowEdit option:selected");
            $("#ddlRPMWorkflowEdit").change(function () {
                lastSelRPM.prop("selected", true);
            });
            $("#ddlRPMWorkflowEdit").click(function () {
                lastSelRPM = $("#ddlRPMWorkflowEdit option:selected");
            });

            $("#cbClaimsCollectorEdit").addClass("disabled").prop("disabled", true);
            $("#cbClaimsHaulerEdit").addClass("disabled").prop("disabled", true);
            $("#cbClaimsProcessorEdit").addClass("disabled").prop("disabled", true);
            $("#cbClaimsRPMEdit").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowRemittancesPermission == "ReadOnly") { 
            $("#ddlStewardWorkflowEdit").css("cursor", "not-allowed").css("background-color", "whitesmoke");
            var lastSelRemittance = $("#ddlStewardWorkflowEdit option:selected");
            $("#ddlStewardWorkflowEdit").change(function () {
                lastSelRemittance.prop("selected", true);
            });
            $("#ddlStewardWorkflowEdit").click(function () {
                lastSelRemittance = $("#ddlStewardWorkflowEdit option:selected");
            });

            $("#cbClaimsStewardEdit").addClass("disabled").prop("disabled", true);
        }
        if (routingWorkflowApplicationsPermission == "ReadOnly") {
            $("#cbApplicationsRoutingStewardEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionStewardEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingCollectorEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionCollectorEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingHaulerEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionHaulerEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingProcessorEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionProcessorEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsRoutingRPMEdit").addClass("disabled").prop("disabled", true);
            $("#cbApplicationsActionRPMEdit").addClass("disabled").prop("disabled", true);
        }

        if (routingWorkflowClaimsPermission == "NoAccess" && routingWorkflowRemittancesPermission == "NoAccess") {
            $("#modalEditUserNew").find(".verticalLine").removeClass("verticalLine");
        }
    });
    //view user
    function fillUserData(email) {       
        $.ajax({
            type: 'GET',
            url: Global.InvitationIndex.LoadStaffUserDetailsUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { emailAddress: email},
            traditional: true,
            success: function (result) {
                if (result.Status !== '') {
                    $('#view_firstname-input').val(result.FirstName);
                    $('#view_lastname-input').val(result.LastName);
                    $('#viewEmailText').text(result.Email);

                    ////////////

                    $('#multiselectView').empty();
                    var listAvailableRoles = $('#multiselectView');

                    $('#multiselectView_to').empty();
                    var listAssignedRoles = $('#multiselectView_to');

                    //select.append('<option value="0">None</option>');                
                    //alert(JSON.stringify(result.responseJSON.AvailableRoles.length));

                    //populate Available roles
                    for (var i = 0; i < result.AvailableRoles.length; i++) {
                        listAvailableRoles.append(
                                $('<option/>', {
                                    value: result.AvailableRoles[i].Id,
                                    html: result.AvailableRoles[i].RoleName,
                                    Title: result.AvailableRoles[i].RoleName
                                })
                        );
                    }

                    //populate Assigned roles
                    for (var i = 0; i < result.AssignedRoles.length; i++) {
                        listAssignedRoles.append(
                                $('<option/>', {
                                    value: result.AssignedRoles[i].Id,
                                    html: result.AssignedRoles[i].RoleName,
                                    Title: result.AssignedRoles[i].RoleName
                                })
                        );
                    }

                    //populate Workflow dropdown lists
                    var ddlStewardWorkflow = $('#ddlStewardWorkflowView');
                    var ddlCollectorWorkflow = $('#ddlCollectorWorkflowView');
                    var ddlHaulerWorkflow = $('#ddlHaulerWorkflowView');
                    var ddlProcessorWorkflow = $('#ddlProcessorWorkflowView');
                    var ddlRPMWorkflow = $('#ddlRPMWorkflowView');

                    $('#ddlStewardWorkflowView').empty();
                    $('#ddlCollectorWorkflowView').empty();
                    $('#ddlHaulerWorkflowView').empty();
                    $('#ddlProcessorWorkflowView').empty();
                    $('#ddlRPMWorkflowView').empty();

                    for (var i = 0; i < result.StewardWorkflowList.length; i++) {
                        ddlStewardWorkflow.append(
                                $('<option/>', {
                                    value: result.StewardWorkflowList[i],
                                    html: result.StewardWorkflowList[i],
                                    title: result.StewardWorkflowList[i],
                                })
                        );
                    }

                    for (var i = 0; i < result.WorkflowList.length; i++) {

                        ddlCollectorWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlHaulerWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlProcessorWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );

                        ddlRPMWorkflow.append(
                                $('<option/>', {
                                    value: result.WorkflowList[i],
                                    html: result.WorkflowList[i],
                                    title: result.WorkflowList[i],
                                })
                        );
                    }


                    $("#ddlStewardWorkflowView").val(result.StewardClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlCollectorWorkflowView").val(result.CollectorClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlHaulerWorkflowView").val(result.HaulerClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlProcessorWorkflowView").val(result.ProcessorClaimsWorkflow.Workflow).attr("selected", "selected");;
                    $("#ddlRPMWorkflowView").val(result.RPMClaimsWorkflow.Workflow).attr("selected", "selected");;

                    $('#cbClaimsStewardView').prop('checked', result.StewardClaimsWorkflow.ActionGear);
                    $('#cbClaimsCollectorView').prop('checked', result.CollectorClaimsWorkflow.ActionGear);
                    $('#cbClaimsHaulerView').prop('checked', result.HaulerClaimsWorkflow.ActionGear);
                    $('#cbClaimsProcessorView').prop('checked', result.ProcessorClaimsWorkflow.ActionGear);
                    $('#cbClaimsRPMView').prop('checked', result.RPMClaimsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingStewardView').prop('checked', result.StewardApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionStewardView').prop('checked', result.StewardApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingCollectorView').prop('checked', result.CollectorApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionCollectorView').prop('checked', result.CollectorApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingHaulerView').prop('checked', result.HaulerApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionHaulerView').prop('checked', result.HaulerApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingProcessorView').prop('checked', result.ProcessorApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionProcessorView').prop('checked', result.ProcessorApplicationsWorkflow.ActionGear);

                    $('#cbApplicationsRoutingRPMView').prop('checked', result.RPMApplicationsWorkflow.Routing);
                    $('#cbApplicationsActionRPMView').prop('checked', result.RPMApplicationsWorkflow.ActionGear);
                    selectedUser = result;
                }
            },
            error: function (result) {
            }
        });      
    }
    // Edit user next click case
    $("#btneditusernext").click(function () {
        //$('#customMessage').html(" test <strong>Andy<strong>");        
        $('#editUserConfirmMsg').show();
        var form = document.forms['frminvitation'];
        //var trigger = $(event.relatedTarget); //comment out for OTSTM2-1045, because it does not work on Firefox
        jQuery.ajaxSetup({ async: false });

        if (!$(form).valid()) {
            if ($("#hasRolePanelEdit").length) {
                if ($('#multiselectEdit_to option').length == 0) {
                    jQuery("label#selectEdit_error").show(); // show Warning 
                    jQuery("select#multiselectEdit_to").focus();  // Focus the select box      
                }
            }           
            return false;
        }
        //Assigned roles list if empty
        if ($("#hasRolePanelEdit").length) { //check if empty only when there is Role panel
            if ($('#multiselectEdit_to option').length == 0) {
                jQuery("label#selectEdit_error").show(); // show Warning 
                jQuery("select#multiselectEdit_to").focus();  // Focus the select box      
                return false;
            }
        }
        
        if ($("#hasRolePanelEdit").length && $("#hasStewardWorkflowPanelEdit").length && $("#hasClaimWorkflowPanelEdit").length && $("#hasApplicationPanelEdit").length) {
            $('#modalEditUserNew').modal('hide');
            $('#modalEditUserNewConfirm').modal('show');
        }
        else {
            $('#modalEditUserNew').modal('hide');
            $('#modalWarningMessage').modal('show');
        }

        return true;
    });

    $('#btneditusernewconfirm').on("click", function () {

        var email = selectedUser.Email;
        var rowversionstring = selectedUser.RowversionString;


        var firstname = $('#edit_firstname-input').val();
        var lastname = $('#edit_lastname-input').val();
        
        var AssignedRolesList = [];
        $('#multiselectEdit_to option').each(function () {
            AssignedRolesList.push({
                Id: this.value,
                RoleName: this.text
            });
        });

        var req = {
            firstName: firstname, lastName: lastname, email: email, AssignedRoles: AssignedRolesList,

            StewardClaimsWorkflow: { Workflow: $("#ddlStewardWorkflowEdit option:selected").text(), ActionGear: $('#cbClaimsStewardEdit').is(':checked') },
            CollectorClaimsWorkflow: { Workflow: $("#ddlCollectorWorkflowEdit option:selected").text(), ActionGear: $('#cbClaimsCollectorEdit').is(':checked') },
            HaulerClaimsWorkflow: { Workflow: $("#ddlHaulerWorkflowEdit option:selected").text(), ActionGear: $('#cbClaimsHaulerEdit').is(':checked') },
            ProcessorClaimsWorkflow: { Workflow: $("#ddlProcessorWorkflowEdit option:selected").text(), ActionGear: $('#cbClaimsProcessorEdit').is(':checked') },
            RPMClaimsWorkflow: { Workflow: $("#ddlRPMWorkflowEdit option:selected").text(), ActionGear: $('#cbClaimsRPMEdit').is(':checked') },

            StewardApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingStewardEdit').is(':checked'), ActionGear: $('#cbApplicationsActionStewardEdit').is(':checked') },
            CollectorApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingCollectorEdit').is(':checked'), ActionGear: $('#cbApplicationsActionCollectorEdit').is(':checked') },
            HaulerApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingHaulerEdit').is(':checked'), ActionGear: $('#cbApplicationsActionHaulerEdit').is(':checked') },
            ProcessorApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingProcessorEdit').is(':checked'), ActionGear: $('#cbApplicationsActionProcessorEdit').is(':checked') },
            RPMApplicationsWorkflow: { Routing: $('#cbApplicationsRoutingRPMEdit').is(':checked'), ActionGear: $('#cbApplicationsActionRPMEdit').is(':checked') }
        }

        var form = document.forms['frminvitation'];
        if (!$(form).valid()) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.EditStaffUserUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    localStorage.setItem("error", data.status);

                    table.search($('#Search').val()).draw(false); //OTSTM2-573
                    //window.location.reload(true);
                } else {
                    localStorage.removeItem('error');

                    table.search($('#Search').val()).draw(false); //OTSTM2-573
                    //window.location.reload(true);
                }
            },
            error: function (xhr) {
            }
        });
    });    

    //Inactive user
    $('#modalInactivateUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedUser = table.row(rowId).data();
        $(this).find('#inactiveuser').text(selectedUser.FirstName + ' ' + selectedUser.LastName);
    });  

    $('#btninactive').on("click", function () {
        var req = { email: selectedUser.InviteEmail }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.InactiveUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                //window.location.reload(true);
                table.search($('#Search').val()).draw(false); //OTSTM2-573
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });

    //Active user
    $('#modalActivateUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedUser = table.row(rowId).data();
        $(this).find('#activeuser').text(selectedUser.FirstName + ' ' + selectedUser.LastName);
    });

    $('#btnactiveuser').on("click", function () {
        var req = { email: selectedUser.InviteEmail }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.ActiveUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                //window.location.reload(true);
                table.search($('#Search').val()).draw(false); //OTSTM2-573
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });

    //OTSTM2-1013 unlock user
    $('#modalUnlockUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedUser = table.row(rowId).data();
        $(this).find('#unlockuser').text(selectedUser.InviteEmail);
    });

    $('#btnunlockuser').on("click", function () {
        var req = { email: selectedUser.InviteEmail }
        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.UnlockStaffUserUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                //window.location.reload(true);
                table.search($('#Search').val()).draw(false); //OTSTM2-573
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });
 
    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        if (data.indexOf('-62135578800000') > -1) return '';

        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return data.substring(0, 10);
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        //var curr_h = c.getHours();
        //var curr_m = c.getMinutes();
        //var curr_s = c.getSeconds();
        //var curr_offset = c.getTimezoneOffset() / 60;
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    }
})

var adminStaffUserApp = angular.module("adminStaffUserApp",
    ["commonLib", "ui.bootstrap", "commonActivitiesLib"]);