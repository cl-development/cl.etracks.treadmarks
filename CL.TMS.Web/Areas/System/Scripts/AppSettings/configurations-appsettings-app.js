﻿'use strict';

var Security = {
    isReadonly: function (resource) {
        return resource === Global.AppSettings.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.AppSettings.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.AppSettings.Security.Constants.NoAccess
    },
    checkUserSecurity: function (type) {
        return Global.AppSettings.Security.AdminApplicationSettings;
    }
}

//Configurations Rate App
var configurationsAppSettingsApp = angular.module("configurationsAppSettingsApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller", "configurationAppSettings.directives", "configurationAppSettings.controllers",
        "configurationAppSettings.services", "commonClaimInternalNotesLib", "ngMessages"]);

//Controllers Section
var controllers = angular.module('configurationAppSettings.controllers', []);
controllers.controller('configurationAppSettingsController', ['$rootScope', '$scope', '$http', '$uibModal', '$timeout', '$window', 'configurationAppSettingsServices', function ($rootScope, $scope, $http, $uibModal, $timeout, $window, configurationAppSettingsServices) {
    var resource = Security.checkUserSecurity();
    //$scope.emailRequire = 'Email is required.';
    //$scope.emailFormat = 'Must be a valid email.';
    $scope.valueRequire = 'Value is required.';
    $scope.valueMin = 'Value must greater then 0.';
    $scope.valueMustEqualOrGreaterThan = 'Purge Days must be equal or greater than Application Invitation Expiry Days.';
    $scope.valueMustLessThan = '';
    $scope.valueMustGreaterThan = 'Payment days must be greater than Approval days.';
    $scope.disableSaveBtn = false;
    if (Security.isReadonly(resource)) {//true: disable Add Notes
        $scope.disableNoteBtn = true;
        $scope.disableSaveBtn = true;
        $scope.disableEdit = true;
    }
    if (Security.isEditSave(resource)) {
        $scope.addNoteUrl = Global.InternalNoteSettings.addNoteUrl;
    }
    configurationAppSettingsServices.loadAppSettings('load it please').then(function (result) {
        $scope.appSettingsVM = result.data;
        $scope.appSettingsVM.Settings_UserDate = moment($scope.appSettingsVM.Settings_UserDate).format("YYYY-MM-DD");
        InitialFormData();
        if ($scope.disableSaveBtn) {//read only user
            $('button').prop("disabled", true);
            $('input.form-control:not("#search")').prop("disabled", true);//disable input[text, number]
            $('input[type=checkbox]').prop("disabled", true);//disable check box
            $('#dpBusinessStartDate').prop("disabled", true).addClass("disable-click");//disable dp picker
        }
        $scope.appSettingsForm.$setSubmitted();
    });
    $scope.loadUrl = Global.InternalNoteSettings.loadUrl;
    $scope.exportUrl = Global.InternalNoteSettings.exportUrl;
    $scope.rateTransactionID = 'AppSetting';
    console.log('$scope.appSettingsForm:', $scope.appSettingsForm, $scope.appSettingsForm.$dirty, $scope.appSettingsForm.$pristine, $scope.appSettingsForm.$valid);
    $scope.saveAppSettingValue = function () {
        if (!$scope.appSettingsForm.$valid) {
            $("html, body").animate({ scrollTop: 80 }, "slow");
            //document.getElementById("cbEmail_CB_CCEmailClaimAndApplication").scrollIntoView();
            return;
        }
        var confirmSaveModal = $uibModal.open({
            templateUrl: 'appsetting-save-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmSaveMessage = Global.AppSettings.ConfirmSaveHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $timeout(function () {//trigger add note button, save current notes text
                        angular.element('#appsettings-viewdetail-notes > div > button').trigger('click');
                    }, 0);

                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                rateTransactionID: function () {
                    return $scope.ratetransactionid;
                },
                rateBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                rateType: function () {
                    return $scope.type;
                }
            }
        });

        confirmSaveModal.result.then(function () {
            var data = $scope.appSettingsVM;
            configurationAppSettingsServices.UpdateAppSettings(data).then(function (response) {
                if (response && response.data) {
                    $scope.appSettingsForm.$setPristine();
                    $scope.appSettingsForm.$setUntouched();
                    console.log('appSettingsForm.$pristine:', $scope.appSettingsForm.$pristine, $scope.disableSaveBtn, $scope.appSettingsForm.$invalid);
                    console.log('form obj:', $scope.appSettingsForm);
                    $window.location.reload(true);
                } else {//return error
                    var failSaveModal = $uibModal.open({
                        templateUrl: 'data-saving-fail-modal.html',
                        controller: 'dataSavingFailedCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.AppSettings.SaveFailHeader },
                            failResult: function () {
                                return response && response.data ? response.data.statusMsg : Global.AppSettings.SaveFailText;
                            }
                        }
                    });
                }
            });
        }, function () {
        });
    };

    $scope.cancelEdit = function () {
        var confirmSaveModal = $uibModal.open({
            templateUrl: 'appsetting-cancel-edit-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmCancelMessage = Global.AppSettings.ConfirmCancelHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                    //$scope.$parent.appSettingsForm.$rollbackViewValue();
                    $window.location.reload(true);//don't reload, just reset() if possible
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                rateTransactionID: function () {
                    return $scope.ratetransactionid;
                },
                rateBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                rateType: function () {
                    return $scope.type;
                }
            }
        });
    };

    $scope.phoneNumberPattern =(function () {
        var regexp = /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
        return {
            test: function (value) {
            return regexp.test(value);
        }
    };
    })();

    //$scope.emailPattern = (function (value) {
    //    var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,4}))$/;
    //    return {
    //        test: function (value) {
    //            return regexp.test(value);
    //        }
    //    };
    //})();

    $scope.valueMustEqualOrGreater = function (a, b) {
        if (!a || !b) {
            return;
        }

        if ($scope.appSettingsForm.Application_InvitationExpiryDays) {
            $scope.appSettingsForm.Application_InvitationExpiryDays.$setDirty();
        }
        if ($scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays) {
            $scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays.$setDirty();
        }
        if (b < a) {
            if ($scope.appSettingsForm.Application_InvitationExpiryDays) {
                $scope.appSettingsForm.Application_InvitationExpiryDays.$setValidity('customValidationEqualOrGreater', false);
            }
            if ($scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays) {
                $scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays.$setValidity('customValidationEqualOrGreater', false);
            }

        } else {
            if ($scope.appSettingsForm.Application_InvitationExpiryDays) {
                $scope.appSettingsForm.Application_InvitationExpiryDays.$setValidity('customValidationEqualOrGreater', true);
            }
            if ($scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays) {
                $scope.appSettingsForm.Application_PurgeOpenApplicationAfterDays.$setValidity('customValidationEqualOrGreater', true);
            }
        }
    };

    $scope.valueMustGreater = function (a, b) {
        if (!a || !b) {
            return;
        }

        if ($scope.appSettingsForm.Claims_ReviewDueDays) {
            $scope.appSettingsForm.Claims_ReviewDueDays.$setDirty();
        }
        if ($scope.appSettingsForm.Claims_ChequeDueDays) {
            $scope.appSettingsForm.Claims_ChequeDueDays.$setDirty();
        }
        if (!(b > a)) {
            if ($scope.appSettingsForm.Claims_ReviewDueDays) {
                $scope.appSettingsForm.Claims_ReviewDueDays.$setValidity('customValidationGreater', false);
            }
            if ($scope.appSettingsForm.Claims_ChequeDueDays) {
                $scope.appSettingsForm.Claims_ChequeDueDays.$setValidity('customValidationGreater', false);
            }

        } else {
            if ($scope.appSettingsForm.Claims_ReviewDueDays) {
                $scope.appSettingsForm.Claims_ReviewDueDays.$setValidity('customValidationGreater', true);
            }
            if ($scope.appSettingsForm.Claims_ChequeDueDays) {
                $scope.appSettingsForm.Claims_ChequeDueDays.$setValidity('customValidationGreater', true);
            }
        }
    };

    //for popover
    var servicelLevelPopover = {
        content: Global.Settings.collectorClaimsContent
    };

    $('.fa-question-circle').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, servicelLevelPopover));

}]);

controllers.controller('dataSavingFailedCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', '$window', function ($scope, $uibModal, $uibModalInstance, failResult, header, $window) {
    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $window.location.reload(true);
    };
}]);

//Services Section
var services = angular.module('configurationAppSettings.services', []);
services.factory('configurationAppSettingsServices', ["$http", function ($http) {
    var factory = {};

    factory.anchorCss = function () {
        return {
            "display": "block",
            "padding": "3px 20px",
            "clear": "both",
            "font-weight": "400",
            "line-height": "1.42857143",
            "color": "#000",
            "white-space": "nowrap"
        }
    }

    factory.UpdateAppSettings = function (data) {
        var submitVal = {
            url: Global.AppSettings.UpdateAppSettingsURL,
            method: "POST",
            data: { appSettingsVM: data }
        }
        return $http(submitVal);
    }

    factory.loadAppSettings = function (param) {
        return $http({
            method: 'GET',
            url: Global.AppSettings.loadAppSettingsUrl,
            params: {
                param: param,
            }
        });
    }

    return factory;
}]);

//Directives Section
var directives = angular.module('configurationAppSettings.directives', []);
directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

// private functions
$('#dpBusinessStartDate').datetimepicker({
    minView: 3,//don't show day
    viewMode: "year",
    startView: 'year',
    minViewMode: "months",
    showOn: 'focus',
    autoclose: true,
    //startDate: "2009-09-01",
    format: 'yyyy-mm-dd',
    //endDate: new Date(),
});

function InitialFormData() {
    //$("#Settings_UserDate").mask("9999/99/99", { placeholder: "yyyy/mm/dd"});
    jQuery(':input[type="number"]').keydown(function (e) {
        if ((e.keyCode === 190) || (e.keyCode === 110)) {
            return false;
        }
    });
    jQuery(':input[type="number"]').keyup(function (e) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $("#Settings_UserDate").keydown(function (event) {
        return false;
    });
}

//$(window).bind('beforeunload', function () {
//    return 'Are you sure you want to leave?';
//});