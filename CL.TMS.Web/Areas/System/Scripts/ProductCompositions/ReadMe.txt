﻿https://jira.centrilogic.com/jira/browse/OTSTM2-1059

Note of the noUiSlider.
1. This slider support "start, slide, update, change, set, end" event. 

2. Click on the slider will fire the "update, slide, set, change" event. Don't have "start, end"

so in click event, there is not the start and end value of this slider, only have the current click on point value. 
if you need the start point value, you need custom the event.

3. Drag the handle will fire "start" once, 
then "update, slide" event keeping the status change, 
when the drag stop (released the handle), it will fire "set, change and end" event once.

So these three "start, slide or update, end" events can present the handle status changing from "start, moving to end".