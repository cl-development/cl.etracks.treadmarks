﻿'use strict';

var Security = {
    isReadonly: function (resource) {
        return resource === Global.Rate.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.Rate.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.Rate.Security.Constants.NoAccess
    },
    checkUserSecurity: function (type) {
        switch (type) {
            case "1":
            case "1 ": //OTSTM2-1061, don't know yet why the type(category) has a white space after value
            case 1:
                return Global.Rate.Security.TireStewardshipFeesRates;                
            case "2":
            case "2 ":
            case 2:
                return Global.Rate.Security.CollectionAllowancesRates;                
            case "3":
            case "3 ":
            case 3:
                return Global.Rate.Security.TransportationIncentivesRate;                
            case "4":
            case "4 ":
            case 4:
                return Global.Rate.Security.ProcessingIncentivesRates;                
            case "5":
            case "5 ":
            case 5:
                return Global.Rate.Security.ManufacturingIncentivesRates;                
                //case "6":
                //case 6:
                //    return Global.Rate.Security.EstimatedWeights;
                //    break;
            case "7":
            case "7 ":
            case 7:
                return Global.Rate.Security.RemittancePenaltyRates;                
            default:
                return Global.Rate.Security.RatePermission;//value can be "Custom"                
        }
    }
}

//Configurations Rate App
var configurationsRateApp = angular.module("configurationsRateApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller", "configurationRate.directives", "configurationRate.controllers", "configurationRate.services", "commonClaimInternalNotesLib", "commonActivitiesLib"]);

//Controllers Section
var controllers = angular.module('configurationRate.controllers', []);
controllers.controller('configurationRateController', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);

//Common Controllers
controllers.controller('rateSuccessCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'successResult', 'header', 'body', function ($scope, $uibModal, $uibModalInstance, successResult, header, body) {

    $scope.sucessMessage = body;
    $scope.successHeader = header;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('rateFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'message', 'header', function ($scope, $uibModal, $uibModalInstance, message, header) {
    $scope.message = message;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Add New Rate Controller
controllers.controller('addNewRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'configurationRateServices', '$rootScope', 'tableIdTmp', function ($scope, $http, $uibModal, $uibModalInstance, category, configurationRateServices, $rootScope, tableIdTmp) {
    $scope.isValid = { note: true, noteIsRequire: true, effectiveStartDate: true };
    $scope.isView = false;
    $scope.isEdit = false;
    $scope.isAddNew = true;

    configurationRateServices.getRateTransactionByID(0, category).then(function (data) {//0:initial a new Rate
        $scope.rateVM = rateViewModel(data.data, category);
        $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        if ((category+"").trim() === '2') { //OTSTM2-1061, don't know yet why the type(category) has a white space after value
            InitialDatePicker(data.data.collectorAllowanceRate.selectableTransactionPeriods);
        } else {
            triggerDateTimePicker(category);
        }
        if (category == Global.Rate.RateCategory.TireStewardshipFeesRates.rateCategoryID) {
            $.each($('.fa-question-circle'), function (index, item) {
                var tempName = $(item).attr("item-id") + "Name";
                var tempContent = "";
                $.each($scope.rateVM.tireStewardshipFeesRate, function (key, name) {
                    if (key == tempName) {
                        //tempContent = name.split(":")[1];
                        tempContent = name; //OTSTM2-1065 display whole item name
                        return false;
                    }
                });
                var trieStewareshipFeeSettings = {
                    content: tempContent,
                };
                $(item).webuiPopover('destroy')
                    .webuiPopover($.extend({}, Global.Settings.popover, trieStewareshipFeeSettings));

            });
        }
    });

    //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id");
    $scope.tableId = tableIdTmp; //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
    $scope.confirm = function () {
        $scope.disableBtn = true;
        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1061
        $uibModalInstance.close(response.data.result);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };

    // Start of Add New Rate
    $scope.submitNewRate = function (e) {
        $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
        var timestamp = Date.parse(moment($scope.rateVM.effectiveDate));

        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        $scope.isValid.effectiveStartDate = true;
        if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {
            $scope.isValid.effectiveStartDate = false;
        } else {
            $scope.rateVM.effectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.rateVM.effectiveDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.rateVM.notes = [];
        }

        if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            return;
        }
        var confirmAddNewModal = $uibModal.open({
            templateUrl: 'rate-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                if (category == Global.Rate.RateCategory.RemittancePenaltyRates.rateCategoryID) {
                    $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml.replace("Rates", "Remittance Penalty Rate").replace("these", "this");
                } else if (category == Global.Rate.RateCategory.TireStewardshipFeesRates.rateCategoryID) {
                    $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml.replace("Rates", "TSF Fees");
                    //} else if (category == Global.Rate.RateCategory.EstimatedWeightsRates.rateCategoryID) {
                    //    $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml.replace("Rates", "Estimated Weights");
                } else {
                    $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml;
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                rateTransactionID: function () {
                    return $scope.ratetransactionid;
                },
                rateBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                rateType: function () {
                    return $scope.category;
                }
            }
        });

        confirmAddNewModal.result.then(function (e) {
            var data = ratePostModel($scope.rateVM);
            configurationRateServices.AddNewRate(data, category).then(function (response) {
                if (response.data.status) {
                    if (category == Global.Rate.RateCategory.ProcessingIncentivesRates.rateCategoryID) {
                        if (response.data.isFutureGlobalRateExists && response.data.isFutureSpecificRateExists) {
                            $("#addNewRateBtn" + category).prop("disabled", true); //OTSTM2-1061 may not work in Pro
                        }
                        $("#hasGlobalRate").val(response.data.isFutureGlobalRateExists);

                        if (response.data.isFutureGlobalRateExists) {
                            $("#addNewPIRateBtn" + category).attr("style", "background-color:#808080;"); //OTSTM2-1061 may not work in Pro
                        }
                    } else {
                        $("#addNewRateBtn" + category).prop("disabled", true); //OTSTM2-1061 may not work in Pro
                    }
                    $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, true); //OTSTM2-1061, new way to enable/disable + button
                    $uibModalInstance.close(true);
                } else {
                    $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    var failSaveModal = $uibModal.open({
                        templateUrl: 'rate-fail-modal.html',
                        controller: 'rateFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return "Warning"; },
                            message: function () { return Global.Rate.AddNewRateFailHtml; } //response && response.data ? response.data.statusMsg :
                        }
                    });
                };
            });
        }, function () {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        });
    };
}]);
controllers.controller('addNewTIRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'configurationRateServices', '$rootScope', 'tableIdTmp', function ($scope, $http, $uibModal, $uibModalInstance, category, configurationRateServices, $rootScope, tableIdTmp) {
    $scope.isValid = { note: true, noteIsRequire: true, effectiveStartDate: true };
    $scope.isView = false;
    $scope.isEdit = false;
    $scope.isAddNew = true;
    $scope.disableAddRateBtn = true;
    $scope.reloadBtn = false;
    $scope.rateVM = {};
    configurationRateServices.getRateTransactionByID(0, category).then(function (data) {//rateTransactionID : 0 => initial a new Rate
        $scope.rateVM = rateViewModel(data.data);
        $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        triggerDateTimePicker(category);

        //1.1 pickup group dropdown list
        var pickupRateGroup = $('#pickupRateGroup');
        fillMultiples(pickupRateGroup, data.data.tiVM.TIRateGroupList.pickupRateGroups);
        $('#pickupRateGroup').select2({ placeholder: 'Select Pickup Rate Group...' });
        $('#pickupRateGroup').val({}).trigger('change');

        //1.2 delivery group dropdown list
        var deliveryRateGroup = $('#deliveryRateGroup');
        fillMultiples(deliveryRateGroup, data.data.tiVM.TIRateGroupList.deliveryRateGroups);
        $('#deliveryRateGroup').select2({ placeholder: 'Select Delivery Rate Group...' });
        $('#deliveryRateGroup').val({}).trigger('change');

        //2 pickup/delivery select event
        $('#pickupRateGroup').on("select2:select", function (e) {
            $scope.loadTIRateTable($('#pickupRateGroup').val(), $('#deliveryRateGroup').val());
        });
        $('#deliveryRateGroup').on("select2:select", function (e) {
            $scope.loadTIRateTable($('#pickupRateGroup').val(), $('#deliveryRateGroup').val());
        });
    });

    $scope.loadTIRateTable = function (pickupGroupID, deliveryGroupID) {
        if (pickupGroupID && deliveryGroupID) {
            // 2.1 disable the dropdown list
            $('#pickupRateGroup').select2('enable', false);
            $('#deliveryRateGroup').select2('enable', false);
            // 2.2 load ti rate content
            configurationRateServices.getTIRates(pickupGroupID, deliveryGroupID).then(function (data) {
                // 2.2 built ti rate table               
                $scope.rateVM.tiVM = rateTIViewModel(data.data, $scope.rateVM.decimalsize);
                $scope.rateVM.tiVM.category = category;
                // 3 show ti rate content
                $scope.reloadBtn = true;
                $('#tiRateContent').show();
                $scope.disableAddRateBtn = false;
                $('#reloadBtn').css('cursor', 'pointer');
            });
        }
    };
    $scope.reload = function () {
        var confirmReloadModal = $uibModal.open({
            templateUrl: 'rate-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {

                $scope.confirmMessage = Global.Rate.ResetConfirmHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    // pickup rategroup dropdownlist
                    $('#pickupRateGroup').select2({ placeholder: 'Select Pickup Rate Group...' });
                    $('#pickupRateGroup').val($scope.pickupGroups).trigger('change');
                    $('#pickupRateGroup').select2('enable');
                    //delivery rategroup dropdownlist
                    $('#deliveryRateGroup').select2({ placeholder: 'Select Delivery Rate Group...' });
                    $('#deliveryRateGroup').val($scope.deliveryGroups).trigger('change');
                    $('#deliveryRateGroup').select2('enable');

                    // rate content
                    $('#tiRateContent').hide();
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {}
        });
        confirmReloadModal.result.then(function (e) {
            //reload button
            $scope.reloadBtn = false;
            // save button
            $scope.disableAddRateBtn = true;
        });
    }
    //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id"); 
    $scope.tableId = tableIdTmp; //OTSTM2-1242 new way to pass table id in Pro (the way in OTS not working in Pro)
    $scope.confirm = function () {
        $scope.disableBtn = true;
        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1242
        $uibModalInstance.close(response.data.result);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };

    // Start of Add New Rate
    $scope.submitNewRate = function (e) {
        $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
        var timestamp = Date.parse(moment($scope.rateVM.effectiveDate));

        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        $scope.isValid.effectiveStartDate = true;
        if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {
            $scope.isValid.effectiveStartDate = false;
        } else {
            $scope.rateVM.effectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.rateVM.effectiveDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.rateVM.notes = [];
        }

        if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            return;
        }
        var confirmAddNewModal = $uibModal.open({
            templateUrl: 'rate-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {

                $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml;

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                rateTransactionID: function () {
                    return $scope.ratetransactionid;
                },
                rateBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                rateType: function () {
                    return $scope.category;
                }
            }
        });

        confirmAddNewModal.result.then(function (e) {
            var data = rateTIPostModel($scope.rateVM);
            configurationRateServices.AddTIRate(data).then(function (response) {
                if (response.data.status) {
                    $("#addNewRateBtn" + category).prop("disabled", true); //OTSTM2-1242 may not work in Pro
                    $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, true); //OTSTM2-1042, new way to enable/disable + button
                    $uibModalInstance.close(true);
                } else {
                    $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    var failSaveModal = $uibModal.open({
                        templateUrl: 'rate-fail-modal.html',
                        controller: 'rateFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return "Warning"; },
                            message: function () { return Global.Rate.AddNewRateFailHtml; } //response && response.data ? response.data.statusMsg :
                        }
                    });
                };
            });
        }, function () {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        });
    };
}]);
controllers.controller('addNewPIRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'configurationRateServices', '$rootScope', 'tableIdTmp', 'isSpecific', function ($scope, $http, $uibModal, $uibModalInstance, category, configurationRateServices, $rootScope, tableIdTmp, isSpecific) {
    $scope.isValid = {
        note: true,
        noteIsRequire: true,
        effectiveStartDate: true,
        effectiveEndDate: true,
        endDateOverStartDate: true,
        assignedProcessIDs: true,
        checkValid: function () {
            return this.note &&
                this.noteIsRequire &&
                this.effectiveStartDate &&
                this.effectiveEndDate &&
                this.endDateOverStartDate &&
                this.assignedProcessIDs
        },
        moveUp: function () {
            return this.effectiveStartDate &&
                this.effectiveEndDate &&
                this.endDateOverStartDate &&
                this.assignedProcessIDs
        }
    };
    $scope.isView = false;
    $scope.isEdit = false;
    $scope.isAddNew = true;

    //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id");
    $scope.tableId = tableIdTmp; //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)
    $scope.isSpecific = isSpecific; //OTSTM2-1243 separate specific and global for activity logs

    configurationRateServices.getRateTransactionByID(0, category, true).then(function (data) {//0:initial a new Rate
        $scope.rateVM = ratePIViewModel(data.data, category);
        // set start/end date
        $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        triggerPIDateTimePicker();

        // assign the multiple select (available/assigned) list 

        var listAvailableProcessors = $('#processors');
        listAvailableProcessors.multiselect();
        fillMultiples(listAvailableProcessors, data.data.piSpecificRate.AvailabledProcessIDs);
        var listAssignedProcessors = $('#processors_to');
        fillMultiples(listAssignedProcessors, data.data.piSpecificRate.AssignedProcessIDs);
        $('#processors_to').on('DOMSubtreeModified', function (e) {
            var temp = getAssignedProcessorIDs();
            $scope.isValid.assignedProcessIDs = temp.AssignedProcessIDs.length > 0;
            $scope.$apply();
        });
    });

    $scope.confirm = function () {
        $scope.disableBtn = true;
        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1243
        $uibModalInstance.close(response.data.result);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };

    // Start of Add New Rate
    $scope.submitNewRate = function (e) {
        //validate note
        $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
        if (!$scope.isValid.note) {
            $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        }
        //validate effective start date
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        var startDT = Date.parse(moment($scope.rateVM.startDate));
        if (isNaN(startDT) || startDT < nextMonth.getTime()) {
            $scope.isValid.effectiveStartDate = false;
            $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        } else {
            $scope.rateVM.effectiveStartDate = new Date(startDT).toISOString().split('Z')[0];
            $scope.rateVM.effectiveDate = new Date(startDT).toISOString().split('Z')[0];
            $scope.rateVM.notes = [];
        }

        //validate effective end date
        var endDT = Date.parse(moment($scope.rateVM.endDate));
        if (isNaN(endDT) || endDT < nextMonth.getTime()) {
            $scope.isValid.effectiveEndDate = false;
            $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        } else if (endDT < startDT) {
            $scope.isValid.endDateOverStartDate = false;
            $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);

        } else {
            $scope.rateVM.effectiveEndDate = new Date(endDT).toISOString().split('Z')[0];;
        }

        //validate assigned processors are not null
        var assignProcessors = getAssignedProcessorIDs();
        $scope.isValid.assignedProcessIDs = true;
        if (assignProcessors.AssignedProcessIDs.length == 0) {
            $scope.isValid.assignedProcessIDs = false;
            $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        }

        if (!$scope.isValid.checkValid()) {
            !$scope.isValid.moveUp() ? $("div[modal-render='true']").animate({ scrollTop: 0 }, "slow") : null;
            return;
        }

        var confirmAddNewModal = $uibModal.open({
            templateUrl: 'rate-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmMessage = Global.Rate.AddNewRateConfirmHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                rateTransactionID: function () {
                    return $scope.ratetransactionid;
                },
                rateBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                rateType: function () {
                    return $scope.category;
                }
            }
        });

        confirmAddNewModal.result.then(function (e) {
            var data = ratePostModel($scope.rateVM);

            data.piSpecificRate = getAssignedProcessorIDs();

            configurationRateServices.AddNewRate(data, category).then(function (response) {
                if (response.data.status) {
                    if (response.data.isFutureGlobalRateExists && response.data.isFutureSpecificRateExists) {
                        $("#addNewRateBtn" + category).prop("disabled", true); //OTSTM2-1243 may not work in Pro                     
                    }
                    $("#hasGlobalRate").val(response.data.isFutureGlobalRateExists);
                    if (response.data.isFutureGlobalRateExists) {
                        $("#addNewPIRateBtn" + category).attr("style", "background-color:#808080;");  //OTSTM2-1243 may not work in Pro
                    }
                    if ($scope.isSpecific) { //OTSTM2-1243 separate the adding specific scenario, + button status no change
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1243, new way to enable/disable + button
                    }
                    else {
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, true); //OTSTM2-1243, new way to enable/disable + button
                    }
                    
                    $uibModalInstance.close();
                } else {
                    $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    var failSaveModal = $uibModal.open({
                        templateUrl: 'rate-fail-modal.html',
                        controller: 'rateFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.Rate.AddNewRateFailHeader },
                            message: function () { return Global.Rate.AddNewRateFailHtml; }
                        }
                    });
                };
            });
        }, function () {
            //$("#addNewRateBtn" + category).prop("disabled", false);
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        });
    };
}]);

//Edit Rate Controller
controllers.controller('editRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'rateTransactionID', 'category', 'panelName', 'type', 'isSpecific',
    'configurationRateServices', '$rootScope', 'tableIdTmp', function ($scope, $http, $uibModal, $uibModalInstance, rateTransactionID, category, panelName, type, isSpecific, configurationRateServices, $rootScope, tableIdTmp) {
        $scope.rateTransactionID = rateTransactionID;
        $scope.disableSaveBtn = false;
        $scope.disableNoteBtn = false;
        $scope.category = category;
        $scope.panelName = panelName;
        var resource = Security.checkUserSecurity($scope.category);
        if (Security.isReadonly(resource)) {//true: disable Add Notes
            $scope.disableNoteBtn = true;
            $scope.disableSaveBtn = true;
        }
        $scope.isValid = {
            note: true,
            noteIsRequire: true,
            effectiveStartDate: true,
            effectiveEndDate: true,
            endDateOverStartDate: true,
            assignedProcessIDs: true
        };
        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;
        $scope.quantity = 5;
        //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id");
        $scope.tableId = tableIdTmp; //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
        $scope.confirmMessage = Global.Rate.EditRateConfirmHtml.replace("RateTransactionID", rateTransactionID).replace("Type", type).replace("PanelName", panelName);

        configurationRateServices.getRateTransactionByID(rateTransactionID, category, $scope.isSpecific).then(function (data) {

            $scope.rateVM = rateViewModel(data.data, category);

            if (typeof ($scope.rateVM) === 'object') {//session time out handling
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);

                if ((category+"").trim() === '2') { //OTSTM2-1061, don't know yet why the type(category) has a white space after value
                    InitialDatePicker(data.data.collectorAllowanceRate.selectableTransactionPeriods);
                } else {
                    triggerDateTimePicker($scope.category);
                }

                $scope.allInternalNotes = $scope.rateVM.notes;
                $scope.viewMore = ($scope.rateVM.notes.length > 5 && !$scope.scroller);
                $('#rate-viewdetail-notes > div > div.form-group > textarea').prop("disabled", $scope.disableNoteBtn);
                $('#rate-viewdetail-notes > div > button').prop("disabled", $scope.disableNoteBtn);

                if (category == Global.Rate.RateCategory.TireStewardshipFeesRates.rateCategoryID) {
                    $.each($('.fa-question-circle'), function (index, item) {
                        var tempName = $(item).attr("item-id") + "Name";
                        var tempContent = "";
                        $.each($scope.rateVM.tireStewardshipFeesRate, function (key, name) {
                            if (key == tempName) {
                                //tempContent = name.split(":")[1];
                                tempContent = name; //OTSTM2-1065 display whole item name
                                return false;
                            }
                        });
                        var trieStewareshipFeeSettings = {
                            content: tempContent,
                        };

                        $(item).webuiPopover('destroy')
                            .webuiPopover($.extend({}, Global.Settings.popover, trieStewareshipFeeSettings));

                    });
                }
            } else {
                console.log('session time out?');
            }
            $scope.OriginVM = angular.copy($scope.rateVM);
            $scope.OriginVM.effectiveDate = $scope.OriginVM.effectiveDate + '-01'; //OTSTM2-1061           
        });

        $scope.addUrl = Global.Rate.AddNewNoteUrl;
        $scope.loadUrl = Global.Rate.LoadNotesListUrl;
        $scope.exportUrl = Global.Rate.InternalNotesExportUrl;

        $scope.sortingOrder = false;

        $scope.noteSorting = function (e) {
            $scope.sortingOrder = !$scope.sortingOrder;
            $(e.target).removeClass('sorting');
            $(e.target).toggleClass('sorting_asc', $scope.sortingOrder);
            $(e.target).toggleClass('sorting_desc', !$scope.sortingOrder);
        };

        // Start of Save Rate
        $scope.editSaveRate = function () {

            $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
            var timestamp = Date.parse(moment($scope.rateVM.effectiveDate));

            var now = new Date();
            var nextMonth;
            if (now.getMonth() == 11) {
                nextMonth = new Date(now.getFullYear() + 1, 0, 1);
            } else {
                nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
            }
            $scope.isValid.effectiveStartDate = true;
            if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {
                $scope.isValid.effectiveStartDate = false;
            } else {
                $scope.rateVM.effectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.rateVM.effectiveDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.rateVM.notes = [];
            }
            if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                return;
            }

            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'rate-confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.Rate.EditRateConfirmHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    rateTransactionID: function () {
                        return $scope.ratetransactionid;
                    },
                    rateBatchEntyId: function () {
                        return $scope.gpiBatchentryId;
                    },
                    rateType: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {
                var data = ratePostModel($scope.rateVM);
                configurationRateServices.EditRate(data).then(function (response) {
                    if (response && response.data && response.data.status) {
                        $uibModalInstance.close();
                        ActivityLogProcess($scope);
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1061, pass null as isFutureRateExists because + button keeps no change when editing
                    } else {
                        $scope.disableSaveBtn = false;
                        $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'rate-fail-modal.html',
                            controller: 'rateFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.Rate.EditRateFailHeader },
                                message: function () {
                                    return response && response.data ? response.data.statusMsg : Global.Rate.EditRateFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {
                $scope.disableSaveBtn = false;
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            });
        };

        $scope.cancel = function () {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1061
        };
        //view more
        $scope.viewMoreNotesClick = function ($event) {
            $scope.quantity = $scope.allInternalNotes.length;
            $scope.viewMore = false;
            $scope.scroller = true;
            $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
        }
    }]);
controllers.controller('editTIRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'rateTransactionID', 'category', 'panelName', 'type', 'isSpecific',
    'configurationRateServices', '$rootScope', 'tableIdTmp', function ($scope, $http, $uibModal, $uibModalInstance, rateTransactionID, category, panelName, type, isSpecific, configurationRateServices, $rootScope, tableIdTmp) {
        $scope.rateTransactionID = rateTransactionID;
        $scope.disableSaveBtn = false;
        $scope.disableNoteBtn = false;
        $scope.category = category;
        $scope.panelName = panelName;
        var resource = Security.checkUserSecurity($scope.category);
        if (Security.isReadonly(resource)) {//true: disable Add Notes
            $scope.disableNoteBtn = true;
            $scope.disableSaveBtn = true;
        }
        $scope.isValid = {
            note: true,
            noteIsRequire: true,
            effectiveStartDate: true,
            effectiveEndDate: true,
            endDateOverStartDate: true,
            assignedProcessIDs: true
        };
        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;
        $scope.isSpecific = false;
        $scope.quantity = 5;
        //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id");
        $scope.tableId = tableIdTmp; //OTSTM2-1242 new way to pass table id in Pro (the way in OTS not working in Pro)
        $scope.confirmMessage = Global.Rate.EditRateConfirmHtml.replace("RateTransactionID", rateTransactionID).replace("Type", type).replace("PanelName", panelName);
        $scope.reloadBtn = false;
        $scope.rateVM = {};
        configurationRateServices.getRateTransactionByID(rateTransactionID, category, $scope.isSpecific).then(function (data) {
            $scope.rateVM = rateViewModel(data.data, category);
            if (data.data.tiVM) {
                $("#dataInvalid").hide();
                $scope.rateVM.tiVM = rateTIViewModel(data.data.tiVM, $scope.rateVM.decimalsize);
                $scope.rateVM.tiVM.category = category;
            } else {
                $("#dataInvalid").show();
                $scope.isEdit = false;
                $scope.isView = true;
                return false;
            }
            // 3 show ti rate content
            $('#tiRateContent').show();
            $('#reloadBtn').css('cursor', 'default');

            // initial rateDetail ViewModel
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            triggerDateTimePicker($scope.category);
            $scope.allInternalNotes = $scope.rateVM.notes;
            $scope.viewMore = ($scope.rateVM.notes.length > 5 && !$scope.scroller);
            $('#rate-viewdetail-notes > div > div.form-group > textarea').prop("disabled", $scope.disableNoteBtn);
            $('#rate-viewdetail-notes > div > button').prop("disabled", $scope.disableNoteBtn);

            //1.1 pickup group dropdown list
            var pickupRateGroup = $('#pickupRateGroup');
            fillMultiples(pickupRateGroup, data.data.tiVM.TIRateGroupList.pickupRateGroups);
            $('#pickupRateGroup').select2();
            $('#pickupRateGroup').val($scope.rateVM.tiVM.PickupRateGroupId).trigger('change');
            $('#pickupRateGroup').select2('enable', false);
            //1.2 delivery group dropdown list
            var deliveryRateGroup = $('#deliveryRateGroup');
            fillMultiples(deliveryRateGroup, data.data.tiVM.TIRateGroupList.deliveryRateGroups);
            $('#deliveryRateGroup').val($scope.deliveryGroups).trigger('change');
            $('#deliveryRateGroup').select2();
            $('#deliveryRateGroup').val($scope.rateVM.tiVM.DeliveryRateGroupId).trigger('change');
            $('#deliveryRateGroup').select2('enable', false);
            $scope.OriginVM = angular.copy($scope.rateVM);
            $scope.OriginVM.effectiveDate = $scope.OriginVM.effectiveDate + '-01'; //OTSTM2-1242
            $scope.OriginVM.effectiveStartDate = $scope.OriginVM.effectiveStartDate + '-01'; //OTSTM2-1242
            $scope.OriginVM.effectiveEndDate = $scope.OriginVM.effectiveEndDate + '-01'; //OTSTM2-1242
        });

        $scope.addUrl = Global.Rate.AddNewNoteUrl;
        $scope.loadUrl = Global.Rate.LoadNotesListUrl;
        $scope.exportUrl = Global.Rate.InternalNotesExportUrl;

        $scope.sortingOrder = false;

        $scope.noteSorting = function (e) {
            $scope.sortingOrder = !$scope.sortingOrder;
            $(e.target).removeClass('sorting');
            $(e.target).toggleClass('sorting_asc', $scope.sortingOrder);
            $(e.target).toggleClass('sorting_desc', !$scope.sortingOrder);
        };

        // Start of Save Rate
        $scope.editSaveRate = function () {
            $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
            var timestamp = Date.parse(moment($scope.rateVM.effectiveDate));

            var now = new Date();
            var nextMonth;
            if (now.getMonth() == 11) {
                nextMonth = new Date(now.getFullYear() + 1, 0, 1);
            } else {
                nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
            }
            $scope.isValid.effectiveStartDate = true;
            if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {
                $scope.isValid.effectiveStartDate = false;
            } else {
                $scope.rateVM.effectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.rateVM.effectiveDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.rateVM.notes = [];
            }
            if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                return;
            }


            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'rate-confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.Rate.EditRateConfirmHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    rateTransactionID: function () {
                        return $scope.ratetransactionid;
                    },
                    rateBatchEntyId: function () {
                        return $scope.gpiBatchentryId;
                    },
                    rateType: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {
                var data = rateTIPostModel($scope.rateVM);

                configurationRateServices.EditTIRate(data).then(function (response) {
                    if (response && response.data && response.data.status) {
                        $uibModalInstance.close();
                        ActivityLogProcess($scope);
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null);  //OTSTM2-1242, pass null as isFutureRateExists because + button keeps no change when editing
                    } else {
                        $scope.disableSaveBtn = false;
                        $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'rate-fail-modal.html',
                            controller: 'rateFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.Rate.EditRateFailHeader },
                                message: function () {
                                    return response && response.data ? response.data.statusMsg : Global.Rate.EditRateFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {
                $scope.disableSaveBtn = false;
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            });
        };

        $scope.cancel = function () {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1242
        };
        //view more
        $scope.viewMoreNotesClick = function ($event) {
            $scope.quantity = $scope.allInternalNotes.length;
            $scope.viewMore = false;
            $scope.scroller = true;
            $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
        }
    }]);
controllers.controller('editPIRateCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'rateTransactionID', 'category', 'panelName', 'type', 'isSpecific',
    'configurationRateServices', '$rootScope', 'tableIdTmp', 'dateTimeAdded', function ($scope, $http, $uibModal, $uibModalInstance, rateTransactionID, category, panelName, type, isSpecific, configurationRateServices, $rootScope, tableIdTmp, dateTimeAdded) {
        $scope.rateTransactionID = rateTransactionID;
        $scope.disableSaveBtn = false;
        $scope.disableNoteBtn = false;
        $scope.category = category;
        $scope.panelName = panelName;
        var resource = Security.checkUserSecurity($scope.category);
        if (Security.isReadonly(resource)) {
            $scope.disableNoteBtn = true;
            $scope.disableSaveBtn = true;
        }
        $scope.isValid = {
            note: true,
            noteIsRequire: true,
            effectiveStartDate: true,
            effectiveEndDate: true,
            endDateOverStartDate: true,
            assignedProcessIDs: true,
            checkValid: function () {
                return this.note &&
                    this.noteIsRequire &&
                    this.effectiveStartDate &&
                    this.effectiveEndDate &&
                    this.endDateOverStartDate &&
                    this.assignedProcessIDs
            },
            moveUp: function () {
                return this.effectiveStartDate &&
                    this.effectiveEndDate &&
                    this.endDateOverStartDate &&
                    this.assignedProcessIDs
            }
        };
        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;
        $scope.isSpecific = typeof (isSpecific) === "boolean" ? isSpecific : isSpecific && isSpecific === 'true';
        $scope.quantity = 5;
        //$scope.tableId = $("#addNewRateBtn" + category).attr("table-id");
        $scope.tableId = tableIdTmp; //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)

        //OTSTM2-1243 yyyy-mm-dd hh:mm:ss am/pm format for the date added of specific PI
        var timestamp = Date.parse(moment(dateTimeAdded));
        var temp = "/Date(" + timestamp + ")/";
        var date = dateTimeConvert(temp);
        $scope.dateTimeAdded = date.date + ' ' + date.time ;

        $scope.confirmMessage = Global.Rate.EditRateConfirmHtml.replace("RateTransactionID", rateTransactionID).replace("Type", type).replace("PanelName", panelName);

        configurationRateServices.getRateTransactionByID(rateTransactionID, category, $scope.isSpecific).then(function (data) {
            if ($scope.isSpecific) {
                $scope.rateVM = ratePIViewModel(data.data, category);               
                var listAvailableProcessors = $('#processors');
                listAvailableProcessors.multiselect();
                fillMultiples(listAvailableProcessors, data.data.piSpecificRate.AvailabledProcessIDs, $scope.isView);
                var listAssignedProcessors = $('#processors_to');
                fillMultiples(listAssignedProcessors, data.data.piSpecificRate.AssignedProcessIDs, $scope.isView);
                $scope.originalAssignedProcessors = data.data.piSpecificRate.AssignedProcessIDs; ///OTSTM2-1243 remember original Assigned Processors area
                $scope.originalAvailableProcessors = data.data.piSpecificRate.AvailabledProcessIDs; ///OTSTM2-1243 remember original Available Processors area
                if ($scope.isView) {
                    $("#processors_rightSelected").attr("disabled", "disabled");
                    $("#processors_leftSelected").attr("disabled", "disabled");
                }
                $('#processors_to').on('DOMSubtreeModified', function (e) {
                    var temp = getAssignedProcessorIDs();
                    $scope.isValid.assignedProcessIDs = temp.AssignedProcessIDs.length > 0;
                    $scope.$apply();
                });
            } else {
                $scope.rateVM = rateViewModel(data.data, category);
            }

            if (typeof ($scope.rateVM) === 'object') {//session time out handling
                $scope.rateVM.effectiveStartDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                $scope.rateVM.effectiveEndDate = $scope.rateVM.effectiveEndDate.substring(0, 7);

                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate;
                $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate;
                $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate;
                triggerPIDateTimePicker();
                $scope.allInternalNotes = $scope.rateVM.notes;
                $scope.viewMore = ($scope.rateVM.notes.length > 5 && !$scope.scroller);
                $('#rate-viewdetail-notes > div > div.form-group > textarea').prop("disabled", $scope.disableNoteBtn);
                $('#rate-viewdetail-notes > div > button').prop("disabled", $scope.disableNoteBtn);
            } else {
                console.log('session time out?');
            }
            $scope.OriginVM = angular.copy($scope.rateVM);
            $scope.OriginVM.effectiveDate = $scope.OriginVM.effectiveDate + '-01'; //OTSTM2-1243
            $scope.OriginVM.effectiveStartDate = $scope.OriginVM.effectiveStartDate + '-01'; //OTSTM2-1243
            $scope.OriginVM.effectiveEndDate = $scope.OriginVM.effectiveEndDate + '-01'; //OTSTM2-1243
            $scope.OriginVM.notes =[];
        });

        $scope.addUrl = Global.Rate.AddNewNoteUrl;
        $scope.loadUrl = Global.Rate.LoadNotesListUrl;
        $scope.exportUrl = Global.Rate.InternalNotesExportUrl;

        $scope.sortingOrder = false;

        $scope.noteSorting = function (e) {
            $scope.sortingOrder = !$scope.sortingOrder;
            $(e.target).removeClass('sorting');
            $(e.target).toggleClass('sorting_asc', $scope.sortingOrder);
            $(e.target).toggleClass('sorting_desc', !$scope.sortingOrder);
        };

        // Start of Save Rate
        $scope.editSaveRate = function () {
            if ($scope.isSpecific) {
                $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
                if (!$scope.isValid.note) {
                    $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
                }
                //validate effective start date
                var now = new Date();
                var nextMonth;
                if (now.getMonth() == 11) {
                    nextMonth = new Date(now.getFullYear() + 1, 0, 1);
                } else {
                    nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
                }
                var startDT = Date.parse(moment($scope.rateVM.startDate));
                if (isNaN(startDT) || startDT < nextMonth.getTime()) {
                    $scope.isValid.effectiveStartDate = false;
                    $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
                } else {
                    $scope.rateVM.effectiveStartDate = new Date(startDT).toISOString().split('Z')[0];
                    $scope.rateVM.effectiveDate = new Date(startDT).toISOString().split('Z')[0];
                    $scope.rateVM.notes = [];
                }

                //validate effective end date
                var endDT = Date.parse(moment($scope.rateVM.endDate));
                if (isNaN(endDT) || endDT < nextMonth.getTime()) {
                    $scope.isValid.effectiveEndDate = false;
                    $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
                } else if (endDT < startDT) {
                    $scope.isValid.endDateOverStartDate = false;
                    $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
                } else {
                    $scope.rateVM.effectiveEndDate = new Date(endDT).toISOString().split('Z')[0];;
                }

                //validate assigned processors are not null
                var assignProcessors = getAssignedProcessorIDs();
                $scope.isValid.assignedProcessIDs = true;
                if (assignProcessors.AssignedProcessIDs.length == 0) {
                    $scope.isValid.assignedProcessIDs = false;
                    $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
                }

                if (!$scope.isValid.checkValid()) {
                    !$scope.isValid.moveUp() ? $("div[modal-render='true']").animate({ scrollTop: 0 }, "slow") : null;
                    return;
                }
            } else {
                $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.rateVM.note != null;
                var timestamp = Date.parse(moment($scope.rateVM.effectiveDate));

                var now = new Date();
                var nextMonth;
                if (now.getMonth() == 11) {
                    nextMonth = new Date(now.getFullYear() + 1, 0, 1);
                } else {
                    nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
                }
                $scope.isValid.effectiveStartDate = true;
                if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {
                    $scope.isValid.effectiveStartDate = false;
                } else {
                    $scope.rateVM.effectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
                    $scope.rateVM.effectiveDate = new Date(timestamp).toISOString().split('Z')[0];
                    $scope.rateVM.notes = [];
                }
                if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
                    $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                    return;
                }
            }

            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'rate-confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.Rate.EditRateConfirmHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    rateTransactionID: function () {
                        return $scope.ratetransactionid;
                    },
                    rateBatchEntyId: function () {
                        return $scope.gpiBatchentryId;
                    },
                    rateType: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {
                var data = ratePostModel($scope.rateVM);
                if ($scope.isSpecific) {
                    data.piSpecificRate = getAssignedProcessorIDs();
                    $scope.finalAssignedProcessors = getFinalAssignedProcessors().FinalAssignedProcessors;  ///OTSTM2-1243 remember final Assigned Processors area
                    $scope.finalAvailableProcessors = getFinalAvailableProcessors().FinalAvailableProcessors;  ///OTSTM2-1243 remember final Available Processors area
                }
                configurationRateServices.EditRate(data).then(function (response) {
                    if (response && response.data && response.data.status) {
                        $("#addNewRateBtn" + category).prop("disabled", response.data.isFutureGlobalRateExists && response.data.isFutureSpecificRateExists); //OTSTM2-1243, may not work in Pro
                        $("#hasGlobalRate").val(response.data.isFutureGlobalRateExists);
                        if (response.data.isFutureGlobalRateExists) {
                            $("#addNewPIRateBtn" + category).attr("style", "background-color:#808080;"); //OTSTM2-1243, may not work in Pro
                        }
                        $uibModalInstance.close();
                        ActivityLogProcess($scope);
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1243, pass null as isFutureRateExists because + button keeps no change when editing
                    } else {
                        $scope.disableSaveBtn = false;
                        $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'rate-fail-modal.html',
                            controller: 'rateFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.Rate.EditRateFailHeader },
                                message: function () {
                                    return response && response.data ? response.data.statusMsg : Global.Rate.EditRateFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {
                $scope.disableSaveBtn = false;
                $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            });
        };

        $scope.cancel = function () {
            $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId, null); //OTSTM2-1243
        };
        //view more
        $scope.viewMoreNotesClick = function ($event) {
            $scope.quantity = $scope.allInternalNotes.length;
            $scope.viewMore = false;
            $scope.scroller = true;
            $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
        }
    }]);

//Remove Transaction Controller
controllers.controller('removeTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'rateTransactionID', 'category', 'effectiveDate', 'configurationRateServices', '$rootScope', 'isSpecific', 'tableIdTmp', 'dateTimeAdded', function ($scope, $uibModal, $uibModalInstance, rateTransactionID, category, effectiveDate, configurationRateServices, $rootScope, isSpecific, tableIdTmp, dateTimeAdded) {

    if (category == Global.Rate.RateCategory.RemittancePenaltyRates.rateCategoryID) {
        $scope.confirmMessage = Global.Rate.RemoveTransactionConfirmHtml.replace("Rates", "Remittance Penalty Rate").replace("these", "this");
    } else if (category == Global.Rate.RateCategory.TireStewardshipFeesRates.rateCategoryID) {
        $scope.confirmMessage = Global.Rate.RemoveTransactionConfirmHtml.replace("Rates", "TSF Fees");
    } else {
        $scope.confirmMessage = Global.Rate.RemoveTransactionConfirmHtml;
    }
    var currentPanel = 'div .panel.panel-default[data-category="' + category + '"]';
    var header = '';
    if (currentPanel) {
        header = $(currentPanel).data('header').trim();
    }
    $scope.effectiveDate = effectiveDate.substring(0, effectiveDate.indexOf("T"));
    $scope.isSpecific = isSpecific;

     //OTSTM2-1243 yyyy-mm-dd hh:mm:ss am/pm format for the date added of specific PI
    var timestamp = Date.parse(moment(dateTimeAdded));
    var temp = "/Date(" + timestamp + ")/";
    var date = dateTimeConvert(temp);
    $scope.dateTimeAdded = date.date + ' ' + date.time ;

    $scope.confirm = function () {
        configurationRateServices.removeTransaction(rateTransactionID, category).then(function (response) {
            if (response.data.result) {
                if (category == 4) { //OTSTM2-1243
                    if ($scope.isSpecific) {
                        Global.CommonActivity.ActivitiesLog.actLogStr = function () {  return  GlobalSearch.UserName() + ' deleted Specific ' + header + ' with Date/Time Added ' + $scope.dateTimeAdded.strongMe();}
                    }
                    else {
                        Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + ' deleted Global ' + header + ' with effective start date ' + $scope.effectiveDate.strongMe();}
                    }
                }
                else {
                    Global.CommonActivity.ActivitiesLog.actLogStr = function () {  return GlobalSearch.UserName() + ' deleted ' + header + ' with effective start date ' + $scope.effectiveDate.strongMe();}
                }
                
                addActivityLog(Global.CommonActivity.ActivityArea,null);

                //var tableId = $("#addNewRateBtn" + category).attr("table-id");
                var tableId = tableIdTmp; //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                
                if (category == 4 && $scope.isSpecific) { //OTSTM2-1243 separate the deleting specific scenario, + button status no change
                    $rootScope.$emit('PANEL_VIEW_UPDATED', tableId, null); //OTSTM2-1061, new way to enable/disable + button in Pro
                }
                else {
                    $rootScope.$emit('PANEL_VIEW_UPDATED', tableId, false); //OTSTM2-1061, new way to enable/disable + button in Pro
                }
                
                $uibModalInstance.close(response.data.result);
                $("#addNewRateBtn" + category).prop("disabled", false); //OTSTM2-1061, may not work in Pro

                if (category == Global.Rate.RateCategory.ProcessingIncentivesRates.rateCategoryID) {
                    $("#hasGlobalRate").val(response.data.isFutureGlobalRateExists);
                    if (!response.data.isFutureGlobalRateExists) {
                        $("#addNewPIRateBtn" + category).attr("style", "background-color:''"); //OTSTM2-1061, may not work in Pro
                    }
                }
            } else {
                $uibModalInstance.dismiss('cancel');
                var failRemoveModal = $uibModal.open({
                    templateUrl: 'rate-fail-modal.html',
                    controller: 'rateFailCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        header: function () { return Global.Rate.RemoveTransactionFailHeader },
                        message: function () {
                            return response && response.data ? response.data.statusMsg : Global.Rate.RemoveTransactionFailHtml;
                        }
                    }
                });
            }
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Services Section
var services = angular.module('configurationRate.services', []);
services.factory('configurationRateServices', ["$http", function ($http) {
    var factory = {};

    factory.anchorCss = function () {
        return {
            "display": "block",
            "padding": "3px 20px",
            "clear": "both",
            "font-weight": "400",
            "line-height": "1.42857143",
            "color": "#000",
            "white-space": "nowrap"
        }
    }

    factory.AddNewRate = function (data, category) {
        var submitVal = {
            url: Global.Rate.AddNewRateUrl,
            method: "POST",
            data: { newRate: data, category: category }
        }
        return $http(submitVal);
    }

    factory.EditRate = function (data) {
        var submitVal = {
            url: Global.Rate.EditRateUrl,
            method: "POST",
            data: { rateVM: data }
        }
        return $http(submitVal);
    }

    factory.removeTransaction = function (rateTransactionID, category) {
        var submitVal = {
            url: Global.Rate.RemoveTransactionUrl,
            method: "POST",
            data: { rateTransactionID: rateTransactionID, category: category }
        }
        return $http(submitVal);
    }

    factory.getRateTransactionByID = function (rateTransactionID, category, isSpecific) {
        return $http({
            method: 'GET',
            url: Global.Rate.LoadRateDetailsByTransactionIDUrl,
            params: {
                rateTransactionID: rateTransactionID,
                category: category,
                isSpecific: isSpecific
            }
        });
    }

    factory.getTIRates = function (pickupGroupID, deliveryGroupID) {
        return $http({
            method: 'GET',
            url: Global.Rate.LoadTIRateDetailsUrl,
            params: {
                pickupRateGroupId: pickupGroupID,
                deliveryRateGroupId: deliveryGroupID,
            }
        });
    }
    factory.AddTIRate = function (data, category) {
        var submitVal = {
            url: Global.Rate.AddTIRateUrl,
            method: "POST",
            data: { transportationIncentiveViewModel: data }
        }
        return $http(submitVal);
    }
    factory.EditTIRate = function (data) {
        var submitVal = {
            url: Global.Rate.EditTIRateUrl,
            method: "POST",
            data: { transportationIncentiveViewModel: data }
        }
        return $http(submitVal);
    }
    factory.isFutureCollectorRateExists = function (category) {
        return $http({
            method: 'GET',
            url: Global.Rate.IsFutureCollectorRateExists,
            params: {
                category: category
            }
        });
    }

    return factory;
}]);

//Models 
function rateViewModel(data, category) {
    var result = {};
    var prefex = category == Global.Rate.RateCategory.RemittancePenaltyRates.rateCategoryID ? '' : '$';
    if (typeof (data) === 'object') {//session time out handling
        $.each(data, function (name, value) {
            if (value instanceof Object && name != "notes") {
                var copy = {};
                $.each(value, function (itemname, itemvalue) {
                    if (typeof itemvalue !== 'object') {
                        if (typeof itemvalue === 'number') {
                            copy[itemname] = prefex + itemvalue.toFixed(data.decimalsize).toString();
                            } else if (typeof itemvalue === 'string') {
                                copy[itemname] = itemvalue;
                            }
                    }
                })
                result[name] = copy;
            } else {
                result[name] = value;
            }
        });
        return result;
    } else {
        console.log('session time out?');
        return false;//return Boolean instead of object[null]
    }
};
function ratePostModel(data) {
    var result = {};
    $.each(data, function (name, value) {
        if (value instanceof Object && name != "notes") {
            var subObj = {};
            $.each(value, function (itemname, itemvalue) {
                subObj[itemname] = 0;
                subObj[itemname] = itemvalue && itemvalue.split('$').length > 1 ? parseFloat(itemvalue.split('$')[1]) : parseFloat(itemvalue.split('$')[0]);
            })
            result[name] = subObj;
        } else {
            result[name] = value;
        }
    });
    return result;
};
function rateTIViewModel(data, decimalsize) {
    var result = {};
    var prefex = '$';
    var tiVMNames = ["DOTPremium", "ProcessorTI", "TransportationPremium"];
    if (typeof (data) === 'object') {//session time out handling
        $.each(data, function (name, value) {
            if (value instanceof Object && tiVMNames.indexOf(name) > -1) {
                var copy = {};
                $.each(value, function (itemname, itemvalue) {
                    if (itemvalue instanceof Object) {
                        var group = [];
                        for (var i = 0; i < itemvalue.length; i++) {
                            var rateItem = {};
                            $.each(itemvalue[i], function (groupname, groupvalue) {
                                if (typeof groupvalue !== 'object') {
                                    if (typeof groupvalue === 'number' && groupname.indexOf("RoadRate") > -1) {
                                        rateItem[groupname] = prefex + groupvalue.toFixed(decimalsize).toString();
                                    } else { //if (typeof groupvalue === 'string') {
                                        rateItem[groupname] = groupvalue;
                                    }
                                }
                            });
                            group[i] = rateItem;
                        }
                        copy[itemname] = group;
                    }
                });
                result[name] = copy;
            } else {
                result[name] = value;
            }
        });
        return result;
    } else {
        console.log('session time out?');
        return false;//return Boolean instead of object[null]
    }
};
function rateTIPostModel(data) {
    var result = {};
    var tiVMNames = ["DOTPremium", "ProcessorTI", "TransportationPremium"];

    $.each(data.tiVM, function (name, value) {
        if (value instanceof Object && tiVMNames.indexOf(name) > -1) {
            var copy = {};
            $.each(value, function (itemname, itemvalue) {
                if (itemvalue instanceof Object) {
                    var group = [];
                    for (var i = 0; i < itemvalue.length; i++) {
                        var rateItem = {};
                        $.each(itemvalue[i], function (groupname, groupvalue) {
                            if (typeof groupvalue !== 'object') {
                                if (groupname.indexOf("RoadRate") > -1) {
                                    rateItem[groupname] = 0;
                                    rateItem[groupname] = groupvalue && groupvalue.split("$").length > 1 ?
                                        parseFloat(groupvalue.split('$')[1]) :
                                        parseFloat(groupvalue.split('$')[0]);
                                } else {
                                    rateItem[groupname] = groupvalue;
                                }
                            }
                        });
                        group[i] = rateItem;
                    }
                    copy[itemname] = group;
                }
            });
            result[name] = copy;
        } else {
            result[name] = value;
        }
    });

    result.RateTransactionId = data.RateTransactionID;
    result.EffectiveDate = data.effectiveDate;
    result.InternalNote = data.note;
    result.category = data.category;
    return result;
}
function ratePIViewModel(data) {
    var result = {};
    var prefex = '$';
    if (typeof (data) === 'object') {//session time out handling
        $.each(data, function (name, value) {
            if (value instanceof Object && (name != "notes" || name != 'piSpecificRate')) {
                var copy = {};
                $.each(value, function (itemname, itemvalue) {
                    if (typeof itemvalue !== 'object') {
                        if (typeof itemvalue === 'number') {
                            copy[itemname] = prefex + itemvalue.toFixed(data.decimalsize).toString();
                        } else if (typeof itemvalue === 'string') {
                            copy[itemname] = itemvalue;
                        }
                    }
                })
                result[name] = copy;
            } else {
                result[name] = value;
            }
        });
        return result;
    } else {
        console.log('session time out?');
        return false;//return Boolean instead of object[null]
    }
};
function getAssignedProcessorIDs() {
    var result = {
        AssignedProcessIDs: []
    };
    var temp = $("#processors_to > option");
    $.each(temp, function (name, data) {
        var item = { ItemID: data.value, ItemName: "", ItemValue: "" };
        result.AssignedProcessIDs.push(item);
    });
    return result;
};

//OTSTM2-1243
function getFinalAssignedProcessors() {
    var result = {
        FinalAssignedProcessors: []
    };
    var temp = $("#processors_to > option");
    $.each(temp, function (name, data) {
        var item = { ItemID: parseInt(data.value), ItemName: data.text, ItemValue: null };
        result.FinalAssignedProcessors.push(item);
    });
    return result;
};

//OTSTM2-1243
function getFinalAvailableProcessors() {
    var result = {
        FinalAvailableProcessors: []
    };
    var temp = $("#processors > option");
    $.each(temp, function (name, data) {
        var item = { ItemID: parseInt(data.value), ItemName: data.text, ItemValue: null };
        result.FinalAvailableProcessors.push(item);
    });
    return result;
};

function fillMultiples(listContents, data, disabled) {
    if (data) {
        for (var i = 0; i < data.length; i++) {
            listContents.append(
                $('<option/>', {
                    value: data[i].ItemID,
                    html: data[i].ItemName,
                }).attr('disabled', !!disabled)
            );
        }
    }
}

//Directives Section
var directives = angular.module('configurationRate.directives', []);
directives.directive('rateListPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', 'configurationRateServices', function (DTOptionsBuilder, DTColumnBuilder, configurationRateServices) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'rate-list-panel-template.html',
        scope: {
            rateUrl: '@',
            category: '@',
            header: '@',
            panelName: '@',
            isTop: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.isEditSave = Security.isEditSave(resource);
            $scope.dtInstance = {};
            $scope.chevronId = $scope.header.replace(/\s/g, '');
            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId, isFutureRateExists) {
                if (tableId == $scope.dtInstance.id) {                  
                    $scope.dtInstance.DataTable.draw();
                    if (isFutureRateExists != null) {
                        $scope.isFutureRateExists = isFutureRateExists;
                    }
                }
            });
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "rateTransactionID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("EffectiveStartDate", "Effective Date").withOption('name', 'EffectiveStartDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn("DateModified", "Date Modified").withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                    DTColumnBuilder.newColumn("NotesAllText", "Notes").notSortable().withOption('name', 'NotesAllText').renderWith(function (data, type, full, meta) {
                        if (data) {
                            var html = $compile($templateCache.get('messagePopover.html').replace('Message', data))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml),
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.rateUrl,
                type: "GET",
                error: function (xhr, error, thrown) {
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [1, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('initComplete', function (settings, result) {
                    //only if not allow add new, than disable button.
                    $("#addNewRateBtn" + result.category).prop("disabled", (result.isFutureRateExists || !$scope.isEditSave));
                    $("#addNewRateBtn" + result.category).attr("table-id", settings.sTableId); //OTSTM2-1061, may not work in Pro
                    $scope.tableIdTmp = settings.sTableId; //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                    $scope.isFutureRateExists = result.isFutureRateExists; //OTSTM2-1061, new way to enable/disable + button in Pro
                    $scope.categoryName = result.categoryName;
                    $scope.categoryId = result.category;
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings, result) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                });

            //open detail modal
            $scope.openDetailsModal = function (data, dtInstance_id, type) {
                var rateDetailModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: $scope.category == Global.Rate.RateCategory.TransportationIncentivesRates.rateCategoryID ?
                            'editTIRateCtrl' : 'editRateCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        rateTransactionID: function () { return data.ID; },
                        category: function () { return $scope.categoryId; },
                        panelName: function () { return $scope.panelName; },
                        type: function () { return type; },
                        isSpecific: function () {
                            return data.isSpecific;
                        },
                        tableIdTmp: function () { return $scope.tableIdTmp; } //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $scope._pageX = 0;
            $scope._pageY = 0;
            $scope._cancelMouseClick = false;

            $('table').on('mousedown', 'tr', function (e) {
                $scope._pageX = e.pageX;
                $scope._pageY = e.pageY;
            });
            $('table').on('mouseup', 'tr', function (e) {
                $scope._pageX = e.pageX - $scope._pageX;
                $scope._pageY = e.pageY - $scope._pageY;
                if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                    $scope._cancelMouseClick = true;
                }
            });

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    if ($scope._cancelMouseClick) {
                        $scope._cancelMouseClick = false;
                        return;
                    }
                    $scope.$apply(function () {
                        $scope.openDetailsModal(aData, $scope.dtInstance.id, 'View');//click on list row will open as [View] mode
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {
                var timestamp = Date.parse(data.EffectiveStartDate);
                var html = "";
                var resource = Security.checkUserSecurity(data.Category);
                if (Security.isReadonly(resource)) {
                    return html;
                }
                if (!isNaN(timestamp)) {
                    var now = new Date();
                    var startDate = new Date(timestamp);
                    if (startDate > now) {
                        var temp = $compile($templateCache.get('rate-list-panel-action.html').replace(/rateTransactionNumber/g, data.ID).replace(/panelName/g, $scope.panelName).replace(/effectiveStartDate/g, data.EffectiveStartDate))($scope);
                        html = temp[0].outerHTML;
                    }
                }
                return html;
            }
        }
    }
}]);

directives.directive('piRateListPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', 'configurationRateServices', function (DTOptionsBuilder, DTColumnBuilder, configurationRateServices) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'pi-rate-list-panel-template.html',
        scope: {
            rateUrl: '@',
            category: '@',
            header: '@',
            panelName: '@',
            isTop: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.dtInstance = {};
            $scope.chevronId = $scope.header.replace(/\s/g, '');
            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId, isFutureGlobalRateExists) {
                if (tableId == $scope.dtInstance.id) {                  
                    $scope.dtInstance.DataTable.draw();
                    if (isFutureGlobalRateExists != null) {
                        $scope.isFutureGlobalRateExists = isFutureGlobalRateExists;
                    }
                }
            });
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "rateTransactionID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("PIType", "Type").withOption('name', 'PIType').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        var html = '';
                        if (data !== null) {
                            if (data) {
                                html = "<div>" +
                                    "<a href='javascript:void(0)' id='associatedVendor" + meta.row +
                                    "' data-placement='auto' title=''  data-text='" +
                                    full.AssociatedVendorsText +
                                    "'>Specific</a></div>";
                            } else {
                                html = "<div>Global</div>";
                            }
                        }
                        return html;
                    }),
                    DTColumnBuilder.newColumn("EffectiveStartDate", "Effective Date").withOption('name', 'EffectiveStartDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("EffectiveEndDate", "End Date").withOption('name', 'EffectiveEndDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("DateAdded", "Date/Time Added").withOption('name', 'DateAdded').withClass('date-time-fixed').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var timestamp = Date.parse(moment(data));
                            var temp = "/Date(" + timestamp + ")/";
                            var date = dateTimeConvert(temp);
                            return '<div>' + date.date + ' ' + date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn("DateModified", "Date Modified").withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                    DTColumnBuilder.newColumn("NotesAllText", "Notes").notSortable().withOption('name', 'NotesAllText').renderWith(function (data, type, full, meta) {
                        if (data) {
                            var html = $compile($templateCache.get('messagePopover.html').replace('Message', data))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml),
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.rateUrl,
                type: "GET",
                error: function (xhr, error, thrown) {
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [4, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('initComplete', function (settings, result) {
                    if (result.isFutureGlobalRateExists && result.isFutureSpecificRateExists) {
                        $("#addNewRateBtn" + result.category).prop("disabled", true); //OTSTM2-1243, may not work in Pro
                    }
                    $("#hasGlobalRate").val(result.isFutureGlobalRateExists);
                    if (result.isFutureGlobalRateExists) {
                        $("#addNewPIRateBtn" + result.category).attr("style", "background-color:#808080;"); //OTSTM2-1243, may not work in Pro
                    }
                    $("#addNewRateBtn" + result.category).attr("table-id", settings.sTableId); //OTSTM2-1243, may not work in Pro
                    $scope.tableIdTmp = settings.sTableId; //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)
                    $scope.isFutureGlobalRateExists = result.isFutureGlobalRateExists; //OTSTM2-1243, new way to enable/disable + button in Pro

                    $scope.categoryName = result.categoryName;
                    $scope.categoryId = result.category;
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings, result) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $("[id^='associatedVendor']").webuiPopover({
                        width: '500',
                        height: '300',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Specific Processor List',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            var internalNote = $(this).attr('data-text');
                            var res = internalNote.split('\n');
                            var result = "";
                            var arrayLength = res.length;
                            for (var i = 0; i < arrayLength; i++) {
                                result = result + '<p>' + res[i] + '</p>';
                            }
                            return result;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                });

            //open detail modal
            $scope.openDetailsModal = function (data, dtInstance_id, type) {
                var rateDetailModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category, data.PIType),
                    controller: 'editPIRateCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        rateTransactionID: function () { return data.ID; },
                        category: function () { return $scope.categoryId; },
                        panelName: function () { return $scope.panelName; },
                        type: function () { return type; },
                        isSpecific: function () { return data.PIType; },
                        tableIdTmp: function () { return $scope.tableIdTmp; }, //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)
                        dateTimeAdded: function () { return ''; } //OTSTM2-1243
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $scope._pageX = 0;
            $scope._pageY = 0;
            $scope._cancelMouseClick = false;

            $('table').on('mousedown', 'tr', function (e) {
                $scope._pageX = e.pageX;
                $scope._pageY = e.pageY;
            });
            $('table').on('mouseup', 'tr', function (e) {
                $scope._pageX = e.pageX - $scope._pageX;
                $scope._pageY = e.pageY - $scope._pageY;
                if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                    $scope._cancelMouseClick = true;
                }
            });

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    if ($scope._cancelMouseClick) {
                        $scope._cancelMouseClick = false;
                        return;
                    }
                    $scope.$apply(function () {
                        $scope.openDetailsModal(aData, $scope.dtInstance.id, 'View');//click on list row will open as [View] mode
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {
                var timestamp = Date.parse(data.EffectiveStartDate);
                var html = "";
                var resource = Security.checkUserSecurity(data.Category);
                if (Security.isReadonly(resource)) {
                    return html;
                }
                if (!isNaN(timestamp)) {
                    var now = new Date();
                    var startDate = new Date(timestamp);            
                    if (startDate > now) {
                        var temp = $compile($templateCache.get('rate-list-panel-action.html')
                                .replace(/rateTransactionNumber/g, data.ID)
                                .replace(/panelName/g, $scope.panelName)
                                .replace(/PIType/g, data.PIType)
                                .replace(/effectiveStartDate/g, data.EffectiveStartDate) //OTSTM2-1243
                                .replace(/dateTimeAdded/g, data.DateAdded)
                                )($scope);
                        html = temp[0].outerHTML;
                    }
                }
                return html;
            }
        }
    }
}]);

directives.directive('addNewRate', ['configurationRateServices', function (configurationRateServices) {
    return {
        restrict: 'E',
        templateUrl: 'rate-add-new-btn.html',
        scope: {
            category: '@',
            panelName: '@',
            tableIdTmp: '@',
            isFutureRateExists: '='
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.rateDetails = function () {
                var addNewRateModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: $scope.category == Global.Rate.RateCategory.TransportationIncentivesRates.rateCategoryID ?
                            'addNewTIRateCtrl' : 'addNewRateCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        tableIdTmp: function () {
                            return $scope.tableIdTmp;
                        }, //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                    }
                });
            }
        }
    }
}]);

directives.directive('addNewPiRate', ['configurationRateServices', function (configurationRateServices) {
    return {
        restrict: 'E',
        templateUrl: 'pi-rate-add-new-btn.html',
        scope: {
            category: '@',
            panelName: '@',
            tableIdTmp: '@',
            isFutureGlobalRateExists: '='
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.rateDetails = function () {
                var hasRate = $("#hasGlobalRate").val();

                if (hasRate && hasRate == 'false') {
                    var addNewRateModal = $uibModal.open({
                        templateUrl: getPanelUrl($scope.category, false),
                        controller: 'addNewRateCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            category: function () {
                                return $scope.category;
                            },
                            panelName: function () {
                                return $scope.panelName;
                            },
                            tableIdTmp: function () {
                                return $scope.tableIdTmp;
                            }, //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)
                        }
                    });
                }
            }
            $scope.piRateDetails = function () {
                var isSpecific = true;
                var addNewRateModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category, isSpecific),
                    controller: 'addNewPIRateCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        tableIdTmp: function () {
                            return $scope.tableIdTmp;
                        }, //OTSTM2-1243 new way to pass table id in Pro (the way in OTS not working in Pro)
                        isSpecific: function () {
                            return isSpecific;
                        }, //OTSTM2-1243, pass it in Pro because the activity logs for global and specific are separated
                    }
                });
            }
        }
    }
}]);

directives.directive('scrollPanelFixBtn', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('scroll', function () {
                element.find('.position-fixed-x').css('left', element.scrollLeft());
            });
        }
    };
});

directives.directive('editRate', ['configurationRateServices', function (configurationRateServices) {
    return {
        restrict: 'E',
        templateUrl: 'rate-edit-btn.html',
        scope: {
            ratetransactionid: '@',
            category: '@',
            type: '@',
            panelName: '@',
            isspecific: '@',
            tableIdTmp: '@',
            dateTimeAdded: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = configurationRateServices.anchorCss();

            if (scope.isReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.editDetails = function () {
                var postBatchModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category, $scope.isspecific),
                    controller: $scope.category == Global.Rate.RateCategory.TransportationIncentivesRates.rateCategoryID ?
                            'editTIRateCtrl' :
                            $scope.category == Global.Rate.RateCategory.ProcessingIncentivesRates.rateCategoryID ?
                            'editPIRateCtrl' : 'editRateCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        rateTransactionID: function () {
                            return $scope.ratetransactionid;
                        },
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        type: function () {
                            return $scope.type;
                        },
                        isSpecific: function () { return $scope.isspecific; },
                        tableIdTmp: function () { return $scope.tableIdTmp; }, //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                        dateTimeAdded: function () { return $scope.dateTimeAdded; }, //OTSTM2-1243
                    }
                });
            }
        }
    }
}]);

directives.directive('removeTransaction', ['configurationRateServices', function (configurationRateServices) {
    return {
        restrict: 'E',
        templateUrl: 'rate-remove-btn.html',
        scope: {
            ratetransactionid: '@',
            category: '@',
            effectiveDate: '@',
            isspecific: '=',
            tableIdTmp: '@',
            dateTimeAdded: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = configurationRateServices.anchorCss();

            if (scope.isReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);

            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'rate-confirm-modal.html',
                    controller: 'removeTransCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        rateTransactionID: function () {
                            return $scope.ratetransactionid;
                        },
                        category: function () {
                            return $scope.category;
                        },
                        effectiveDate: function () {
                            return $scope.effectiveDate;
                        },
                        isSpecific: function () { return $scope.isspecific; },
                        tableIdTmp: function () { return $scope.tableIdTmp; }, //OTSTM2-1061 new way to pass table id in Pro (the way in OTS not working in Pro)
                        dateTimeAdded: function () { return $scope.dateTimeAdded; }, //OTSTM2-1243
                    }
                });
            }
        }
    }
}]);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

directives.directive('tablerowpopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

directives.directive('noteMessageHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;
            var note = toHtml(content);

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    if (res[i]) {
                        result = result + '<p>' + res[i] + '</p>';
                    }
                }
                return result;
            }
            var placement = 'auto';
            if (scope.header === "Remittance Penalty") {
                placement = 'bottom';
            }
            $(element).webuiPopover({
                width: '500',
                height: '300',
                padding: true,
                multi: true,
                closeable: true,
                title: scope.header + ' Notes',
                type: 'html',
                trigger: 'hover',
                content: function () {
                    return $compile(note)(scope);
                },
                delay: {
                    show: 100, hide: 100
                },
                placement: placement,
            });
        },
        controller: function ($scope, $element) { }
    };
});

directives.directive('validNumber', function () {
    return {
        require: '?ngModel',
        scope: {
            theprefex: '@',
            decimalsize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {
            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 12;
            var zero = "00000";
            var prefex = scope.theprefex == "%" || scope.theprefex == "non-dollar" ? '' : '$';
            var scale;


            ngModelCtrl.$parsers.push(function (val) {

                if (angular.isUndefined(val)) {
                    var val = '0';
                }


                var clean = val.replace(/[^0-9\.]/g, '');
                var decimalCheck = clean.split('.');
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(prefex + clean);
                    ngModelCtrl.$render();
                }
                return clean != "" ? prefex + clean : clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            }).bind('blur', function () {
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                var elementValue = element.val() ? element.val().split('$').length > 1 ? element.val().split('$')[1] : element.val().split('$')[0] : '';
                if (elementValue == '' || elementValue == '-' || elementValue.indexOf('-') > -1) {
                    ngModelCtrl.$setViewValue(prefex + (scale > 0 ? "0." + zero.substr(0, scale) : "0"));
                } else {
                    var splitValue = elementValue.split('.');
                    if (splitValue.length > 1 && splitValue[1].length > scale) {
                        elementValue = splitValue[0] + '.' + splitValue[1].substr(0, scale);
                    }
                    $(".valid-number-error").hide()
                    var fixedValue = parseFloat(elementValue).toFixed(scale).toString();
                    if (scope.theprefex) {
                        if (scope.theprefex == "%" && parseFloat(elementValue) > 100) {
                            $(".valid-number-error").show();
                            fixedValue = "0";
                        }
                    } else {
                        fixedValue = prefex + fixedValue;
                    }
                    ngModelCtrl.$setViewValue(fixedValue);
                }
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});

// private functions
function dateTimeConvert(data) {

    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

    return {
        date: d,
        time: curr_time
    }
}

function triggerDateTimePicker(category) {
    var now = new Date();
    if (category == 7) {
        //now.setMonth(now.getMonth() - 4);///QATEST
    }
    var nextMonth;
    if (now.getMonth() == 11) {
        nextMonth = new Date(now.getFullYear() + 1, 0, 1).toISOString().split('T')[0];
    } else {
        nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1).toISOString().split('T')[0];
    }
    var thisMonth = now.getFullYear() + ' ' + (now.getMonth() + 1);//zero based month counting.
    var quater = Math.round(now.getMonth() / 3 + 0.5);
    var nextQuarter = now.getFullYear() + ' ' + (quater * 3 + 1);

    $('#rateEffectiveStartDate').datetimepicker({
        minView: 3,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm",
        viewMode: "year",
        startView: 'year',
        minViewMode: "months",
        startDate: nextMonth,///thisMonth,///QATEST
        minDate: nextMonth
    });

    $('#effectiveStartDate').on("focusout", function (e) {
        var inputDate = Date.parse(this.value);
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        if (isNaN(inputDate) || inputDate < nextMonth.getTime()) {
            this.value = nextMonth.toISOString().substr(0, 7);
        }
    });
}

function triggerPIDateTimePicker() {
    var now = new Date();
    var nextMonth;
    if (now.getMonth() == 11) {
        nextMonth = new Date(now.getFullYear() + 1, 0, 1).toISOString().split('T')[0];
    } else {
        nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1).toISOString().split('T')[0];
    }
    var thisMonth = now.getFullYear() + ' ' + (now.getMonth() + 1);//zero based month counting.
    var quater = Math.round(now.getMonth() / 3 + 0.5);
    var nextQuarter = now.getFullYear() + ' ' + (quater * 3 + 1);

    $('#rateEffectiveStartDate').datetimepicker({
        minView: 3,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm",
        viewMode: "year",
        startView: 'year',
        minViewMode: "months",
        startDate: nextMonth,///thisMonth,///QATEST
        minDate: nextMonth
    });

    $('#effectiveStartDate').on("focusout", function (e) {
        var inputDate = Date.parse(this.value);
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        if (isNaN(inputDate) || inputDate < nextMonth.getTime()) {
            this.value = nextMonth.toISOString().substr(0, 7);
        }
    });
    $('#rateEffectiveEndDate').datetimepicker({
        minView: 3,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm",
        viewMode: "year",
        startView: 'year',
        minViewMode: "months",
        startDate: nextMonth,
        minDate: nextMonth
    });

    $('#effectiveEndDate').on("focusout", function (e) {
        var inputDate = Date.parse(this.value);
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        if (isNaN(inputDate) || inputDate < nextMonth.getTime()) {
            this.value = nextMonth.toISOString().substr(0, 7);
        }
    });
}

function InitialDatePicker(selectableTransactionPeriods) {
    var now = new Date();
    var nextMonth;
    if (now.getMonth() == 11) {
        nextMonth = new Date(now.getFullYear() + 1, 0, 1).toISOString().split('T')[0];
    } else {
        nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1).toISOString().split('T')[0];
    }
    var thisMonth = now.getFullYear() + ' ' + (now.getMonth() + 1);//zero based month counting.
    var quater = Math.round(now.getMonth() / 3 + 0.5);
    var nextQuarter = now.getFullYear() + ' ' + (quater * 3 + 1);

    $.fn.datepicker.dates['qtrs'] = {
        days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        daysShort: ["Sun", "Moon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        //months: ["Q1", "Q1", "Q1", "Q2", "Q2", "Q2", "Q3", "Q3", "Q3", "Q4", "Q4", "Q4"],
        months: ["Jan", "Jan", "Jan", "Apr", "Apr", "Apr", "Jul", "Jul", "Jul", "Oct", "Oct", "Oct"],
        monthsShort: ["Jan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mar",
                    "", "",
                    "Apr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jun",
                    "", "",
                    "Jul&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aug&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sep",
                    "", "",
                    "Oct&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nov&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dec",
                    "", ""],
        today: "Today",
        clear: "Clear",
        format: "mm/dd/yyyy",
        titleFormat: "MM yyyy",
        /* Leverages same syntax as 'format' */
        weekStart: 0,
    };

    var startDate1 = new Date(2018, 0, 1);//zero based month
    var endDate1 = new Date(2018, 11, 31);//2018, 12, 31
    if (selectableTransactionPeriods.length > 0) {
        startDate1 = new Date(selectableTransactionPeriods[0].StartDate);
        endDate1 = new Date((selectableTransactionPeriods[selectableTransactionPeriods.length - 1].StartDate));
    }
    $('#rateEffectiveStartDateQuarterly').datepicker({
        //format: "MM yyyy",
        format: "yyyy-mm",
        minViewMode: 1,
        autoclose: true,
        language: "qtrs",
        forceParse: false,
        startDate: startDate1,
        endDate: endDate1,
        disabledDates: [
        ],
        beforeShowDay: function (date) {
            $(".month:empty").hide();
        },
    }).on("show", function (event) {
        $(".month:empty").hide();
    });

    var localScope = angular.element(document.getElementById("rateEffectiveStartDateQuarterly")).scope();
    var initDate = startDate1;
    if (typeof (localScope) !== 'undefined') {
        if (localScope.isEdit) {
            initDate = new Date(localScope.rateVM.effectiveStartDate); //take current Effective Date from rateTransaction
        }
    }
    $('#rateEffectiveStartDateQuarterly').datepicker('setDate', initDate);

    $('#effectiveStartDate').on("focusout", function (e) {
        var inputDate = Date.parse(this.value);
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        if (isNaN(inputDate) || inputDate < nextMonth.getTime()) {
            this.value = nextMonth.toISOString().substr(0, 7);
        }
    });

    $('#effectiveStartDate').keydown(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        console.log('#effectiveStartDate:', evt);
        return false
    }).keyup(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        console.log('#effectiveStartDate up:', evt);
        return false
    });
}

function getPanelUrl(categoryID, isSpecific) {
    var isSpecial = typeof (isSpecific) === 'boolean' ? isSpecific : isSpecific && isSpecific === "true";
    var url = ""
    if (categoryID == Global.Rate.RateCategory.TransportationIncentivesRates.rateCategoryID) {//3 TI Rate
        url = "modal-transportation-incentive-rate-detail-template.html";
    } else if (categoryID == Global.Rate.RateCategory.RemittancePenaltyRates.rateCategoryID) {
        url = "modal-steward-penalty-rate-detail-template.html";
    } else if (categoryID == Global.Rate.RateCategory.CollectionAllowancesRates.rateCategoryID) {
        url = "modal-collector-allowance-rate-detail-template.html";
    } else if (categoryID == Global.Rate.RateCategory.ProcessingIncentivesRates.rateCategoryID && !isSpecial) {//4 PI global
        url = "modal-processing-incentive-rate-detail-template.html";
    } else if (categoryID == Global.Rate.RateCategory.ProcessingIncentivesRates.rateCategoryID && isSpecial) {//4 PI
        url = "modal-processing-incentive-rate-detail-special-template.html";
    } else if (categoryID == Global.Rate.RateCategory.ManufacturingIncentivesRates.rateCategoryID) {
        url = "modal-manufacturing-incentive-rate-detail-template.html";
    } else if (categoryID == Global.Rate.RateCategory.TireStewardshipFeesRates.rateCategoryID) {
        url = "modal-tire-stewardship-fees-rate-detail-template.html";
        //} else if (categoryID == Global.Rate.RateCategory.EstimatedWeightsRates.rateCategoryID) {
        //    url = "modal-estimate-weights-rate-detail-template.html";
    }
    return url;
}

//function addActivityLog(logArray) {
//    if (!logArray) {
//        $.ajax({
//            url: '/System/Common/AddCommonActivity',
//            dataType: 'json',
//            type: 'POST',
//            data: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, logContent: Global.CommonActivity.ActivitiesLog.actLogStr, ActivityType: Global.CommonActivity.ActivityType },
//            success: function (result) {
//                if (result) {
//                    switch (result.status) {
//                        case "Valid Data"://success, reload activity panel
//                            angular.element(document.getElementById("panelClaimActivity")).scope().dtInstance.DataTable.draw(false);
//                            Global.CommonActivity.ActivitiesLog.actLogStr = '';//avoid duplicate log
//                            break;
//                        default:
//                            break;
//                    }
//                }
//            },
//            error: function (result) {
//                console.log('error:', result);
//            }
//        });
//    } else {
//        $.ajax({
//            url: '/System/Common/AddCommonActivityBatch',
//            dataType: 'json',
//            type: 'POST',
//            data: { ObjectId: Global.CommonActivity.ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, logContent: Global.CommonActivity.ActivitiesLog.actLogStr, logBatch: logArray, ActivityType: Global.CommonActivity.ActivityType },
//            success: function (result) {
//                if (result) {
//                    switch (result.status) {
//                        case "Valid Data"://success, reload activity panel
//                            angular.element(document.getElementById("panelClaimActivity")).scope().dtInstance.DataTable.draw(false);
//                            Global.CommonActivity.ActivitiesLog.actLogStr = '';//avoid duplicate log
//                            break;
//                        default:
//                            break;
//                    }
//                }
//            },
//            error: function (result) {
//                console.log('error:', result);
//            }
//        });
//    }

//}

function GetURLFilename(url) {//'http://www.example.com/blah/th.html' => 'th'
    if (url) {
        var m = url.toString().match(/.*\/(.+?)\./);
        if (m && m.length > 1) {
            return m[1];
        }
    }
    return "";
}

function GetPathFilename(path) {
    if (path) {
        var m = path.toString().match(/.^\\(.+\\)*(.+)\.(.+)$./);
        if (m && m.length > 1) {
            return m[1];
        }
    }
    return "";
}

function ActivityLogProcess($scope) {
    for (var propName in $scope.OriginVM) {
        var id = "Rate";
        var exists = new RegExp(id + "$").test(propName);//Regix String start ^ and finish $ (end with "Rate")
        if (exists) {
            if ($scope.OriginVM[propName]) {//find the object in VM, (ending with "Rate") && (Not null)
                if ((Object.keys($scope.OriginVM[propName]).length > 0)) {//not an empty object
                    $scope.currentVMName = propName;
                }
            }
        }
    }

    if (($scope.OriginVM.category === 3) && (!$scope.currentVMName) && ($scope.OriginVM.tiVM)) {//ti Rate
        $scope.currentVMName = 'tiVM';
        var qStr = 'option[value = ' + $scope.OriginVM.tiVM.PickupRateGroupId + ']';
        $scope.pickupRateGroupName = $('#pickupRateGroup').find(qStr).text();
        qStr = 'option[value = ' + $scope.OriginVM.tiVM.DeliveryRateGroupId + ']';
        $scope.deliveryRateGroupName = $('#deliveryRateGroup').find(qStr).text()
    }

    //$scope.OriginVM is an original clone of rateVM which have effectiveDate as 0,7
    $scope.rateVM.effectiveDate = $scope.rateVM.effectiveStartDate.substring(0, 10);
    //$scope.effectiveDate = effectiveDate.substring(0, effectiveDate.indexOf("T"));
  
    if ($scope.category == 4) {//PI Rate
        $scope.rateVM.effectiveStartDate = $scope.rateVM.effectiveStartDate.substring(0, 10);
        $scope.rateVM.effectiveEndDate = $scope.rateVM.effectiveEndDate.substring(0, 10);
    }
    var compareResultAll = diff.getDiff($scope.OriginVM, $scope.rateVM);
    var compareResult = diff.getDiff($scope.OriginVM[$scope.currentVMName], $scope.rateVM[$scope.currentVMName]);
    var compareAssignedProcessorsAdding = _.differenceWith($scope.finalAssignedProcessors, $scope.originalAssignedProcessors, _.isEqual); //for adding(move processors from Available to Assigned)
    var compareAssignedProcessorsDeleting = _.differenceWith($scope.originalAssignedProcessors, $scope.finalAssignedProcessors, _.isEqual); //for deleting(move processors from Assigned to Available)
    
    if ($scope.category == 4 && $scope.isSpecific) { //OTSTM2-1243 PI Specific--Processor Moving
        if (compareAssignedProcessorsAdding.length > 0) {
            for (var i = 0; i < compareAssignedProcessorsAdding.length; i++) {
                var ItemArray = compareAssignedProcessorsAdding[i].ItemName.split(" ");
                var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                var ItemName = ItemArray[0] + ' - ' + legalName;
               Global.CommonActivity.ActivitiesLog.actLogStr = function () {  return  GlobalSearch.UserName() + ' added ' + ItemName.strongMe() + ' to ' + 'Assigned Processors'.strongMe() + 'of Specific ' + $scope.panelName + ' with Date/Time Added ' + $scope.dateTimeAdded.strongMe();}
                Global.CommonActivity.LogHistory.push({
                    name: $scope.OriginVM[$scope.currentVMName][fieldName], value: Global.CommonActivity.ActivitiesLog.actLogStr()});
            }
        }
        if (compareAssignedProcessorsDeleting.length > 0) {
            for (var i = 0; i < compareAssignedProcessorsDeleting.length; i++) {
                var ItemArray = compareAssignedProcessorsDeleting[i].ItemName.split(" ");
                var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                var ItemName = ItemArray[0] + ' - ' + legalName;
                Global.CommonActivity.ActivitiesLog.actLogStr = function () { return GlobalSearch.UserName() + ' removed ' + ItemName.strongMe() + ' from ' + 'Assigned Processors'.strongMe() + 'of Specific ' + $scope.panelName + ' with Date/Time Added ' + $scope.dateTimeAdded.strongMe();}
                Global.CommonActivity.LogHistory.push({
                    name: $scope.OriginVM[$scope.currentVMName][fieldName], value: Global.CommonActivity.ActivitiesLog.actLogStr()
                });
            }
        }      
    }

    if (($scope.OriginVM.effectiveDate != $scope.rateVM.effectiveDate) && (!$scope.isSpecific)) {
        var piRateType = $scope.category == 4 ? 'Global ': '';
        if ($scope.category == 1) {
            Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                return GlobalSearch.UserName() + ' changed Effective Date from'
                    + $scope.OriginVM.effectiveStartDate.substring(0, 10).strongMe()
                    + 'to' + $scope.rateVM.effectiveStartDate.substring(0, 10).strongMe() + 'of ' + piRateType + $scope.panelName + ' with effective start date ' + $scope.OriginVM.effectiveStartDate.substring(0, 10).strongMe();
            }
        }
        else {
            Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                return GlobalSearch.UserName() + ' changed ' + piRateType + $scope.panelName + ' Effective Date from'
                + $scope.OriginVM.effectiveStartDate.substring(0, 10).strongMe() + 'to' + $scope.rateVM.effectiveStartDate.substring(0, 10).strongMe();
            }
        }      

        Global.CommonActivity.LogHistory.push({ name: 'Effective Date', value: Global.CommonActivity.ActivitiesLog.actLogStr() });
    }
    //parse field change
    var tiRateType = '';
    if ($scope.category == 4 && $scope.isSpecific) {//PI rate
        if (('/effectiveStartDate' in compareResultAll) || ('/effectiveEndDate' in compareResultAll)) {//.compareResultAll["/effectiveEndDate"]  .compareResultAll["/effectiveStartDate"]
            for (var propName in compareResultAll) {
                if (propName === '/effectiveStartDate') {
                    Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                        return GlobalSearch.UserName() + ' changed Processing Incentives Start Date from '
                             + compareResultAll['/effectiveStartDate'].oldvalue.substring(0, 10).strongMe() + 'to' + compareResultAll['/effectiveStartDate'].value.strongMe() + ' in ' + $scope.dateTimeAdded.strongMe();
                    }
                    Global.CommonActivity.LogHistory.push({ name: $scope.OriginVM[$scope.currentVMName][fieldName], value : Global.CommonActivity.ActivitiesLog.actLogStr() });
                }
                if (propName == '/effectiveEndDate') {
                    Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                        return GlobalSearch.UserName() + ' changed End Date from '
                             + compareResultAll['/effectiveEndDate'].oldvalue.substring(0, 10).strongMe() + 'to' + compareResultAll['/effectiveEndDate'].value.strongMe() + 'of Specific ' + $scope.panelName + ' with Date/Time Added ' + $scope.dateTimeAdded.strongMe();
                    }
                    Global.CommonActivity.LogHistory.push({ name: $scope.OriginVM[$scope.currentVMName][fieldName], value: Global.CommonActivity.ActivitiesLog.actLogStr()});
                }
            }
        }
    }

    for (var propName in compareResult) {//for all the rate value field
        var fieldName = $scope.OriginVM[$scope.currentVMName][propName.split('/').pop() + 'Name'];//Tire Stewardship Fees, name in other field
        if (!fieldName) {//find name for other rate
            fieldName = propName.split('/').pop();
            if ($scope.category == 7) {//Remittance Penalty Rate
                fieldName = propName.split('/').pop().match(/[A-Z][a-z]+|[0-9]+/g).join(" ");//Split string on UpperCase Characters
            }
        }
        var qStr = '/' + fieldName +'$';//find ending string.
        var vIndex = propName.replace(new RegExp(qStr), '').split('/').pop();//find index
        if ($scope.category == 7) {
            compareResult[propName].oldvalue += '%';
            compareResult[propName].value += '%';
        }
        if ($scope.category == 3) {//TI rate
            switch (propName.split('/')[1]) {
                case 'TransportationPremium':
                    tiRateType = ' Transportation Premium Rate' +
                    ($scope.rateVM.tiVM.TransportationPremium.TransportationPremiumItems[vIndex].PickupGroupName + ' , ' + $scope.rateVM.tiVM.TransportationPremium.TransportationPremiumItems[vIndex].DeliveryGroupName).strongMe();
                    break;
                case 'DOTPremium':
                    tiRateType = ' DOT Premium Rate' + 
                    ($scope.rateVM.tiVM.DOTPremium.DOTPremiumItems[vIndex].PickupGroupName + ' , ' + $scope.rateVM.tiVM.DOTPremium.DOTPremiumItems[vIndex].DeliveryGroupName).strongMe();
                    break;
                case 'ProcessorTI':
                    tiRateType = ' Processor TI Rate' + $scope.rateVM.tiVM.ProcessorTI.ProcessorTIItems[vIndex].DeliveryGroupName.strongMe();
                    break;
                default:
                    break;

            }
        }
        var piTireType = '';
        var piRateType = '';
        if ($scope.category == 4) {//PI rate
            piTireType = 'PIT ';
            if (propName.indexOf('OnRoad') > 0) {
                piTireType = 'SPS (On-Road) ';
            }
            if (propName.indexOf('OffRoad') > 0) {
                piTireType = 'SPS (Off-Road) ';
            }
            piRateType = $scope.isSpecific ? ' Specific ' : ' Global ';
        }
        if ($scope.category == 4) { //OTSTM2-1243
            if (!$scope.isSpecific) {
                if (piTireType == 'SPS (On-Road) ') {
                    Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                        return GlobalSearch.UserName() + ' changed ' + piTireType.strongMe() + fieldName.replace('OnRoad', '').replace('OffRoad', '').replace('Feedstock', '(Feedstock)').strongMe()
                        + 'from' + compareResult[propName].oldvalue.strongMe() + 'to' + compareResult[propName].value.strongMe() + 'of ' + piRateType + $scope.panelName + ' with ' + $scope.rateVM.effectiveStartDate.substring(0, 10).strongMe();
                    }
                }
                else {
                    Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                        return GlobalSearch.UserName() + ' changed ' + piTireType.strongMe() + fieldName.replace('OnRoad', '').replace('OffRoad', '').replace('Feedstock', '(Feedstock)').replace('NoPI', '(No PI)').replace('TransferFibreRubber', 'Transfer-Fibre/Rubber').strongMe()
                        + 'from' + compareResult[propName].oldvalue.strongMe() + 'to' + compareResult[propName].value.strongMe() + 'of ' + piRateType + $scope.panelName + ' with effective start date ' + $scope.rateVM.effectiveStartDate.substring(0, 10).strongMe();
                    }
                }          
            }
            else {
                Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                    return GlobalSearch.UserName() + ' changed ' + piTireType.strongMe() + fieldName.replace('OnRoad', '').replace('OffRoad', '').replace('Feedstock', '(Feedstock)').replace('NoPI', '(No PI)').replace('TransferFibreRubber', 'Transfer-Fibre/Rubber').strongMe()
                        + 'from' + compareResult[propName].oldvalue.strongMe() + 'to' + compareResult[propName].value.strongMe() + 'of ' + piRateType + $scope.panelName + ' with Date/Time Added ' + $scope.dateTimeAdded.strongMe();
                }
            }
        }
        else {
            Global.CommonActivity.ActivitiesLog.actLogStr = function () {
                return GlobalSearch.UserName() + ' changed ' + piRateType + tiRateType + ($scope.category == 3 ?
                    fieldName.replace('Rate', '').split('/').pop().match(/[A-Z][a-z]+|[0-9]+/g).join("-").toLocaleUpperCase() + ' ' :
                    (($scope.category == 7) ? 'Penalty for Late Submissions and/or Late Payment' :
                    (piTireType + fieldName.replace('OnRoad', '').replace('OffRoad', '').replace('AGLS', 'AG/LS'))).strongMe())
                    + 'from' + compareResult[propName].oldvalue.strongMe() + 'to' + compareResult[propName].value.strongMe() + 'of ' + $scope.panelName + ' with effective start date ' + $scope.rateVM.effectiveStartDate.substring(0, 10).strongMe();
            }
        }
        
        Global.CommonActivity.LogHistory.push({ name: $scope.OriginVM[$scope.currentVMName][fieldName], value: Global.CommonActivity.ActivitiesLog.actLogStr() });
    }
    addActivityLog(Global.CommonActivity.ActivityArea,Global.CommonActivity.LogHistory);
    Global.CommonActivity.LogHistory = [];
}