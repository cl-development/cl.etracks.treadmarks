﻿/* global CurrentUser */
(function ($) {
    $.fn.createDataTable = function (options) {
        TM = {};
        UserRoles = { Read: 6, Write: 7, Submit: 8, ParticipantAdmin: 9 };
        TM.settings = $.extend({
            ajaxHandler: '',
            viewMoreId: '',
            totalRecordsId: '',
            searchId: '',
            exportId: '',
            exportUrl: '',
            foundId: '',
            removeIconId: '',
            modalResendEmailId: '',
            buttonResendEmailId: '',
            modalActivateId: '',
            buttonActivateId: '',
            modalInactivateId: '',
            buttonInactivateId: '',
            modalEditUserId: '',
            buttonEditUserNextId: '',
            modalEditUserConfirmId: '',
            buttonEditUserId: '',
            checkboxRoleClassName: '',
            serverSide: true,
            scrollY: 465,
            scrollX: true,
            rowUrl: '',
            ColumnId: 0,
            pageSize: 12,
            //columnNames: [{ columnName: 'FirstName' }, { columnName: 'LastName' }, { columnName: 'UserEmail' }, { columnName: 'DisplayStatus' }, { columnName: 'Actions', className: 'td-center', orderable: false }],
        }, options);

        var table = this.DataTable({
            serverSide: TM.settings.serverSide,
            ajax: TM.settings.ajaxHandler,
            scrollY: TM.settings.scrollY,
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            scrollCollapse: true,
            createdRow: function (row, data, index) {

                if (TM.settings.rowUrl != '') {
                    $(row).addClass('cursor-pointer');
                    var claimId = data.ClaimId;
                    var regNumber = data.RegNumber;
                    $(row).on('click', 'td:not(.special-td)', function () {
                        //if (this.parentElement.className.includes('NoAccess')) {
                        //    window.location.href = '/System/Common/UnauthorizedPage?reason=' + 'Current user has No Access to view detail.';
                        //    return;
                        //}
                        if (_cancelMouseClick) {
                            _cancelMouseClick = false;
                            return;
                        }
                        window.open(TM.settings.rowUrl + '?claimId=' + claimId + '&regNumber=' + regNumber, '_self');
                    });

                    //$(row).on('click', 'td.special-td', function () {
                    //    if (this.parentElement.className.includes('NoAccess')) {
                    //        //$('div.popover-content > ul > li').addClass('avoid-clicks');
                    //        $('div.popover-content > ul > li > a').attr('data-target', '').attr('href', '/System/Common/NoAccess?reason=' + 'Current user does not have Permission.');
                    //        return;
                    //    }
                    //});
                }

                if (TM.settings.applicationType == "staffAllClaims") {
                    //switch (data.Status) {
                    //    case "Submitted":
                    //        $('td', row).eq(7).empty().append($('<div class="panel-table-status color-tm-orange-bg">' + data.Status + '</div>'));
                    //        break;
                    //    case "Open":
                    //        $('td', row).eq(7).empty().append($('<div class="panel-table-status color-tm-blue-bg">' + data.Status + '</div>'));
                    //        break;
                    //    case "Under Review":
                    //        $('td', row).eq(7).empty().append($('<div class="panel-table-status color-tm-yellow-bg">' + data.Status + '</div>'));
                    //        break;
                    //    case "Approved":
                    //        $('td', row).eq(7).empty().append($('<div class="panel-table-status color-tm-green-bg">' + data.Status + '</div>'));
                    //        break;
                    //    default:
                    //        $('td', row).eq(7).empty().append('');
                    //}

                    switch (data.Type) {
                        case 2:
                            $(row).addClass(Global.StaffAllIndex.AccessPermission.CollectorClaimClaimSummary)
                                .addClass(Global.StaffAllIndex.UserClaimsWorkflow.Collector.WorkFlow);
                            break;
                        case 3:
                            $(row).addClass(Global.StaffAllIndex.AccessPermission.HaulerClaimClaimSummary)
                                .addClass(Global.StaffAllIndex.UserClaimsWorkflow.Hauler.WorkFlow);
                            break;
                        case 4:
                            $(row).addClass(Global.StaffAllIndex.AccessPermission.ProcessorClaimClaimSummary)
                                .addClass(Global.StaffAllIndex.UserClaimsWorkflow.Processor.WorkFlow);
                            break;
                        case 5:
                            $(row).addClass(Global.StaffAllIndex.AccessPermission.RPMClaimClaimSummary)
                                .addClass(Global.StaffAllIndex.UserClaimsWorkflow.RPM.WorkFlow);
                            break;
                        default:
                            break;
                    }
                    if ((data.Status == "Submitted") || (data.Status == "Under Review")) {
                        //$('td:eq(10)', row).addClass("special-td");
                        $('td:has(a)', row).addClass("special-td");//td with action menu item
                    }
                    //if (data.IsClaimSupervisor && data.Status == "Under Review") {
                    //    $('td:eq(10)', row).addClass("special-td");
                    //}
                }
            },
            processing: false,
            scrollCollapse: true, //OTSTM2-904 "true" for dynamic height of datatables
            order: [[1, "desc"]],
            searching: true,
            "dom": "rtiS",
            info: false,
            deferRender: true,
            "scroller": {
                displayBuffer: 20,
                rowHeight: 25,
                serverWait: 100,
                loadingIndicator: false
            },
            columns: [
                {
                    name: "RegNumber",
                    width: "6%",
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.RegNumber + "</span>";
                    }
                },
                {
                    name: "Type",
                    width: "6%",
                    data: null,
                    render: function (data, type, full, meta) {
                        //return "<span>" + data.Type + "</span>";
                        switch (data.Type) {
                            case 2:
                                return ('Collector');
                                break;
                            case 3:
                                return ('Hauler');
                                break;
                            case 4:
                                return ('Processor');
                                break;
                            case 5:
                                return ('RPM');
                                break;
                            default:
                                return ('');
                        }
                    }
                },
                {
                    name: "Company",
                    width: "15%",
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.Company + "</span>";
                    }
                },
                {
                    name: "Period",
                    width: "8%",
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.Period + "</span>";
                    }
                },
                {
                    name: "Submitted",
                    width: "8%",
                    data: null,
                    //className: "td-center",
                    render: function (data, type, full, meta) {
                        if (data.Submitted != null && data.Submitted !== undefined) {
                            return dateTimeConvert(data.Submitted);
                        } else {
                            return '';
                        }
                    }
                },
                {
                    name: "PaymentDue",
                    width: "11%",
                    data: null,
                    //className: "td-center",
                    render: function (data, type, full, meta) {
                        if (data.PaymentDue != null && data.PaymentDue !== undefined) {
                            return dateTimeConvert(data.PaymentDue);
                        } else {
                            return '';
                        }
                    }
                },
                {
                    name: "TotalClaimAmount",
                    width: "13%",
                    data: null,
                    //className: "td-center",
                    render: function (data, type, full, meta) {
                        if (data.TotalClaimAmount == null) {
                            return "<span>" + "" + "</span>";
                        } //else return "<span>" + data.TotalClaimAmount.toFixed(2) + "</span>"; //OTSTM2-194

                            //OTSTM2-423
                        else return "<span>" + data.TotalClaimAmount.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + "</span>";
                    }
                },
                {
                    name: "Status",
                    width: "8%",
                    //className: "td-center",
                    data: null,
                    render: function (data, type, full, meta) {
                        //return "<span>" + data.Status + "</span>";
                        switch (data.Status) {
                            case "Submitted":
                                return ('<div class="panel-table-status color-tm-orange-bg">' + data.Status + '</div>');
                                break;
                            case "Open":
                                return ('<div class="panel-table-status color-tm-blue-bg">' + data.Status + '</div>');
                                break;
                            case "Under Review":
                                return ('<div class="panel-table-status color-tm-yellow-bg">' + data.Status + '</div>');
                                break;
                            case "Approved":
                                return ('<div class="panel-table-status color-tm-green-bg">' + data.Status + '</div>');
                                break;
                            default:
                                return ('');
                        }
                    }
                },
                {
                    name: "HoldStatus",
                    width: "9%",
                    //className: "td-center",
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.HoldStatus + "</span>";
                    }
                },
                {
                    name: "AssignedTo",
                    width: "9%",
                    className: 'assignedTo',
                    data: null,
                    render: function (data, type, full, meta) {
                        return "<span>" + data.AssignedTo + "</span>";
                    }
                },
                {
                    name: "ClaimsActions",
                    width: "7%",
                    data: null,
                    className: 'td-center',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        TM.settings.RoutingCount = data.RoutingCount;
                        var status = data.Status;
                        var content = '';
                        var html = '';
                        if (Global.StaffAllIndex.AccessPermission.ClaimsAssignmentPage != 'EditSave') return html;
                        switch (data.Type) {
                            case 2:
                                if (!Global.StaffAllIndex.UserClaimsWorkflow.Collector.ActionGear) return html;
                                break;
                            case 3:
                                if (!Global.StaffAllIndex.UserClaimsWorkflow.Hauler.ActionGear) return html;
                                break;
                            case 4:
                                if (!Global.StaffAllIndex.UserClaimsWorkflow.Processor.ActionGear) return html;
                                break;
                            case 5:
                                if (!Global.StaffAllIndex.UserClaimsWorkflow.RPM.ActionGear) return html;
                                break;
                            default:
                                break;
                        }
                        if (status == "Submitted") {
                            html = '<div class="btn-group dropdown-menu-actions">' +
                                '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                                '<i class="glyphicon glyphicon-cog"></i></a>' +
                                '<div class="popover-menu"><ul><li><a data-rowId="' + meta.row + '"  data-target="' + TM.settings.modalAssign + '" data-toggle="modal" href="javascript:void(0)" >' +
                                '<i class="fa fa-user"></i> Assign</a>' +
                                '</li></ul></div></div>';

                        } else if (status == "Under Review") {
                            html = '<div class="btn-group dropdown-menu-actions"><a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                                '<i class="glyphicon glyphicon-cog"></i></a>' +
                                '<div class="popover-menu"><ul>' +
                                '<li><a data-rowId="' + meta.row + '"  data-target="' + TM.settings.modalReAssign + '" data-toggle="modal" href="javascript:void(0)">' +
                                '<i class="fa fa-user"></i> Re-Assign</a></li>' +
                                '<li><a data-rowId="' + meta.row + '" data-target="' + TM.settings.modalUnAssign + '" data-toggle="modal" href="javascript:void(0)">' +
                                '<i class="fa fa-minus-square-o"></i> Un-Assign</a></li></ul></div></div>';
                        }

                        return html;
                    }
                },
                {
                    name: "ClaimId",
                    data: null,
                    className: 'display-none',
                    render: function (data, type, full, meta) {
                        return "<span>" + data.ClaimId + "</span>";
                    }
                },
                {
                    name: "AssignedToUserId",
                data: null,
                className: 'display-none',
                render: function (data, type, full, meta) {
                    return "<span>" + data.AssignedToUserId + "</span>";
                },
            }
            ],
            initComplete: function (settings, json) {
                $('.dataTables_scrollBody').css('overflow-y', 'hidden');
                $(TM.settings.totalRecordsId).val('Total ' + settings.fnRecordsTotal());

                if (TM.settings.RoutingCount > 0) {
                    $(TM.settings.asgnToMeBtn).show();
                } else {
                    $(TM.settings.asgnToMeBtn).hide();
                }
               
                //OTSTM2-904
                table.columns().eq(0).each(function (index) {
                    var column = table.column(index);
                    var that = column;
                    $('input', column.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            },
            drawCallback: function (settings) {
                var direction = settings.aaSorting[0][1];
                $('.sort').html('<i class="fa fa-sort"></i>');
                (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
                $(TM.settings.foundId).html('Found ' + settings.fnRecordsDisplay());
                (settings.fnDisplayEnd() >= TM.settings.pageSize) ? $(TM.settings.viewMoreId).css('visibility', 'visible') : $(TM.settings.viewMoreId).css('visibility', 'hidden');

                //OTSTM2-904
                if ($(TM.settings.viewMoreId).is(':visible')) {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                }
                else {
                    if (settings.fnDisplayEnd() >= TM.settings.pageSize) {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'scroll' });
                    }
                    else {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                    }
                }
               
                if ($("#searchTextReg").val() || $("#searchTextType").val() || $("#searchTextCompany").val() || $("#searchTextPeriod").val() || $("#searchTextSubmitted").val() || $("#searchTextPaymentDueDate").val()
                    || $("#searchTextTotalClaimAmount").val() || $("#searchTextStatus").val() || $("#searchTextHoldStatus").val() || $("#searchTextAssignedTo").val() || $("#search").val()) {
                    $(TM.settings.foundId).css('display', 'block');
                }
                else {
                    $(TM.settings.foundId).css('display', 'none');
                }

                var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                var searchValue = $(TM.settings.searchId).val();
                var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)
                .replace('-st-reg', $("#searchTextReg").val()).replace('-st-type', $("#searchTextType").val()).replace('-st-company', $("#searchTextCompany").val())
                .replace('-st-period', $("#searchTextPeriod").val()).replace('-st-submitted', $("#searchTextSubmitted").val()).replace('-st-paymentduedate', $("#searchTextPaymentDueDate").val())
                .replace('-st-totalclaimamount', $("#searchTextTotalClaimAmount").val()).replace('-st-status', $("#searchTextStatus").val())
                .replace('-st-holdstatus', $("#searchTextHoldStatus").val()).replace('-st-assignedto', $("#searchTextAssignedTo").val()); //OTSTM2-904
                $(TM.settings.exportId).attr('href', url);
            },
            footerCallback: function (row, data, start, end, display) {
            }
        });

        _pageX = 0;
        _pageY = 0;
        _cancelMouseClick = false;

        $('#tblStaffAllClaimsList tbody').on('mousedown', 'tr', function (e) {
            _pageX = e.pageX;
            _pageY = e.pageY;
        });
        $('#tblStaffAllClaimsList tbody').on('mouseup', 'tr', function (e) {
            _pageX = e.pageX - _pageX;
            _pageY = e.pageY - _pageY;
            if ((Math.abs(_pageX) > 5) || (Math.abs(_pageY) > 5)) {
                _cancelMouseClick = true;
            }
        });

        table.on("draw.dt", function () {
            $("[id^='actions']").popover({
                placement: "bottom",
                container: "body",
                html: true,
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                content: function () {
                    return $($(this)).siblings(".popover-menu").html();
                },
            });
        });        

        $(TM.settings.viewMoreId).on('click', function () {
            $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'auto' });
            $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
            $('.dataTables_scrollFoot').removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-904
            $(this).hide();
        });

        var myTimer;
        var copying = false;

        $(TM.settings.searchId).bind('copy', function () {
            copying = true;
        });

        $(TM.settings.searchId).on('keyup', function (e) {
            if (copying) {
                copying = false;
                return;
            }
            if ((e.keyCode == 37) || (e.keyCode == 38) || (e.keyCode == 39) || (e.keyCode == 40) || (e.keyCode === 35) || (e.keyCode === 36)
                || (e.keyCode == 16) || (e.keyCode == 17)) {//16-shift|| shift-L || shift-R || ctrl-c || 17-ctrl
                return;
            }
            var str = $(this).val();
            var direction;
            var sortColumn;

            clearTimeout(myTimer);
            myTimer = setTimeout(searchIt, 500, str, direction, sortColumn);
        });

        var searchIt = function (str, direction, sortColumn) {
            //console.log('send to server:' +str);
            if (str.length >= 1) {
                table.search(str).draw(false);
                //$(TM.settings.searchId).css('visibility', 'visible'); //OTSTM2-904 fix search input visible issue (if there is search text)
                //$(TM.settings.foundId).css('display', 'block');
                //direction = (undefined !== table.settings) ? (((undefined !== table.settings.aaSorting)) ? table.settings.aaSorting[0][1] : 'asc') : "asc";
                //sortColumn = TM.settings.columnNames[table.settings()[0].aLastSort[0].col];
                //var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', $(TM.settings.searchId).val());
                //$(TM.settings.exportId).attr('href', url);
                $(TM.settings.searchId).siblings('.remove-icon').show();
            }
            else {
                $(TM.settings.searchId).siblings('.remove-icon').hide();
                //$(TM.settings.searchId).removeAttr('style');
                //$(TM.settings.foundId).css('display', 'none');
                //direction = (undefined !== table.settings) ? (((undefined !== table.settings.aaSorting)) ? table.settings.aaSorting[0][1] : 'asc') : "asc";
                //sortColumn = TM.settings.columnNames[table.settings()[0].aLastSort[0].col].columnName;
                //var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', $(TM.settings.searchId).val());
                //$(TM.settings.exportId).attr('href', url);
                table.search(str).draw(false);
            }
        }

        //on clear field click
        $(TM.settings.removeIconId).click(function () {

            $(this).siblings("input").val("");
            $(this).hide();
            //$(TM.settings.foundId).css('display', 'none');
            //$(TM.settings.searchId).trigger('keyup');
            table.search("").draw(false);
        });

        //Staff All claims
        $(TM.settings.listOfAssignedNames).on('change', function () {
            if ($(TM.settings.listOfAssignedNames + " option:selected").index() == 0) {
                $(TM.settings.btnAssignClaim).prop('disabled', true);
            } else {
                $(TM.settings.btnAssignClaim).prop('disabled', false);
            }
        });

        $(TM.settings.listOfReAssignedNames).on('change', function () {
            if ($(TM.settings.listOfReAssignedNames + " option:selected").index() == 0) {
                $(TM.settings.btnReAssignClaim).prop('disabled', true);
            } else {
                $(TM.settings.btnReAssignClaim).prop('disabled', false);
            }
        });

        var dateTimeConvert = function (data) {
            if (data == null) return '1/1/1950';
            var r = /\/Date\(([0-9]+)\)\//gi;
            var matches = data.match(r);
            if (matches == null) return '1/1/1950';
            var result = matches.toString().substring(6, 19);
            var epochMilliseconds = result.replace(
            /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
            '$1');
            var b = new Date(parseInt(epochMilliseconds));
            var c = new Date(b.toString());
            var curr_date = c.getDate();
            if (curr_date < 10) {
                curr_date = '0' + curr_date;
            }
            var curr_month = c.getMonth() + 1;
            if (curr_month < 10) {
                curr_month = '0' + curr_month;
            }
            var curr_year = c.getFullYear();
            //var curr_h = c.getHours();
            //var curr_m = c.getMinutes();
            //var curr_s = c.getSeconds();
            //var curr_offset = c.getTimezoneOffset() / 60;
            var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
            return d;
        }

        var getAllAssignedNames = function (assignedType, claimTypeNumber, claimId, lAssignedUserId) {
            var sAccountName;
            switch (claimTypeNumber) {
                case 2:
                    sAccountName = 'Collector';
                    break;
                case 3:
                    sAccountName = 'Hauler';
                    break;
                case 4:
                    sAccountName = 'Processor';
                    break;
                case 5:
                    sAccountName = 'RPM';
                    break;
                default:
                    break;
            }
            req = { claimId: claimId, accountName: sAccountName, claimType:'Claim' };
            $.ajax({
                type: 'POST',
                url: TM.settings.userHandler,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    $.each(data, function (index, data) {
                        $(assignedType).append('<option value="' + index + '" id="' + data.Value + '" cid="' + claimId + '">' + data.Text + '</option>');
                    });
                    if ($(assignedType)[0].length > 0) {
                        $(assignedType).prop('selectedIndex', 1);
                        $(TM.settings.btnAssignClaim).prop('disabled', false);
                        $(TM.settings.btnReAssignClaim).prop('disabled', false);
                    }
                },
                failure: function (data) {
                },
            });
        }

        var assignAndReassignHandler = function (req) {
            $.ajax({
                type: 'POST',
                url: TM.settings.GetAssignReassignHandler,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    table.search($(TM.settings.searchId).val()).draw(false);
                },
                failure: function (data) {
                    table.search($(TM.settings.searchId).val()).draw(false);
                },
            });
        }

        $(TM.settings.modalAssign).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalAssignText).html('Reg. # ' + currentRow.RegNumber + ', Period: ' + currentRow.Period);

            $(TM.settings.listOfAssignedNames).empty().append('<option value="0" selected="selected"></option>');
            getAllAssignedNames(TM.settings.listOfAssignedNames, currentRow.Type, currentRow.ClaimId);

            $(TM.settings.btnAssignClaim).prop('disabled', true);
        });

        $(TM.settings.modalReAssign).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalReAssignText).html('Reg. # ' + currentRow.RegNumber + ', Period: ' + currentRow.Period);

            $(TM.settings.listOfReAssignedNames).empty().append('<option value="0" selected="selected"></option>');
            getAllAssignedNames(TM.settings.listOfReAssignedNames, currentRow.Type, currentRow.ClaimId, currentRow.AssignedToUserId);

            $(TM.settings.btnReAssignClaim).prop('disabled', true);
        });

        $(TM.settings.modalUnAssign).on('show.bs.modal', function (event) {
            var invoker = $(event.relatedTarget);
            var rowId = $(invoker).attr('data-rowId');
            var currentRow = table.row(rowId).data();
            $(TM.settings.modalUnAssignText).html('Reg. # ' + currentRow.RegNumber + ', Period: ' + currentRow.Period);
            $(TM.settings.modalUnAssignName).html(currentRow.AssignedTo);
            TM.settings.currUnAssignedClaimid = currentRow.ClaimId;
        });

        $(TM.settings.modalAssingToMe).on('show.bs.modal', function (event) {

            $.ajax({
                type: 'POST',
                url: TM.settings.AssignToMeHandler,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    if (data.status != 'Invalid data') {
                        $(TM.settings.modalAssingToMeText).html('Reg. # ' + data.claim.RegNumber + ', Period: ' + data.claim.Period);
                    } else {
                        $(TM.settings.modalAssingToMeText).closest('.txt-c').empty().append('There is no eligible claim to assign to you at this time');
                        $(TM.settings.assignHeader).empty().html(TM.settings.UnabletoAssign);
                    }
                },
                failure: function (data) {
                    window.location.reload(true);
                },
            });

        });

        $(TM.settings.btnAssignClaim).on('click', function () {

            var userid = $(TM.settings.listOfAssignedNames).find(':selected').attr('id');
            var claimid = $(TM.settings.listOfAssignedNames).find(':selected').attr('cid');
            var req = { userid: userid, claimid: claimid };

            assignAndReassignHandler(req);
        });

        $(TM.settings.btnReAssignClaim).on('click', function () {
            var userid = $(TM.settings.listOfReAssignedNames).find(':selected').attr('id');
            var claimid = $(TM.settings.listOfReAssignedNames).find(':selected').attr('cid');
            var req = { userid: userid, claimid: claimid };

            assignAndReassignHandler(req);
        });

        $(TM.settings.btnUnAssignClaim).on('click', function () {

            var req = { claimid: TM.settings.currUnAssignedClaimid };

            $.ajax({
                type: 'POST',
                url: TM.settings.GetUnassignHandler,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    table.search($(TM.settings.searchId).val()).draw(false);
                },
                failure: function (data) {
                    table.search($(TM.settings.searchId).val()).draw(false);
                },
            });
        });

        $(TM.settings.btnAssignToMeClaim).on('click', function () {
            table.search($(TM.settings.searchId).val()).draw(false);
        });       
    };

    //OTSTM2-904
    $('#tblStaffAllClaimsList tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchText' + idText + '"/>');
    });

}(jQuery));

