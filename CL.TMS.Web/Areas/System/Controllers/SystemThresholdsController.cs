﻿using CL.Framework.Logging;
using CL.TMS.Common;

using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Web.Infrastructure;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class SystemThresholdsController : BaseController
    {
        #region Services

        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        private IConfigurationsServices configurationService;

        #endregion Services

        public SystemThresholdsController(IUserService userService, IRegistrantService registrantService, IMessageService messageService, IConfigurationsServices configurationService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
            this.configurationService = configurationService;
        }

        [ClaimsAuthorization(TreadMarksConstants.AdminSystemThresholds)]
        public ActionResult AccountThresholds()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "systemThresholdsApp";
                ViewBag.SelectedMenu = "SystemThresholds";
                ViewBag.SelectedSubMenu = "Accounts";

                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public JsonResult LoadAccountThresholds()
        {
            var vm = configurationService.LoadAccountThresholds();
            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAccountThredholds(AccountThresholdsVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminSystemThresholds, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized", data = vm });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = vm });
            }
            var dicts = vm.Items.ToDictionary(x => x.ItemName);
            string msg = string.Empty, sResult = string.Empty;
            if (vm.Items.AsEnumerable().Any(i => string.IsNullOrEmpty(i.ItemValue)))
            {
                sResult += "Model has empty value.";
            }
            if (string.IsNullOrEmpty(sResult))
            {
                msg = validateModel("Collector:", dicts["CBCollectorAutoApprove"].ItemValue == "1", Int32.Parse(dicts["CollectorAutoApproveAmount"].ItemValue),
                    dicts["CBCollectorApprover1Reqd"].ItemValue == "1", Int32.Parse(dicts["CollectorApprover1ReqdAmount"].ItemValue),
                    dicts["CBCollectorApprover2Reqd"].ItemValue == "1", Int32.Parse(dicts["CollectorApprover2ReqdAmount"].ItemValue));
                sResult += msg;

                msg = validateModel("Hauler:", false, 0,
                    dicts["CBHaulerApprover1Reqd"].ItemValue == "1", Int32.Parse(dicts["HaulerApprover1ReqdAmount"].ItemValue),
                    dicts["CBHaulerApprover2Reqd"].ItemValue == "1", Int32.Parse(dicts["HaulerApprover2ReqdAmount"].ItemValue));
                sResult += msg;

                msg = validateModel("Processor:", false, 0,
                    dicts["CBProcessorApprover1Reqd"].ItemValue == "1", Int32.Parse(dicts["ProcessorApprover1ReqdAmount"].ItemValue),
                    dicts["CBProcessorApprover2Reqd"].ItemValue == "1", Int32.Parse(dicts["ProcessorApprover2ReqdAmount"].ItemValue));
                sResult += msg;

                msg = validateModel("RPM:", false, 0,
                    dicts["CBRPMApprover1Reqd"].ItemValue == "1", Int32.Parse(dicts["RPMApprover1ReqdAmount"].ItemValue),
                    dicts["CBRPMApprover2Reqd"].ItemValue == "1", Int32.Parse(dicts["RPMApprover2ReqdAmount"].ItemValue));
                sResult += msg;
            }

            if (!string.IsNullOrEmpty(sResult))
            {
                return Json(new { status = false, statusMsg = sResult, data = vm });
            }

            bool result = configurationService.UpdateAccountThresholds(vm);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Account Thresholds setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = result, data = vm });
        }

        private string validateModel(string ModelName, bool CBAutoApprove, int AutoApproveAmount, bool CBApprover1Reqd, int Approver1ReqdAmount, bool CBApprover2Reqd, int Approver2ReqdAmount)
        {
            string msg = string.Empty;
            if (CBAutoApprove && CBApprover1Reqd && CBApprover2Reqd)
            {
                msg = (Approver2ReqdAmount > Approver1ReqdAmount) && (Approver1ReqdAmount > AutoApproveAmount) ?
                    msg : ModelName + "Invalid Input: It should be Approver 2's > Approver 1's > Automatic approve\r\n";
            }
            else if (CBApprover2Reqd && CBApprover1Reqd)
            {
                msg = (Approver2ReqdAmount > Approver1ReqdAmount) ?
                    msg : ModelName + "Invalid Input: It should be Approver 2's > Approver 1's\r\n";
            }
            else if (CBAutoApprove && CBApprover1Reqd)
            {
                msg = (Approver1ReqdAmount > AutoApproveAmount) ?
                   msg : ModelName + "Invalid Input: It should be Approver 1's > Automatic approve\r\n";
            }
            else if (CBApprover2Reqd && !CBApprover1Reqd)
            {
                msg = ModelName + "Invalid Input: Approver 1's check-box must be checked\r\n";
            }

            return msg;
        }
        public ActionResult LoadNotes(string applicationTypeID)
        {
            List<InternalNoteViewModel> result = configurationService.LoadAppSettingNotesByType(applicationTypeID);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        public ActionResult AddNote(string applicationTypeID, string notes)
        {
            var claimNote = configurationService.AddTransactionNote(applicationTypeID, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }

        public ActionResult ExportNotes(string applicationTypeID, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = configurationService.ExportInternalNotesToExcel(applicationTypeID, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public ActionResult AccountThresholdsFrom(AccountThresholdsVM vm)
        {
            return RedirectToAction("AccountThresholds");
        }

        #region Transaction Thresholds
        [ClaimsAuthorization(TreadMarksConstants.AdminSystemThresholds)]
        public ActionResult TransactionThresholds()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "systemThresholdsApp";
                ViewBag.SelectedMenu = "SystemThresholds";
                ViewBag.SelectedSubMenu = "Transactions";

                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public JsonResult LoadTransactionThresholds()
        {
            var result = configurationService.LoadTransactionThresholds();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            //check user write authority #OTSTM2-961
            //if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminSystemThresholds, (int)SecurityResultType.EditSave))
            //{
            //    return Json(new { status = false, statusMsg = "Unauthorized"});
            //};
            if (!ModelState.IsValid)
            {
                LogManager.LogInfo(string.Format("Server side validation fails: Updating transaction thresholds. UTC time:{0}", DateTime.UtcNow.ToString()));
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = configurationService.UpdateTransactionThresholds(transactionThresholdsVM);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) update transaction thresholds. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = true, data = transactionThresholdsVM });
        }
        #endregion
    }
}