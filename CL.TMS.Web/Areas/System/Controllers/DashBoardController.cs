﻿using MicrosoftSystem = System.Web;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Security.Interfaces;
using CL.TMS.ServiceContracts.SystemServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using Microsoft.AspNet.Identity;
using CL.TMS.DataContracts.DomainEntities;
using System.Security.Claims;
using CL.TMS.UI.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.Configuration;
using CL.TMS.UI.Common.UserInterface;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Diagnostics;
using CL.Framework.Common;
using System.Threading;
using System.Globalization;
using CL.TMS.Security;
using CL.TMS.Web.Infrastructure;
using CL.TMS.UI.Common.Security;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Resources;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class DashBoardController : BaseController
    {
        #region Fields
        private IDashBoardService dashBoardService;
        private IAuthentication authentication;
        private IUserService userService;
        private IClaimService claimService;
        private IRegistrantService registrantService;
        private IApplicationInvitationService applicationInvitationService;
        private IApplicationService applicationService;
        private IConfigurationsServices configurationService;
        private IMessageService messageService;
        #endregion

        #region Constructors
        public DashBoardController(IDashBoardService dashBoardService, IAuthentication authentication, IUserService userService, IClaimService claimService, IRegistrantService registrantService, IApplicationInvitationService applicationInvitationService, IApplicationService applicationService, IMessageService messageService, IConfigurationsServices configurationService)
        {
            this.authentication = authentication;
            this.dashBoardService = dashBoardService;
            this.userService = userService;
            this.claimService = claimService;
            this.registrantService = registrantService;
            this.applicationInvitationService = applicationInvitationService;
            this.applicationService = applicationService;
            this.configurationService = configurationService;
            this.messageService = messageService;
        }
        #endregion

        //main menu

        [ClaimsAuthorization(TreadMarksConstants.Dashboard)]
        public ActionResult Index(string isAdmin = "")
        {
            ViewBag.ngApp = "dashboardMainMenu";
            ViewBag.SelectedMenu = "Dashboard";
            ViewBag.isAdmin = isAdmin == "True";
            string startDate = string.IsNullOrEmpty(
                AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.UserDate").Value) ?
                "2009-09-01" : AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.UserDate").Value;
            //for OTS specific startDate from 2016-04-01;
            var isPro = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Site.ApplicationInstance");
            if (isPro == null || isPro.Value == "OTS")
            {
                startDate = "2016-04-01";
            }
            ViewBag.startDate = startDate;

            return View();
        }

        #region //applications
        public ActionResult Applications()
        {
            ViewBag.ngApp = "DashboardApp";
            ViewBag.SelectedMenu = "Applications";
            if (SecurityContextHelper.IsStaff())
            {
                if (SecurityContextHelper.CurrentVendor != null)
                {
                    return RedirectToParticipantPageActionResult();
                }
                //return RedirectToStaffPageActionResult();
                return View();
            }

            //CurrentVendor is not null if it is participant user
            return RedirectToParticipantPageActionResult();
        }

        private ActionResult RedirectToParticipantPageActionResult()
        {
            ViewBag.ngApp = "DashboardApp";

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;
                ViewBag.RegistrationNumber = vendor.RegistrationNumber;
                ViewBag.VendorId = vendor.VendorId;
                string sArea = string.Empty;
                switch (vendor.VendorType)
                {
                    case 1: //Stewward
                        sArea = "Steward";
                        break;
                    case 2: //Collector
                        sArea = "Collector";
                        break;
                    case 3: //Hauler
                        sArea = "Hauler";
                        break;
                    case 4: //Processor
                        sArea = "Processor";
                        break;
                    case 5: //RPM
                        sArea = "RPM";
                        break;

                }
                if (vendor.Status == null)
                {
                    vendor.Status = ApplicationStatusEnum.None.ToString();
                }
                switch (EnumHelper.ToEnum<ApplicationStatusEnum>(vendor.Status))
                {
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                        if (vendor.VendorType == 1)
                        {
                            return RedirectToAction("CustomerIndex", "Registration", new { area = sArea, cId = vendor.VendorId });
                        }
                        else
                        {
                            return RedirectToAction("RegistrationIndex", "Registration", new { area = sArea, vId = vendor.VendorId });
                        }
                    default:
                        return RedirectToAction("EmptyPage", "Common", new { area = "System" });

                }
                //Guid Token = applicationInvitationService.GetTokenByApplicationId(vendor.ApplicationID);
                //return RedirectToAction("Index", "Registration", new { area = sArea, id = Token });                             
            }

            return RedirectToAction("EmptyPage", "Common", new { area = "System" });
        }

        private ActionResult RedirectToStaffPageActionResult()
        {
            ViewBag.ngApp = "DashboardApp";

            var result = new List<ApplicationViewModel>();

            //Get first submit application from DB
            var firstApplicationViewMode = dashBoardService.GetFirstSubmitApplication();
            if (firstApplicationViewMode != null)
            {
                ViewBag.assignToMeUrl = "/" + firstApplicationViewMode.ParticipantType + "/Registration/ChangeStatus/";
                ViewBag.assignToMeId = firstApplicationViewMode.ID;
                ViewBag.assignToMeCompanyName = firstApplicationViewMode.BusinessName;
            }
            else
            {
                ViewBag.assignToMeUrl = "";
                ViewBag.assignToMeId = 0;
                ViewBag.assignToMeCompanyName = "";
            }

            var user = SecurityContextHelper.CurrentUser;

            ViewBag.curentUserId = user.Id;


            return View(result);
        }

        [HttpGet]
        public ActionResult GetFirstSubmitApplication()
        {
            var firstSubmitApplication = dashBoardService.GetFirstSubmitApplication();
            if (firstSubmitApplication != null)
            {
                firstSubmitApplication.CurrentUser = SecurityContextHelper.CurrentUser;
            }

            return new NewtonSoftJsonResult(firstSubmitApplication, false);
        }

        public JsonResult GetListOfApplications(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"] != null ? Convert.ToInt16(Request["order[0][column]"]) : 0);
            var orderBy = Request[string.Format("columns[{0}][name]", sortColumnIndex)] != null ? Convert.ToString(Request[string.Format("columns[{0}][name]", sortColumnIndex)]) : "ApplicationSubmitDate";
            var sortDirection = Convert.ToString(Request["order[0][dir]"] != null ? Request["order[0][dir]"] : "asc");

            //OTSTM2-982
            var columnSearchText = new Dictionary<string, string>
            {
                {"Date", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                {"Type", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                {"EmailAddress", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                {"Company", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                {"Contact",Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                {"Status", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                {"AssignedTo",Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
            };

            var alluserApplications = dashBoardService.GetAllApplicatoinBriefByFilter(param.Start, param.Length, searchText, orderBy, sortDirection, columnSearchText);

            //OTSTM2-982 comment out, because ApplicationSubmitDate is converted to local time in Repository layer
            ////OTSTM2-347
            //alluserApplications.DTOCollection.ForEach(s =>
            //{
            //    s.ApplicationSubmitDate = ConvertTimeFromUtc(s.ApplicationSubmitDate);
            //});

            var json = new
            {
                sEcho = param.Draw,
                iTotalRecords = alluserApplications.TotalRecords,
                iTotalDisplayRecords = alluserApplications.TotalRecords,
                aaData = alluserApplications.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        public JsonResult GetListOfRegistrants(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"] != null ? Convert.ToInt16(Request["order[0][column]"]) : 0);
            var orderBy = Request[string.Format("columns[{0}][name]", sortColumnIndex)] != null ? Convert.ToString(Request[string.Format("columns[{0}][name]", sortColumnIndex)]) : "ApplicationSubmitDate";
            var sortDirection = Convert.ToString(Request["order[0][dir]"] != null ? Request["order[0][dir]"] : "asc");

            //OTSTM2-931
            var columnSearchText = new Dictionary<string, string>
            {
                {"Reg", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                {"Company", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                {"Contact", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                {"ApprovedDate", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                {"Status",Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                {"BankStatus", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
            };

            var RegistrantList = dashBoardService.GetAllRegistrantByFilter(param.Start, param.Length, searchText, orderBy, sortDirection, columnSearchText);

            //OTSTM2-931 comment out, because CreatedDate is converted to local time in Repository layer
            //RegistrantList.DTOCollection.ForEach(i => i.CreatedDate = ConvertTimeFromUtc(i.CreatedDate));

            var json = new
            {
                sEcho = param.Draw,
                iTotalRecords = RegistrantList.TotalRecords,
                iTotalDisplayRecords = RegistrantList.TotalRecords,
                aaData = RegistrantList.DTOCollection,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        public ActionResult ExportToExcelForApplications(string searchtext)
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];
            if (string.IsNullOrWhiteSpace(sortcolumn))
            {
                sortcolumn = "ApplicationSubmitDate";
                sortdirection = "asc";
            }

            //OTSTM2-982
            var columnSearchText = new Dictionary<string, string>
            {
                {"Date", Request.QueryString[3]},
                {"Type", Request.QueryString[4]},
                {"EmailAddress", Request.QueryString[5]},
                {"Company", Request.QueryString[6]},
                {"Contact",Request.QueryString[7]},
                {"Status", Request.QueryString[8]},
                {"AssignedTo",Request.QueryString[9]},
            };

            var applications = dashBoardService.LoadApplications(searchtext, sortcolumn, sortdirection, columnSearchText);

            //OTSTM2-982 comment out, because ApplicationSubmitDate is converted to local time in Repository layer
            ////OTSTM2-347
            //applications.ForEach(s =>
            //{
            //    s.ApplicationSubmitDate = ConvertTimeFromUtc(s.ApplicationSubmitDate);
            //});

            var columnNames = new List<string>(){
                "Date", "Type", "Email Address", "Company", "Contact","Status", "Assigned To"
            };

            var delegates = new List<Func<ApplicationViewModel, string>>()
            {
                u => u.ApplicationSubmitDate==null?string.Empty: ((DateTime)u.ApplicationSubmitDate).ToString("yyyy/MM/dd h:mm tt"),
                u => u.ParticipantType,
                u => u.Email,
                u => u.BusinessName,
                u => u.ContactName,
                u => u.Status,
                u => u.assignedToName
            };

            var csv = applications.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Applications-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public ActionResult ExportToExcelForRegistrants(string searchtext)
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];
            if (string.IsNullOrWhiteSpace(sortcolumn))
            {
                sortcolumn = "Number";
                sortdirection = "asc";
            }

            //OTSTM2-931
            var columnSearchText = new Dictionary<string, string>
            {
                {"Reg", Request.QueryString[3]},
                {"Company", Request.QueryString[4]},
                {"Contact", Request.QueryString[5]},
                {"ApprovedDate", Request.QueryString[6]},
                {"Status", Request.QueryString[7]},
                {"BankStatus",Request.QueryString[8]},
            };

            var registrants = dashBoardService.LoadRegistrants(searchtext, sortcolumn, sortdirection, columnSearchText);

            //OTSTM2-931 comment out, because CreatedDate is converted to local time in Repository layer
            //registrants.ForEach(i => i.CreatedDate = ConvertTimeFromUtc(i.CreatedDate));

            var columnNames = new List<string>(){
                "Reg#", "Company", "Contact", "Approved Date", "Status", "Bank Status"
            };

            var delegates = new List<Func<RegistrantManageModel, string>>()
            {
                u => u.Number,
                u => u.BusinessName,
                u => u.Contact,
                u => u.CreatedDate.ToString(TreadMarksConstants.DateFormat),
                u => u.Status,
                u => u.BankInfoReviewStatus,
            };

            var csv = registrants.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Registrants-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public ActionResult ResendAppInvitationEmail(int applicationId)
        {
            string sEmail = applicationInvitationService.GetEmailByApplicationId(applicationId);
            Guid guid = applicationInvitationService.GetTokenByApplicationId(applicationId);
            ApplicationInvitation appinv = applicationInvitationService.UpdateInvitationDatetime(applicationId);

            int days = 7;
            try
            {
                days = int.Parse(CL.TMS.Configuration.AppSettings.Instance.GetSettingValue("Application.InvitationExpiryDays"));
            }
            catch (Exception)
            {
                days = 7;
            }

            applicationService.UpdateApplicatioExpireDate(applicationId, days);
            return RedirectToAction("ResendAppInvitationEmail", "AppInvitation", new { area = "System", applicationId = applicationId });
        }

        #endregion

        #region STC Premium
        public ActionResult LoadSTCPremiumList(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ItemID"}, //hide
                {1, "EventNumber" },
                {2, "Description"},
                {3, "DateAdded" }
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = configurationService.LoadSTCPremiumList(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadSTCEventsCount()
        {
            var result = configurationService.LoadSTCEventsCount();
            return Json(new { result, JsonRequestBehavior.AllowGet });
        }

        public ActionResult LoadSTCEventDetailsByEventNumber(string eventNumber)
        {
            var result = configurationService.LoadSTCEventDetailsByEventNumber(eventNumber);
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public ActionResult AddNewSTCEvent(STCPremiumListViewModel newEvent)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardSTC, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.AddNewSTCEvent(newEvent, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added STC Event. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,

            });
        }

        [HttpPost]
        public ActionResult UpdateSTCEvent(STCPremiumListViewModel updatedEvent)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardSTC, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.UpdateSTCEvent(updatedEvent, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) updated STC Event. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,
                eventNumber = updatedEvent.EventNumber
            });
        }

        [HttpPost]
        public ActionResult RemoveSTCEvent(int id, string eventNumber)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardSTC, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.RemoveSTCEvent(id, eventNumber);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) removed STC Event. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,
                eventNumber = eventNumber
            });
        }
        #endregion


        #region TCR Service Threshold
        public ActionResult LoadTCRServiceThresholdList()
        {
            var result = configurationService.LoadTCRServiceThresholdList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTCRServiceThreshold(TCRServiceThresholdViewModel vm)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardTCR, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };

            bool result = configurationService.AddTCRServiceThreshold(vm, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added TCR Service Threshold. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,

            });
        }

        [HttpPost]
        public ActionResult UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardTCR, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };

            bool result = configurationService.UpdateTCRServiceThreshold(vm, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) updated TCR Service Threshold. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result
            });
        }

        [HttpPost]
        public ActionResult RemoveTCRServiceThresohold(int id)
        {
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.DashboardTCR, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.RemoveTCRServiceThresohold(id, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) removed TCR Service Threshold. UTC Time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result
            });
        }

        public ActionResult ExportToExcelForTCR()
        {
            var tcrs = configurationService.ExportTCRServiceThresholds();
            var columnNames = new List<string>(){
                "Reg#", "Company", "Current Status", "Threshold Days", "Date added", "Date deleted"
            };
            var delegates = new List<Func<TCRListViewModel, string>>()
            {
                u => u.RegNumber,
                u => u.BusinessName,
                u=>  u.IsActive ? "Active" : "Inactive",
                u => u.ThresholdDays.ToString(),
                u => u.CreatedDate.ToString("yyyy/MM/dd h:mm tt"),
                u=>u.ModifiedDate.HasValue && u.IsDeleted.HasValue && u.IsDeleted.Value ?  u.ModifiedDate.Value.ToString("yyyy/MM/dd h:mm tt") : string.Empty
            };
            var csv = tcrs.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TCR Service Thresholds-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion
        // announcement      
        public ActionResult LoadDashboardAnnouncements()
        {

            var result = messageService.LoadDashboardAnnouncements();
            var temp = SecurityContextHelper.CurrentVendor;
            if (SecurityContextHelper.IsStaff())
            {
                if (temp != null)
                {
                    if (temp.VendorType == (int)ClaimType.Collector)
                    {
                        result = result.Where(x => x.Collector);
                    }
                    else if (temp.VendorType == (int)ClaimType.Processor)
                    {
                        result = result.Where(x => x.Processor);
                    }
                    else if (temp.VendorType == (int)ClaimType.Hauler)
                    {
                        result = result.Where(x => x.Hauler);
                    }
                    else if (temp.VendorType == (int)ClaimType.RPM)
                    {
                        result = result.Where(x => x.RPM);
                    }
                    else if (temp.VendorType == (int)ClaimType.Steward)
                    {
                        result = result.Where(x => x.Steward);
                    }
                }
                else
                {
                    result = result.Where(x => x.Staff);
                }
            }
            else
            {
                if (temp.VendorType == (int)ClaimType.Collector)
                {
                    result = result.Where(x => x.Collector);
                }
                else if (temp.VendorType == (int)ClaimType.Processor)
                {
                    result = result.Where(x => x.Processor);
                }
                else if (temp.VendorType == (int)ClaimType.Hauler)
                {
                    result = result.Where(x => x.Hauler);
                }
                else if (temp.VendorType == (int)ClaimType.RPM)
                {
                    result = result.Where(x => x.RPM);
                }
                else if (temp.VendorType == (int)ClaimType.Steward)
                {
                    result = result.Where(x => x.Steward);
                }
            }

            return Json(new { result = result.OrderByDescending(x => x.CreateDate) }, JsonRequestBehavior.AllowGet);
        }

        #region Private Methods

        #endregion
    }
}