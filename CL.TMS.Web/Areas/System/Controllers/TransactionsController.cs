﻿using SystemIO = System.IO;
using MicrosoftSystem = System.Web;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.RegistrantServices;
using System.Diagnostics;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Configuration;
using CL.Framework.Logging;
using CL.TMS.ExceptionHandling;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using CL.TMS.UI.Common.Helper;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using CL.TMS.Web.Infrastructure;
using Newtonsoft.Json;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class TransactionsController : BaseController
    {
        #region Properties
        private IRegistrantService registrantService;
        private ITransactionService transactionService;
        private IUserService userService;
        private IClaimService claimService;
        #endregion


        #region Constructor
        public TransactionsController(ITransactionService transactionService, IRegistrantService registrantService, IUserService userService, IClaimService claimService)
        {
            this.registrantService = registrantService;
            this.transactionService = transactionService;
            this.userService = userService;
            this.claimService = claimService;
        }
        #endregion


        #region Staff & Participant All Transactions
        //public ActionResult Index()
        [ClaimsAuthorization(TreadMarksConstants.TransactionsAllTransactions)]
        public ActionResult Index(string type = "ALL", int direction = 0, string searchText = "", int transID = 0)
        {
            ViewBag.ngApp = "AllTransactionApp";

            ViewBag.ClaimId = 0;
            ViewBag.TypeId = type;
            ViewBag.Direction = direction;

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = false;
            ViewBag.AllowAddTransaction = false;

            ViewBag.ClaimPeriod = "Unknown";
            ViewBag.SelectedMenu = "Transactions";
            ViewBag.SearchText = searchText;
            ViewBag.TransID = transID;


            //Clear Global search session if searchText is there
            if (!string.IsNullOrEmpty(searchText))
                Session["SelectedVendor"] = null;


            return ApplyRoutingLogic();
        }

        public ActionResult VendorStatusHistory(int id)
        {
            ViewBag.VendorID = id;

            return View("VendorStatusHistory", transactionService.GetVendorStatusDateRanges(id));
        }

        private ActionResult ApplyRoutingLogic()
        {
            ViewBag.RegistrationNumber = string.Empty;
            ViewBag.BusinessName = string.Empty;

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;
                ViewBag.RegistrationNumber = vendor.RegistrationNumber;
                ViewBag.BusinessName = vendor.BusinessName;

                if (vendor.IsVendor) ViewBag.VendorCode = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, vendor.VendorType).Code;
                else ViewBag.VendorCode = "ALL";
            }

            return View("Transactions");
        }

        public JsonResult GetAllTransactionsListHandler(DataGridModelExtend param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                  : string.Empty;
            string VendorType = param.DataSubType;

            Dictionary<int, string> columns = new Dictionary<int, string>();
            switch (VendorType)
            {
                case "RPM":
                    columns.Add(0, "Status");
                    columns.Add(1, "TransNum");
                    columns.Add(2, "Type");
                    columns.Add(3, "Start");
                    columns.Add(4, "SyncAdd");
                    columns.Add(5, "IncRegNum");
                    columns.Add(6, "OutRegNum");
                    columns.Add(7, "BadgeUser");
                    break;
                default:
                    columns.Add(0, "Status");
                    columns.Add(1, "iPadForm");
                    columns.Add(2, "TransNum");
                    columns.Add(3, "Type");
                    columns.Add(4, "Start");

                    columns.Add(5, "End");
                    columns.Add(6, "SyncAdd");
                    columns.Add(7, "IncRegNum");
                    columns.Add(8, "OutRegNum");
                    columns.Add(9, "BadgeUser");

                    break;
            }

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            int vendorId = (null == SecurityContextHelper.CurrentVendor) ? 0 : SecurityContextHelper.CurrentVendor.VendorId;
            string queryFilter = string.Empty;
            string sExtendSearchTxt = string.Empty;
            // Get vendor detail
            //var vendor = registrantService.GetVendorByNumber(vendorId.ToString());
            if (null != SecurityContextHelper.CurrentVendor)
            {
                switch (SecurityContextHelper.CurrentVendor.VendorType)
                {
                    case 1: //Stewward
                        //queryFilter = "Steward";
                        break;
                    case 2: //Collector
                        queryFilter = "UCR"; //do not show UCR transaction for Collector user
                        break;
                    case 3: //Hauler
                        //queryFilter = "Hauler";
                        break;
                    case 4: //Processor
                        //queryFilter = "Processor";
                        break;
                    case 5: //RPM
                        //queryFilter = "RPM";
                        if ((searchText.ToLower() == "sir") || (searchText.ToLower() == "si"))
                        {
                            sExtendSearchTxt = "sps";
                        }
                        if ((searchText.ToLower() == "sps") || (searchText.ToLower() == "sp"))
                        {
                            sExtendSearchTxt = "spsr";
                        }
                        break;
                }
            }

            var transactions = this.transactionService.LoadAllTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendorId, queryFilter, sExtendSearchTxt);

            if (TreadMarksConstants.RPM == VendorType)
            {
                transactions.DTOCollection.ForEach(c =>
                {
                    if (c.Type == "SPS")
                    {
                        c.Type = "SIR";
                    }
                    if (c.Type == "SPSR")
                    {
                        c.Type = "SPS";
                    }
                });
            }
            bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.TransactionsAllTransactions) == CL.TMS.Security.SecurityResultType.EditSave);
            transactions.DTOCollection.ForEach(c =>
            {
                c.TransactionListSecurity.PageReadonly = !bWritePermission;
                c.SyncAdd = !c.MobileFormat ? ConvertTimeFromUtc(c.SyncAdd) : c.SyncAdd;
                //c.Start = c.MobileFormat ? ConvertTimeFromUtc(c.Start) : c.Start;
                //c.End = c.MobileFormat ? ConvertTimeFromUtc(c.End) : c.End;
            });

            var json = new
            {
                PageReadonly = !bWritePermission,
                draw = param.Draw,
                iTotalRecords = transactions.TotalRecords,
                iTotalDisplayRecords = transactions.TotalRecords,
                data = transactions.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VoidTransaction(int transactionId)
        {
            transactionService.VoidTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AcceptTransaction(int transactionId)
        {
            transactionService.AcceptTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RejectTransaction(int transactionId)
        {
            transactionService.RejectTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ApproveTransaction(int transactionId)
        {
            transactionService.ApproveTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InvalidateTransaction(int transactionId)
        {
            transactionService.InvalidTransaction(transactionId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddTransactionRetailPrice(int transactionId, decimal retailPrice)
        {
            transactionService.AddTransactionRetailPrice(transactionId, retailPrice);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public NewtonSoftJsonResult UpdateTransactionProcessingStatus(int transactionId, string status, string currentStatus)
        {
            TransactionProcessingStatus processingStatus;

            if (status == "approve")
            {
                processingStatus = (currentStatus == TransactionProcessingStatus.Approved.ToString()) ? TransactionProcessingStatus.Unreviewed : TransactionProcessingStatus.Approved;
            }
            else if (status == "invalidate")
            {
                processingStatus = (currentStatus == TransactionProcessingStatus.Invalidated.ToString()) ? TransactionProcessingStatus.Unreviewed : TransactionProcessingStatus.Invalidated;
            }
            else return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

            var updated = transactionService.UpdateTransactionProcessingStatus(transactionId, processingStatus);
            if (updated) return new NewtonSoftJsonResult { Data = new { status = updated, statusMsg = MessageResource.ValidData, data = new { newStatus = processingStatus.ToString() } } };

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }

        #endregion

        #region Public Methods
        public JsonResult GetTransactionListHandler(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"] != null ? Convert.ToInt16(Request["order[0][column]"]) : 0);
            var orderBy = Request[string.Format("columns[{0}][name]", sortColumnIndex)] != null ? Convert.ToString(Request[string.Format("columns[{0}][name]", sortColumnIndex)]) : "SyncDate";
            var sortDirection = Convert.ToString(Request["order[0][dir]"] != null ? Request["order[0][dir]"] : "asc");

            int VendorId = (null == SecurityContextHelper.CurrentVendor) ? 0 : SecurityContextHelper.CurrentVendor.VendorId;
            var transactions = transactionService.LoadTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, VendorId);

            var result = from d in transactions.DTOCollection
                         select new[]{
                                d.TransactionStatusTypeNameKey,
                                d.DeviceName,
                                d.FriendlyId.ToString(),
                                d.TransactionTypeShortNameKey,
                                d.CreatedDate.ToString(TreadMarksConstants.DateFormat),
                                d.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                                d.SyncDate.GetValueOrDefault().ToString(TreadMarksConstants.DateFormat),
                                d.IncomingRegistrationNumber.ToString(),
                                d.OutgoingRegistrationNumber.ToString(),
                                d.BadgeId,
                                d.TransactionStatusTypeNameKey,//for Actions to generate action gear
                                d.ID.ToString()
                         };

            var json = new
            {
                sEcho = param.Draw,
                iTotalRecords = transactions.TotalRecords,
                iTotalDisplayRecords = transactions.TotalRecords,
                aaData = result
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TransactionAction(Guid Token, string transId, string action)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int applicationID;
            int.TryParse(transId, out applicationID);

            if (applicationID == 0)
            {
                throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
            }

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        public NewtonSoftJsonResult GetVendorInfo(string vendorNumber, DateTime transactionDate, string[] vendorCodeList)
        {
            // Check vendor is same as current vendor
            if (SecurityContextHelper.CurrentVendor != null && SecurityContextHelper.CurrentVendor.RegistrationNumber == vendorNumber)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = new { SameAsCurrent = true } } };
            }

            try
            {
                // Get vendor detail
                var vendor = registrantService.GetVendorByNumber(vendorNumber);

                // If vendor is not found or not in the vendor type list
                var validVendorIdList = new List<int>();
                foreach (var vendorCode in vendorCodeList) { validVendorIdList.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, vendorCode).DefinitionValue); }
                if (vendor == null || !validVendorIdList.Contains(vendor.VendorType)) return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = new { IsFound = false } } };

                if (!transactionService.IsVendorActive(vendor.VendorId, transactionDate)) return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = new { IsFound = true, IsActive = false } } };

                // A valid vendor found
                var VendorInfoModel = new
                {
                    IsFound = true,
                    IsActive = true,
                    SameAsCurrent = false,
                    Info = new
                    {
                        VendorTypeCode = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, vendor.VendorType).Code,
                        VendorTypeId = vendor.VendorType,
                        Name = vendor.BusinessName,
                        Address = new
                        {
                            Address1 = vendor.Address.Address1,
                            Address2 = vendor.Address.Address2,
                            Address3 = vendor.Address.Address3,
                            City = vendor.Address.City,
                            Province = vendor.Address.Province,
                            PostalCode = vendor.Address.PostalCode,
                        },
                        Active = vendor.IsActive
                    }
                };

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = VendorInfoModel } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.InvalidData, data = new { IsFound = false } } };
            }
        }

        public NewtonSoftJsonResult CheckFormNumberDuplication(int claimId, long formNumber)
        {
            var existingTransaction = transactionService.GetTransactionByFriendlyID(formNumber);

            if (existingTransaction != null && (existingTransaction.Status != CL.TMS.Common.Enum.TransactionStatus.Voided.ToString() && existingTransaction.Status != CL.TMS.Common.Enum.TransactionStatus.Rejected.ToString()))
            {
                if (claimService.IsTransactionInSameClaim(claimId, existingTransaction.ID))
                {
                    var transactionDuplicationModel = new
                    {
                        isFound = true,
                        inSameClaim = true
                    };
                    return new NewtonSoftJsonResult { Data = new { data = transactionDuplicationModel } };
                }
                else
                {
                    var transactionDuplicationModel = new
                    {
                        isFound = true,
                        inSameClaim = false
                    };
                    return new NewtonSoftJsonResult { Data = new { data = transactionDuplicationModel } };
                }
            }
            else
            {
                var transactionDuplicationModel = new
                {
                    isFound = false,
                    inSameClaim = false
                };
                return new NewtonSoftJsonResult { Data = new { data = transactionDuplicationModel } };
            }
        }

        public NewtonSoftJsonResult validatePTRNumber(long ptrNumber)
        {
            var ptrWeightDetail = transactionService.GetPTRWeightDetail(ptrNumber);

            if (ptrWeightDetail == null) return new NewtonSoftJsonResult { Data = new { data = new { isFound = false } } };

            return new NewtonSoftJsonResult { Data = new { data = new { isFound = true, detail = ptrWeightDetail } } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddTransactionNote(int id, string note)
        {
            var transactionNote = transactionService.AddTransactionNote(id, note);
            transactionNote.AddedOn = ConvertTimeFromUtc(transactionNote.AddedOn);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionNote } };
        }

        //OTSTM2-765
        [HttpPost]
        public NewtonSoftJsonResult GetAllTransactionNote(int id)
        {
            var allTransactionNoteList = transactionService.GetAllTransactionNote(id);
            allTransactionNoteList.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = allTransactionNoteList } };
        }

        [HttpPost]
        public NewtonSoftJsonResult GetTransactionDetailData(int id, string vendorType, bool adjRequested, int claimId, int? adjustmentId)
        {
            var transactionDetailData = transactionService.GetTransactionHandlerData(id, vendorType, adjRequested, adjustmentId, claimId);
            transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType)
        {
            var currentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
            var type = transactionType;
            var direction = (transactionType == "All") ? 0 : 1;
            if (transactionType.Substring(0, 1) == "-")
            {
                type = transactionType.Substring(1, (transactionType.Length - 1));
                direction = 2;
            }

            var transactionDetailData = claimService.GetTransactionSearchList(claimId, transactionFormat, processingStatus, type, direction, currentVendorId);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
        }
        [HttpPost]
        public NewtonSoftJsonResult ToDisableTransactionActionGear(int claimId, int transactionId)
        {
            var IsAdjPendingAndTransactionUnreviewed = this.transactionService.IsTransAdjPendingAndUnreviewed(claimId, transactionId);

            string OtherClaimStatus = claimService.checkOtherClaimStatus(claimId, transactionId);

            string IsVendorInactive = transactionService.CheckTransactionVendorStatus(transactionId, false);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = (IsAdjPendingAndTransactionUnreviewed || string.Equals(OtherClaimStatus, "Approved")), vendorStatus = IsVendorInactive } };
        }

        [HttpPost]
        public NewtonSoftJsonResult GetTransactionHandlerClaimData(int claimId)
        {
            string periodName = "Unknown";

            if (claimId != 0)
            {
                var period = claimService.GetClaimPeriod(claimId);
                periodName = period.ShortName;
            }

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = new { PeriodName = periodName } } };
        }

        [HttpPost]
        public NewtonSoftJsonResult GetTransactionAdjustmentData(int id)
        {
            var adjData = transactionService.GetTransactionAdjustmentModel(id);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = adjData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitTCRAdjustment(TCRTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitDOTAdjustment(DOTTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitHITAdjustment(HITTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitPTRAdjustment(PTRTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitSTCAdjustment(STCTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitUCRAdjustment(UCRTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitRTRAdjustment(RTRTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);
                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitPITAdjustment(PITTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);
            
                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitSPSAdjustment(SPSTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                //if (adjustmentDataModel.SoldTo == "registered") //OTSTM2-138
                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitSPSRAdjustment(SPSRTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);
                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitDORAdjustment(DORTransactionAdjViewModel adjustmentDataModel, string vendorType, int claimId, bool getUpdatedData)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

                var claimPeriod = this.claimService.GetClaimPeriod(claimId);
                var claimAssociation = this.claimService.GetTransactionClaimAssociationDetail(adjustmentDataModel.TransactionId);

                transactionService.AdjustTransaction(adjustmentDataModel, claimPeriod);

                HandleTransactionAdjustmentNotification(adjustmentDataModel, claimAssociation);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(adjustmentDataModel.TransactionId, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitAdjustmentReview(int id, bool accepted, bool getUpdatedData, string notes, int claimId, string vendorType)
        {
            try
            {
                var reviewStatus = (accepted) ? TransactionAdjustmentStatus.Accepted : TransactionAdjustmentStatus.Rejected;

                transactionService.SubmitAdjustmentStatusChange(id, reviewStatus, getUpdatedData, notes);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(id, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitAdjustmentRecall(int id, bool getUpdatedData, string notes, int claimId, string vendorType)
        {
            try
            {
                var reviewStatus = TransactionAdjustmentStatus.Recalled;

                transactionService.SubmitAdjustmentStatusChange(id, reviewStatus, getUpdatedData, notes);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(id, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult SubmitRejectAdjustment(int id, bool getUpdatedData, string notes, int claimId, string vendorType)
        {
            try
            {
                var reviewStatus = TransactionAdjustmentStatus.Rejected;

                transactionService.SubmitAdjustmentStatusChange(id, reviewStatus, getUpdatedData, notes);

                if (getUpdatedData)
                {
                    var transactionDetailData = transactionService.GetTransactionHandlerData(id, vendorType, false, null, claimId);
                    transactionDetailData.InternalNote.NoteList.ForEach(i => i.AddedOn = ConvertTimeFromUtc(i.AddedOn));

                    return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionDetailData } };
                }

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            }
        }
        #endregion

        public ActionResult AllTransactions()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            if (null != SecurityContextHelper.CurrentVendor)
            {
                ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;
            }
            return View("ALLTransactions");
        }

        public ActionResult ExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";

            List<Func<AllTransactionsViewModel, string>> delegates = new List<Func<AllTransactionsViewModel, string>>();

            int vendorId = (null == SecurityContextHelper.CurrentVendor) ? 0 : SecurityContextHelper.CurrentVendor.VendorId;
            string queryFilter = string.Empty;
            // Get vendor detail
            //var vendor = registrantService.GetVendorByNumber(vendorId.ToString());
            if (null != SecurityContextHelper.CurrentVendor)
            {
                switch (SecurityContextHelper.CurrentVendor.VendorType)
                {
                    case 1: //Stewward
                        //queryFilter = "Steward";
                        break;
                    case 2: //Collector
                        queryFilter = "UCR";
                        break;
                    case 3: //Hauler
                        //queryFilter = "Hauler";
                        break;
                    case 4: //Processor
                        //queryFilter = "Processor";
                        break;
                    case 5: //RPM
                        //queryFilter = "RPM";
                        break;
                }
            }

            #region column define
            switch (dataSubType)
            {
                case "RPM":
                    columns.Add("Status");
                    columns.Add("Trans #");
                    columns.Add("Type");
                    columns.Add("Start");
                    columns.Add("Add");
                    columns.Add("Inc. Reg.#");

                    columns.Add("Out. Reg.#");
                    columns.Add("User");

                    delegates = new List<Func<AllTransactionsViewModel, string>>()
                    {
                        u => u.Status,
                        u => u.TransNum.ToString(),
                        u => u.Type.ToString(),
                        u => (u.MobileFormat ? u.Start.ToString() : u.Start.ToShortDateString()),
                        u => u.SyncAdd.ToString(),
                        u => u.IncRegNum,

                        u => u.OutRegNum,
                        u => u.BadgeUser,
                    };
                    break;


                default:
                    columns.Add("Status");
                    columns.Add("iPad/Form");
                    columns.Add("Trans #");
                    columns.Add("Type");
                    columns.Add("Start");

                    columns.Add("End");
                    columns.Add("Sync/add");
                    columns.Add("Inc. Reg.#");
                    columns.Add("Out. Reg.#");
                    columns.Add("Badge/User");

                    delegates = new List<Func<AllTransactionsViewModel, string>>()
                    {
                        u => u.Status,
                        u => u.iPadForm,
                        u => u.TransNum.ToString(),
                        u => u.Type.ToString(),
                        u => (u.MobileFormat ? u.Start.ToString() : u.Start.ToShortDateString()),

                        u => (u.MobileFormat) ? u.End.ToString() : string.Empty,
                        u => u.SyncAdd.ToString(),
                        u => u.IncRegNum,
                        u => u.OutRegNum,
                        u => u.BadgeUser,
                    };

                    break;
            };
            #endregion

            var Transactions = this.transactionService.GetTransactionDetailsExport(search, sortcolumn, sortdirection, vendorId, transactionType, claimId, dataSubType, queryFilter);

            if (TreadMarksConstants.RPM == dataSubType)
            {
                Transactions.ForEach(c =>
                {
                    if (c.Type == "SPS")
                    {
                        c.Type = "SIR";
                    }
                    if (c.Type == "SPSR")
                    {
                        c.Type = "SPS";
                    }
                });
            }

            Transactions.ForEach(c =>
            {
                c.SyncAdd = !c.MobileFormat ? ConvertTimeFromUtc(c.SyncAdd) : c.SyncAdd;
                //c.Start = c.MobileFormat ? ConvertTimeFromUtc(c.Start) : c.Start;
                //c.End = c.MobileFormat ? ConvertTimeFromUtc(c.End) : c.End;
            });

            string csv = Transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }


        private void HandleTransactionAdjustmentNotification(ITransactionAdjViewModel adjModel, TransactionClaimAssociationDetailViewModel claimAssociation)
        {
            if (SecurityContextHelper.IsStaff())
            {
                claimAssociation.Associations.ForEach(i =>
                {
                    var notificationDetail = new TransactionAdjustmentNotificationDetailViewModel()
                    {
                        TransactionType = adjModel.TransactionType,
                        TransactionNumber = adjModel.TransactionNumber.ToString(),
                        ClaimPeriod = i.ClaimPeriodName,
                        Email = i.ContactEmail,
                        InitiatedBy = "Staff"
                    };

                    TransactionCommonHelper.SendTransactionAdjustmentEmailNotification(notificationDetail);
                });
            }
            else
            {
                var currentVendorId = SecurityContextHelper.CurrentVendor.VendorId;

                claimAssociation.Associations.ForEach(i =>
                {
                    if (currentVendorId != i.ParticipantID)
                    {
                        var notificationDetail = new TransactionAdjustmentNotificationDetailViewModel()
                        {
                            TransactionType = adjModel.TransactionType,
                            TransactionNumber = adjModel.TransactionNumber.ToString(),
                            ClaimPeriod = i.ClaimPeriodName,
                            Email = i.ContactEmail,
                            InitiatedBy = "the user of Reg# " + SecurityContextHelper.CurrentVendor.RegistrationNumber
                        };

                        TransactionCommonHelper.SendTransactionAdjustmentEmailNotification(notificationDetail);
                    }
                });
            }
        }

        #region STC Premium Implementation
        [HttpPost]
        public ActionResult EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate)
        {
            STCEventNumberValidationStatus result = transactionService.EventNumberValidationCheck(eventNumber, transactionStartDate);
            return Json(new { status = result });
        }
        #endregion
    }
}