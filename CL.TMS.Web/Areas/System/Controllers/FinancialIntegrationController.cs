﻿using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.Helper;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ServiceContracts.StewardServices;
using MicrosoftSystem = System.Web;
using CL.TMS.Framework.DTO;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.UI.Common;
using CL.TMS.ServiceContracts.RPMServices;
using System.IO;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Reflection;
using CL.TMS.Common;
using CL.TMS.ServiceContracts.CollectorServices;
using SystemIO = System.IO;
using System.Text;
using System.Net.Mime;
using CL.TMS.ExceptionHandling;
using CL.TMS.UI.Common.UserInterface;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class FinancialIntegrationController : BaseController
    {
        private readonly IHaulerRegistrationInvitationService registrationInvitationService;
        private readonly IApplicationService applicationService;
        private readonly IUserService userService;
        private readonly IRegistrantService registrantService;
        private readonly IRPMClaimService rpmClaimService;
        private readonly IProcessorClaimService processorClaimService;
        private readonly IHaulerClaimService haulerClaimService;
        private readonly ITSFRemittanceService tsfRemittanceService;
        private readonly ICollectorClaimService collectorClaimService;
        private readonly IMessageService messageService;

        public FinancialIntegrationController(IHaulerRegistrationService haulerRegistrationService, IHaulerRegistrationInvitationService applicationInvitationService,
            IUserService userService, IApplicationService applicationService, IRegistrantService registrantService, IHaulerClaimService haulerClaimService,
            ITSFRemittanceService tsfRemittanceService, IRPMClaimService rpmClaimService, IProcessorClaimService processorClaimService,
            ICollectorClaimService collectorClaimService, IMessageService messageService)
        {
            this.registrationInvitationService = applicationInvitationService;
            this.userService = userService;
            this.applicationService = applicationService;
            this.registrantService = registrantService;
            this.haulerClaimService = haulerClaimService;
            this.tsfRemittanceService = tsfRemittanceService;
            this.rpmClaimService = rpmClaimService;
            this.processorClaimService = processorClaimService;
            this.collectorClaimService = collectorClaimService;
            this.messageService = messageService;
        }


        [ClaimsAuthorizationAttribute(TreadMarksConstants.FinancialIntegration)]
        public ActionResult Index()
        {
            ViewBag.ngApp = "financialIntegrationApp";
            ViewBag.SelectedMenu = "FinancialIntegration";
            registrantService.GpStatusCheck();
            return View();
        }
        public ActionResult IsPageEditable()
        {
            var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.FinancialIntegrationGPFinancialIntegration) == Security.SecurityResultType.EditSave);
            var result = new
            {
                PageEditable = isEditable
            };
            return new NewtonSoftJsonResult(result);
        }

        #region Banking Information

        [RedirectAction(false)]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsAllApplicationsBankingActions)]
        public ActionResult BankingInformation(int id)
        {
            var vendor = registrantService.GetSingleVendorByID(id);
            var bankingInformationModel = new BankingInformationRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            if (vendor != null)
            {
                bankingInformationModel.applicationId = vendor.ApplicationID ?? 0;
                bankingInformationModel.RegistrationNumber = vendor.Number;
                bankingInformationModel.OperatingName = vendor.OperatingName;
                bankingInformationModel.VendorId = id;

                ViewBag.AddFilesURL = "/System/FileUpload/UploadFiles";
                ViewBag.GetFilesURL = "/System/FileUpload/GetUploadedFiles";
                ViewBag.DeleteFileURL = "/System/FileUpload/DeleteUploadedFile";

            }
            return View(bankingInformationModel);
        }

        [HttpPost]
        [RedirectAction(false)]
        public ActionResult BankingInformation(BankingInformationRegistrationModel bankInformationModel)
        {
            int vendorId = bankInformationModel.VendorId;

            var vendor = registrantService.GetSingleVendorByID(vendorId);

            if (vendor.BankInformations.Count == 0)
            {
                registrantService.CreateBankInformation(vendor, bankInformationModel);

                userService.UpdateInvitationRedirectUrl(vendorId, "");

                SendBankInfoConfirmEmail(vendor);
            }

            //Clear current redirect url
            if (SecurityContextHelper.CurrentVendor != null)
                SecurityContextHelper.CurrentVendor.RedirectUrl = string.Empty;

            string logContent = "<strong>Participant submitted</strong> Banking Information.";
            if (vendor.ApplicationID.HasValue)
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, vendor.ApplicationID ?? 0);
            }
            return RedirectPermanent("/");
        }

        public NewtonSoftJsonResult GetBankinfoDetail(int bankId)
        {
            BankInformation bankinfoData = registrantService.GetBankInformationById(bankId);

            return new NewtonSoftJsonResult
            {
                Data = new
                {
                    status = true,
                    statusMsg = MessageResource.ValidData,
                    data = new
                    {
                        bankName = bankinfoData.BankName,
                        bankNumber = bankinfoData.BankNumber,
                        accountNumber = bankinfoData.AccountNumber,
                        transitNumber = bankinfoData.TransitNumber,
                        reviewStatus = bankinfoData.ReviewStatus,
                        bankinfoId = bankinfoData.ID,
                        ccEmail = bankinfoData.CCEmail,
                        email = bankinfoData.Email,
                        supportDocOption = bankinfoData.SupportDocOption,
                        submittedOnDate = ConvertTimeFromUtc(bankinfoData.SubmittedOnDate),
                        notifyToPrimaryContactEmail = bankinfoData.NotifyToPrimaryContactEmail, //OTSTM2-906
                    }
                }
            };
        }

        [HttpPost]
        public ActionResult Savebankinfo(BankInformation bankinfoData)
        {
            string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>changed</strong> Banking Information.";
            if (!string.IsNullOrEmpty(bankinfoData.ReviewStatus) && bankinfoData.ReviewStatus == BankInfoReviewStatus.Submitted.ToString())
            {
                logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>approved</strong> Banking Information.";
            }
            bankinfoData.ReviewStatus = BankInfoReviewStatus.Approved.ToString();
            Vendor vendor = this.registrantService.UpdateBankInformation(bankinfoData);

            if (vendor.ApplicationID.HasValue)
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, vendor.ApplicationID ?? 0);
                //Publish activity event
                DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(vendor.ApplicationID.Value);
            }

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult RejectBankinfo(int bankId, int vendorId)
        {
            BankInformation bankinfoData = registrantService.GetBankInformationById(bankId);
            var vendor = registrantService.GetSingleVendorByID(vendorId);
            var approvedHaulerApplicationModel = new ApplicationEmailModel()
            {
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                Address1 = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City,
                City = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).City,
                ProvinceState = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Province,
                PostalCode = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).PostalCode,
                Country = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1).Country,
                Email = bankinfoData.Email,
            };

            if (bankinfoData.NotifyToPrimaryContactEmail)
            {
                //Find primary contact
                Contact primaryContact = null;
                var contractAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 3);
                if (contractAddress != null)
                {
                    primaryContact = contractAddress.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                }
                if (primaryContact == null)
                {
                    var businessAddess = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                    if (businessAddess != null)
                    {
                        primaryContact = businessAddess.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                    }
                }

                if (primaryContact != null)
                {
                    approvedHaulerApplicationModel.Email = primaryContact.Email;
                }
            }

            string message = "";
            if (EmailContentHelper.IsEmailEnabled("BankInformationRejected"))
                message = ComposeAndSendRejectEMail(approvedHaulerApplicationModel);

            userService.UpdateInvitationRedirectUrl(vendorId, "/System/FinancialIntegration/BankingInformation/" + vendorId);
            this.registrantService.DeleteBankInformation(bankId, vendorId);

            string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>rejected</strong> Banking Information.";
            if (vendor.ApplicationID.HasValue)
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Collector, logContent, vendor.ApplicationID ?? 0);
            }

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }

        private bool SendBankInfoConfirmEmail(Vendor vendor)
        {
            string currentUserId = ResourceActionAuthorizationHandler.GetResourceValue("Id");

            int userId;
            int.TryParse(currentUserId, out userId);

            userService.RemoveAfterLoginRedirectUrl(userId);

            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            string subject = MessageResource.BankInformationSubmitted;
            string currentUserName = ResourceActionAuthorizationHandler.GetResourceValue("username");
            string emailBody = string.Format(Resources.MessageResource.BankInformationSubmittedEmailBody, vendor.Number, currentUserName);


            ApplicationEmailModel applicationEmailModel;
            var applicationId = vendor.ApplicationID;
            if (applicationId != null)
            {
                applicationEmailModel = this.applicationService.GetApprovedApplicantInformation(applicationId.Value);
                string assignToUserEmail = userService.FindUserByUserId(applicationEmailModel.AssignedToUserID.Value).Email;
                MailAddress to = new MailAddress(assignToUserEmail);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(from.ToString(), to.ToString(), applicationEmailModel.Email, null, subject, emailBody, null, from.ToString());
                });
            }
            else
            {
                applicationEmailModel = this.registrantService.GetApprovedRegistrantInformation(vendor.ID);
                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(from.ToString(), applicationEmailModel.Email, null, null, subject, emailBody, null, from.ToString());
                });

            }
            return true;
        }

        private string ComposeAndSendRejectEMail(ApplicationEmailModel approvedApplicationModel)
        {
            string message = string.Empty;

            try
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.RejectBankinfoEmailTemplLocation"));
                //string emailBody = SystemIO.File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("BankInformationRejected");
                string subject = EmailContentHelper.GetSubjectByName("BankInformationRejected");

                emailBody = emailBody
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "Hauler")
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, approvedApplicationModel.BusinessLegalName)
                    .Replace("@RegistrationNumber", approvedApplicationModel.RegistrationNumber)
                    .Replace("@BusinessName", approvedApplicationModel.BusinessName)
                    .Replace("@BusinessAddress1", approvedApplicationModel.Address1)
                    .Replace("@BusinessCity", approvedApplicationModel.City)
                    .Replace("@BusinessProvinceState", approvedApplicationModel.ProvinceState)
                    .Replace("@BusinessCountry", approvedApplicationModel.Country)
                    .Replace("@BusinessPostalCode", approvedApplicationModel.PostalCode);

                AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"));
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                treadMarksLogo.ContentId = "@ApplicationLogo";
                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Boolean.Parse(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                email.SendEmail(SiteSettings.Instance.GetSettingValue("Company.Email"), approvedApplicationModel.Email, null, null, subject, emailBody, null, null, alternateViewHTML);

                message = string.Format("Reject Banking Information Email was successfully sent to {0} at email {1}", approvedApplicationModel.BusinessLegalName, approvedApplicationModel.Email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                message = ex.Message;
            }

            return message;
        }

        #endregion

        #region GP Integration
        [HttpPost]
        public ActionResult CreateBatch(string type = "")
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            //R
            if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.Participant), StringComparison.OrdinalIgnoreCase))
            {
                var result = registrantService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                    registrantService.AddGPActivityForCreateBatch(result.ID);

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            //S
            else if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.Steward), StringComparison.OrdinalIgnoreCase))
            {
                var result = tsfRemittanceService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                {
                    foreach (var batchID in result.BatchIDs)
                    {
                        registrantService.AddGPActivityForCreateBatch(batchID);

                        //needs to be fixed later when we show both Ids in the confirmation message 
                        result.ID = batchID;
                    }
                }

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            //M
            else if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.RPM), StringComparison.OrdinalIgnoreCase))
            {
                var result = rpmClaimService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                {
                    registrantService.AddGPActivityForCreateBatch(result.ID);
                }

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            //P
            else if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.Processor), StringComparison.OrdinalIgnoreCase))
            {
                var result = processorClaimService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                {
                    registrantService.AddGPActivityForCreateBatch(result.ID);
                }

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            //C
            else if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.Collector), StringComparison.OrdinalIgnoreCase))
            {
                var result = collectorClaimService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                {
                    registrantService.AddGPActivityForCreateBatch(result.ID);
                }

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            //H
            else if (string.Equals(type, GpHelper.GetGpiTypeID(GpManager.Hauler), StringComparison.OrdinalIgnoreCase))
            {
                var result = haulerClaimService.CreateBatch();
                //OTSMTM2-745
                if (!result.Warnings.Any() && !result.Errors.Any())
                {
                    registrantService.AddGPActivityForCreateBatch(result.ID);
                }

                return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
            }
            return Json(new { status = false, statusMsg = MessageResource.InvalidData });
        }
        [HttpPost]
        public ActionResult PutOnHold(int gpiBatchEntryId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = registrantService.PutOnHold(gpiBatchEntryId);
            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }
        [HttpPost]
        public JsonResult PutOffHold(int gpiBatchEntryId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = registrantService.PutOffHold(gpiBatchEntryId);

            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }

        public ActionResult GetExport(int batchId, string panelName)
        {
            var obj = registrantService.GetExport(batchId);
            using (var package = new ExcelPackage())
            {
                foreach (var item in obj)
                {
                    var customers = item as List<RrOtsRmCustomer>;
                    if (customers != null && customers.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(customers, "RR_OTS_CUSTOMERS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(customers, "RR_TM_CUSTOMERS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var vendors = item as List<RrOtsPmVendor>;
                    if (vendors != null && vendors.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(vendors, "RR_OTS_VENDORS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(vendors, "RR_TM_VENDORS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var transactions = item as List<RrOtsRmTransaction>;
                    if (transactions != null && transactions.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(transactions, "RR_OTS_RM_TRANSACTIONS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(transactions, "RR_TM_RM_TRANSACTIONS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var transDistributions = item as List<RrOtsRmTransDistribution>;
                    if (transDistributions != null && transDistributions.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(transDistributions, "RR_OTS_RM_TRANS_DISTRIBUTIONS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(transDistributions, "RR_TM_RM_TRANS_DISTRIBUTIONS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var cashReceipts = item as List<RrOtsRmCashReceipt>;
                    if (cashReceipts != null && cashReceipts.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(cashReceipts, "RR_OTS_RM_CASH_RECEIPT", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(cashReceipts, "RR_TM_RM_CASH_RECEIPT", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var applies = item as List<RrOtsRmApply>;
                    if (applies != null && applies.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(applies, "RR_OTS_RM_APPLY", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(applies, "RR_TM_RM_APPLY", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var transactions_pm = item as List<RrOtsPmTransaction>;
                    if (transactions_pm != null && transactions_pm.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(transactions_pm, "RR_OTS_PM_TRANSACTIONS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(transactions_pm, "RR_TM_PM_TRANSACTIONS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                    var transDistributions_pm = item as List<RrOtsPmTransDistribution>;
                    if (transDistributions_pm != null && transDistributions_pm.Any())
                    {
                        //ExcelFileHandler.ExportListWithPackage(transDistributions_pm, "RR_OTS_PM_TRANS_DISTRIBUTIONS", package, new List<string>() { "PSTGDATE" });
                        ExcelFileHandler.ExportListWithPackage(transDistributions_pm, "RR_TM_PM_TRANS_DISTRIBUTIONS", package, new List<string>() { "PSTGDATE" }); //OTSTM2-1124
                    }
                }

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("TreadMark {0} Interface CoA Import_BatchNo_{1}-{2:yyyy-MM-dd-HH-mm-ss}.xlsx", panelName, batchId, ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }

                return RedirectToAction("PageNotFound", "Error");
            }
        }
        [HttpPost]
        public ActionResult PostBatch(int batchId, bool IsPost, bool IsRepost)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            ResponseMsgBase result = null;

            if (IsPost)
            {
                result = registrantService.PostBatch(batchId, "Posted");
            }
            else if (IsRepost)
            {
                result = registrantService.PostBatch(batchId, "Re-posted");
            }

            result.ID = batchId;

            if (result.Errors.Count == 0)
            {
                //perform GPStatusCheck from Legacy 
            }
            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }
        [HttpPost]
        public JsonResult DeleteEntryFromBatch(int batchEntryId)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = registrantService.RemoveFromBatch(batchEntryId);

            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }
        [HttpPost]
        public JsonResult LoadGPByType(DataGridModel param, string type = "")
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "BatchNum"},
                {1, "Count"},
                {2, "Modified"},
                {3, "Status"},
                {4, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            int recordsTotal = 0;

            var result = registrantService.GetAllGpBatchesPagination(param.Start, param.Length, searchText, orderBy, sortDirection, type);

            result.DTOCollection.ForEach(c =>
            {
                c.Modified = ConvertTimeFromUtc(c.Modified);
            });

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadGpTransactionsByBatch(DataGridModel param, int batchId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "GPTransactionNum"},
                {1, "Status"},
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = registrantService.GetGpTransactionsByBatchIdPagination(param.Start, param.Length, searchText, orderBy, sortDirection, batchId);
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGpBatchesJson(string type = "")
        {
            var result = registrantService.GetGpBatches(type);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGpTransactionsJson(string batchId)
        {
            var result = registrantService.GetGpTransactionsByBatchId(int.Parse(batchId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Admin Financial Integration 
        [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.AdminFinancialIntegration)]
        public ActionResult AdminIndex()
        {
            ViewBag.ngApp = "adminFinancialIntegrationApp";
            ViewBag.SelectedMenu = "AdminFinancialIntegration";

            var adminFICategory = AppDefinitions.Instance.TypeDefinitions[DefinitionCategory.AdminFICategory.ToString()].Select(x => new BaseItemModel()
            {
                ItemName = x.Name,
                ItemValue = x.DefinitionValue,
            })
            .OrderBy(x => x.DisplayOrder).ToList();

            ViewBag.AdminFICategoryList = adminFICategory;

            return View();
        }
        [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.AdminFinancialIntegration)]
        public NewtonSoftJsonResult GetData(int categoryID)
        {
            var query = registrantService.GetAdminFinancialIntegration();

            var result = new FICategoryVM()
            {
                steward = query.Where(c => c.CategoryID == (int)AdminFICategoryEnum.Steward).Select(x => new FinancialIntegrationVM()
                {
                    AccountLabel = x.AccountLabel,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    AccountNumber = x.AccountNumber,
                    DisplayOrder = x.DisplayOrder
                }),
                collector = query.Where(c => c.CategoryID == (int)AdminFICategoryEnum.Collector).Select(x => new FinancialIntegrationVM()
                {
                    AccountLabel = x.AccountLabel,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    AccountNumber = x.AccountNumber,
                    DisplayOrder = x.DisplayOrder
                }),
                hauler = query.Where(c => c.CategoryID == (int)AdminFICategoryEnum.Hauler).Select(x => new FinancialIntegrationVM()
                {
                    AccountLabel = x.AccountLabel,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    AccountNumber = x.AccountNumber,
                    DisplayOrder = x.DisplayOrder
                }),
                processor = query.Where(c => c.CategoryID == (int)AdminFICategoryEnum.Processor).Select(x => new FinancialIntegrationVM()
                {
                    AccountLabel = x.AccountLabel,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    AccountNumber = x.AccountNumber,
                    DisplayOrder = x.DisplayOrder
                }),
                rpm = query.Where(c => c.CategoryID == (int)AdminFICategoryEnum.RPM).Select(x => new FinancialIntegrationVM()
                {
                    AccountLabel = x.AccountLabel,
                    ID = x.ID,
                    CategoryID = x.CategoryID,
                    AccountNumber = x.AccountNumber,
                    DisplayOrder = x.DisplayOrder
                })
            };
            result.stewardTooltip = new List<ItemModel<string>>();

            for (int i = 1; i < 19; i++)
            {
                var temp = new ItemModel<string>()
                {
                    ItemID = i,
                    ItemName = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == ("C" + i.ToString())).Name
                };
                result.stewardTooltip.Add(temp);
            }
            return new NewtonSoftJsonResult(result, false);
        }
        [HttpPost]
        [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.AdminFinancialIntegration)]
        public JsonResult UpdateData(FICategoryVM vm, int categoryID)
        {
            var temp = new List<FinancialIntegrationVM>();
            switch (categoryID)
            {
                case (int)AdminFICategoryEnum.Steward: //steward
                    temp = vm.steward.ToList();
                    break;
                case (int)AdminFICategoryEnum.Collector: //collector
                    temp = vm.collector.ToList();
                    break;
                case (int)AdminFICategoryEnum.Hauler: //hauler
                    temp = vm.hauler.ToList();
                    break;
                case (int)AdminFICategoryEnum.Processor: //processor
                    temp = vm.processor.ToList();
                    break;
                case (int)AdminFICategoryEnum.RPM: //rpm
                    temp = vm.rpm.ToList();
                    break;
            }
            bool valid = true;
            foreach (var item in temp)
            {
                valid = item.AccountNumber != null && item.AccountNumber.Length > 0 && item.AccountNumber.Length < 26;
                if (!valid) break;
            }
            bool result = valid;
            if (valid)
            {
                result = registrantService.UpdateFIData(temp);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Activities
        //OTSMTM2-745
        [HttpPost]
        public JsonResult LoadAllGPActivities(DataGridModel param, string temp)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }
            else
            {
                if (temp != null)
                {
                    searchText = temp.Trim();
                }
            }

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadAllGPActivities(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllGPActivitiesExport(string searchText)
        {
            var sortColumn = Request.QueryString[1];
            var sortDirection = Request.QueryString[2];

            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var result = messageService.GetAllGPActivitiesExport(searchText, sortColumn, sortDirection);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All Activity Excel-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        private void AddActivity(UserReference assignerUser, string ActivityArea, string logContent, int ObjectId)
        {
            var activity = new Activity();
            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant";
            string tmp = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.';
            this.messageService.AddActivity(new Activity()
            {
                ObjectId = ObjectId,
                InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant",
                CreatedTime = DateTime.Now,
                ActivityType = TreadMarksConstants.VendorRegistrationActivity,// 6: application activity
                ActivityArea = ActivityArea,
                Message = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.',
                Initiator = assignerUser != null ? assignerUser.UserName : "Applicant",
                Assignee = assignerUser != null ? assignerUser.UserName : "Applicant@test.com",
                AssigneeName = assignerUser != null ? assignerUser.UserName : "Applicant",
            });
        }
        #endregion
    }
}