﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.Framework.Common;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Metric;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.StewardServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class MetricController : BaseController
    {
        #region Services

        private TSFRemittanceService tsfService;
        private readonly IReportingService reportingService;
        private readonly IRegistrantService registrantService;

        #endregion Services

        public MetricController(IReportingService reportingService, IRegistrantService registrantService, TSFRemittanceService tsfService)
        {
            this.tsfService = tsfService;
            this.reportingService = reportingService;
            this.registrantService = registrantService;
        }

        // GET: System/Metric
        [ClaimsAuthorization(TreadMarksConstants.Dashboard)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadMetricList()
        {
            var metricList = new List<ItemModel<string>>();
            foreach (MetricCategoryEnum item in Enum.GetValues(typeof(MetricCategoryEnum)))
            {
                metricList.Add(new ItemModel<string>()
                {
                    ItemID = (int)item,
                    ItemName = EnumHelper.GetEnumDescription(item)
                });
            }
            return new NewtonSoftJsonResult(metricList, false);
        }

        [HttpPost]
        public ActionResult LoadDetailsByID(MetricParams paramModel)
        {
            var result = reportingService.LoadMetricDetailsByID(paramModel);
            var json = new
            {
                status = true,
                result = result,
                statusMsg = MessageResource.ValidData,
            };
            return new NewtonSoftJsonResult(json, false);
        }

        public JsonResult LoadClaimPeriods()
        {
            int vendorID = (int)ClaimType.Steward;
            var result = tsfService.GetClaimPeriods(vendorID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}