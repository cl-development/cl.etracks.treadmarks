﻿using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.EmailSettings;
using CL.TMS.Resources;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CL.TMS.Web.Infrastructure;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.EmailSettings;
using CL.TMS.Resources;
using CL.TMS.Security;
using System.Text.RegularExpressions;
using CL.TMS.UI.Common.Helper;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class EmailSettingsController : BaseController
    {
        #region Services
        private IConfigurationsServices configurationService;
        private IEmailSettingsService emailSettingsService;
        #endregion

        public EmailSettingsController(IConfigurationsServices configurationService, IEmailSettingsService emailSettingsService)
        {            
            this.configurationService = configurationService;
            this.emailSettingsService = emailSettingsService;
        }

        #region Email Settings - General
        [ClaimsAuthorization(TreadMarksConstants.AdminEmailSettings)]
        public ActionResult GeneralIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "emailSettingsApp";
                ViewBag.SelectedMenu = "EmailSettings";
                ViewBag.SelectedSubMenu = "ETGeneral";

                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public ActionResult LoadEmailSettingsGeneralVM()
        {
            var result = emailSettingsService.LoadEmailSettingsGeneralInformation();           
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public ActionResult UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM)
        {
            //check user write authority #OTSTM2-961
            //if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminEmailSettings, (int)SecurityResultType.EditSave))
            //{
            //    return Json(new { status = false, statusMsg = "Unauthorized" });
            //};
            if (!ModelState.IsValid)
            {
                LogManager.LogInfo(string.Format("Server side validation fails: Updating email settings. UTC time:{0}", DateTime.UtcNow.ToString()));
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = emailSettingsService.UpdateEmailSettingsGeneralInformation(emailSettingsGeneralVM);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) update email settings. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = true, data = emailSettingsGeneralVM});
        }
        #endregion

        #region Email Settings - Content
        [ClaimsAuthorization(TreadMarksConstants.AdminEmailSettings)]
        public ActionResult ContentIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "emailSettingsApp";
                ViewBag.SelectedMenu = "EmailSettings";
                ViewBag.SelectedSubMenu = "ETContent";

                var emailDisplayNameList = new List<BaseItemModel>();
                foreach (var item in emailSettingsService.GetEmailDisplayNamesList())
                {
                    emailDisplayNameList.Add(new BaseItemModel()
                    {
                        ItemValue = item.Key,
                        ItemName = item.Value
                    });
                }
                ViewBag.EmailDisplayNameList = emailDisplayNameList;
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [HttpPost]
        public JsonResult GetEmailDisplayNamesList()
        {
            var emailDisplayNameList = new List<BaseItemModel>();

            foreach (var item in emailSettingsService.GetEmailDisplayNamesList())
            {
                emailDisplayNameList.Add(new BaseItemModel()
                {
                    ItemValue = item.Key,
                    ItemName = item.Value
                });
            }

            ViewBag.EmailDisplayNameList = emailDisplayNameList;


            return Json(emailDisplayNameList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmailByID(int emailID)
        {
            var result = emailSettingsService.GetEmailByID(emailID);

            return new NewtonSoftJsonResult(result, false);
        }

        public ActionResult GetPreviewEmailByID(int emailID)
        {
            var result = emailSettingsService.GetPreviewEmailByID(emailID);

            result = InsertImagestoHTML(result);

            return new NewtonSoftJsonResult(result, false);
        }

        private string InsertImagestoHTML(string result)
        {
            string imgSrcTM = MakeImageSrcData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")));
            result = result.Replace("cid:" + HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo, imgSrcTM);

            string imgSrcOTS;
            try
            {
                //System.IO.Path.Combine(uploadRepositoryPath, logoURL)
                imgSrcOTS = MakeImageSrcData(Path.Combine(SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"), AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value));
            }
            catch (Exception ex)
            {
                LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                imgSrcOTS = MakeImageSrcData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")));
            }

            result = result.Replace("cid:" + HaulerApplicationEmailTemplPlaceHolders.CompanyLogo, imgSrcOTS);
            
            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                imgSrcOTS = MakeImageSrcData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")));
                result = result.Replace("cid:" + HaulerApplicationEmailTemplPlaceHolders.FacebookLogo, imgSrcOTS);
            }        

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {        
                imgSrcOTS = MakeImageSrcData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")));
                result = result.Replace("cid:" + HaulerApplicationEmailTemplPlaceHolders.TwitterLogo, imgSrcOTS);                
            }

            return result;
        }

        string MakeImageSrcData(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            return "data:image/jpeg;base64," +
              Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
        }

        [HttpPost]
        public ActionResult SaveEmailContent(EmailVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminCompanyBrandingTermsAndConditions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized", data = vm });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = vm });
            }
            else
            {
                var textValue = stripHtml(HttpUtility.HtmlDecode(vm.Body));
                if (string.IsNullOrWhiteSpace(textValue))
                {
                    return Json(new { status = false, statusMsg = "Terms and Conditions contents are required.", data = vm });
                }
            }
            vm.ModifiedByID = SecurityContextHelper.CurrentUser.Id;
            bool result = emailSettingsService.SaveEmailContent(vm);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = result, data = vm });
        }

        private string stripHtml(string input)
        {
            // \n=newline \t=tab \r= carriage return.
            string HTML_TAG_PATTERN = "\n*\t*\r*<.*?>\n*\t*\r*";
            return Regex.Replace(input, HTML_TAG_PATTERN, string.Empty);
        }
        #endregion
    }
}