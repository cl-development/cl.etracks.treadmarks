﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.UI.Common.Security;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using OfficeOpenXml;
using CL.TMS.UI.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using System.Configuration;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.Web.Infrastructure;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.Transaction;
using Newtonsoft.Json;
using CL.TMS.Configuration;
using CL.TMS.Security;
using CL.TMS.DataContracts.ViewModel.GroupRate;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class RatesController : BaseController
    {
        #region Services
        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        private IConfigurationsServices configurationService;
        #endregion

        public RatesController(IUserService userService, IRegistrantService registrantService, IMessageService messageService, IConfigurationsServices configurationService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
            this.configurationService = configurationService;
        }


        #region //Fees and Incentives

        [ClaimsAuthorization(TreadMarksConstants.AdminFeesAndIncentives)]
        public ActionResult RatesIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "configurationsRateApp";
                ViewBag.SelectedMenu = "AdminRates";
                ViewBag.SelectedSubMenu = "FeesAndIncentives";

                //use the common code about Claim Activity, 0 is for Fees and Incentives page
                ViewBag.ClaimsId = 0;
                if (SecurityContextHelper.IsStaff())
                {
                    ViewBag.HasActivityPanel = 1;
                }

                var rateIndex = AppDefinitions.Instance.
                                TypeDefinitions[DefinitionCategory.RateCategory.ToString()]
                                .Select(x => new BaseItemModel()
                                {
                                    ItemCode = x.Code,
                                    ItemName = x.Name,
                                    ItemValue = x.DefinitionValue,
                                    DisplayOrder = x.DisplayOrder,
                                })
                .OrderBy(x => x.DisplayOrder).ToList();

                rateIndex.ForEach(i =>
                {
                    switch (i.ItemName)
                    {
                        case TreadMarksConstants.TireStewardshipFeeRates: // "TireStewardshipFeesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees).ToString();
                            break;
                        case TreadMarksConstants.RemittancePenaltyRates: // "StewardRemittancePenaltyRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty).ToString();
                            break;
                        case TreadMarksConstants.CollectionAllowanceRates: // "CollectionAllowancesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance).ToString();
                            break;
                        case TreadMarksConstants.TransportationIncentiveRates: // "TransportationIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive).ToString();
                            break;
                        case TreadMarksConstants.ProcessingIncentiveRates: // "ProcessingIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive).ToString();
                            break;
                        case TreadMarksConstants.ManufacturingIncentiveRates: // "ManufacturingIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive).ToString();
                            break;
                        default:
                            break;
                    }
                });
                return View("RatesIndex", rateIndex);
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]//clear cache, avoid access this (admin)page by click on browser back button from none-admin page.
        public ActionResult LoadRateList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                { 0,"ID"},
                {1, "EffectiveStartDate"},
                {2, "DateAdded"},
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = configurationService.LoadRateList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                isFutureRateExists = (2 != category) ? result.DTOCollection.Any(x => x.EffectiveStartDate > today) : configurationService.IsFutureCollectorRateTransactionExists(category),
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }
        public ActionResult LoadPIRateList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                { 0,"ID"},
                {1,"PIType"},
                {2, "EffectiveStartDate"},
                {3, "EffectiveStartDate"},
                {4, "DateAdded"},
                {5, "AddedBy"},
                {6, "DateModified"},
                {7, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = configurationService.LoadPIRateList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            result.DTOCollection.ForEach(c =>
            {
                c.DateAdded = ConvertTimeFromUtc(c.DateAdded);
            });
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false),
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }
        [HttpGet]
        public ActionResult LoadTransactionNotesList(int parentId)
        {
            List<InternalNoteViewModel> result = configurationService.LoadRateTransactionNoteByID(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public ActionResult AddNewRate(RateDetailsVM newRate, int category)
        {
            //OTSTM2-1061
            switch (newRate.category)
            {
                case 1:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 2:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 3:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 4:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 5:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 7:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                default:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentives, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
            }
            //if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            //{
            //    return Json(new { status = false, statusMsg = "Unauthorized" });
            //};
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.AddNewRate(newRate, SecurityContextHelper.CurrentUser.Id, category);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,
                startDate = newRate.effectiveDate,
                category = "TI",
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false)
            });
        }

        [HttpPost]
        public ActionResult EditSaveRate(RateDetailsVM rateVM)
        {
            //OTSTM2-1061
            switch (rateVM.category)
            {
                case 1:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 2:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 3:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 4:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 5:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 7:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                default:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentives, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
            }
            //if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            //{
            //    return Json(new { status = false, statusMsg = "Unauthorized" });
            //};
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            rateVM.effectiveStartDate = new DateTime(rateVM.effectiveStartDate.Year, rateVM.effectiveStartDate.Month, 1);//UI send 2ed day of month due to fix FF date time issue, so set back to first day of month here.
            var result = configurationService.updateRate(rateVM, SecurityContextHelper.CurrentUser.Id);

            return Json(new
            {
                status = result,
                isSpecific = rateVM.isSpecific,
                category = rateVM.category,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(rateVM.category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(rateVM.category, false)
            });
        }
        [HttpPost]
        public ActionResult RemoveFromList(int rateTransactionID, int category)
        {
            //OTSTM2-1061
            switch (category)
            {
                case 1:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 2:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 3:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 4:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 5:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                case 7:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
                default:
                    if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentives, (int)SecurityResultType.EditSave))
                    {
                        return Json(new { status = false, statusMsg = "Unauthorized" });
                    };
                    break;
            }
            //if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            //{
            //    return Json(new { status = false, statusMsg = "Unauthorized" });
            //};
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = configurationService.RemoveRateTransaction(rateTransactionID, category);
            var isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true);
            var isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            var json = new
            {
                status = true,
                result = result,
                statusMsg = MessageResource.ValidData,
                category = category,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false)
            };
            return new NewtonSoftJsonResult(json, false);
        }
        public ActionResult LoadRateDetailsByTransactionID(int rateTransactionID, int category, bool? isSpecific)
        {
            bool isSpecial = isSpecific ?? false;
            RateDetailsVM result = configurationService.LoadRateDetailsByTransactionID(rateTransactionID, category, isSpecial);
            result.notes.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        public ActionResult IsFutureRateExists(int category)
        {
            bool result = configurationService.IsFutureRateExists(category);
            return Json(new { result = result, statusMsg = MessageResource.ValidData });
        }
        public ActionResult IsFutureCollectorRateExists(int category)
        {
            bool result = configurationService.IsFutureCollectorRateExists(category);
            return Json(new { result = result, statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandler(int parentId, string notes)
        {
            var claimNote = configurationService.AddTransactionNote(parentId, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }

        public ActionResult ExportTransactionNotesList(int parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = configurationService.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Vendor Groups
        [ClaimsAuthorization(TreadMarksConstants.AdminRateGroups)]
        public ActionResult PickupGroupsIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "pickupGroupsApp";
                ViewBag.SelectedMenu = "AdminRateGroups";
                ViewBag.SelectedSubMenu = "PickupGroups";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public ActionResult LoadVendorGroupList(DataGridModel param, string category)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ItemID"}, //hide
                {1, "Name" },
                {2, "DateAdded"},
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = configurationService.LoadVendorGroupList(param.Start, param.Length, searchText, orderBy, sortDirection, category);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadEffectiveVendorGroupList(DateTime effectiveDate, string category)
        {
            var result = configurationService.LoadEffectiveVendorGroupList(effectiveDate, category);

            var json = new
            {
                data = result,
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddNewVendorGroup(VendorGroupDetailCommonViewModel newGroup, string category)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            bool result = configurationService.AddNewVendorGroup(newGroup, SecurityContextHelper.CurrentUser.Id, category);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Vendor Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new { status = result });
        }

        [HttpPost]
        public ActionResult UpdateVendorGroup(VendorGroupDetailCommonViewModel updatedGroup)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            bool result = configurationService.UpdateVendorGroup(updatedGroup, SecurityContextHelper.CurrentUser.Id);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) updated Vendor Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new { status = result });
        }

        [HttpPost]
        public ActionResult RemoveVendorGroup(int groupId)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = configurationService.RemoveVendorGroup(groupId);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Vendor Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }


        [HttpPost]
        public ActionResult VendorGroupUniqueNameCheck(string name, string category)
        {
            bool result = configurationService.VendorGroupUniqueNameCheck(name, category);
            return Json(new { status = true, result = result });
        }

        [HttpPost]
        public ActionResult IsGroupMappingExisting(int id)
        {
            bool result = configurationService.IsGroupMappingExisting(id);
            return Json(new { status = true, result = result });
        }

        //public ActionResult IsPickupRateGroupExisting(int RateGroupId)
        //{
        //    bool result = configurationService.IsGroupMappingExisting(RateGroupId, null, null);
        //    return Json(new { status = true, result = result }, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Rate Group

        [ClaimsAuthorization(TreadMarksConstants.AdminRateGroups)]
        public ActionResult DeliveryGroupsIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "deliveryGroupsApp";
                ViewBag.SelectedMenu = "AdminRateGroups";
                ViewBag.SelectedSubMenu = "DeliveryGroups";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public ActionResult LoadRateGroupsList(DataGridModel param, string category)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ItemID"}, //hide
                {1, "Name" },
                {2, "DateAdded"},
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = configurationService.LoadRateGroupList(param.Start, param.Length, searchText, orderBy, sortDirection, category);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVendorGroups(string category)
        {
            var result = configurationService.GetVendorGroupList(category);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAssociatedVendorGroups(int rateGroupId)
        {
            var result = configurationService.GetAssociatedVendorGroupList(rateGroupId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVendors(int vendorGroupId, int rateGroupId)
        {
            var result = configurationService.LoadVendors(vendorGroupId, rateGroupId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVendorGroupInfoByVid(int vendorId, DateTime claimPeriodStart, DateTime? claimPeriodEnd, string rateGroupCategory)
        {
            var result = configurationService.GetVendorGroupInfoByDateVid(vendorId, claimPeriodStart, claimPeriodEnd, rateGroupCategory);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// category:  "PickupGroup" and "DeliveryGroup"
        /// </summary>
        /// <param name="vendorGroupId"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDefaultVendors(int vendorGroupId, string category)
        {
            var result = configurationService.LoadDefaultVendors(vendorGroupId, category);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RateGroupUniqueNameCheck(string name, string category)
        {
            bool result = configurationService.RateGroupUniqueNameCheck(name, category);
            return Json(new { status = true, result = result });
        }

        [HttpPost]
        public ActionResult AddNewRateGroup(RateGroupViewModel newRateGroup)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid) //
            {
                var err = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            bool result = configurationService.CreateRateGroup(newRateGroup, SecurityContextHelper.CurrentUser.Id);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rate Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new { status = result });
        }

        public ActionResult UpdateRateGroup(RateGroupViewModel rateGroupViewModel)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            bool result = configurationService.UpdateRateGroup(rateGroupViewModel, SecurityContextHelper.CurrentUser.Id);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) update Rate Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new { status = result });
        }

        [HttpPost]
        public ActionResult RemoveRateGroup(int RateGroupId)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminRateGroups, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = configurationService.RemoveVendorRateGroupMapping(RateGroupId);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Rate Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }

        //public ActionResult GetRateGroup(int id)
        //{
        //    var result = configurationService.LoadRateGroup(id);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult DelteRateGroup(int id)
        //{
        //    bool result = configurationService.DeleteRateGroup(id);
        //    if (result)
        //    {
        //        LogManager.LogInfo(string.Format("User: {0} (ID:{1}) delete Rate Group. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
        //    }
        //    return Json(new { status = result });

        //}
        #endregion

        #region Activity
        [HttpPost]
        public JsonResult LoadAllActivities(DataGridModel param)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            int objectID = TreadMarksConstants.AdminMenuActivity;
            var result = messageService.LoadRecoverableMaterialActivities(param.Start, param.Length, searchText, orderBy, sortDirection, objectID);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadRateGroupsActivities(DataGridModel param, int? activityType = TreadMarksConstants.AdminMenuActivity)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadRateGroupsActivities(param.Start, param.Length, searchText, orderBy, sortDirection, activityType.Value);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetAllActivitiesExport(string searchText, string activityArea = "", int activityType = 0) //(string searchText, string sortColumn, string sortDirection)
        {
            var sortColumn = Request.QueryString["sortcolumn"];
            var sortDirection = Request.QueryString["sortDirection"];
            var searchTxt = Request.QueryString["searchText"];
            var claimId = Request.QueryString[3];
            int cid = 0;
            int.TryParse(claimId, out cid);
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var result = messageService.GetAllCommonActivitiesExport(cid, searchText, sortColumn, sortDirection, activityArea, activityType);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All-Activity-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult TotalActivity(int claimId)
        {
            var count = messageService.TotalActivity(claimId);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Transportation Incentive Rate
        public ActionResult GetRateGroups(string category)
        {
            var result = configurationService.LoadRateGroups(category);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadNewTransportationIncentiveRates(int pickupRateGroupId, int deliveryRateGroupId)
        {
            var result = configurationService.LoadNewTransportationIncentiveRates(pickupRateGroupId, deliveryRateGroupId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveNewTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel)
        {
            var result = configurationService.SaveNewTransportationIncentiveRates(transportationIncentiveViewModel, SecurityContextHelper.CurrentUser.Id);
            return Json(new { status = true });
        }

        public ActionResult LoadTransportationIncentiveRates(int rateTransactionId)
        {
            var result = configurationService.LoadTransportationIncentiveViewModel(rateTransactionId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel)
        {
            var result = configurationService.UpdateTransportationIncentiveRates(transportationIncentiveViewModel, SecurityContextHelper.CurrentUser.Id);
            return Json(new { status = result });
        }

        #endregion

    }
}