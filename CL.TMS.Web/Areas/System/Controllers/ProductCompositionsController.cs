﻿using CL.Framework.Logging;
using CL.TMS.Common;

using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Web.Infrastructure;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class ProductCompositionsController : BaseController
    {
        #region Services

        private IUserService userService;
        private IProductCompositionServices productCompositionService;
        private IMessageService messageService;

        #endregion Services

        public ProductCompositionsController(IUserService userService, 
                                            IProductCompositionServices productCompositionService, 
                                            IMessageService messageService
                                            )
        {
            this.userService = userService;
            this.productCompositionService = productCompositionService;
            this.messageService = messageService;
        }

        [ClaimsAuthorization(TreadMarksConstants.AdminProductCompositions)]
        public ActionResult ProductList()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "productCompositionsApp";
                ViewBag.SelectedMenu = "ProductCompositions";
                ViewBag.SelectedSubMenu = "Materials";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });              
            }
        }

      
        public JsonResult MaterialCompositionList(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ItemID"}, //hide
                {1, "ItemName" },
                {2, "ItemShortName"},
                {3, "ItemType"}, //hide
                {4, "DateModified"},
                {5, "ModifiedBy"},
                {6, "ItemMaterialList" }
            };

           // int orderColumnIndex;
           // int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = string.Empty; // columns[orderColumnIndex];
            var sortDirection = string.Empty; // param.Order[0]["dir"];

            var result = productCompositionService.LoadMaterialCompositions(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

    
        public JsonResult RecoverableMaterialList(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ID" },  //material id
                {1, "MaterialName"},
                {2, "DateAdded" },
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"},   
                {6, "MaterialDescription"},
                {7, "Color"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = productCompositionService.LoadRecoverableMaterials(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]     
        public ActionResult SaveMaterialCompositions(MaterialCompositionsVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {              
                return Json(new { status = false, statusMsg = "Unauthorized", data = vm });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = vm });
            }

            bool result = productCompositionService.SaveMaterialComposition(vm);

            return Json(new { status = true, result = result });
        }
        [HttpPost]
        public ActionResult SaveRecoverableMaterial(RecoverableMaterialsVM rvm)
        {
           
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized", data = rvm });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = rvm });
            }

            bool result = productCompositionService.SaveRecoverableMaterial(rvm);

            return Json(new { status = true,result=result });
        }
        [HttpPost]
        public ActionResult UniqueName(string name)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized", data = name });
            };
            if (!ModelState.IsValid || string.IsNullOrEmpty(name))
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = name });
            }
       

            bool result = productCompositionService.CheckMaterialNameUnique(name);

            return Json(new { status = true,result=result});
        }

        #region Estimated Weights
        [ClaimsAuthorization(TreadMarksConstants.AdminProductCompositions)]
        public ActionResult EstimatedWeights()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "productCompositionsApp";
                ViewBag.SelectedMenu = "ProductCompositions";
                ViewBag.SelectedSubMenu = "EstimatedWeights";
                var weightIndex = AppDefinitions.Instance.TypeDefinitions[DefinitionCategory.WeightCategory.ToString()].Select(x => new BaseItemModel()
                {
                    ItemCode = x.Code,
                    ItemName = x.Name,
                    ItemValue = x.DefinitionValue,
                    DisplayOrder = x.DisplayOrder,
                })
                .OrderBy(x => x.DisplayOrder).ToList();

                weightIndex.ForEach(i =>
                {
                    switch (i.ItemName)
                    {                      
                        case TreadMarksConstants.SupplyEstimatedWeights: // "Supply Estimated Weights":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminProductCompositions).ToString();
                            break;
                        case TreadMarksConstants.RecoveryEstimatedWeights: // "Recovery Estimated Weights":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminProductCompositions).ToString();
                            break;
                        default:
                            break;
                    }
                });
                return View(weightIndex);
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]//clear cache, avoid access this (admin)page by click on browser back button from none-admin page.
        public ActionResult LoadSupplyEstimatedWeightList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;
            if (true)
            {

            }
            var columns = new Dictionary<int, string>
            {
                {0, "ID"},
                {1, "ProductDescription"},
                {2, "Min"},
                {3, "Max"},                
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = productCompositionService.LoadSupplyEstimatedWeightList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,              
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.WeightCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]//clear cache, avoid access this (admin)page by click on browser back button from none-admin page.
        public ActionResult LoadRecoveryEstimatedWeightList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;
            if (true)
            {

            }
            var columns = new Dictionary<int, string>
            {
                {0, "ID"},
                {1, "EffectiveStartDate"},
                {2, "DateAdded"},
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = productCompositionService.LoadRecoveryEstimatedWeightList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                isFutureWeightExists = result.DTOCollection.Any(x => x.EffectiveStartDate > today),
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.WeightCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }

        [HttpPost]
        public ActionResult AddNewRecoveryEstimatedWeight(WeightDetailsVM newWeight, int category)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = productCompositionService.AddNewRecoveryEstimatedWeight(newWeight, SecurityContextHelper.CurrentUser.Id, category);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new { status = result, startDate = newWeight.EffectiveDate, category = "TI" });
        }

        [HttpPost]
        public ActionResult EditSaveRecoveryEstimatedWeight(WeightDetailsVM weightVM)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            weightVM.EffectiveStartDate = new DateTime(weightVM.EffectiveStartDate.Year, weightVM.EffectiveStartDate.Month, 1);//UI send 2ed day of month due to fix FF date time issue, so set back to first day of month here.
            var result = productCompositionService.updateRecoveryEstimatedWeight(weightVM, SecurityContextHelper.CurrentUser.Id);

            return Json(new { status = result, rateID = weightVM.WeightTransactionID, startDate = weightVM.EffectiveDate });
        }
        [HttpPost]
        public ActionResult RemoveFromList(int weightTransactionID, int category)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = productCompositionService.RemoveWeightTransaction(weightTransactionID, category);
            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = true, result = result, statusMsg = MessageResource.ValidData });
        }

        public ActionResult LoadRecoveryEstimatedWeightDetailsByTransactionID(int weightTransactionID, int category)
        {
            WeightDetailsVM result = productCompositionService.LoadRecoveryEstimatedWeightDetailsByTransactionID(weightTransactionID, category);
            result.Notes.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        public ActionResult LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category)
        {
            SupplyEstimatedWeightViewModel result = productCompositionService.LoadSupplyEstimatedWeightDetailsByItemID(itemID, category);           
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public ActionResult EditSaveSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminProductCompositions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            
            var result = productCompositionService.updateSupplyEstimatedWeight(supplyEstimatedWeightVM, SecurityContextHelper.CurrentUser.Id);

            return Json(new { status = result, rateID = supplyEstimatedWeightVM.ID });
        }       
        #endregion

        #region recovery estimated weights internal note
        public ActionResult LoadTransactionNotesList(int parentId)
        {
            List<InternalNoteViewModel> result = productCompositionService.LoadWeightTransactionNoteByID(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandler(int parentId, string notes)
        {
            var claimNote = productCompositionService.AddTransactionNote(parentId, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }

        public ActionResult ExportTransactionNotesList(int parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = productCompositionService.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Activity
        [HttpPost]
        public JsonResult LoadAllActivities(DataGridModel param)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }           

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            int objectID = TreadMarksConstants.AdminMenuActivity;
            var result = messageService.LoadRecoverableMaterialActivities(param.Start, param.Length, searchText, orderBy, sortDirection, objectID);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetAllActivitiesExport(string searchText,string sortColumn, string sortDirection)
        {  
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                 sortColumn = "CreatedDate";
                 sortDirection = "desc";
            }
            var result = messageService.GetProductCompositionActivitiesExport(searchText, sortColumn, sortDirection);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Product-Composition-Activity-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult TotalActivity(int claimId)
        {
            var count = messageService.TotalActivity(claimId);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}