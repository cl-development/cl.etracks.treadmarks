﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CL.TMS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            #region GP Financial Integration

            routes.MapRoute(
                       name: "GPExport",
                       url: "GP/GetExport/{batchId}",
                       defaults: new { controller = "FinancialIntegration", action = "GetExport", batchId = UrlParameter.Optional }
                   );
            routes.MapRoute(
                       name: "GPHold",
                       url: "GP/PutOnHold/{gpiBatchEntryId}",
                       defaults: new { controller = "FinancialIntegration", action = "PutOnHold", batchId = UrlParameter.Optional }
                   );
            routes.MapRoute(
                       name: "GPDeleteFromBatch",
                       url: "GP/DeleteEntryFromBatch/{batchEntryId}",
                       defaults: new { controller = "FinancialIntegration", action = "DeleteEntryFromBatch", batchEntryId = UrlParameter.Optional }
                   );

            routes.MapRoute(
                       name: "GPPost",
                       url: "GP/PostBatch/{batchId}",
                       defaults: new { controller = "FinancialIntegration", action = "PostBatch", batchId = UrlParameter.Optional }
                   );
            routes.MapRoute(
                       name: "GPTransactions",
                       url: "GP/GetGpTransactionsJson/{batchId}",
                       defaults: new { controller = "FinancialIntegration", action = "GetGpTransactionsJSON", batchId = UrlParameter.Optional }
                   );
            routes.MapRoute(
                       name: "GP",
                       url: "GP/{action}/{type}",
                       defaults: new { controller = "FinancialIntegration", action = UrlParameter.Optional, type = UrlParameter.Optional }
                   );

            #endregion


            #region Reports

            routes.MapRoute(
                       name: "GetReportsByReportingCategoryId",
                       url: "System/Reports/{action}/{Id}",
                       defaults: new { controller = "Reports", action = UrlParameter.Optional, type = UrlParameter.Optional }
                   );

            #endregion

            #region Other



            routes.MapRoute(
                name: "SpotlightSearch",
                url: "System/Common/SpotlightSearch/{pageLimit}/{keyWords}/{searchFilters}",
                defaults: new { controller = "Common", action = "SpotLightSearch", keyWords = UrlParameter.Optional, pageLimit = UrlParameter.Optional, searchFilters = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "SpotlightSearchDefault",
                url: "Default/SpotlightSearch/{pageLimit}/{keyWords}/{searchFilters}",
                defaults: new { controller = "Common", action = "SpotLightSearch", keyWords = UrlParameter.Optional, pageLimit = UrlParameter.Optional, searchFilters = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "SpotlightSearchResults",
                url: "System/Common/SpotlightSearchResults/{keyWords}/{searchFilters}",
                defaults: new { controller = "Common", action = "SpotLightSearchResults", keyWords = UrlParameter.Optional, searchFilters = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "AcceptApplicationInvitation",
                url: "AppInvitation/AcceptApplicationInvitation/{guid}",
                defaults: new { controller = "AppInvitation", action = "AcceptApplicationInvitation", guid = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "GetParticipantById",
                url: "System/Participant/GetParticipantById/{type}/{id}",
                defaults: new { controller = "Participant", action = "GetParticipantById", type = UrlParameter.Optional, id = UrlParameter.Optional }
                );


            routes.MapRoute(
                name: "Deactivate",
                url: "System/Participant/DeactivateParticipant/{type}/{id}",
                defaults: new { controller = "Participant", action = "DeactivateParticipant", type = UrlParameter.Optional, id = UrlParameter.Optional }
                );


            routes.MapRoute(
                       name: "Default",
                       url: "{controller}/{action}/{id}",
                       defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional }
                   );


            #endregion



        }
    }
}
