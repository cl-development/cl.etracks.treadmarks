﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.Web.Hubs
{
    public class NotificationHub: Hub
    {
        public void NotifyAllClients(int notificationId, string receiver)
        {
            Clients.All.newNotification(notificationId, receiver);
        }
    }
}