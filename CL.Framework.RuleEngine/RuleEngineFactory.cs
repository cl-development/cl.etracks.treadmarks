﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.RuleEngine
{
    /// <summary>
    /// Generic Factory for creating a rule set
    /// </summary>
    public class RuleEngineFactory
    {
        /// <summary>
        /// Create rule set
        /// </summary>
        /// <returns></returns>
        public static T CreateRuleSet<T>() where T:new()
        {
            return new T();
        }
    }
}
