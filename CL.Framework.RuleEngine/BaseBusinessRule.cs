﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using FluentValidation.Results;

namespace CL.Framework.RuleEngine
{
    /// <summary>
    /// Base business rule
    /// </summary>
    public abstract class BaseBusinessRule
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ruleName"></param>
        protected BaseBusinessRule(string ruleName = null)
        {
            this.IsEnable = true;
            this.RuleName = string.IsNullOrEmpty(ruleName) ? "" : this.RuleName = ruleName;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Rule name
        /// </summary>
        public string RuleName { get; set; }
        /// <summary>
        /// IsEnable flag to enable/disable business rule
        /// </summary>
        public bool IsEnable { get; set; }

        /// <summary>
        /// TerminateOnFail flag to terminate next rule(s) execution if a rule failes
        /// </summary>
        public bool TerminateOnFail { get; set; }
        #endregion 

        #region Public Methods
        /// <summary>
        /// Exectute business rule
        /// </summary>
        /// <param name="target"></param>
        /// <param name="validationResults"></param>
        /// <param name="ruleContext"></param>
        public abstract void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null);
        #endregion 
    }
}
