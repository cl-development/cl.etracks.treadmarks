﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DAL;

namespace CL.TMS.DependencyBuilder
{
    public static class DbInterceptor
    {
        public static void InitializeDbInterceptor()
        {
            DbInterception.Add(new DbInterceptorTracing());
        }
    }
}
