﻿using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.HaulerBLL;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Claims;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;


namespace CL.TMS.HaulerServices
{
    public class HaulerClaimService: IHaulerClaimService
    {
        private HaulerClaimBO haulerClaimBO;

        public HaulerClaimService(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, IVendorRepository vendorRepository, ISettingRepository settingRepository, IConfigurationsRepository configurationsRepository)
        {
            haulerClaimBO = new HaulerClaimBO(claimsRepository, transactionRepository, gpRepository, vendorRepository, settingRepository, configurationsRepository);
        }
        public HaulerClaimSummaryModel LoadClaimSummaryData(int vendorId, int claimId)
        {
            try
            {
                var result= haulerClaimBO.LoadHaulerClaimSummary(vendorId, claimId);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public HaulerPaymentViewModel LoadHaulerPayment(int claimId)
        {
            try
            {
                return haulerClaimBO.LoadHaulerPayment(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void UpdateYardCountSubmission(int claimId, ItemRow itemRow)
        {
            try
            {
                haulerClaimBO.UpdateYardCountSubmission(claimId, itemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public HaulerSubmitClaimViewModel HaulerClaimSubmitBusinessRule(HaulerSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                return haulerClaimBO.HaulerClaimSubmitBusinessRule(submitClaimModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region GP 
        public GpResponseMsg CreateBatch()
        {
            try
            {
                return haulerClaimBO.CreateBatch();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<GpBatchDto> GetGpBatches()
        {
            throw new NotImplementedException();
        }

        #endregion 


        public List<InventoryItem> LoadInventoryItems(int vendorId, DateTime periodEndDate)
        {
            try
            {
                return haulerClaimBO.LoadInventoryItems(vendorId, periodEndDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
    }
}
