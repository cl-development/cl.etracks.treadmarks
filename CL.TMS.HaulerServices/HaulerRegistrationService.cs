﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.ExceptionHandling;
using CL.TMS.HaulerBLL;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.SystemBLL;
using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.IRepository;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Registrant;
using CL.TMS.RegistrantBLL;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.Framework.Common;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.TSFSteward;

namespace CL.TMS.HaulerServices
{
    public class HaulerRegistrationService : IHaulerRegistrationService
    {
        private HaulerRegistrationBO haulerRegistrationBO;
        
        private FileUploadBO fileUploadBO;
        private RegistrantBO registrantBO;

        public HaulerRegistrationService(IApplicationRepository applicationRepository, IItemRepository itemRepository,
            IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository,
            IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationInvitationRepository appInvitationRepository, ITSFRemittanceRepository tsfRemittanceRepository, IClaimsRepository claimsRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository, IMessageRepository messageRepository)
        {
            this.haulerRegistrationBO = new HaulerRegistrationBO(applicationRepository, itemRepository, vendorRepository, applicationInvitationRepository, claimsRepository);
            this.registrantBO = new RegistrantBO(vendorRepository, assetRepository, appUserRepository, applicationRepository, tsfRemittanceRepository, appInvitationRepository, gpRepository, settingRepository, claimsRepository, messageRepository);
        }

        #region ApplicationServices Methods

        public HaulerRegistrationModel GetFormObject(int id)
        {
            HaulerRegistrationModel result = new HaulerRegistrationModel();
            try
            {
                result = haulerRegistrationBO.GetFormObject(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public HaulerRegistrationModel GetByTokenId(Guid id)
        {
            HaulerRegistrationModel result = new HaulerRegistrationModel();
            try
            {
                result = haulerRegistrationBO.GetByTokenId(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public int ApproveApplication(HaulerRegistrationModel model, int appID)
        {
            try
            {
                return this.haulerRegistrationBO.ApproveApplication(model, appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }
        public int UpdateApproveApplication(HaulerRegistrationModel model, int vendorId)
        {
            try
            {

                return this.haulerRegistrationBO.UpdateApproveApplication(model, vendorId);

            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;               
            }
        }
        public HaulerRegistrationModel GetApprovedApplication(int vendorId)
        {
            try
            {
                return this.haulerRegistrationBO.GetApprovedApplication(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }
        
        #endregion

        #region IHaulerRegistrationService Methods

        public void UpdateUserExistsinApplication(HaulerRegistrationModel model, int appId, string updatedBy)
        {
            try
            {
                var application = haulerRegistrationBO.GetApplicationById(appId);
                if (application != null)
                {                    
                    if (model.Status != ApplicationStatusEnum.None)
                    {
                        if (model.Status != EnumHelper.ToEnum<ApplicationStatusEnum>(application.Status))
                        {
                            throw new CLValidaitonException(string.Format("Unable to save data.  The application status has changed to {0}.", application.Status));
                        }
                    }

                    IList<string> tmpInvalidFields = model.InvalidFormFields ?? new List<string>();
                    IList<ContactRegistrationModel> tmpContacts = model.ContactInformationList;
                    IList<SortYardDetailsRegistrationModel> tmpSortYardDetails = model.SortYardDetails;

                    //remove empty contacts from lists
                    haulerRegistrationBO.RemoveEmptyContacts(ref tmpContacts, ref tmpInvalidFields);
                    haulerRegistrationBO.RemoveEmptySortYardDetails(ref tmpSortYardDetails, ref tmpInvalidFields);

                    //reassign model values to new tmp variables
                    model.ContactInformationList = tmpContacts as List<ContactRegistrationModel>;
                    model.SortYardDetails = tmpSortYardDetails as List<SortYardDetailsRegistrationModel>;
                    model.InvalidFormFields = tmpInvalidFields ?? new List<string>();

                    //serialize object
                    string formObjectDataSerialized = JsonConvert.SerializeObject(model);
                    Application result = new Application()
                    {
                        FormObject = formObjectDataSerialized,
                        ID = appId,
                        AgreementAcceptedCheck = true,
                        UserIP = model.UserIPAddress
                    };

                    if (model.BusinessLocation != null)
                    {
                        result.Company = model.BusinessLocation.BusinessName;
                    }

                    if (model.ContactInformationList.Count > 0)
                    {
                        result.Contact = string.Format("{0} {1} {2}",
                        model.ContactInformationList[0].ContactInformation.FirstName,
                        model.ContactInformationList[0].ContactInformation.LastName,
                        model.ContactInformationList[0].ContactInformation.PhoneNumber);
                    }

                    haulerRegistrationBO.UpdateApplication(result);
                }
                else
                {
                    string formObjectData = JsonConvert.SerializeObject(model);
                    haulerRegistrationBO.UpdateFormObject(appId, formObjectData, updatedBy);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Address GetAddressByID(int addressID)
        {
            Address addr = new Address();
            return addr;
        }

        //public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy)
        //{
        //    this.haulerRegistrationBO.SetStatus(applicationId, applicationStatus, denyReasons);

        //    #region Activity Logging
        //    //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
        //    #endregion

        //}

        //public HaulerRegistrationModel GetAllByFilter(int appId)
        //{
        //    HaulerRegistrationModel model = new HaulerRegistrationModel();
        //    return model;
        //}

        public HaulerRegistrationModel GetAllItemsList()
        {
            HaulerRegistrationModel result = new HaulerRegistrationModel();
            try
            {
                result = haulerRegistrationBO.GetAllItemsList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }

        public HaulerRegistrationModel GetAllItemsWithAllChecks()
        {
            HaulerRegistrationModel model = new HaulerRegistrationModel();
            return model;
        }

        #endregion

        #region IActivityMessageService Methods

        public IList<ActivityMessageModel> GetActivityMessages(long? applicationId)
        {

            throw new NotImplementedException();

            //if (applicationId == null || applicationId <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = activityPageLength;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;


        }

        public IList<ActivityMessageModel> GetAllActivityMessages(long? applicationId, string searchValue)
        {
            throw new NotImplementedException();

            //if (applicationID == null || applicationID <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = 0;
            //actShow.PageNumber = 0;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;
        }


        #endregion

        #region WorkFlow Services

        public void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel)
        {
            try
            {
                this.registrantBO.SendEmailForBackToApplicant(applicationID, emailModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion
    }
}
