﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.GpStaging;
using CL.TMS.DataContracts.ViewModel.GP;

namespace CL.TMS.IRepository.System
{
    public interface IFIStagingRepository
    {
        void AddGpRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer);
        void AddGpRrOtsPmVendor(RR_OTS_PM_VENDORS vendor);
        void AddGpRrOtsRmTransaction(RR_OTS_RM_TRANSACTIONS transaction);
        void AddGpRrOtsPmTransaction(RR_OTS_PM_TRANSACTIONS transaction);
        void AddGpRrOtsPmTransDistribution(RR_OTS_PM_TRANS_DISTRIBUTIONS transDistribution);
        void AddGpRrOtsRmTransDistributionsRange(List<RR_OTS_RM_TRANS_DISTRIBUTIONS> transDistributions);
        void AddGpRrOtsRmCashReceipts(List<RR_OTS_RM_CASH_RECEIPT> cashReceipts);
        void AddGpRrOtsRmApplies(List<RR_OTS_RM_APPLY> applies);
        IQueryable<RR_OTS_RM_CUSTOMERS> GetAllRrOtsRmCustomers();
        IQueryable<RR_OTS_PM_VENDORS> GetAllRrOtsPmVendors();
        IQueryable<RR_OTS_RM_TRANSACTIONS> GetAllRrOtsRmTransactions();
        IQueryable<RR_OTS_PM_TRANSACTIONS> GetAllRrOtsPmTransactions();        
        IQueryable<RR_OTS_PM_TRANS_DISTRIBUTIONS> GetAllRrOtsPmTransDistributions();        
        IQueryable<RR_OTS_RM_TRANS_DISTRIBUTIONS> GetAllRrOtsRmTransDistributions();
        IQueryable<RR_OTS_VW_Vendors_Error> GetAllRrOtsVwVendorsError();
        IQueryable<RR_OTS_VW_Customers_Error> GetAllRrOtsVwCustomersError();
        IQueryable<RR_OTS_VW_Receivables_Invoice_Error> GetAllRrOtsVwReceivablesInvoiceErrors();
        IQueryable<RR_OTS_VW_Cash_Receipt_Error> GetAllRrOtsVwCashReceiptErrors();
        IQueryable<RR_OTS_VW_Payables_Invoice_Error> GetAllRrOtsVwPayablesInvoiceErrors();
        IQueryable<RR_OTS_VW_Cash_Receipt_Apply_Error> GetAllRrOtsVwCashReceiptApplyErrors();
        IQueryable<RR_OTS_RM_CASH_RECEIPT> GetAllRrOtsRmCashReceipts();
        IQueryable<RR_OTS_RM_APPLY> GetAllRrOtsRmApplies();
        void UpdateRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer);
        void UpdateRrOtsPmVendor(RR_OTS_PM_VENDORS vendor);
        void DeleteRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer);
        void DeleteRrOtsPmVendor(RR_OTS_PM_VENDORS vendor);
        void DeleteRrOtsRmCashReceipts(List<RR_OTS_RM_CASH_RECEIPT> cashReceipts);
        void DeleteRrOtsRmTransactions(List<RR_OTS_RM_TRANSACTIONS> transactions);
        void DeleteRrOtsPmTransactions(List<RR_OTS_PM_TRANSACTIONS> transactions);
        void DeleteRrOtsPmTransaction(RR_OTS_PM_TRANSACTIONS transaction);
        void DeleteRrOtsRmTransDistributions(List<RR_OTS_RM_TRANS_DISTRIBUTIONS> transDistributions);
        void DeleteRrOtsPmTransDistributions(List<RR_OTS_PM_TRANS_DISTRIBUTIONS> transDistributions);
        void DeleteRrOtsPmTransDistribution(RR_OTS_PM_TRANS_DISTRIBUTIONS transDistribution);
        void DeleteRrOtsRmApplies(List<RR_OTS_RM_APPLY> applies);

        GpPaymentSummary GetPaymentSummary(string transactionNumber, string INTERID);

        void AddGpRrOtsPmTransDistributions(List<RR_OTS_PM_TRANS_DISTRIBUTIONS> transDistributions);
    }
}
