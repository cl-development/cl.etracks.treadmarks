﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.IRepository.System
{
    public interface IInvitationRepository
    {
        PaginationDTO<UserInviteModel, long> LoadInvitations(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);

        bool EmailIsExisting(string emailAddress);

        void CreateInviation(Invitation invitation);

        Invitation FindInvitation(Guid guid);

        Invitation FindInvitationByUserName(string userName);

        void UpdateInvitation(Invitation invitation);

        void UpdateStatus(Invitation invitation);

        void UpdateRedirectUrl(Invitation invitation);
        List<UserInviteModel> LoadInvitations(string searchText, int vendorId, string orderBy, string sortDirection);

        Invitation FindInvitationByGUID(Guid InviteGUID);

        Invitation FindInvitationByApplicationGUID(Guid ApplicationGUID);

        void AddVendorUser(VendorUser vendorUser);

        void AddCustomerUser(CustomerUser customerUser);

        Invitation FindInvitationByApplicationId(int applicationId);

        Invitation FindInvitationByVendorId(int vendorId);

        VendorUser FindVendorUser(long userId, int vendorId);
        CustomerUser FindCustomerUser(long userId, int customerId);

        void CleanVendorUser(int vendorId, long userId);
        void CleanCustomerUser(int customerId, long userId);
        bool EmailIsExistingForParticipant(string emailAddress, int participantId, bool isVendor);

        Invitation FindParticipantInvitation(string userName, int vendorId, bool isVendor);

        Invitation FindInvitation(int vendorId, string email, bool isVendor);

        Invitation FindInvitationByUserName(string userName, int vendorId, bool isVendor);
    }
}
