﻿using System.Collections.Generic;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using Claim = System.Security.Claims.Claim;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.Reporting;

namespace CL.TMS.IRepository.System
{
    public interface IUserRepository
    {
        void AddUser(User user);
        void UpdateUser(User user);
        void DeleteUser(User user);
        string FindUserNameByUserId(long userId);
        User FindUserByUserName(string userName);
        User FindUserByUserId(long userId);
        void ResetUserPassword(User user, string hashedPassword, int passwordExpirationInDays);
        void ArchiveOldPassword(long userId, string hashedPassword);
        void ClearPasswordResetToken(long userId);
        void AddPasswordResetToken(long userId, string token, string secretKey);
        IReadOnlyList<string> GetPasswordResetToken(long userId);
        IEnumerable<string> GetUserHashedPasswordArchive(long userId);
        void AddAfterloginRedirectUrl(long userId, string redirectUrl);
        void RemoveAfterloginRedirectUrl(long userId);
        void ResetloginRedirectUrl(long userId, string redirectURL);
        User FindUserByEmailAddress(string emailAddress);
        IDictionary<long, string> GetUsersByGroup(string[] groupName);
        string FindEmailAddressByUsername(string Username);
        User FindUserByName(string username);

        List<Role> LoadAllUserRoles();

        User FindUserDetailsByEmail(string email);

        void DeleteUserDetails(long userId);

        void ActiveUserByEmail(string email, bool isActive);
        void UnlockStaffUserByEmail(string email); //OTSTM2-1013

        void UnlockParticipantUserById(long userId); //OTSTM2-1013

        List<User> GetClaimWorkflowUsers(string accountName, string workflowName);
        List<UserClaimsWorkflow> GetUserClaimsWorkflowsByUserID(int userID, string accountName, string workflow);
    
        List<ClaimWorkflowUser> LoadClaimWorkflowUsers(string Workflow, string AccountName);

        List<Role> LoadParticipantUserRoles(long userId, int participantId, bool isVendor);
        List<Role> LoadParticipantUserRolesView(long userId, int participantId, bool isVendor);
        List<Claim> LoadParticipantUserPermissionClaims(long userId, int participantId, bool isVendor);

        List<UserClaimsWorkflow> LoadAllClaimWorkflowUsers();

        List<Claim> GetUserVendors(long userId);

        void ActiveParticipantUser(long userId, int vendorId, bool isVendor, bool isActive);

        PaginationDTO<RoleListViewModel, int> GetAllRoles(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<string> GetUserNameByRoleName(string roleName);

        #region User Security Changes
        List<RolePermissionViewModel> GetRolePermissions(string roleName);
        void AddNewUserRole(RoleViewModel roleViewModel, string userName, List<AppResource> appResources, List<AppPermission> appPermissions);
        void UpdateUserRole(RoleViewModel roleViewModel, string userName, List<AppPermission> appPermissions);
        bool DeleteUserRole(int roleID);
        List<RoleSelectionViewModel> GetStaffUserRoles();
        bool RoleNameExists(string roleName);

        //OTSTM2-764
        List<UserPermissionViewModel> LoadRolePermissionsForUser(string userName);
        IDictionary<long, string> GetUsersByApplicationsWorkflow(string accountName);
        Dictionary<long, string> GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType);

        List<UserClaimsWorkflow> LoadClaimsWorkflowForUser(string userEmail);
        List<UserApplicationsWorkflow> LoadApplicationsWorkflowForUser(string userEmail);
        #endregion
    }
}
