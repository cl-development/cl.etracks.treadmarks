﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.IRepository.System
{
    public interface IApplicationInvitationRepository
    {
        ApplicationInvitation GetById(Guid ID);        
        void Update(ApplicationInvitation appInvitation);
        Guid GetTokenByApplicationId(int applicationId);
        string GetEmailByApplicationId(int applicationId);
        ApplicationInvitation GetApplicationInvitationByEmailID(string email, string participantType);
        ApplicationInvitation AddApplicationInvitation(ApplicationInvitation appInvitation);
        int GetParticipantTypeID(string participantName);
        ApplicationInvitation FindApplicationInvitationByAppID(int appId);
        ApplicationInvitation UpdateInvitationDatetime(int applicaitonId);
    }
}
    