﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.EmailSettings;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.IRepository.System
{
    public interface IEmailSettingsRepository
    {
        //Email Settings - General
        EmailSettingsGeneralVM LoadEmailSettingsGeneralInformation();
        void UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM);

        //Email Settings - Content
        Dictionary<int, string> GetEmailDisplayNamesList();
        Email GetEmailByID(int emailID);
        List<EmailElement> GetEmailElements();
        void SaveEmailContent(EmailVM emailContent);
        Email GetEmailByName(string emailName);
        bool IsEmailEnabled(string emailName);
    }
}