﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Announcement;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.IRepository.System
{
    public interface IMessageRepository
    {
        #region Notification
        int TotalUnreadNotification(string userName);
        int TotalNotification(string userName);
        List<Notification> LoadUnreadNotifications(string userName);
        PaginationDTO<NotificationViewModel, int> LoadAllNotifications(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string userName);

        void AddNotification(Notification notification);
        bool AddBatchPostNotification(Notification notification);  //OTSTM2-635

        void UpdateNotificationStatus(int notificationId, string newStatus);
        void MarkAllAsRead(string userName);

        Notification GetNotificationById(int notificationId);

        List<AllNotificationsExportModel> GetAllNotificationsExport(string userName, string searchText, string sortColumn, string sortDirection);

        #endregion

        #region Activity
        void AddActivity(Activity activity);
        PaginationDTO<ActivityViewModel, int> LoadAllActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ActivityViewModel, int> LoadRecoverableMaterialActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectID);
        PaginationDTO<ActivityViewModel, int> LoadRateGroupsActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int activityType);
        List<AllActivitiesExportModel> GetAllActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection);
        List<AllActivitiesExportModel> GetProductCompositionActivitiesExport(string searchText, string sortColumn, string sortDirection);

        int TotalActivity(int claimId);
        //OTSTM2-745
        PaginationDTO<ActivityViewModel, int> LoadAllGPActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<AllActivitiesExportModel> GetAllGPActivitiesExport(string searchText, string sortColumn, string sortDirection);
        PaginationDTO<ActivityViewModel, int> LoadAllCommonActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectId, string activityArea, int activityType);
        List<AllActivitiesExportModel> GetAllCommonActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection, string activityArea, int activityType);
        #endregion

        #region //Announcement
        void AddAnnouncement(AnnouncementVM vm, long userID);
        void UpdateAnnouncement(AnnouncementVM vm, long userID);
        void RemoveAnnouncement(int announcementID);
        AnnouncementVM LoadAnnouncementByID(int announcementID);
        PaginationDTO<AnnouncementListVM, int> LoadAnnouncementList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        IEnumerable<AnnouncementVM> LoadDashboardAnnouncements();
        #endregion
    }
}
