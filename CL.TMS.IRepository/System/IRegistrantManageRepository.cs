﻿using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.IRepository.System
{
    public interface IRegistrantManageRepository
    {
        PaginationDTO<RegistrantManageModel,long> LoadAllRegistrant(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        List<RegistrantManageModel> LoadRegistrants(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText);
    }
}
