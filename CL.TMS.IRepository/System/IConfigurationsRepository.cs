﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.GroupRate;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.IRepository.System
{
    public interface IConfigurationsRepository
    {
        #region //loading rates

        List<Rate> LoadRateTransactionByID(int rateTransactionID);

        IEnumerable<Rate> GetCurrentRates(int[] iclaimTypes);
        IEnumerable<RateGroupRate> GetCurrentRateGroupRates();
        IEnumerable<Rate> GetCurrentCollectorRates(int iclaimType);

        Period GetLatestPeriod(int iPeriodType);

        PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);

        List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID);

        RateTransaction FindLatestFutureRateTransaction();

        bool IsFutureRateExists(int category);
        bool IsFutureRateExists(int category, bool isSpecific);
        bool IsFutureRateExists(int category, int paymentType);
        IEnumerable<ItemModel<string>> LoadAvailabledProcessorIDs();
        IEnumerable<ItemModel<string>> LoadAssignedProcessorIDs(int rateTransactionID);
        #endregion //loading rates

        #region //add rates

        void AddRates(RateTransaction rateTransaction, RateParamsDTO param);

        #endregion //add rates

        #region //udpate rates

        bool UpdateRates(RateDetailsVM reteDetailsVM, RateParamsDTO param);

        #endregion //udpate rates

        #region //remove rates

        bool RemoveRateTransaction(int rateTransactionID, string categoryName);

        #endregion //remove rates

        #region //notes

        RateTransactionNote AddTransactionNote(RateTransactionNote transactionNote);

        AdminSectionNotes AddAppSettingNotes(AdminSectionNotes note);

        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);

        List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText);

        Period GetPeriodByDate(DateTime effectiveStartDate, int iPeriodType);

        List<Period> LoadFuturePeriodByType(DateTime effectiveStartDate, int iPeriodType);

        bool IsFutureCollectorRateExists(int category);

        bool IsCollectorRateTransactionExists(int category);

        bool IsFutureCollectorRateTransactionExists(int category);

        List<Rate> LoadRatesForPeriods(IEnumerable<int> periodIDs);

        bool UnBondCollectorRatesToRateTransaction(int rateTransactionID, int category);

        List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType);

        #endregion //notes

        AppSettingsVM LoadAppSettings();

        #region //Account Thresholds
        AccountThresholdsVM LoadAccountThresholds();
        bool UpdateAccountThresholds(AccountThresholdsVM vm, long userID);
        bool updateAppSettings(AppSettingsVM appSettingsVM, long userId);
        #endregion

        #region Transaction Thresholds
        TransactionThresholdsVM LoadTransactionThresholds();
        void UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM);
        #endregion

        #region Vendor Group
        PaginationDTO<VendorGroupListViewModel, int> LoadVendorGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category);
        List<VendorGroupListViewModel> LoadEffectiveVendorGroupList(DateTime effectiveDate, string category, List<int> typeList);
        List<VendorGroupListViewModel> LoadEffectiveVendorGroupList(DateTime effectiveDate, string groupType);

        void AddNewVendorGroup(VendorGroupDetailCommonViewModel newGroup, long userId, string category);
        void ApproveVendortoGroup(int rateGroupId, int vendorGroupId, int vendorId, string rateGroupType, long uerId);
        Group UpdateVendorGroup(VendorGroupDetailCommonViewModel updatedGroup, long userId);
        Group RemoveVendorGroup(int groupId);

        bool VendorGroupUniqueNameCheck(string name, string category);
        bool IsGroupMappingExisting(int id);
        //bool IsGroupMappingExisting(int? RateGroupId, int? GroupId, int? VendorId);

        #endregion

        #region Rate Group
        PaginationDTO<RateGroupListViewModel, int> LoadRateGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string categoryId);
        IEnumerable<ItemModel<string>> GetVendorGroupList(string category);
        IEnumerable<ItemModel<string>> GetAssociatedVendorGroupList(int rateGroupId);
        IEnumerable<ItemModel<int>> LoadVendors(int vendorGroupId, int rateGroupId);
        List<ItemModel<int>> LoadDefaultVendors(int vendorGroupId, string category);
        bool RateGroupUniqueNameCheck(string name, string category);
        void CreateRateGroup(RateGroup rateGroup);
        void UpdateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId);
        string GetRateGroupNameById(int rgid);
        RateGroup RemoveVendorRateGroupMapping(int RateGroupId);
        List<VendorRateGroup> GetEffectiveVendorRateGroups(string category, int rateTransactionCategory);
        //RateGroupViewModel LoadRateGroup(int id);
        //void DeleteRateGroup(int id);

        #endregion

        #region Transportation Incentive Rate
        List<ItemModel<string>> LoadRateGroups(string category);
        List<ItemModel<string>> LoadEffectiveRateGroups(string category);
        List<ItemModel<int>> LoadGroups(int rateGroupId);
        RateTransaction getRateTransactionByID(int rateTransactionID);
        RateTransaction getPreviousRateTransaction(int claimType, int? skip);
        void AddRateTransaction(RateTransaction rateTransaction);
        //void UpdateExitingRateEndDate(TransportationIncentiveViewModel transportationIncentiveViewModel, RateParamsDTO param);
        RateTransaction LoadRateTransaction(int rateTransactionId);
        void UpdateTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, RateParamsDTO param);
        void UpdateExitingRateEndDate(int category, DateTime endDate, int? skip);
        #endregion

        #region Claim Calculation
        List<RateGroupRate> LoadRateGroupRates(DateTime periodStartDate, DateTime periodEndDate);
        List<VendorRateGroup> LoadVendorRateGroups(int rateGroupId);
        List<Group> LoadVendorGroups();
        List<CommonVendorGroupModel> LoadEffectiveVendorGroups(DateTime periodStartDate, DateTime periodEndDate, string rateGroupCategory);
        IEnumerable<VendorClaimRateGroupInfo> GetVendorGroupInfoByDateVid(int vendorId, DateTime claimPeriodStart, DateTime? claimPeriodEnd, string rateGroupCategory);

        #endregion

        #region STC Premium
        PaginationDTO<STCPremiumListViewModel, int> LoadSTCPremiumList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        STCEventsCountViewModel GetSTCEventsCount();
        STCPremiumListViewModel LoadSTCEventDetailsByEventNumber(string eventNumber);
        void AddNewSTCEvent(STCPremiumListViewModel newEvent, long userId);
        bool UpdateSTCEvent(STCPremiumListViewModel updatedEvent, long userId);
        bool RemoveSTCEvent(int id, string eventNumber);
        SpecialItemRateList GetSTCRatesByTransactionID(int transactionID);
        List<STCEventBriefViewModel> LoadStcEventBriefList(List<HaulerSTCTransactionBriefViewModel> stcTransactionBriefList);
        #endregion

        #region TCR Service Threshold
        TCRServiceThresholdListViewModel LoadTCRServiceThresholdList();
        void AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool RemoveTCRServiceThresohold(int id, long userId);
        IEnumerable<TCRListViewModel> ExportTCRServiceThresholds();
        #endregion        
    }
}