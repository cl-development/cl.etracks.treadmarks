﻿using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.IRepository.Registrant
{
    public interface IAssetRepository
    {
        IReadOnlyList<Asset> IPadList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection);
        IReadOnlyList<Asset> IPadList(string searchValue);
        IReadOnlyList<Asset> IPadList();
        void AddIPad(int amountToAdd, int? vendorId, int assetTypeID, string createdBy);
        void AssignIPad(int iPadId, int vendorId, string unAssignBy);
        void UnAssignIPad(int iPadId, string unAssignBy);
        void ActiveIPad(int iPadId, string activatedBy);
        void DeActiveIPad(int iPadId, string deActivatedBy);
        string CheckForUnassignedIPadSearchValue(string searchValue);        
        
    }
}
