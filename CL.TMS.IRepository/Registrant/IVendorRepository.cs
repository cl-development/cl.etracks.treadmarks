﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.Framework.DTO;

namespace CL.TMS.IRepository.Registrant
{
    public interface IVendorRepository
    {
        IEnumerable<ProcessorListModel> GetProcessorList();
        IReadOnlyDictionary<int, string> GetVendorNameList();

        //-----------Added by Frank, begin: new function-------------
        IReadOnlyDictionary<int, string> GetListOfRegistrantsWithPageIndex(int pageIndex, int pageSize);
        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement//Add by Frank
        string GetMatchedRegistrant(string regNumberEntered);

        Vendor GetSingleWithApplicationID(int appId);
        Vendor GetSingleVendorByID(int vendorId);
        Vendor FindVendorByID(int vendorId);
        Customer FindCustomerByID(int customerId);
        Customer GetSingleCustomerByID(int customerID);

        void AddVendor(Vendor vendor);
        void UpdateVendor(Vendor vendor);
        void UpdateCustomer(Customer vendor);
        void UpdateVendorKeywords(Vendor vendor);
        void UpdateVendorKeywordsAll(int vendorType = -1);
        void UpdateCustomerKeywords(Customer customer);
        void UpdateCustomerKeywordsAll();
        void ImportVendorDetails(IEnumerable<Vendor> vendorList);
        void AddVendors(IReadOnlyList<Vendor> vendors);
        string GetVendorNumber(int vendorId);
        int? GetVendorId(string vendorNumber);
        IReadOnlyList<Address> GetVendorAddressList(int vendorId);
        Address GetVendorAddressByType(int vendorId, int typeId);
        IList<Vendor> GetUpdatedVendorList(DateTime lastUpdated);
        long GetLastAddedVendorNumber();
        void createBankInformation(int vendorId, BankInformation bankInformation);
        void UpdateBankInformation(BankInformation bankInformation);
        void AddBankInformation(BankInformation bankInfo);
        void RemoveBankInformation(BankInformation bankInfo);
        void UpdateVendorContacts(List<ContactAddressDto> contactAddresses, int vendorID);
        void UpdateCustomerContacts(List<ContactAddressDto> contactAddresses, int customerID);
        void RemoveAddresses(List<Address> addresses);

        void RemoveContacts(List<Contact> contacts); //OTSTM2-50
        void UpdateAddressGPS(int addressID, decimal lat, decimal lng);

        BankInformation GetBankInformation(int vendorId);
        BankInformation GetBankInformationById(int bankId);
        //int GetVendorRegistrationNumber(int vendorTypeID);
        List<Item> GetDefaultItems(int itemCategoryNum);
        List<Item> GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum participantTypeID);
        List<ItemParticipantType> LoadAllItemParticipantTypes();
        IList<Vendor> GetVendorsByApplicationId(int applicationId);
        IList<Customer> GetCustomersByApplicationId(int applicationId);
        VendorReference GetVendorByNumber(string number);

        VendorReference GetVendorById(int vendorId);
        IEnumerable<SpGetRegistrantSpotlight> GetRegistrantSpotlights(string keyWords, string section, int? pageLimit);
        IEnumerable<SpGetRegistrantSpotlightsPaginate> GetRegistrantSpotlightPaginate(string keyWords, string section, int start, int pageLimit);
        PaginationDTO<RegistrantSpotlightModel, int> LoadSpotlightViewMoreResult(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string keyWords, int pageLimit, List<TypeDefinition> GSModuleType, string section);
        List<RegistrantSpotlightModel> LoadSpotlightResultExport(string searchText, string orderBy, string sortDirection, string keyWords, List<TypeDefinition> GSModuleType, string section); //OTSTM2-960/963/964/965/966

        void UpdateVendorActive(Vendor vendor);
        bool IsNotActive(int vendorID, DateTime transactionDate);

        #region Customer related operations

        void SetRemitanceStatus(int customerId, int ID, string PropName, int userId);
        int GetCustomerRegistrationNumber();
        int GetVendorRegistrationNumber(int vendorTypeID);
        Customer GetSingleCustomeWithApplicatioId(int appId);
        void AddCustomer(Customer customer);


        void UpdateCustomerAcitve(Customer customer);

        List<Item> GetVendorItems(int vendorId);

        bool ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory);

        VendorActiveHistory LoadCurrenActiveHistoryForVendor(int vendorId);

        VendorActiveHistory LoadPreviousActiveHistoryForVendor(int vendorId, bool isActive);

        void ActiveInactiveCustomer(CustomerActiveHistory customerrActiveHistory);

        CustomerActiveHistory LoadCurrenActiveHistoryForCustomer(int customerId);

        CustomerActiveHistory LoadPreviousActiveHistoryForCustomer(int customerId, bool isActive);

        #endregion

        List<InactiveDateRangeModel> GetVendorInactiveDateRanges(int vendorId);
        bool IsVendorActive(int vendorId, DateTime on);
        IEnumerable<VendorStatusDateRangeModel> GetVendorStatusDateRanges(int vendorId);

        void AddVendorAttachments(int vendorId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo = false);
        AttachmentModel GetVendorAttachment(int vendorId, string fileUniqueName);
        void RemoveVendorAttachment(int vendorId, string fileUniqueName);
        void RemoveVendorBankinfoAttachment(int vendorId);

        void AddVendorAttachments(List<VendorAttachment> vendorAttachments);

        void AddCustomerAttachments(List<CustomerAttachment> customerAttachments);

        void AddCustomerAttachments(int customerId, IEnumerable<AttachmentModel> attachments);

        IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo = false);

        AttachmentModel GetCustomerAttachment(int customerId, string fileUniqueName);

        void RemoveCustomerAttachment(int customerId, string fileUniqueName);
        VendorReference GetVendorByNumber(string number, long userId, string userName);

        void UpdateVendorForGpBatch(List<int> vendorIds);
        void UpdateCustomerForGpBatch(List<int> customerIds);
        List<GpParticipantDto> LoadParticipants();
        bool UpdateVendorForRemoveBatch(string registrationNumber);
        bool UpdateCustomerForRemoveBatch(string registrationNumber);

        #region OTSTM-72
        IList<Contact> GetAllPrimaryEmailAddressByVendorType(int vendorType);
        #endregion

        void AutoSwitchForAllCustomers(int changedBy); //OTSTM2-449 backup (manually)
        bool RemoveTCRServiceThresohold(int vendorIdId, long userId);

    }
}
