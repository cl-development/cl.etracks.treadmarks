﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Registrant;

namespace CL.TMS.IRepository.Registrant
{
    public interface IAppUserRepository
    {
        IReadOnlyList<AppUserModel> GetAppUserList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection, string currentVendorNumber);
        IReadOnlyList<AppUserModel> GetAppUserList(string searchValue, string RegistrationNumber);
        void ActivateAppUser(int qrCodeId);
        void DeActivateAppUser(int qrCodeId);
        void AddAppUser(int amountToAdd, int vendorId, string createdBy);
        IList<AppUser> GetUpdatedAppUserList(DateTime lastUpdated);
    }
}
