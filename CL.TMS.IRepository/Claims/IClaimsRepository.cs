﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using Microsoft.Win32;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.Common.Enum;

namespace CL.TMS.IRepository.Claims
{
    public interface IClaimsRepository
    {
        #region Common
        PaginationDTO<StaffAllClaimsModel, Guid> LoadAllClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int routingCount, Dictionary<string, string> columnSearchText);
        void UpdateClaim(Claim claim);

        void UpdateClaimsForGPBatch(List<int> claimIds, string batchNumber);

        Claim FindClaimByClaimId(int claimId);
        List<StaffAllClaimsModel> GetExportDetails(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        void InitializeClaimForActiveVendors();
        void InitializeClaimForVendor(int vendorId);
        void Initialize3ClaimsForProcessorRMP(int vendorId, DateTime activeStateChangeDate);

        PaginationDTO<ClaimViewModel, int> LoadVendorClaims(int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);
        List<ClaimViewModel> GetVendorParticipantClaimsList(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);
        PaginationDTO<ClaimViewModel, int> LoadVendorStaffClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);
        List<ClaimViewModel> GetVendorStaffClaimsList(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);

        InventoryOpeningSummary LoadInventoryOpeningSummary(int vendorId);

        List<ClaimDetailViewModel> LoadClaimDetails(int claimId, List<Item> items);

        List<ClaimDetailViewModel> LoadClaimDetailsForCalculation(int claimId, List<Item> items);

        bool IsGenerateTires(int claimId);
        bool IncompleteTransaction(int claimId);

        List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId);

        PaginationDTO<ClaimInternalAdjustment, int> LoadClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false);

        PaginationDTO<TransactionAdjustmentViewModel, int> LoadClaimTransactionAdjustment(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        bool HasTransactionAdjustment(int claimId);

        ClaimPaymentSummary LoadClaimPaymentSummary(int claimId);

        bool IsClaimPosted(int claimId);

        List<ClaimPayment> LoadClaimPayments(int claimId);

        List<ClaimInternalPaymentAdjust> LoadClaimPaymentInternalAdjusts(int claimId);

        void SaveContext();


        Claim GetClaimForTransaction(int vendorId, DateTime transactionDate, bool isStaff);
        Claim GetClaimForDORTransaction(int transactionId); //OTSTM2-1313
        Claim GetNextAvailableOpenClaimForTransaction(int vendorId, DateTime transactionDate, bool forStaff);
        void AddClaimDetail(ClaimDetail claimDetail);
        List<int> RemoveClaimDetail(int transactionId);
        //OTSTM2-81
        void AddClaimDetailRange(List<ClaimDetail> claimDetailList);
        void UpdateClaimSummary(ClaimSummary claimSummary);
        void UpdateClaimPayments(int claimId, List<ClaimPayment> claimPayments);
        void UpdateClaimPaymentDetails(int claimId, List<ClaimPaymentDetail> claimPaymentDetails);
        Period GetClaimPeriod(int claimId);
        IEnumerable<int> GetAllClaimsToReCalculate(int? vendorType = null);
        IEnumerable<int> GetAllClaimsToReCalculateByStatus(int? vendorType = null, string status = "");
        IEnumerable<int> GetAllUnderReviewClaimsIds(); //Notification Load test only
        IList GetClaimPeriodForBreadCrumb(int vendorId);
        bool IsTransactionInSameClaim(int claimId, int transactionId);
        TransactionClaimAssociationDetailViewModel GetTransactionClaimAssociationDetailDataModel(int transactionId);
        List<TransactionSearchListViewModel> GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType, int direction, int vendorId);
        bool IsClaimOpen(int claimId);
        bool IsClaimAllowAddNewTransaction(int claimId);
        bool HasPendingTransactionAndAdjust(int claimId);
        bool HasPendingTransactionAndAdjustForThisClaimPeriod(int claimId);

        string GetClaimStatus(int claimId);
        string GetClaimStatusByTransactionId(int claimId);
        Dictionary<int, string> GetClaimStatusforTransaction();

        List<InventoryItem> LoadClaimInventoryItems(int vendorId, DateTime claimEndDate);

        void AddInventoryAdjustment(ClaimInventoryAdjustment inventoryAdjustment);
        //OTSTM2-270
        void AddInternalAdjustmentNote(ClaimInternalAdjustmentNote note);
        void AddInternalPaymentAdjustment(ClaimInternalPaymentAdjust claimInternalPaymentAdjust);
        void RemoveInternalAdjustment(int adjustmentType, int internalAdjustmentId);

        ClaimInventoryAdjustment GetClaimInventoryAdjustment(int claimInternalAdjustmentId);
        ClaimInternalPaymentAdjust GetClaimInternalPaymentAdjust(int claimInternalPaymentAdjustId);

        void UpdateInternalPaymentAdjust(int claimInternalAdjustId, ClaimAdjustmentModalResult modalResult, long editUserId);
        void UpdateInventoryPaymentAdjust(int claimInternalAdjustId, ClaimInventoryAdjustment editAdjust);

        void InitializeReviewStartDate(int claimId, DateTime reviewStartDate);
        ClaimStatusViewModel LoadClaimStatusViewModel(int claimId);
        DateTime? UpdateClaimOnhold(int claimId, bool isOnhold, long userId, long systemUserId);
        DateTime? UpdateAuditOnhold(int claimId, bool isOnhold, long userId, long systemUserId);
        ClaimProcess GetClaimProcess(int claimId);
        void UpdateClaimStatus(ClaimProcess claimProcess, bool isAdd);
        void UpdateHST(int claimId, decimal totalTax, bool isTaxApplicable = true);
        void ClaimBackToParticipant(int claimId);

        ClaimSummary GetPreviousClaimSummary(int vendorId, DateTime claimPeriodDate);

        List<InventoryItem> LoadClaimItems(int claimId);
        void UpdateVendorInventory(List<Inventory> inventoryItems, int vendorId);
        ClaimSummary GetClaimSummaryByClaimId(int claimId);

        Claim GetClaimWithSummaryPeriod(int claimId);

        List<int> GetClaimsByTransactionId(int transactionId);

        void ApproveAllAssociatedTransactions(int claimId);

        void ApproveMobileTransactions(int claimId, string TransactionType);

        bool AllTransactionIsReviewed(int claimId);

        StaffAllClaimsModel GetTopClaimForMe(long userId);

        bool IsPreviousClaimApproved(int vendorId, DateTime claimPeriodDate);
        bool IsPreviousClaimPosted(int vendorId, DateTime currentClaimPeriodDate);

        void UpdateClaimMailDate(int claimId, DateTime mailDate);
        bool IsTaxExempt(int claimId);
        List<int> GetTransactionItemIds(int claimId);
        IEnumerable<string> GetTransactionItemShortName(int claimId);
        List<InventoryItem> LoadHaulerClaimItems(int claimId);

        int? GetTransactionIdByFriendlyId(long friendlyId, int claimId);

        void Detach(object entity);

        bool HasAnyTransactionInClaim(int claimId);
        #endregion

        #region Claim Period
        Claim GetFirstOpenClaimByStartDate(int vendorId);

        #endregion

        #region Claim Attachment
        void AddClaimAttachments(int claimId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo = false);
        AttachmentModel GetClaimAttachment(int claimId, string fileUniqueName);
        void RemoveClaimAttachment(int claimId, string fileUniqueName);

        #endregion

        #region Hauler Claims
        List<ClaimYardCountSubmission> LoadYardCountSubmission(int claimId);
        decimal LoadVendorSortYardCapacity(int vendorId);
        void AddClaimYardCountSubmission(List<ClaimYardCountSubmission> yardCountSubmissions);
        #endregion

        #region Collector Claims
        List<ClaimTireOrigin> LoadClaimTireOrigins(int claimId);
        List<ClaimReuseTires> LoadClaimReuseTires(int claimId);
        void AddTireOrigin(int claimId, int tireOriginValue, List<ClaimTireOrigin> claimTireOrigins);
        void RemoveTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow);
        void RemoveClaimPaymentSummary(ClaimPaymentSummary claimPaymentSummary);

        void AddClaimReuseTires(int claimId, List<ClaimReuseTires> claimReuseTires);
        void RemoveClaimReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow);

        #endregion

        #region claim internal notest
        List<ClaimNote> LoadClaimInternalNotes(int claimId);
        ClaimNote AddClaimNote(ClaimNote claimNote);

        List<InternalNoteViewModel> LoadClaimNotesForExport(int claimId, string searchText, string sortcolumn, bool sortReverse);

        void AddClaimNotes(List<ClaimNote> claimNotes);

        #endregion

        bool IsFirstClaim(int vendorId, int claimId);
        bool IsPreviousClaimOnHold(int vendorId, DateTime claimPeriodDate, string vendorType = "");
        bool IsPreviousAuditOnHold(int vendorId, DateTime claimPeriodDate, string vendorType = "");
        bool IsFutureClaimOnHold(int vendorId, DateTime claimPeriodDate);
        bool IsFutureAuditOnHold(int vendorId, DateTime claimPeriodDate);
        bool HasUnreviewedTransactions(int claimId);
        bool TireOriginCheckForAutoApproval(int claimId);
        bool AllTransactionIsApproved(int claimId);
        bool TransactionExistsInClaim(Guid transactionId);
        Claim GetCurrentClaim(int vendorId, DateTime transactionDate);

        #region GP Intergration
        List<Claim> LoadApprovedWithoutBatch(int claimType, bool includeNegativeAmounts);
        List<ClaimDetailViewModel> LoadClaimDetailsForGP(List<int> claimIds, List<string> transactionTypes, List<Item> items);
        Claim GetPreviousApprovedClaim(int claimType, DateTime claimPeriodDate, int participantId = -1);
        void UpdateClaimPaymentSummary(GpPaymentSummary gpPaymentSummary, int claimId);
        ClaimPaymentSummary UpdateAndReturnClaimPaymentSummary(GpPaymentSummary gpPaymentSummary, int claimId); //OTSTM2-484
        List<ClaimDetailViewModel> LoadClaimDetailsForRPMGP(List<int> claimIds, List<string> transactionTypes, List<Item> items);
        void RollbackVendorInventory(List<Inventory> inventoryItems, int vendorId);

        List<Claim> FindApprovedBankClaimIds(List<int> claimIds);

        List<int> GetNextClaimsIds(int vendorId, DateTime claimStartDate);
        void UpdateClaimForCalculation(Claim claim);

        bool IsReadyForApproval(int vendorId, DateTime claimPeriodDate);

        List<InventoryItem> LoadAllClaimInventoryItems(int vendorId, DateTime claimEndDate);

        List<int> GetAllApprovedClaimIdsForInventoryCal(int? vendorId);

        void RecalculateClaimInventory(List<ClaimInventory> claimInventoryItems, int claimId);

        List<InventoryItem> LoadPreviousApprovedClaimInventories(int vendorId, DateTime claimEndDate);
        #endregion

        bool HasAnyInternalAdjustment(int claimId);

        string checkOtherClaimStatus(int claimId, int transactionId);

        StaffAllClaimsModel FindClaimById(int claimId);
        //OTSTM2-400       
        Claim GetNextClaimByVendorId(int participantId, Period currentPeriod);

        Claim GetClaimByNumberAndPeriod(string registrationNumber, int claimPeriodId);

        List<ClaimDetailViewModel> LoadClaimDetailsForReport(int claimId, List<Item> items);

        void UpdateClaimInventoryDetails(int claimId, List<ClaimInventoryDetail> claimInventoryDetails);

        List<ClaimDetailViewModel> LoadClaimDetailsForReport(int claimId, int transactionId, List<Item> items);

        void UpdateClaimInventoryDetails(int claimId, int transactionId, List<ClaimInventoryDetail> claimInventoryDetails);

        Claim FindClaimByClaimIdForReport(int claimId);

        List<int> GetMissingReportDataClaimIds();
        List<ClaimDetailVM> GetMissingReportDataTransactionIds();

        //OTSTM2-270
        List<ClaimInternalAdjustmentNote> LoadClaimInternalNotesForInventory(int ClaimInternalAdjustmentId);
        //OTSTM2-270
        List<ClaimInternalAdjustmentNote> LoadClaimInternalNotesForPayment(int ClaimInternalPaymentAdjustId);
        //OTSTM2-270
        List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse);

        List<int> GetMissingCollectorClaimIds();

        List<ClaimDetailVM> GetMissingReportData(ClaimType claimType, string registrationNumber);
        List<int> LoadAllClaimIdsByVendorTypeAndStatus(int vendorType = 2, CL.TMS.Common.Enum.ClaimStatus claimStatus = Common.Enum.ClaimStatus.Approved);
        List<int> LoadAllClaimIdsByVendorID(int vendorType = 4);

        VendorReference GetVendorReference(int claimId);

        void RemoveClaimProgress(int claimId);
        User FindUserById(long userId);

        List<ClaimSupportingDocument> LoadClaimSupportingDocuments(int claimId);
        ClaimSupportingDocument AddClaimSupportingDocument(ClaimSupportingDocument ClaimSupportingDocument);
        void RemoveClaimSupportingDocument(int claimId, int tireOriginValue);
        void UpdateSupportingDocOption(int claimId, int defValue, string option);

        List<SubmitClaimTransaction> LoadClaimTransactions(int claimId);
        List<int> getAllVenderByType(int vType);

        int GetAssociatedRateTrnsactionId(DateTime startDate, DateTime endDate, int vendorId);
        List<Rate> LoadRateTransactionByID(int rateTransactionID);
        List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID);
        #region OTSTM2-1215 STC Premium
        List<HaulerSTCTransactionBriefViewModel> GetStcTransactionBriefListByClaimId(int claimId);
        List<ClaimPaymentDetail> LoadClaimPaymentDetails(int claimId);
        #endregion
    }
}