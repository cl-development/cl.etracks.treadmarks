﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;

namespace CL.TMS.IRepository.Claims
{
    public interface ITransactionRepository
    {
        void AddTransaction(Transaction transaction);
        //OTSTM2-81
        void AddTransactionRange(List<Transaction> transactionList);
        Transaction GetTransactionByFriendlyID(long friendlyId);
        void AddTransactionAttachments(List<TransactionAttachment> transactionAttachments);        
        Transaction GetTransactionById(int transactionId);

        TransactionAdjustment GetTransactionAdjustmentById(int adjustmentId);
        Transaction GetTransactionById(Guid transactionId);       
        bool UpdateTransactionStatus(int transactionId, string oldStatus, string newStatus);
        bool UpdateSPSRTransactionStatus(int transactionId, string oldStatus, string newStatus);
        bool UdpateTransactioProcessingStatus(int transactionId, TransactionProcessingStatus status);
        bool AddTransactionRetailPrice(int transactionId, decimal retailPrice);
        

        PaginationDTO<AllTransactionsViewModel, int> LoadAllTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt="");
        List<AllTransactionsViewModel> LoadAllTransactionsList(string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter);
        CompanyInfo FindCompanyByName(string companyName);

        void AddTransactionAttachments(List<AttachmentModel> attachmentModelList);
        void UpdateAttachmentDescription(List<TransactionFileUploadModel> list);
        AttachmentModel GetTransactionAttachment(int attachmentId);
        void RemoveTemporaryTransactionAttachment(int attachmentId);
        void RemoveTemporaryTransactionAttachments(List<int> attachmentIdList);

        TransactionNote AddTransactionInternalNote(TransactionNote note);
        List<TransactionNote> GetAllTransactionInternalNote(int transactionId); //OTSTM2-765
        long GetNextAutoPaperFormFriendlyID(long initialValue, string[] typeCodeList);

        PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMTransactionList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType = "");
        PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMStaffTransactionList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType);
        List<RPMCommonTransactionListViewModel> GetRPMSPSTransactionList(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType, int pageIndex = 0, int pageSize = 0);
        List<RPMCommonTransactionListViewModel> GetRPMSIRTransactionList(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, Period claimPeriod, int claimId, string dataSubType, int pageIndex = 0, int pageSize = 0);

        TransactionAdjustment AddTransactionAdjustment(TransactionAdjustment adjustment);
        void UdpateIncompleteTransaction(Transaction transaction);
        void ChangeTransactionAdjustmentStatus(int adjustmentId, TransactionAdjustmentStatus reviewStatus, long reviewedById, int transactionId);
        void UpdateCompletedTransaction(Transaction newTansaction);

        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, List<TypeDefinition> DispositionReasonList = null);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, List<TypeDefinition> DispositionReasonList = null);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);


        List<Transaction> GetTransactionsByClaimId(int claimId, int vendorId, string transactionType);
        List<Transaction> GetTransactionsByPeriod(Period period, int vendorId, string transactionType);
        Period GetPreviousPeriod(Period period);

        void TransactionItemAdjustment<T>(List<T> returnVal, string transactionType = "");

        //OTSTM2-620
        TransactionAdjustment GetMostRecentTransactionAdjustment(int claimId, int transactionId);

        //OTSTM2-940
        TransactionAdjustment AddSTCTransactionAdjustment(TransactionAdjustment adjustment, string transactionFromPostalCode);

        #region Business Rule        
        List<PreviousScaleTicketResult> GetDuplicateScaleTicket(string transactionType, int Id, string ticketNumber, int? periodType, bool is_Reg_Transaction = true, bool is_SPS = false);

        List<DuplicateTransactionModel> GetDuplicatePaperTransactions(int friendlyId);
        bool HasAnyDuplicatePaperTransaction(int friendlyId);

        #endregion

        #region STC Premium Implementation
        STCEventNumberValidationStatus EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate);
        void AddTransactionIdIntoEvent(int transactionId, string eventNumber);
        #endregion
    }
}
