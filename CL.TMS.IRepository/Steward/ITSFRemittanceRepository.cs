﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections;

namespace CL.TMS.IRepository.TSFSteward
{
    public interface ITSFRemittanceRepository
    {
        IReadOnlyList<TSFClaim> GetAllTSFClaims();

        IQueryable<TSFClaim> GetAllTsfClaimsAsQueryable();

        TSFClaimDetail GetTSFClaimDetailsByClaimID(int ClaimId);
        decimal GetRemittanceRateByItemId(int itemID);
        TSFRemittanceBriefModel GetFirstSubmitRemittance();

        PaginationDTO<TSFRemittanceBriefModel, long> LoadAllTSFRemittance(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        PaginationDTO<TSFRemittanceBriefModel, long> LoadAllTSFRemittanceByCustomer(string RegistertionNo, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);

        IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod);

        //Tashi add-------------------------------------------------------------------------
        TSFRemittanceModel GetTSFRemittanceByCustomerId(Customer customer);
        //TSFRemittanceModel GetTSFRemittanceById(int id, Customer customer);

        TSFClaim GetTSFClaimById(int id);
        TSFRemittanceModel PopulateTSFModelByTSFClaimId(int TSFClaimId, TSFRemittanceModel mod);
        //------------------------------------------------------------------------------------
        void AddRemittanceDetail(TSFRemittanceItemModel model);
        void UpdatePenaltiesManually(int id, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false);
        void UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true);
        void UpdateTsfClaim(TSFClaim TsfClaim);
        void UpdateSupportingDocumentValue(int id, string docOptionValue, string modifiedBy);
        void DeleteRemittanceDetail(int claimId, int detailId);
        TSFRemittanceItemModel GetRemittanceDetail(int claimId, int detailId);
        IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod, int ItemID);
        RateListModel GetRate(DateTime reportingPeriod, int ItemID); //OTSTM2-567
        int AddTSFClaim(TSFRemittanceModel model, ref bool isCreated);
        StewardActivityHandlingModel SetRemittanceStatus(int claimId, string remittanceStatus, string updatedBy, long assignToUser, bool isAssigningToMe);
        decimal? SetRemittanceStatus(int claimId, string remittanceStatus, string updatedBy, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignToUser);
        void SetRemittanceStatusAuto(int claimId, string remittanceStatus, string updatedBy, string amount, long assignToUser);

        #region TSFClaim Attachment
        void AddTSFClaimAttachments(int TSFclaimId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetTSFClaimAttachments(int TSFclaimId, bool bBankInfo = false);
        AttachmentModel GetTSFClaimAttachment(int TSFclaimId, string fileUniqueName);
        void RemoveTSFClaimAttachment(int TSFclaimId, string fileUniqueName);

        #endregion

        #region TSF Remittance Notes
        PaginationDTO<TSFRemittanceNoteViewMode, int> LoadTSFRemittanceNotes(int claimId, int pageIndex, int pageSize, string searchText,
           string orderBy, string sortDirection);

        void AddTSFRemittanceNote(TSFRemittanceNote claimNote);

        List<TSFRemittanceNoteViewMode> LoadTSFRemittanceNotesForExport(int claimId, string searchText, string sortcolumn, string sortdirection);
        List<TSFRemittanceBriefModel> LoadRemittanceApplications(string searchText, string sortcolumn, string sortdirection, string regNo, Dictionary<string, string> columnSearchText);

        #endregion

        TSFClaim GetLastApprovedTSFRemittance(int customerID, int claimID);
        bool IsAnyPastPenalty(int customerID, int claimID);

        //OTSTM2-485
        PaginationDTO<TSFClaimInternalAdjustment, int> LoadTSFClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false);
        void AddInternalPaymentAdjustment(TSFClaimInternalPaymentAdjustment claimInternalPaymentAdjust);
        void AddInternalAdjustmentNote(TSFClaimInternalAdjustmentNote note);
        void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId);
        TSFClaimInternalPaymentAdjustment GetClaimInternalPaymentAdjust(int claimInternalPaymentAdjustId);
        void UpdateInternalPaymentAdjust(int claimInternalAdjustId, TSFClaimAdjustmentModalResult modalResult, long editUserId);
        List<TSFClaimInternalAdjustmentNote> LoadClaimInternalNotesForPayment(int ClaimInternalPaymentAdjustId);
        List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse);
        User GetByUserId(long? userId);
        bool HasData(int tsfClaimId);
        bool RemoveTSFClaim(int tsfClaimId);
        IList GetTSFClaimPeriodForBreadCrumb(int customerId);
        IEnumerable GetTSFClaimPeriods(int periodTypeID);
        VendorReference GetVendorReference(int tsfClaimId);
    }
}
