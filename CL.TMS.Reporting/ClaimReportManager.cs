﻿using CL.TMS.Communication.Events;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Repository.Claims;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.Common;
using CL.Framework.Logging;
using CL.TMS.Repository.System;
using CL.TMS.Common.Enum;

namespace CL.TMS.Reporting
{
    /// <summary>
    /// Singleton report manager to listen ClaimInventoryDetail event
    /// </summary>
    public class ClaimReportManager
    {
        private static readonly Lazy<ClaimReportManager> lazy = new Lazy<ClaimReportManager>(() => new ClaimReportManager());
        private IEventAggregator eventAggregator;
        private List<int> claimIdList;

        private ClaimReportManager()
        {

        }

        public static ClaimReportManager Instance
        {
            get { return lazy.Value; }
        }

        public void InitializeClaimReportManager(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            claimIdList = new List<int>();
            eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Subscribe(StartPopulateClaimInventoryDetail, true);

        }

        private void PopulateClaimInventoryDetail(ClaimsRepository claimsRepository, int claimId, int transactionId)
        {
            //Prepare data for calculation
            var claim = claimsRepository.FindClaimByClaimIdForReport(claimId);
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetailsForReport(claimId, transactionId, items);
            var claimInventoryDetails = CalculateClaimInventoryDetails(claim, claimDetails);
            claimsRepository.UpdateClaimInventoryDetails(claim.ID, transactionId, claimInventoryDetails);
        }

        private void StartPopulateClaimInventoryDetail(ClaimReportPayload claimReportPayload)
        {
            if (claimIdList.Contains(claimReportPayload.ClaimId))
            {
                //Do nothing the claim is in calculation
                LogManager.LogWarning(string.Format("Claim Report {0} is in calculation", claimReportPayload.ClaimId));
            }
            else
            {
                claimIdList.Add(claimReportPayload.ClaimId);
                if (claimReportPayload.IsSynchronous)
                {
                    var task = Task.Run(() =>
                    {
                        try
                        {
                            PopulateClaimInventoryDetail(claimReportPayload);
                            claimIdList.Remove(claimReportPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimReportPayload", Value = claimReportPayload } });
                            claimIdList.Remove(claimReportPayload.ClaimId);
                        }
                    });
                    task.Wait();
                }
                else
                {
                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            PopulateClaimInventoryDetail(claimReportPayload);
                            claimIdList.Remove(claimReportPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimReportPayload", Value = claimReportPayload } });
                            claimIdList.Remove(claimReportPayload.ClaimId);
                        }
                    });
                }
            }
        }
        private void PopulateClaimInventoryDetail(ClaimReportPayload claimReportPayload)
        {
            //Prepare data for calculation
            var claimsRepository = new ClaimsRepository();
            var claim = claimsRepository.FindClaimByClaimIdForReport(claimReportPayload.ClaimId);

            var items = DataLoader.Items;
            List<ClaimDetailViewModel> claimDetails;
            if (claimReportPayload.TransactionId == 0)
            {
                claimDetails = claimsRepository.LoadClaimDetailsForReport(claimReportPayload.ClaimId, items);
            }
            else
            {
                claimDetails = claimsRepository.LoadClaimDetailsForReport(claimReportPayload.ClaimId, claimReportPayload.TransactionId, items);
            }
            var claimInventoryDetails = CalculateClaimInventoryDetails(claim, claimDetails);


            if (claimReportPayload.TransactionId == 0)
            {
                claimsRepository.UpdateClaimInventoryDetails(claim.ID, claimInventoryDetails);
            }
            else
            {
                claimsRepository.UpdateClaimInventoryDetails(claim.ID, claimReportPayload.TransactionId, claimInventoryDetails);
            }

        }

        private List<ClaimInventoryDetail> CalculateClaimInventoryDetails(Claim claim, List<ClaimDetailViewModel> claimDetails)
        {
            var claimInventoryDetails = new List<ClaimInventoryDetail>();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            claimDetails.ForEach(c =>
            {
                if ((c.PLTQty > 0) || (c.PLTActualWeight > 0) || (c.PLTAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, pltItem, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, TreadMarksConstants.Kg));
                }
                if ((c.MTQty > 0) || (c.MTActualWeight > 0) || (c.MTAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, mtItem, c.MTActualWeight, c.MTAverageWeight, c.MTQty, TreadMarksConstants.Kg));
                }
                if ((c.AGLSQty > 0) || (c.AGLSActualWeight > 0) || (c.AGLSAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, aglsItem, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, TreadMarksConstants.Kg));
                }
                if ((c.INDQty > 0) || (c.INDActualWeight > 0) || (c.INDAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, indItem, c.INDActualWeight, c.INDAverageWeight, c.INDQty, TreadMarksConstants.Kg));
                }
                if ((c.SOTRQty > 0) || (c.SOTRActualWeight > 0) || (c.SOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, sotrItem, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.MOTRQty > 0) || (c.MOTRActualWeight > 0) || (c.MOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, motrItem, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.LOTRQty > 0) || (c.LOTRActualWeight > 0) || (c.LOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, lotrItem, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, TreadMarksConstants.Kg));
                }
                if ((c.GOTRQty > 0) || (c.GOTRActualWeight > 0) || (c.GOTRAverageWeight > 0))
                {
                    claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, gotrItem, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, TreadMarksConstants.Kg));
                }

                //Other item -- it should be first transaction item
                if ((c.PLTQty == 0) && (c.MTQty == 0) && (c.AGLSQty == 0) && (c.INDQty == 0) && (c.SOTRQty == 0) && (c.MOTRQty == 0) && (c.LOTRQty == 0) && (c.GOTRQty == 0))
                {
                    var transactionItem = c.TransactionItems.FirstOrDefault();
                    if (transactionItem != null)
                    {
                        var item = transactionItem.Item;
                        claimInventoryDetails.Add(GetClaimInventoryDetail(claim, c, item, transactionItem.ActualWeight, transactionItem.AverageWeight, transactionItem.Quantity ?? 0, TreadMarksConstants.Kg));
                    }
                }
            });

            return claimInventoryDetails;
        }

        private ClaimInventoryDetail GetClaimInventoryDetail(Claim claim, ClaimDetailViewModel c, Item item, decimal actualWeight, decimal averageWeight, int qty, int unitType)
        {
            var claimInventoryDetail = new ClaimInventoryDetail();
            claimInventoryDetail.ClaimId = claim.ID;
            claimInventoryDetail.TransactionId = c.Transaction.ID;
            claimInventoryDetail.Direction = c.Direction;
            claimInventoryDetail.ItemId = item.ID;
            claimInventoryDetail.ItemType = item.ItemType;
            claimInventoryDetail.ItemShortName = item.ShortName;
            claimInventoryDetail.ItemName = item.Name;
            claimInventoryDetail.Description = item.Description;
            claimInventoryDetail.Quantity = qty;
            claimInventoryDetail.AverageWeight = averageWeight;
            claimInventoryDetail.ActualWeight = actualWeight;
            claimInventoryDetail.UnitType = unitType;
            claimInventoryDetail.TransactionTypeCode = c.Transaction.TransactionTypeCode;
            claimInventoryDetail.TransactionProcessingStatus = c.Transaction.ProcessingStatus;
            claimInventoryDetail.ClaimPeriodId = claim.ClaimPeriodId;
            claimInventoryDetail.PeriodName = claim.ClaimPeriod.ShortName;
            claimInventoryDetail.PeriodStartDate = claim.ClaimPeriod.StartDate;
            claimInventoryDetail.PeriodEndDate = claim.ClaimPeriod.EndDate;
            claimInventoryDetail.VendorId = claim.ParticipantId;
            claimInventoryDetail.VendorType = claim.Participant.VendorType;
            claimInventoryDetail.BusinessName = claim.Participant.BusinessName;
            claimInventoryDetail.RegistrationNumber = claim.Participant.Number;
            claimInventoryDetail.CreatedDate = DateTime.UtcNow;
            var transactionItem = c.TransactionItems.FirstOrDefault();
            if (transactionItem != null)
            {
                claimInventoryDetail.ItemDescription = transactionItem.ItemDescription;
                claimInventoryDetail.ItemCode = transactionItem.ItemCode;
                claimInventoryDetail.TransactionItemRate = transactionItem.Rate;
            }
            return claimInventoryDetail;
        }


        #region reporting data calculation
        public void PopulateReportData(ClaimType claimType, string registrationNumber)
        {
            var claimsRepository = new ClaimsRepository();
            var claimDetails = claimsRepository.GetMissingReportData(claimType, registrationNumber);
            claimDetails.ForEach(c =>
            {
                PopulateClaimInventoryDetail(claimsRepository, c.ClaimId, c.TransactionId);
            });
        }
        #endregion
    }
}
