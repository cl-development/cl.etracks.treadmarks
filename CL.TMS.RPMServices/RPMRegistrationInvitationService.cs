﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.RPMServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using CL.TMS.RPMBLL;
using CL.TMS.ExceptionHandling;

namespace CL.TMS.RPMServices
{
    public class RPMRegistrationInvitationService : IRPMRegistrationInvitationService
    {
        private readonly RPMRegistrationInvitationBO RPMRegistrationInvitationBO;
        public RPMRegistrationInvitationService(IApplicationInvitationRepository applicationInvitationRepository, IApplicationRepository applicationRepository)
        {
            ////AutoMapperWebConfiguration.Configure();         
            this.RPMRegistrationInvitationBO = new RPMRegistrationInvitationBO(applicationInvitationRepository, applicationRepository);
        }
        public ApplicationInvitation GetApplicationInvitationByEmail(string emailID, string participantType)
        {
            ////var applicationInvitation = AppInvitationRepository.GetApplicationInvitationByEmailID(emailID, participantType);
            ////return applicationInvitation;          
            throw new NotImplementedException();
        }
        public bool ComposeAndSendEmail(ApplicationInvitation appInvitation)
        {
            throw new NotImplementedException();
            bool isEmailed = false;
            try
            {                             
                //TMS.Common.Email.Email emailer = new Common.Email.Email(Convert.ToInt32(AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpPort")),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpServer"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpUserName"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpPassword"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.dataElementBegin"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.dataElementEnd"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Email.defaultEmailTemplateDirectory"),
                //          AppInvitationRepository.GetApplicationSettingParamValue("Company.Email"),
                //          Convert.ToBoolean(AppInvitationRepository.GetApplicationSettingParamValue("Email.useSSL")));
                ////string body = GetInvitaionParamValue("Invitation.InviteBody");

                //string invitationSubject = AppInvitationRepository.GetApplicationSettingParamValue("Application.InvitationEmailSubject").Replace("*ParticipantType*", appInvitation.ParticipantTypeName);
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\ApplicationInviteEmailTemplate.html");
                //string body = System.IO.File.ReadAllText(htmlbody);
                //var siteURl =Path.Combine(AppInvitationRepository.GetApplicationSettingParamValue("Settings.DomainName"),
                //    AppInvitationRepository.GetApplicationSettingParamValue("Settings.ApplicationInvitationAbsolutePath")                    
                //    );
                //string combinedPathUrl = string.Format("{0}{1}/{2}", AppInvitationRepository.GetApplicationSettingParamValue("Settings.DomainName"),
                //    AppInvitationRepository.GetApplicationSettingParamValue("Settings.ApplicationInvitationAbsolutePath"),
                //    appInvitation.TokenID);
                //body = body.Replace("@siteUrl", combinedPathUrl).Replace("@participantType", appInvitation.ParticipantTypeName);
                
                //// body = body.Replace("@InviteGUID", appInvitation.Guid);
                //// body = body.Replace("@ExpireDate", inv.ExpireDate.ToString());
               
                //AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\RethinkTires.jpg"), MediaTypeNames.Image.Jpeg);
                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\follow_us_on_twitter.jpg"), MediaTypeNames.Image.Jpeg);
                //var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);
                //var oTSEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\OTS_Email_Logo.jpg"), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLog.ContentId = "RethinkTires";
                //followUsOnTwitter.ContentId = "follow_us_on_twitter";
                //treadMarksLogo.ContentId = "TreadMarksLogo";
                //oTSEmailLogo.ContentId = "OTS_Email_Logo";

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
                //alternateViewHTML.LinkedResources.Add(treadMarksLogo);
                //alternateViewHTML.LinkedResources.Add(oTSEmailLogo);

               
                // emailer.SendEmail(null, appInvitation.Email, null, null, invitationSubject, body, appInvitation, null, alternateViewHTML);
              

                //isEmailed = true;
                return isEmailed;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);               
            }

           
        }

        public ApplicationInvitation GetByTokenID(Guid guid)
        {
           //return AppInvitationRepository.GetById(guid);
           try
           {
               return RPMRegistrationInvitationBO.GetByTokenID(guid);
           }
           catch (Exception ex)
           {
               var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
               if (rethrow)
               {
                   throw;
               }
           }
           return null;
        }
        public int GetParticipantTypeID(string participantName)
        {
          ////return  AppInvitationRepository.GetParticipantTypeID(participantName);
            throw new NotImplementedException();
        }
        public IQueryable<ApplicationInvitation> GetAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<ApplicationInvitation> FindBy(System.Linq.Expressions.Expression<Func<ApplicationInvitation, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Add(ApplicationInvitation entity)
        {
            ////AppInvitationRepository.Add(entity);            
        }

        public void Delete(ApplicationInvitation entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(ApplicationInvitation entity)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
        public void Update(ApplicationInvitation appInvitation)
        {
            RPMRegistrationInvitationBO.Update(appInvitation);
        }

        public Guid GetTokenByApplicationId(int applicationId)
        {
            try
            {
                return this.RPMRegistrationInvitationBO.GetTokenByApplicationId(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return Guid.NewGuid();
        }
        public string GetEmailByApplicationId(int applicationId)
        {
            try
            {
                return this.RPMRegistrationInvitationBO.GetEmailByApplicationId(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        //public IList<Vendor> GetVendorsByApplicationId(int applicationId)
        //{
        //    return RPMRegistrationInvitationBO.GetVendorsByApplicationId(applicationId);
        //}
    }
}
