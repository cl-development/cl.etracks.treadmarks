﻿using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.RPMBLL;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Claims;
using CL.TMS.ServiceContracts.RPMServices;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.IRepository.System;


namespace CL.TMS.RPMServices
{
    public class RPMClaimService : IRPMClaimService
    {
        private RPMClaimBO rpmClaimBO;

        public RPMClaimService(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            rpmClaimBO = new RPMClaimBO(claimsRepository, transactionRepository, gpRepository, settingRepository);
        }
        public RPMClaimSummaryModel LoadClaimSummaryData(int vendorId, int claimId)
        {
            try
            {
                return rpmClaimBO.LoadRPMClaimSummary(vendorId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public RPMPaymentViewModel LoadRPMPayment(int claimId)
        {
            try
            {
                //return rpmClaimBO.LoadRPMPayment(claimId);
                return null;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public RPMSubmitClaimViewModel RPMClaimSubmitBusinessRule(RPMSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                return rpmClaimBO.RPMClaimSubmitBusinessRule(submitClaimModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public GpResponseMsg CreateBatch()
        {
            return rpmClaimBO.CreateBatch();
        }
    }
}
