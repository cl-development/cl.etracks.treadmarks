﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.ApplicationPartialViewData;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Configuration
{
    public static class ApplicationHelper
    {
        public static IList<TireDetailsItemTypeModel> GetTireDetailsItemType (ITireDetails tireDetails)
        {
            //This method formats the TireDetailsItems list and creates an item model to populate
            var tireDetailsResult = new List<TireDetailsItemTypeModel>();
            string name, shortname, datamyattri;
            int id;
            bool isChecked;

            tireDetails.TireDetailsItemTypeString.ForEach(item =>
            {
                string[] split = item.Split(':');
                datamyattri = split[0];
                Boolean.TryParse(split[1], out isChecked);
                Int32.TryParse(split[split.Length - 1], out id);

                if (datamyattri.Contains('-'))
                {
                    //datamyattri tries to split by - for name and shortname and get the final portion
                    string[] namesplit = datamyattri.Split('-');
                    name = namesplit[namesplit.Length - 1];
                    shortname = namesplit[0];

                    tireDetailsResult.Add(new TireDetailsItemTypeModel() { Name = name, ShortName = shortname, isChecked = isChecked, ID = id });
                }
            });

            return tireDetailsResult;
        }

        //OTSTM2-782
        public static SupportingDocumentPartialView ToSupportingDocumentPartialView(string view, string status, IList<string> InvalidFormFieldList, bool allowFileDownload, bool allowFileDelete, string prefix, bool IsParticipant)
        {
            return new SupportingDocumentPartialView
            {
                view = view,
                status = status,
                InvalidFormFieldList = InvalidFormFieldList,
                allowFileDelete = allowFileDelete,
                allowFileDownload = allowFileDownload,
                prefix = prefix,
                IsParticipant = IsParticipant
            };
        }

        public static CommonPartialView ToCommonPartialView(string prefix, string view, IList<string> InvalidFormFieldList, bool IsParticipant, ApplicationStatusEnum Status, ApplicationTypes type)
        {
            return new CommonPartialView
            {
                prefix = prefix,
                view = view,
                InvalidFormFieldList = InvalidFormFieldList,
                IsParticipant = IsParticipant,
                Type = type,
                Status = Status
            };
        }

        public static SecurityResultType CheckUserSecurityForInternalNotes(ApplicationTypes type)
        {

            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationInternalNotes);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationInternalNotes);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationInternalNotes);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationInternalNotes);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationInternalNotes);
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckSecurityForActiveInactive(ApplicationTypes type)
        {
            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationActiveInactive);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationActiveInactive);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationActiveInactive);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationActiveInactive);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationActiveInactive);
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckUserSecurityForBankingInformation()
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            if (vendor.IsVendor)
            {
                if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
                {
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationBankingInformation);
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
                {
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationBankingInformation);
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
                {
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationBankingInformation);
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
                {
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationBankingInformation);
                }
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckUserSecurityForTermsAndConditions(ApplicationTypes type)
        {

            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationTermsAndConditions);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationTermsAndConditions);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationTermsAndConditions);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationTermsAndConditions);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationTermsAndConditions);
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckUserSecurityForBusinessLocation(ApplicationTypes type)
        {

            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationBusinessLocation);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationBusinessLocation);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationBusinessLocation);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationBusinessLocation);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationBusinessLocation);
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckUserSecurityForContactInfo(ApplicationTypes type)
        {

            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationContactInformation);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationContactInformation);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationContactInformation);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationContactInformation);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationContactInformation);
            }

            return SecurityResultType.NoAccess;
        }

        public static SecurityResultType CheckUserSecurityForSupportingDoc(ApplicationTypes type)
        {
            //var vendor = SecurityContextHelper.CurrentVendor;

            //if (vendor.IsVendor)
            //{
            //    if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
            //    {
            //        return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationSupportingDocuments);
            //    }
            //    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
            //    {
            //        return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationSupportingDocuments);
            //    }
            //    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
            //    {
            //        return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationSupportingDocuments);
            //    }
            //    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
            //    {
            //        return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationSupportingDocuments);
            //    }
            //}
            //else
            //{
            //    //steward
            //    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationSupportingDocuments);
            //}

            switch (type)
            {
                case ApplicationTypes.Hauler:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsHaulerApplicationSupportingDocuments);
                case ApplicationTypes.Collector:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsCollectorApplicationSupportingDocuments);
                case ApplicationTypes.RPM:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsRPMApplicationSupportingDocuments);
                case ApplicationTypes.Processor:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsProcessorApplicationSupportingDocuments);
                case ApplicationTypes.Steward:
                    return ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ApplicationsStewardApplicationSupportingDocuments);
            }


            return SecurityResultType.NoAccess;
        }

        //Routing Security
        public static bool IsAnyPanelEditSave(List<Claim> resourceListByType)
        {
            return resourceListByType.Any(c => c.Value.Contains(",3"));
        }
        public static bool IsAllPanelEditSave(List<Claim> resourceListByType)
        {
            return resourceListByType.All(c => c.Value.Contains(",3"));
        }
        public static bool IsAnyPanelNoAccess(List<Claim> resourceListByType)
        {
            return resourceListByType.Any(c => c.Value.Contains(",0"));
        }

        //steward
        public static bool IsStewardAutoSaveDisabled(List<Claim> resourceListByType)
        {
            var query = resourceListByType.Where(c => c.Value.Contains(",0") && !(c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationActiveInactive + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationRemitFrequency + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationAuditToggle + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationMOEToggle + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationInternalNotes + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationSubmissionInformation + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsStewardApplicationResendWelcomeLetter + ",")
                )).ToList();

            return query.Count > 0;
        }

        //Collector
        public static bool IsCollectorAutoSaveDisabled(List<Claim> resourceListByType)
        {
            var query = resourceListByType.Where(c => c.Value.Contains(",0") && !(c.Value.StartsWith(TreadMarksConstants.ApplicationsCollectorApplicationActiveInactive + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsCollectorApplicationGeneratorToggle + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsCollectorApplicationSubmissionInformation + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsCollectorApplicationInternalNotes + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsCollectorApplicationResendWelcomeLetter + ",")
                )).ToList();

            return query.Count > 0;
        }

        //Hauler
        public static bool IsHaulerAutoSaveDisabled(List<Claim> resourceListByType)
        {
            var query = resourceListByType.Where(c => c.Value.Contains(",0") && !(c.Value.StartsWith(TreadMarksConstants.ApplicationsHaulerApplicationActiveInactive + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsHaulerApplicationSubmissionInformation + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsHaulerApplicationInternalNotes + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsHaulerApplicationResendWelcomeLetter + ",")
                )).ToList();

            return query.Count > 0;
        }

        //RPM
        public static bool IsRPMAutoSaveDisabled(List<Claim> resourceListByType)
        {
            var query = resourceListByType.Where(c => c.Value.Contains(",0") && !(c.Value.StartsWith(TreadMarksConstants.ApplicationsRPMApplicationActiveInactive + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsRPMApplicationSubmissionInformation + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsRPMApplicationInternalNotes + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsRPMApplicationResendWelcomeLetter + ",")
                )).ToList();

            return query.Count > 0;
        }

        //Processor
        public static bool IsProcessorAutoSaveDisabled(List<Claim> resourceListByType)
        {
            var query = resourceListByType.Where(c => c.Value.Contains(",0") && !(c.Value.StartsWith(TreadMarksConstants.ApplicationsProcessorApplicationActiveInactive + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsProcessorApplicationSubmissionInformation + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsProcessorApplicationInternalNotes + ",") ||
                c.Value.StartsWith(TreadMarksConstants.ApplicationsProcessorApplicationResendWelcomeLetter + ",")
                )).ToList();

            return query.Count > 0;
        }
    }
}
