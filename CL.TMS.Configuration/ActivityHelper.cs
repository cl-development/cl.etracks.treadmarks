﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.System;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Configuration
{
    /// <summary>
    /// Singleton System Activity Helper
    /// </summary>
    public class ActivityHelper
    {
        private static readonly Lazy<ActivityHelper> lazy = new Lazy<ActivityHelper>(() => new ActivityHelper());
        private ISettingRepository repository;
        private ActivityHelper()
        {
            repository = new SettingRepository();
        }

        public void AddSystemActivity(string message, string category, int activityId)
        {
            var systemActivity = new SystemActivity();
            systemActivity.Message = message;
            systemActivity.Category = category;
            systemActivity.ActivityId = activityId;
            systemActivity.UserId = SecurityContextHelper.CurrentUser.Id;
            systemActivity.CreatedDate = DateTime.UtcNow;
            repository.AddSystemActivity(systemActivity);
        }

        public List<SystemActivity> LoadSystemActivities(string category, int activityId)
        {
            return repository.LoadSystemActivity(category, activityId);
        }
    }
}
