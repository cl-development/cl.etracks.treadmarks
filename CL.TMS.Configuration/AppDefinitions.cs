﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.System;

namespace CL.TMS.Configuration
{
    /// <summary>
    /// Singleton App Definitions
    /// </summary>
    public class AppDefinitions
    {
        private static readonly Lazy<AppDefinitions> lazy = new Lazy<AppDefinitions>(() => new AppDefinitions());
        private ISettingRepository repository;
        private List<TypeDefinition> allTypeDefinitions;
        private AppDefinitions()
        {
            repository = new SettingRepository();
            allTypeDefinitions = LoadAllTypes();
            AddUserRoles();
        }

        private void AddUserRoles()
        {
            var userRoles = repository.LoadUserRoles();
            userRoles.ForEach(c =>
            {
                var item = new TypeDefinition
                {
                    Category = "UserRole",
                    Name = c.Name,
                    Code = c.Name,
                    DefinitionValue = c.ID
                };
                allTypeDefinitions.Add(item);
            });
        }

        private List<TypeDefinition> LoadAllTypes()
        {
            return repository.LoadDefinitions();
        }

        public static AppDefinitions Instance
        {
            get { return lazy.Value; }
        }

        public Dictionary<string, List<TypeDefinition>> TypeDefinitions
        {
            get { return allTypeDefinitions.GroupBy(c => c.Category).ToDictionary(g => g.Key, g => g.ToList()); }
        }

        public TypeDefinition GetDefinitionByValue(DefinitionCategory category, int value)
        {
            var definions = TypeDefinitions[category.ToString()];
            return definions.FirstOrDefault(c => c.DefinitionValue == value);
        }

        public TypeDefinition GetDefinitionByName(DefinitionCategory category, string name)
        {
            var definions = TypeDefinitions[category.ToString()];
            return definions.FirstOrDefault(c => string.Compare(c.Name.Trim(), name, StringComparison.OrdinalIgnoreCase) == 0);
        }
        public TypeDefinition FindDefinitionByName(DefinitionCategory category, string name)
        {
            var definions = TypeDefinitions[category.ToString()];
            
            return definions.FirstOrDefault(c => c.Name.ToLower().IndexOf(name.ToLower())>-1);
        }
        public TypeDefinition GetDefinitionByCode(DefinitionCategory category, string code)
        {
            var definions = TypeDefinitions[category.ToString()];
            return definions.FirstOrDefault(c => string.Compare(c.Code.Trim(), code, StringComparison.OrdinalIgnoreCase) == 0);
        }
    }


    public enum DefinitionCategory
    {
        UserRole,
        ClaimsRoles,
        ClaimsRouting,
        ItemType,
        StorageSiteType,
        AddressType,
        AssetType,
        CustomerType,
        ResourceType,
        StewardType,
        ProcessorProdType,
        VendorType,
        ClaimType,
        ItemCategory,
        AdjustmentReasonType,
        ScaleTicketType,
        TireOrigin,
        MaterialType,
        DispositionReason,
        RatePaymentType,
        TireOriginFieldLevelValidation,
        PeriodType,
        GSModuleType,
        RateCategory,
        ApplicationType,
        WeightCategory,
        AdminFICategory,
        VendorGroup,
        RateGroup
    }
}
