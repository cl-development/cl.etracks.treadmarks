﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CL.Framework.Common
{
    /// <summary>
    /// A common helper class which could be used across entire solution
    /// </summary>
    public static class CommonFun
    {
        #region Public Methods
        /// <summary>
        /// Dump a object to a json string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string DumpObjectToJsonString(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

       /// <summary>
       /// Dump a object to a json string without throwing exception if encountering serialization errors
       /// </summary>
       /// <param name="obj"></param>
       /// <returns></returns>
        public static string DumpObjectToJsonStringIgnoreException(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception ex)
            {
                return "Exception:Not able to serialize to Json format string";
            }
            
        }

        public static string DumpExceptionToJsonString(Exception ex)
        {
            try
            {
                return JsonConvert.SerializeObject(ex);
            }
            catch (Exception)
            {
                return ex.Message;
            }
        }
        /// <summary>
        /// Deserialize object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static IList<string> ParseJsonByXpath(string sJsonText,string sRootName, IList<string> sXpath)
        {
            XmlDocument doc = new XmlDocument();
            IList<string> sReturnVal = new List<string>();
            try
            {
                doc = JsonConvert.DeserializeXmlNode(sJsonText, sRootName);
                foreach (string s in sXpath)
                { 
                    XmlNodeList elements = doc.SelectNodes(s);
                    sReturnVal.Add((elements.Count > 0) ? elements[0].InnerText : null);//get the first node found
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            return sReturnVal;
        }
        #endregion 
    }
}
