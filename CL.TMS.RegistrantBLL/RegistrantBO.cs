﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.IRepository;
using CL.TMS.Security.Implementations;
using FluentValidation.Results;
using CL.Framework.BLL;
using CL.TMS.RuleEngine.PasswordBusinessRules;
using CL.TMS.Security;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.IRepository.System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.IRepository.Registrant;
using SystemIO = System.IO;
using System.Transactions;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.GpStaging;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.IRepository.Claims;
using CL.TMS.Security.Authorization;
using CL.TMS.DataContracts.ViewModel.Claims;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;
using CL.TMS.Common;
using System.Threading;
using CL.TMS.DataContracts.ViewModel.Common.Email;
using CL.TMS.ExceptionHandling;
using System.Runtime.CompilerServices;
using CL.Framework.Logging;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Helper;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;

namespace CL.TMS.RegistrantBLL
{
    public class RegistrantBO : BaseBO
    {
        private IAssetRepository assetRepository;
        private IVendorRepository vendorRepository;
        private IAppUserRepository appUserRepository;
        private IApplicationRepository applicationRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private ITSFRemittanceRepository tsfRemittanceRepository;
        private IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        private readonly IClaimsRepository claimsRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository; //OTSTM2-400
        private string uploadRepositoryPath;

        public RegistrantBO(IVendorRepository vendorRepository, IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationRepository applicationRepository, ITSFRemittanceRepository tsfRemittanceRepository,
             IApplicationInvitationRepository appInvitationRepository, IGpRepository gpRepository, ISettingRepository settingRepository, IClaimsRepository claimsRepository, IMessageRepository messageRepository)
        {
            this.assetRepository = assetRepository;
            this.vendorRepository = vendorRepository;
            this.appUserRepository = appUserRepository;
            this.applicationRepository = applicationRepository;
            this.tsfRemittanceRepository = tsfRemittanceRepository;
            this.applicationInvitationRepository = appInvitationRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;
            this.claimsRepository = claimsRepository;
            this.messageRepository = messageRepository; //OTSTM2-400
            this.uploadRepositoryPath = AppSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        public RegistrantBO(IVendorRepository vendorRepository, IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationRepository applicationRepository, ITSFRemittanceRepository tsfRemittanceRepository,
            IApplicationInvitationRepository appInvitationRepository, IGpRepository gpRepository, ISettingRepository settingRepository, IClaimsRepository claimsRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository)
        {
            this.assetRepository = assetRepository;
            this.vendorRepository = vendorRepository;
            this.appUserRepository = appUserRepository;
            this.applicationRepository = applicationRepository;
            this.tsfRemittanceRepository = tsfRemittanceRepository;
            this.applicationInvitationRepository = appInvitationRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;
            this.claimsRepository = claimsRepository;
            this.eventAggregator = eventAggregator;
            this.messageRepository = messageRepository; //OTSTM2-400
        }

        //Vendor methods
        public IReadOnlyDictionary<int, string> GetVendorNameList()
        {
            return vendorRepository.GetVendorNameList();
        }

        //-----------Added by Frank, begin: new function-------------
        public IReadOnlyDictionary<int, string> GetListOfRegistrantsWithPageIndex(int pageIndex, int pageSize)
        {
            return vendorRepository.GetListOfRegistrantsWithPageIndex(pageIndex, pageSize);
        }
        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement
        public string GetMatchedRegistrant(string regNumberEntered)
        {
            return vendorRepository.GetMatchedRegistrant(regNumberEntered);
        }

        public Vendor GetSingleWithApplicationID(int appId)
        {
            return vendorRepository.GetSingleWithApplicationID(appId);
        }
        public Vendor GetSingleVendorByID(int vendorId)
        {
            return vendorRepository.GetSingleVendorByID(vendorId);
        }
        public Customer GetSingleCustomerByID(int CustomerId)
        {
            return vendorRepository.GetSingleCustomerByID(CustomerId);
        }

        #region Worflow for all applications

        public void SendEmailForBackToApplicant(int ApplicationID, ApplicationEmailModel emailModel)
        {
            #region
            string message = string.Empty;

            Guid invitationToken = this.applicationInvitationRepository.GetTokenByApplicationId(ApplicationID);
            emailModel.InvitationToken = invitationToken;

            string viewApplication = string.Format("{0}{1}{2}", AppSettings.Instance.GetSettingValue("Settings.DomainName"), "/Hauler/Registration/index/", emailModel.InvitationToken);

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.BackToApplicantEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationBacktoApplicant");
            string subject = EmailContentHelper.GetSubjectByName("NewApplicationBacktoApplicant");

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "Hauler")
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ViewApplication, viewApplication)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            bool bSendBCCEmail = AppSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            Email.Email email = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                AppSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(AppSettings.Instance.GetSettingValue("Email.useSSL")));

            //OTSTM2-132 BCC value update
            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, bSendBCCEmail ? AppSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("Hauler application deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);

            #endregion
        }

        //Send Back To application to Processor
        public void SendEmailForBackToApplicantProcessor(int ApplicationID, ApplicationEmailModel emailModel)
        {
            #region
            string message = string.Empty;

            Guid invitationToken = this.applicationInvitationRepository.GetTokenByApplicationId(ApplicationID);
            emailModel.InvitationToken = invitationToken;

            string viewApplication = string.Format("{0}{1}{2}", AppSettings.Instance.GetSettingValue("Settings.DomainName"), "/Processor/Registration/index/", emailModel.InvitationToken);

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.BackToApplicantEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationBacktoApplicant");
            string subject = EmailContentHelper.GetSubjectByName("NewApplicationBacktoApplicant");

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "Processor")
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ViewApplication, viewApplication)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            bool bSendBCCEmail = AppSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            Email.Email email = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                AppSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(AppSettings.Instance.GetSettingValue("Email.useSSL")));

            //OTSTM2-132 BCC value update
            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, bSendBCCEmail ? AppSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("Processor application deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);

            #endregion
        }


        //Send Back To application to RPM
        public void SendEmailForBackToApplicantRPM(int ApplicationID, ApplicationEmailModel emailModel)
        {
            #region
            string message = string.Empty;

            Guid invitationToken = this.applicationInvitationRepository.GetTokenByApplicationId(ApplicationID);
            emailModel.InvitationToken = invitationToken;

            string viewApplication = string.Format("{0}{1}{2}", AppSettings.Instance.GetSettingValue("Settings.DomainName"), "/RPM/Registration/index/", emailModel.InvitationToken);

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.BackToApplicantEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationBacktoApplicant");
            string subject = EmailContentHelper.GetSubjectByName("NewApplicationBacktoApplicant");

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "RPM")
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ViewApplication, viewApplication)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            bool bSendBCCEmail = AppSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            Email.Email email = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                AppSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(AppSettings.Instance.GetSettingValue("Email.useSSL")));

            //OTSTM2-132 BCC value update
            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, bSendBCCEmail ? AppSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("RPM application deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);

            #endregion
        }
        public void SetStatus(int applicationId, ApplicationStatusEnum status, string denyReasons, long assignToUser = 0)
        {
            applicationRepository.SetStatus(applicationId, status, denyReasons, assignToUser);
        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }
            string[] vendorPrimaryContactName = vendorPrimaryContact.FirstName.Split(' ');

            var applicationHaulerApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContactName[0] != null ? vendorPrimaryContactName[0] : string.Empty,
                PrimaryContactLastName = vendorPrimaryContactName.Count() >= 2 ? vendorPrimaryContactName[1] : string.Empty,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = applicationInvitationRepository.GetTokenByApplicationId(applicationId)
            };

            return applicationHaulerApplicationModel;
        }

        #endregion

        public void AddVendor(Vendor vendor)
        {
            vendorRepository.AddVendor(vendor);
        }

        public void UpdateVendor(Vendor vendor)
        {
            vendorRepository.UpdateVendor(vendor);
        }

        public void UpdateKeyWords(int? vendorType = null)
        {
            //using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            //{
            var v = vendorType ?? -1;
            if (v == -1)
            {
                vendorRepository.UpdateCustomerKeywordsAll();
                vendorRepository.UpdateVendorKeywordsAll();
            }
            else if (v == 1)
            {
                vendorRepository.UpdateCustomerKeywordsAll();
            }
            else
            {
                vendorRepository.UpdateVendorKeywordsAll(v);
            }
            //    transationScope.Complete();
            //}
        }

        public void ImportVendorDetails(IEnumerable<Vendor> vendorList)
        {
            vendorRepository.ImportVendorDetails(vendorList);
        }

        public void AddVendors(IReadOnlyList<Vendor> vendors)
        {
            AddVendors(vendors);
        }

        public string GetVendorNumber(int vendorId)
        {
            return vendorRepository.GetVendorNumber(vendorId);
        }

        public IReadOnlyList<Address> GetVendorAddressList(int vendorId)
        {
            return vendorRepository.GetVendorAddressList(vendorId);
        }

        public Address GetVendorAddressByType(int vendorId, int typeId)
        {
            return vendorRepository.GetVendorAddressByType(vendorId, typeId);
        }

        public IList<Vendor> GetUpdatedVendorList(DateTime lastUpdated)
        {
            return vendorRepository.GetUpdatedVendorList(lastUpdated);
        }

        public long GetLastAddedVendorNumber()
        {
            return vendorRepository.GetLastAddedVendorNumber();
        }

        public void CreateBankInformation(Vendor vendor, BankingInformationRegistrationModel bankInformationModel)
        {
            string key = Token.GenerateRandomSecretKey();

            var bankingInfo = new BankInformation()
            {
                BankName = bankInformationModel.BankName,
                Key = key,
                TransitNumber = Token.Encrypt(bankInformationModel.TransitNumber, key),
                BankNumber = Token.Encrypt(bankInformationModel.BankNumber, key),
                AccountNumber = Token.Encrypt(bankInformationModel.AccountNumber, key),
                Email = bankInformationModel.Email,
                CCEmail = bankInformationModel.CCEmail,
                EmailPaymentTransferNotification = true,
                NotifyToPrimaryContactEmail = bankInformationModel.NotifyToPrimaryContactEmail,
                ReviewStatus = BankInfoReviewStatus.Submitted.ToString(),
                SubmittedOnDate = DateTime.UtcNow,
            };

            if (bankInformationModel.NotifyToPrimaryContactEmail)
            {
                //Find primary contact
                Contact primaryContact = null;
                var contractAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 3);
                if (contractAddress != null)
                {
                    primaryContact = contractAddress.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                }
                if (primaryContact == null)
                {
                    var businessAddess = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                    if (businessAddess != null)
                    {
                        primaryContact = businessAddess.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
                    }
                }

                if (primaryContact != null)
                {
                    bankingInfo.Email = primaryContact.Email;
                }
            }

            vendor.BankInformations.Add(bankingInfo);

            vendor.RegistrantStatus = ApplicationStatusEnum.BankInformationSubmitted.ToString();

            var dbVendorSupportingDocument = vendor.VendorSupportingDocuments.FirstOrDefault(r => r.TypeID == (int)SupportingDocumentTypeEnum.CHQ);
            if (dbVendorSupportingDocument != null)
            {
                dbVendorSupportingDocument.Option = bankInformationModel.SupportingDocumentsOption;
            }
            else
            {
                vendor.VendorSupportingDocuments.Add(new VendorSupportingDocument
                {
                    TypeID = (int)SupportingDocumentTypeEnum.CHQ,
                    Option = bankInformationModel.SupportingDocumentsOption
                });
            }
            vendorRepository.UpdateVendor(vendor);
        }

        public Vendor UpdateBankInformation(BankInformation bankInformation)
        {
            Vendor vdr = new Vendor();
            if (string.Equals(bankInformation.ReviewStatus, BankInfoReviewStatus.Approved.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                var bankInformationOld = vendorRepository.GetBankInformationById(bankInformation.ID);
                if (bankInformationOld != null)
                {
                    if (!bankInformationOld.Equals(bankInformation))
                    {
                        var vendor = vendorRepository.FindVendorByID(bankInformationOld.VendorId);
                        vendor.GpUpdate = true;
                        vendorRepository.UpdateVendor(vendor);
                        vdr = vendor;
                    }
                }
            }
            vendorRepository.UpdateBankInformation(bankInformation);
            return vdr;
        }
        public void DeleteBankInformation(int bankId, int vendorId)
        {
            var vendor = vendorRepository.GetSingleVendorByID(vendorId);
            var bankinfo = vendorRepository.GetBankInformationById(bankId);

            vendor.RegistrantStatus = ApplicationStatusEnum.Approved.ToString();

            vendorRepository.RemoveBankInformation(bankinfo);
            vendorRepository.UpdateVendor(vendor);
            vendorRepository.RemoveVendorBankinfoAttachment(vendorId);
        }
        public BankInformation GetBankInformation(int vendorId)
        {
            return vendorRepository.GetBankInformation(vendorId);
        }

        public BankInformation GetBankInformationById(int bankId)
        {
            return vendorRepository.GetBankInformationById(bankId);
        }
        // Asset Methods
        public IReadOnlyList<Asset> IPadList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection)
        {

            return assetRepository.IPadList(skip, take, searchValue, orderColumnIndex, orderDirection);
        }

        public IReadOnlyList<Asset> IPadList(string searchValue)
        {

            return assetRepository.IPadList(searchValue);
        }

        public IReadOnlyList<Asset> IPadList()
        {
            return assetRepository.IPadList();
        }

        public void AddIPad(int amountToAdd, int? vendorId, int assetTypeID, string createdBy)
        {
            assetRepository.AddIPad(amountToAdd, vendorId, assetTypeID, createdBy);
        }

        public void AssignIPad(int iPadId, int vendorId, string unAssignBy)
        {
            assetRepository.AssignIPad(iPadId, vendorId, unAssignBy);
        }

        public void UnAssignIPad(int iPadId, string unAssignBy)
        {
            assetRepository.UnAssignIPad(iPadId, unAssignBy);
        }

        public void ActiveIPad(int iPadId, string activatedBy)
        {
            assetRepository.ActiveIPad(iPadId, activatedBy);
        }

        public void DeActiveIPad(int iPadId, string deActivatedBy)
        {
            assetRepository.DeActiveIPad(iPadId, deActivatedBy);
        }

        public string CheckForUnassignedIPadSearchValue(string searchValue)
        {
            return assetRepository.CheckForUnassignedIPadSearchValue(searchValue);
        }

        //AppUser Methods
        public IReadOnlyList<AppUserModel> GetAppUserList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection)
        {
            string RegistrationNumber = string.Empty;
            if (null != SecurityContextHelper.CurrentVendor)
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }

            return appUserRepository.GetAppUserList(skip, take, searchValue, orderColumnIndex, orderDirection, RegistrationNumber);
        }

        public IReadOnlyList<AppUserModel> GetAppUserList(string searchValue)
        {
            string RegistrationNumber = string.Empty;
            if (null != SecurityContextHelper.CurrentVendor)
            {
                RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }
            return appUserRepository.GetAppUserList(searchValue, RegistrationNumber);
        }

        public void ActivateAppUser(int qrCodeId)
        {
            appUserRepository.ActivateAppUser(qrCodeId);
        }

        public void DeActivateAppUser(int qrCodeId)
        {
            appUserRepository.DeActivateAppUser(qrCodeId);
        }

        public void AddAppUser(int amountToAdd, int vendorId, string createdBy)
        {
            appUserRepository.AddAppUser(amountToAdd, vendorId, createdBy);
        }

        public IList<AppUser> GetUpdatedAppUserList(DateTime lastUpdated)
        {
            return appUserRepository.GetUpdatedAppUserList(lastUpdated);
        }

        public IList<Vendor> GetVendorsByApplicationId(int applicationId)
        {
            return this.vendorRepository.GetVendorsByApplicationId(applicationId);
        }
        public Vendor FindVendorByID(int vendorId)
        {
            return vendorRepository.FindVendorByID(vendorId);
        }

        public VendorReference GetVendorByNumber(string number)
        {
            return vendorRepository.GetVendorByNumber(number);
        }

        public VendorReference GetVendorByNumber(string number, long userId, string userName)
        {
            return vendorRepository.GetVendorByNumber(number, userId, userName);
        }
        public VendorReference GetVendorById(int vendorId)
        {
            return vendorRepository.GetVendorById(vendorId);
        }

        #region Spotlight Search

        public IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlights(string keyWords, string section, int? pageLimit)
        {
            var tmp = vendorRepository.GetRegistrantSpotlights(keyWords, section, pageLimit).ToList();
            var result = new List<RegistrantSpotlightModel>();
            var GSModuleType = AppDefinitions.Instance.TypeDefinitions["GSModuleType"].ToList();
            result.AddRange(tmp.Select(i => new RegistrantSpotlightModel()
            {
                ID = i.ID,
                //Number = i.Number,
                //LegalName = i.LegalName,
                //OperatingName = i.OperatingName,
                vendorID = i.vendorID,
                PlaceHolder1 = i.PlaceHolder1,
                PlaceHolder2 = i.PlaceHolder2,
                PlaceHolder3 = i.PlaceHolder3,
                PlaceHolder4 = i.PlaceHolder4,
                PlaceHolder5 = i.PlaceHolder5,
                ModuleType = GSModuleType.First(t => t.DefinitionValue == i.ModuleType).Code,
                Status = i.Status,
                //Section = GlobalSearchSection.Applications.ToString()
                Section = i.Section
            }));
            this.UpdateRegistrantSpotlightUrls(ref result);
            return result;
        }
        public IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlightsPaginate(string keyWords, string section, int start, int pageLimit)
        {
            var tmp = vendorRepository.GetRegistrantSpotlightPaginate(keyWords, section, start, pageLimit).ToList();
            var result = new List<RegistrantSpotlightModel>();
            var GSModuleType = AppDefinitions.Instance.TypeDefinitions["GSModuleType"].ToList();
            result.AddRange(tmp.Select(i => new RegistrantSpotlightModel()
            {
                ID = i.ID,
                //Number = i.Number,
                //LegalName = i.LegalName,
                //OperatingName = i.OperatingName,
                vendorID = i.vendorID,
                PlaceHolder1 = i.PlaceHolder1,
                PlaceHolder2 = i.PlaceHolder2,
                PlaceHolder3 = i.PlaceHolder3,
                PlaceHolder4 = i.PlaceHolder4,
                PlaceHolder5 = i.PlaceHolder5,
                ModuleType = GSModuleType.First(t => t.DefinitionValue == i.ModuleType).Code,
                //Active = i.Active,
                Status = i.Status,
                TotalCount = i.TotalCount
            }));
            this.UpdateRegistrantSpotlightUrls(ref result);
            return result;
        }
        public PaginationDTO<RegistrantSpotlightModel, int> LoadSpotlightViewMoreResult(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string keyWords, int pageLimit, string section)
        {
            var GSModuleType = AppDefinitions.Instance.TypeDefinitions["GSModuleType"].ToList();

            return vendorRepository.LoadSpotlightViewMoreResult(pageIndex, pageSize, searchText, orderBy, sortDirection, keyWords, pageLimit, GSModuleType, section);
        }

        //OTSTM2-960/963/964/965/966
        public List<RegistrantSpotlightModel> LoadSpotlightResultExport(string searchText, string orderBy, string sortDirection, string keyWords, string section)
        {
            var GSModuleType = AppDefinitions.Instance.TypeDefinitions["GSModuleType"].ToList();

            return vendorRepository.LoadSpotlightResultExport(searchText, orderBy, sortDirection, keyWords, GSModuleType, section);
        }

        private void UpdateRegistrantSpotlightUrls(ref List<RegistrantSpotlightModel> spotlights)
        {
            spotlights.ForEach(i =>
            {
                if (i.ModuleType == "Steward" && i.Section == "Applications")
                {
                    i.Url = string.Format("{0}/Registration/CustomerIndex?cId={1}", i.ModuleType, i.ID);
                }
                else if (i.Section == "Applications")
                {
                    i.Url = string.Format("{0}/Registration/RegistrationIndex?vId={1}", i.ModuleType, i.ID);
                }
                else if (i.Section == "Claims")
                {
                    i.Url = string.Format("{0}/Claims/ClaimSummary?claimId={1}&vId={2}", i.ModuleType, i.ID, i.vendorID);
                }
                else if (i.Section == "Remittances")
                {
                    i.Url = string.Format("Steward/TSFRemittances/StaffIndex/{0}?vId={1}", i.ID, i.vendorID);
                }
                else if (i.Section == "Users")
                {
                    i.Url = string.Format("System/Invitation/ParticipantIndex?vendorId={0}&searchText={1}", i.vendorID, i.PlaceHolder2);
                }
                else if (i.Section == "Transactions")
                {
                    i.Url = string.Format("System/Transactions/Index?searchText={0}&transID={1}", i.PlaceHolder1, i.ID);
                }

            });
            return;
        }

        #endregion

        #region Application Notes
        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return applicationRepository.LoadApplicationNotes(applicationId, pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForVendor(int vendorId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return applicationRepository.LoadApplicationNotesForVendor(vendorId, pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForCustomer(int customerId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return applicationRepository.LoadApplicationNotesForCustomer(customerId, pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public void AddApplicationNode(ApplicationNote applicationNote)
        {
            applicationRepository.AddApplicationNote(applicationNote);
        }

        public List<ApplicationNoteViewMode> LoadApplicationNotesForExport(int applicationId, string searchText,
            string sortcolumn, string sortdirection)
        {
            return applicationRepository.LoadApplicationNotesForExport(applicationId, searchText, sortcolumn,
                sortdirection);
        }

        public List<ApplicationNoteViewMode> LoadVendorNotesForExport(int vendorId, string searchText,
            string sortcolumn, string sortdirection)
        {
            return applicationRepository.LoadVendorNotesForExport(vendorId, searchText, sortcolumn,
                sortdirection);
        }

        public List<ApplicationNoteViewMode> LoadCustomerNotesForExport(int customerId, string searchText,
            string sortcolumn, string sortdirection)
        {
            return applicationRepository.LoadCustomerNotesForExport(customerId, searchText, sortcolumn,
                sortdirection);
        }


        #endregion

        #region TSFRemittance Notes
        public PaginationDTO<TSFRemittanceNoteViewMode, int> LoadTSFRemittanceNotes(int claimId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return tsfRemittanceRepository.LoadTSFRemittanceNotes(claimId, pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public void AddTSFRemittanceNote(TSFRemittanceNote applicationNote)
        {
            tsfRemittanceRepository.AddTSFRemittanceNote(applicationNote);
        }

        public List<TSFRemittanceNoteViewMode> LoadTSFRemittanceNotesForExport(int claimId, string searchText,
            string sortcolumn, string sortdirection)
        {
            return tsfRemittanceRepository.LoadTSFRemittanceNotesForExport(claimId, searchText, sortcolumn,
                sortdirection);
        }
        #endregion
        public bool UpdateVendorGeneratorStatus(int vendorId, bool Status, DateTime date)
        {
            Vendor vdr = vendorRepository.GetSingleVendorByID(vendorId);
            vdr.CIsGenerator = Status;
            vdr.CGeneratorDate = date;
            vendorRepository.UpdateVendor(vdr);

            return true;
        }

        //OTSTM2-953
        public bool UpdateVendorSubCollectorStatus(int vendorId, bool Status, DateTime date)
        {
            Vendor vdr = vendorRepository.GetSingleVendorByID(vendorId);
            vdr.IsSubCollector = Status;
            vdr.SubCollectorDate = date;
            vendorRepository.UpdateVendor(vdr);

            return true;
        }

        //Added
        public ApplicationEmailModel GetApprovedRegistrantInformation(int vendorId)
        {
            var vendor = vendorRepository.GetSingleVendorByID(vendorId);

            if (vendor != null)
            {
                var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);

                Contact vendorPrimaryContact = null;

                foreach (var address in vendor.VendorAddresses)
                {
                    foreach (var contact in address.Contacts)
                    {
                        if (contact.IsPrimary.Value)
                        {
                            vendorPrimaryContact = contact;
                            break;
                        }
                    }

                    if (vendorPrimaryContact != null)
                        break;
                }

                if (vendorPrimaryContact == null)
                {
                    throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
                }

                var applicationHaulerApplicationModel = new ApplicationEmailModel()
                {
                    PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                    PrimaryContactLastName = vendorPrimaryContact.LastName,
                    RegistrationNumber = vendor.Number,
                    BusinessName = vendor.BusinessName,
                    BusinessLegalName = vendor.BusinessName,
                    Address1 = vendorAddress.Address1,
                    City = vendorAddress.City,
                    ProvinceState = vendorAddress.Province,
                    PostalCode = vendorAddress.PostalCode,
                    Country = vendorAddress.Country,
                    Email = vendorPrimaryContact.Email
                };

                return applicationHaulerApplicationModel;
            }

            return null;
        }

        public ApplicationEmailModel GetApprovedCustomerInformation(int customerId)
        {
            var customer = vendorRepository.GetSingleCustomerByID(customerId);

            if (customer != null)
            {
                var customerAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);

                Contact customerPrimaryContact = null;

                foreach (var address in customer.CustomerAddresses)
                {
                    foreach (var contact in address.Contacts)
                    {
                        if (contact.IsPrimary.Value)
                        {
                            customerPrimaryContact = contact;
                            break;
                        }
                    }

                    if (customerPrimaryContact != null)
                        break;
                }

                if (customerPrimaryContact == null)
                {
                    throw new NullReferenceException("No primary contact found for customer: " + customer.BusinessName);
                }

                var ApplicationEmailModel = new ApplicationEmailModel()
                {
                    PrimaryContactFirstName = customerPrimaryContact.FirstName,
                    PrimaryContactLastName = customerPrimaryContact.LastName,
                    RegistrationNumber = customer.RegistrationNumber,
                    BusinessName = customer.BusinessName,
                    BusinessLegalName = customer.BusinessName,
                    Address1 = customerAddress.Address1,
                    City = customerAddress.City,
                    ProvinceState = customerAddress.Province,
                    PostalCode = customerAddress.PostalCode,
                    Country = customerAddress.Country,
                    Email = customerPrimaryContact.Email
                };

                return ApplicationEmailModel;
            }

            return null;
        }

        #region NewActiveInactive        
        public void ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory)
        {
            //OTSTM2-72
            bool isNotifyEmail = false;

            if (Boolean.TryParse(ConfigurationManager.AppSettings["UATemailNotificationsInactive"], out isNotifyEmail))
            {
                if (!vendorActiveHistory.ActiveState && isNotifyEmail)
                {
                    SendGroupEmailForInactiveEventChange(vendorActiveHistory.VendorID, vendorActiveHistory);
                }
            }
            if (vendorRepository.ActiveInactiveVendor(vendorActiveHistory))
            {
                //claim should be only created at 12:00AM first day of month
                //claimsRepository.Initialize3ClaimsForProcessorRMP(vendorActiveHistory.VendorID, vendorActiveHistory.ActiveStateChangeDate);
            }

            //Delete TCR Service Threshold when collector is made InActive
            if (vendorActiveHistory.ActiveState == false)
            {
                var vendor = vendorRepository.GetSingleVendorByID(vendorActiveHistory.VendorID);
                if (vendor != null && vendor.VendorType == (int)ClaimType.Collector)
                    vendorRepository.RemoveTCRServiceThresohold(vendorActiveHistory.VendorID, SecurityContextHelper.CurrentUser.Id);
            }
        }
        //OTSTM2-72
        private void SendGroupEmailForInactiveEventChange(int vendorId, VendorActiveHistory vendorActiveHistory)
        {
            //1.Get Vendor Type
            Vendor vendor = vendorRepository.FindVendorByID(vendorId);

            if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
            {
                if (EmailContentHelper.IsEmailEnabled("InactiveParticipantHauler"))
                    NotifyProcessorsForHaulerInactive(vendor, vendorActiveHistory);
            }
            else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
            {
                //notify haulers                
                if (EmailContentHelper.IsEmailEnabled("InactiveParticipantCollector"))
                    NotifyHaulersForCollectorInactive(vendor, vendorActiveHistory);
            }
            else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
            {
                if (EmailContentHelper.IsEmailEnabled("InactiveParticipantRPM"))
                    NotifyProcessorsForRPMInactive(vendor, vendorActiveHistory);
            }
            else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
            {
                //notify RPM's & Hauler
                if (EmailContentHelper.IsEmailEnabled("InactiveParticipantProcessor"))
                {
                    NotifyRPMsForProcessorInactive(vendor, vendorActiveHistory);
                    NotifyHaulersForProcessorInactive(vendor, vendorActiveHistory);
                }
            }
        }
        //OTSTM2-72
        private void NotifyHaulersForCollectorInactive(Vendor vendor, VendorActiveHistory vendorActiveHistory)
        {
            //1
            var allEmails = vendorRepository.GetAllPrimaryEmailAddressByVendorType(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue).ToList();

            if (allEmails.Count > 0)
            {
                //2
                #region prepare model

                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.NotifyHaulersForCollectorInactive"));
                //string emailBody = File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("InactiveParticipantCollector");
                string subject = EmailContentHelper.GetSubjectByName("InactiveParticipantCollector");

                var businessAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                emailBody = emailBody
                    .Replace(InactiveVendorEmailNotifications.Date, vendorActiveHistory.CreateDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.InactiveRegType, AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").Code)
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(InactiveVendorEmailNotifications.ActiveRegType, "Hauler")
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(InactiveVendorEmailNotifications.InactiveBusinessName, vendor.BusinessName)
                    .Replace(InactiveVendorEmailNotifications.InactiveAddress, string.Format("{0} {1} {2} {3}", businessAddress.Address1, businessAddress.City,
                    businessAddress.Province, businessAddress.PostalCode).ToUpper())
                    .Replace(InactiveVendorEmailNotifications.InactiveDate, vendorActiveHistory.ActiveStateChangeDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = InactiveVendorEmailNotifications.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = InactiveVendorEmailNotifications.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = InactiveVendorEmailNotifications.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Inactive Collector Registration Number {0}", vendor.Number);
                #endregion

                //3            
                ProcessBulkEmails(allEmails, subject, emailBody, alternateViewHTML);
            }
        }

        //OTSTM2-72
        private void NotifyRPMsForProcessorInactive(Vendor vendor, VendorActiveHistory vendorActiveHistory)
        {
            //1
            var allEmails = vendorRepository.GetAllPrimaryEmailAddressByVendorType(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue).ToList();

            if (allEmails.Count > 0)
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.NotifyHaulersAndRpmsForProcessorInactive"));
                //string emailBody = File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("InactiveParticipantProcessor");
                string subject = EmailContentHelper.GetSubjectByName("InactiveParticipantProcessor");

                var businessAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                emailBody = emailBody
                    .Replace(InactiveVendorEmailNotifications.Date, vendorActiveHistory.CreateDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.InactiveRegType, AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").Code)
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(InactiveVendorEmailNotifications.ActiveRegType, "Haulers & RPMs")
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(InactiveVendorEmailNotifications.InactiveBusinessName, vendor.BusinessName)
                    .Replace(InactiveVendorEmailNotifications.InactiveAddress, string.Format("{0} {1} {2} {3}", businessAddress.Address1, businessAddress.City,
                        businessAddress.Province, businessAddress.PostalCode).ToUpper())
                    .Replace(InactiveVendorEmailNotifications.InactiveDate, vendorActiveHistory.ActiveStateChangeDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = InactiveVendorEmailNotifications.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = InactiveVendorEmailNotifications.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = InactiveVendorEmailNotifications.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Inactive Processor Registration Number {0}", vendor.Number);

                //3            
                ProcessBulkEmails(allEmails, subject, emailBody, alternateViewHTML);
            }
        }

        //OTSTM2-72
        private void NotifyHaulersForProcessorInactive(Vendor vendor, VendorActiveHistory vendorActiveHistory)
        {
            //1
            var allEmails = vendorRepository.GetAllPrimaryEmailAddressByVendorType(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue).ToList();

            if (allEmails.Count > 0)
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.NotifyHaulersAndRpmsForProcessorInactive"));
                //string emailBody = File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("InactiveParticipantProcessor");
                string subject = EmailContentHelper.GetSubjectByName("InactiveParticipantProcessor");

                var businessAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                emailBody = emailBody
                    .Replace(InactiveVendorEmailNotifications.Date, vendorActiveHistory.CreateDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.InactiveRegType, AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").Code)
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(InactiveVendorEmailNotifications.ActiveRegType, "Haulers & RPMs")
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(InactiveVendorEmailNotifications.InactiveBusinessName, vendor.BusinessName)
                    .Replace(InactiveVendorEmailNotifications.InactiveAddress, string.Format("{0} {1} {2} {3}", businessAddress.Address1, businessAddress.City,
                    businessAddress.Province, businessAddress.PostalCode).ToUpper())
                    .Replace(InactiveVendorEmailNotifications.InactiveDate, vendorActiveHistory.ActiveStateChangeDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = InactiveVendorEmailNotifications.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = InactiveVendorEmailNotifications.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = InactiveVendorEmailNotifications.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Inactive Processor Registration Number {0}", vendor.Number);

                //3            
                ProcessBulkEmails(allEmails, subject, emailBody, alternateViewHTML);
            }
        }

        //OTSTM2-72
        private void NotifyProcessorsForRPMInactive(Vendor vendor, VendorActiveHistory vendorActiveHistory)
        {
            //1
            var allEmails = vendorRepository.GetAllPrimaryEmailAddressByVendorType(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue).ToList();

            if (allEmails.Count > 0)
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.NotifyProcessorsForRPMInactive"));
                //string emailBody = File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("InactiveParticipantRPM");
                string subject = EmailContentHelper.GetSubjectByName("InactiveParticipantRPM");

                var businessAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                emailBody = emailBody
                    .Replace(InactiveVendorEmailNotifications.Date, vendorActiveHistory.CreateDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.InactiveRegType, AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").Code)
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(InactiveVendorEmailNotifications.ActiveRegType, "Processors")
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(InactiveVendorEmailNotifications.InactiveBusinessName, vendor.BusinessName)
                    .Replace(InactiveVendorEmailNotifications.InactiveAddress, string.Format("{0} {1} {2} {3}", businessAddress.Address1, businessAddress.City,
                    businessAddress.Province, businessAddress.PostalCode).ToUpper())
                    .Replace(InactiveVendorEmailNotifications.InactiveDate, vendorActiveHistory.ActiveStateChangeDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = InactiveVendorEmailNotifications.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = InactiveVendorEmailNotifications.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = InactiveVendorEmailNotifications.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Inactive RPM Registration Number {0}", vendor.Number);

                //3            
                ProcessBulkEmails(allEmails, subject, emailBody, alternateViewHTML);
            }
        }

        //OTSTM2-72
        private void NotifyProcessorsForHaulerInactive(Vendor vendor, VendorActiveHistory vendorActiveHistory)
        {
            //1
            var allEmails = vendorRepository.GetAllPrimaryEmailAddressByVendorType(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue).ToList();

            if (allEmails.Count > 0)
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.NotifyProcessorsForHaulerInactive"));
                //string emailBody = File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("InactiveParticipantHauler");
                string subject = EmailContentHelper.GetSubjectByName("InactiveParticipantHauler");

                var businessAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                emailBody = emailBody
                    .Replace(InactiveVendorEmailNotifications.Date, vendorActiveHistory.CreateDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.InactiveRegType, AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").Code)
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(InactiveVendorEmailNotifications.ActiveRegType, "Processors")
                    .Replace(InactiveVendorEmailNotifications.InactiveRegNo, vendor.Number)
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(InactiveVendorEmailNotifications.InactiveBusinessName, vendor.BusinessName)
                    .Replace(InactiveVendorEmailNotifications.InactiveAddress, string.Format("{0} {1} {2} {3}", businessAddress.Address1, businessAddress.City,
                    businessAddress.Province, businessAddress.PostalCode).ToUpper())
                    .Replace(InactiveVendorEmailNotifications.InactiveDate, vendorActiveHistory.ActiveStateChangeDate.ToString("MMMM dd, yyyy"))
                    .Replace(InactiveVendorEmailNotifications.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = InactiveVendorEmailNotifications.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = InactiveVendorEmailNotifications.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = InactiveVendorEmailNotifications.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = string.Format("Inactive Hauler Registration Number {0}", vendor.Number);

                //3            
                ProcessBulkEmails(allEmails, subject, emailBody, alternateViewHTML);
            }
        }

        //OTSTM2-72
        private void ProcessBulkEmails(List<Contact> allEmails, string subject, string emailBody, AlternateView alternateViewHTML)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                                 AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                                 AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                                 AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                                 AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                                 AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                                 AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                                 AppSettings.Instance.GetSettingValue("Company.Email"),
                                 Convert.ToBoolean(AppSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(AppSettings.Instance.GetSettingValue("Company.Email"));

            // keep number of threads low for less cpu intensive             
            var totalThreads = 5;
            var totalItems = allEmails.Count;
            int iterator = totalItems / (totalThreads - 1);
            if (iterator == 0)
                iterator = totalItems;

            for (var i = 0; i < totalItems; i += iterator)
            {
                var batch = allEmails.Skip(i).Take(iterator).Select(c => c.Email).ToList();

                try
                {
                    emailer.SendEmailBulk(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), batch, null, null, subject, emailBody, null, null, alternateViewHTML);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
            }
        }

        public VendorActiveHistory LoadCurrenActiveHistoryForVendor(int vendorId)
        {
            var result = vendorRepository.LoadCurrenActiveHistoryForVendor(vendorId);
            if (result.ChangeToActive)
            {
                //ET-37 Stop creating claims
                //claimsRepository.InitializeClaimForVendor(vendorId);
            }

            return result;
        }

        public VendorActiveHistory LoadPreviousActiveHistoryForVendor(int vendorId, bool isActive)
        {
            return vendorRepository.LoadPreviousActiveHistoryForVendor(vendorId, isActive);
        }

        public void ActiveInactiveCustomer(CustomerActiveHistory customerrActiveHistory)
        {
            vendorRepository.ActiveInactiveCustomer(customerrActiveHistory);
        }

        public CustomerActiveHistory LoadCurrenActiveHistoryForCustomer(int customerId)
        {
            return vendorRepository.LoadCurrenActiveHistoryForCustomer(customerId);
        }

        public CustomerActiveHistory LoadPreviousActiveHistoryForCustomer(int vendorId, bool isActive)
        {
            return vendorRepository.LoadPreviousActiveHistoryForCustomer(vendorId, isActive);
        }

        #endregion

        #region GP
        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                // STEP 1 GET LIST OF REGISTRANTS NOT IN BATCH
                var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID") ?? "TEST1";

                var participants = vendorRepository.LoadParticipants();

                participants.ForEach(p =>
                {
                    p.Contact = this.FindPrimaryContact(p.Addresses);
                });

                if (!participants.Any())
                {
                    msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
                }
                else
                {
                    // STEP 2 INSERT INTO GpiBatch table		
                    var gpiBatch = new GpiBatch()
                    {
                        GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Participant),
                        GpiBatchCount = participants.Count(),
                        GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                    };
                    gpRepository.AddGpBatch(gpiBatch);
                    msg.ID = gpiBatch.ID;
                    //STEP 3 INSERT INTO GpiBatchEntry table
                    var gpiBatchEntries = (from participant in participants
                                           let gpiTxnNumber = this.GetTxnNumber(participant.RegistrationNumber)
                                           select new GpiBatchEntry()
                                           {
                                               GpiBatchID = gpiBatch.ID,
                                               GpiTxnNumber = gpiTxnNumber,
                                               GpiBatchEntryDataKey = participant.RegistrationNumber,
                                               GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                                               CreatedDate = DateTime.UtcNow,
                                               CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                                               LastUpdatedDate = DateTime.UtcNow,
                                               LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                                           }).ToList();
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    //STEP 4 INSERT INTO RrOtsRmCustomers
                    var tmpParticipants = participants.Join(gpiBatchEntries,
                        p => p.RegistrationNumber,
                        e => e.GpiBatchEntryDataKey,
                        (p, e) => new { p, e }).Where(r => r.e.GpiBatchID == gpiBatch.ID).Select(r => new
                        {
                            participant = r.p,
                            GpiBatchEntryId = r.e.ID
                        }).ToList();

                    var rrOtsRmCustomers = new List<RrOtsRmCustomer>();
                    foreach (var row in tmpParticipants)
                    {
                        var p = row.participant;
                        int? INTSTATUS = 0;
                        if (p.GpUpdate) INTSTATUS = null;
                        var address = p.Address ?? new Address();
                        var contact = p.Contact ?? new Contact();

                        var strCntcprsn = string.Format("{0} {1}", contact.FirstName, contact.LastName);
                        var strCustName = p.BusinessName;
                        var customer = new RrOtsRmCustomer()
                        {
                            CUSTNMBR = p.RegistrationNumber,
                            CUSTNAME = strCustName.Length > 65 ? strCustName.Substring(0, 65) : strCustName,
                            CUSTCLAS = p.RegistrantTypeGP,
                            CNTCPRSN = strCntcprsn.Length > 61 ? strCntcprsn.Substring(0, 61) : strCntcprsn,
                            ADRSCODE = "PRIMARY",
                            TAXSCHID = p.RegistrantTypeId == 1 ? (p.IsTaxExempt ? "S-EXEMPT" : "S-HST") : "P-EXEMPT",
                            ADDRESS1 = address.Address1 != null && address.Address1.Length > 61 ? address.Address1.Substring(0, 61) : address.Address1,
                            ADDRESS2 = address.Address2 != null && address.Address2.Length > 61 ? address.Address2.Substring(0, 61) : address.Address2,
                            ADDRESS3 = address.Address3 != null && address.Address3.Length > 61 ? address.Address3.Substring(0, 61) : address.Address3,
                            COUNTRY = address.Country != null && address.Country.Length > 61 ? address.Country.Substring(0, 61) : address.Country,
                            CITY = address.City != null && address.City.Length > 35 ? address.City.Substring(0, 35) : address.City,
                            //TODO fix hardcoded value for province p.Address.Province
                            STATE = "ON",
                            ZIP = address.PostalCode != null && address.PostalCode.Length > 11 ? address.PostalCode.Substring(0, 11) : address.PostalCode,
                            PHNUMBR1 = (contact.PhoneNumber ?? string.Empty).Trim().Length > 21 ? (contact.PhoneNumber ?? string.Empty).Trim().Substring(0, 21) : (contact.PhoneNumber ?? string.Empty).Trim(),
                            FAX = (contact.FaxNumber ?? string.Empty).Trim().Length > 21 ? (contact.FaxNumber ?? string.Empty).Trim().Substring(0, 21) : (contact.FaxNumber ?? string.Empty).Trim(),
                            CURNCYID = null,
                            PYMTRMID = null,
                            USERDEF1 = gpiBatch.ID.ToString(),
                            USERDEF2 = null,
                            INACTIVE = p.IsActive ? 0 : 1,
                            INTERID = INTERID,
                            INTSTATUS = INTSTATUS,
                            INTDATE = null,
                            ERRORCODE = null,
                            //DEX_ROW_ID = -1,
                            gpibatchentry_id = row.GpiBatchEntryId
                        };
                        rrOtsRmCustomers.Add(customer);
                    }
                    gpRepository.AddGpRrOtsCustomers(rrOtsRmCustomers);

                    // STEP 5 INSERT INTO RrOtsPmVendors
                    var rrOtsPmVendors = new List<RrOtsPmVendor>();
                    foreach (var row in tmpParticipants.Where(v => v.participant.IsVendor))
                    {

                        var p = row.participant;
                        int? INTSTATUS = 0;
                        if (p.GpUpdate) INTSTATUS = null;
                        var address = p.Address ?? new Address();
                        var contact = p.Contact ?? new Contact();
                        var strCntcprsn = string.Format("{0} {1}", contact.FirstName, contact.LastName);
                        var strCustName = p.BusinessName;
                        var bankingInfo = p.BankInformation ?? new BankInformation();
                        var vendor = new RrOtsPmVendor()
                        {
                            VENDORID = p.RegistrationNumber,
                            VENDNAME = strCustName.Length > 65 ? strCustName.Substring(0, 65) : strCustName,
                            VADDCDPR = "PRIMARY",
                            VNDCLSID = p.RegistrantTypeGP,
                            VNDCNTCT = strCntcprsn.Length > 61 ? strCntcprsn.Substring(0, 61) : strCntcprsn,
                            ADDRESS1 = address.Address1 != null && address.Address1.Length > 61 ? address.Address1.Substring(0, 61) : address.Address1,
                            ADDRESS2 = address.Address2 != null && address.Address2.Length > 61 ? address.Address2.Substring(0, 61) : address.Address2,
                            ADDRESS3 = address.Address3 != null && address.Address3.Length > 61 ? address.Address3.Substring(0, 61) : address.Address3,
                            CITY = address.City != null && address.City.Length > 35 ? address.City.Substring(0, 35) : address.City,
                            //TODO fix hardcoded value for province p.Address.Province
                            STATE = "ON",
                            ZIPCODE = address.PostalCode != null && address.PostalCode.Length > 11 ? address.PostalCode.Substring(0, 11) : address.PostalCode,
                            COUNTRY = address.Country != null && address.Country.Length > 61 ? address.Country.Substring(0, 61) : address.Country,
                            PHNUMBR1 = (contact.PhoneNumber ?? string.Empty).Trim().Length > 21 ? (contact.PhoneNumber ?? string.Empty).Trim().Substring(0, 21) : (contact.PhoneNumber ?? string.Empty).Trim(),
                            FAXNUMBR = (contact.FaxNumber ?? string.Empty).Trim().Length > 21 ? (contact.FaxNumber ?? string.Empty).Trim().Substring(0, 21) : (contact.FaxNumber ?? string.Empty).Trim(),
                            CURNCYID = null,
                            TAXSCHID = p.IsTaxExempt ? "P-EXEMPT" : "P-HST",
                            PYMTRMID = null,
                            USERDEF1 = gpiBatch.ID.ToString(),
                            USERDEF2 = null,
                            VENDSTTS = p.IsActive ? 1 : 2,
                            INTERID = INTERID,
                            INTSTATUS = INTSTATUS,
                            INTDATE = null,
                            ERRORCODE = null,
                            //DEX_ROW_ID = -1,
                            gpibatchentry_id = row.GpiBatchEntryId,
                            BANKNAME = (bankingInfo.BankName ?? string.Empty).Trim().Length > 61 ? (bankingInfo.BankName ?? string.Empty).Trim().Substring(0, 61) : (bankingInfo.BankName ?? string.Empty).Trim(),
                            EFTTransitNumber = bankingInfo.TransitNumber,
                            EFTBankNumber = (bankingInfo.BankNumber ?? string.Empty).Trim(),
                            EFTBankAcct = (bankingInfo.AccountNumber ?? string.Empty).Trim(),
                            EmailToAddress = (bankingInfo.Email ?? string.Empty).Trim(),
                            EmailCcAddress = (bankingInfo.CCEmail ?? string.Empty).Trim(),
                            Key = bankingInfo.Key,
                        };
                        rrOtsPmVendors.Add(vendor);
                    }
                    gpRepository.AddGpRrOtsVendors(rrOtsPmVendors);

                    // STEP 6 UPDATE registrant TABLE
                    var customerParticipantIds = tmpParticipants.Where(p => !p.participant.IsVendor).Select(p => p.participant.ObjectID).ToList();

                    vendorRepository.UpdateCustomerForGpBatch(customerParticipantIds);
                    var vendorParticipantIds = tmpParticipants.Where(v => v.participant.IsVendor).Select(p => p.participant.ObjectID).ToList();
                    vendorRepository.UpdateVendorForGpBatch(vendorParticipantIds);
                }
                transationScope.Complete();
            }
            return msg;
        }


        private Contact FindPrimaryContact(List<Address> lstAddress)
        {
            Contact primaryContact = null;

            var contractAddress = lstAddress.FirstOrDefault(c => c.AddressType == 3);
            if (contractAddress != null) primaryContact = contractAddress.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);

            if (primaryContact == null)
            {
                var businessAddess = lstAddress.FirstOrDefault(c => c.AddressType == 1);
                if (businessAddess != null) primaryContact = businessAddess.Contacts.FirstOrDefault(c => (bool)c.IsPrimary);
            }

            return primaryContact;
        }


        public List<GpBatchDto> GetGpBatchDtos(string type)
        {
            return gpRepository.GetAllGpBatches().Where(b => b.GpiTypeID == type).ToList().Select(ModelAdapter.ToGpBatchDto).ToList();
        }
        public PaginationDTO<GPListViewModel, int> GetAllGpBatchesPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string type)
        {

            Dictionary<string, string> GpiStatus = new Dictionary<string, string>()
            {
                { GpHelper.GetGpiStatusString(GpStatus.PostedQueued), EnumHelper.GetEnumDescription(GpStatus.PostedQueued)},
                { GpHelper.GetGpiStatusString(GpStatus.PostedImported), EnumHelper.GetEnumDescription(GpStatus.PostedImported) },
                { GpHelper.GetGpiStatusString(GpStatus.FailedStaging), EnumHelper.GetEnumDescription(GpStatus.FailedStaging) },
                { GpHelper.GetGpiStatusString(GpStatus.FailedImport), EnumHelper.GetEnumDescription(GpStatus.FailedImport) },
                { GpHelper.GetGpiStatusString(GpStatus.Hold), EnumHelper.GetEnumDescription(GpStatus.Hold) },
                { GpHelper.GetGpiStatusString(GpStatus.Extract), EnumHelper.GetEnumDescription(GpStatus.Extract) }
            };

            return gpRepository.GetAllGpBatchesPagination(pageIndex, pageSize, searchText, orderBy, sortDirection, type, GpiStatus);
        }
        public PaginationDTO<GPModalListViewModal, int> GetGpTransactionsByBatchIdPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int batchId)
        {

            Dictionary<string, string> GpiStatus = new Dictionary<string, string>()
            {
                { GpHelper.GetGpiStatusString(GpStatus.PostedQueued), EnumHelper.GetEnumDescription(GpStatus.PostedQueued)},
                { GpHelper.GetGpiStatusString(GpStatus.PostedImported), EnumHelper.GetEnumDescription(GpStatus.PostedImported) },
                { GpHelper.GetGpiStatusString(GpStatus.FailedStaging), EnumHelper.GetEnumDescription(GpStatus.FailedStaging) },
                { GpHelper.GetGpiStatusString(GpStatus.FailedImport), EnumHelper.GetEnumDescription(GpStatus.FailedImport) },
                { GpHelper.GetGpiStatusString(GpStatus.Hold), EnumHelper.GetEnumDescription(GpStatus.Hold) },
                { GpHelper.GetGpiStatusString(GpStatus.Extract), EnumHelper.GetEnumDescription(GpStatus.Extract) }
            };

            return gpRepository.GetGpTransactionsByBatchIdPagination(pageIndex, pageSize, searchText, orderBy, sortDirection, batchId, GpiStatus);
        }
        public List<GpBatchEntryDto> GetGpBatchEntries(int gpBatchId)
        {
            return gpRepository.GetGpBatchEntriesByBatchId(gpBatchId).ToList().Select(ModelAdapter.ToGpBatchEntryDto).ToList();
        }
        public ResponseMsgBase RemoveFromBatch(int gpiBatchEntryId)
        {
            var msg = new GpResponseMsg();
            var entry = gpRepository.GetGpBatchEntry(gpiBatchEntryId);
            if (entry != null)
            {

                if (GpHelper.GetGpiStatus(entry.GpiStatusID) == GpStatus.PostedImported ||
                    GpHelper.GetGpiStatus(entry.GpiStatusID) == GpStatus.PostedQueued ||
                    GpHelper.GetGpiStatus(entry.GpiStatusID) == GpStatus.Extract ||
                    GpHelper.GetGpiStatus(entry.GpiStatusID) == GpStatus.FailedImport ||
                    GpHelper.GetGpiStatus(entry.GpiStatusID) == GpStatus.FailedStaging)
                {
                    msg.Errors.Add("You cannot remove a posted, extracted or failed (import) entry from a batch");
                    return msg;
                }

                if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Participant), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
                {
                    var regNo = entry.GpiBatchEntryDataKey;

                    if (!vendorRepository.UpdateVendorForRemoveBatch(regNo))
                    {
                        vendorRepository.UpdateCustomerForRemoveBatch(regNo);
                    }

                }
                if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Steward), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
                {
                    var claimId = entry.GpiBatchEntryDataKey;
                    var tsfClaim = tsfRemittanceRepository.GetTSFClaimById(int.Parse(claimId));
                    tsfClaim.RecordState = "Under Review";
                    tsfClaim.InBatch = false;
                    tsfClaim.ModifiedDate = DateTime.Now;
                    tsfClaim.ModifiedUser = SecurityContextHelper.CurrentUser.UserName;

                    //OTSTM2-859
                    AddRemittanceActivityForRemoveBatch(tsfClaim, entry);

                    tsfRemittanceRepository.UpdateTsfClaim(tsfClaim);
                }
                if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Processor), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                    || string.Equals(GpHelper.GetGpiTypeID(GpManager.Hauler), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                    || string.Equals(GpHelper.GetGpiTypeID(GpManager.Collector), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                    || string.Equals(GpHelper.GetGpiTypeID(GpManager.RPM), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
                {
                    var claimId = entry.GpiBatchEntryDataKey;
                    var claimIdValue = int.Parse(claimId);

                    //OTSTM2-657
                    AddNotificationForRemoveBatch(claimIdValue);

                    //OTSTM2-751
                    AddClaimActivityForRemoveBatch(claimIdValue, entry);

                    ResetClaim(claimIdValue);
                }

            }

            //OTSTM2-745
            AddGPActivityForRemoveBatch(entry);

            gpRepository.DeleteGpEntry(entry);

            return msg;
        }

        private void ResetClaim(int claimId)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            claim.Status = "Under Review";
            claim.InBatch = false;
            claim.ApprovedBy = null;
            claim.ApprovalDate = null;
            claim.ReviewEndDate = null;
            if (claim.ClaimProcess != null)
            {
                //claim.CliamProcess.SupervisorReview = null;
                //claim.CliamProcess.SupervisorReviewDate = null;
                claim.ClaimProcess.Approver1 = null;
                claim.ClaimProcess.Approver1Date = null;
                claim.ClaimProcess.Approver2 = null;
                claim.ClaimProcess.Approver2Date = null;

                claim.AssignToUserId = claim.ClaimProcess.SupervisorReview; //OTSTM2-794
                claim.ReviewedBy = claim.ClaimProcess.SupervisorReview;
                claim.ClaimProcess.SupervisorReview = null; //OTSTM2-794
                claim.ClaimProcess.SupervisorReviewDate = null; //OTSTM2-794          
            }
            if (claim.ClaimPaymentSummary != null)
                claimsRepository.RemoveClaimPaymentSummary(claim.ClaimPaymentSummary);
            claimsRepository.UpdateClaim(claim);

            //Rollback Claim Inventory Changes
            RollbackClaimInventoryChanges(claim);

            //Recalculation for the current claim and next claims
            var claimsIds = claimsRepository.GetNextClaimsIds(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            claimsIds.Insert(0, claim.ID);
            claimsIds.ForEach(c =>
            {
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = c };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            });
        }

        private void RollbackClaimInventoryChanges(Claim claim)
        {
            //Calculate inbound, outbound and inventory adjustment
            List<InventoryItem> claimInventoryItems;
            if (claim.ClaimType == (int)ClaimType.Hauler)
                claimInventoryItems = claimsRepository.LoadHaulerClaimItems(claim.ID);
            else
                claimInventoryItems = claimsRepository.LoadClaimItems(claim.ID);

            var query = claimInventoryItems
                .GroupBy(c => new { c.ItemId, c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var inventoryItems = query.Select(c => new Inventory
            {
                VendorId = claim.ParticipantId,
                ItemId = c.Name.ItemId,
                IsEligible = c.Name.IsEligible,
                Qty = c.Name.Direction ? c.Qty : 0 - c.Qty,
                Weight = c.Name.Direction ? c.Weight : 0 - c.Weight,
                ActualWeight = c.Name.Direction ? c.ActualWeight : 0 - c.ActualWeight
            }).ToList();

            var newInventoryItemsQuery = inventoryItems.GroupBy(c => new { c.ItemId, c.IsEligible }).
                Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var newInventoryItems = newInventoryItemsQuery.Select(c =>
                new Inventory
                {
                    VendorId = claim.ParticipantId,
                    ItemId = c.Name.ItemId,
                    IsEligible = c.Name.IsEligible,
                    Qty = c.Qty,
                    Weight = c.Weight,
                    ActualWeight = c.ActualWeight,
                    UpdatedBy = SecurityContextHelper.CurrentUser.Id,
                    UpdatedDate = DateTime.Now
                }
             ).ToList();

            claimsRepository.RollbackVendorInventory(newInventoryItems, claim.ParticipantId);
        }

        public Object[] GetExport(int gpiBatchId)
        {
            var gpBatch = gpRepository.GetGpBatch(gpiBatchId);
            if (gpBatch != null)
            {
                if (GpHelper.GetGpiTypeID(GpManager.Participant) == gpBatch.GpiTypeID)
                {
                    var vendors = gpRepository.GetRrOtsPmVendorsByBatchId(gpiBatchId);
                    var customers = gpRepository.GetRrOtsRmCustomersByBatchId(gpiBatchId);
                    return new object[] { vendors, customers };
                }
                else if (GpHelper.GetGpiTypeID(GpManager.Steward) == gpBatch.GpiTypeID)
                {
                    var rrOtsRmTransactions = gpRepository.GetRrOtsRmTransactionByBatchId(gpiBatchId);
                    var rrOtsRmTransDistributions = gpRepository.GetRrOtsRmTransDistributionByBatchId(gpiBatchId);
                    var rrOtsRmCashReceipts = gpRepository.GetRrOtsRmCashReceiptByBatchId(gpiBatchId);
                    var rrOtsRmApplies = gpRepository.GetRrOtsRmApplyByBatchId(gpiBatchId);
                    return new object[] { rrOtsRmTransactions, rrOtsRmTransDistributions, rrOtsRmCashReceipts, rrOtsRmApplies };
                }
                else if (GpHelper.GetGpiTypeID(GpManager.Hauler) == gpBatch.GpiTypeID)
                {
                    var rrOtsPmTransactions = gpRepository.GetRrOtsPmTransactionByBatchId(gpiBatchId);
                    var rrOtsPmTransDistributions = gpRepository.GetRrOtsPmTransDistributionByBatchId(gpiBatchId);
                    return new object[] { rrOtsPmTransactions, rrOtsPmTransDistributions };
                }
                else if (GpHelper.GetGpiTypeID(GpManager.Processor) == gpBatch.GpiTypeID || GpHelper.GetGpiTypeID(GpManager.RPM) == gpBatch.GpiTypeID)
                {
                    var rrOtsPmTransactions = gpRepository.GetRrOtsPmTransactionByBatchId(gpiBatchId);
                    var rrOtsPmTransDistributions = gpRepository.GetRrOtsPmTransDistributionByBatchId(gpiBatchId);
                    return new object[] { rrOtsPmTransactions, rrOtsPmTransDistributions };
                }
                else if (GpHelper.GetGpiTypeID(GpManager.Collector) == gpBatch.GpiTypeID)
                {
                    var rrOtsPmTransactions = gpRepository.GetRrOtsPmTransactionByBatchId(gpiBatchId);
                    var rrOtsPmTransDistributions = gpRepository.GetRrOtsPmTransDistributionByBatchId(gpiBatchId);
                    return new object[] { rrOtsPmTransactions, rrOtsPmTransDistributions };
                }
                else
                {
                    throw new ArgumentException("Inavalid argument");
                }
            }
            else
            {
                throw new NullReferenceException("Invalid Batch Id");
            }
            throw new Exception();
        }
        public ResponseMsgBase PutOnHold(int gpiBatchEntryId)
        {
            var msg = new GpResponseMsg();
            var gpiBatchEntry = gpRepository.GetGpBatchEntry(gpiBatchEntryId);

            if (GpHelper.GetGpiStatus(gpiBatchEntry.GpiStatusID) != GpStatus.Extract)
            {
                msg.Errors.Add("You cannot hold an item that is posted, failed (import) or already on hold");
                return msg;
            }
            gpiBatchEntry.GpiStatusID = "H";
            gpiBatchEntry.LastUpdatedDate = DateTime.Now;
            gpiBatchEntry.LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName;
            gpRepository.UpdateGpBatchEntry(gpiBatchEntry);
            msg.ID = gpiBatchEntry.ID;
            return msg;

        }
        public ResponseMsgBase PutOffHold(int gpiBatchEntryId)
        {
            var msg = new GpResponseMsg();
            var gpiBatchEntry = gpRepository.GetGpBatchEntry(gpiBatchEntryId);

            if (GpHelper.GetGpiStatus(gpiBatchEntry.GpiStatusID) != GpStatus.Hold)
            {
                msg.Errors.Add("You can only take an item off hold if it is on hold");
                return msg;
            }
            gpiBatchEntry.GpiStatusID = "E";
            gpiBatchEntry.LastUpdatedDate = DateTime.Now;
            gpiBatchEntry.LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName;
            gpRepository.UpdateGpBatchEntry(gpiBatchEntry);
            msg.ID = gpiBatchEntry.ID;
            return msg;

        }
        private string GetTxnNumber(string registrationNumber)
        {
            if (registrationNumber.StartsWith("1"))
                return string.Format("11280-99-10-{0}", registrationNumber);
            if (registrationNumber.StartsWith("2"))
                return string.Format("21130-99-20-{0}", registrationNumber);
            if (registrationNumber.StartsWith("3"))
                return string.Format("21130-99-30-{0}", registrationNumber);
            if (registrationNumber.StartsWith("4"))
            {
                if (registrationNumber.StartsWith("41"))
                    return string.Format("21130-99-41-{0}", registrationNumber);
                else
                    return string.Format("21130-99-40-{0}", registrationNumber);
            }
            if (registrationNumber.StartsWith("5"))
                return string.Format("21130-99-50-{0}", registrationNumber);
            else
                return string.Empty;
        }
        public ResponseMsgBase PostBatch(int batchId, string type)
        {
            var msg = new GpResponseMsg();
            var gpBatch = gpRepository.GetGpBatch(batchId);
            if (gpBatch.GpiBatchEntries.Any(e => GpHelper.GetGpiStatus(e.GpiStatusID) == GpStatus.Hold))
            {
                msg.Errors.Add("You cannot post a batch with on hold entries");
                return msg;
            }

            var failedRecords = new Dictionary<int, string>();
            int[] batchEntryIDs = new int[] { };

            if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Participant), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                batchEntryIDs = this.CopyParticipantRecords(batchId, out failedRecords);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Steward), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                batchEntryIDs = this.CopyStewardRecords(batchId, out failedRecords);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Hauler), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                || string.Equals(GpHelper.GetGpiTypeID(GpManager.Processor), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                || string.Equals(GpHelper.GetGpiTypeID(GpManager.RPM), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase)
                || string.Equals(GpHelper.GetGpiTypeID(GpManager.Collector), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                batchEntryIDs = this.CopyPayableRecords(batchId, out failedRecords);
            }

            if (failedRecords.Count > 0)
            {
                //update batch entries as failed
                var failedEntries = failedRecords.Keys.Select(key => new GpiBatchEntry()
                {
                    ID = key,
                    LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                    LastUpdatedDate = DateTime.Now,
                    GpiStatusID = "X",
                    Message = failedRecords[key]
                }).ToList();
                gpRepository.UpdateGpBatchEntries(failedEntries);

                //update batch entries as extract
                gpRepository.UpdateGpBatchEntries(batchEntryIDs.ToList(), "E", SecurityContextHelper.CurrentUser.UserName, string.Empty);


                throw new Exception(string.Format("({0}) Errors", failedRecords.Count));
            }

            //if no failed update batch entries to posted

            //Changed it to Posted directly from P to P2
            gpRepository.UpdateGpBatchEntries(batchEntryIDs.ToList(), "P2", SecurityContextHelper.CurrentUser.UserName, string.Empty);

            //Update Batch
            //OTSTM2-1139 No talk back needed
            //gpBatch.GpiStatusId = "P"; //Queued

            //Changed it to Posted directly 
            gpBatch.GpiStatusId = "P2"; //Posted

            gpBatch.Message = string.Empty;
            gpBatch.LastUpdatedDate = DateTime.UtcNow;
            gpBatch.LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName;
            gpRepository.UpdateGpBatch(gpBatch);

            //OTSTM2-745
            AddGPActivityForPostBatch(gpBatch, type);

            return msg;
        }
        public void GpStatusCheck()
        {
            var postedQueued = GpHelper.GetGpiStatusString(GpStatus.PostedQueued);
            var postedQueuedBatches = gpRepository.GetAllGpBatches().Where(b => b.GpiStatusId == postedQueued).ToList();

            foreach (var batch in postedQueuedBatches)
            {
                this.CheckBatchErrors(batch);
            }
        }
        private void CheckBatchErrors(GpiBatch batch)
        {
            var statusId = GpHelper.GetGpiStatusString(GpStatus.PostedQueued);
            var gpBatchEntries = batch.GpiBatchEntries.Where(e => e.GpiBatchID == batch.ID && e.GpiStatusID == statusId).ToList();
            int? errorCount = null;
            int? success = null;
            foreach (var gpiBatchEntry in gpBatchEntries)
            {
                bool? error = this.CheckBatchEntryErrors(batch.GpiTypeID, gpiBatchEntry);
                if (error == null)
                    return;
                if (error ?? true)
                {
                    errorCount = (errorCount ?? 0) + 1;
                }
                else
                {
                    success = (success ?? 0) + 1;
                }
            }
            //GP has not imported the records yet
            if (errorCount == null && success == null)
                return;
            if (errorCount > 0)
            {
                batch.GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.FailedImport);
                batch.PrependMessage(string.Format("{0} entries failed to import.", errorCount), "Scheduled Task");
            }
            else
            {
                batch.GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.PostedImported);
                batch.Message = String.Empty;
                addNotificationForPostBatch(batch); //OTSTM2-400
            }
            batch.LastUpdatedBy = "System";
            batch.LastUpdatedDate = DateTime.UtcNow;
            gpRepository.UpdateGpBatch(batch);
        }

        //OTSTM2-400
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void addNotificationForPostBatch(GpiBatch gpBatch)
        {
            if (!(gpBatch.GpiTypeID == "S" || gpBatch.GpiTypeID == "R"))
            {
                var claimIdList = gpBatch.GpiBatchEntries.Select(g => g.ClaimId).ToList();
                foreach (var claimId in claimIdList)
                {
                    var currentClaim = this.claimsRepository.FindClaimByClaimId(claimId ?? 0);
                    var currentPeriod = currentClaim.ClaimPeriod;
                    var participantId = currentClaim.ParticipantId;
                    var nextClaim = this.claimsRepository.GetNextClaimByVendorId(participantId, currentPeriod);
                    if (nextClaim != null)
                    {
                        if (nextClaim.Status == "Under Review")
                        {
                            var notification = new Notification();
                            notification.Assigner = string.Empty;
                            notification.AssignerName = string.Empty;
                            notification.AssignerInitial = "TM";
                            notification.Assignee = nextClaim.AssignedToUser.UserName;
                            notification.AssigneeName = string.Format("{0} {1}", nextClaim.AssignedToUser.FirstName, nextClaim.AssignedToUser.LastName);
                            notification.CreatedTime = DateTime.Now;
                            notification.NotificationType = TreadMarksConstants.ClaimNotification;
                            notification.Status = "Unread";
                            switch (nextClaim.ClaimType)
                            {
                                case (int)ClaimType.Collector:
                                    notification.NotificationArea = TreadMarksConstants.Collector;
                                    break;
                                case (int)ClaimType.Hauler:
                                    notification.NotificationArea = TreadMarksConstants.Hauler;
                                    break;
                                case (int)ClaimType.Processor:
                                    notification.NotificationArea = TreadMarksConstants.Processor;
                                    break;
                                case (int)ClaimType.RPM:
                                    notification.NotificationArea = TreadMarksConstants.RPM;
                                    break;
                            }
                            notification.ObjectId = nextClaim.ID;
                            notification.Message = string.Format("{0} - {1} Claim is posted. You may proceed with <strong>{2} - {3} Claim review</strong>.", nextClaim.Participant.Number, currentClaim.ClaimPeriod.ShortName, nextClaim.Participant.Number, nextClaim.ClaimPeriod.ShortName);

                            var isAdded = this.messageRepository.AddBatchPostNotification(notification); //OTSTM2-635
                            //Publish notification event
                            if (isAdded)
                            {
                                var notificationPayload = new NotificationPayload
                                {
                                    NotificationId = notification.ID,
                                    Receiver = notification.Assignee
                                };
                                eventAggregator.GetEvent<NotificationEvent>().Publish(notificationPayload);
                            }
                        }
                    }
                }
            }
        }

        //OTSTM2-657
        private void AddNotificationForRemoveBatch(int claimId)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            var currentPeriod = claim.ClaimPeriod;
            var participantId = claim.ParticipantId;
            var nextClaim = this.claimsRepository.GetNextClaimByVendorId(participantId, currentPeriod);

            var assigner = SecurityContextHelper.CurrentUser;
            var staffUser = string.Format("{0} {1}", assigner.FirstName, assigner.LastName);

            var notification = new Notification();
            notification.Assigner = string.Empty;
            notification.AssignerName = string.Empty;
            notification.AssignerInitial = "TM";
            notification.Assignee = "test@test.com";
            string supervisorFullName = "System Admin";

            if (claim.ClaimProcess != null)//auto approve claims doesn't have [CliamProcess]
            {
                var supervisor = claimsRepository.FindUserById(claim.ClaimProcess.SupervisorReview.Value);
                supervisorFullName = string.Format("{0} {1}", supervisor.FirstName, supervisor.LastName);
                notification.Assignee = supervisor.UserName;
            }
            else
            {
                if ((ClaimType)claim.ClaimType == ClaimType.Collector)
                {
                    return;//no one need a notification.
                }
                else//corruption data found, continue send notification to [System Admin] (only collector claim has auto approve which a approved claim has no "ClaimProcess" data);
                {
                    notification.Message = "<strong>something wrong</strong>:" + string.Format("<strong>{0}</strong> removed {1} - {2} Claim from Financial Integration. You may proceed with <strong>{3} - {4} Claim review </strong>.", staffUser,
                    claim.Participant.Number,
                    claim.ClaimPeriod.ShortName,
                    claim.Participant.Number,
                    claim.ClaimPeriod.ShortName);
                }
            }

            notification.AssigneeName = supervisorFullName;

            notification.CreatedTime = DateTime.Now;
            notification.NotificationType = TreadMarksConstants.ClaimNotification;
            notification.Status = "Unread";
            switch (claim.ClaimType)
            {
                case (int)ClaimType.Collector:
                    notification.NotificationArea = TreadMarksConstants.Collector;
                    break;
                case (int)ClaimType.Hauler:
                    notification.NotificationArea = TreadMarksConstants.Hauler;
                    break;
                case (int)ClaimType.Processor:
                    notification.NotificationArea = TreadMarksConstants.Processor;
                    break;
                case (int)ClaimType.RPM:
                    notification.NotificationArea = TreadMarksConstants.RPM;
                    break;
            }
            notification.ObjectId = claim.ID;

            if (string.IsNullOrEmpty(notification.Message))
            {
                notification.Message = string.Format("<strong>{0}</strong> removed {1} - {2} Claim from Financial Integration. You may proceed with <strong>{3} - {4} Claim review </strong>.", staffUser,
                    claim.Participant.Number,
                    claim.ClaimPeriod.ShortName,
                    claim.Participant.Number,
                    claim.ClaimPeriod.ShortName); //nextClaim != null ? nextClaim.ClaimPeriod.ShortName : string.Empty);
            }

            this.messageRepository.AddNotification(notification); //OTSTM2-657

            //Publish notification event
            var notificationPayload = new NotificationPayload
            {
                NotificationId = notification.ID,
                Receiver = notification.Assignee
            };
            eventAggregator.GetEvent<NotificationEvent>().Publish(notificationPayload);
        }

        //OTSTM2-859
        private void AddRemittanceActivityForRemoveBatch(TSFClaim tsfClaim, GpiBatchEntry entry)
        {
            var activity = new Activity();
            activity.Initiator = SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = string.Empty;
            activity.AssigneeName = string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.TsfClaimActivity;
            activity.ActivityArea = TreadMarksConstants.StewardAccount;
            activity.ObjectId = tsfClaim.ID;

            var assigner = SecurityContextHelper.CurrentUser;
            var staffUser = string.Format("{0} {1}", assigner.FirstName, assigner.LastName);

            var tsfClerk = tsfRemittanceRepository.GetByUserId(tsfClaim.AssignToUser);
            var tsfClerkFullName = string.Format("{0} {1}", tsfClerk.FirstName, tsfClerk.LastName);

            activity.Message = string.Format("{0} <strong>removed</strong> this Remittance from {1} Batch #{2} and the Remittance has been " +
                "<strong>assigned</strong> to <strong>{3}</strong> (TSF Clerk).", staffUser, activity.ActivityArea, entry.GpiBatchID, tsfClerkFullName);

            this.messageRepository.AddActivity(activity);

            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(tsfClaim.ID);
        }

        //OTSTM2-751
        private void AddClaimActivityForRemoveBatch(int claimId, GpiBatchEntry entry)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var activity = new Activity();
            var assigner = SecurityContextHelper.CurrentUser;
            var staffUser = string.Format("{0} {1}", assigner.FirstName, assigner.LastName);
            string supervisorFullName = "System";

            if (claim.ClaimProcess != null)//auto approve claims doesn't have [CliamProcess]
            {
                var supervisor = claimsRepository.FindUserById(claim.ClaimProcess.SupervisorReview.Value);
                supervisorFullName = string.Format("{0} {1}", supervisor.FirstName, supervisor.LastName);
            }
            else
            {
                if ((ClaimType)claim.ClaimType == ClaimType.Collector)//auto approved collector claim
                {
                    activity.Message = string.Format("{0} <strong>removed</strong> this claim from {1} and the claim is ready to be assigned", staffUser, TreadMarksConstants.Collector + " Batch #" + entry.GpiBatchID.ToString());
                }
            }

            var currentPeriod = claim.ClaimPeriod;
            var participantId = claim.ParticipantId;
            var nextClaim = this.claimsRepository.GetNextClaimByVendorId(participantId, currentPeriod);

            activity.Initiator = SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = string.Empty;
            activity.AssigneeName = string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.ClaimActivity;
            switch (claim.ClaimType)
            {
                case 2:
                    activity.ActivityArea = TreadMarksConstants.Collector;
                    break;
                case 3:
                    activity.ActivityArea = TreadMarksConstants.Hauler;
                    break;
                case 4:
                    activity.ActivityArea = TreadMarksConstants.Processor;
                    break;
                case 5:
                    activity.ActivityArea = TreadMarksConstants.RPM;
                    break;
            }
            activity.ObjectId = claim.ID;

            //OTSTM2-751
            if (string.IsNullOrEmpty(activity.Message))
            {
                activity.Message = string.Format("{0} <strong>removed</strong> this claim from {1} and the claim has been " +
                    "<strong>assigned</strong> to <strong>{2}</strong> (Supervisor).", staffUser, activity.ActivityArea + " Batch #" + entry.GpiBatchID.ToString(), supervisorFullName);
            }

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(claim.ID);
        }

        //OTSTM2-745
        private void AddGPActivityForRemoveBatch(GpiBatchEntry entry)
        {
            var activity = new Activity();
            var assignerUser = SecurityContextHelper.CurrentUser;
            UserReference assigneeUser = null;

            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName) : string.Empty;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName) : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.GPClaimActivity;

            if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Hauler), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPHauler;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from Hauler Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Collector), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPCollector;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from Collector Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Steward), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPSteward;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from Steward Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Processor), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPProcessor;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from Processor Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.RPM), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPRPM;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from RPM Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Participant), entry.GpiBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                //participant manager
                activity.ActivityArea = TreadMarksConstants.GPParticipant;
                activity.Message = string.Format("{0} <strong>removed</strong> transaction <strong># {1}</strong> from Participant Batch <strong># {2}</strong>", activity.InitiatorName, entry.GpiTxnNumber, entry.GpiBatchID);
            }

            activity.ObjectId = entry.GpiBatchID;

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(activity.ObjectId);

        }
        //OTSTM2-745
        private void AddGPActivityForPostBatch(GpiBatch gpBatch, string postType)
        {
            var activity = new Activity();
            var assignerUser = SecurityContextHelper.CurrentUser;
            UserReference assigneeUser = null;

            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName) : string.Empty;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName) : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.GPClaimActivity;

            if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Hauler), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPHauler;
                activity.Message = string.Format("{0} <strong>{1}</strong> Hauler Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Collector), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPCollector;
                activity.Message = string.Format("{0} <strong>{1}</strong> Collector Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Steward), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPSteward;
                activity.Message = string.Format("{0} <strong>{1}</strong> Steward Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Processor), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPProcessor;
                activity.Message = string.Format("{0} <strong>{1}</strong> Processor Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.RPM), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPRPM;
                activity.Message = string.Format("{0} <strong>{1}</strong> RPM Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Participant), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                //participant manager
                activity.ActivityArea = TreadMarksConstants.GPParticipant;
                activity.Message = string.Format("{0} <strong>{1}</strong> Participant Batch <strong># {2}</strong>", activity.InitiatorName, postType, gpBatch.ID);
            }

            activity.ObjectId = gpBatch.ID;

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(activity.ObjectId);
        }
        //OTSTM2-745
        public void AddGPActivityForCreateBatch(int batchId)
        {
            var gpBatch = gpRepository.GetGpBatch(batchId);

            var activity = new Activity();
            var assignerUser = SecurityContextHelper.CurrentUser;
            UserReference assigneeUser = null;

            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName) : string.Empty;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName) : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.GPClaimActivity;
            activity.ObjectId = batchId;

            if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Hauler), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPHauler;
                activity.Message = string.Format("{0} <strong>created</strong> Hauler Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Collector), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPCollector;
                activity.Message = string.Format("{0} <strong>created</strong> Collector Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Steward), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPSteward;
                activity.Message = string.Format("{0} <strong>created</strong> Steward Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Processor), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPProcessor;
                activity.Message = string.Format("{0} <strong>created</strong> Processor Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.RPM), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                activity.ActivityArea = TreadMarksConstants.GPRPM;
                activity.Message = string.Format("{0} <strong>created</strong> RPM Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }
            else if (string.Equals(GpHelper.GetGpiTypeID(GpManager.Participant), gpBatch.GpiTypeID, StringComparison.OrdinalIgnoreCase))
            {
                //participant manager
                activity.ActivityArea = TreadMarksConstants.GPParticipant;
                activity.Message = string.Format("{0} <strong>created</strong> Participant Batch <strong>#{1}</strong>", activity.InitiatorName, activity.ObjectId);
            }

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(activity.ObjectId);
        }

        private bool? CheckBatchEntryErrors(string batchType, GpiBatchEntry gpiBatchEntry)
        {
            bool? result = null;
            var errorMessages = new List<string>();
            switch (batchType)
            {
                case "R":
                    //OTSTM2-1139 No talk back needed
                    //result = this.CheckRegistrantErrors(gpiBatchEntry, out errorMessages);
                    break;
                case "S":
                    //OTSTM2-1139 No talk back needed
                    //result = this.CheckStewardErrors(gpiBatchEntry, out errorMessages);
                    break;
                case "C":
                case "H":
                case "P":
                case "P2":
                case "M":
                    //OTSTM2-1139 No talk back needed
                    //result = this.CheckPayableErrors(gpiBatchEntry, out errorMessages);
                    break;
            }
            /// GP has not imported the records yet
            if (result == null)
            {
                return null;
            }

            /// Import success
            if (result == false)
            {
                gpiBatchEntry.IntStatus = 1;
                gpiBatchEntry.GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.PostedImported);
                gpiBatchEntry.Message = string.Empty;

                return false;
            }
            /// Import failed
            else
            {
                gpiBatchEntry.IntStatus = 2;
                gpiBatchEntry.GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.FailedImport);
                gpiBatchEntry.Message = string.Join("\r\n", errorMessages.Distinct().ToArray());
                return true;
            }
        }

        //OTSTM2-1139 No talk back needed
        //private bool? CheckPayableErrors(GpiBatchEntry entry, out List<string> errorMessages)
        //{
        //    errorMessages = new List<string>();
        //    var transaction = gpRepository.GetAllRrOtsPmTransactions().SingleOrDefault(i => i.GpiBatchEntryId == entry.ID && i.DOCTYPE == 1);
        //    //OTSL-322 Thus no payable exists so it must only be credits existing in claim
        //    if (transaction == null)
        //    {
        //        return false;
        //    }

        //    var gpTransaction = gpStagingRepository.GetAllRrOtsPmTransactions().SingleOrDefault(i => i.VCHNUMWK == transaction.VCHNUMWK &&
        //        i.DOCTYPE == transaction.DOCTYPE &&
        //        i.INTERID == transaction.INTERID &&
        //        i.INTSTATUS != null &&
        //        i.INTSTATUS != 0);
        //    if (gpTransaction == null)
        //    {
        //        return null;
        //    }
        //    transaction.INTSTATUS = gpTransaction.INTSTATUS;
        //    transaction.INTDATE = gpTransaction.INTDATE;
        //    bool error = false;
        //    if (gpTransaction.INTSTATUS == 2)
        //    {
        //        try
        //        {
        //            var errorTransaction = gpStagingRepository.GetAllRrOtsVwPayablesInvoiceErrors().Single(i => i.VCHNUMWK == gpTransaction.VCHNUMWK &&
        //                                                                                                    i.DOCTYPE == gpTransaction.DOCTYPE &&
        //                                                                                                    i.SORTBY == 1);
        //            errorMessages.Add(errorTransaction.ErrorDesc);
        //        }
        //        catch
        //        {
        //            errorMessages.Add("Unknown error");
        //        }

        //        error = true;
        //    }
        //    return error || (this.CheckPayableDistributionErrors(entry, transaction, errorMessages) ?? false);
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckPayableDistributionErrors(GpiBatchEntry entry, RrOtsPmTransaction transaction, List<string> errorMessages)
        //{
        //    int? error = null;
        //    errorMessages = new List<string>();
        //    var rrOtsPmTransDistributions = gpRepository.GetRrOtsPmTransDistributionByBatchId(entry.GpiBatchID).Where(i => i.VCHNUMWK == transaction.VCHNUMWK &&
        //            i.DOCTYPE == transaction.DOCTYPE &&
        //            i.INTERID == transaction.INTERID).ToList();


        //    foreach (var transDistribution in rrOtsPmTransDistributions)
        //    {
        //        var gpDistribution = gpStagingRepository.GetAllRrOtsPmTransDistributions().SingleOrDefault(i => i.VCHNUMWK == transDistribution.VCHNUMWK &&
        //            i.DOCTYPE == transDistribution.DOCTYPE &&
        //            i.DSTSQNUM == transDistribution.DSTSQNUM &&
        //            i.INTERID == transDistribution.INTERID &&
        //            i.INTSTATUS != null &&
        //            i.INTSTATUS != 0);

        //        if (gpDistribution == null)
        //        {
        //            break;
        //        }

        //        transDistribution.INTSTATUS = gpDistribution.INTSTATUS;
        //        transDistribution.INTDATE = gpDistribution.INTDATE;

        //        if (gpDistribution.INTSTATUS == 2)
        //        {
        //            try
        //            {
        //                var errorTransaction = gpStagingRepository.GetAllRrOtsVwPayablesInvoiceErrors().Single(i => i.VCHNUMWK == gpDistribution.VCHNUMWK &&
        //                    i.DOCTYPE == gpDistribution.DOCTYPE &&
        //                    i.SORTBY == 2 &&
        //                    i.ACTNUMST == gpDistribution.ACTNUMST);
        //                errorMessages.Add(errorTransaction.ErrorDesc);
        //            }
        //            catch
        //            {
        //                errorMessages.Add("Unknown error");
        //            }
        //            error++;
        //        }
        //    }
        //    if (error > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckRegistrantErrors(GpiBatchEntry entry, out List<string> errorMessages)
        //{
        //    errorMessages = new List<string>();
        //    string errorMessage;
        //    bool? vendorErrors = this.CheckVendorErrors(entry, out errorMessage);
        //    bool? customerErrors = this.CheckCustomerErrors(entry, out errorMessage);

        //    if (vendorErrors == null &&
        //        customerErrors == null)
        //    {
        //        return null;
        //    }

        //    if (errorMessage != null)
        //    {
        //        errorMessages.Add(errorMessage);
        //    }
        //    return (vendorErrors ?? false) || (customerErrors ?? false);
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckVendorErrors(GpiBatchEntry entry, out string errorMessage)
        //{
        //    errorMessage = null;
        //    var myVendor = gpRepository.GetRrOtsPmVendorsByBatchId(entry.GpiBatchID).SingleOrDefault(i => i.gpibatchentry_id == entry.ID);

        //    /// Entry is not a vendor
        //    if (myVendor == null)
        //    {
        //        return null;
        //    }
        //    var gpVendor = gpStagingRepository.GetAllRrOtsPmVendors().SingleOrDefault(i => i.VENDORID == myVendor.VENDORID &&
        //                                                                        i.VADDCDPR == myVendor.VADDCDPR &&
        //                                                                        i.INTERID == myVendor.INTERID &&
        //                                                                        i.INTSTATUS != null &&
        //                                                                        i.INTSTATUS != 0);
        //    /// Record has not been integrated yet.
        //    if (gpVendor == null)
        //    {
        //        return null;
        //    }

        //    myVendor.INTSTATUS = gpVendor.INTSTATUS;
        //    myVendor.INTDATE = gpVendor.INTDATE;

        //    /// INTSTATUS == 2 --> integration error
        //    if (gpVendor.INTSTATUS == 2)
        //    {
        //        try
        //        {
        //            RR_OTS_VW_Vendors_Error errorVendor = gpStagingRepository.GetAllRrOtsVwVendorsError()
        //                    .SingleOrDefault(i => i.VENDORID == gpVendor.VENDORID &&
        //                                          i.VADDCDPR == gpVendor.VADDCDPR &&
        //                                          i.INTERID == gpVendor.INTERID);
        //            errorMessage = errorVendor.ErrorDesc;
        //        }
        //        catch
        //        {
        //            errorMessage = "Unknown error";
        //        }

        //        return true;
        //    }

        //    return false;
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckCustomerErrors(GpiBatchEntry entry, out string errorMessage)
        //{
        //    errorMessage = null;
        //    var myCustomer = gpRepository.GetRrOtsRmCustomersByBatchId(entry.GpiBatchID).SingleOrDefault(i => i.gpibatchentry_id == entry.ID);
        //    /// Entry is not a customer entry
        //    if (myCustomer == null)
        //    {
        //        return null;
        //    }
        //    var gpCustomer = gpStagingRepository.GetAllRrOtsRmCustomers().SingleOrDefault(i => i.CUSTNMBR == myCustomer.CUSTNMBR &&
        //        i.ADRSCODE == myCustomer.ADRSCODE &&
        //        i.INTERID == myCustomer.INTERID &&
        //        i.INTSTATUS != null &&
        //        i.INTSTATUS != 0);

        //    if (gpCustomer == null)
        //    {
        //        return null;
        //    }
        //    myCustomer.INTSTATUS = gpCustomer.INTSTATUS;
        //    myCustomer.INTDATE = gpCustomer.INTDATE;

        //    /// gpCustomer.INTSTATUS == 2 --> Integration error
        //    if (gpCustomer.INTSTATUS == 2)
        //    {
        //        try
        //        {
        //            var errorCustomer = gpStagingRepository.GetAllRrOtsVwCustomersError().Single(i => i.CUSTNMBR == gpCustomer.CUSTNMBR &&
        //                                                                                            i.ADRSCODE == gpCustomer.ADRSCODE &&
        //                                                                                            i.INTERID == gpCustomer.INTERID);
        //            errorMessage = errorCustomer.ErrorDesc;
        //        }
        //        catch
        //        {
        //            errorMessage = "Unknown error";
        //        }
        //        return true;
        //    }
        //    return false;
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckStewardErrors(GpiBatchEntry entry, out List<string> errorMessages)
        //{
        //    errorMessages = new List<string>();

        //    var transactions = gpRepository.GetRrOtsRmTransactionByBatchId(entry.GpiBatchID).Where(i => i.gpibatchentry_id == entry.ID);
        //    var error = false;
        //    var receivableDistributionErrors = false;
        //    foreach (var transaction in transactions)
        //    {
        //        var gpTransaction = gpStagingRepository.GetAllRrOtsRmTransactions().SingleOrDefault(i => i.RMDTYPAL == transaction.RMDTYPAL && i.DOCNUMBR == transaction.DOCNUMBR && i.INTERID == transaction.INTERID && (i.INTSTATUS != null && i.INTSTATUS != 0));

        //        if (gpTransaction == null)
        //        {
        //            return null;
        //        }
        //        transaction.INTSTATUS = gpTransaction.INTSTATUS;
        //        transaction.INTDATE = gpTransaction.INTDATE;

        //        ///// gpTransaction.INTSTATUS == 2 --> integration error
        //        if (gpTransaction.INTSTATUS == 2)
        //        {
        //            try
        //            {
        //                RR_OTS_VW_Receivables_Invoice_Error errorTransaction = gpStagingRepository.GetAllRrOtsVwReceivablesInvoiceErrors().Single(i => i.RMDTYPAL == gpTransaction.RMDTYPAL &&
        //                    i.DOCNUMBR == gpTransaction.DOCNUMBR &&
        //                    i.SORTBY == 1);
        //                if (!errorMessages.Contains(errorTransaction.ErrorDesc))
        //                {
        //                    errorMessages.Add(errorTransaction.ErrorDesc);
        //                }
        //            }
        //            catch
        //            {
        //                errorMessages.Add("Unknown error");
        //            }

        //            error = true;
        //        }
        //        receivableDistributionErrors = receivableDistributionErrors || (this.CheckReceivableDistributionErrors(entry, transaction, errorMessages) ?? false);
        //    }
        //    return error | receivableDistributionErrors | (this.CheckCashReceiptErrors(entry, errorMessages) ?? false);
        //}


        private int[] CopyParticipantRecords(int batchId, out Dictionary<int, string> failedRecords)
        {
            var batchEntryIDs = new List<int>();
            failedRecords = new Dictionary<int, string>();
            var rrOtsRmCustomers = gpRepository.GetAllRrOtsRmCustomers().Where(i => i.USERDEF1 == batchId.ToString()).ToList();
            // copy customer records
            foreach (var rrOtsRmCustomer in rrOtsRmCustomers)
            {
                //OTSTM2-1139 No talk back needed
                //var entity = new RR_OTS_RM_CUSTOMERS();
                //try
                //{
                //    entity = ModelAdapter.ToRrOtsRmCustomers(rrOtsRmCustomer);
                //    entity.INTSTATUS = 0;
                //    entity.INTDATE = null;
                //    entity.ERRORCODE = null;
                //    try
                //    {
                //        //insert or update record
                //        var existingCustomer = gpStagingRepository.GetAllRrOtsRmCustomers().SingleOrDefault(i => i.CUSTNMBR == entity.CUSTNMBR &&
                //                                                                                              i.ADRSCODE == entity.ADRSCODE &&
                //                                                                                              i.INTERID == entity.INTERID);
                //        if (existingCustomer != null)
                //        {
                //            existingCustomer = entity;
                //            //AutoMapper.Mapper.Map<RR_OTS_RM_CUSTOMERS>(entity);
                //            gpStagingRepository.UpdateRrOtsRmCustomer(existingCustomer);
                //        }
                //        else
                //        {
                //            gpStagingRepository.AddGpRrOtsRmCustomer(entity);
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        gpStagingRepository.DeleteRrOtsRmCustomer(entity);
                //        throw ex;
                //    }

                batchEntryIDs.Add((int)rrOtsRmCustomer.gpibatchentry_id);

                //OTSTM2-1139 No talk back needed
                //}
                //catch (Exception ex)
                //{
                //    failedRecords.Add((int)rrOtsRmCustomer.gpibatchentry_id, ex.Message);
                //}
            }
            // copy vendor records
            var rrOtsPmVendors = gpRepository.GetAllRrOtsPmVendors().Where(i => i.USERDEF1 == batchId.ToString()).ToList();
            foreach (var rrOtsPmVendor in rrOtsPmVendors)
            {
                //OTSTM2-1139 No talk back needed
                //var entity = new RR_OTS_PM_VENDORS();
                //try
                //{
                //    entity = ModelAdapter.ToRrOtsPmVendors(rrOtsPmVendor);
                //    entity.INTSTATUS = 0;
                //    entity.INTDATE = null;
                //    entity.ERRORCODE = null;
                //    try
                //    {
                //        var existingVendor = gpStagingRepository.GetAllRrOtsPmVendors().SingleOrDefault(i => i.VENDORID == entity.VENDORID &&
                //                                                                                        i.VADDCDPR == entity.VADDCDPR &&
                //                                                                                        i.INTERID == entity.INTERID);
                //        if (existingVendor != null)
                //        {
                //            existingVendor = entity;
                //            //AutoMapper.Mapper.Map<RR_OTS_PM_VENDORS>(entity);
                //            gpStagingRepository.UpdateRrOtsPmVendor(existingVendor);
                //        }
                //        else
                //        {
                //            gpStagingRepository.AddGpRrOtsPmVendor(entity);
                //        }

                //    }
                //    catch (Exception ex)
                //    {
                //        gpStagingRepository.DeleteRrOtsPmVendor(entity);
                //        throw ex;
                //    }
                if (!batchEntryIDs.Contains((int)rrOtsPmVendor.gpibatchentry_id))
                    batchEntryIDs.Add((int)rrOtsPmVendor.gpibatchentry_id);
                //}
                //catch (Exception ex)
                //{
                //    failedRecords.Add((int)rrOtsPmVendor.gpibatchentry_id, ex.Message);
                //}
            }
            return batchEntryIDs.ToArray();
        }
        private int[] CopyStewardRecords(int batchId, out Dictionary<int, string> failedRecords)
        {
            var batchEntryIDs = new List<int>();
            failedRecords = new Dictionary<int, string>();
            var rrOtsRmCustomers = gpRepository.GetAllRrOtsRmCustomers().Where(i => i.USERDEF1 == batchId.ToString()).ToList();

            var rrOtsRmTransactions = gpRepository.GetRrOtsRmTransactionByBatchId(batchId);

            foreach (var rrOtsRmTransaction in rrOtsRmTransactions)
            {
                //OTSTM2-1139 No talk back needed
                //STEP 1 DELETE TRANSACTION FROM STAGING
                //OTSTM2-1139 No talk back needed
                //var deleteTransactions = gpStagingRepository.GetAllRrOtsRmTransactions()
                //                        .Where(
                //                            t => t.RMDTYPAL == rrOtsRmTransaction.RMDTYPAL &&
                //                                t.DOCNUMBR.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                                t.INTERID.Trim() == rrOtsRmTransaction.INTERID.Trim())
                //                        .ToList();
                //gpStagingRepository.DeleteRrOtsRmTransactions(deleteTransactions);

                //STEP 2 INSERT TRANSACTION
                //var entity = new RR_OTS_RM_TRANSACTIONS();
                //try
                //{
                //    entity = ModelAdapter.ToRrOtsRmTransactions(rrOtsRmTransaction);
                //    entity.INTSTATUS = 0;
                //    entity.INTDATE = null;
                //    entity.ERRORCODE = null;
                //    gpStagingRepository.AddGpRrOtsRmTransaction(entity);

                //    //DELETE TRANS DISTRIBUTION FROM STAGING
                //    var deleteTransDistributions = gpStagingRepository.GetAllRrOtsRmTransDistributions()
                //            .Where(
                //                i =>
                //                    i.RMDTYPAL == rrOtsRmTransaction.RMDTYPAL &&
                //                    i.DOCNUMBR.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                    i.INTERID.Trim() == rrOtsRmTransaction.INTERID.Trim())
                //                    .ToList();
                //    gpStagingRepository.DeleteRrOtsRmTransDistributions(deleteTransDistributions);

                //    //INSERT TRANS DISTRIBUTIONS
                //    var insertTransDistributions = gpRepository.GetRrOtsRmTransDistributionByBatchId(batchId)
                //            .Where(
                //                i =>
                //                    i.DOCNUMBR.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                    i.RMDTYPAL == rrOtsRmTransaction.RMDTYPAL &&
                //                    i.INTERID == rrOtsRmTransaction.INTERID).ToList().Select(ModelAdapter.ToRrOtsRmTransDistributions).ToList();
                //    gpStagingRepository.AddGpRrOtsRmTransDistributionsRange(insertTransDistributions);

                //    //DELETE CASH RECEIPT FROM STAGING
                //    var deleteCashReceipts = gpStagingRepository.GetAllRrOtsRmCashReceipts()
                //            .Where(
                //                i =>
                //                    i.DOCNUMBR.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                    i.INTERID.Trim() == rrOtsRmTransaction.INTERID.Trim())
                //            .ToList();
                //    gpStagingRepository.DeleteRrOtsRmCashReceipts(deleteCashReceipts);

                //    //INSERT CASH RECEIPTS
                //    var insertCashReceipts =
                //        gpRepository.GetRrOtsRmCashReceiptByBatchId(batchId)
                //            .Where(
                //                i => i.DOCNUMBR.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                     i.INTERID == rrOtsRmTransaction.INTERID)
                //            .ToList().Select(ModelAdapter.ToRrOtsRmCashReceipt).ToList();
                //    gpStagingRepository.AddGpRrOtsRmCashReceipts(insertCashReceipts);

                //    //DELETE APPLY FROM STAGING
                //    var deleteApplies =
                //        gpStagingRepository.GetAllRrOtsRmApplies()
                //            .Where(
                //                i =>
                //                    i.APTODCNM.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                    i.INTERID.Trim() == rrOtsRmTransaction.INTERID.Trim())
                //            .ToList();
                //    gpStagingRepository.DeleteRrOtsRmApplies(deleteApplies);

                //    //INSERT APPLY
                //    var insertApplies =
                //        gpRepository.GetRrOtsRmApplyByBatchId(batchId)
                //            .Where(
                //                i =>
                //                    i.APTODCNM.Trim() == rrOtsRmTransaction.DOCNUMBR.Trim() &&
                //                    i.INTERID.Trim() == rrOtsRmTransaction.INTERID.Trim())
                //            .ToList()
                //            .Select(ModelAdapter.ToRrOtsRmApply)
                //            .ToList();
                //    gpStagingRepository.AddGpRrOtsRmApplies(insertApplies);

                if (!batchEntryIDs.Contains((int)rrOtsRmTransaction.gpibatchentry_id))
                    batchEntryIDs.Add((int)rrOtsRmTransaction.gpibatchentry_id);

                //}
                //catch (Exception ex)
                //{
                //    failedRecords.Add((int)rrOtsRmTransaction.gpibatchentry_id, ex.Message);
                //}


            }
            return batchEntryIDs.ToArray();
        }

        //OTSTM2-1139 No talk back needed
        //private bool? CheckReceivableDistributionErrors(GpiBatchEntry entry, RrOtsRmTransaction myTransaction, List<string> errorMessages)
        //{
        //    int error = 0;
        //    var distributions = gpRepository.GetRrOtsRmTransDistributionByBatchId(entry.GpiBatchID)
        //            .Where(i => i.RMDTYPAL == myTransaction.RMDTYPAL &&
        //                        i.DOCNUMBR.Trim() == myTransaction.DOCNUMBR.Trim() &&
        //                        i.INTERID.Trim() == myTransaction.INTERID.Trim()).ToList();

        //    foreach (var distribution in distributions)
        //    {
        //        var gpDistribution = gpStagingRepository.GetAllRrOtsRmTransDistributions().SingleOrDefault(i => i.RMDTYPAL == distribution.RMDTYPAL &&
        //            i.DOCNUMBR.Trim() == distribution.DOCNUMBR.Trim() &&
        //            i.SEQNUMBR == distribution.SEQNUMBR &&
        //            i.INTERID == distribution.INTERID &&
        //            i.INTSTATUS != null &&
        //            i.INTSTATUS != 0);

        //        /// The record was not imported yet so ignore
        //        if (gpDistribution == null)
        //        {
        //            break;
        //        }
        //        distribution.INTSTATUS = gpDistribution.INTSTATUS;
        //        distribution.INTDATE = gpDistribution.INTDATE;

        //        /// Error
        //        if (gpDistribution.INTSTATUS == 2)
        //        {
        //            try
        //            {
        //                RR_OTS_VW_Receivables_Invoice_Error errorTransaction =
        //                    gpStagingRepository.GetAllRrOtsVwReceivablesInvoiceErrors().Single(i => i.RMDTYPAL == gpDistribution.RMDTYPAL &&
        //                    i.DOCNUMBR.Trim() == gpDistribution.DOCNUMBR.Trim() &&
        //                    i.SORTBY == 2 &&
        //                    i.ACTNUMST.Trim() == gpDistribution.ACTNUMST.Trim());
        //                errorMessages.Add(errorTransaction.ErrorDesc);
        //            }
        //            catch
        //            {
        //                errorMessages.Add("Unknown error");
        //            }

        //            error++;
        //        }
        //    }
        //    if (error > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckCashReceiptErrors(GpiBatchEntry entry, List<string> errorMessages)
        //{
        //    var cashReceipt = gpRepository.GetRrOtsRmCashReceiptByBatchId(entry.GpiBatchID).SingleOrDefault(i => i.gpibatchentry_id == entry.ID);
        //    ///// No cash receipt record
        //    if (cashReceipt == null)
        //    {
        //        return null;
        //    }
        //    var gpCashReceipt = gpStagingRepository.GetAllRrOtsRmCashReceipts().SingleOrDefault(i => i.DOCNUMBR == cashReceipt.DOCNUMBR &&
        //        i.RMDTYPAL == cashReceipt.RMDTYPAL &&
        //        i.INTERID == cashReceipt.INTERID &&
        //        i.INTSTATUS != null &&
        //        i.INTSTATUS != 0);
        //    /// Record was not integrated yet.
        //    if (gpCashReceipt == null)
        //    {
        //        return null;
        //    }

        //    cashReceipt.INTSTATUS = gpCashReceipt.INTSTATUS;
        //    cashReceipt.INTDATE = gpCashReceipt.INTDATE;

        //    bool error = false;
        //    if (gpCashReceipt.INTSTATUS == 2)
        //    {
        //        try
        //        {
        //            var errorCashReceipt = gpStagingRepository.GetAllRrOtsVwCashReceiptErrors().Single(i => i.DOCNUMBR == gpCashReceipt.DOCNUMBR &&
        //                i.RMDTYPAL == gpCashReceipt.RMDTYPAL &&
        //                i.INTERID == gpCashReceipt.INTERID);

        //            errorMessages.Add(errorCashReceipt.ErrorDesc);
        //        }
        //        catch
        //        {
        //            errorMessages.Add("Unknown error");
        //        }
        //        error = true;
        //    }
        //    return error | (this.CheckApplyErrors(entry, cashReceipt, errorMessages) ?? false);
        //}

        //OTSTM2-1139 No talk back needed
        //private bool? CheckApplyErrors(GpiBatchEntry entry, RrOtsRmCashReceipt myCashReceipt, List<string> errorMessages)
        //{
        //    var apply = gpRepository.GetRrOtsRmApplyByBatchId(entry.GpiBatchID).FirstOrDefault(i => i.APFRDCNM == myCashReceipt.DOCNUMBR &&
        //        i.APFRDCTY == myCashReceipt.RMDTYPAL &&
        //        i.INTERID == myCashReceipt.INTERID);

        //    if (apply != null)
        //    {
        //        var gpApply = gpStagingRepository.GetAllRrOtsRmApplies().FirstOrDefault(i => i.APTODCNM == apply.APTODCNM &&
        //            i.APFRDCNM == apply.APFRDCNM &&
        //            i.APFRDCTY == apply.APFRDCTY &&
        //            i.APTODCTY == apply.APTODCTY &&
        //            i.INTERID == apply.INTERID &&
        //            i.INTSTATUS != null &&
        //            i.INTSTATUS != 0);
        //        if (gpApply == null)
        //        {
        //            return null;
        //        }
        //        apply.INTSTATUS = gpApply.INTSTATUS;
        //        apply.INTDATE = gpApply.INTDATE;
        //        if (gpApply.INTSTATUS == 2)
        //        {
        //            try
        //            {
        //                var errorApply = gpStagingRepository.GetAllRrOtsVwCashReceiptApplyErrors().Single(i => i.APTODCNM == gpApply.APTODCNM &&
        //                                                                                                i.APFRDCNM == gpApply.APFRDCNM &&
        //                                                                                                i.APFRDCTY == gpApply.APFRDCTY &&
        //                                                                                                i.APTODCTY == gpApply.APTODCTY &&
        //                                                                                                i.INTERID == gpApply.INTERID);
        //                errorMessages.Add(errorApply.ErrorDesc);
        //            }
        //            catch
        //            {
        //                errorMessages.Add("Unknown error");
        //            }
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        private int[] CopyPayableRecords(int batchId, out Dictionary<int, string> failedRecords)
        {
            failedRecords = new Dictionary<int, string>();
            List<int> batchEntryIDs = new List<int>();
            var rrOtsPmTransactions = gpRepository.GetAllRrOtsPmTransactions().Where(i => i.USERDEF1 == batchId.ToString()).ToList();

            //Update the posted date as we go 
            gpRepository.UpdateRrOtsPmTransaction(batchId.ToString());

            foreach (var transaction in rrOtsPmTransactions)
            {
                //OTSTM2-1139 No talk back needed
                //var entity = new RR_OTS_PM_TRANSACTIONS();
                //var entities = new List<RR_OTS_PM_TRANS_DISTRIBUTIONS>();
                //try
                //{
                //    entity = ModelAdapter.ToRrOtsPmTransactions(transaction);
                //    entity.INTSTATUS = 0;
                //    entity.INTDATE = null;
                //    entity.ERRORCODE = null;
                //    /// Insert record into SQL Server
                //    try
                //    {
                //        /// Delete any existing records first
                //        var deleteTransactions = gpStagingRepository.GetAllRrOtsPmTransactions().Where(i => i.VCHNUMWK == entity.VCHNUMWK &&
                //                                                                                        i.DOCTYPE == entity.DOCTYPE &&
                //                                                                                        i.INTERID == entity.INTERID).ToList();
                //        gpStagingRepository.DeleteRrOtsPmTransactions(deleteTransactions);
                //        gpStagingRepository.AddGpRrOtsPmTransaction(entity);
                //    }
                //    catch (Exception ex)
                //    {
                //        gpStagingRepository.DeleteRrOtsPmTransaction(entity);
                //        throw ex;
                //    }

                //    entities = gpRepository.GetRrOtsPmTransDistributionByBatchId(batchId).Where(i => i.VCHNUMWK == transaction.VCHNUMWK &&
                //                                                                  i.DOCTYPE == transaction.DOCTYPE &&
                //                                                                  i.INTERID == transaction.INTERID).Select(ModelAdapter.ToRrOtsPmTransDistributions).ToList();

                //    entities.ForEach(c =>
                //    {
                //        c.INTSTATUS = 0;
                //        c.INTDATE = null;
                //        c.ERRORCODE = null;
                //    });

                //    var deleteTransDistributions = gpStagingRepository.GetAllRrOtsPmTransDistributions().Where(i => i.VCHNUMWK == transaction.VCHNUMWK &&
                //                                                                   i.DOCTYPE == transaction.DOCTYPE &&
                //                                                                   i.INTERID == transaction.INTERID).ToList();
                //    /// Delete already existing staging records
                //    gpStagingRepository.DeleteRrOtsPmTransDistributions(deleteTransDistributions);

                //    gpStagingRepository.AddGpRrOtsPmTransDistributions(entities);

                if (!batchEntryIDs.Contains(transaction.GpiBatchEntryId))
                    batchEntryIDs.Add(transaction.GpiBatchEntryId);

                //}
                //catch (Exception ex)
                //{
                //    failedRecords.Add(transaction.GpiBatchEntryId, ex.Message);
                //    LogManager.LogExceptionWithMessage("GpiBatchEntryId: " + transaction.GpiBatchEntryId.ToString(), ex);
                //}
            }
            return batchEntryIDs.ToArray();
        }
        #endregion

        //OTSTM2-449 backup (manually)
        public void AutoSwitchForAllCustomers(int changedBy)
        {
            vendorRepository.AutoSwitchForAllCustomers(changedBy);
        }
        #region //Admin Financial Integration
        public IEnumerable<FinancialIntegrationVM> GetAdminFinancialIntegration()
        {
            var query = gpRepository.GetAdminFinancialIntegration();

            var result = query.Select(c => new FinancialIntegrationVM()
            {
                ID = c.ID,
                CategoryID = c.AdminFICategoryID,
                AccountLabel = c.AccountLabel,
                AccountNumber = c.AccountNumber,
                DisplayOrder = c.DisplayOrder
            }).OrderBy(a => a.CategoryID).ThenBy(o => o.DisplayOrder);

            return result;
        }
        public bool FIUpdateData(List<FinancialIntegrationVM> vm)
        {
            return gpRepository.FIUpdateData(vm, SecurityContextHelper.CurrentUser.Id);
        }
        #endregion
    }
}
