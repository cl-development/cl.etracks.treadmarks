﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Caching
{
    /// <summary>
    /// Defines applicaiton level cache key
    /// </summary>
    public class CachKeyManager
    {
        #region Fields
        public const string AppSettingsKey = "AppSettings";
        public const string ItemKey = "Items";
        public const string RateKey = "Rates";
        public const string ItemWeightKey = "ItemWeights"; 
        public const string SupplyItemWeightKey = "SupplyItemWeights"; //OTSTM2-1042
        public const string TransactionTypeKey = "TransactionType";
        public const string PostalCodeKey = "PostalCode";
        public const string ZoneKey = "Zone";
        public const string EligibilityKey = "Eligibility";
        public const string SystemUser = "SystemUser";
        public const string GpiAccountKey = "GPIAccount";
        public const string BusinessActivityKey = "BusinessActivities"; //OTSTM2-486
        public const string RolePermissionsKey = "RolePermissions";
        public const string RegionKey = "RegionKey"; //OTSTM2-215
        public const string CountryNameKey = "CountryNameKey"; //OTSTM2-1016
        public const string AppResourcesKey = "AppResources";
        public const string AppPermissionsKey = "AppPermissions";
        public const string ParticipantTypeKey = "ParticipantTypes";
        public const string FIAccountKey = "FIAccount"; //OTSTM2-1124 for Successor only
        public const string ProcessorSpecificRateKey = "ProcessorSpecificRate";
        public const string RateGroupRateKey = "RateGroupRate"; //OTSTM2-1185
        #endregion
    }
}