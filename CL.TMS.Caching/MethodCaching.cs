﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Caching
{
    /// <summary>
    /// Helper class to provide method data caching
    /// </summary>
    public static class MethodCaching
    {
        #region Public Methods
        /// <summary>
        /// Method invoker without parameters
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<TResult>(Func<TResult> function)
        {
            string key = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke();
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }

        /// <summary>
        /// Method invoker with one parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<T, TResult>(Func<T, TResult> function, T arg)
        {
            string methodName = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            string key = new Tuple<string, T>(methodName, arg).ToString();
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke(arg);
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }
        /// <summary>
        /// Method invoker with 2 parameters
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<T1, T2, TResult>(Func<T1, T2, TResult> function, T1 arg1, T2 arg2)
        {
            string methodName = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            string key = new Tuple<string, T1, T2>(methodName, arg1, arg2).ToString();
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke(arg1, arg2);
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }
        /// <summary>
        /// Method invoker with 3 parameters
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> function, T1 arg1, T2 arg2, T3 arg3)
        {
            string methodName = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            string key = new Tuple<string, T1, T2, T3>(methodName, arg1, arg2, arg3).ToString();
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke(arg1, arg2, arg3);
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }
        /// <summary>
        /// Method invoker with 4 parameters
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<T1, T2, T3, T4, TResult>(Func<T1, T2, T3, T4, TResult> function, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            string methodName = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            string key = new Tuple<string, T1, T2, T3, T4>(methodName, arg1, arg2, arg3, arg4).ToString();
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke(arg1, arg2, arg3, arg4);
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }
        /// <summary>
        /// Method invoker with 5 parameters
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="function"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public static TResult MethodInvoker<T1, T2, T3, T4, T5, TResult>(Func<T1, T2, T3, T4, T5, TResult> function, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            string methodName = string.Format("{0}-{1}", function.Method.Module.FullyQualifiedName, function.Method.Name);
            string key = new Tuple<string, T1, T2, T3, T4, T5>(methodName, arg1, arg2, arg3, arg4, arg5).ToString();
            if (CachingHelper.ContainsKey(key))
            {
                return CachingHelper.GetItem<TResult>(key);
            }
            else
            {
                TResult result = function.Invoke(arg1, arg2, arg3, arg4, arg5);
                CachingHelper.AddItem<TResult>(key, result);
                return result;
            }
        }
        #endregion 
    }
}
