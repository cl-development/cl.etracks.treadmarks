﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Reflection;

[assembly: AssemblyCompany("CentriLogic")]
[assembly: AssemblyCopyright("Copyright © CentriLogic 2018")]
[assembly: AssemblyProduct("TreadMarks")]
[assembly: AssemblyTrademark("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:

// Version number used to satisfy assembly references.  Just major.minor(.0.0).
[assembly: AssemblyVersion("10.20.0.1")]

// Informational version displayed to user
[assembly: AssemblyInformationalVersion("10.20.0.1")]

// Represents the "revision" level, for service packs and such.  major.minor should agree with AssemblyVersion above.
// Build.Revision must be bumped for new builds / service packs and such
[assembly: AssemblyFileVersion("10.20.0.1")]
