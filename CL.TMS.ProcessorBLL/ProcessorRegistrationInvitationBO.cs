﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Repository.System;

namespace CL.TMS.ProcessorBLL
{
    public class ProcessorRegistrationInvitationBO
    {
        private IApplicationInvitationRepository applicationInvitationRepository;
        private IApplicationRepository applicationRepository;

        public ProcessorRegistrationInvitationBO(IApplicationInvitationRepository applicationInvitationRepository, IApplicationRepository applicationRepository)
        {
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.applicationRepository = applicationRepository;
        }
        public ApplicationInvitation GetByTokenID(Guid guid)
        {
            return applicationInvitationRepository.GetById(guid);
        }
        //public void UpdateFormObject(int id, string formObject)
        //{
        //    applicationRepository.UpdateFormObject(id, formObject);

        //}

        //public ProcessorRegistrationModel GetFormObject(int id)
        //{
        //    string formObject = this.applicationRepository.GetFormObject(id);
        //    ProcessorRegistrationModel result = JsonConvert.DeserializeObject<ProcessorRegistrationModel>(formObject);
        //    return result;
        //}

        /// <summary>
        /// Set ApplicationID of Invitation to ID of new Application.
        /// </summary>
        /// <param name="appInvitation">The application invitation.</param>
        /// <exception cref="System.Exception"></exception>
        public void Update(ApplicationInvitation appInvitation)
        {
          applicationInvitationRepository.Update(appInvitation);
        }

        //public List<Vendor> GetVendorsByApplicationId(int applicationId)
        //{
        //    return applicationRepository.GetVendorsByApplicationId(applicationId);
        //}

        public Guid GetTokenByApplicationId(int applicationId)
        {
            return this.applicationInvitationRepository.GetTokenByApplicationId(applicationId);
        }

        public string GetEmailByApplicationId(int applicationId)
        {
            return this.applicationInvitationRepository.GetEmailByApplicationId(applicationId);
        }
    }
}
