﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.ViewModel.Registrant;

namespace CL.TMS.ProcessorBLL
{
    public class ProcessorRegistrationBO
    {
        private IApplicationRepository applicationRepository;
        private IItemRepository itemRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private IClaimsRepository claimsRepository;
        private IConfigurationsRepository configurationsRepository;
        public ProcessorRegistrationBO(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository,
            IClaimsRepository claimsRepository, IConfigurationsRepository configurationsRepository)
        {
            this.applicationRepository = applicationRepository;
            this.itemRepository = itemRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.claimsRepository = claimsRepository;
            this.configurationsRepository = configurationsRepository;
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            applicationRepository.UpdateFormObject(id, formObject, updatedBy);
        }

        public void UpdateApplication(Application application)
        {
            applicationRepository.UpdateApplication(application);
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>ProcessorRegistrationModel based on FormObject column in Application table</returns>
        public ProcessorRegistrationModel GetFormObject(int id)
        {
            var application = applicationRepository.GetSingleApplication(id);
            if (application == null)
            {
                throw new KeyNotFoundException();
            }

            ProcessorRegistrationModel result = null;
            var applicationStatus = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            if (applicationStatus == ApplicationStatusEnum.Approved
                || applicationStatus == ApplicationStatusEnum.BankInformationSubmitted
                || applicationStatus == ApplicationStatusEnum.BankInformationApproved
                || applicationStatus == ApplicationStatusEnum.Completed)
            {
                var vendor = vendorRepository.GetSingleWithApplicationID(id);
                result = GetApprovedApplication(vendor.ID);
                result.ID = id;
                result.VendorID = vendor.ID;
            }
            else
            {
                string formObject = application.FormObject;
                if (formObject != null)
                {
                    result = JsonConvert.DeserializeObject<ProcessorRegistrationModel>(formObject);
                }
                else
                {
                    result = new ProcessorRegistrationModel(id);
                }
            }

            if (application.AssignToUser.HasValue)
            {
                result.AssignToUserId = application.AssignToUser.Value;
            }
            result.Status = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            result.SubmittedDate = application.SubmittedDate;
            result.ExpireDate = application.ExpireDate;

            //OTSTM2-973 TermsAndConditons
            result.TermsAndConditions.TermsAndConditionsID = application.TermsAndConditionsID;

            return result;
        }

        public ProcessorRegistrationModel GetAllItemsList()
        {
            var result = new ProcessorRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            var ListItemModel = new List<ProcessorTireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new ProcessorTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new ProcessorTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public Application GetApplicationById(int appId)
        {
            var result = applicationRepository.GetSingleApplication(appId);
            return result;
        }

        public void RemoveEmptyContacts(ref IList<ContactRegistrationModel> contacts, ref IList<string> invalidFieldList)
        {
            if (contacts != null && contacts.Count() > 1)
            {
                List<ContactRegistrationModel> results = new List<ContactRegistrationModel>() { contacts[0] };
                IList<string> resultsFieldList = invalidFieldList.ToList();

                int totalContacts = contacts.Count();
                for (int i = 1; i < totalContacts; i++)
                {
                    bool remove = false;
                    if (contacts[i].ContactInformation != null)
                    {
                        var contact = contacts[i].ContactInformation;
                        remove = string.IsNullOrWhiteSpace(contact.FirstName) &&
                                  string.IsNullOrWhiteSpace(contact.LastName) &&
                                  string.IsNullOrWhiteSpace(contact.Position) &&
                                  string.IsNullOrWhiteSpace(contact.PhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Ext) &&
                                  string.IsNullOrWhiteSpace(contact.AlternatePhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Email);
                        if (remove)
                        {
                            //check if contact address has any information
                            if (!contacts[i].ContactAddressSameAsBusinessAddress)
                            {
                                if (contacts[i].ContactAddress != null)
                                {
                                    var address = contacts[i].ContactAddress;
                                    remove = !contact.ContactAddressSameAsBusiness &&
                                            string.IsNullOrWhiteSpace(address.AddressLine1) &&
                                            string.IsNullOrWhiteSpace(address.AddressLine2) &&
                                            string.IsNullOrWhiteSpace(address.City) &&
                                            string.IsNullOrWhiteSpace(address.Postal) &&
                                            string.IsNullOrWhiteSpace(address.Province) &&
                                            string.IsNullOrWhiteSpace(address.Country);
                                }
                            }
                        }
                    }
                    if (remove)
                    {
                        //remove items from invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                            }
                        }
                    }
                    else
                    {
                        results.Add(contacts[i]);
                        int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                        if (numRemoved > 0)
                        {
                            //replace all the names in invalidlist
                            foreach (string nameStr in invalidFieldList)
                            {
                                string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                                Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    resultsFieldList.Remove(nameStr);
                                    string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                    resultsFieldList.Add(newName);
                                }
                            }
                        }
                    }
                }
                contacts = results;
                invalidFieldList = resultsFieldList;
            }
        }

        public void RemoveEmptySortYardDetails(ref IList<ProcessorSortYardDetailsRegistrationModel> sortyardDetails, ref IList<string> invalidFieldList)
        {
            if (sortyardDetails.Count() == 0)
            {
                ProcessorSortYardDetailsRegistrationModel newItem = new ProcessorSortYardDetailsRegistrationModel();
                sortyardDetails.Add(newItem);
            }

            List<ProcessorSortYardDetailsRegistrationModel> results = new List<ProcessorSortYardDetailsRegistrationModel>() { sortyardDetails[0] };
            IList<string> resultsFieldList = invalidFieldList.ToList();

            int totalSortYardDetails = sortyardDetails.Count();
            for (int i = 1; i < totalSortYardDetails; i++)
            {
                bool remove = false;
                if (sortyardDetails[i].SortYardAddress != null)
                {
                    var address = sortyardDetails[i].SortYardAddress;

                    remove = string.IsNullOrEmpty(address.AddressLine1) &&
                             string.IsNullOrEmpty(address.AddressLine2) &&
                             string.IsNullOrEmpty(address.City) &&
                             string.IsNullOrEmpty(address.Postal) &&
                             string.IsNullOrEmpty(address.Province) &&
                             string.IsNullOrEmpty(address.Country) &&
                             string.IsNullOrEmpty(sortyardDetails[i].MaxStorageCapacity);
                }
                if (remove)
                {
                    //remove items from invalidlist
                    foreach (string nameStr in invalidFieldList)
                    {
                        string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                        Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            resultsFieldList.Remove(nameStr);
                        }
                    }
                }
                else
                {
                    results.Add(sortyardDetails[i]);
                    int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                    if (numRemoved > 0)
                    {
                        //replace all the names in invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                                string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                resultsFieldList.Add(newName);
                            }
                        }
                    }
                }
            }
            sortyardDetails = results;
            invalidFieldList = resultsFieldList;
        }

        public ProcessorRegistrationModel GetAllProcessorProduced()
        {
            var result = new ProcessorRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            var ListItemModel = new List<ProcessorTireDetailsItemTypeModel>();

            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new ProcessorTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new ProcessorTireDetailsRegistrationModel()
            {
                ProductsProduceType = ListItemModel
            };
            return result;
        }

        public ProcessorRegistrationModel GetAllProduct(int ItemCategory)
        {
            var result = new ProcessorRegistrationModel();
            var allTireItems = itemRepository.GetAllProduct(ItemCategory);
            var ListItemModel = new List<ProcessorTireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new ProcessorTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new ProcessorTireDetailsRegistrationModel()
            {
                ProductsProduceType = ListItemModel
            };
            return result;
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>ProcessorRegistrationModel based on FormObject column in Application table</returns>

        public ProcessorRegistrationModel GetByTokenId(Guid tokenId)
        {
            var application = applicationRepository.GetApplicationByTokenId(tokenId);
            return GetFormObject(application.ID);
        }

        public ApplicationEmailModel GetApprovedProcessorApplicantInformation(int applicationId)
        {
            var application = this.applicationRepository.FindApplicationByAppID(applicationId);
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }

            var applicationProcessorApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                PrimaryContactLastName = vendorPrimaryContact.LastName,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = applicationInvitationRepository.GetTokenByApplicationId(applicationId)
            };

            return applicationProcessorApplicationModel;
        }

        #region Workflow

        /// <summary>
        /// updating the entities after approve selection
        /// </summary>
        /// <param name="applicationProcessorModel"></param>
        /// <param name="appID"></param>
        public int ApproveApplication(ProcessorRegistrationModel applicationProcessorModel, int appID)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var app = this.applicationRepository.FindApplicationByAppID(appID);
            app.SigningAuthorityFirstName = applicationProcessorModel.TermsAndConditions.SigningAuthorityFirstName;
            app.SigningAuthorityLastName = applicationProcessorModel.TermsAndConditions.SigningAuthorityLastName;
            app.SigningAuthorityPosition = applicationProcessorModel.TermsAndConditions.SigningAuthorityPosition;
            app.FormCompletedByFirstName = applicationProcessorModel.TermsAndConditions.FormCompletedByFirstName;
            app.FormCompletedByLastName = applicationProcessorModel.TermsAndConditions.FormCompletedByLastName;
            app.FormCompletedByPhone = applicationProcessorModel.TermsAndConditions.FormCompletedByPhone;
            app.AgreementAcceptedByFullName = applicationProcessorModel.TermsAndConditions.AgreementAcceptedByFullName;
            app.UserIP = applicationProcessorModel.UserIPAddress;
            app.Status = ApplicationStatusEnum.Approved.ToString();
            app.FormObject = app.FormObject;

            AddApplicationSupportingDocuments(applicationProcessorModel, app);

            var vendor = new Vendor
            {
                BusinessName = applicationProcessorModel.BusinessLocation.BusinessName,
                OperatingName = applicationProcessorModel.BusinessLocation.OperatingName,
                BusinessStartDate = applicationProcessorModel.ProcessorDetails.BusinessStartDate,
                BusinessNumber = applicationProcessorModel.ProcessorDetails.BusinessNumber,
                CommercialLiabHSTNumber = applicationProcessorModel.ProcessorDetails.CommercialLiabHstNumber,
                CertificateOfApprovalNum = applicationProcessorModel.ProcessorDetails.CertificateOfApproval,
                IsTaxExempt = applicationProcessorModel.ProcessorDetails.IsTaxExempt,
                CommercialLiabInsurerName = applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerName,
                CommercialLiabInsurerExpDate = applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerExpDate,
                HIsGVWR = applicationProcessorModel.ProcessorDetails.HIsGvwr,
                CVORExpiryDate = applicationProcessorModel.ProcessorDetails.CvorExpiryDate,
                HasMoreThanOneEmp = applicationProcessorModel.ProcessorDetails.HasMoreThanOneEmp,
                WSIBNumber = applicationProcessorModel.ProcessorDetails.WsibNumber,
                ApplicationID = app.ID,
                CreatedDate = DateTime.UtcNow,
                VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue,
                LastUpdatedBy = updatedBy,
                LastUpdatedDate = DateTime.UtcNow,
                OtherRegistrantSubType = applicationProcessorModel.TireDetails.OtherRegistrantSubType,
                OtherProcessProduct = applicationProcessorModel.TireDetails.OtherProcessProduct,
                RegistrantSubTypeID = applicationProcessorModel.TireDetails.RegistrantSubTypeID,
                ActiveStateChangeDate = DateTime.Now.Date,
                ActivationDate = DateTime.Now.Date,
                IsActive = true,
                ContactInfo = string.Format("{0} {1}, {2}", applicationProcessorModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationProcessorModel.ContactInformationList[0].ContactInformation.LastName, applicationProcessorModel.ContactInformationList[0].ContactInformation.PhoneNumber),
                RegistrantStatus = ApplicationStatusEnum.Approved.ToString(),
                SigningAuthorityFirstName = applicationProcessorModel.TermsAndConditions.SigningAuthorityFirstName,
                SigningAuthorityLastName = applicationProcessorModel.TermsAndConditions.SigningAuthorityLastName,
                SigningAuthorityPosition = applicationProcessorModel.TermsAndConditions.SigningAuthorityPosition,
                FormCompletedByFirstName = applicationProcessorModel.TermsAndConditions.FormCompletedByFirstName,
                FormCompletedByLastName = applicationProcessorModel.TermsAndConditions.FormCompletedByLastName,
                FormCompletedByPhone = applicationProcessorModel.TermsAndConditions.FormCompletedByPhone,
                AgreementAcceptedByFullName = applicationProcessorModel.TermsAndConditions.AgreementAcceptedByFullName,
                AgreementAcceptedCheck = applicationProcessorModel.TermsAndConditions.AgreementAcceptedCheck,

            };

            MoveAttachmentToVendor(applicationProcessorModel, vendor);

            var vendorPrimaryAddress = new Address()
            {
                Address1 = applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine1,
                Address2 = applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine2,
                City = applicationProcessorModel.BusinessLocation.BusinessAddress.City,
                Province = applicationProcessorModel.BusinessLocation.BusinessAddress.Province,
                PostalCode = applicationProcessorModel.BusinessLocation.BusinessAddress.Postal,
                Country = applicationProcessorModel.BusinessLocation.BusinessAddress.Country,
                Phone = applicationProcessorModel.BusinessLocation.Phone,
                Email = applicationProcessorModel.BusinessLocation.Email,
                Ext = applicationProcessorModel.BusinessLocation.Extension,
                AddressType = (int)AddressTypeEnum.Business,
            };

            vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (applicationProcessorModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = new Address
                {
                    Address1 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1,
                    Address2 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine2,
                    City = applicationProcessorModel.BusinessLocation.MailingAddress.City,
                    Province = applicationProcessorModel.BusinessLocation.MailingAddress.Province,
                    PostalCode = applicationProcessorModel.BusinessLocation.MailingAddress.Postal,
                    Country = applicationProcessorModel.BusinessLocation.MailingAddress.Country,
                    Phone = applicationProcessorModel.BusinessLocation.Phone,
                    Email = applicationProcessorModel.BusinessLocation.Email,
                    AddressType = (int)AddressTypeEnum.Mail,
                };

                vendor.VendorAddresses.Add(vendorMailingAddress);
            }

            var firstVendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
            if (firstVendorAddress != null)
            {
                for (var i = 0; i < applicationProcessorModel.ContactInformationList.Count; i++)
                {
                    if (applicationProcessorModel.ContactInformationList[i].ContactInformation.FirstName != null &&
                        applicationProcessorModel.ContactInformationList[i].ContactInformation.LastName != null)
                    {
                        var contact = new Contact()
                        {
                            FirstName = applicationProcessorModel.ContactInformationList[i].ContactInformation.FirstName,
                            LastName = applicationProcessorModel.ContactInformationList[i].ContactInformation.LastName,
                            Position = applicationProcessorModel.ContactInformationList[i].ContactInformation.Position,
                            PhoneNumber =
                                applicationProcessorModel.ContactInformationList[i].ContactInformation.PhoneNumber,
                            Ext = applicationProcessorModel.ContactInformationList[i].ContactInformation.Ext,
                            AlternatePhoneNumber =
                                applicationProcessorModel.ContactInformationList[i].ContactInformation.AlternatePhoneNumber,
                            Email = applicationProcessorModel.ContactInformationList[i].ContactInformation.Email,
                            IsPrimary = i == 0,
                        };

                        if (!applicationProcessorModel.ContactInformationList[i].ContactAddressSameAsBusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(applicationProcessorModel.ContactInformationList[i].ContactAddress.AddressLine1))
                            {
                                var contactAddress = new Address()
                                {
                                    Address1 =
                                        applicationProcessorModel.ContactInformationList[i].ContactAddress.AddressLine1,
                                    Address2 =
                                        applicationProcessorModel.ContactInformationList[i].ContactAddress.AddressLine2,
                                    City = applicationProcessorModel.ContactInformationList[i].ContactAddress.City,
                                    Province = applicationProcessorModel.ContactInformationList[i].ContactAddress.Province,
                                    PostalCode = applicationProcessorModel.ContactInformationList[i].ContactAddress.Postal,
                                    Country = string.IsNullOrWhiteSpace(applicationProcessorModel.ContactInformationList[i].ContactAddress.Country) ? "Canada" : applicationProcessorModel.ContactInformationList[i].ContactAddress.Country,
                                    AddressType = (int)AddressTypeEnum.Contact
                                };
                                contactAddress.Contacts.Add(contact);
                                vendor.VendorAddresses.Add(contactAddress);
                            }
                            else
                            {
                                firstVendorAddress.Contacts.Add(contact);
                            }
                        }
                        else
                        {
                            firstVendorAddress.Contacts.Add(contact);
                        }
                    }
                }
            }
            var addresses = new List<Address>();
            foreach (var sortYard in applicationProcessorModel.SortYardDetails)
            {
                sortYard.StorageSiteType = 1;
                if (!string.IsNullOrEmpty(sortYard.SortYardAddress.AddressLine1))
                {
                    var found = false;
                    foreach (var eachAddress in vendor.VendorAddresses)
                    {
                        if ((string.Compare(sortYard.SortYardAddress.AddressLine1, eachAddress.Address1, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.AddressLine2, eachAddress.Address2, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.City, eachAddress.City, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.Province, eachAddress.Province, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Postal, eachAddress.PostalCode, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Country, eachAddress.Country, StringComparison.OrdinalIgnoreCase) == 0))
                        {
                            double maxSortYardCapacity;
                            double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                            VendorStorageSite vS = new VendorStorageSite()
                            {
                                MaxStorageCapacity = maxSortYardCapacity,
                                CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                                StorageSiteType = 1
                            };
                            eachAddress.VendorStorageSites.Add(vS);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        var sortYardAddress = new Address();

                        sortYardAddress.Address1 = sortYard.SortYardAddress.AddressLine1;
                        sortYardAddress.Address2 = sortYard.SortYardAddress.AddressLine2;
                        sortYardAddress.City = sortYard.SortYardAddress.City;
                        sortYardAddress.PostalCode = sortYard.SortYardAddress.Postal;
                        sortYardAddress.Province = sortYard.SortYardAddress.Province;
                        sortYardAddress.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country)
                                ? "Canada"
                                : sortYard.SortYardAddress.Country;
                        sortYardAddress.AddressType = (int)AddressTypeEnum.Sortyard;
                        sortYardAddress.Contacts = null;

                        double maxSortYardCapacity;
                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        VendorStorageSite vS = new VendorStorageSite()
                        {
                            MaxStorageCapacity = maxSortYardCapacity,
                            CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                            StorageSiteType = 1
                        };

                        sortYardAddress.VendorStorageSites.Add(vS);

                        addresses.Add(sortYardAddress);
                    }
                }
            }

            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            //vendoractivehistory
            var vendorActiveHistory = new VendorActiveHistory()
            {
                ActiveState = true,
                ActiveStateChangeDate = DateTime.Now.Date,
                VendorID = vendor.ID,
                CreateDate = DateTime.UtcNow,
                IsValid = true
            };

            var currentUser = TMSContext.TMSPrincipal.Identity;
            vendorActiveHistory.CreatedBy = currentUser.Name;

            vendor.VendorActiveHistories.Add(vendorActiveHistory);

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            applicationProcessorModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = defaultItems.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            });

            vendor.RegistrantSubTypeID = applicationProcessorModel.TireDetails.RegistrantSubTypeID;

            var productItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();

            applicationProcessorModel.TireDetails.ProductsProduceType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = productItems.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            });

            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                applicationRepository.UpdateApplicationEntity(app);
                vendor.Number = vendorRepository.GetVendorRegistrationNumber(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue).ToString();//OTSTM2-508
                vendorRepository.AddVendor(vendor);

                //create vendor - GroupRate mapping
                this.configurationsRepository.ApproveVendortoGroup(applicationProcessorModel.rateGroupId, applicationProcessorModel.SelectedVendorGroupId, vendor.ID, TreadMarksConstants.DeliveryGroup, applicationProcessorModel.approvedByUserId);

                //Add VendorAttachments
                var vendorAttachments = new List<VendorAttachment>();
                app.Attachments.ToList().ForEach(c =>
                {
                    var vendorAttachment = new VendorAttachment();
                    vendorAttachment.AttachmentId = c.ID;
                    vendorAttachment.VendorId = vendor.ID;
                    vendorAttachments.Add(vendorAttachment);
                });
                vendorRepository.AddVendorAttachments(vendorAttachments);
                vendorRepository.UpdateVendorKeywords(vendor);
                //Update applicationnote
                applicationRepository.UpdateApplicatioNoteWithVendorId(vendor.ID, app.ID);

                //Initialize Claim for approved application
                //ET-37 Stop creating claims
                //claimsRepository.InitializeClaimForVendor(vendor.ID);

                transationScope.Complete();
            }

            return vendor.ID;
        }

        private void MoveAttachmentToVendor(ProcessorRegistrationModel applicationProcessorModel, Vendor vendor)
        {
            //Moving supporting document options
            var vendorSupportingDocumentMBL = new VendorSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentMBL);

            var vendorSupportingDocumentCLI = new VendorSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCLI);

            var vendorSupportingDocumentHST = new VendorSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentHST);

            var vendorSupportingDocumentWSIB = new VendorSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentWSIB);

            var vendorSupportingDocumentCHQ = new VendorSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCHQ);

        }

        private void AddApplicationSupportingDocuments(ProcessorRegistrationModel applicationProcessorModel, Application app)
        {
            var applicationSupportingDocumentMBL = new ApplicationSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentMBL);

            var applicationSupportingDocumentCLI = new ApplicationSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCLI);


            var applicationSupportingDocumentHST = new ApplicationSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentHST);

            var applicationSupportingDocumentWSIB = new ApplicationSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentWSIB);

            var applicationSupportingDocumentCHQ = new ApplicationSupportingDocument()
            {
                Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCHQ);
        }

        public int UpdateApproveApplication(ProcessorRegistrationModel applicationProcessorModel, int vendorId)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var deleteAddresses = new List<Address>(); //OTSTM2-643 use existing list to collect sortyard addresses that to be deleted
            var newContactAddresses = new List<ContactAddressDto>();
            var deleteContacts = new List<Contact>(); //OTSTM2-50

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                if (!(vendor.GpUpdate ?? false))
                {
                    var participantExisting = new GpParticipantUpdate(vendor);
                    var newContact = applicationProcessorModel.ContactInformationList.First();
                    var participantNew = new GpParticipantUpdate()
                    {
                        MailingAddress1 = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine1 : applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1,
                        MailingAddress2 = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine2 : applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine2,
                        MailingCity = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.City : applicationProcessorModel.BusinessLocation.MailingAddress.City,
                        MailingCountry = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.Country : applicationProcessorModel.BusinessLocation.MailingAddress.Country,
                        MailingPostalCode = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.Postal : applicationProcessorModel.BusinessLocation.MailingAddress.Postal,
                        MailingProvince = applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationProcessorModel.BusinessLocation.BusinessAddress.Province : applicationProcessorModel.BusinessLocation.MailingAddress.Province,
                        PrimaryContactName = string.Format("{0} {1}", newContact.ContactInformation.FirstName, newContact.ContactInformation.LastName),
                        PrimaryContactPhone = newContact.ContactInformation.PhoneNumber ?? string.Empty,
                        IsTaxExempt = applicationProcessorModel.ProcessorDetails.IsTaxExempt,
                        IsActive = vendor.IsActive
                    };
                    if (!participantExisting.Equals(participantNew))
                    {
                        vendor.GpUpdate = true;
                    }
                }
                vendor.BusinessName = applicationProcessorModel.BusinessLocation.BusinessName;
                vendor.OperatingName = applicationProcessorModel.BusinessLocation.OperatingName;
                vendor.BusinessStartDate = applicationProcessorModel.ProcessorDetails.BusinessStartDate;
                vendor.BusinessNumber = applicationProcessorModel.ProcessorDetails.BusinessNumber;
                vendor.CommercialLiabHSTNumber = applicationProcessorModel.ProcessorDetails.CommercialLiabHstNumber;
                vendor.IsTaxExempt = applicationProcessorModel.ProcessorDetails.IsTaxExempt;
                vendor.CommercialLiabInsurerName = applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerName;
                vendor.CommercialLiabInsurerExpDate = applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerExpDate;
                vendor.CertificateOfApprovalNum = applicationProcessorModel.ProcessorDetails.CertificateOfApproval;
                vendor.HIsGVWR = applicationProcessorModel.ProcessorDetails.HIsGvwr;
                vendor.CVORExpiryDate = applicationProcessorModel.ProcessorDetails.CvorExpiryDate;
                vendor.HasMoreThanOneEmp = applicationProcessorModel.ProcessorDetails.HasMoreThanOneEmp;
                vendor.WSIBNumber = applicationProcessorModel.ProcessorDetails.WsibNumber;
                vendor.VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue;
                vendor.LastUpdatedBy = updatedBy;
                vendor.OtherRegistrantSubType = applicationProcessorModel.TireDetails.OtherRegistrantSubType;
                vendor.OtherProcessProduct = applicationProcessorModel.TireDetails.OtherProcessProduct;
                vendor.RegistrantSubTypeID = applicationProcessorModel.TireDetails.RegistrantSubTypeID;
                vendor.LastUpdatedDate = DateTime.UtcNow;
                vendor.ContactInfo = string.Format("{0} {1}, {2}", applicationProcessorModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationProcessorModel.ContactInformationList[0].ContactInformation.LastName, applicationProcessorModel.ContactInformationList[0].ContactInformation.PhoneNumber);

                //added
                vendor.SigningAuthorityFirstName = applicationProcessorModel.TermsAndConditions.SigningAuthorityFirstName;
                vendor.SigningAuthorityLastName = applicationProcessorModel.TermsAndConditions.SigningAuthorityLastName;
                vendor.SigningAuthorityPosition = applicationProcessorModel.TermsAndConditions.SigningAuthorityPosition;
                vendor.FormCompletedByFirstName = applicationProcessorModel.TermsAndConditions.FormCompletedByFirstName;
                vendor.FormCompletedByLastName = applicationProcessorModel.TermsAndConditions.FormCompletedByLastName;
                vendor.FormCompletedByPhone = applicationProcessorModel.TermsAndConditions.FormCompletedByPhone;
                vendor.AgreementAcceptedByFullName = applicationProcessorModel.TermsAndConditions.AgreementAcceptedByFullName;
                vendor.AgreementAcceptedCheck = applicationProcessorModel.TermsAndConditions.AgreementAcceptedCheck;

                //Save vendor supporting document
                UpdateVendorSupportingDocuments(applicationProcessorModel, vendor);

            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                vendorPrimaryAddress.Address1 = applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine1;
                vendorPrimaryAddress.Address2 = applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine2;
                vendorPrimaryAddress.City = applicationProcessorModel.BusinessLocation.BusinessAddress.City;
                vendorPrimaryAddress.Province = applicationProcessorModel.BusinessLocation.BusinessAddress.Province;
                vendorPrimaryAddress.PostalCode = applicationProcessorModel.BusinessLocation.BusinessAddress.Postal;
                vendorPrimaryAddress.Country = applicationProcessorModel.BusinessLocation.BusinessAddress.Country;
                vendorPrimaryAddress.Phone = applicationProcessorModel.BusinessLocation.Phone;
                vendorPrimaryAddress.Email = applicationProcessorModel.BusinessLocation.Email;
                vendorPrimaryAddress.Ext = applicationProcessorModel.BusinessLocation.Extension;
                vendorPrimaryAddress.AddressType = (int)AddressTypeEnum.Business;
            }

            //vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (applicationProcessorModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);

                if (vendorMailingAddress != null)
                {
                    vendorMailingAddress.Address1 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1;
                    vendorMailingAddress.Address2 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine2;
                    vendorMailingAddress.City = applicationProcessorModel.BusinessLocation.MailingAddress.City;
                    vendorMailingAddress.Province = applicationProcessorModel.BusinessLocation.MailingAddress.Province;
                    vendorMailingAddress.PostalCode = applicationProcessorModel.BusinessLocation.MailingAddress.Postal;
                    vendorMailingAddress.Country = applicationProcessorModel.BusinessLocation.MailingAddress.Country;
                    vendorMailingAddress.Phone = applicationProcessorModel.BusinessLocation.Phone;
                    vendorMailingAddress.Email = applicationProcessorModel.BusinessLocation.Email;
                }
                else
                {
                    var mailingAddress = new Address()
                    {
                        Address1 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1,
                        Address2 = applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine2,
                        City = applicationProcessorModel.BusinessLocation.MailingAddress.City,
                        Province = applicationProcessorModel.BusinessLocation.MailingAddress.Province,
                        PostalCode = applicationProcessorModel.BusinessLocation.MailingAddress.Postal,
                        Country = applicationProcessorModel.BusinessLocation.MailingAddress.Country,
                        Phone = applicationProcessorModel.BusinessLocation.Phone,
                        Email = applicationProcessorModel.BusinessLocation.Email,
                        AddressType = (int)AddressTypeEnum.Mail
                    };

                    vendor.VendorAddresses.Add(mailingAddress);
                }
            }

            else
            {
                //OTSTM2-476
                var currentMailingAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == (int)AddressTypeEnum.Mail);

                if (currentMailingAddress != null)
                {
                    vendor.VendorAddresses.Remove(currentMailingAddress);
                }
            }

            //OTSTM2-50 remove contact
            var contactIdList = applicationProcessorModel.ContactInformationList.Select(c => c.ContactInformation.ID).ToList();
            foreach (var contactAddress in vendor.VendorAddresses.ToList())
            {
                foreach (var contact in contactAddress.Contacts.ToList())
                {
                    if (!contactIdList.Contains(contact.ID))
                    {
                        deleteContacts.Add(contact);
                    }
                }
            }

            foreach (var applicationContactInfo in applicationProcessorModel.ContactInformationList)
            {
                Contact contact = null;

                if (applicationContactInfo.isEmptyContact)
                {
                    var contactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                    contact = contactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                    contactAddress.Contacts.Remove(contact);
                    vendor.VendorAddresses.Remove(contactAddress);
                }
                else
                {
                    #region contact
                    //check if this is a new contact and address
                    if (applicationContactInfo.ContactInformation.AddressID == 0)
                    {
                        contact = new Contact() { IsPrimary = false };
                        ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        if (!applicationContactInfo.ContactInformation.ContactAddressSameAsBusiness)
                        {
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contactAddress.Contacts.Add(contact);
                            vendor.VendorAddresses.Add(contactAddress);
                        }
                        else
                        {
                            var vendorBusinessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationProcessorModel.BusinessLocation.BusinessAddress.ID);
                            vendorBusinessAddress.Contacts.Add(contact);
                            vendorRepository.UpdateVendor(vendor); //OTSTM2-1037 make sure contact 1 with same as Business Address checked to be saved earlier, so that has a smaller ID value
                        }
                    }
                    else
                    {
                        var currentContactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                        var businessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationProcessorModel.BusinessLocation.BusinessAddress.ID);
                        var oldIsAddressSameAsBusiness = currentContactAddress.AddressType == (int)AddressTypeEnum.Business;
                        var newIsAddressSameAsBusiness = applicationContactInfo.ContactAddressSameAsBusinessAddress;
                        if (newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //no change update contact information
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                        else if (newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //changed selection to same as business
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            contact.AddressID = businessAddress.ID;
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = businessAddress
                            });
                            deleteAddresses.Add(contact.Address);
                        }
                        else if (!newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //remove from business address and add new address
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = contactAddress
                            });
                        }
                        else if (!newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //no change update address and contact information
                            ModelAdapter.UpdateAddress(ref currentContactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                    }
                    #endregion
                }
            }

            //OTSTM2-643 remove sort yard
            var sortYardIdList = applicationProcessorModel.SortYardDetails.Select(c => c.SortYardAddress.ID).ToList(); //SortYardAddress.ID and VendorStorageSite.ID are same thing
            foreach (var vendorStorageSite in vendor.VendorStorageSites.ToList())
            {
                if (!sortYardIdList.Contains(vendorStorageSite.ID))
                {
                    deleteAddresses.Add(vendorStorageSite.Address); //OTSTM2-643 use existing list to collect sortyard addresses that to be deleted
                    vendor.VendorStorageSites.Remove(vendorStorageSite);
                }
            }

            foreach (var sortYard in applicationProcessorModel.SortYardDetails)
            {
                bool addNewSortYard = false;
                if (sortYard.SortYardAddress.ID != 0)
                {
                    var existingSortYardDetails = vendor.VendorStorageSites.Where(r => r.ID == sortYard.SortYardAddress.ID);

                    if (existingSortYardDetails.Any())
                    {
                        var selectedSortYard = existingSortYardDetails.FirstOrDefault();

                        selectedSortYard.Address.Address1 = sortYard.SortYardAddress.AddressLine1;
                        selectedSortYard.Address.Address2 = sortYard.SortYardAddress.AddressLine2;
                        selectedSortYard.Address.City = sortYard.SortYardAddress.City;
                        selectedSortYard.Address.PostalCode = sortYard.SortYardAddress.Postal;
                        selectedSortYard.Address.Province = sortYard.SortYardAddress.Province;

                        selectedSortYard.Address.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country;

                        double maxSortYardCapacity;

                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        selectedSortYard.MaxStorageCapacity = maxSortYardCapacity;
                        selectedSortYard.CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber;
                        selectedSortYard.StorageSiteType = 1;
                    }

                    else
                    {
                        addNewSortYard = true;
                    }
                }

                else
                {
                    addNewSortYard = true;
                }

                if (addNewSortYard)
                {
                    var vendorStorageSite = new VendorStorageSite()
                    {
                        Address = new Address()
                        {
                            Address1 = sortYard.SortYardAddress.AddressLine1,
                            Address2 = sortYard.SortYardAddress.AddressLine2,
                            City = sortYard.SortYardAddress.City,
                            PostalCode = sortYard.SortYardAddress.Postal,
                            Province = sortYard.SortYardAddress.Province,
                            Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country
                        },
                        MaxStorageCapacity = Convert.ToDouble(sortYard.MaxStorageCapacity),
                        CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                        StorageSiteType = 1,
                        UnitType = 3
                    };

                    vendor.VendorStorageSites.Add(vendorStorageSite);
                }
            }

            var addresses = new List<Address>();
            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            foreach (var item in applicationProcessorModel.TireDetails.TireDetailsItemType)
            {
                if (item.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == item.ID))
                    {
                        var result = defaultItems.Single(s => s.ID == item.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == item.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            }

            vendor.RegistrantSubTypeID = applicationProcessorModel.TireDetails.RegistrantSubTypeID;

            var productItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            foreach (var item in applicationProcessorModel.TireDetails.ProductsProduceType)
            {
                if (item.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == item.ID))
                    {
                        var result = productItems.Single(s => s.ID == item.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == item.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            }

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                vendorRepository.RemoveAddresses(deleteAddresses); //OTSTM2-643 Deleting address should be done before deleting vendorStorageSite in vendor because of foreign key
                vendorRepository.RemoveContacts(deleteContacts); //OTSTM2-50
                vendorRepository.UpdateVendorContacts(newContactAddresses, vendor.ID);
                vendorRepository.UpdateVendor(vendor);
                vendorRepository.UpdateVendorKeywords(vendor);
                transationScope.Complete();
            }

            return vendor.ID;
        }

        private void UpdateVendorSupportingDocuments(ProcessorRegistrationModel applicationProcessorModel, Vendor vendor)
        {
            foreach (var supportDocOption in vendor.VendorSupportingDocuments)
            {
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.MBL)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionMBL;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CLI)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCLI;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CFP)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCFP;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.HST)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionHST;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.WSIB)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionWSIB;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CHQ)
                {
                    supportDocOption.Option = applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCHQ;
                }
            }
        }

        public ProcessorRegistrationModel GetApprovedApplication(int vendorId)
        {
            ProcessorRegistrationModel applicationProcessorModel = new ProcessorRegistrationModel();

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                applicationProcessorModel.Status = vendor.RegistrantStatus == null ? ApplicationStatusEnum.Approved :
                        (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), vendor.RegistrantStatus);

                applicationProcessorModel.ActiveInactive.ApprovedDate = vendor.CreatedDate;

                if (vendor.ApplicationID.HasValue)
                {
                    var app = this.applicationRepository.FindApplicationByAppID(vendor.ApplicationID.Value);
                    applicationProcessorModel.ID = app.ID;
                    applicationProcessorModel.UserIPAddress = app.UserIP;
                    applicationProcessorModel.SubmittedDate = app.SubmittedDate;
                    //OTSTM2-973
                    applicationProcessorModel.TermsAndConditions.TermsAndConditionsID = app.TermsAndConditionsID;
                }

                applicationProcessorModel.BusinessLocation.BusinessName = vendor.BusinessName;
                applicationProcessorModel.BusinessLocation.OperatingName = vendor.OperatingName;
                applicationProcessorModel.ProcessorDetails.BusinessStartDate = vendor.BusinessStartDate;
                applicationProcessorModel.ProcessorDetails.BusinessNumber = vendor.BusinessNumber;
                applicationProcessorModel.ProcessorDetails.CommercialLiabHstNumber = vendor.CommercialLiabHSTNumber;
                applicationProcessorModel.ProcessorDetails.IsTaxExempt = vendor.IsTaxExempt;
                applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerName = vendor.CommercialLiabInsurerName;
                applicationProcessorModel.ProcessorDetails.CommercialLiabInsurerExpDate = vendor.CommercialLiabInsurerExpDate;
                applicationProcessorModel.ProcessorDetails.CertificateOfApproval = vendor.CertificateOfApprovalNum;
                applicationProcessorModel.ProcessorDetails.HIsGvwr = vendor.HIsGVWR;
                applicationProcessorModel.ProcessorDetails.CvorExpiryDate = vendor.CVORExpiryDate;
                applicationProcessorModel.ProcessorDetails.HasMoreThanOneEmp = vendor.HasMoreThanOneEmp;
                applicationProcessorModel.ProcessorDetails.WsibNumber = vendor.WSIBNumber;
                applicationProcessorModel.RegistrationNumber = Convert.ToInt32(vendor.Number);

                applicationProcessorModel.TermsAndConditions.SigningAuthorityFirstName = vendor.SigningAuthorityFirstName;
                applicationProcessorModel.TermsAndConditions.SigningAuthorityLastName = vendor.SigningAuthorityLastName;
                applicationProcessorModel.TermsAndConditions.SigningAuthorityPosition = vendor.SigningAuthorityPosition;
                applicationProcessorModel.TermsAndConditions.FormCompletedByFirstName = vendor.FormCompletedByFirstName;
                applicationProcessorModel.TermsAndConditions.FormCompletedByLastName = vendor.FormCompletedByLastName;
                applicationProcessorModel.TermsAndConditions.FormCompletedByPhone = vendor.FormCompletedByPhone;
                applicationProcessorModel.TermsAndConditions.AgreementAcceptedByFullName = vendor.AgreementAcceptedByFullName;
                applicationProcessorModel.TermsAndConditions.AgreementAcceptedCheck = vendor.AgreementAcceptedCheck ?? true;
                applicationProcessorModel.CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue));

            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine1 = vendorPrimaryAddress.Address1;
                applicationProcessorModel.BusinessLocation.BusinessAddress.AddressLine2 = vendorPrimaryAddress.Address2;
                applicationProcessorModel.BusinessLocation.BusinessAddress.City = vendorPrimaryAddress.City;
                applicationProcessorModel.BusinessLocation.BusinessAddress.Province = vendorPrimaryAddress.Province;
                applicationProcessorModel.BusinessLocation.BusinessAddress.Postal = vendorPrimaryAddress.PostalCode;
                applicationProcessorModel.BusinessLocation.BusinessAddress.Country = vendorPrimaryAddress.Country;
                applicationProcessorModel.BusinessLocation.Phone = vendorPrimaryAddress.Phone;
                applicationProcessorModel.BusinessLocation.Email = vendorPrimaryAddress.Email;
                applicationProcessorModel.BusinessLocation.Extension = vendorPrimaryAddress.Ext;
                applicationProcessorModel.BusinessLocation.BusinessAddress.ID = vendorPrimaryAddress.ID;
            }

            var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
            if (vendorMailingAddress != null)
            {
                applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine1 = vendorMailingAddress.Address1;
                applicationProcessorModel.BusinessLocation.MailingAddress.AddressLine2 = vendorMailingAddress.Address2;
                applicationProcessorModel.BusinessLocation.MailingAddress.City = vendorMailingAddress.City;
                applicationProcessorModel.BusinessLocation.MailingAddress.Province = vendorMailingAddress.Province;
                applicationProcessorModel.BusinessLocation.MailingAddress.Postal = vendorMailingAddress.PostalCode;
                applicationProcessorModel.BusinessLocation.MailingAddress.Country = vendorMailingAddress.Country;
                applicationProcessorModel.BusinessLocation.MailingAddress.ID = vendorMailingAddress.ID;
                applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness = false;
            }
            else
            {
                applicationProcessorModel.BusinessLocation.MailingAddressSameAsBusiness = true;
            }

            foreach (var contactAddress in vendor.VendorAddresses)
            {
                foreach (var contact in contactAddress.Contacts)
                {
                    ContactRegistrationModel contactRegistrationModel = new ContactRegistrationModel()
                    {
                        ContactInformation = new ContactModel()
                        {
                            ID = contact.ID,
                            AddressID = contact.AddressID,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Position = contact.Position,
                            PhoneNumber = contact.PhoneNumber,
                            Ext = contact.Ext,
                            AlternatePhoneNumber = contact.AlternatePhoneNumber,
                            Email = contact.Email,
                            IsPrimary = (contact.IsPrimary.HasValue ? contact.IsPrimary.Value : false)
                        },

                        ContactAddress = new ContactAddressModel()
                        {
                            AddressLine1 = contact.Address.Address1,
                            AddressLine2 = contact.Address.Address2,
                            City = contact.Address.City,
                            Province = contact.Address.Province,
                            Postal = contact.Address.PostalCode,
                            Country = contact.Address.Country,
                        },
                        ContactAddressSameAsBusinessAddress = (applicationProcessorModel.BusinessLocation.BusinessAddress.ID == contact.AddressID)
                    };

                    applicationProcessorModel.ContactInformationList.Add(contactRegistrationModel);
                }
            }

            applicationProcessorModel.ContactInformationList = applicationProcessorModel.ContactInformationList.OrderBy(c => c.ContactInformation.ID).ToList();

            foreach (var sortYard in vendor.VendorStorageSites)
            {
                ProcessorSortYardDetailsRegistrationModel sortYardDetailsRegistrationModel = new ProcessorSortYardDetailsRegistrationModel()
                {
                    SortYardAddress = new SortYardAddressModel()
                    {
                        ID = sortYard.ID,
                        AddressLine1 = sortYard.Address.Address1,
                        AddressLine2 = sortYard.Address.Address2,
                        City = sortYard.Address.City,
                        Province = sortYard.Address.Province,
                        Postal = sortYard.Address.PostalCode,
                        Country = sortYard.Address.Country
                    },

                    CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                    MaxStorageCapacity = sortYard.MaxStorageCapacity.ToString(),
                    StorageSiteType = sortYard.StorageSiteType,
                };

                applicationProcessorModel.SortYardDetails.Add(sortYardDetailsRegistrationModel);
            }

            applicationProcessorModel.TireDetails = new ProcessorTireDetailsRegistrationModel();
            applicationProcessorModel.TireDetails.TireDetailsItemType = new List<ProcessorTireDetailsItemTypeModel>();

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            defaultItems.ToList().ForEach(s =>
            {
                bool isChecked = false;

                if (vendor.Items.Any(r => r.ID == s.ID))
                {
                    isChecked = true;
                }

                ProcessorTireDetailsItemTypeModel tireDetailsItemTypeModel = new ProcessorTireDetailsItemTypeModel()
                {
                    ID = s.ID,
                    Description = s.Description,
                    Name = s.Name,
                    ShortName = s.ShortName,
                    isChecked = isChecked,
                    IsActive = s.IsActive,
                    ItemTypeID = s.ItemType,
                };

                applicationProcessorModel.TireDetails.TireDetailsItemType.Add(tireDetailsItemTypeModel);
            });

            applicationProcessorModel.TireDetails.RegistrantSubTypeID = vendor.RegistrantSubTypeID;
            applicationProcessorModel.TireDetails.ProductsProduceType = new List<ProcessorTireDetailsItemTypeModel>();

            var productItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            productItems.ToList().ForEach(s =>
            {
                bool isChecked = false;
                if (vendor.Items.Any(r => r.ID == s.ID))
                {
                    isChecked = true;
                }

                ProcessorTireDetailsItemTypeModel tireDetailsItemTypeModel = new ProcessorTireDetailsItemTypeModel()
                {
                    ID = s.ID,
                    Description = s.Description,
                    Name = s.Name,
                    ShortName = s.ShortName,
                    isChecked = isChecked,
                    IsActive = s.IsActive,
                    ItemTypeID = s.ItemType,
                };
                applicationProcessorModel.TireDetails.ProductsProduceType.Add(tireDetailsItemTypeModel);
            });

            applicationProcessorModel.TireDetails.OtherProcessProduct = vendor.OtherProcessProduct;
            applicationProcessorModel.TireDetails.OtherRegistrantSubType = vendor.OtherRegistrantSubType;

            applicationProcessorModel.SupportingDocuments = new SupportingDocumentsRegistrationModel();
            foreach (var supportingDocument in vendor.VendorSupportingDocuments)
            {
                SupportingDocumentTypeEnum supportingDocumentType;
                if (Enum.TryParse(supportingDocument.TypeID.ToString(), out supportingDocumentType))
                {
                    switch (supportingDocumentType)
                    {
                        case SupportingDocumentTypeEnum.MBL:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionMBL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CLI:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCLI = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CFP:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCFP = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.HST:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionHST = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.WSIB:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionWSIB = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CHQ:
                            applicationProcessorModel.SupportingDocuments.SupportingDocumentsOptionCHQ = supportingDocument.Option;
                            break;
                        default:
                            break;
                    }
                }
            }

            var bankingInformation = vendor.BankInformations.FirstOrDefault();

            if (bankingInformation != null)
            {
                applicationProcessorModel.BankingInformation.BankName = bankingInformation.BankName;
                applicationProcessorModel.BankingInformation.TransitNumber = bankingInformation.TransitNumber;
                applicationProcessorModel.BankingInformation.BankNumber = bankingInformation.BankNumber;
                applicationProcessorModel.BankingInformation.AccountNumber = bankingInformation.AccountNumber;
                applicationProcessorModel.BankingInformation.EmailPaymentTransferNotification = bankingInformation.EmailPaymentTransferNotification;
                applicationProcessorModel.BankingInformation.NotifyToPrimaryContactEmail = bankingInformation.NotifyToPrimaryContactEmail;
            }

            return applicationProcessorModel;
        }

        #endregion Workflow
    }
}