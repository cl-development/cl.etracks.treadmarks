﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Logging;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Framework.DTO;
using Microsoft.Win32;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// A generic helper class for db CRUD operation
    /// </summary>
    public class EntityStore<TEntity, TKey> where TEntity : BaseDTO<TKey>
    {
        #region Fields
        private DbContext context;
        private DbSet<TEntity> entitySet;
        #endregion 

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public EntityStore(DbContext context)
        {
            this.context = context;
            entitySet = context.Set<TEntity>();
        }
        #endregion 

        #region Public Methods
        /// <summary>
        /// Get all data
        /// </summary>
        public IQueryable<TEntity> All
        {
            get { return entitySet.AsQueryable(); }
        }
        /// <summary>
        /// Get all data including all children objects
        /// </summary>
        /// <param name="includedProperties"></param>
        /// <returns></returns>
        public IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includedProperties)
        {
            var query = entitySet.AsQueryable();
            foreach (var includedProperty in includedProperties)
            {
                query = query.Include(includedProperty);
            }
            return query;
        }
        /// <summary>
        /// Find record by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity FindById(TKey id)
        {
            return entitySet.Find(id);
        }

        /// <summary>
        /// Insert a new record
        /// </summary>
        /// <param name="entity"></param>
        public void Insert(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Added;
            context.SaveChanges();
        }

        /// <summary>
        /// Upadte a record
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity)
        {
            if (entity.HasRowVersion)
            {
                //Handling concurrency conflicts
                try
                {
                    context.Entry(entity).State = EntityState.Modified; 
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    LogManager.LogException(ex);
                    throw new CLDbConcurrencyException("DbConcurrencyMessageCode");
                }             
            }
            else
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Insert or update object graph
        /// </summary>
        /// <param name="entityGraph"></param>
        public void InsertOrUpdateGraph(TEntity entityGraph)
        {
            if (entityGraph.State == State.Added)
            {
                entitySet.Add(entityGraph);
            }
            else
            {
                entitySet.Add(entityGraph);
                context.ApplyStateChanges();
            }
            context.SaveChanges();
        }

        /// <summary>
        /// Delete a record
        /// </summary>
        /// <param name="id"></param>
        public void Delete(TKey id)
        {
            var entity = entitySet.Find(id);
            entitySet.Remove(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Update a record with specified properties
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        public void Update(TEntity entity,params Expression<Func<TEntity, object>>[] properties)
        {
            foreach (var property in properties)
            {
                context.Entry(entity).Property(property).IsModified = true;
            }

            if (entity.HasRowVersion)
            {
                //Handling concurrency conflicts
                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    LogManager.LogException(ex);
                    throw new CLDbConcurrencyException("DbConcurrencyMessageCode");
                }
            }
            else
            {
                context.SaveChanges();
            }
        }
        #endregion 
    }
}
