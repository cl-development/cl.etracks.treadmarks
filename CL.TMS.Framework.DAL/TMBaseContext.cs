﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// Base DbContext for TreadMarks database
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class TMBaseContext<TContext> : DbContext where TContext : DbContext
    {
        /// <summary>
        /// Static constructor
        /// </summary>
        static TMBaseContext()
        {
            Database.SetInitializer<TContext>(null);          
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected TMBaseContext() : base("TMSDB")
        {

        }
    }
}
