﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// Base repository
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public abstract class BaseRepository<TContext, TEntity, TKey> : IDisposable
        where TEntity : BaseDTO<TKey>
        where TContext : DbContext, new()
    {
        #region Fields
        protected DbContext context;
        protected EntityStore<TEntity, TKey> entityStore;
        #endregion

        #region Constructors
        protected BaseRepository()
        {
            context = new TContext();
            entityStore = new EntityStore<TEntity, TKey>(context);
        }

        ~BaseRepository()
        {
            Dispose();
        }

        #endregion 

        #region Public Methods
        public void Dispose()
        {
            this.context.Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion 
    }

    public abstract class BaseRepository<TContext, TKey> : IDisposable
        where TContext : DbContext, new()
    {
        #region Fields
        protected DbContext context;
        #endregion

        #region Constructors
        protected BaseRepository()
        {
            context = new TContext();
        }

        ~BaseRepository()
        {
            Dispose();
        }

        #endregion

        #region Public Methods
        public void Dispose()
        {
            this.context.Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion
    }


}
