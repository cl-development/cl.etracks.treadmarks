﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// Base DbContext for MD database
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class MDBaseContext<TContext> : DbContext where TContext : DbContext
    {
        /// <summary>
        /// Static constractor
        /// </summary>
        static MDBaseContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        /// <summary>
        /// Constractor to read database connection string
        /// </summary>
        protected MDBaseContext() : base("MDDB")
        {

        }
    }
}
