﻿using CL.Framework.BLL;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules
{
    public class SubmitClaimRuleStrategyFactory
    {
        public static IBusinessRuleSetStrategy LoadSubmitClaimRuleStrategy()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    return new SubmitClaimRuleSetOTSStrategy();
                default:
                    return new SubmitClaimRuleSetOTSStrategy();
            }
        }
    }
}
