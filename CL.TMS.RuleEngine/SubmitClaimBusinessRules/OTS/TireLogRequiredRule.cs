﻿using CL.Framework.RuleEngine;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class TireLogRequiredRule : BaseBusinessRule
    {
        public static string ValidationMessage = "You have reported tire types other than PLT and AG/LS which require a tire log report document to be uploaded.";

        public TireLogRequiredRule() : base("AutoDismantlerRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            //
            var claim = target as Claim;

            if (string.Equals(claim.Participant.PrimaryBusinessActivity, "Auto dismantler", StringComparison.InvariantCultureIgnoreCase))
            {
                //if claim has reported items other than PLT and AGLS without a tire log trigger validation
                if (!claim.Attachments.Any())
                {
                    var claimsRepository = ruleContext["claimRepository"] as IClaimsRepository;
                    var items = DataLoader.Items;
                    var claimDetails = claimsRepository.LoadClaimDetails(claim.ID,items);
                    var tcrClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR).ToList();
                    var dotClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT).ToList();
                    var logEntryItems = DataLoader.TransactionItems.Where(p => p.ShortName == TreadMarksConstants.MT ||
                                                                                p.ShortName == TreadMarksConstants.IND ||
                                                                                p.ShortName == TreadMarksConstants.SOTR ||
                                                                                p.ShortName == TreadMarksConstants.MOTR ||
                                                                                p.ShortName == TreadMarksConstants.LOTR ||
                                                                                p.ShortName == TreadMarksConstants.GOTR
                                                                            ).Select(s => s.ID).Distinct();
                    if ((tcrClaimDetails.Any(i => i.TransactionItems.Where(t => logEntryItems.Contains(t.ItemID)).Any(q => q.Quantity > 0))
                        || dotClaimDetails.Any(i => i.TransactionItems.Where(t => logEntryItems.Contains(t.ItemID)).Any(q => q.Quantity > 0))))
                    {
                        var error = new ValidationFailure("TireLogRequiredRule", ValidationMessage);
                        error.ErrorCode = "TireLogRequiredRule";
                        validationResults.Errors.Add(error);
                    }
                }
            }
        }
    }
}
