﻿using CL.Framework.RuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class OTSSubmitClaimRuleSet : BaseRuleSet
    {
        public override sealed void Initialize()
        {
            RuleSet.Add(new PreviousClaimStillOpenRule());
            RuleSet.Add(new HaulerNilActivityClaimsRule());
            RuleSet.Add(new CollectorNilActivityClaimsRule());
            RuleSet.Add(new HaulerNegativeClosingInventoryRule());
            RuleSet.Add(new IncompleteAndSyncedRule());
            RuleSet.Add(new PendingStatusRule());
            RuleSet.Add(new LateClaimRule());
            RuleSet.Add(new ZeroDollarClaimsRule());
            RuleSet.Add(new CollectorInboundOutboundMatchClaimRule());
        }

        public OTSSubmitClaimRuleSet()
        {
            Initialize();
        }
    }
}
