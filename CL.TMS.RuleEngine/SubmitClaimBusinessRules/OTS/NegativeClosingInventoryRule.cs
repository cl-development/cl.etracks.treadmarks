﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class NegativeClosingInventoryRule : BaseBusinessRule
    {
        public NegativeClosingInventoryRule() : base("NegativeClosingInventoryRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool negativeClosingInventoryRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            var claimSummary = claimRepository.GetClaimSummaryByClaimId(claim.ID);
            decimal claimSummaryTotalClosing = 0;
            if (claim.ClaimSummary != null)
                claimSummaryTotalClosing = claim.ClaimSummary.TotalClosing ?? 0;

            var submitClaimModel = ruleContext["submitClaimModel"] as HaulerSubmitClaimViewModel;

            if (claimSummaryTotalClosing < 0)
                negativeClosingInventoryRule = true;

            if (negativeClosingInventoryRule)
            {
                var error = new ValidationFailure("NegativeClosingInventoryRule", "This claim has negative closing Inventory.");
                error.ErrorCode = "NegativeClosingInventoryRule";
                validationResults.Errors.Add(error);
            }
        }
    }
}
