﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Security.Implementations;
using FluentValidation.Results;

namespace CL.TMS.RuleEngine.PasswordBusinessRules.OTS
{
    public class PasswordCannotSameRule:BaseBusinessRule
    {
        public PasswordCannotSameRule() : base("PasswordCannotSameRule")
        {
            
        }
        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            var user = target as User;
            var password = ruleContext["password"] as string;
            if (PBKDF2PasswordHasher.Compare(password, user.Password))
            {
                var error = new ValidationFailure("PasswordCannotSame", "Password cannot be the same as the current password.");
                error.ErrorCode = "PasswordCannotSame";
                validationResults.Errors.Add(error);
            }
        }
    }
}
