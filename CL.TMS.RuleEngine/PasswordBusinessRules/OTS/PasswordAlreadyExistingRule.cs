﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Implementations;
using FluentValidation.Results;

namespace CL.TMS.RuleEngine.PasswordBusinessRules.OTS
{
    public class PasswordAlreadyExistingRule : BaseBusinessRule
    {
        public PasswordAlreadyExistingRule() : base("PasswordAlreadyExistingRule")
        {
            
        }
        public override void Execute<T>(T target,ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            var user = target as User;
            var userRepository = ruleContext["userRepository"] as IUserRepository;
            var password = ruleContext["password"] as string;
            var userArchivedHashedPasswordList = userRepository.GetUserHashedPasswordArchive(user.ID);
            var passwordAlreadyUsed = false;
            foreach (var archivedHashedPassword in userArchivedHashedPasswordList)
            {
                if (PBKDF2PasswordHasher.Compare(password, archivedHashedPassword))
                {
                    passwordAlreadyUsed = true;
                    break;
                }
            }
            if (passwordAlreadyUsed)
            {
                var error = new ValidationFailure("PasswordAlreadyUsed", "You cannot use the same password. Please choose a unique password that you have not used in the past.");
                error.ErrorCode = "PasswordAlreadyUsed";
                validationResults.Errors.Add(error);
            }
        }
    }
}
