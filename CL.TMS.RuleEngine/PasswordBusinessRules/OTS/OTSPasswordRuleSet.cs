﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.PasswordBusinessRules.OTS
{
    public class OTSPasswordRuleSet : BaseRuleSet
    {
        public override sealed void Initialize()
        {
            RuleSet.Add(new PasswordCannotSameRule());
            RuleSet.Add(new PasswordAlreadyExistingRule());
        }

        public OTSPasswordRuleSet()
        {
            Initialize();
        }
    }
}
