﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.BLL;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.InvitationBusinessRules.OTS;
using CL.TMS.RuleEngine.PasswordBusinessRules.OTS;

namespace CL.TMS.RuleEngine.PasswordBusinessRules
{
    public class PasswordRuleStrategyFactory
    {
        public static IBusinessRuleSetStrategy LoadPasswordRuleStrategy()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    return new PasswordRuleSetOTSStrategy();
                default:
                    return new PasswordRuleSetOTSStrategy();
            }
        }
    }
}
