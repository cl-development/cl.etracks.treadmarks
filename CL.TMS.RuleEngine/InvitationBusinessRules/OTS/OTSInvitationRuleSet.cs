﻿
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.InvitationBusinessRules.OTS
{
    public class OTSInvitationRuleSet : BaseRuleSet
    {
        public override sealed void Initialize()
        {
            base.RuleSet.Add(new InvitationBusinessRuleA());
        }

        public OTSInvitationRuleSet()
        {
            Initialize();
        }
    }
}
