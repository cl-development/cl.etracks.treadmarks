﻿using CL.Framework.RuleEngine;
using CL.TMS.RuleEngine.TransactionBusinessRules.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.TransactionBusinessRules.OTS
{
    public class OTSTransactionRuleSet: BaseRuleSet
    {
        public override sealed void Initialize()
        {
            RuleSet.Add(new PreviousScaleTicketNumberRule());
            RuleSet.Add(new PreviousScaleTicketMobileRule());
        }

        public OTSTransactionRuleSet()
        {
            Initialize();
        }
    }
}
