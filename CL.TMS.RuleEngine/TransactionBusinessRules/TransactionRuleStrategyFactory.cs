﻿using CL.Framework.BLL;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.TransactionBusinessRules.OTS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.TransactionBusinessRules
{
    public class TransactionRuleStrategyFactory
    {
        public static IBusinessRuleSetStrategy LoadTransactionRuleStrategy()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    return new TransactionRuleSetOTSStrategy();
                default:
                    return new TransactionRuleSetOTSStrategy();
            }
        } 
    }
}
