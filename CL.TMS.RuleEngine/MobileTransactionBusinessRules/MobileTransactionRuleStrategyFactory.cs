﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.BLL;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.MobileTransactionBusinessRules.OTS;

namespace CL.TMS.RuleEngine.MobileTransactionBusinessRules
{
    public class MobileTransactionRuleStrategyFactory
    {
        public static IBusinessRuleSetStrategy LoadMobileTransactionRuleStrategy()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    return new MobileTransactionRuleSetOTSStrategy();
                default:
                    return new MobileTransactionRuleSetOTSStrategy();
            }
        }
    }
}
