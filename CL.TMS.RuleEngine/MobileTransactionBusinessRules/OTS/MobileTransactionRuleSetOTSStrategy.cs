﻿using CL.Framework.BLL;
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.MobileTransactionBusinessRules.OTS
{
    public class MobileTransactionRuleSetOTSStrategy:IBusinessRuleSetStrategy
    {
        public BaseRuleSet CreateBusinessRuleSet()
        {
            return new OTSMobileTransactionRuleSet();
        }
    }
}
