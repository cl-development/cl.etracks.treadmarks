﻿
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.MobileTransactionBusinessRules.OTS
{
    public class OTSMobileTransactionRuleSet : BaseRuleSet
    {
        public override sealed void Initialize()
        {
            base.RuleSet.Add(new MaxTireCountRule());
        }

        public OTSMobileTransactionRuleSet()
        {
            Initialize();
        }
    }
}
