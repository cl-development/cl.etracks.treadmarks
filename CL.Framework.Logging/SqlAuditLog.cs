﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    [Table("SqlAuditLog")]
    public class SqlAuditLog
    {
        public int Id { get; set; }
        public string SqlStatement { get; set; }
        public string SqlParameters { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
