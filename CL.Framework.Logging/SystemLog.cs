﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    [Table("SystemLog")]
    public class SystemLog
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
