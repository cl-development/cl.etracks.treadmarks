﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging.Implementation
{
    public class LogRepository
    {
        public void LogMessage(SystemLog systemLog)
        {
            using (var dbContext = new LogDbContext())
            {
                dbContext.SystemLogs.Add(systemLog);
                dbContext.SaveChanges();
            }
        }

        public void LogSqlAuditLog(SqlAuditLog sqlAuditLog)
        {
            using (var dbContext = new LogDbContext())
            {
                dbContext.SqlAuditLogs.Add(sqlAuditLog);
                dbContext.SaveChanges();
            }

        }
    }
}
