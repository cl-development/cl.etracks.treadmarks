﻿using CL.Framework.Logging.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging.Providers
{
    public class DBLogger : ILogger
    {
        private LogRepository logRepository;
        public DBLogger()
        {
            logRepository = new LogRepository();
        }
        public void WriteLogEntry(SystemLog systemLog)
        {
            logRepository.LogMessage(systemLog);
        }

        public void WriteSqlAuditEntry(SqlAuditLog sqlAuditLog)
        {
            logRepository.LogSqlAuditLog(sqlAuditLog);
        }
    }
}
