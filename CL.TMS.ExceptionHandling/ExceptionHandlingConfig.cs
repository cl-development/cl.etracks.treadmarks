﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ExceptionHandling.Exceptions;

namespace CL.TMS.ExceptionHandling
{
    /// <summary>
    /// Exception handling configuration
    /// </summary>
    public class ExceptionHandlingConfig
    {
        #region Fields
        public const string ServicePolicy = "Service Policy";
        public const string UIPolicy = "UI Policy";
        #endregion 

        #region Public Methods
        /// <summary>
        /// Define exception policies used in Application
        /// </summary>
        public static void BootstrapExceptionHandling()
        {
            //UI Policy
            var uiPolicies = new List<ExceptionPolicyEntry>
            {                    
                new ExceptionPolicyEntry(
                    typeof(Exception),
                    PostHandlingAction.None,
                    new IExceptionHandler[]
                    { new CLLoggingExceptionHandler()})
            };

            //Service Policy
            var servicePolicies = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(
                    typeof (CLDbConcurrencyException),
                    PostHandlingAction.NotifyRethrow,
                    new IExceptionHandler[]
                    { new CLLoggingExceptionHandler()}
                    ),

                new ExceptionPolicyEntry(
                    typeof (CLValidaitonException),
                    PostHandlingAction.NotifyRethrow,
                    new IExceptionHandler[]
                    { new CLLoggingExceptionHandler()}
                    ),

                new ExceptionPolicyEntry(
                    typeof (SecurityException),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                    {
                        new CLLoggingExceptionHandler(),
                        new ReplaceHandler("Unauthorized Access", typeof(SecurityException))
                    }
                   ),
               new ExceptionPolicyEntry(
                    typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                    {
                        new CLLoggingExceptionHandler(),
                        new ReplaceHandler("Application Error. Please contact your administrator with support ID: {handlingInstanceID}", typeof(Exception))
                    }
                   )
            };

            var policies = new List<ExceptionPolicyDefinition>
            {
                new ExceptionPolicyDefinition(UIPolicy, uiPolicies),
                new ExceptionPolicyDefinition(ServicePolicy, servicePolicies)
            };

            ExceptionPolicy.SetExceptionManager(new ExceptionManager(policies));
        }
        #endregion 
    }
}
