﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common.UserInterface;

namespace CL.TMS.UI.Common.Extensions
{
    public class CSVResult<T>: FileResult
    {
        private List<string> columns;
        private List<Func<T,string>> funcs;
        private List<T> data;
        public CSVResult(List<string> columns, List<Func<T, string>> funcs, List<T> data, string fileName):base("text/csv")
        {
            this.columns = columns;
            this.funcs = funcs;
            this.data = data;
            FileDownloadName = fileName;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            var result = data.ToCSV(",", columns.ToArray(), funcs.ToArray());
            response.Write(result);
        }
    }
}
