﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CL.TMS.Common.Converters;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.DataContracts.ViewModel.Hauler;

namespace CL.TMS.UI.Common
{
    /// <summary>
    /// AutoMapper configuration
    /// </summary>
    public class AutoMapperConfig
    {
        /// <summary>
        /// Register all mappings required by AutoMapper
        /// </summary>
        public static void RegisterMappings()
        {
            Mapper.CreateMap<DateTime?, DateTime>().ConvertUsing<DateTimeConverter>();
            Mapper.CreateMap<DateTime?, DateTime?>().ConvertUsing<NullableDateTimeConverter>();
            //It will be used during entity projection
            Mapper.CreateMap<User, AuthenticationUser>()
            .ForMember(c => c.PasswordExpirationDate, opt => opt.NullSubstitute(default(DateTime)))
            .ForMember(c => c.LastLoginDate, opt => opt.NullSubstitute(default(DateTime)))
            .ForMember(c => c.Rowversion, opt => opt.Ignore())
            .ForMember(c => c.State, opt => opt.Ignore());

            Mapper.CreateMap<AttachmentModel, Attachment>();
            Mapper.CreateMap<Attachment, AttachmentModel>();
            Mapper.CreateMap<BankInformation, BankingInformationRegistrationModel>();
           
        }
    }
}
