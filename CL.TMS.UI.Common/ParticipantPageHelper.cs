﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using System.Web;

namespace CL.TMS.UI.Common
{
    public static class ParticipantPageHelper
    {
        /// <summary>
        /// Set current 
        /// </summary>
        /// <param name="registrantService"></param>
        public static void SetGlobalRegistrationNumber(IRegistrantService registrantService)
        {
            if (SecurityContextHelper.CurrentVendor == null)
            {
                var userVendorClaims = SecurityContextHelper.UserVendorClaims;
                if ((userVendorClaims != null) && (userVendorClaims.Count == 1))
                {
                    //Load vendor from database
                    var userVendorclaim = userVendorClaims.First();
                    var vendorId = Convert.ToInt32(userVendorclaim.Value);
                    var vendor = registrantService.GetVendorById(vendorId);
                    HttpContext.Current.Session["SelectedVendor"] = vendor;
                }
            }
        }

        public static void SetCurrentVendor(IRegistrantService registrantService, int vendorId)
        {
            var vendor = registrantService.GetVendorById(vendorId);
            HttpContext.Current.Session["SelectedVendor"] = vendor;
        }
    }
}
