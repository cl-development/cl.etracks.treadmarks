﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.Framework.Logging;
using CL.TMS.ExceptionHandling;
using System.Web.Routing;

namespace CL.TMS.UI.Common.Logging
{
    public class GlobalExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception as HttpAntiForgeryException;
            if (exception != null)
            {
                var routeValues = new RouteValueDictionary();
                routeValues["controller"] = "Account";
                routeValues["action"] = "Login";
                filterContext.Result = new RedirectToRouteResult(routeValues);
                filterContext.ExceptionHandled = true;
            }

            if (!filterContext.ExceptionHandled)
            {
                //Handle the client side excpetion
                ExceptionHandlingHelper.HandlingClientException(filterContext.Exception);
            }
        }
    }
}
