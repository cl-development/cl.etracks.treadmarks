﻿using CL.TMS.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.UI.Common.Helper
{
    /// <summary>
    /// Using Singleton Design Pattern to implement a singleton SiteSettings
    /// </summary>
    public class SiteSettings
    {
        private static readonly Lazy<SiteSettings> lazy = new Lazy<SiteSettings>(() => new SiteSettings());

        private IAppService systemService;
        private SiteSettings()
        {
            systemService = IoCContainer.GetService<IAppService>();
        }

        public static SiteSettings Instance
        {
            get { return lazy.Value; }
        }
        public string DomainName
        {
            get { return systemService.GetSettingValue("Settings.DomainName"); }
        }

        public string PasswordResetAbsolutePath
        {
            get { return systemService.GetSettingValue("Settings.PasswordResetAbsolutePath"); }
        }


        public string ForgotPasswordEmailMessage
        {
            get { return systemService.GetSettingValue("Settings.ForgotPasswordEmailMessage"); }
        }


        public string DefaultFromEmailAddress
        {
            get { return systemService.GetSettingValue("Company.Email"); }
        }
        public string GetSettingValue(string key)
        {
            return systemService.GetSettingValue(key);
        }

        public string FileUploadRepositoryPath
        {
            get { return systemService.GetSettingValue("Settings.FileUploadRepositoryPath"); }
        }

        public string FileUploadPreviewDomainName
        {
            get { return systemService.GetSettingValue("Settings.FileUploadPreviewDomainName"); }
        }

        public string EmailSmtpPort
        {
            get { return systemService.GetSettingValue("Email.smtpPort"); }
        }
        public string EmailSmtpServer
        {
            get { return systemService.GetSettingValue("Email.smtpServer"); }
        }
        public string EmailSmtpUserName
        {
            get { return systemService.GetSettingValue("Email.smtpUserName"); }
        }
        public string EmailSmtpPassword
        {
            get { return systemService.GetSettingValue("Email.smtpPassword"); }
        }
        public string EmailDataElementBegin
        {
            get { return systemService.GetSettingValue("Email.dataElementBegin"); }
        }
        public string EmailDataElementEnd
        {
            get { return systemService.GetSettingValue("Email.dataElementEnd"); }
        }
        public string EmailDefaultEmailTemplateDirectory
        {
            get { return systemService.GetSettingValue("Email.defaultEmailTemplateDirectory"); }
        }
        public string EmailDefaultFrom
        {
            get { return systemService.GetSettingValue("Company.Email"); }
        }
        public string EmailUseSSL
        {
            get { return systemService.GetSettingValue("Email.useSSL"); }
        }
        public string PasswordResetEmailTemplLocation
        {
            get { return systemService.GetSettingValue("Email.PasswordResetEmailTemplLocation"); }
        }
        public string ApprovedHaulerApplicationEmailTemplLocation
        {
            get { return systemService.GetSettingValue("Email.ApprovedHaulerApplicationEmailTemplLocation"); }
        }
        public string DenyHaulerApplicationEmailTemplLocation
        {
            get { return systemService.GetSettingValue("Email.DenyHaulerApplicationEmailTemplLocation"); }
        }
        public string RejectBankinfoEmailTemplLocation
        {
            get { return systemService.GetSettingValue("Email.RejectBankinfoEmailTemplLocation"); }
        }
        public string CompanyLogo1FileLocation
        {
            get { return systemService.GetSettingValue("Site.CompanyLogo1FileLocation"); }
        }
        public string CompanyLogo2FileLocation
        {
            get { return systemService.GetSettingValue("Site.CompanyLogo2FileLocation"); }
        }
        public string ApplicationLogoFileLocation
        {
            get { return systemService.GetSettingValue("Settings.ApplicationLogoFileLocation"); }
        }
        public string TwitterImageLocation
        {
            get { return systemService.GetSettingValue("Site.TwitterImageLocation"); }
        }
        public string TwitterURL
        {
            get { return systemService.GetSettingValue("Company.TwitterURL"); }
        }
        public string HaulerApplicationWelcomeMessage
        {
            get { return systemService.GetSettingValue("Application.HaulerWelcomeMessage"); }
        }

        public string SiteStewardType
        {
            get { return systemService.GetSettingValue("Site.StewardType"); }
        }
        public string ApplicationInstance
        {
            get { return systemService.GetSettingValue("Site.ApplicationInstance"); }
        }
    }
}
