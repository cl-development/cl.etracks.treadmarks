﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CL.TMS.UI.Common.Helper
{
    public class GetUrlUI
    {
        public static UriBuilder GetUrl(string actionName, string controllerName)
        {
            Uri uri = HttpContext.Current.Request.Url;
            UriBuilder url = new UriBuilder(uri.Host);
            url.Scheme = "http";
            url.Path = (new UrlHelper(HttpContext.Current.Request.RequestContext)).Action(actionName, controllerName);
            url.Port = (uri.Port == 80) ? -1 : uri.Port;
            return url;
        }
    }
}
