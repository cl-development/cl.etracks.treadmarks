﻿using CL.TMS.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.UI.Common.Helper
{
    public static class EmailContentHelper
    {        
        private static IEmailSettingsService emailSettingsService = IoCContainer.GetService<IEmailSettingsService>();
        
        public static string GetSubjectByName(string emailName)
        {
            return emailSettingsService.GetEmailByName(emailName).Subject;
        }
        public static string GetCompleteEmailByName(string emailName)
        {
            return emailSettingsService.GetCompleteEmailByName(emailName);
        }
        public static bool IsEmailEnabled(string emailName)
        {
            return emailSettingsService.IsEmailEnabled(emailName);
        }
        public static void Reset()
        {
            emailSettingsService = IoCContainer.GetService<IEmailSettingsService>();
        }
    }
}
