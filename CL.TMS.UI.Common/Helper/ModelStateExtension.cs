﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CL.TMS.UI.Common.Helper
{
    public static class ModelStateExtension
    {
        public static void RemoveDuplicateValidationErrorMessage(this ModelStateDictionary modelStateDictionary)
        {
            foreach (ModelState modelState in modelStateDictionary.Values)
            {
                for (int i = 0; i < modelState.Errors.Count; i++)
                {
                    for (int x = (i + 1); x < modelState.Errors.Count; x++)
                    {
                        if (modelState.Errors[i].ErrorMessage == modelState.Errors[x].ErrorMessage)
                        {
                            modelState.Errors.RemoveAt(x);
                        }
                    }
                }
            }
        }
    }
}
