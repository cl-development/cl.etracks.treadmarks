﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.UI.Common.Helper
{
    public static class DatetTimeExtension
    {
        /// <summary>
        /// Convert UTC time to local time
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="timeZoneInfo"></param>
        /// <returns></returns>
        public static DateTime ToLocalTime(this DateTime dt, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dt, timeZoneInfo);
        }

        /// <summary>
        /// Convert to UTC time from local time
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="timeZoneInfo"></param>
        /// <returns></returns>
        public static DateTime ToUTCTime(this DateTime dt, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeToUtc(dt, timeZoneInfo);
        }
    }
}
