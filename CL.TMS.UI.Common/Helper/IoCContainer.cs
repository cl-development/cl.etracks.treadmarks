﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CL.TMS.UI.Common.Helper
{
    public static class IoCContainer
    {
        public static T GetService<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }         
    }
}
