﻿using CL.TMS.PDFGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CL.TMS.PDFGenerator;

namespace CL.TMS.UI.Common.Helper
{
    public static class IPadQRCodeLabelGenerator
    {
        public static void DeviceLabel(DeviceLabelModel deviceModel)
        {
            string templateDir = @"~/Templates/pdf/DeviceLabels.pdf";
            string path = HttpContext.Current.Server.MapPath(templateDir);
            DeviceLabels pdf = new DeviceLabels(path);
            pdf.GenerateSingleDeviceLabel(deviceModel);
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("inline;filename=\"{0}{1}.pdf\"", "DeviceLabel", deviceModel.DeviceID));
            HttpContext.Current.Response.BinaryWrite(pdf.GetBytes());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public static void DeviceLabelCollection(List<DeviceLabelModel> deviceModelCollection)
        {
            string templateDir = @"~/Templates/pdf/DeviceLabels.pdf";
            string path = HttpContext.Current.Server.MapPath(templateDir);
            DeviceLabels pdf = new DeviceLabels(path);
            pdf.GenerateDeviceLabels(deviceModelCollection);
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("inline;filename=\"{0}.pdf\"", "DeviceLabelCollection"));
            HttpContext.Current.Response.BinaryWrite(pdf.GetBytes());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public static void SingleSign(SignModel signModel)
        {
            string templateDir = GetSignTemplate(signModel.RegistrationNumber.Substring(0, 1));
            string path = HttpContext.Current.Server.MapPath(templateDir);
            Signs pdf = new Signs(path);
            pdf.GenerateSingleSign(signModel);
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("inline;filename=\"{0}{1}.pdf\"", "SingleSign", signModel.RegistrationNumber));
            HttpContext.Current.Response.BinaryWrite(pdf.GetBytes());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        private static string GetSignTemplate(string vendorType)
        {
            string templateDir;
            switch (vendorType)
            {
                case "2":
                    templateDir = @"~/Templates/pdf/Signs_Collector.pdf";
                    break;
                case "3":
                    templateDir = @"~/Templates/pdf/Signs_Hauler.pdf";
                    break;
                case "4":
                    templateDir = @"~/Templates/pdf/Signs_Processor.pdf";
                    break;
                default:
                    templateDir = @"~/Templates/pdf/Signs_Collector.pdf";
                    break;
            }
            return templateDir;
        }

        public static void SingleBadge(BadgeModel badgeModel)
        {
            string templateDir = GetBadgeTemplate(badgeModel.RegistrationNumber.Substring(0, 1));
            string path = HttpContext.Current.Server.MapPath(templateDir);
            Badges pdf = new Badges(path);
            pdf.GenerateSingleBadge(badgeModel);
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("inline;filename=\"{0}{1}.pdf\"", "SingleBadge", badgeModel.RegistrationNumber));
            HttpContext.Current.Response.BinaryWrite(pdf.GetBytes());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        private static string GetBadgeTemplate(string vendorType)
        {
            string templateDir;
            switch (vendorType)
            {
                case "2":
                    templateDir = @"~/Templates/pdf/Badges_Collector.pdf";
                    break;
                case "3":
                    templateDir = @"~/Templates/pdf/Badges_Hauler.pdf";
                    break;
                case "4":
                    templateDir = @"~/Templates/pdf/Badges_Processor.pdf";
                    break;
                default:
                    templateDir = @"~/Templates/pdf/Badges_Hauler.pdf";
                    break;
            }
            return templateDir;
        }

        public static void BadgeCollection(List<BadgeModel> badgeModelCollection)
        {
            string templateDir = GetBadgeTemplate(badgeModelCollection[0].RegistrationNumber.Substring(0, 1));
            string path = HttpContext.Current.Server.MapPath(templateDir);
            Badges pdf = new Badges(path);
            //var collection = badgeModelCollection.OrderBy(r => r.UserID).ToList();
            //pdf.GenerateBadges(collection);
            pdf.GenerateBadges(badgeModelCollection);            
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("inline;filename=\"{0}.pdf\"", "BadgeCollection"));
            HttpContext.Current.Response.BinaryWrite(pdf.GetBytes());
            HttpContext.Current.Response.Flush();
            pdf = null;
            HttpContext.Current.Response.End();
        }
    }
}
