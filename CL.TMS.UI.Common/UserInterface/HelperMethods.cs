﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using CL.TMS.UI.Common.Helper;
using CL.TMS.Common.Enum;
using System.Web.UI.WebControls;

namespace CL.TMS.UI.Common.UserInterface
{
    /// <summary>
    /// Helper method
    /// </summary>
    public static class HelperMethods
    {
        /// <summary>
        /// Get url by action name and controller name
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static UriBuilder GetUrl(string actionName, string controllerName)
        {
            Uri uri = HttpContext.Current.Request.Url;
            UriBuilder url = new UriBuilder(uri.Host);
            url.Scheme = "http";
            url.Path = (new UrlHelper(HttpContext.Current.Request.RequestContext)).Action(actionName, controllerName);
            url.Port = (uri.Port == 80) ? -1 : uri.Port;
            return url;
        }

        /// <summary>
        /// Html helper to create application type message
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="applicationType"></param>
        /// <returns></returns>
        public static MvcHtmlString ApplicationMessageBlock(this HtmlHelper htmlHelper, ApplicationTypes applicationType)
        {
            string message = null;
            switch (applicationType)
            {
                case ApplicationTypes.Hauler:
                    message = SiteSettings.Instance.HaulerApplicationWelcomeMessage;
                    break;
            }

            return MvcHtmlString.Create(message);
        }

        /// <summary>
        /// Html helper to create applicatio submission string
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="submittedOnDateTime"></param>
        /// <param name="company"></param>
        /// <param name="submittedBy"></param>
        /// <param name="address"></param>
        /// <param name="email"></param>
        /// <param name="ipAddress"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static MvcHtmlString ApplicationSubmissionInfo(this HtmlHelper htmlHelper, Nullable<DateTime> submittedOnDateTime, string company, string submittedBy, string address, string email, string ipAddress, string status, string approvedDate)
        {
            string html = string.Format(

                "<div class='panel-heading' role='tab' id='panelName-SubmissionInformationHeading'>" +
                   " <h4 class='panel-title'>" +
                        "<a data-toggle='collapse' data-parent='#accordion' href='#panelName-SubmissionInformation' aria-expanded='true' aria-controls='panelName-SubmissionInformation' class='panel-arrow-collapse collapsed'>" +
                            "Submission Information" +
                        "</a>" +
                    "</h4>" +
                "</div>" +

               "<div id='panelName-SubmissionInformation' class='panel-collapse collapse' role='tabpanel' aria-labelledby='panelName-SubmissionInformationHeading' style='height: 0px;'>" +
                "<div class='panel-body'>" +
                  "<div class='row'>" +
                    "<div class='col-md-12'>" +
                     "<table class='table table-hover' style='margin-right:5px;'>" +
                       "<thead>" +
                         "<tr>" +
                           "<th>Date &amp; Time</th>" +
                             "<th>Company</th>" +
                             "<th>Submitted By</th>" +
                             "<th>Address</th>" +
                             "<th>Email</th>" +
                             "<th>IP Address</th>" +
                             "<th>Status</th>" +
                             "<th>Approved Date</th>" +
                          "</tr>" +
                     "</thead>" +
                   "<tbody>" +
                     "<tr>" +
                           "<td>{0}</td>" +
                           "<td>{1}</td>" +
                           "<td>{2}</td>" +
                           "<td>{3}</td>" +
                           "<td>{4}</td>" +
                           "<td>{5}</td>" +
                           "<td>{6}</td>" +
                           "<td>{7}</td>" +
                       "</tr>" +
                   "</tbody>" +
                   "</table>" +
                 "</div>" +
                "</div>" +
               "</div>" +
              "</div>",
                    (submittedOnDateTime.HasValue && submittedOnDateTime.Value != DateTime.MinValue ? submittedOnDateTime.Value.ToString("ddd, yyyy-MM-dd @ h:mm tt").ToUpper() : string.Empty),
                    company,
                    submittedBy,
                    address,
                    email,
                    ipAddress,
                    status,
                    approvedDate
                   );

            return MvcHtmlString.Create(html);
        }

        #region HtmlHelper Overloads

        public static MvcHtmlString GetPropertyName<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression
        )
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
            return MvcHtmlString.Create(value);

        }


        public static string GetExpressionText<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression
        )
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
            return ExpressionHelper.GetExpressionText(expression);
            //return MvcHtmlString.Create(value);

        }

        public static MvcHtmlString GetPropertyValue<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var value = ModelMetadata.FromLambdaExpression(
                expression, htmlHelper.ViewData
            ).Model;
            return MvcHtmlString.Create(value.ToString());
        }


        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText = "", string height = "", string width = "", string style = "", string cssClass = "")
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttribute("height", height);
            builder.MergeAttribute("width", width);
            builder.MergeAttribute("style", style);
            builder.MergeAttribute("class", cssClass);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string src, IDictionary<string, string> attrs)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            foreach (KeyValuePair<string, string> attr in attrs)
            {
                builder.MergeAttribute(attr.Key, attr.Value);
            }
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        //public static MvcHtmlString TextBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, string name, object value, object htmlAttributes, bool disabled)
        //{
        //    var attributes = new RouteValueDictionary(htmlAttributes);
        //    if (disabled)
        //    {
        //        attributes["disabled"] = "disabled";
        //    }
        //    return htmlHelper.TextBox(name, value, htmlAttributes);
        //}

        public static MvcHtmlString TextBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, string name, object value, object htmlAttributes, bool disabled)
        {
            var attributes = new RouteValueDictionary(htmlAttributes);
            if (disabled)
            {
                attributes["disabled"] = "disabled";
            }
            return htmlHelper.TextBox(name, value, htmlAttributes);
        }
        public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList, string optionalLabel, object htmlAttributes, bool disabled)
        {
            var attributes = new RouteValueDictionary(htmlAttributes);
            if (disabled)
            {
                attributes["disabled"] = "disabled";
            }
            attributes = ConvertUnderscoresToDashes(attributes);
            return htmlHelper.DropDownListFor(expression, selectList, optionalLabel, attributes);
        }

        /// <summary>
        /// takes the "underscores" in Keys and converts them to "dashes" as per MVC behaviour 
        /// </summary>
        /// <param name="attributes"></param>
        private static RouteValueDictionary ConvertUnderscoresToDashes(RouteValueDictionary attributes)
        {
            if (attributes == null)
                return null;
            List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();
            foreach (var attr in (attributes))
            {
                if (attr.Key.Contains("_"))
                {
                    list.Add(new KeyValuePair<string, object>(attr.Key.ToString().Replace("_", "-"), attr.Value));
                }
            }
            foreach (var attr in list)
            {
                attributes.Add(attr.Key, attr.Value);
                string removeKey = attr.Key.ToString().Replace("-", "_");
                if (attributes.ContainsKey(removeKey))
                    attributes.Remove(removeKey);
            }
            return attributes;
        }



        public static IHtmlString TextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, bool disabled)
        {
            RouteValueDictionary attributesOrig = new RouteValueDictionary(htmlAttributes);
            RouteValueDictionary attributes = new RouteValueDictionary();
            foreach (var item in attributesOrig)
            {
                attributes.Add(item.Key.ToString().Replace("_", "-"), item.Value);
            }
            if (disabled)
            {
                attributes["disabled"] = "disabled";
            }
            return htmlHelper.TextBoxFor(expression, attributes);
        }

        public static RouteValueDictionary ConditionalDisable(bool disabled, object htmlAttributes = null)
        {
            var dictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (disabled)
                dictionary.Add("disabled", "disabled");

            return dictionary;
        }

        public static IHtmlString RadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value, object htmlAttributes, bool disabled)
        {

            RouteValueDictionary attributesOrig = new RouteValueDictionary(htmlAttributes);
            RouteValueDictionary attributes = new RouteValueDictionary();
            foreach (var item in attributesOrig)
            {
                attributes.Add(item.Key.ToString().Replace("_", "-"), item.Value);
            }
            if (disabled)
            {
                attributes["disabled"] = "disabled";
            }
            return htmlHelper.RadioButtonFor(expression, value, attributes);
        }


        #endregion

        #region Custom extension OTSTM2-782

        public static MvcHtmlString Button(object htmlAttributes, bool disabled, string buttonText)
        {
            var dictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (disabled)
                dictionary.Add("disabled", "disabled");

            return Button(dictionary, buttonText);
        }

        public static MvcHtmlString Button(IDictionary<string, object> htmlAttributes, string buttonText)
        {
            var builder = new TagBuilder("button");
            builder.Attributes.Add("type", "button");
            builder.InnerHtml = buttonText; 

            builder.MergeAttributes(htmlAttributes);
            return MvcHtmlString.Create(builder.ToString());
        }

        public static IHtmlString TextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string format, object htmlAttributes, bool disabled)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (disabled)
                attributes.Add("disabled", "disabled");

            return htmlHelper.TextBoxFor(expression, format, attributes);
        }

        public static MvcHtmlString CustomPartial<T>(this HtmlHelper htmlHelper, string partialViewName, object model, T partialViewData, string key = null)
        {
            var value = partialViewData;
                
            var partialData = new ViewDataDictionary 
            { 
                new KeyValuePair<string, object>(key, value)
            };

            return htmlHelper.Partial(partialViewName, model, partialData);
        }

        #endregion

    }
}
