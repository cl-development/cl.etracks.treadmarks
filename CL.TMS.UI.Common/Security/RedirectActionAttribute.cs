﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CL.TMS.Security.Authorization;

namespace CL.TMS.UI.Common.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class RedirectActionAttribute : ActionFilterAttribute
    {

        private readonly bool redirectCheck;
        public RedirectActionAttribute()
        {
            redirectCheck = true;
        }

        public RedirectActionAttribute(bool redirectCheck)
        {
            this.redirectCheck = redirectCheck;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (redirectCheck)
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    if (!SecurityContextHelper.IsStaff())
                    {
                        //Participant User
                        if (SecurityContextHelper.CurrentVendor != null)
                        {
                            if (!string.IsNullOrWhiteSpace(SecurityContextHelper.CurrentVendor.RedirectUrl))
                            {
                                filterContext.Result = new RedirectResult(SecurityContextHelper.CurrentVendor.RedirectUrl);
                            }
                        }
                        else
                        {
                            filterContext.Result = new RedirectResult("~/System/Common/EmptyPage");
                        }
                    }
                    else
                    {
                        //Staff User
                        RedirectUser(filterContext);
                    }
                }
            }
        }

        private static void RedirectUser(ActionExecutingContext filterContext)
        {
            var redirectUrl = ResourceActionAuthorizationHandler.GetResourceValue("RedirectUserAfterLoginUrl");

            if (!string.IsNullOrEmpty(redirectUrl))
            {
                filterContext.Result = new RedirectResult(redirectUrl);
            }
        }
    }
}
