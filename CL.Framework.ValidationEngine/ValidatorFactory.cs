﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.ValidationEngine
{
    /// <summary>
    /// Validator factory
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class ValidatorFactory<T> where T : class
    {
        /// <summary>
        /// Create a validator
        /// </summary>
        /// <returns></returns>
        public static IValidator<T> GetValidator()
        {
            return new VABValidator<T>();
        }
    }
}
