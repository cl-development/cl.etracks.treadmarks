﻿using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.ValidationEngine
{
    public static class ValidationHelper
    {
        public static ValidationResults Validate<T>(T dataEntity, params string[] rulesets) where T : class
        {
            var validator = ValidatorFactory<T>.GetValidator();
            var validationResults = validator.Validate(dataEntity, rulesets);
            return validationResults;
        }
    }
}
