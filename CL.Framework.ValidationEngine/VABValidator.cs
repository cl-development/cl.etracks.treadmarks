﻿using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.ValidationEngine
{
    /// <summary>
    /// Concrete validator object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class VABValidator<T> : IValidator<T> where T : class
    {

        #region IValidator<T> Members

        /// <summary>
        /// Validate object
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public ValidationResults Validate(T target)
        {
            return Validation.Validate<T>(target);
        }
        /// <summary>
        /// validate object based on rule set
        /// </summary>
        /// <param name="target"></param>
        /// <param name="rulesets"></param>
        /// <returns></returns>
        public ValidationResults Validate(T target, params string[] rulesets)
        {
            return Validation.Validate<T>(target, rulesets);
        }

        #endregion
    }
}
