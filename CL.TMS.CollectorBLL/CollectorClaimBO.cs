﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.IRepository.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.System;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.TMS.Security.Authorization;
using System.Configuration;
using CL.TMS.Security;

namespace CL.TMS.CollectorBLL
{
    public class CollectorClaimBO : BaseBO
    {
        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        
        public CollectorClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;        
        }
        public CollectorClaimSummaryModel LoadCollectorClaimSummary(int claimId)
        {
            var collectorClaimSummary = new CollectorClaimSummaryModel();

            //Load status
            var claim = claimsRepository.GetClaimWithSummaryPeriod(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            collectorClaimSummary.ClaimCommonModel.Status = status;
            if ((status != ClaimStatus.Open) || (SecurityContextHelper.IsReadOnly()))
            {
                collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
            }

            //Load Claim Period
            collectorClaimSummary.ClaimCommonModel.ClaimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (collectorClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;

            //Load Tire Origin
            var tireOrigins = claimsRepository.LoadClaimTireOrigins(claimId);
            PopulateInbound(collectorClaimSummary, tireOrigins);

            //Load Claim Supporting Documents
            var claimSupportingDocuments = claimsRepository.LoadClaimSupportingDocuments(claimId);
            PopulateSupportingDocumentsSelection(collectorClaimSummary, claimSupportingDocuments);

            //Load outbound
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);

            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateOutbound(collectorClaimSummary, claimDetails, inventoryAdjustments);

            //OTSTM2-588
            PopulateAdjustment(collectorClaimSummary, claimDetails, inventoryAdjustments);

            //Load reuse tires
            var reuesTires = claimsRepository.LoadClaimReuseTires(claimId);
            PopulateRuseTires(collectorClaimSummary, reuesTires);

            //Load payment and adjustment
            //OTSTM2-1139 No talk back needed
            //UpdateGpBatchForClaim(claimId);

            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    collectorClaimSummary.CollectorPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(collectorClaimSummary.CollectorPayment, claimPaymentSummay, claimPayments, claimPaymentAdjustments, claim);

            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                collectorClaimSummary.CollectorPayment.HSTBase = 0.00m;
            }
            PopulatAllowancePaymentDetail(collectorClaimSummary, claim.ClaimPeriodId);
            collectorClaimSummary.CollectorPayment.PaymentDueDate = claim.ChequeDueDate;

            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                int vId = (SecurityContextHelper.CurrentVendor != null ? SecurityContextHelper.CurrentVendor.VendorId : 0);
                LoadStaffClaimSummary(collectorClaimSummary, claimId, vId, collectorClaimSummary.ClaimCommonModel.ClaimPeriod);

                collectorClaimSummary.ClaimStatus.isPreviousClaimOnHold = false; //OTSTM2-499

                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    collectorClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                }

                if (status == ClaimStatus.Open)
                {
                    collectorClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    collectorClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}", claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }
                if (status == ClaimStatus.Approved)
                {
                    collectorClaimSummary.ClaimStatus.UnderReview = "Approved";
                }

                collectorClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate, TreadMarksConstants.Collector); //OTSTM2-499

                collectorClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate, TreadMarksConstants.Collector);

                collectorClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                collectorClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

            }

            ApplyUserSecurity(status, collectorClaimSummary);
            return collectorClaimSummary;
        }

        private void PopulatAllowancePaymentDetail(CollectorClaimSummaryModel collectorClaimSummary, int claimPeriodId)
        {
            Item item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            Rate itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            decimal tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.PLTRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.MTRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.AGLSRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.INDRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.SOTRRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.MOTRRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.LOTRRate = tireRate;

            item = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            itemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == item.ID && c.PeriodID == claimPeriodId);
            tireRate = itemRate != null ? itemRate.ItemRate : 0;
            collectorClaimSummary.CollectorAllowancePaymentDetail.GOTRRate = tireRate;

            collectorClaimSummary.CollectorAllowancePaymentDetail.PLTTireCountQty = collectorClaimSummary.TotalEligible.PLT + collectorClaimSummary.TotalAdjustmentEligible.PLT;
            collectorClaimSummary.CollectorAllowancePaymentDetail.MTTireCountQty = collectorClaimSummary.TotalEligible.MT + collectorClaimSummary.TotalAdjustmentEligible.MT;
            collectorClaimSummary.CollectorAllowancePaymentDetail.AGLSTireCountQty = collectorClaimSummary.TotalEligible.AGLS + collectorClaimSummary.TotalAdjustmentEligible.AGLS;
            collectorClaimSummary.CollectorAllowancePaymentDetail.INDTireCountQty = collectorClaimSummary.TotalEligible.IND + collectorClaimSummary.TotalAdjustmentEligible.IND;
            collectorClaimSummary.CollectorAllowancePaymentDetail.SOTRTireCountQty = collectorClaimSummary.TotalEligible.SOTR + collectorClaimSummary.TotalAdjustmentEligible.SOTR;
            collectorClaimSummary.CollectorAllowancePaymentDetail.MOTRTireCountQty = collectorClaimSummary.TotalEligible.MOTR + collectorClaimSummary.TotalAdjustmentEligible.MOTR;
            collectorClaimSummary.CollectorAllowancePaymentDetail.LOTRTireCountQty = collectorClaimSummary.TotalEligible.LOTR + collectorClaimSummary.TotalAdjustmentEligible.LOTR;
            collectorClaimSummary.CollectorAllowancePaymentDetail.GOTRTireCountQty = collectorClaimSummary.TotalEligible.GOTR + collectorClaimSummary.TotalAdjustmentEligible.GOTR;
        }

        private void PopulateSupportingDocumentsSelection(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimSupportingDocument> claimSupportingDocuments)
        {
            collectorClaimSummary.ClaimSupportingDocVMList = new List<ClaimSupportingDocVM>();
            claimSupportingDocuments.ForEach(c =>
            {
                collectorClaimSummary.ClaimSupportingDocVMList.Add(new ClaimSupportingDocVM
                {
                    Id = c.ID,
                    ClaimId = c.ClaimId,
                    Category = c.Category,
                    TypeCode = c.TypeCode,
                    DefinitionValue = c.DefinitionValue,
                    SelectedOption = c.SelectedOption,
                });
            });
        }

        public void UpdateSupportingDocOption(int claimId, int defValue, string option)
        {
            claimsRepository.UpdateSupportingDocOption(claimId, defValue, option);
        }

        public List<ClaimSupportingDocVM> LoadSupportingDocs(int claimId)
        {
            //Load Claim Supporting Documents
            var claimSupportingDocuments = claimsRepository.LoadClaimSupportingDocuments(claimId);
            List<ClaimSupportingDocVM> ClaimSupportingDocVMList = new List<ClaimSupportingDocVM>();
            claimSupportingDocuments.ForEach(c =>
            {
                ClaimSupportingDocVMList.Add(new ClaimSupportingDocVM
                {
                    Id = c.ID,
                    ClaimId = c.ClaimId,
                    Category = c.Category,
                    TypeCode = c.TypeCode,
                    DefinitionValue = c.DefinitionValue,
                    SelectedOption = c.SelectedOption,
                });
            });
            return ClaimSupportingDocVMList;
        }

        private void ApplyUserSecurity(ClaimStatus status, CollectorClaimSummaryModel collectorClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    collectorClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    collectorClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        collectorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        //OTSTM2-1139 No talk back needed
        //private void UpdateGpBatchForClaim(int claimId)
        //{
        //    var needPullBatch = claimsRepository.IsClaimPosted(claimId);
        //    if (needPullBatch)
        //    {
        //        var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);

        //        if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
        //        {
        //            var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
        //            var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
        //            if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
        //            {
        //                claimsRepository.UpdateClaimPaymentSummary(gpPaymentSummary, claimId);
        //            }
        //        }
        //    }
        //}

        private void LoadStaffClaimSummary(CollectorClaimSummaryModel collectorClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            collectorClaimSummary.ClaimStatus = claimStatus;
            collectorClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy) ? string.Empty : string.Format("Under Review by {0}", claimStatus.ReviewedBy);

            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        public void AddTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            var claimTireOrigins = ConvertToClaimTireOriginList(claimId, tireOriginItemRow, claimPeriod);
            claimsRepository.AddTireOrigin(claimId, tireOriginItemRow.TireOriginValue, claimTireOrigins);
            bool bAddClaimSupportingDocument = false;
            ClaimSupportingDocument claimSupportingDocument = new ClaimSupportingDocument()
            {
                ClaimId = claimId,
                Category = "TireOrigin",
                TypeCode = "UnknownSource",
                DefinitionValue = 2,
                SelectedOption = "optionupload",
            };
            switch (tireOriginItemRow.TireOriginValue)//select DefinitionValue, * from TypeDefinition where Category='TireOrigin'
            {
                case 2:
                    bAddClaimSupportingDocument = true;
                    claimSupportingDocument.TypeCode = "UnknownSource";
                    claimSupportingDocument.DefinitionValue = 2;
                    break;
                case 3:
                    bAddClaimSupportingDocument = true;
                    claimSupportingDocument.TypeCode = "ApprovedOTS";
                    claimSupportingDocument.DefinitionValue = 3;
                    break;
                case 4:
                    bAddClaimSupportingDocument = true;
                    claimSupportingDocument.TypeCode = "DroppedOffByConsumer";
                    claimSupportingDocument.DefinitionValue = 4;
                    break;
                case 9:
                    bAddClaimSupportingDocument = true;
                    claimSupportingDocument.TypeCode = "RemovedPurchased";
                    claimSupportingDocument.DefinitionValue = 9;
                    break;
                default:
                    break;
            }
            if (bAddClaimSupportingDocument)
            {
                claimsRepository.AddClaimSupportingDocument(claimSupportingDocument);
            }
        }
        public void RemoveTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            claimsRepository.RemoveTireOrigin(claimId, tireOriginItemRow);
            //remove support doc
            if ((tireOriginItemRow.TireOriginValue == 2) || (tireOriginItemRow.TireOriginValue == 3) || (tireOriginItemRow.TireOriginValue == 4) || (tireOriginItemRow.TireOriginValue == 9))
            {
                claimsRepository.RemoveClaimSupportingDocument(claimId, tireOriginItemRow.TireOriginValue);
            }
        }
        public void UpdateTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            var claimTireOrigins = ConvertToClaimTireOriginList(claimId, tireOriginItemRow, claimPeriod);
            claimsRepository.AddTireOrigin(claimId, tireOriginItemRow.TireOriginValue, claimTireOrigins);
        }
        public void AddReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            var claimReuseTires = ConvertToClaimReuseTiresList(claimId, reuseTiresItemRow, claimPeriod);
            claimsRepository.AddClaimReuseTires(claimId, claimReuseTires);
        }
        public void UpdateReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            var claimReuseTires = ConvertToClaimReuseTiresList(claimId, reuseTiresItemRow, claimPeriod);
            claimsRepository.AddClaimReuseTires(claimId, claimReuseTires);
        }
        public void RemoveReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            claimsRepository.RemoveClaimReuseTires(claimId, reuseTiresItemRow);
        }

        public CollectorClaimInboundReuseTireModel LoadInboundTires(int claimId)
        {
            var collectorClaimSummary = new CollectorClaimSummaryModel();
            var collectorInbound = new CollectorClaimInboundReuseTireModel();
            //Load Tire Origin
            var tireOrigins = claimsRepository.LoadClaimTireOrigins(claimId);
            PopulateInbound(collectorClaimSummary, tireOrigins);

            collectorInbound.InboundList = collectorClaimSummary.InboundList;
            collectorInbound.TotalInbound = collectorClaimSummary.TotalInbound;

            return collectorInbound;
        }

        public CollectorClaimInboundReuseTireModel LoadReuseTires(int claimId)
        {
            var collectorClaimSummary = new CollectorClaimSummaryModel();
            var collectorReuse = new CollectorClaimInboundReuseTireModel();
            //Load reuse tires
            var reuesTires = claimsRepository.LoadClaimReuseTires(claimId);
            PopulateRuseTires(collectorClaimSummary, reuesTires);

            collectorReuse.ReuseTire = collectorClaimSummary.ReuseTire;

            return collectorReuse;
        }

        #region submit claim rules
        public CollectorSubmitClaimViewModel CollectorClaimSubmitBusinessRule(CollectorSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            LoadDataForCollectorClaimSubmitBusinessRule(claim, submitClaimModel);

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>{
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel},
                {"TotalInbound", submitClaimModel.TotalInbound},
                {"TotalOutbound", submitClaimModel.TotalOutbound}
            };

            var updatedSubmitClaimForError = CollectorClaimSubmitErrors(claim, ruleContext, submitClaimModel);

            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = CollectorClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;
        }

        public void LoadDataForCollectorClaimSubmitBusinessRule(Claim claim, CollectorSubmitClaimViewModel submitClaimModel)
        {
            var collectorClaimSummary = new CollectorClaimSummaryModel();
            //Load Tire Origin
            var tireOrigins = claimsRepository.LoadClaimTireOrigins(claim.ID);
            PopulateInbound(collectorClaimSummary, tireOrigins);

            //Load outbound
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claim.ID, items);
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claim.ID);
            PopulateOutbound(collectorClaimSummary, claimDetails, inventoryAdjustments);

            submitClaimModel.TotalInbound = collectorClaimSummary.TotalInbound;
            submitClaimModel.TotalOutbound = collectorClaimSummary.TotalOutbound;
        }

        private CollectorSubmitClaimViewModel CollectorClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, CollectorSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new CollectorInboundOutboundMatchClaimRule());
            this.BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            this.BusinessRuleSet.AddBusinessRule(new IncompleteAndSyncedRule());
            this.BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);

            return submitClaimModel;

        }

        public void ExecuteBusinessRuleForErrors(CollectorSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }

        public CollectorSubmitClaimViewModel CollectorClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, CollectorSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new CollectorNilActivityClaimsRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());
            this.BusinessRuleSet.AddBusinessRule(new ZeroDollarClaimsRule());
            this.BusinessRuleSet.AddBusinessRule(new TireLogRequiredRule());


            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }

        #endregion

        #region Private Methods
        private List<ClaimTireOrigin> ConvertToClaimTireOriginList(int claimId, TireOriginItemRow tireOriginItemRow, Period claimPeriod)
        {
            var claimTireOrigins = new List<ClaimTireOrigin>();
            if (tireOriginItemRow.PLT > 0)
            {
                var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == pltItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = pltItem.ID,
                    Quantity = tireOriginItemRow.PLT,
                    ActualWeight = tireOriginItemRow.PLT * itemWeight,
                    AverageWeight = tireOriginItemRow.PLT * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.MT > 0)
            {
                var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == mtItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = mtItem.ID,
                    Quantity = tireOriginItemRow.MT,
                    ActualWeight = tireOriginItemRow.MT * itemWeight,
                    AverageWeight = tireOriginItemRow.MT * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.AGLS > 0)
            {
                var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == aglsItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = aglsItem.ID,
                    Quantity = tireOriginItemRow.AGLS,
                    ActualWeight = tireOriginItemRow.AGLS * itemWeight,
                    AverageWeight = tireOriginItemRow.AGLS * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.IND > 0)
            {
                var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == indItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = indItem.ID,
                    Quantity = tireOriginItemRow.IND,
                    ActualWeight = tireOriginItemRow.IND * itemWeight,
                    AverageWeight = tireOriginItemRow.IND * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }

            if (tireOriginItemRow.SOTR > 0)
            {
                var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == sotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = sotrItem.ID,
                    Quantity = tireOriginItemRow.SOTR,
                    ActualWeight = tireOriginItemRow.SOTR * itemWeight,
                    AverageWeight = tireOriginItemRow.SOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.MOTR > 0)
            {
                var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == motrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = motrItem.ID,
                    Quantity = tireOriginItemRow.MOTR,
                    ActualWeight = tireOriginItemRow.MOTR * itemWeight,
                    AverageWeight = tireOriginItemRow.MOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.LOTR > 0)
            {
                var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == lotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = lotrItem.ID,
                    Quantity = tireOriginItemRow.LOTR,
                    ActualWeight = tireOriginItemRow.LOTR * itemWeight,
                    AverageWeight = tireOriginItemRow.LOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            if (tireOriginItemRow.GOTR > 0)
            {
                var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == gotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimTireOrigin = new ClaimTireOrigin
                {
                    ClaimId = claimId,
                    TireOriginValue = tireOriginItemRow.TireOriginValue,
                    ItemId = gotrItem.ID,
                    Quantity = tireOriginItemRow.GOTR,
                    ActualWeight = tireOriginItemRow.GOTR * itemWeight,
                    AverageWeight = tireOriginItemRow.GOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimTireOrigins.Add(claimTireOrigin);
            }
            return claimTireOrigins;
        }

        private List<ClaimReuseTires> ConvertToClaimReuseTiresList(int claimId, ReuseTiresItemRow reuseTiresItemRow, Period claimPeriod)
        {
            var claimReuseTires = new List<ClaimReuseTires>();
            if (reuseTiresItemRow.PLT > 0)
            {
                var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == pltItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = pltItem.ID,
                    Quantity = reuseTiresItemRow.PLT,
                    ActualWeight = reuseTiresItemRow.PLT * itemWeight,
                    AverageWeight = reuseTiresItemRow.PLT * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.MT > 0)
            {
                var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == mtItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = mtItem.ID,
                    Quantity = reuseTiresItemRow.MT,
                    ActualWeight = reuseTiresItemRow.MT * itemWeight,
                    AverageWeight = reuseTiresItemRow.MT * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.AGLS > 0)
            {
                var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == aglsItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = aglsItem.ID,
                    Quantity = reuseTiresItemRow.AGLS,
                    ActualWeight = reuseTiresItemRow.AGLS * itemWeight,
                    AverageWeight = reuseTiresItemRow.AGLS * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.IND > 0)
            {
                var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == indItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = indItem.ID,
                    Quantity = reuseTiresItemRow.IND,
                    ActualWeight = reuseTiresItemRow.IND * itemWeight,
                    AverageWeight = reuseTiresItemRow.IND * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }

            if (reuseTiresItemRow.SOTR > 0)
            {
                var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == sotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = sotrItem.ID,
                    Quantity = reuseTiresItemRow.SOTR,
                    ActualWeight = reuseTiresItemRow.SOTR * itemWeight,
                    AverageWeight = reuseTiresItemRow.SOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.MOTR > 0)
            {
                var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == motrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = motrItem.ID,
                    Quantity = reuseTiresItemRow.MOTR,
                    ActualWeight = reuseTiresItemRow.MOTR * itemWeight,
                    AverageWeight = reuseTiresItemRow.MOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.LOTR > 0)
            {
                var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == lotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = lotrItem.ID,
                    Quantity = reuseTiresItemRow.LOTR,
                    ActualWeight = reuseTiresItemRow.LOTR * itemWeight,
                    AverageWeight = reuseTiresItemRow.LOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            if (reuseTiresItemRow.GOTR > 0)
            {
                var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == gotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var claimReuseTire = new ClaimReuseTires
                {
                    ClaimId = claimId,
                    ItemId = gotrItem.ID,
                    Quantity = reuseTiresItemRow.GOTR,
                    ActualWeight = reuseTiresItemRow.GOTR * itemWeight,
                    AverageWeight = reuseTiresItemRow.GOTR * itemWeight,
                    UpdatedDate = DateTime.UtcNow,
                    UnitType = 1
                };
                claimReuseTires.Add(claimReuseTire);
            }
            return claimReuseTires;
        }

        /* Original Outbound
        private void PopulateOutbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            #region Load TCR
            var tcrClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR).ToList();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.PLT += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MT += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.AGLS += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.IND += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.SOTR += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region  Load DOT
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT).ToList();
            var motrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItemDot.ID).Sum(q => q.Quantity);
            });
            var lotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItemDot.ID).Sum(q => q.Quantity);
            });
            var gotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItemDot.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region Load Eligible/Ineligible Adjustment
            var query = inventoryAdjustments.GroupBy(c => new { c.IsEligible }).Select(c => new
            {
                Name = c.Key,
                ClaimInventoryAdjustItems = c.Select(i => i.ClaimInventoryAdjustItems).ToList()
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible)
                {
                    //Load Eligible
                    c.ClaimInventoryAdjustItems.ForEach(item =>
                    {
                        var pltItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                        collectorClaimSummary.EligibleAdjustments.PLT += item.Where(q => q.ItemId == pltItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var mtItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                        collectorClaimSummary.EligibleAdjustments.MT += item.Where(q => q.ItemId == mtItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var aglsItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                        collectorClaimSummary.EligibleAdjustments.AGLS += item.Where(q => q.ItemId == aglsItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var indItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                        collectorClaimSummary.EligibleAdjustments.IND += item.Where(q => q.ItemId == indItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var sotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                        collectorClaimSummary.EligibleAdjustments.SOTR += item.Where(q => q.ItemId == sotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var motrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                        collectorClaimSummary.EligibleAdjustments.MOTR += item.Where(q => q.ItemId == motrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var lotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                        collectorClaimSummary.EligibleAdjustments.LOTR += item.Where(q => q.ItemId == lotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var gotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                        collectorClaimSummary.EligibleAdjustments.GOTR += item.Where(q => q.ItemId == gotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                    });
                }
                else
                {
                    //Load Ineligible
                    c.ClaimInventoryAdjustItems.ForEach(item =>
                    {
                        var pltItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                        collectorClaimSummary.IneligibleAdjustments.PLT += item.Where(q => q.ItemId == pltItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var mtItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                        collectorClaimSummary.IneligibleAdjustments.MT += item.Where(q => q.ItemId == mtItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var aglsItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                        collectorClaimSummary.IneligibleAdjustments.AGLS += item.Where(q => q.ItemId == aglsItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var indItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                        collectorClaimSummary.IneligibleAdjustments.IND += item.Where(q => q.ItemId == indItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var sotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                        collectorClaimSummary.IneligibleAdjustments.SOTR += item.Where(q => q.ItemId == sotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var motrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                        collectorClaimSummary.IneligibleAdjustments.MOTR += item.Where(q => q.ItemId == motrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var lotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                        collectorClaimSummary.IneligibleAdjustments.LOTR += item.Where(q => q.ItemId == lotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var gotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                        collectorClaimSummary.IneligibleAdjustments.GOTR += item.Where(q => q.ItemId == gotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                    });
                }
            });
            #endregion

            #region Load TotalEligible
            var tcrClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & c.IsEligible).ToList();
            int pltEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                pltEligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.PLT = pltEligible + collectorClaimSummary.EligibleAdjustments.PLT;

            int mtEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                mtEligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MT = mtEligible + collectorClaimSummary.EligibleAdjustments.MT;

            int aglsEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                aglsEligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.AGLS = aglsEligible + collectorClaimSummary.EligibleAdjustments.AGLS;

            int indEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                indEligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.IND = indEligible + collectorClaimSummary.EligibleAdjustments.IND;

            int sotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                sotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.SOTR = sotrEligible + collectorClaimSummary.EligibleAdjustments.SOTR;

            int motrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                motrEligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrEligibleDot = 0;
            var dotClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & c.IsEligible).ToList();
            dotClaimDetailsEligible.ForEach(c =>
            {
                motrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MOTR = motrEligible + motrEligibleDot + collectorClaimSummary.EligibleAdjustments.MOTR;

            int lotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                lotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                lotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.LOTR = lotrEligible + lotrEligibleDot + collectorClaimSummary.EligibleAdjustments.LOTR;

            int gotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                gotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                gotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.GOTR = gotrEligible + gotrEligibleDot + collectorClaimSummary.EligibleAdjustments.GOTR;

            #endregion

            #region Load TotalIneligible
            var tcrClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & !c.IsEligible).ToList();
            int pltIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                pltIneligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.PLT = pltIneligible + collectorClaimSummary.IneligibleAdjustments.PLT;

            int mtIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                mtIneligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MT = mtIneligible + collectorClaimSummary.IneligibleAdjustments.MT;

            int aglsIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                aglsIneligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.AGLS = aglsIneligible + collectorClaimSummary.IneligibleAdjustments.AGLS;

            int indIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                indIneligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.IND = indIneligible + collectorClaimSummary.IneligibleAdjustments.IND;

            int sotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                sotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.SOTR = sotrIneligible + collectorClaimSummary.IneligibleAdjustments.SOTR;

            int motrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrIneligibleDot = 0;
            var dotClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & !c.IsEligible).ToList();
            dotClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MOTR = motrIneligible + motrIneligibleDot + collectorClaimSummary.IneligibleAdjustments.MOTR;

            int lotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.LOTR = lotrIneligible + lotrIneligibleDot + collectorClaimSummary.IneligibleAdjustments.LOTR;

            int gotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.GOTR = gotrIneligible + gotrIneligibleDot + collectorClaimSummary.IneligibleAdjustments.GOTR;

            #endregion

            #region Load TotalOutbound
            collectorClaimSummary.TotalOutbound.PLT = collectorClaimSummary.TotalEligible.PLT + collectorClaimSummary.TotalIneligible.PLT;
            collectorClaimSummary.TotalOutbound.MT = collectorClaimSummary.TotalEligible.MT + collectorClaimSummary.TotalIneligible.MT;
            collectorClaimSummary.TotalOutbound.AGLS = collectorClaimSummary.TotalEligible.AGLS + collectorClaimSummary.TotalIneligible.AGLS;
            collectorClaimSummary.TotalOutbound.IND = collectorClaimSummary.TotalEligible.IND + collectorClaimSummary.TotalIneligible.IND;
            collectorClaimSummary.TotalOutbound.SOTR = collectorClaimSummary.TotalEligible.SOTR + collectorClaimSummary.TotalIneligible.SOTR;
            collectorClaimSummary.TotalOutbound.MOTR = collectorClaimSummary.TotalEligible.MOTR + collectorClaimSummary.TotalIneligible.MOTR;
            collectorClaimSummary.TotalOutbound.LOTR = collectorClaimSummary.TotalEligible.LOTR + collectorClaimSummary.TotalIneligible.LOTR;
            collectorClaimSummary.TotalOutbound.GOTR = collectorClaimSummary.TotalEligible.GOTR + collectorClaimSummary.TotalIneligible.GOTR;
            #endregion

        }              
        */
        //OTSTM2-588
        private void PopulateOutbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            #region Load TCR
            var tcrClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR).ToList();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.PLT += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MT += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.AGLS += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.IND += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.SOTR += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region  Load DOT
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT).ToList();
            var motrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItemDot.ID).Sum(q => q.Quantity);
            });
            var lotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItemDot.ID).Sum(q => q.Quantity);
            });
            var gotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItemDot.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region Load TotalEligible
            var tcrClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & c.IsEligible).ToList();
            int pltEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                pltEligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.PLT = pltEligible;

            int mtEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                mtEligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MT = mtEligible;

            int aglsEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                aglsEligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.AGLS = aglsEligible;

            int indEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                indEligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.IND = indEligible;

            int sotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                sotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.SOTR = sotrEligible;

            int motrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                motrEligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrEligibleDot = 0;
            var dotClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & c.IsEligible).ToList();
            dotClaimDetailsEligible.ForEach(c =>
            {
                motrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MOTR = motrEligible + motrEligibleDot;

            int lotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                lotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                lotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.LOTR = lotrEligible + lotrEligibleDot;

            int gotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                gotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                gotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.GOTR = gotrEligible + gotrEligibleDot;

            #endregion

            #region Load TotalIneligible
            var tcrClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & !c.IsEligible).ToList();
            int pltIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                pltIneligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.PLT = pltIneligible;

            int mtIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                mtIneligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MT = mtIneligible;

            int aglsIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                aglsIneligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.AGLS = aglsIneligible;

            int indIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                indIneligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.IND = indIneligible;

            int sotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                sotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.SOTR = sotrIneligible;

            int motrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrIneligibleDot = 0;
            var dotClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & !c.IsEligible).ToList();
            dotClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MOTR = motrIneligible + motrIneligibleDot;

            int lotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.LOTR = lotrIneligible + lotrIneligibleDot;

            int gotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.GOTR = gotrIneligible + gotrIneligibleDot;

            #endregion

            #region Load TotalOutbound
            collectorClaimSummary.TotalOutbound.PLT = collectorClaimSummary.TotalEligible.PLT + collectorClaimSummary.TotalIneligible.PLT;
            collectorClaimSummary.TotalOutbound.MT = collectorClaimSummary.TotalEligible.MT + collectorClaimSummary.TotalIneligible.MT;
            collectorClaimSummary.TotalOutbound.AGLS = collectorClaimSummary.TotalEligible.AGLS + collectorClaimSummary.TotalIneligible.AGLS;
            collectorClaimSummary.TotalOutbound.IND = collectorClaimSummary.TotalEligible.IND + collectorClaimSummary.TotalIneligible.IND;
            collectorClaimSummary.TotalOutbound.SOTR = collectorClaimSummary.TotalEligible.SOTR + collectorClaimSummary.TotalIneligible.SOTR;
            collectorClaimSummary.TotalOutbound.MOTR = collectorClaimSummary.TotalEligible.MOTR + collectorClaimSummary.TotalIneligible.MOTR;
            collectorClaimSummary.TotalOutbound.LOTR = collectorClaimSummary.TotalEligible.LOTR + collectorClaimSummary.TotalIneligible.LOTR;
            collectorClaimSummary.TotalOutbound.GOTR = collectorClaimSummary.TotalEligible.GOTR + collectorClaimSummary.TotalIneligible.GOTR;
            #endregion

        }
        //OTSTM2-588
        private void PopulateAdjustment(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            #region Load Eligible/Ineligible Adjustment
            var query = inventoryAdjustments.GroupBy(c => new { c.IsEligible }).Select(c => new
            {
                Name = c.Key,
                ClaimInventoryAdjustItems = c.Select(i => i.ClaimInventoryAdjustItems).ToList()
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible)
                {
                    //Load Eligible
                    c.ClaimInventoryAdjustItems.ForEach(item =>
                    {
                        var pltItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                        collectorClaimSummary.TotalAdjustmentEligible.PLT += item.Where(q => q.ItemId == pltItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var mtItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                        collectorClaimSummary.TotalAdjustmentEligible.MT += item.Where(q => q.ItemId == mtItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var aglsItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                        collectorClaimSummary.TotalAdjustmentEligible.AGLS += item.Where(q => q.ItemId == aglsItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var indItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                        collectorClaimSummary.TotalAdjustmentEligible.IND += item.Where(q => q.ItemId == indItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var sotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                        collectorClaimSummary.TotalAdjustmentEligible.SOTR += item.Where(q => q.ItemId == sotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var motrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                        collectorClaimSummary.TotalAdjustmentEligible.MOTR += item.Where(q => q.ItemId == motrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var lotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                        collectorClaimSummary.TotalAdjustmentEligible.LOTR += item.Where(q => q.ItemId == lotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var gotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                        collectorClaimSummary.TotalAdjustmentEligible.GOTR += item.Where(q => q.ItemId == gotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                    });
                }
                else
                {
                    //Load Ineligible
                    c.ClaimInventoryAdjustItems.ForEach(item =>
                    {
                        var pltItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                        collectorClaimSummary.TotalAdjustmentIneligible.PLT += item.Where(q => q.ItemId == pltItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var mtItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                        collectorClaimSummary.TotalAdjustmentIneligible.MT += item.Where(q => q.ItemId == mtItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var aglsItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                        collectorClaimSummary.TotalAdjustmentIneligible.AGLS += item.Where(q => q.ItemId == aglsItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var indItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                        collectorClaimSummary.TotalAdjustmentIneligible.IND += item.Where(q => q.ItemId == indItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var sotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                        collectorClaimSummary.TotalAdjustmentIneligible.SOTR += item.Where(q => q.ItemId == sotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var motrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                        collectorClaimSummary.TotalAdjustmentIneligible.MOTR += item.Where(q => q.ItemId == motrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var lotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                        collectorClaimSummary.TotalAdjustmentIneligible.LOTR += item.Where(q => q.ItemId == lotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                        var gotrItemAdjustment = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                        collectorClaimSummary.TotalAdjustmentIneligible.GOTR += item.Where(q => q.ItemId == gotrItemAdjustment.ID).Sum(o => o.QtyAdjustment);

                    });
                }

                #region Load TotalOutbound
                collectorClaimSummary.TotalAdjustments.PLT = collectorClaimSummary.TotalAdjustmentEligible.PLT + collectorClaimSummary.TotalAdjustmentIneligible.PLT;
                collectorClaimSummary.TotalAdjustments.MT = collectorClaimSummary.TotalAdjustmentEligible.MT + collectorClaimSummary.TotalAdjustmentIneligible.MT;
                collectorClaimSummary.TotalAdjustments.AGLS = collectorClaimSummary.TotalAdjustmentEligible.AGLS + collectorClaimSummary.TotalAdjustmentIneligible.AGLS;
                collectorClaimSummary.TotalAdjustments.IND = collectorClaimSummary.TotalAdjustmentEligible.IND + collectorClaimSummary.TotalAdjustmentIneligible.IND;
                collectorClaimSummary.TotalAdjustments.SOTR = collectorClaimSummary.TotalAdjustmentEligible.SOTR + collectorClaimSummary.TotalAdjustmentIneligible.SOTR;
                collectorClaimSummary.TotalAdjustments.MOTR = collectorClaimSummary.TotalAdjustmentEligible.MOTR + collectorClaimSummary.TotalAdjustmentIneligible.MOTR;
                collectorClaimSummary.TotalAdjustments.LOTR = collectorClaimSummary.TotalAdjustmentEligible.LOTR + collectorClaimSummary.TotalAdjustmentIneligible.LOTR;
                collectorClaimSummary.TotalAdjustments.GOTR = collectorClaimSummary.TotalAdjustmentEligible.GOTR + collectorClaimSummary.TotalAdjustmentIneligible.GOTR;
                #endregion
            });
            #endregion
        }
        private void PopulateInbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimTireOrigin> tireOrigins)
        {
            var query = tireOrigins.GroupBy(c => new { c.TireOriginValue })
                    .Select(c => new { Name = c.Key, ItemList = c.ToList() });
            query.ToList().ForEach(c =>
            {
                var tireOriginItemRow = new TireOriginItemRow();
                tireOriginItemRow.TireOriginValue = c.Name.TireOriginValue;
                tireOriginItemRow.TireOrigin = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).Description;
                tireOriginItemRow.DisplayOrder = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).DisplayOrder;
                c.ItemList.ForEach(i =>
                {
                    var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == i.ItemId);
                    var propertyInfo = tireOriginItemRow.GetType().GetProperty(item.ShortName);
                    propertyInfo.SetValue(tireOriginItemRow, i.Quantity, null);
                });
                collectorClaimSummary.InboundList.Add(tireOriginItemRow);
            });

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            collectorClaimSummary.TotalInbound.PLT = tireOrigins.Where(c => c.ItemId == pltItem.ID).Sum(c => c.Quantity);

            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            collectorClaimSummary.TotalInbound.MT = tireOrigins.Where(c => c.ItemId == mtItem.ID).Sum(c => c.Quantity);

            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            collectorClaimSummary.TotalInbound.AGLS = tireOrigins.Where(c => c.ItemId == aglsItem.ID).Sum(c => c.Quantity);

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            collectorClaimSummary.TotalInbound.IND = tireOrigins.Where(c => c.ItemId == indItem.ID).Sum(c => c.Quantity);

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            collectorClaimSummary.TotalInbound.SOTR = tireOrigins.Where(c => c.ItemId == sotrItem.ID).Sum(c => c.Quantity);

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            collectorClaimSummary.TotalInbound.MOTR = tireOrigins.Where(c => c.ItemId == motrItem.ID).Sum(c => c.Quantity);

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            collectorClaimSummary.TotalInbound.LOTR = tireOrigins.Where(c => c.ItemId == lotrItem.ID).Sum(c => c.Quantity);

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            collectorClaimSummary.TotalInbound.GOTR = tireOrigins.Where(c => c.ItemId == gotrItem.ID).Sum(c => c.Quantity);

        }
        private void PopulateRuseTires(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimReuseTires> reuesTires)
        {
            var reuseTiresItemRow = new ReuseTiresItemRow();

            reuesTires.ForEach(c =>
            {
                var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == c.ItemId);
                var propertyInfo = reuseTiresItemRow.GetType().GetProperty(item.ShortName);
                propertyInfo.SetValue(reuseTiresItemRow, c.Quantity, null);
            });

            collectorClaimSummary.ReuseTire = reuseTiresItemRow;
        }
        private void PopulatPaymentAdjustments(CollectorPaymentViewModel collectorPayment, ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments, Claim claim)
        {
            //Populate Rate
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var pltItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == pltItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var pltRate = pltItemRate != null ? pltItemRate.ItemRate : 0;
            collectorPayment.PLTRate = pltRate;

            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var mtItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == mtItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var mtRate = mtItemRate != null ? mtItemRate.ItemRate : 0;
            collectorPayment.MTRate = mtRate;

            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var aglsItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == aglsItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var aglsRate = aglsItemRate != null ? aglsItemRate.ItemRate : 0;
            collectorPayment.AGLSRate = aglsRate;

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var indItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == indItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var indRate = indItemRate != null ? indItemRate.ItemRate : 0;
            collectorPayment.INDRate = indRate;

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var sotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == sotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var sotrRate = sotrItemRate != null ? sotrItemRate.ItemRate : 0;
            collectorPayment.SOTRRate = sotrRate;

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var motrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == motrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var motrRate = motrItemRate != null ? motrItemRate.ItemRate : 0;
            collectorPayment.MOTRRate = motrRate;

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var lotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == lotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var lotrRate = lotrItemRate != null ? lotrItemRate.ItemRate : 0;
            collectorPayment.LOTRRate = lotrRate;

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            var gotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == gotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var gotrRate = gotrItemRate != null ? gotrItemRate.ItemRate : 0;
            collectorPayment.GOTRRate = gotrRate;

            //Populate from payment table
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Qty = c.Sum(i => i.Weight),
                Rate = c.First().Rate
            });

            collectorPayment.UsingOldMTGOTRPayment = (claimPayments.FirstOrDefault(c => c.PaymentType == (int)ClaimPaymentType.MTGOTR) != null);

            if (collectorPayment.UsingOldMTGOTRPayment)
            {
                query.ToList().ForEach(c =>
                {
                    if (c.Name == (int)ClaimPaymentType.PLT)
                    {
                        collectorPayment.PLTQty = c.Qty;
                        collectorPayment.PLTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MTGOTR)
                    {
                        collectorPayment.MTGOTRQty = c.Qty;
                        collectorPayment.MTGOTRRate = c.Rate;
                    }
                });
            }
            else
            {
                query.ToList().ForEach(c =>
                {
                    if (c.Name == (int)ClaimPaymentType.PLT)
                    {
                        collectorPayment.PLTQty = c.Qty;
                        collectorPayment.PLTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MT)
                    {
                        collectorPayment.MTQty = c.Qty;
                        collectorPayment.MTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.AGLS)
                    {
                        collectorPayment.AGLSQty = c.Qty;
                        collectorPayment.AGLSRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.IND)
                    {
                        collectorPayment.INDQty = c.Qty;
                        collectorPayment.INDRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.SOTR)
                    {
                        collectorPayment.SOTRQty = c.Qty;
                        collectorPayment.SOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MOTR)
                    {
                        collectorPayment.MOTRQty = c.Qty;
                        collectorPayment.MOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.LOTR)
                    {
                        collectorPayment.LOTRQty = c.Qty;
                        collectorPayment.LOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.GOTR)
                    {
                        collectorPayment.GOTRQty = c.Qty;
                        collectorPayment.GOTRRate = c.Rate;
                    }
                });
            }

            collectorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

            if (claimPaymentSummay != null)
            {
                collectorPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                collectorPayment.PaidDate = claimPaymentSummay.PaidDate;
                collectorPayment.MailedDate = claimPaymentSummay.MailDate;
                collectorPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                collectorPayment.BatchNumber = claimPaymentSummay.BatchNumber;
            }
        }

        #endregion

        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();

            var claimType = (int)ClaimType.Collector;
            var claims = claimsRepository.LoadApprovedWithoutBatch(claimType, true);
            //For collector, all primary Business Activity = "generator" type will be excluded
            var noGeneratorClaims = claims.Where(c => c.Participant.PrimaryBusinessActivity != "Generator").ToList();
            //Remove claim if it is banking status is not approved
            var allClaimIds = noGeneratorClaims.Select(c => c.ID).ToList();
            var validClaims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

            if (!validClaims.Any())
            {
                msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
            }
            else
            {
                var gpiBatch = new GpiBatch()
                {
                    GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Collector),
                    GpiBatchCount = validClaims.Count(),
                    GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                };

                var minPostDate = gpRepository.GetAllGpFiscalYears()
                    .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                    .Select(d => d.MinimumDocDate).FirstOrDefault();
                var claimIds = validClaims.Select(c => c.ID).ToList();
                var transactionTypes = new List<string> { "TCR", "DOT" };
                var items = DataLoader.Items;
                var claimDetails = claimsRepository.LoadClaimDetailsForGP(claimIds, transactionTypes, items);
                var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
                //var gpiAccounts = DataLoader.GPIAccounts;
                var fiAccounts = DataLoader.FIAccounts; //OTSTM2-1124

                //Update database
                using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //1. GP Batch
                    gpRepository.AddGpBatch(gpiBatch);
                    msg.ID = gpiBatch.ID;

                    //2. GP Batch entries
                    var gpiBatchEntries = GetBatchEntries(validClaims, gpiBatch.ID);
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    //3 GP Transaction and Transaction Distribution
                    var rrOtsPmTransaction = AddGPTransactions(validClaims, gpiBatch.ID, gpiBatchEntries, minPostDate, INTERID, claimDetails, fiAccounts); //OTSTM2-1124

                    gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransaction.Keys.ToList());
                    var rrOtsTransDistributions = new List<RrOtsPmTransDistribution>();
                    rrOtsPmTransaction.Values.ToList().ForEach(c =>
                    {
                        rrOtsTransDistributions.AddRange(c);
                    });
                    gpRepository.AddGpRrOtsPmTransDistributions(rrOtsTransDistributions);

                    //4. Update claim
                    claimsRepository.UpdateClaimsForGPBatch(claimIds, gpiBatch.ID.ToString());
                    transactionScope.Complete();
                }
            }
            return msg;
        }

        private CollectorPaymentViewModel LoadCollectorPaymentForGP(int claimId)
        {
            var collectorPayment = new CollectorPaymentViewModel();
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Qty = c.Sum(i => i.Weight),
                Rate = c.First().Rate
            });

            //OTSTM2-961
            collectorPayment.UsingOldMTGOTRPayment = (claimPayments.FirstOrDefault(c => c.PaymentType == (int)ClaimPaymentType.MTGOTR) != null);

            if (collectorPayment.UsingOldMTGOTRPayment)
            {
                query.ToList().ForEach(c =>
                {
                    if (c.Name == (int)ClaimPaymentType.PLT)
                    {
                        collectorPayment.PLTQty = c.Qty;
                        collectorPayment.PLTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MTGOTR)
                    {
                        collectorPayment.MTGOTRQty = c.Qty;
                        collectorPayment.MTGOTRRate = c.Rate;
                    }
                });
            }
            else
            {
                query.ToList().ForEach(c =>
                {
                    if (c.Name == (int)ClaimPaymentType.PLT)
                    {
                        collectorPayment.PLTQty = c.Qty;
                        collectorPayment.PLTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MT)
                    {
                        collectorPayment.MTQty = c.Qty;
                        collectorPayment.MTRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.AGLS)
                    {
                        collectorPayment.AGLSQty = c.Qty;
                        collectorPayment.AGLSRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.IND)
                    {
                        collectorPayment.INDQty = c.Qty;
                        collectorPayment.INDRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.SOTR)
                    {
                        collectorPayment.SOTRQty = c.Qty;
                        collectorPayment.SOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.MOTR)
                    {
                        collectorPayment.MOTRQty = c.Qty;
                        collectorPayment.MOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.LOTR)
                    {
                        collectorPayment.LOTRQty = c.Qty;
                        collectorPayment.LOTRRate = c.Rate;
                    }
                    if (c.Name == (int)ClaimPaymentType.GOTR)
                    {
                        collectorPayment.GOTRQty = c.Qty;
                        collectorPayment.GOTRRate = c.Rate;
                    }
                });
            }

            collectorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                collectorPayment.HSTBase = 0.00m;
            }
            collectorPayment.IsTaxExempt = isTaxExempt;
            return collectorPayment;

        }

        private Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>> AddGPTransactions(List<Claim> claims, int gpiBatchId, List<GpiBatchEntry> gpiBatchEntries, DateTime minPostDate, string INTERID, List<ClaimDetailViewModel> claimDetails, List<FIAccount> fiAccounts)
        {
            //Step 4 Add RM_Transaction
            var result = new Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>>();
            var rrOtsPmTransaction = new List<RrOtsPmTransaction>();
            claims.ForEach(c =>
            {
                var collectorPaymentViewModel = LoadCollectorPaymentForGP(c.ID);

                //DOCType=1
                var prchAmount1 = Math.Round(collectorPaymentViewModel.PLTPayment + collectorPaymentViewModel.MTGOTRPayment, 2, MidpointRounding.AwayFromZero);
                var prchTax1 = Math.Round(prchAmount1 * collectorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);
                var adjustAmount = Math.Round(collectorPaymentViewModel.PaymentAdjustment > 0 ? collectorPaymentViewModel.PaymentAdjustment : 0, 2, MidpointRounding.AwayFromZero);
                var adjustTaxAmount = Math.Round(adjustAmount * collectorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                //DOCType=5
                //var prchAmount5 = Math.Round(collectorPaymentViewModel.IneligibleInventoryPayment + (collectorPaymentViewModel.IneligibleInventoryPaymentAdjust < 0 ? collectorPaymentViewModel.IneligibleInventoryPaymentAdjust : 0) + (collectorPaymentViewModel.PaymentAdjustment < 0 ? collectorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                var prchAmount5 = Math.Round((collectorPaymentViewModel.PaymentAdjustment < 0 ? collectorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                var prchTax5 = Math.Round((collectorPaymentViewModel.PaymentAdjustment < 0 ? collectorPaymentViewModel.PaymentAdjustment : 0) * collectorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var positiveTotal = prchAmount1 + prchTax1 + adjustAmount + adjustTaxAmount;
                var gpTotal = positiveTotal + prchAmount5 + prchTax5;
                if (gpTotal != collectorPaymentViewModel.GrandTotal)
                {
                    var difference = (gpTotal - collectorPaymentViewModel.GrandTotal);
                    prchAmount1 -= difference;
                }

                var gpiBatchEntryId = gpiBatchEntries.Single(q => q.ClaimId == c.ID).ID;
                int dstSeqNum = 1;

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                var typeOneTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "1"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1} {2:MMyy}-{3}", c.Participant.Number, "REG", c.ClaimPeriod.StartDate, "1"),
                    DOCTYPE = 1,
                    DOCAMNT = prchAmount1 + prchTax1 + adjustAmount + adjustTaxAmount,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = collectorPaymentViewModel.HST == 0 && collectorPaymentViewModel.IsTaxExempt ? "P-EXEMPT" : "P-HST",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount1 + adjustAmount,
                    TAXAMNT = prchTax1 + adjustTaxAmount,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1} {2:MMyy}-{3}", c.Participant.Number, "REG", c.ClaimPeriod.StartDate, "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId,
                };

                //Add transaction distribution
                var currentClaimDetails = claimDetails.Where(q => q.ClaimId == c.ID).ToList();
                var typeOneDistrubutions = AddTypeOneDistributions(gpiBatchId, INTERID, prchAmount1, prchTax1, adjustAmount, adjustTaxAmount, typeOneTransaction, fiAccounts, currentClaimDetails, collectorPaymentViewModel, ref dstSeqNum); //OTSTM2-1124
                result.Add(typeOneTransaction, typeOneDistrubutions);

                //Add type 5 transaction
                if (prchAmount5 + prchTax5 != 0)
                {
                    var typeFiveTransaction = new RrOtsPmTransaction
                    {
                        BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                        VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "5"),
                        VENDORID = c.Participant.Number,
                        DOCNUMBR = string.Format("{0}-{1} {2:MMyy}-{3}", c.Participant.Number, "REG", c.ClaimPeriod.StartDate, "5"),
                        DOCTYPE = 5,
                        DOCAMNT = prchAmount5 + prchTax5,
                        DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                        PSTGDATE = null,
                        VADDCDPR = "PRIMARY",
                        PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                        TAXSCHID = collectorPaymentViewModel.HST == 0 && collectorPaymentViewModel.IsTaxExempt ? "P-EXEMPT" : "P-HST",
                        DUEDATE = null,
                        PRCHAMNT = prchAmount5,
                        TAXAMNT = prchTax5,
                        PORDNMBR = null,
                        USERDEF1 = gpiBatchId.ToString(),
                        USERDEF2 = string.Format("{0}-{1} {2:MMyy}-{3}", c.Participant.Number, "REG", c.ClaimPeriod.StartDate, "5"),
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        GpiBatchEntryId = gpiBatchEntryId,
                    };
                    var typeFiveDistributions = AddTypeFiveDistributions(gpiBatchId, INTERID, prchAmount5, prchTax5, typeFiveTransaction, fiAccounts, ref dstSeqNum); //OTSTM2-1124
                    result.Add(typeFiveTransaction, typeFiveDistributions);
                }
            });
            return result;
        }

        private RrOtsPmTransDistribution GetTransDistribution(RrOtsPmTransaction gpTransaction, int gpiBatchId, string INTERID, int dstSeqNum)
        {
            var result = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = gpTransaction.VCHNUMWK,
                DOCTYPE = gpTransaction.DOCTYPE,
                VENDORID = gpTransaction.VENDORID,
                DSTSQNUM = dstSeqNum,
                DistRef = null,
                USERDEF1 = gpiBatchId.ToString(),
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null
            };
            return result;
        }

        private List<RrOtsPmTransDistribution> AddTypeOneDistributions(int gpiBatchId, string INTERID, decimal prchAmount1, decimal prchTax1, decimal adjustAmount, decimal adjustTaxAmount, RrOtsPmTransaction typeOneTransaction, List<FIAccount> fiAccounts, List<ClaimDetailViewModel> currentClaimDetails, CollectorPaymentViewModel collectorPaymentViewModel, ref int dstSeqNum)
        {
            var typeOneDistrubutions = new List<RrOtsPmTransDistribution>();
            //var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "C");
            //var pltAccount = gpiAccounts.Single(a => a.account_number == "4110-10-20-40");
            //var pltHst = gpiAccounts.Single(a => a.account_number == "4115-10-20-40");
            //var mtAccount = gpiAccounts.Single(a => a.account_number == "4120-20-20-40");
            //var mtHst = gpiAccounts.Single(a => a.account_number == "4125-20-20-40");
            //var aglsAccount = gpiAccounts.Single(a => a.account_number == "4130-30-20-40");
            //var aglsHst = gpiAccounts.Single(a => a.account_number == "4135-30-20-40");
            //var indAccount = gpiAccounts.Single(a => a.account_number == "4140-40-20-40");
            //var indHst = gpiAccounts.Single(a => a.account_number == "4145-40-20-40");
            //var sotrAccount = gpiAccounts.Single(a => a.account_number == "4150-50-20-40");
            //var sotrHst = gpiAccounts.Single(a => a.account_number == "4155-50-20-40");
            //var motrAccount = gpiAccounts.Single(a => a.account_number == "4160-60-20-40");
            //var motrHst = gpiAccounts.Single(a => a.account_number == "4165-60-20-40");
            //var lotrAccount = gpiAccounts.Single(a => a.account_number == "4170-70-20-40");
            //var lotrHst = gpiAccounts.Single(a => a.account_number == "4175-70-20-40");
            //var gotrAccount = gpiAccounts.Single(a => a.account_number == "4180-80-20-40");
            //var gotrHst = gpiAccounts.Single(a => a.account_number == "4185-80-20-40");

            //var adjustAccount = gpiAccounts.Single(a => a.account_number == "4195-90-20-40");
            //var adjustHstAccount = gpiAccounts.Single(a => a.account_number == "4196-90-20-40");

            //OTSTM2-1124
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Collector Payable" && a.AdminFICategoryID == 2);
            var pltAccount = fiAccounts.FirstOrDefault(a => a.Name == "PLT" && a.AdminFICategoryID == 2);
            var pltHst = fiAccounts.FirstOrDefault(a => a.Name == "PLT HST" && a.AdminFICategoryID == 2);
            var mtAccount = fiAccounts.FirstOrDefault(a => a.Name == "MT" && a.AdminFICategoryID == 2);
            var mtHst = fiAccounts.FirstOrDefault(a => a.Name == "MT HST" && a.AdminFICategoryID == 2);
            var aglsAccount = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS" && a.AdminFICategoryID == 2);
            var aglsHst = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS HST" && a.AdminFICategoryID == 2);
            var indAccount = fiAccounts.FirstOrDefault(a => a.Name == "IND" && a.AdminFICategoryID == 2);
            var indHst = fiAccounts.FirstOrDefault(a => a.Name == "IND HST" && a.AdminFICategoryID == 2);
            var sotrAccount = fiAccounts.FirstOrDefault(a => a.Name == "SOTR" && a.AdminFICategoryID == 2);
            var sotrHst = fiAccounts.FirstOrDefault(a => a.Name == "SOTR HST" && a.AdminFICategoryID == 2);
            var motrAccount = fiAccounts.FirstOrDefault(a => a.Name == "MOTR" && a.AdminFICategoryID == 2);
            var motrHst = fiAccounts.FirstOrDefault(a => a.Name == "MOTR HST" && a.AdminFICategoryID == 2);
            var lotrAccount = fiAccounts.FirstOrDefault(a => a.Name == "LOTR" && a.AdminFICategoryID == 2);
            var lotrHst = fiAccounts.FirstOrDefault(a => a.Name == "LOTR HST" && a.AdminFICategoryID == 2);
            var gotrAccount = fiAccounts.FirstOrDefault(a => a.Name == "GOTR" && a.AdminFICategoryID == 2);
            var gotrHst = fiAccounts.FirstOrDefault(a => a.Name == "GOTR HST" && a.AdminFICategoryID == 2);

            var adjustAccount = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 2);
            var adjustHstAccount = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 2);

            if (prchAmount1 + prchTax1 + adjustAmount + adjustTaxAmount > 0)
            {
                var rowAccountPayrable = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = 0;
                rowAccountPayrable.CRDTAMNT = prchAmount1 + prchTax1 + adjustAmount + adjustTaxAmount;
                typeOneDistrubutions.Add(rowAccountPayrable);
            }

            if (prchAmount1 + prchTax1 > 0)
            {
                //TODO: this method not include the adjustment data (TypeFiveDistributions)
                var gpTireItem = FindTirePercentage(currentClaimDetails, collectorPaymentViewModel);

                //prchAmount distribution
                if (prchAmount1 > 0)
                {
                    var prchAmountDistributions = new List<RrOtsPmTransDistribution>();
                    if (gpTireItem.PLTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)pltAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = pltAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)mtAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = mtAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.AGLSPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)aglsAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = aglsAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.INDPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)indAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = indAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.SOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)sotrAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = sotrAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)motrAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = motrAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)lotrAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = lotrAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)gotrAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = gotrAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchAmount1 * gpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchAmountDistributions.Add(rowDistribution);
                    }

                    //Fixed cent difference caused by rounding
                    var totalAmount = prchAmountDistributions.Sum(c => c.DEBITAMT);
                    if (totalAmount != prchAmount1)
                    {
                        if (prchAmountDistributions.Count > 0)
                        {
                            var difference = (totalAmount - prchAmount1);
                            var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                            if (numberOfDifferenceDistribution < prchAmountDistributions.Count)
                            {
                                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                                {
                                    prchAmountDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                                }
                            }
                        }
                        else
                        {
                            //no transactions exist for this claim; thus PLT and MT-GOTR Amounts are only from Payment Inventory Adjustments
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)adjustAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = adjustAccount.AccountNumber; //OTSTM2-1124
                            rowDistribution.DEBITAMT = prchAmount1;
                            rowDistribution.CRDTAMNT = 0;
                            prchAmountDistributions.Add(rowDistribution);
                            //var adjustAccount = gpiAccounts.Single(a => a.account_number == "4195-90-20-40");
                            //var adjustHstAccount = gpiAccounts.Single(a => a.account_number == "4196-90-20-40");
                        }
                    }

                    typeOneDistrubutions.AddRange(prchAmountDistributions);
                }

                //prchTax distribution
                if (prchTax1 > 0)
                {
                    var prchTaxDistributions = new List<RrOtsPmTransDistribution>();
                    if (gpTireItem.PLTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)pltHst.distribution_type_id;
                        rowDistribution.ACTNUMST = pltHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)mtHst.distribution_type_id;
                        rowDistribution.ACTNUMST = mtHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.AGLSPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)aglsHst.distribution_type_id;
                        rowDistribution.ACTNUMST = aglsHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.INDPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)indHst.distribution_type_id;
                        rowDistribution.ACTNUMST = indHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.SOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)sotrHst.distribution_type_id;
                        rowDistribution.ACTNUMST = sotrHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)motrHst.distribution_type_id;
                        rowDistribution.ACTNUMST = motrHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)lotrHst.distribution_type_id;
                        rowDistribution.ACTNUMST = lotrHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)gotrHst.distribution_type_id;
                        rowDistribution.ACTNUMST = gotrHst.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(prchTax1 * gpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        prchTaxDistributions.Add(rowDistribution);
                    }

                    //Fixed cent difference caused by rounding
                    var totalAmount = prchTaxDistributions.Sum(c => c.DEBITAMT);
                    if (totalAmount != prchTax1)
                    {

                        if (prchTaxDistributions.Count > 0)
                        {
                            var difference = (totalAmount - prchTax1);
                            var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                            if (numberOfDifferenceDistribution < prchTaxDistributions.Count)
                            {
                                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                                {
                                    prchTaxDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                                }
                            }
                        }
                        else
                        {
                            //no transactions exist for this claim; thus PLT and MT-GOTR Amounts are only from Payment Inventory Adjustments
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)adjustHstAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = adjustHstAccount.AccountNumber; //OTSTM2-1124
                            rowDistribution.DEBITAMT = prchTax1;
                            rowDistribution.CRDTAMNT = 0;
                            typeOneDistrubutions.Add(rowDistribution);
                        }


                    }
                    typeOneDistrubutions.AddRange(prchTaxDistributions);
                }
            }

            if (adjustAmount > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)adjustAccount.distribution_type_id;
                rowDistribution.ACTNUMST = adjustAccount.AccountNumber; //OTSTM2-1124
                rowDistribution.DEBITAMT = adjustAmount;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }

            if (adjustTaxAmount > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)adjustHstAccount.distribution_type_id;
                rowDistribution.ACTNUMST = adjustHstAccount.AccountNumber; //OTSTM2-1124
                rowDistribution.DEBITAMT = adjustTaxAmount;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }

            return typeOneDistrubutions;
        }

        private List<RrOtsPmTransDistribution> AddTypeFiveDistributions(int gpiBatchId, string INTERID, decimal prchAmount5, decimal prchTax5, RrOtsPmTransaction typeFiveTransaction, List<FIAccount> fiAccounts, ref int dstSeqNum)
        {
            //4196-90-20-40   CA - TM Adjustment HST    //10 Collector Adjustments Tax
            //4195-90-20-40   CA - TM Adjustments       //6  Collector Adjustments
            var typeFiveDistributions = new List<RrOtsPmTransDistribution>();
            //var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "C");
            //var adjustAccount = gpiAccounts.Single(a => a.account_number == "4195-90-20-40");
            //var adjustHstAccount = gpiAccounts.Single(a => a.account_number == "4196-90-20-40");

            //OTSTM2-1124
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Collector Payable" && a.AdminFICategoryID == 2);
            var adjustAccount = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 2);
            var adjustHstAccount = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 2);

            if (prchAmount5 != 0 || prchTax5 != 0)
            {
                var rowAccountPayable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayable.DEBITAMT = Math.Abs(prchAmount5) + Math.Abs(prchTax5);
                rowAccountPayable.CRDTAMNT = 0;
                typeFiveDistributions.Add(rowAccountPayable);
            }
            if (prchAmount5 != 0)
            {
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)adjustAccount.distribution_type_id;
                rowTIAdjustment.ACTNUMST = adjustAccount.AccountNumber; //OTSTM2-1124
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = Math.Abs(prchAmount5);
                typeFiveDistributions.Add(rowTIAdjustment);
            }
            if (prchTax5 != 0)
            {
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)adjustHstAccount.distribution_type_id;
                rowTIAdjustment.ACTNUMST = adjustHstAccount.AccountNumber; //OTSTM2-1124
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = Math.Abs(prchTax5);
                typeFiveDistributions.Add(rowTIAdjustment);
            }
            return typeFiveDistributions;
        }

        private GpTireItem FindTirePercentage(List<ClaimDetailViewModel> claimDetails, CollectorPaymentViewModel collectorPaymentViewModel)
        {
            var eligibleClaimDetails = claimDetails.Where(c => c.IsEligible).ToList();
            //pull internal adjustment --- ClaimInventoryAdjustItem   ClaimInventoryAdjustment
            var claimID = claimDetails.FirstOrDefault() != null ? claimDetails.FirstOrDefault().ClaimId : 0;
            var eligibleInternalAdjustment = claimsRepository.LoadClaimInventoryAdjustments(claimID).Where(x => x.IsEligible).ToList();

            var gpTireItem = new GpTireItem();

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            var pltItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                pltItemQty += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            pltItemQty += GetInternalAdjustment(pltItem.ID, eligibleInternalAdjustment);

            gpTireItem.PLT = pltItemQty;
            gpTireItem.PLT_standeredWeight = collectorPaymentViewModel.PLTRate;
            //gpTireItem.PLTWeight = pltItemQty * collectorPaymentViewModel.PLTRate;

            var mtItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                mtItemQty += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            mtItemQty += GetInternalAdjustment(mtItem.ID, eligibleInternalAdjustment);

            gpTireItem.MT = mtItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.MT_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.MTWeight = mtItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.MT_standeredWeight = collectorPaymentViewModel.MTRate;
                //gpTireItem.MTWeight = mtItemQty * collectorPaymentViewModel.MTRate;
            }

            var aglsItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                aglsItemQty += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            aglsItemQty += GetInternalAdjustment(aglsItem.ID, eligibleInternalAdjustment);

            gpTireItem.AGLS = aglsItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.AGLS_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.AGLSWeight = aglsItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.AGLS_standeredWeight = collectorPaymentViewModel.AGLSRate;
                //gpTireItem.AGLSWeight = aglsItemQty * collectorPaymentViewModel.AGLSRate;
            }

            var indItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                indItemQty += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            indItemQty += GetInternalAdjustment(indItem.ID, eligibleInternalAdjustment);

            gpTireItem.IND = indItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.IND_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.INDWeight = indItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.IND_standeredWeight = collectorPaymentViewModel.INDRate;
                //gpTireItem.INDWeight = indItemQty * collectorPaymentViewModel.INDRate;
            }

            var sotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                sotrItemQty += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            sotrItemQty += GetInternalAdjustment(sotrItem.ID, eligibleInternalAdjustment);

            gpTireItem.SOTR = sotrItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.SOTR_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.SOTRWeight = sotrItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.SOTR_standeredWeight = collectorPaymentViewModel.SOTRRate;
                //gpTireItem.SOTRWeight = sotrItemQty * collectorPaymentViewModel.SOTRRate;
            }

            var motrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                motrItemQty += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            motrItemQty += GetInternalAdjustment(motrItem.ID, eligibleInternalAdjustment);

            gpTireItem.MOTR = motrItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.MOTR_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.MOTRWeight = motrItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.MOTR_standeredWeight = collectorPaymentViewModel.MOTRRate;
                //gpTireItem.MOTRWeight = motrItemQty * collectorPaymentViewModel.MOTRRate;
            }

            var lotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                lotrItemQty += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            lotrItemQty += GetInternalAdjustment(lotrItem.ID, eligibleInternalAdjustment);

            gpTireItem.LOTR = lotrItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.LOTR_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.LOTRWeight = lotrItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.LOTR_standeredWeight = collectorPaymentViewModel.LOTRRate;
                //gpTireItem.LOTRWeight = lotrItemQty * collectorPaymentViewModel.LOTRRate;
            }

            var gotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                gotrItemQty += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            //#1040 Collector GP Distribution for claim which contains Eligible Tire Count missing internal adjustments
            gotrItemQty += GetInternalAdjustment(gotrItem.ID, eligibleInternalAdjustment);

            gpTireItem.GOTR = gotrItemQty;
            if (collectorPaymentViewModel.UsingOldMTGOTRPayment)
            {
                gpTireItem.GOTR_standeredWeight = collectorPaymentViewModel.MTGOTRRate;
                //gpTireItem.GOTRWeight = gotrItemQty * collectorPaymentViewModel.MTGOTRRate;
            }
            else
            {
                gpTireItem.GOTR_standeredWeight = collectorPaymentViewModel.GOTRRate;
                //gpTireItem.GOTRWeight = gotrItemQty * collectorPaymentViewModel.GOTRRate;
            }

            return gpTireItem;
        }
        private int GetInternalAdjustment(int itemID, List<ClaimInventoryAdjustment> adjustment)
        {
            int itemQty = 0;
            adjustment.ForEach(c =>
              {
                  itemQty += (int)c.ClaimInventoryAdjustItems.Where(x => x.ItemId == itemID).Sum(q => q.QtyAdjustment);
              });
            return itemQty;
        }
        private List<GpiBatchEntry> GetBatchEntries(List<Claim> claims, int gpiBatchId)
        {
            return claims.Select(c => new GpiBatchEntry
            {
                GpiBatchID = gpiBatchId,
                GpiTxnNumber = string.Format("{0}-{1}-{2:yyyy MM dd}", c.ID.ToString(), c.Participant.Number, c.ClaimPeriod.StartDate),
                GpiBatchEntryDataKey = c.ID.ToString(),
                GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                CreatedDate = DateTime.Now,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.Now,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                ClaimId = c.ID
            }).ToList();
        }

        //OTSTM2-434
        public bool IsStaffTotalInboundOutbound(int claimId)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            var collectorClaimSummary = new CollectorClaimSummaryModel();
            //Load Tire Origin
            var tireOrigins = claimsRepository.LoadClaimTireOrigins(claim.ID);
            PopulateInbound(collectorClaimSummary, tireOrigins);

            //Load outbound
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claim.ID, items);
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claim.ID);
            PopulateOutbound(collectorClaimSummary, claimDetails, inventoryAdjustments);

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>{
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"TotalInbound", collectorClaimSummary.TotalInbound},
                {"TotalOutbound", collectorClaimSummary.TotalOutbound}
            };

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new CollectorInboundOutboundMatchClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (BusinessRuleExecutioResult.Errors.Count > 0)
            {
                return false;
            }

            return true;
        }
    }
}
