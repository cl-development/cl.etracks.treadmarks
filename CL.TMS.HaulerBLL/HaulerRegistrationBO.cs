﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;

namespace CL.TMS.HaulerBLL
{
    public class HaulerRegistrationBO
    {
        private IApplicationRepository applicationRepository;
        private IItemRepository itemRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private IClaimsRepository claimsRepository;

        public HaulerRegistrationBO(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository, IClaimsRepository claimsRepository)
        {
            this.applicationRepository = applicationRepository;
            this.itemRepository = itemRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.claimsRepository = claimsRepository;
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            applicationRepository.UpdateFormObject(id, formObject, updatedBy);
        }

        public void UpdateApplication(Application application)
        {
            applicationRepository.UpdateApplication(application);
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>HaulerRegistrationModel based on FormObject column in Application table</returns>
        public HaulerRegistrationModel GetFormObject(int id)
        {
            var application = applicationRepository.GetSingleApplication(id);
            if (application == null)
            {
                throw new KeyNotFoundException();
            }

            HaulerRegistrationModel result = null;
            var applicationStatus = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            if (applicationStatus == ApplicationStatusEnum.Approved
                || applicationStatus == ApplicationStatusEnum.BankInformationSubmitted
                || applicationStatus == ApplicationStatusEnum.BankInformationApproved
                || applicationStatus == ApplicationStatusEnum.Completed)
            {
                var vendor = vendorRepository.GetSingleWithApplicationID(id);
                result = GetApprovedApplication(vendor.ID);
                result.ID = id;
                result.VendorID = vendor.ID;
            }
            else
            {
                string formObject = application.FormObject;
                if (formObject != null)
                {
                    result = JsonConvert.DeserializeObject<HaulerRegistrationModel>(formObject);
                }
                else
                {
                    result = new HaulerRegistrationModel(id);
                }
            }
            if (application.AssignToUser.HasValue)
            {
                result.AssignToUserId = application.AssignToUser.Value;
            }
            result.Status = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            result.SubmittedDate = application.SubmittedDate;
            result.ExpireDate = application.ExpireDate;
            //OTSTM2-973 TermsAndConditons
            result.TermsAndConditions.TermsAndConditionsID = application.TermsAndConditionsID;

            return result;
        }

        public HaulerRegistrationModel GetAllItemsList()
        {
            var result = new HaulerRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Hauler)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            var ListItemModel = new List<TireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new TireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new TireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public Application GetApplicationById(int appId)
        {
            var result = applicationRepository.GetSingleApplication(appId);
            return result;
        }

        public void RemoveEmptyContacts(ref IList<ContactRegistrationModel> contacts, ref IList<string> invalidFieldList)
        {
            if (contacts != null && contacts.Count() > 1)
            {
                List<ContactRegistrationModel> results = new List<ContactRegistrationModel>() { contacts[0] };
                IList<string> resultsFieldList = invalidFieldList.ToList();

                int totalContacts = contacts.Count();
                for (int i = 1; i < totalContacts; i++)
                {
                    bool remove = false;
                    if (contacts[i].ContactInformation != null)
                    {
                        var contact = contacts[i].ContactInformation;
                        remove = string.IsNullOrWhiteSpace(contact.FirstName) &&
                                  string.IsNullOrWhiteSpace(contact.LastName) &&
                                  string.IsNullOrWhiteSpace(contact.Position) &&
                                  string.IsNullOrWhiteSpace(contact.PhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Ext) &&
                                  string.IsNullOrWhiteSpace(contact.AlternatePhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Email);
                        if (remove)
                        {
                            //check if contact address has any information
                            if (!contacts[i].ContactAddressSameAsBusinessAddress)
                            {
                                if (contacts[i].ContactAddress != null)
                                {
                                    var address = contacts[i].ContactAddress;
                                    remove = !contact.ContactAddressSameAsBusiness &&
                                            string.IsNullOrWhiteSpace(address.AddressLine1) &&
                                            string.IsNullOrWhiteSpace(address.AddressLine2) &&
                                            string.IsNullOrWhiteSpace(address.City) &&
                                            string.IsNullOrWhiteSpace(address.Postal) &&
                                            string.IsNullOrWhiteSpace(address.Province) &&
                                            string.IsNullOrWhiteSpace(address.Country);
                                }
                            }
                        }
                    }
                    if (remove)
                    {
                        //remove items from invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                            }
                        }
                    }
                    else
                    {
                        results.Add(contacts[i]);
                        int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                        if (numRemoved > 0)
                        {
                            //replace all the names in invalidlist
                            foreach (string nameStr in invalidFieldList)
                            {
                                string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                                Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    resultsFieldList.Remove(nameStr);
                                    string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                    resultsFieldList.Add(newName);
                                }
                            }
                        }
                    }
                }
                contacts = results;
                invalidFieldList = resultsFieldList;
            }
        }

        public void RemoveEmptySortYardDetails(ref IList<SortYardDetailsRegistrationModel> sortyardDetails, ref IList<string> invalidFieldList)
        {
            if (sortyardDetails.Count() == 0)
            {
                SortYardDetailsRegistrationModel newItem = new SortYardDetailsRegistrationModel();
                sortyardDetails.Add(newItem);
            }

            List<SortYardDetailsRegistrationModel> results = new List<SortYardDetailsRegistrationModel>() { sortyardDetails[0] };
            IList<string> resultsFieldList = invalidFieldList.ToList();

            int totalSortYardDetails = sortyardDetails.Count();
            for (int i = 1; i < totalSortYardDetails; i++)
            {
                bool remove = false;
                if (sortyardDetails[i].SortYardAddress != null)
                {
                    var address = sortyardDetails[i].SortYardAddress;
                    remove = string.IsNullOrEmpty(address.AddressLine1) &&
                             string.IsNullOrEmpty(address.AddressLine2) &&
                             string.IsNullOrEmpty(address.City) &&
                             string.IsNullOrEmpty(address.Postal) &&
                             string.IsNullOrEmpty(address.Province) &&
                             string.IsNullOrEmpty(address.Country);

                    if (remove)
                    {
                        remove = string.IsNullOrEmpty(sortyardDetails[i].CertificateOfApprovalNumber) &&
                                     (string.IsNullOrEmpty(sortyardDetails[i].MaxStorageCapacity) || string.Compare(sortyardDetails[i].MaxStorageCapacity, "0", true) == 0);
                    }
                }
                if (remove)
                {
                    //remove items from invalidlist
                    foreach (string nameStr in invalidFieldList)
                    {
                        string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                        Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            resultsFieldList.Remove(nameStr);
                        }
                    }
                }
                else
                {
                    results.Add(sortyardDetails[i]);
                    int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                    if (numRemoved > 0)
                    {
                        //replace all the names in invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                                string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                resultsFieldList.Add(newName);
                            }
                        }
                    }
                }
            }
            sortyardDetails = results;
            invalidFieldList = resultsFieldList;
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>HaulerRegistrationModel based on FormObject column in Application table</returns>

        public HaulerRegistrationModel GetByTokenId(Guid tokenId)
        {
            var application = applicationRepository.GetApplicationByTokenId(tokenId);
            return GetFormObject(application.ID);
        }

        public ApplicationEmailModel GetApprovedHaulerApplicantInformation(int applicationId)
        {
            //var application = this.applicationRepository.FindApplicationByAppID(applicationId);
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }

            var applicationHaulerApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                PrimaryContactLastName = vendorPrimaryContact.LastName,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = applicationInvitationRepository.GetTokenByApplicationId(applicationId)
            };

            return applicationHaulerApplicationModel;
        }

        #region Workflow

        /// <summary>
        /// updating the entities after approve selection
        /// </summary>
        /// <param name="applicationHaulerModel"></param>
        /// <param name="appID"></param>
        public int ApproveApplication(HaulerRegistrationModel applicationHaulerModel, int appID)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var app = this.applicationRepository.FindApplicationByAppID(appID);
            app.SigningAuthorityFirstName = applicationHaulerModel.TermsAndConditions.SigningAuthorityFirstName;
            app.SigningAuthorityLastName = applicationHaulerModel.TermsAndConditions.SigningAuthorityLastName;
            app.SigningAuthorityPosition = applicationHaulerModel.TermsAndConditions.SigningAuthorityPosition;
            app.FormCompletedByFirstName = applicationHaulerModel.TermsAndConditions.FormCompletedByFirstName;
            app.FormCompletedByLastName = applicationHaulerModel.TermsAndConditions.FormCompletedByLastName;
            app.FormCompletedByPhone = applicationHaulerModel.TermsAndConditions.FormCompletedByPhone;
            app.AgreementAcceptedByFullName = applicationHaulerModel.TermsAndConditions.AgreementAcceptedByFullName;
            app.Status = ApplicationStatusEnum.Approved.ToString();
            app.FormObject = app.FormObject;

            AddApplicationSupportingDocuments(applicationHaulerModel, app);

            var vendor = new Vendor
            {
                BusinessName = applicationHaulerModel.BusinessLocation.BusinessName,
                OperatingName = applicationHaulerModel.BusinessLocation.OperatingName,
                BusinessStartDate = applicationHaulerModel.HaulerDetails.BusinessStartDate,
                BusinessNumber = applicationHaulerModel.HaulerDetails.BusinessNumber,
                CommercialLiabHSTNumber = applicationHaulerModel.HaulerDetails.CommercialLiabHstNumber,
                IsTaxExempt = applicationHaulerModel.HaulerDetails.IsTaxExempt,
                CommercialLiabInsurerName = applicationHaulerModel.HaulerDetails.CommercialLiabInsurerName,
                CommercialLiabInsurerExpDate = applicationHaulerModel.HaulerDetails.CommercialLiabInsurerExpDate,
                HIsGVWR = applicationHaulerModel.HaulerDetails.HIsGvwr,
                CVORNumber = applicationHaulerModel.HaulerDetails.CvorNumber,
                CVORExpiryDate = applicationHaulerModel.HaulerDetails.CvorExpiryDate,
                HasMoreThanOneEmp = applicationHaulerModel.HaulerDetails.HasMoreThanOneEmp,
                WSIBNumber = applicationHaulerModel.HaulerDetails.WsibNumber,
                ApplicationID = app.ID,
                CreatedDate = DateTime.UtcNow,
                VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue,
                LastUpdatedBy = updatedBy,
                LastUpdatedDate = DateTime.UtcNow,
                ActiveStateChangeDate = DateTime.Now.Date,
                ActivationDate = DateTime.Now.Date,
                IsActive = true,
                ContactInfo = string.Format("{0} {1}, {2}", applicationHaulerModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationHaulerModel.ContactInformationList[0].ContactInformation.LastName, applicationHaulerModel.ContactInformationList[0].ContactInformation.PhoneNumber),
                //added

                RegistrantStatus = ApplicationStatusEnum.Approved.ToString(),
                SigningAuthorityFirstName = applicationHaulerModel.TermsAndConditions.SigningAuthorityFirstName,
                SigningAuthorityLastName = applicationHaulerModel.TermsAndConditions.SigningAuthorityLastName,
                SigningAuthorityPosition = applicationHaulerModel.TermsAndConditions.SigningAuthorityPosition,
                FormCompletedByFirstName = applicationHaulerModel.TermsAndConditions.FormCompletedByFirstName,
                FormCompletedByLastName = applicationHaulerModel.TermsAndConditions.FormCompletedByLastName,
                FormCompletedByPhone = applicationHaulerModel.TermsAndConditions.FormCompletedByPhone,
                AgreementAcceptedByFullName = applicationHaulerModel.TermsAndConditions.AgreementAcceptedByFullName,
                AgreementAcceptedCheck = applicationHaulerModel.TermsAndConditions.AgreementAcceptedCheck,
            };

            MoveAttachmentToVendor(applicationHaulerModel, vendor);

            var vendorPrimaryAddress = new Address()
            {
                Address1 = applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine1,
                Address2 = applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine2,
                City = applicationHaulerModel.BusinessLocation.BusinessAddress.City,
                Province = applicationHaulerModel.BusinessLocation.BusinessAddress.Province,
                PostalCode = applicationHaulerModel.BusinessLocation.BusinessAddress.Postal,
                Country = applicationHaulerModel.BusinessLocation.BusinessAddress.Country,
                Phone = applicationHaulerModel.BusinessLocation.Phone,
                Email = applicationHaulerModel.BusinessLocation.Email,
                Ext = applicationHaulerModel.BusinessLocation.Extension,
                AddressType = (int)AddressTypeEnum.Business
            };

            vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (applicationHaulerModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = new Address
                {
                    Address1 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1,
                    Address2 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine2,
                    City = applicationHaulerModel.BusinessLocation.MailingAddress.City,
                    Province = applicationHaulerModel.BusinessLocation.MailingAddress.Province,
                    PostalCode = applicationHaulerModel.BusinessLocation.MailingAddress.Postal,
                    Country = applicationHaulerModel.BusinessLocation.MailingAddress.Country,
                    Phone = applicationHaulerModel.BusinessLocation.Phone,
                    Email = applicationHaulerModel.BusinessLocation.Email,
                    AddressType = (int)AddressTypeEnum.Mail
                };

                vendor.VendorAddresses.Add(vendorMailingAddress);
            }

            var firstVendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
            if (firstVendorAddress != null)
            {
                for (var i = 0; i < applicationHaulerModel.ContactInformationList.Count; i++)
                {

                    if (applicationHaulerModel.ContactInformationList[i].ContactInformation.FirstName != null &&
                        applicationHaulerModel.ContactInformationList[i].ContactInformation.LastName != null)
                    {
                        var contact = new Contact()
                        {
                            FirstName = applicationHaulerModel.ContactInformationList[i].ContactInformation.FirstName,
                            LastName = applicationHaulerModel.ContactInformationList[i].ContactInformation.LastName,
                            Position = applicationHaulerModel.ContactInformationList[i].ContactInformation.Position,
                            PhoneNumber =
                                applicationHaulerModel.ContactInformationList[i].ContactInformation.PhoneNumber,
                            Ext = applicationHaulerModel.ContactInformationList[i].ContactInformation.Ext,
                            AlternatePhoneNumber =
                                applicationHaulerModel.ContactInformationList[i].ContactInformation.AlternatePhoneNumber,
                            Email = applicationHaulerModel.ContactInformationList[i].ContactInformation.Email,
                            IsPrimary = i == 0,
                        };

                        if (!applicationHaulerModel.ContactInformationList[i].ContactAddressSameAsBusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(applicationHaulerModel.ContactInformationList[i].ContactAddress.AddressLine1))
                            {
                                var contactAddress = new Address()
                                {
                                    Address1 =
                                        applicationHaulerModel.ContactInformationList[i].ContactAddress.AddressLine1,
                                    Address2 =
                                        applicationHaulerModel.ContactInformationList[i].ContactAddress.AddressLine2,
                                    City = applicationHaulerModel.ContactInformationList[i].ContactAddress.City,
                                    Province = applicationHaulerModel.ContactInformationList[i].ContactAddress.Province,
                                    PostalCode = applicationHaulerModel.ContactInformationList[i].ContactAddress.Postal,
                                    Country = string.IsNullOrWhiteSpace(applicationHaulerModel.ContactInformationList[i].ContactAddress.Country) ? "Canada" : applicationHaulerModel.ContactInformationList[i].ContactAddress.Country,
                                    AddressType = (int)AddressTypeEnum.Contact
                                };
                                contactAddress.Contacts.Add(contact);
                                vendor.VendorAddresses.Add(contactAddress);
                            }
                            else
                            {
                                firstVendorAddress.Contacts.Add(contact);
                            }
                        }
                        else
                        {
                            firstVendorAddress.Contacts.Add(contact);
                        }
                    }
                }
            }
            var addresses = new List<Address>();
            foreach (var sortYard in applicationHaulerModel.SortYardDetails)
            {
                sortYard.StorageSiteType = 1;
                if (!string.IsNullOrEmpty(sortYard.SortYardAddress.AddressLine1))
                {
                    var found = false;
                    foreach (var eachAddress in vendor.VendorAddresses)
                    {
                        if ((string.Compare(sortYard.SortYardAddress.AddressLine1, eachAddress.Address1, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.AddressLine2, eachAddress.Address2, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.City, eachAddress.City, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.Province, eachAddress.Province, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Postal, eachAddress.PostalCode, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Country, eachAddress.Country, StringComparison.OrdinalIgnoreCase) == 0))
                        {
                            double maxSortYardCapacity;
                            double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                            VendorStorageSite vS = new VendorStorageSite()
                            {
                                MaxStorageCapacity = maxSortYardCapacity,
                                CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                                StorageSiteType = 1
                            };
                            eachAddress.VendorStorageSites.Add(vS);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        var sortYardAddress = new Address();

                        sortYardAddress.Address1 = sortYard.SortYardAddress.AddressLine1;
                        sortYardAddress.Address2 = sortYard.SortYardAddress.AddressLine2;
                        sortYardAddress.City = sortYard.SortYardAddress.City;
                        sortYardAddress.PostalCode = sortYard.SortYardAddress.Postal;
                        sortYardAddress.Province = sortYard.SortYardAddress.Province;
                        sortYardAddress.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country)
                                ? "Canada"
                                : sortYard.SortYardAddress.Country;
                        sortYardAddress.AddressType = (int)AddressTypeEnum.Sortyard;
                        sortYardAddress.Contacts = null;

                        double maxSortYardCapacity;
                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        VendorStorageSite vS = new VendorStorageSite()
                        {
                            MaxStorageCapacity = maxSortYardCapacity,
                            CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                            StorageSiteType = 1
                        };

                        sortYardAddress.VendorStorageSites.Add(vS);

                        addresses.Add(sortYardAddress);
                    }
                }
            }

            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            //vendoractivehistory
            var vendorActiveHistory = new VendorActiveHistory()
            {
                ActiveState = true,
                ActiveStateChangeDate = DateTime.Now.Date,
                VendorID = vendor.ID,
                CreateDate = DateTime.UtcNow,
                IsValid = true
            };

            var currentUser = TMSContext.TMSPrincipal.Identity;
            vendorActiveHistory.CreatedBy = currentUser.Name;

            vendor.VendorActiveHistories.Add(vendorActiveHistory);

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Hauler)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            applicationHaulerModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = defaultItems.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            });

            vendor.HHasRelatioshipWithProcessor = applicationHaulerModel.TireDetails.HHasRelatioshipWithProcessor;
            vendor.HRelatedProcessor = applicationHaulerModel.TireDetails.HRelatedProcessor;

            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                applicationRepository.UpdateApplicationEntity(app);
                vendor.Number = vendorRepository.GetVendorRegistrationNumber(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue).ToString();
                vendorRepository.AddVendor(vendor);

                //Add VendorAttachments
                var vendorAttachments = new List<VendorAttachment>();
                app.Attachments.ToList().ForEach(c =>
                {
                    var vendorAttachment = new VendorAttachment();
                    vendorAttachment.AttachmentId = c.ID;
                    vendorAttachment.VendorId = vendor.ID;
                    vendorAttachments.Add(vendorAttachment);
                });
                vendorRepository.AddVendorAttachments(vendorAttachments);
                vendorRepository.UpdateVendorKeywords(vendor);
                //Update applicationnote
                applicationRepository.UpdateApplicatioNoteWithVendorId(vendor.ID, app.ID);

                //Initialize Claim for approved application
                //ET-37 Stop creating claims
                //claimsRepository.InitializeClaimForVendor(vendor.ID);

                transationScope.Complete();
            }

            return vendor.ID;
        }

        private void AddApplicationSupportingDocuments(HaulerRegistrationModel applicationHaulerModel, Application app)
        {
            var applicationSupportingDocumentMBL = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentMBL);

            var applicationSupportingDocumentCLI = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCLI);

            var applicationSupportingDocumentCVOR = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCVOR,
                TypeID = (int)SupportingDocumentTypeEnum.CVOR
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCVOR);

            var applicationSupportingDocumentPRL = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionPRL,
                TypeID = (int)SupportingDocumentTypeEnum.PRL
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentPRL);

            var applicationSupportingDocumentHST = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentHST);

            var applicationSupportingDocumentWSIB = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentWSIB);

            var applicationSupportingDocumentCHQ = new ApplicationSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCHQ);
        }

        private void MoveAttachmentToVendor(HaulerRegistrationModel applicationHaulerModel, Vendor vendor)
        {
            //Moving supporting document options
            var vendorSupportingDocumentMBL = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentMBL);

            var vendorSupportingDocumentCLI = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCLI);

            var vendorSupportingDocumentCVOR = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCVOR,
                TypeID = (int)SupportingDocumentTypeEnum.CVOR
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCVOR);

            var vendorSupportingDocumentPRL = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionPRL,
                TypeID = (int)SupportingDocumentTypeEnum.PRL
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentPRL);

            var vendorSupportingDocumentHST = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentHST);

            var vendorSupportingDocumentWSIB = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentWSIB);

            var vendorSupportingDocumentCHQ = new VendorSupportingDocument()
            {
                Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCHQ);
        }

        public int UpdateApproveApplication(HaulerRegistrationModel applicationHaulerModel, int vendorId)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var deleteAddresses = new List<Address>(); //OTSTM2-642 use existing list to collect sortyard addresses that to be deleted
            var newContactAddresses = new List<ContactAddressDto>();
            var deleteContacts = new List<Contact>(); //OTSTM2-50

            var vendor = vendorRepository.FindVendorByID(vendorId);

            if (vendor != null)
            {
                if (!(vendor.GpUpdate ?? false))
                {
                    var participantExisting = new GpParticipantUpdate(vendor);
                    var newContact = applicationHaulerModel.ContactInformationList.First();
                    var participantNew = new GpParticipantUpdate()
                    {
                        MailingAddress1 = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine1 : applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1,
                        MailingAddress2 = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine2 : applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine2,
                        MailingCity = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.City : applicationHaulerModel.BusinessLocation.MailingAddress.City,
                        MailingCountry = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.Country : applicationHaulerModel.BusinessLocation.MailingAddress.Country,
                        MailingPostalCode = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.Postal : applicationHaulerModel.BusinessLocation.MailingAddress.Postal,
                        MailingProvince = applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationHaulerModel.BusinessLocation.BusinessAddress.Province : applicationHaulerModel.BusinessLocation.MailingAddress.Province,
                        PrimaryContactName = string.Format("{0} {1}", newContact.ContactInformation.FirstName, newContact.ContactInformation.LastName),
                        PrimaryContactPhone = newContact.ContactInformation.PhoneNumber ?? string.Empty,
                        IsTaxExempt = applicationHaulerModel.HaulerDetails.IsTaxExempt,
                        IsActive = vendor.IsActive
                    };
                    if (!participantExisting.Equals(participantNew))
                    {
                        vendor.GpUpdate = true;
                    }
                }
                vendor.BusinessName = applicationHaulerModel.BusinessLocation.BusinessName;
                vendor.OperatingName = applicationHaulerModel.BusinessLocation.OperatingName;
                vendor.BusinessStartDate = applicationHaulerModel.HaulerDetails.BusinessStartDate;
                vendor.BusinessNumber = applicationHaulerModel.HaulerDetails.BusinessNumber;
                vendor.CommercialLiabHSTNumber = applicationHaulerModel.HaulerDetails.CommercialLiabHstNumber;
                vendor.IsTaxExempt = applicationHaulerModel.HaulerDetails.IsTaxExempt;
                vendor.CommercialLiabInsurerName = applicationHaulerModel.HaulerDetails.CommercialLiabInsurerName;
                vendor.CommercialLiabInsurerExpDate = applicationHaulerModel.HaulerDetails.CommercialLiabInsurerExpDate;
                vendor.HIsGVWR = applicationHaulerModel.HaulerDetails.HIsGvwr;
                vendor.CVORNumber = applicationHaulerModel.HaulerDetails.CvorNumber;
                vendor.CVORExpiryDate = applicationHaulerModel.HaulerDetails.CvorExpiryDate;
                vendor.HasMoreThanOneEmp = applicationHaulerModel.HaulerDetails.HasMoreThanOneEmp;
                vendor.WSIBNumber = applicationHaulerModel.HaulerDetails.WsibNumber;
                vendor.Number = Convert.ToString(applicationHaulerModel.RegistrationNumber);
                vendor.VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue;
                vendor.LastUpdatedBy = updatedBy;
                vendor.LastUpdatedDate = DateTime.UtcNow;
                vendor.ContactInfo = string.Format("{0} {1}, {2}", applicationHaulerModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationHaulerModel.ContactInformationList[0].ContactInformation.LastName, applicationHaulerModel.ContactInformationList[0].ContactInformation.PhoneNumber);

                //added
                vendor.SigningAuthorityFirstName = applicationHaulerModel.TermsAndConditions.SigningAuthorityFirstName;
                vendor.SigningAuthorityLastName = applicationHaulerModel.TermsAndConditions.SigningAuthorityLastName;
                vendor.SigningAuthorityPosition = applicationHaulerModel.TermsAndConditions.SigningAuthorityPosition;
                vendor.FormCompletedByFirstName = applicationHaulerModel.TermsAndConditions.FormCompletedByFirstName;
                vendor.FormCompletedByLastName = applicationHaulerModel.TermsAndConditions.FormCompletedByLastName;
                vendor.FormCompletedByPhone = applicationHaulerModel.TermsAndConditions.FormCompletedByPhone;
                vendor.AgreementAcceptedByFullName = applicationHaulerModel.TermsAndConditions.AgreementAcceptedByFullName;
                vendor.AgreementAcceptedCheck = applicationHaulerModel.TermsAndConditions.AgreementAcceptedCheck;


                //Save vendor supporting document
                UpdateVendorSupportingDocuments(applicationHaulerModel, vendor);

            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                vendorPrimaryAddress.Address1 = applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine1;
                vendorPrimaryAddress.Address2 = applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine2;
                vendorPrimaryAddress.City = applicationHaulerModel.BusinessLocation.BusinessAddress.City;
                vendorPrimaryAddress.Province = applicationHaulerModel.BusinessLocation.BusinessAddress.Province;
                vendorPrimaryAddress.PostalCode = applicationHaulerModel.BusinessLocation.BusinessAddress.Postal;
                vendorPrimaryAddress.Country = applicationHaulerModel.BusinessLocation.BusinessAddress.Country;
                vendorPrimaryAddress.Phone = applicationHaulerModel.BusinessLocation.Phone;
                vendorPrimaryAddress.Email = applicationHaulerModel.BusinessLocation.Email;
                vendorPrimaryAddress.AddressType = (int)AddressTypeEnum.Business;
                vendorPrimaryAddress.Ext = applicationHaulerModel.BusinessLocation.Extension;
            }

            //vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (applicationHaulerModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);

                if (vendorMailingAddress != null)
                {
                    vendorMailingAddress.Address1 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1;
                    vendorMailingAddress.Address2 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine2;
                    vendorMailingAddress.City = applicationHaulerModel.BusinessLocation.MailingAddress.City;
                    vendorMailingAddress.Province = applicationHaulerModel.BusinessLocation.MailingAddress.Province;
                    vendorMailingAddress.PostalCode = applicationHaulerModel.BusinessLocation.MailingAddress.Postal;
                    vendorMailingAddress.Country = applicationHaulerModel.BusinessLocation.MailingAddress.Country;
                    vendorMailingAddress.Phone = applicationHaulerModel.BusinessLocation.Phone;
                    vendorMailingAddress.Email = applicationHaulerModel.BusinessLocation.Email;
                }
                else
                {
                    var mailingAddress = new Address()
                    {
                        Address1 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1,
                        Address2 = applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine2,
                        City = applicationHaulerModel.BusinessLocation.MailingAddress.City,
                        Province = applicationHaulerModel.BusinessLocation.MailingAddress.Province,
                        PostalCode = applicationHaulerModel.BusinessLocation.MailingAddress.Postal,
                        Country = applicationHaulerModel.BusinessLocation.MailingAddress.Country,
                        Phone = applicationHaulerModel.BusinessLocation.Phone,
                        Email = applicationHaulerModel.BusinessLocation.Email,
                        AddressType = (int)AddressTypeEnum.Mail
                    };

                    vendor.VendorAddresses.Add(mailingAddress);
                }
            }

            else
            {
                //OTSTM2-476
                var currentMailingAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == (int)AddressTypeEnum.Mail);

                if (currentMailingAddress != null)
                {
                    vendor.VendorAddresses.Remove(currentMailingAddress);
                }
            }

            //OTSTM2-50 remove contact
            var contactIdList = applicationHaulerModel.ContactInformationList.Select(c => c.ContactInformation.ID).ToList();
            foreach (var contactAddress in vendor.VendorAddresses.ToList())
            {
                foreach (var contact in contactAddress.Contacts.ToList())
                {
                    if (!contactIdList.Contains(contact.ID))
                    {
                        deleteContacts.Add(contact);
                    }
                }
            }

            foreach (var applicationContactInfo in applicationHaulerModel.ContactInformationList)
            {
                Contact contact = null;

                if (applicationContactInfo.isEmptyContact)
                {
                    var contactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                    contact = contactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                    contactAddress.Contacts.Remove(contact);
                    vendor.VendorAddresses.Remove(contactAddress);
                }
                else
                {
                    #region contact
                    //check if this is a new contact and address
                    if (applicationContactInfo.ContactInformation.AddressID == 0)
                    {
                        contact = new Contact() { IsPrimary = false };
                        ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        if (!applicationContactInfo.ContactInformation.ContactAddressSameAsBusiness)
                        {
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contactAddress.Contacts.Add(contact);
                            vendor.VendorAddresses.Add(contactAddress);
                        }
                        else
                        {
                            var vendorBusinessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationHaulerModel.BusinessLocation.BusinessAddress.ID);
                            vendorBusinessAddress.Contacts.Add(contact);
                            vendorRepository.UpdateVendor(vendor); //OTSTM2-1037 make sure contact 1 with same as Business Address checked to be saved earlier, so that has a smaller ID value
                        }
                    }
                    else
                    {
                        var currentContactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                        var businessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationHaulerModel.BusinessLocation.BusinessAddress.ID);
                        var oldIsAddressSameAsBusiness = currentContactAddress.AddressType == (int)AddressTypeEnum.Business;
                        var newIsAddressSameAsBusiness = applicationContactInfo.ContactAddressSameAsBusinessAddress;

                        if (newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //no change update contact information
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                        else if (newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //changed selection to same as business
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            contact.AddressID = businessAddress.ID;
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = businessAddress
                            });
                            deleteAddresses.Add(contact.Address);
                        }
                        else if (!newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //remove from business address and add new address
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = contactAddress
                            });
                        }
                        else if (!newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //no change update address and contact information
                            ModelAdapter.UpdateAddress(ref currentContactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                    }
                    #endregion
                }
            }

            //OTSTM2-642 remove sort yard
            var sortYardIdList = applicationHaulerModel.SortYardDetails.Select(c => c.SortYardAddress.ID).ToList(); //SortYardAddress.ID and VendorStorageSite.ID are same thing
            foreach (var vendorStorageSite in vendor.VendorStorageSites.ToList())
            {
                if (!sortYardIdList.Contains(vendorStorageSite.ID))
                {
                    deleteAddresses.Add(vendorStorageSite.Address); //OTSTM2-642 use existing list to collect sortyard addresses that to be deleted
                    vendor.VendorStorageSites.Remove(vendorStorageSite);
                }
            }

            foreach (var sortYard in applicationHaulerModel.SortYardDetails)
            {
                bool addNewSortYard = false;
                if (sortYard.SortYardAddress.ID != 0)
                {
                    var existingSortYardDetails = vendor.VendorStorageSites.Where(r => r.ID == sortYard.SortYardAddress.ID);

                    if (existingSortYardDetails.Any())
                    {
                        var selectedSortYard = existingSortYardDetails.FirstOrDefault();

                        selectedSortYard.Address.Address1 = sortYard.SortYardAddress.AddressLine1;
                        selectedSortYard.Address.Address2 = sortYard.SortYardAddress.AddressLine2;
                        selectedSortYard.Address.City = sortYard.SortYardAddress.City;
                        selectedSortYard.Address.PostalCode = sortYard.SortYardAddress.Postal;
                        selectedSortYard.Address.Province = sortYard.SortYardAddress.Province;
                        selectedSortYard.Address.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country;

                        double maxSortYardCapacity;
                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        selectedSortYard.MaxStorageCapacity = maxSortYardCapacity;
                        selectedSortYard.CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber;
                        selectedSortYard.StorageSiteType = 1;
                    }
                    else
                    {
                        addNewSortYard = true;
                    }
                }
                else
                {
                    addNewSortYard = true;
                }

                if (addNewSortYard)
                {
                    var vendorStorageSite = new VendorStorageSite()
                    {
                        Address = new Address()
                        {
                            Address1 = sortYard.SortYardAddress.AddressLine1,
                            Address2 = sortYard.SortYardAddress.AddressLine2,
                            City = sortYard.SortYardAddress.City,
                            PostalCode = sortYard.SortYardAddress.Postal,
                            Province = sortYard.SortYardAddress.Province,
                            Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country
                        },
                        MaxStorageCapacity = Convert.ToDouble(sortYard.MaxStorageCapacity),
                        CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                        StorageSiteType = 1,
                        UnitType = 3
                    };

                    vendor.VendorStorageSites.Add(vendorStorageSite);
                }
            }

            var addresses = new List<Address>();
            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Hauler)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            applicationHaulerModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == s.ID))
                    {
                        var result = defaultItems.Single(p => p.ID == s.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            });

            vendor.HHasRelatioshipWithProcessor = applicationHaulerModel.TireDetails.HHasRelatioshipWithProcessor;
            vendor.HRelatedProcessor = applicationHaulerModel.TireDetails.HRelatedProcessor;
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                vendorRepository.RemoveAddresses(deleteAddresses); //OTSTM2-642 Deleting address should be done before deleting vendorStorageSite in vendor because of foreign key
                vendorRepository.RemoveContacts(deleteContacts); //OTSTM2-50
                vendorRepository.UpdateVendorContacts(newContactAddresses, vendor.ID);
                vendorRepository.UpdateVendor(vendor);
                vendorRepository.UpdateVendorKeywords(vendor);
                transationScope.Complete();
            }

            return vendor.ID;
        }

        private void UpdateVendorSupportingDocuments(HaulerRegistrationModel applicationHaulerModel, Vendor vendor)
        {
            foreach (var supportDocOption in vendor.VendorSupportingDocuments)
            {
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.MBL)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionMBL;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CLI)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCLI;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CVOR)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCVOR;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.PRL)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionPRL;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.HST)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionHST;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.WSIB)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionWSIB;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CHQ)
                {
                    supportDocOption.Option = applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCHQ;
                }
            }
        }

        public HaulerRegistrationModel GetApprovedApplication(int vendorId)
        {
            HaulerRegistrationModel applicationHaulerModel = new HaulerRegistrationModel();

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                applicationHaulerModel.Status = vendor.RegistrantStatus == null ? ApplicationStatusEnum.Approved :
                    (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), vendor.RegistrantStatus);
                applicationHaulerModel.ActiveInactive.ApprovedDate = vendor.CreatedDate;

                if (vendor.ApplicationID.HasValue)
                {
                    var app = this.applicationRepository.FindApplicationByAppID(vendor.ApplicationID.Value);
                    applicationHaulerModel.ID = app.ID;
                    applicationHaulerModel.UserIPAddress = app.UserIP;
                    applicationHaulerModel.SubmittedDate = app.SubmittedDate;
                    //OTSTM2-973
                    applicationHaulerModel.TermsAndConditions.TermsAndConditionsID = app.TermsAndConditionsID;
                }

                applicationHaulerModel.BusinessLocation.BusinessName = vendor.BusinessName;
                applicationHaulerModel.BusinessLocation.OperatingName = vendor.OperatingName;
                applicationHaulerModel.HaulerDetails.BusinessStartDate = vendor.BusinessStartDate;
                applicationHaulerModel.HaulerDetails.BusinessNumber = vendor.BusinessNumber;
                applicationHaulerModel.HaulerDetails.CommercialLiabHstNumber = vendor.CommercialLiabHSTNumber;
                applicationHaulerModel.HaulerDetails.IsTaxExempt = vendor.IsTaxExempt;
                applicationHaulerModel.HaulerDetails.CommercialLiabInsurerName = vendor.CommercialLiabInsurerName;
                applicationHaulerModel.HaulerDetails.CommercialLiabInsurerExpDate = vendor.CommercialLiabInsurerExpDate;
                applicationHaulerModel.HaulerDetails.HIsGvwr = vendor.HIsGVWR;
                applicationHaulerModel.HaulerDetails.CvorNumber = vendor.CVORNumber;
                applicationHaulerModel.HaulerDetails.CvorExpiryDate = vendor.CVORExpiryDate;
                applicationHaulerModel.HaulerDetails.HasMoreThanOneEmp = vendor.HasMoreThanOneEmp;
                applicationHaulerModel.HaulerDetails.WsibNumber = vendor.WSIBNumber;
                applicationHaulerModel.RegistrationNumber = Convert.ToInt32(vendor.Number);


                applicationHaulerModel.TermsAndConditions.SigningAuthorityFirstName = vendor.SigningAuthorityFirstName;
                applicationHaulerModel.TermsAndConditions.SigningAuthorityLastName = vendor.SigningAuthorityLastName;
                applicationHaulerModel.TermsAndConditions.SigningAuthorityPosition = vendor.SigningAuthorityPosition;
                applicationHaulerModel.TermsAndConditions.FormCompletedByFirstName = vendor.FormCompletedByFirstName;
                applicationHaulerModel.TermsAndConditions.FormCompletedByLastName = vendor.FormCompletedByLastName;
                applicationHaulerModel.TermsAndConditions.FormCompletedByPhone = vendor.FormCompletedByPhone;
                applicationHaulerModel.TermsAndConditions.AgreementAcceptedByFullName = vendor.AgreementAcceptedByFullName;
                applicationHaulerModel.TermsAndConditions.AgreementAcceptedCheck = vendor.AgreementAcceptedCheck ?? true;
                applicationHaulerModel.CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue));
            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine1 = vendorPrimaryAddress.Address1;
                applicationHaulerModel.BusinessLocation.BusinessAddress.AddressLine2 = vendorPrimaryAddress.Address2;
                applicationHaulerModel.BusinessLocation.BusinessAddress.City = vendorPrimaryAddress.City;
                applicationHaulerModel.BusinessLocation.BusinessAddress.Province = vendorPrimaryAddress.Province;
                applicationHaulerModel.BusinessLocation.BusinessAddress.Postal = vendorPrimaryAddress.PostalCode;
                applicationHaulerModel.BusinessLocation.BusinessAddress.Country = vendorPrimaryAddress.Country;
                applicationHaulerModel.BusinessLocation.Phone = vendorPrimaryAddress.Phone;
                applicationHaulerModel.BusinessLocation.Email = vendorPrimaryAddress.Email;
                applicationHaulerModel.BusinessLocation.Extension = vendorPrimaryAddress.Ext;
                applicationHaulerModel.BusinessLocation.BusinessAddress.ID = vendorPrimaryAddress.ID;
            }

            var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
            if (vendorMailingAddress != null)
            {
                applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine1 = vendorMailingAddress.Address1;
                applicationHaulerModel.BusinessLocation.MailingAddress.AddressLine2 = vendorMailingAddress.Address2;
                applicationHaulerModel.BusinessLocation.MailingAddress.City = vendorMailingAddress.City;
                applicationHaulerModel.BusinessLocation.MailingAddress.Province = vendorMailingAddress.Province;
                applicationHaulerModel.BusinessLocation.MailingAddress.Postal = vendorMailingAddress.PostalCode;
                applicationHaulerModel.BusinessLocation.MailingAddress.Country = vendorMailingAddress.Country;
                applicationHaulerModel.BusinessLocation.MailingAddress.ID = vendorMailingAddress.ID;
                applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness = false;
            }
            else
            {
                applicationHaulerModel.BusinessLocation.MailingAddressSameAsBusiness = true;
            }

            foreach (var contactAddress in vendor.VendorAddresses)
            {
                foreach (var contact in contactAddress.Contacts)
                {
                    ContactRegistrationModel contactRegistrationModel = new ContactRegistrationModel()
                    {
                        ContactInformation = new ContactModel()
                        {
                            ID = contact.ID,
                            AddressID = contact.AddressID,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Position = contact.Position,
                            PhoneNumber = contact.PhoneNumber,
                            Ext = contact.Ext,
                            AlternatePhoneNumber = contact.AlternatePhoneNumber,
                            Email = contact.Email,
                            IsPrimary = (contact.IsPrimary.HasValue ? contact.IsPrimary.Value : false)
                        },

                        ContactAddress = new ContactAddressModel()
                        {
                            AddressLine1 = contact.Address.Address1,
                            AddressLine2 = contact.Address.Address2,
                            City = contact.Address.City,
                            Province = contact.Address.Province,
                            Postal = contact.Address.PostalCode,
                            Country = contact.Address.Country,
                        },
                        ContactAddressSameAsBusinessAddress = (applicationHaulerModel.BusinessLocation.BusinessAddress.ID == contact.AddressID)
                    };

                    applicationHaulerModel.ContactInformationList.Add(contactRegistrationModel);
                }
            }

            applicationHaulerModel.ContactInformationList = applicationHaulerModel.ContactInformationList.OrderBy(c => c.ContactInformation.ID).ToList();

            foreach (var sortYard in vendor.VendorStorageSites)
            {
                SortYardDetailsRegistrationModel sortYardDetailsRegistrationModel = new SortYardDetailsRegistrationModel()
                {
                    SortYardAddress = new SortYardAddressModel()
                    {
                        ID = sortYard.ID,
                        AddressLine1 = sortYard.Address.Address1,
                        AddressLine2 = sortYard.Address.Address2,
                        City = sortYard.Address.City,
                        Province = sortYard.Address.Province,
                        Postal = sortYard.Address.PostalCode,
                        Country = sortYard.Address.Country
                    },

                    CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                    MaxStorageCapacity = sortYard.MaxStorageCapacity.ToString(),
                    StorageSiteType = sortYard.StorageSiteType,
                };

                applicationHaulerModel.SortYardDetails.Add(sortYardDetailsRegistrationModel);
            }

            applicationHaulerModel.TireDetails = new TireDetailsRegistrationModel();
            applicationHaulerModel.TireDetails.TireDetailsItemType = new List<TireDetailsItemTypeModel>();

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Hauler)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            defaultItems.ToList().ForEach(s =>
            {
                bool isChecked = false;

                if (vendor.Items.Any(r => r.ID == s.ID))
                {
                    isChecked = true;
                }

                TireDetailsItemTypeModel tireDetailsItemTypeModel = new TireDetailsItemTypeModel()
                {
                    ID = s.ID,
                    Description = s.Description,
                    Name = s.Name,
                    ShortName = s.ShortName,
                    isChecked = isChecked,
                    IsActive = s.IsActive,
                    ItemTypeID = s.ItemType,
                };

                applicationHaulerModel.TireDetails.TireDetailsItemType.Add(tireDetailsItemTypeModel);
            });


            applicationHaulerModel.TireDetails.HHasRelatioshipWithProcessor = vendor.HHasRelatioshipWithProcessor;
            applicationHaulerModel.TireDetails.HRelatedProcessor = vendor.HRelatedProcessor;

            applicationHaulerModel.SupportingDocuments = new SupportingDocumentsRegistrationModel();
            foreach (var supportingDocument in vendor.VendorSupportingDocuments)
            {
                SupportingDocumentTypeEnum supportingDocumentType;
                if (Enum.TryParse(supportingDocument.TypeID.ToString(), out supportingDocumentType))
                {
                    switch (supportingDocumentType)
                    {
                        case SupportingDocumentTypeEnum.MBL:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionMBL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CLI:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCLI = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CVOR:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCVOR = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.PRL:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionPRL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.HST:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionHST = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.WSIB:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionWSIB = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CHQ:
                            applicationHaulerModel.SupportingDocuments.SupportingDocumentsOptionCHQ = supportingDocument.Option;
                            break;
                        default:
                            break;
                    }
                }
            }

            var bankingInformation = vendor.BankInformations.FirstOrDefault();

            if (bankingInformation != null)
            {
                applicationHaulerModel.BankingInformation.BankName = bankingInformation.BankName;
                applicationHaulerModel.BankingInformation.TransitNumber = bankingInformation.TransitNumber;
                applicationHaulerModel.BankingInformation.BankNumber = bankingInformation.BankNumber;
                applicationHaulerModel.BankingInformation.AccountNumber = bankingInformation.AccountNumber;
                applicationHaulerModel.BankingInformation.EmailPaymentTransferNotification = bankingInformation.EmailPaymentTransferNotification;
                applicationHaulerModel.BankingInformation.NotifyToPrimaryContactEmail = bankingInformation.NotifyToPrimaryContactEmail;
                applicationHaulerModel.BankingInformation.ReviewStatus = bankingInformation.ReviewStatus;
            }

            return applicationHaulerModel;
        }

        #endregion Workflow
    }
}