﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.RegistrantBLL;
using CL.TMS.ServiceContracts.RegistrantServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.IRepository.Registrant;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.TSFSteward;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;

namespace CL.TMS.RegistrantServices
{
    public class RegistrantService : IRegistrantService
    {
        private readonly RegistrantBO registrantBO;
        public RegistrantService(IVendorRepository vendorRepository, IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationRepository applicationRepository, ITSFRemittanceRepository tsfRemittanceRepository,
            IApplicationInvitationRepository appInvitationRepository, IGpRepository gpRepository, ISettingRepository settingRepository, IClaimsRepository claimsRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository)
        {
            registrantBO = new RegistrantBO(vendorRepository, assetRepository, appUserRepository, applicationRepository, tsfRemittanceRepository, appInvitationRepository, gpRepository, settingRepository, claimsRepository, eventAggregator, messageRepository);
        }

        //Vendor Methods
        public IReadOnlyDictionary<int, string> GetVendorNameList()
        {
            try
            {
                return registrantBO.GetVendorNameList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        //-----------Added by Frank, begin: new function-------------
        public IReadOnlyDictionary<int, string> GetListOfRegistrantsWithPageIndex(int pageIndex, int pageSize)
        {
            try
            {
                return registrantBO.GetListOfRegistrantsWithPageIndex(pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement
        public string GetMatchedRegistrant(string regNumberEntered)
        {
            try
            {
                return registrantBO.GetMatchedRegistrant(regNumberEntered);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public Vendor GetSingleWithApplicationID(int appId)
        {
            try
            {
                return registrantBO.GetSingleWithApplicationID(appId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public Vendor GetSingleVendorByID(int vendorId)
        {
            try
            {
                return registrantBO.GetSingleVendorByID(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateVendorGeneratorStatus(int vendorId, bool Status, DateTime date)
        {
            try
            {
                registrantBO.UpdateVendorGeneratorStatus(vendorId, Status, date);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        //OTSTM2-953
        public void UpdateVendorSubCollectorStatus(int vendorId, bool Status, DateTime date)
        {
            try
            {
                registrantBO.UpdateVendorSubCollectorStatus(vendorId, Status, date);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddVendor(Vendor vendor)
        {
            try
            {
                registrantBO.AddVendor(vendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateVendor(Vendor vendor)
        {
            try
            {
                registrantBO.UpdateVendor(vendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ImportVendorDetails(IEnumerable<Vendor> vendorList)
        {
            try
            {
                registrantBO.ImportVendorDetails(vendorList);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddVendors(IReadOnlyList<Vendor> vendors)
        {
            try
            {
                AddVendors(vendors);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public string GetVendorNumber(int vendorId)
        {
            try
            {
                return registrantBO.GetVendorNumber(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<Address> GetVendorAddressList(int vendorId)
        {
            try
            {
                return registrantBO.GetVendorAddressList(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Address GetVendorAddressByType(int vendorId, int typeId)
        {
            try
            {
                return registrantBO.GetVendorAddressByType(vendorId, typeId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IList<Vendor> GetUpdatedVendorList(DateTime lastUpdated)
        {
            try
            {
                return registrantBO.GetUpdatedVendorList(lastUpdated);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IList<Vendor> GetVendorsByApplicationId(int applicationId)
        {
            try
            {
                return registrantBO.GetVendorsByApplicationId(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public Vendor FindVendorByID(int vendorId)
        {
            try
            {
                return registrantBO.FindVendorByID(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public long GetLastAddedVendorNumber()
        {
            try
            {
                return registrantBO.GetLastAddedVendorNumber();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            //test it
            return -1;
        }

        public void CreateBankInformation(Vendor vendor, BankingInformationRegistrationModel bankInformationModel)
        {
            try
            {
                registrantBO.CreateBankInformation(vendor, bankInformationModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Vendor UpdateBankInformation(BankInformation bankInformation)
        {
            Vendor vdr = new Vendor();
            try
            {
                vdr = registrantBO.UpdateBankInformation(bankInformation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return vdr;
        }
        public void DeleteBankInformation(int bankId, int vendorId)
        {
            try
            {
                registrantBO.DeleteBankInformation(bankId, vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public BankInformation GetBankInformation(int vendorId)
        {
            try
            {
                return registrantBO.GetBankInformation(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public BankInformation GetBankInformationById(int bankId)
        {
            try
            {
                return registrantBO.GetBankInformationById(bankId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        // Asset Methods
        public IReadOnlyList<IPadModel> IPadList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection)
        {
            try
            {
                var iPadList = registrantBO.IPadList(skip, take, searchValue, orderColumnIndex, orderDirection);

                List<IPadModel> list = new List<IPadModel>();
                foreach (var iPad in iPadList)
                {
                    var ipad = new IPadModel()
                    {
                        ID = iPad.ID,
                        Number = iPad.AssetTag,
                        AssignedToName = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.BusinessName),
                        AssignedToNumber = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.Number),
                        Activated = iPad.Active,
                        IsAssigned = (!iPad.VendorID.HasValue ? false : true)
                    };

                    list.Add(ipad);
                }

                return list;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<IPadModel> IPadList(string searchValue)
        {
            try
            {
                var iPadList = registrantBO.IPadList(searchValue);

                List<IPadModel> list = new List<IPadModel>();
                foreach (var iPad in iPadList)
                {
                    var ipad = new IPadModel()
                    {
                        ID = iPad.ID,
                        Number = iPad.AssetTag,
                        AssignedToName = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.BusinessName),
                        AssignedToNumber = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.Number),
                        Activated = iPad.Active,
                        IsAssigned = (!iPad.VendorID.HasValue ? false : true),
                    };

                    list.Add(ipad);
                }

                return list;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<IPadModel> IPadList()
        {
            try
            {
                var iPadList = registrantBO.IPadList();
                List<IPadModel> list = new List<IPadModel>();

                foreach (var iPad in iPadList)
                {
                    var ipad = new IPadModel()
                    {
                        ID = iPad.ID,
                        Number = iPad.AssetTag,
                        AssignedToName = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.BusinessName),
                        AssignedToNumber = (!iPad.VendorID.HasValue ? string.Empty : iPad.Vendor.Number),
                        Activated = iPad.Active,
                        IsAssigned = (!iPad.VendorID.HasValue ? false : true),
                    };

                    list.Add(ipad);
                }

                return list;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddIPad(int amountToAdd, int? vendorId, int assetTypeID, string createdBy)
        {
            try
            {
                registrantBO.AddIPad(amountToAdd, vendorId, assetTypeID, createdBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AssignIPad(int iPadId, int vendorId, string unAssignBy)
        {
            try
            {
                registrantBO.AssignIPad(iPadId, vendorId, unAssignBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UnAssignIPad(int iPadId, string unAssignBy)
        {
            try
            {
                registrantBO.UnAssignIPad(iPadId, unAssignBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ActiveIPad(int iPadId, string activatedBy)
        {
            try
            {
                registrantBO.ActiveIPad(iPadId, activatedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void DeActiveIPad(int iPadId, string deActivatedBy)
        {
            try
            {
                registrantBO.DeActiveIPad(iPadId, deActivatedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public string CheckForUnassignedIPadSearchValue(string searchValue)
        {
            try
            {
                return registrantBO.CheckForUnassignedIPadSearchValue(searchValue);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //AppUser Methods
        public IReadOnlyList<AppUserModel> GetAppUserList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection)
        {
            try
            {
                var appUserList = registrantBO.GetAppUserList(skip, take, searchValue, orderColumnIndex, orderDirection);

                return appUserList;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<AppUserModel> GetAppUserList(string searchValue)
        {
            try
            {
                return registrantBO.GetAppUserList(searchValue);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void ActivateAppUser(int qrCodeId)
        {
            try
            {
                registrantBO.ActivateAppUser(qrCodeId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void DeActivateAppUser(int qrCodeId)
        {
            try
            {
                registrantBO.DeActivateAppUser(qrCodeId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddAppUser(int amountToAdd, int vendorId, string createdBy)
        {
            try
            {
                registrantBO.AddAppUser(amountToAdd, vendorId, createdBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public IList<AppUser> GetUpdatedAppUserList(DateTime lastUpdated)
        {
            try
            {
                return registrantBO.GetUpdatedAppUserList(lastUpdated);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public VendorReference GetVendorByNumber(string number)
        {
            try
            {
                return registrantBO.GetVendorByNumber(number);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public VendorReference GetVendorByNumber(string number, long userId, string userName)
        {
            try
            {
                return registrantBO.GetVendorByNumber(number, userId, userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }
        public VendorReference GetVendorById(int vendorId)
        {
            try
            {
                return registrantBO.GetVendorById(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            try
            {
                return registrantBO.GetApprovedApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ApplicationEmailModel GetApprovedRegistrantInformation(int vendorId)
        {
            try
            {
                return this.registrantBO.GetApprovedRegistrantInformation(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ApplicationEmailModel GetApprovedCustomerInformation(int customerId)
        {
            try
            {
                return this.registrantBO.GetApprovedCustomerInformation(customerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlights(string keyWords, string section, int? pageLimit)
        {
            return registrantBO.GetRegistrantSpotlights(keyWords, section, pageLimit);
        }

        public IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlightsPaginate(string keyWords, string section, int start, int pageLimit)
        {
            return registrantBO.GetRegistrantSpotlightsPaginate(keyWords, section, start, pageLimit);
        }
        public PaginationDTO<RegistrantSpotlightModel, int> LoadSpotlightViewMoreResult(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string keyWords, int pageLimit, string section)
        {
            try
            {
                return registrantBO.LoadSpotlightViewMoreResult(pageIndex, pageSize, searchText, orderBy, sortDirection, keyWords, pageLimit, section);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-960/963/964/965/966
        public List<RegistrantSpotlightModel> LoadSpotlightResultExport(string searchText, string orderBy, string sortDirection, string keyWords, string section)
        {
            try
            {
                return registrantBO.LoadSpotlightResultExport(searchText, orderBy, sortDirection, keyWords, section);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateRegistrantKeywords(int? registrantType = null)
        {
            registrantBO.UpdateKeyWords(registrantType);
        }

        #region GP Integration

        public GpResponseMsg CreateBatch()
        {
            try
            {
                return registrantBO.CreateBatch();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<GpBatchDto> GetGpBatches(string type)
        {
            try
            {
                return registrantBO.GetGpBatchDtos(type);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }
        public PaginationDTO<GPListViewModel, int> GetAllGpBatchesPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string type)
        {
            try
            {
                return registrantBO.GetAllGpBatchesPagination(pageIndex, pageSize, searchText, orderBy, sortDirection, type);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public PaginationDTO<GPModalListViewModal, int> GetGpTransactionsByBatchIdPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int batchId)
        {
            try
            {
                return registrantBO.GetGpTransactionsByBatchIdPagination(pageIndex, pageSize, searchText, orderBy, sortDirection, batchId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public Object[] GetExport(int gpiBatchId)
        {
            try
            {
                return registrantBO.GetExport(gpiBatchId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ResponseMsgBase PutOnHold(int gpiBatchEntryId)
        {
            try
            {
                return registrantBO.PutOnHold(gpiBatchEntryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public ResponseMsgBase PutOffHold(int gpiBatchEntryId)
        {
            try
            {
                return registrantBO.PutOffHold(gpiBatchEntryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<GpBatchEntryDto> GetGpTransactionsByBatchId(int gpBatchId)
        {
            try
            {
                return registrantBO.GetGpBatchEntries(gpBatchId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ResponseMsgBase PostBatch(int gpiBatchId, string type)
        {
            try
            {
                return registrantBO.PostBatch(gpiBatchId, type);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ResponseMsgBase RemoveFromBatch(int gpiBatchEntryId)
        {
            try
            {
                return registrantBO.RemoveFromBatch(gpiBatchEntryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void GpStatusCheck()
        {
            registrantBO.GpStatusCheck();
        }

        #endregion




        #region Application Notes
        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return registrantBO.LoadApplicationNotes(applicationId, pageIndex, pageSize, searchText, orderBy,
                    sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForVendor(int vendorId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return registrantBO.LoadApplicationNotesForVendor(vendorId, pageIndex, pageSize, searchText, orderBy,
                    sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForCustomer(int customerId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return registrantBO.LoadApplicationNotesForCustomer(customerId, pageIndex, pageSize, searchText, orderBy,
                    sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddApplicationNote(ApplicationNote applicationNote)
        {
            try
            {
                registrantBO.AddApplicationNode(applicationNote);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<ApplicationNoteViewMode> LoadApplicationNotesForExport(int applicationId, string searchText, string sortcolumn, string sortdirection)
        {
            try
            {
                return registrantBO.LoadApplicationNotesForExport(applicationId, searchText, sortcolumn, sortdirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ApplicationNoteViewMode> LoadVendorNotesForExport(int vendorId, string searchText, string sortcolumn, string sortdirection)
        {
            try
            {
                return registrantBO.LoadVendorNotesForExport(vendorId, searchText, sortcolumn, sortdirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ApplicationNoteViewMode> LoadCustomerNotesForExport(int customerId, string searchText, string sortcolumn, string sortdirection)
        {
            try
            {
                return registrantBO.LoadCustomerNotesForExport(customerId, searchText, sortcolumn, sortdirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region TSFRemittance Notes
        public PaginationDTO<TSFRemittanceNoteViewMode, int> LoadTSFRemittanceNotes(int claimId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return registrantBO.LoadTSFRemittanceNotes(claimId, pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddTSFRemittanceNote(TSFRemittanceNote claimNote)
        {
            try
            {
                registrantBO.AddTSFRemittanceNote(claimNote);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<TSFRemittanceNoteViewMode> LoadTSFRemittanceNotesForExport(int claimId, string searchText, string sortcolumn, string sortdirection)
        {
            try
            {
                return registrantBO.LoadTSFRemittanceNotesForExport(claimId, searchText, sortcolumn, sortdirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region NewActiveInactive
        public void ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory)
        {
            try
            {
                registrantBO.ActiveInactiveVendor(vendorActiveHistory);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public VendorActiveHistory LoadCurrenActiveHistoryForVendor(int vendorId)
        {
            try
            {
                return registrantBO.LoadCurrenActiveHistoryForVendor(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public VendorActiveHistory LoadPreviousActiveHistoryForVendor(int vendorId, bool isActive)
        {
            try
            {
                return registrantBO.LoadPreviousActiveHistoryForVendor(vendorId, isActive);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public void ActiveInactiveCustomer(CustomerActiveHistory customerrActiveHistory)
        {
            try
            {
                registrantBO.ActiveInactiveCustomer(customerrActiveHistory);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public CustomerActiveHistory LoadCurrenActiveHistoryForCustomer(int customerId)
        {
            try
            {
                return registrantBO.LoadCurrenActiveHistoryForCustomer(customerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public CustomerActiveHistory LoadPreviousActiveHistoryForCustomer(int customerId, bool isActive)
        {
            try
            {
                return registrantBO.LoadPreviousActiveHistoryForCustomer(customerId, isActive);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #endregion

        //OTSTM2-449 backup (manually)
        public void AutoSwitchForAllCustomers(int changedBy)
        {
            registrantBO.AutoSwitchForAllCustomers(changedBy);
        }

        //OTSMTM2-745
        public void AddGPActivityForCreateBatch(int batchId)
        {
            try
            {
                registrantBO.AddGPActivityForCreateBatch(batchId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #region //Admin Financial Integration
        public IEnumerable<FinancialIntegrationVM> GetAdminFinancialIntegration()
        {
            try
            {
                return registrantBO.GetAdminFinancialIntegration();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public bool UpdateFIData(List<FinancialIntegrationVM> vm)
        {
            try
            {
                return registrantBO.FIUpdateData(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        #endregion

    }
}
