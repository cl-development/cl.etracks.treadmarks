﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using System.Web.Script.Serialization;

namespace CL.TMS.StewardBLL
{
    public class StewardRegistrationInvitationBO
    {
        private IApplicationInvitationRepository applicationInvitationRepository;

        public StewardRegistrationInvitationBO(IApplicationInvitationRepository applicationInvitationRepository)
        {
            this.applicationInvitationRepository = applicationInvitationRepository;
        }

        public void UpdateFormObject(int id, string formObject)
        {
           // applicationInvitationRepository.Update()

        }

        public ApplicationInvitation GetByTokenID(Guid guid)
        {
            return applicationInvitationRepository.GetById(guid);
        }

        public string GetEmailByApplicationId(int applicationId)
        {
            return this.applicationInvitationRepository.GetEmailByApplicationId(applicationId);
        }
    }
}
