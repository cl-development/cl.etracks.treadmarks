﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.DataContracts.Adapter;

namespace CL.TMS.RPMBLL
{
    public class RPMRegistrationBO
    {
        private IApplicationRepository applicationRepository;
        private IItemRepository itemRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private IClaimsRepository claimsRepository;

        public RPMRegistrationBO(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository, IClaimsRepository claimsRepository)
        {
            this.applicationRepository = applicationRepository;
            this.itemRepository = itemRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.claimsRepository = claimsRepository;
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            applicationRepository.UpdateFormObject(id, formObject, updatedBy);
        }

        public void UpdateApplication(Application application)
        {
            applicationRepository.UpdateApplication(application);
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>RPMRegistrationModel based on FormObject column in Application table</returns>
        public RPMRegistrationModel GetFormObject(int id)
        {
            var application = applicationRepository.GetSingleApplication(id);
            if (application == null)
            {
                throw new KeyNotFoundException();
            }

            RPMRegistrationModel result = null;
            var applicationStatus = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            if (applicationStatus == ApplicationStatusEnum.Approved
                || applicationStatus == ApplicationStatusEnum.BankInformationSubmitted
                || applicationStatus == ApplicationStatusEnum.BankInformationApproved
                || applicationStatus == ApplicationStatusEnum.Completed)
            {
                var vendor = vendorRepository.GetSingleWithApplicationID(id);
                result = GetApprovedApplication(vendor.ID);
                result.ID = id;
                result.VendorID = vendor.ID;
            }
            else
            {
                string formObject = application.FormObject;
                if (formObject != null)
                {
                    result = JsonConvert.DeserializeObject<RPMRegistrationModel>(formObject);
                }
                else
                {
                    result = new RPMRegistrationModel(id);
                }
            }

            if (application.AssignToUser.HasValue)
            {
                result.AssignToUserId = application.AssignToUser.Value;
            }

            result.Status = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            result.SubmittedDate = application.SubmittedDate;
            result.ExpireDate = application.ExpireDate;

            //OTSTM2-973 TermsAndConditons
            result.TermsAndConditions.TermsAndConditionsID = application.TermsAndConditionsID;

            return result;
        }

        public RPMRegistrationModel GetAllItemsList(int ParticipantType)
        {
            var result = new RPMRegistrationModel();
            var allTireItems = itemRepository.GetAllItems(ParticipantType);
            var ListItemModel = new List<RPMTireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new RPMTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new RPMTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public RPMRegistrationModel GetAllItemsList()
        {
            var result = new RPMRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            var ListItemModel = new List<RPMTireDetailsItemTypeModel>();

            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new RPMTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new RPMTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public RPMRegistrationModel GetAllProduct()
        {
            var result = new RPMRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.RPMType).ToList();
            var ListItemModel = new List<RPMTireDetailsItemTypeModel>();

            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new RPMTireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new RPMTireDetailsRegistrationModel()
            {
                ProductsProduceType = ListItemModel
            };
            return result;
        }

        public Application GetApplicationById(int appId)
        {
            var result = applicationRepository.GetSingleApplication(appId);
            return result;
        }

        public void RemoveEmptyContacts(ref IList<ContactRegistrationModel> contacts, ref IList<string> invalidFieldList)
        {
            if (contacts != null && contacts.Count() > 1)
            {
                List<ContactRegistrationModel> results = new List<ContactRegistrationModel>() { contacts[0] };
                IList<string> resultsFieldList = invalidFieldList.ToList();

                int totalContacts = contacts.Count();
                for (int i = 1; i < totalContacts; i++)
                {
                    bool remove = false;
                    if (contacts[i].ContactInformation != null)
                    {
                        var contact = contacts[i].ContactInformation;
                        remove = string.IsNullOrWhiteSpace(contact.FirstName) &&
                                  string.IsNullOrWhiteSpace(contact.LastName) &&
                                  string.IsNullOrWhiteSpace(contact.Position) &&
                                  string.IsNullOrWhiteSpace(contact.PhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Ext) &&
                                  string.IsNullOrWhiteSpace(contact.AlternatePhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Email);
                        if (remove)
                        {
                            //check if contact address has any information
                            if (!contacts[i].ContactAddressSameAsBusinessAddress)
                            {
                                if (contacts[i].ContactAddress != null)
                                {
                                    var address = contacts[i].ContactAddress;
                                    remove = !contact.ContactAddressSameAsBusiness &&
                                            string.IsNullOrWhiteSpace(address.AddressLine1) &&
                                            string.IsNullOrWhiteSpace(address.AddressLine2) &&
                                            string.IsNullOrWhiteSpace(address.City) &&
                                            string.IsNullOrWhiteSpace(address.Postal) &&
                                            string.IsNullOrWhiteSpace(address.Province) &&
                                            string.IsNullOrWhiteSpace(address.Country);
                                }
                            }
                        }
                    }
                    if (remove)
                    {
                        //remove items from invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                            }
                        }
                    }
                    else
                    {
                        results.Add(contacts[i]);
                        int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                        if (numRemoved > 0)
                        {
                            //replace all the names in invalidlist
                            foreach (string nameStr in invalidFieldList)
                            {
                                string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                                Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    resultsFieldList.Remove(nameStr);
                                    string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                    resultsFieldList.Add(newName);
                                }
                            }
                        }
                    }
                }
                contacts = results;
                invalidFieldList = resultsFieldList;
            }
        }

        public void RemoveEmptySortYardDetails(ref IList<RPMSortYardDetailsRegistrationModel> sortyardDetails, ref IList<string> invalidFieldList)
        {
            if (sortyardDetails.Count() == 0)
            {
                RPMSortYardDetailsRegistrationModel newItem = new RPMSortYardDetailsRegistrationModel();
                sortyardDetails.Add(newItem);
            }

            List<RPMSortYardDetailsRegistrationModel> results = new List<RPMSortYardDetailsRegistrationModel>() { sortyardDetails[0] };
            IList<string> resultsFieldList = invalidFieldList.ToList();

            int totalSortYardDetails = sortyardDetails.Count();
            for (int i = 1; i < totalSortYardDetails; i++)
            {
                bool remove = false;
                if (sortyardDetails[i].SortYardAddress != null)
                {
                    var address = sortyardDetails[i].SortYardAddress;
                    remove = string.IsNullOrEmpty(address.AddressLine1) &&
                             string.IsNullOrEmpty(address.AddressLine2) &&
                             string.IsNullOrEmpty(address.City) &&
                             string.IsNullOrEmpty(address.Postal) &&
                             string.IsNullOrEmpty(address.Province) &&
                             string.IsNullOrEmpty(address.Country);
                    if (remove)
                    {
                        remove = string.IsNullOrEmpty(sortyardDetails[i].MaxStorageCapacity) || string.Compare(sortyardDetails[i].MaxStorageCapacity, "0", true) == 0;
                    }
                }
                if (remove)
                {
                    //remove items from invalidlist
                    foreach (string nameStr in invalidFieldList)
                    {
                        string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                        Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            resultsFieldList.Remove(nameStr);
                        }
                    }
                }
                else
                {
                    results.Add(sortyardDetails[i]);
                    int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                    if (numRemoved > 0)
                    {
                        //replace all the names in invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(SortYardDetails\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                                string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                resultsFieldList.Add(newName);
                            }
                        }
                    }
                }
            }
            sortyardDetails = results;
            invalidFieldList = resultsFieldList;
        }

        /// <summary>
        /// will return result from json string formObject in Application tabel
        /// if the id not found returns error
        /// if formObject is null returns a new Model with id
        /// </summary>
        /// <param name="id">id from Application table which has the formObjecxt</param>
        /// <returns>RPMRegistrationModel based on FormObject column in Application table</returns>

        public RPMRegistrationModel GetByTokenId(Guid tokenId)
        {
            var application = applicationRepository.GetApplicationByTokenId(tokenId);
            return GetFormObject(application.ID);
        }

        public ApplicationEmailModel GetApprovedRPMApplicantInformation(int applicationId)
        {
            var application = this.applicationRepository.FindApplicationByAppID(applicationId);
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }
            var applicationRPMApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                PrimaryContactLastName = vendorPrimaryContact.LastName,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = applicationInvitationRepository.GetTokenByApplicationId(applicationId)
            };

            return applicationRPMApplicationModel;
        }

        #region Workflow

        /// <summary>
        /// updating the entities after approve selection
        /// </summary>
        /// <param name="applicationRPMModel"></param>
        /// <param name="appID"></param>
        public int ApproveApplication(RPMRegistrationModel applicationRPMModel, int appID)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;
            var app = this.applicationRepository.FindApplicationByAppID(appID);
            app.SigningAuthorityFirstName = applicationRPMModel.TermsAndConditions.SigningAuthorityFirstName;
            app.SigningAuthorityLastName = applicationRPMModel.TermsAndConditions.SigningAuthorityLastName;
            app.SigningAuthorityPosition = applicationRPMModel.TermsAndConditions.SigningAuthorityPosition;
            app.FormCompletedByFirstName = applicationRPMModel.TermsAndConditions.FormCompletedByFirstName;
            app.FormCompletedByLastName = applicationRPMModel.TermsAndConditions.FormCompletedByLastName;
            app.FormCompletedByPhone = applicationRPMModel.TermsAndConditions.FormCompletedByPhone;
            app.AgreementAcceptedByFullName = applicationRPMModel.TermsAndConditions.AgreementAcceptedByFullName;
            app.Status = ApplicationStatusEnum.Approved.ToString();
            app.FormObject = app.FormObject;

            AddApplicationSupportingDocuments(applicationRPMModel, app);

            var vendor = new Vendor
            {
                BusinessName = applicationRPMModel.BusinessLocation.BusinessName,
                OperatingName = applicationRPMModel.BusinessLocation.OperatingName,
                BusinessStartDate = applicationRPMModel.RPMDetails.BusinessStartDate,
                BusinessNumber = applicationRPMModel.RPMDetails.BusinessNumber,
                CommercialLiabHSTNumber = applicationRPMModel.RPMDetails.CommercialLiabHstNumber,
                IsTaxExempt = applicationRPMModel.RPMDetails.IsTaxExempt,
                CommercialLiabInsurerName = applicationRPMModel.RPMDetails.CommercialLiabInsurerName,
                CommercialLiabInsurerExpDate = applicationRPMModel.RPMDetails.CommercialLiabInsurerExpDate,
                HIsGVWR = applicationRPMModel.RPMDetails.HIsGvwr,
                CVORExpiryDate = applicationRPMModel.RPMDetails.CvorExpiryDate,
                HasMoreThanOneEmp = applicationRPMModel.RPMDetails.HasMoreThanOneEmp,
                WSIBNumber = applicationRPMModel.RPMDetails.WsibNumber,
                CompanyYearEndDate = applicationRPMModel.RPMDetails.BusinessFicalYearEndDate,
                ApplicationID = app.ID,
                CreatedDate = DateTime.UtcNow,
                VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue,
                LastUpdatedBy = updatedBy,
                LastUpdatedDate = DateTime.UtcNow,
                ActiveStateChangeDate = DateTime.Now.Date,
                ActivationDate = DateTime.Now.Date,
                IsActive = true,
                ContactInfo = string.Format("{0} {1}, {2}", applicationRPMModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationRPMModel.ContactInformationList[0].ContactInformation.LastName, applicationRPMModel.ContactInformationList[0].ContactInformation.PhoneNumber),
                OtherRegistrantSubType = applicationRPMModel.TireDetails.OtherRegistrantSubType,
                OtherProcessProduct = applicationRPMModel.TireDetails.OtherProcessProduct,
                RegistrantStatus = ApplicationStatusEnum.Approved.ToString(),
                SigningAuthorityFirstName = applicationRPMModel.TermsAndConditions.SigningAuthorityFirstName,
                SigningAuthorityLastName = applicationRPMModel.TermsAndConditions.SigningAuthorityLastName,
                SigningAuthorityPosition = applicationRPMModel.TermsAndConditions.SigningAuthorityPosition,
                FormCompletedByFirstName = applicationRPMModel.TermsAndConditions.FormCompletedByFirstName,
                FormCompletedByLastName = applicationRPMModel.TermsAndConditions.FormCompletedByLastName,
                FormCompletedByPhone = applicationRPMModel.TermsAndConditions.FormCompletedByPhone,
                AgreementAcceptedByFullName = applicationRPMModel.TermsAndConditions.AgreementAcceptedByFullName,
                AgreementAcceptedCheck = applicationRPMModel.TermsAndConditions.AgreementAcceptedCheck,

            };

            MoveAttachmentToVendor(applicationRPMModel, vendor);

            var vendorPrimaryAddress = new Address()
            {
                Address1 = applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine1,
                Address2 = applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine2,
                City = applicationRPMModel.BusinessLocation.BusinessAddress.City,
                Province = applicationRPMModel.BusinessLocation.BusinessAddress.Province,
                PostalCode = applicationRPMModel.BusinessLocation.BusinessAddress.Postal,
                Country = applicationRPMModel.BusinessLocation.BusinessAddress.Country,
                Phone = applicationRPMModel.BusinessLocation.Phone,
                Email = applicationRPMModel.BusinessLocation.Email,
                Ext = applicationRPMModel.BusinessLocation.Extension,
                AddressType = (int)AddressTypeEnum.Business
            };

            vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (applicationRPMModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = new Address
                {
                    Address1 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1,
                    Address2 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine2,
                    City = applicationRPMModel.BusinessLocation.MailingAddress.City,
                    Province = applicationRPMModel.BusinessLocation.MailingAddress.Province,
                    PostalCode = applicationRPMModel.BusinessLocation.MailingAddress.Postal,
                    Country = applicationRPMModel.BusinessLocation.MailingAddress.Country,
                    Phone = applicationRPMModel.BusinessLocation.Phone,
                    Email = applicationRPMModel.BusinessLocation.Email,
                    AddressType = (int)AddressTypeEnum.Mail
                };

                vendor.VendorAddresses.Add(vendorMailingAddress);
            }

            var firstVendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
            if (firstVendorAddress != null)
            {
                for (var i = 0; i < applicationRPMModel.ContactInformationList.Count; i++)
                {
                    if (applicationRPMModel.ContactInformationList[i].ContactInformation.FirstName != null &&
                        applicationRPMModel.ContactInformationList[i].ContactInformation.LastName != null)
                    {
                        var contact = new Contact()
                        {
                            FirstName = applicationRPMModel.ContactInformationList[i].ContactInformation.FirstName,
                            LastName = applicationRPMModel.ContactInformationList[i].ContactInformation.LastName,
                            Position = applicationRPMModel.ContactInformationList[i].ContactInformation.Position,
                            PhoneNumber =
                                applicationRPMModel.ContactInformationList[i].ContactInformation.PhoneNumber,
                            Ext = applicationRPMModel.ContactInformationList[i].ContactInformation.Ext,
                            AlternatePhoneNumber =
                                applicationRPMModel.ContactInformationList[i].ContactInformation.AlternatePhoneNumber,
                            Email = applicationRPMModel.ContactInformationList[i].ContactInformation.Email,
                            IsPrimary = i == 0,
                        };

                        if (!applicationRPMModel.ContactInformationList[i].ContactAddressSameAsBusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(applicationRPMModel.ContactInformationList[i].ContactAddress.AddressLine1))
                            {
                                var contactAddress = new Address()
                                {
                                    Address1 =
                                        applicationRPMModel.ContactInformationList[i].ContactAddress.AddressLine1,
                                    Address2 =
                                        applicationRPMModel.ContactInformationList[i].ContactAddress.AddressLine2,
                                    City = applicationRPMModel.ContactInformationList[i].ContactAddress.City,
                                    Province = applicationRPMModel.ContactInformationList[i].ContactAddress.Province,
                                    PostalCode = applicationRPMModel.ContactInformationList[i].ContactAddress.Postal,
                                    Country = string.IsNullOrWhiteSpace(applicationRPMModel.ContactInformationList[i].ContactAddress.Country) ? "Canada" : applicationRPMModel.ContactInformationList[i].ContactAddress.Country,
                                    AddressType = (int)AddressTypeEnum.Contact
                                };
                                contactAddress.Contacts.Add(contact);
                                vendor.VendorAddresses.Add(contactAddress);
                            }
                            else
                            {
                                firstVendorAddress.Contacts.Add(contact);
                            }
                        }
                        else
                        {
                            firstVendorAddress.Contacts.Add(contact);
                        }
                    }
                }
            }
            var addresses = new List<Address>();
            foreach (var sortYard in applicationRPMModel.SortYardDetails)
            {
                sortYard.StorageSiteType = 1;
                if (!string.IsNullOrEmpty(sortYard.SortYardAddress.AddressLine1))
                {
                    var found = false;
                    foreach (var eachAddress in vendor.VendorAddresses)
                    {
                        if ((string.Compare(sortYard.SortYardAddress.AddressLine1, eachAddress.Address1, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.AddressLine2, eachAddress.Address2, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.City, eachAddress.City, StringComparison.OrdinalIgnoreCase) == 0) &&
                            (string.Compare(sortYard.SortYardAddress.Province, eachAddress.Province, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Postal, eachAddress.PostalCode, StringComparison.OrdinalIgnoreCase) == 0) &&
                             (string.Compare(sortYard.SortYardAddress.Country, eachAddress.Country, StringComparison.OrdinalIgnoreCase) == 0))
                        {
                            double maxSortYardCapacity;
                            double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                            VendorStorageSite vS = new VendorStorageSite()
                            {
                                MaxStorageCapacity = maxSortYardCapacity,
                                CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                                StorageSiteType = 1
                            };
                            eachAddress.VendorStorageSites.Add(vS);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        var sortYardAddress = new Address();

                        sortYardAddress.Address1 = sortYard.SortYardAddress.AddressLine1;
                        sortYardAddress.Address2 = sortYard.SortYardAddress.AddressLine2;
                        sortYardAddress.City = sortYard.SortYardAddress.City;
                        sortYardAddress.PostalCode = sortYard.SortYardAddress.Postal;
                        sortYardAddress.Province = sortYard.SortYardAddress.Province;
                        sortYardAddress.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country)
                                ? "Canada"
                                : sortYard.SortYardAddress.Country;
                        sortYardAddress.AddressType = (int)AddressTypeEnum.Sortyard;
                        sortYardAddress.Contacts = null;

                        double maxSortYardCapacity;
                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        VendorStorageSite vS = new VendorStorageSite()
                        {
                            MaxStorageCapacity = maxSortYardCapacity,
                            CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                            StorageSiteType = 1
                        };

                        sortYardAddress.VendorStorageSites.Add(vS);

                        addresses.Add(sortYardAddress);
                    }
                }
            }

            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            //vendoractivehistory
            var vendorActiveHistory = new VendorActiveHistory()
            {
                ActiveState = true,
                ActiveStateChangeDate = DateTime.Now.Date,
                VendorID = vendor.ID,
                CreateDate = DateTime.UtcNow,
                IsValid = true
            };

            var currentUser = TMSContext.TMSPrincipal.Identity;
            vendorActiveHistory.CreatedBy = currentUser.Name;

            vendor.VendorActiveHistories.Add(vendorActiveHistory);

            var productPurchased = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();

            applicationRPMModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = productPurchased.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            });

            var productProduced = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.RPMType).ToList();

            applicationRPMModel.TireDetails.ProductsProduceType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = productProduced.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            });

            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                applicationRepository.UpdateApplicationEntity(app);
                vendor.Number = vendorRepository.GetVendorRegistrationNumber(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue).ToString();//OTSTM2-508
                vendorRepository.AddVendor(vendor);

                //Add VendorAttachments
                var vendorAttachments = new List<VendorAttachment>();
                app.Attachments.ToList().ForEach(c =>
                {
                    var vendorAttachment = new VendorAttachment();
                    vendorAttachment.AttachmentId = c.ID;
                    vendorAttachment.VendorId = vendor.ID;
                    vendorAttachments.Add(vendorAttachment);
                });
                vendorRepository.AddVendorAttachments(vendorAttachments);
                vendorRepository.UpdateVendorKeywords(vendor);
                //Update applicationnote
                applicationRepository.UpdateApplicatioNoteWithVendorId(vendor.ID, app.ID);

                //Initialize Claim for approved application
                //ET-37 Stop creating claims
                //claimsRepository.InitializeClaimForVendor(vendor.ID);

                transationScope.Complete();
            }
            return vendor.ID;
        }

        private void MoveAttachmentToVendor(RPMRegistrationModel applicationRPMModel, Vendor vendor)
        {
            var vendorSupportingDocumentMBL = new VendorSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentMBL);

            var vendorSupportingDocumentCLI = new VendorSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCLI);

            var vendorSupportingDocumentHST = new VendorSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentHST);

            var vendorSupportingDocumentWSIB = new VendorSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentWSIB);

            var vendorSupportingDocumentCHQ = new VendorSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCHQ);

        }

        private void AddApplicationSupportingDocuments(RPMRegistrationModel applicationRPMModel, Application app)
        {
            var applicationSupportingDocumentMBL = new ApplicationSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentMBL);

            var applicationSupportingDocumentCLI = new ApplicationSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCLI);

            var applicationSupportingDocumentHST = new ApplicationSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentHST);

            var applicationSupportingDocumentWSIB = new ApplicationSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentWSIB);

            var applicationSupportingDocumentCHQ = new ApplicationSupportingDocument()
            {
                Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCHQ);
        }

        public int UpdateApproveApplication(RPMRegistrationModel applicationRPMModel, int vendorId)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;
            var vendor = vendorRepository.FindVendorByID(vendorId);
            var deleteAddresses = new List<Address>(); //OTSTM2-644 use existing list to collect sortyard addresses that to be deleted
            var newContactAddresses = new List<ContactAddressDto>();
            var deleteContacts = new List<Contact>(); //OTSTM2-50


            if (vendor != null)
            {
                if (!(vendor.GpUpdate ?? false))
                {
                    var participantExisting = new GpParticipantUpdate(vendor);
                    var newContact = applicationRPMModel.ContactInformationList.First();
                    var participantNew = new GpParticipantUpdate()
                    {
                        MailingAddress1 = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine1 : applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1,
                        MailingAddress2 = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine2 : applicationRPMModel.BusinessLocation.MailingAddress.AddressLine2,
                        MailingCity = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.City : applicationRPMModel.BusinessLocation.MailingAddress.City,
                        MailingCountry = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.Country : applicationRPMModel.BusinessLocation.MailingAddress.Country,
                        MailingPostalCode = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.Postal : applicationRPMModel.BusinessLocation.MailingAddress.Postal,
                        MailingProvince = applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness ? applicationRPMModel.BusinessLocation.BusinessAddress.Province : applicationRPMModel.BusinessLocation.MailingAddress.Province,
                        PrimaryContactName = string.Format("{0} {1}", newContact.ContactInformation.FirstName, newContact.ContactInformation.LastName),
                        PrimaryContactPhone = newContact.ContactInformation.PhoneNumber ?? string.Empty,
                        IsTaxExempt = applicationRPMModel.RPMDetails.IsTaxExempt,
                        IsActive = vendor.IsActive
                    };
                    if (!participantExisting.Equals(participantNew))
                    {
                        vendor.GpUpdate = true;
                    }
                }
                vendor.BusinessName = applicationRPMModel.BusinessLocation.BusinessName;
                vendor.OperatingName = applicationRPMModel.BusinessLocation.OperatingName;
                vendor.BusinessStartDate = applicationRPMModel.RPMDetails.BusinessStartDate;
                vendor.BusinessNumber = applicationRPMModel.RPMDetails.BusinessNumber;
                vendor.CommercialLiabHSTNumber = applicationRPMModel.RPMDetails.CommercialLiabHstNumber;
                vendor.IsTaxExempt = applicationRPMModel.RPMDetails.IsTaxExempt;
                vendor.CommercialLiabInsurerName = applicationRPMModel.RPMDetails.CommercialLiabInsurerName;
                vendor.CommercialLiabInsurerExpDate = applicationRPMModel.RPMDetails.CommercialLiabInsurerExpDate;
                vendor.HIsGVWR = applicationRPMModel.RPMDetails.HIsGvwr;
                vendor.CVORExpiryDate = applicationRPMModel.RPMDetails.CvorExpiryDate;
                vendor.HasMoreThanOneEmp = applicationRPMModel.RPMDetails.HasMoreThanOneEmp;
                vendor.WSIBNumber = applicationRPMModel.RPMDetails.WsibNumber;
                vendor.CompanyYearEndDate = applicationRPMModel.RPMDetails.BusinessFicalYearEndDate;
                vendor.VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue;
                vendor.LastUpdatedBy = updatedBy;
                vendor.LastUpdatedDate = DateTime.UtcNow;
                vendor.OtherRegistrantSubType = applicationRPMModel.TireDetails.OtherRegistrantSubType;
                vendor.OtherProcessProduct = applicationRPMModel.TireDetails.OtherProcessProduct;
                vendor.ContactInfo = string.Format("{0} {1}, {2}", applicationRPMModel.ContactInformationList[0].ContactInformation.FirstName,
                                applicationRPMModel.ContactInformationList[0].ContactInformation.LastName, applicationRPMModel.ContactInformationList[0].ContactInformation.PhoneNumber);
                //added
                vendor.SigningAuthorityFirstName = applicationRPMModel.TermsAndConditions.SigningAuthorityFirstName;
                vendor.SigningAuthorityLastName = applicationRPMModel.TermsAndConditions.SigningAuthorityLastName;
                vendor.SigningAuthorityPosition = applicationRPMModel.TermsAndConditions.SigningAuthorityPosition;
                vendor.FormCompletedByFirstName = applicationRPMModel.TermsAndConditions.FormCompletedByFirstName;
                vendor.FormCompletedByLastName = applicationRPMModel.TermsAndConditions.FormCompletedByLastName;
                vendor.FormCompletedByPhone = applicationRPMModel.TermsAndConditions.FormCompletedByPhone;
                vendor.AgreementAcceptedByFullName = applicationRPMModel.TermsAndConditions.AgreementAcceptedByFullName;
                vendor.AgreementAcceptedCheck = applicationRPMModel.TermsAndConditions.AgreementAcceptedCheck;

                //Save vendor supporting document
                UpdateVendorSupportingDocuments(applicationRPMModel, vendor);
            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                vendorPrimaryAddress.Address1 = applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine1;
                vendorPrimaryAddress.Address2 = applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine2;
                vendorPrimaryAddress.City = applicationRPMModel.BusinessLocation.BusinessAddress.City;
                vendorPrimaryAddress.Province = applicationRPMModel.BusinessLocation.BusinessAddress.Province;
                vendorPrimaryAddress.PostalCode = applicationRPMModel.BusinessLocation.BusinessAddress.Postal;
                vendorPrimaryAddress.Country = applicationRPMModel.BusinessLocation.BusinessAddress.Country;
                vendorPrimaryAddress.Phone = applicationRPMModel.BusinessLocation.Phone;
                vendorPrimaryAddress.Email = applicationRPMModel.BusinessLocation.Email;
                vendorPrimaryAddress.Phone = applicationRPMModel.BusinessLocation.Phone;
                vendorPrimaryAddress.Email = applicationRPMModel.BusinessLocation.Email;
                vendorPrimaryAddress.Ext = applicationRPMModel.BusinessLocation.Extension;
                vendorPrimaryAddress.AddressType = (int)AddressTypeEnum.Business;
            }

            //vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                    (applicationRPMModel.BusinessLocation.MailingAddress != null) &&
                    (!string.IsNullOrWhiteSpace(applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
                if (vendorMailingAddress != null)
                {
                    vendorMailingAddress.Address1 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1;
                    vendorMailingAddress.Address2 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine2;
                    vendorMailingAddress.City = applicationRPMModel.BusinessLocation.MailingAddress.City;
                    vendorMailingAddress.Province = applicationRPMModel.BusinessLocation.MailingAddress.Province;
                    vendorMailingAddress.PostalCode = applicationRPMModel.BusinessLocation.MailingAddress.Postal;
                    vendorMailingAddress.Country = applicationRPMModel.BusinessLocation.MailingAddress.Country;
                    vendorMailingAddress.Phone = applicationRPMModel.BusinessLocation.Phone;
                    vendorMailingAddress.Email = applicationRPMModel.BusinessLocation.Email;
                }
                else
                {
                    var mailingAddress = new Address()
                    {
                        Address1 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1,
                        Address2 = applicationRPMModel.BusinessLocation.MailingAddress.AddressLine2,
                        City = applicationRPMModel.BusinessLocation.MailingAddress.City,
                        Province = applicationRPMModel.BusinessLocation.MailingAddress.Province,
                        PostalCode = applicationRPMModel.BusinessLocation.MailingAddress.Postal,
                        Country = applicationRPMModel.BusinessLocation.MailingAddress.Country,
                        Phone = applicationRPMModel.BusinessLocation.Phone,
                        Email = applicationRPMModel.BusinessLocation.Email,
                        AddressType = (int)AddressTypeEnum.Mail
                    };

                    vendor.VendorAddresses.Add(mailingAddress);
                }
            }

            else
            {
                //OTSTM2-476
                var currentMailingAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == (int)AddressTypeEnum.Mail);

                if (currentMailingAddress != null)
                {
                    vendor.VendorAddresses.Remove(currentMailingAddress);
                }
            }

            //OTSTM2-50 remove contact
            var contactIdList = applicationRPMModel.ContactInformationList.Select(c => c.ContactInformation.ID).ToList();
            foreach (var contactAddress in vendor.VendorAddresses.ToList())
            {
                foreach (var contact in contactAddress.Contacts.ToList())
                {
                    if (!contactIdList.Contains(contact.ID))
                    {
                        deleteContacts.Add(contact);
                    }
                }
            }

            foreach (var applicationContactInfo in applicationRPMModel.ContactInformationList)
            {
                var contact = new Contact();
                if (applicationContactInfo.isEmptyContact)
                {
                    var contactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                    contact = contactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                    contactAddress.Contacts.Remove(contact);
                    vendor.VendorAddresses.Remove(contactAddress);
                }
                else
                {
                    //check if this is a new contact and address
                    if (applicationContactInfo.ContactInformation.AddressID == 0)
                    {
                        contact = new Contact() { IsPrimary = false };
                        ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        if (!applicationContactInfo.ContactInformation.ContactAddressSameAsBusiness)
                        {
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contactAddress.Contacts.Add(contact);
                            vendor.VendorAddresses.Add(contactAddress);
                        }
                        else
                        {
                            var vendorBusinessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationRPMModel.BusinessLocation.BusinessAddress.ID);
                            vendorBusinessAddress.Contacts.Add(contact);
                            vendorRepository.UpdateVendor(vendor); //OTSTM2-1037 make sure contact 1 with same as Business Address checked to be saved earlier, so that has a smaller ID value
                        }
                    }
                    else
                    {
                        var currentContactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                        var businessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationRPMModel.BusinessLocation.BusinessAddress.ID);
                        var oldIsAddressSameAsBusiness = currentContactAddress.AddressType == (int)AddressTypeEnum.Business;
                        var newIsAddressSameAsBusiness = applicationContactInfo.ContactAddressSameAsBusinessAddress;
                        if (newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //no change update contact information
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                        else if (newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //changed selection to same as business
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            contact.AddressID = businessAddress.ID;
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = businessAddress
                            });
                            deleteAddresses.Add(contact.Address);
                        }
                        else if (!newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //remove from business address and add new address
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = contactAddress
                            });
                        }
                        else if (!newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //no change update address and contact information
                            ModelAdapter.UpdateAddress(ref currentContactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                    }
                }
            }

            //OTSTM2-644 remove sort yard
            var sortYardIdList = applicationRPMModel.SortYardDetails.Select(c => c.SortYardAddress.ID).ToList(); //SortYardAddress.ID and VendorStorageSite.ID are same thing
            foreach (var vendorStorageSite in vendor.VendorStorageSites.ToList())
            {
                if (!sortYardIdList.Contains(vendorStorageSite.ID))
                {
                    deleteAddresses.Add(vendorStorageSite.Address); //OTSTM2-644 use existing list to collect sortyard addresses that to be deleted
                    vendor.VendorStorageSites.Remove(vendorStorageSite);
                }
            }

            foreach (var sortYard in applicationRPMModel.SortYardDetails)
            {
                bool addNewSortYard = false;
                if (sortYard.SortYardAddress.ID != 0)
                {
                    var existingSortYardDetails = vendor.VendorStorageSites.Where(r => r.ID == sortYard.SortYardAddress.ID);

                    if (existingSortYardDetails.Any())
                    {
                        var selectedSortYard = existingSortYardDetails.FirstOrDefault();

                        selectedSortYard.Address.Address1 = sortYard.SortYardAddress.AddressLine1;
                        selectedSortYard.Address.Address2 = sortYard.SortYardAddress.AddressLine2;
                        selectedSortYard.Address.City = sortYard.SortYardAddress.City;
                        selectedSortYard.Address.PostalCode = sortYard.SortYardAddress.Postal;
                        selectedSortYard.Address.Province = sortYard.SortYardAddress.Province;
                        selectedSortYard.Address.Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country;

                        double maxSortYardCapacity;
                        double.TryParse(sortYard.MaxStorageCapacity, out maxSortYardCapacity);

                        selectedSortYard.MaxStorageCapacity = maxSortYardCapacity;
                        selectedSortYard.CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber;
                        selectedSortYard.StorageSiteType = 1;
                    }
                    else
                    {
                        addNewSortYard = true;
                    }
                }
                else
                {
                    addNewSortYard = true;
                }

                if (addNewSortYard)
                {
                    var vendorStorageSite = new VendorStorageSite()
                    {
                        Address = new Address()
                        {
                            Address1 = sortYard.SortYardAddress.AddressLine1,
                            Address2 = sortYard.SortYardAddress.AddressLine2,
                            City = sortYard.SortYardAddress.City,
                            PostalCode = sortYard.SortYardAddress.Postal,
                            Province = sortYard.SortYardAddress.Province,
                            Country = string.IsNullOrWhiteSpace(sortYard.SortYardAddress.Country) ? "Canada" : sortYard.SortYardAddress.Country
                        },
                        MaxStorageCapacity = Convert.ToDouble(sortYard.MaxStorageCapacity),
                        CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                        StorageSiteType = 1,
                        UnitType = 3
                    };

                    vendor.VendorStorageSites.Add(vendorStorageSite);
                }
            }

            var addresses = new List<Address>();
            addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            //var listOfAllProducts = vendorRepository.GetDefaultItems(ItemCategoryEnum.ProductsProduced);
            var listOfAllProducts = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            foreach (var item in applicationRPMModel.TireDetails.TireDetailsItemType)
            {
                if (item.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == item.ID))
                    {
                        var result = listOfAllProducts.Single(s => s.ID == item.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == item.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            }

            //var listOfProductsProduced = vendorRepository.GetDefaultItems(ItemCategoryEnum.RPMType);
            var listOfProductsProduced = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.RPMType).ToList();
            foreach (var productProduced in applicationRPMModel.TireDetails.ProductsProduceType)
            {
                if (productProduced.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == productProduced.ID))
                    {
                        var result = listOfProductsProduced.Single(r => r.ID == productProduced.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == productProduced.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            }

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                vendorRepository.RemoveAddresses(deleteAddresses); //OTSTM2-644 Deleting address should be done before deleting vendorStorageSite in vendor because of foreign key
                vendorRepository.RemoveContacts(deleteContacts); //OTSTM2-50
                vendorRepository.UpdateVendorContacts(newContactAddresses, vendor.ID);
                vendorRepository.UpdateVendor(vendor);
                vendorRepository.UpdateVendorKeywords(vendor);
                transationScope.Complete();
            }

            return vendor.ID;
        }

        private void UpdateVendorSupportingDocuments(RPMRegistrationModel applicationRPMModel, Vendor vendor)
        {
            foreach (var supportDocOption in vendor.VendorSupportingDocuments)
            {
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.MBL)
                {
                    supportDocOption.Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionMBL;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CLI)
                {
                    supportDocOption.Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCLI;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.HST)
                {
                    supportDocOption.Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionHST;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.WSIB)
                {
                    supportDocOption.Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionWSIB;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CHQ)
                {
                    supportDocOption.Option = applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCHQ;
                }
            }
        }

        public RPMRegistrationModel GetApprovedApplication(int vendorId)
        {
            RPMRegistrationModel applicationRPMModel = new RPMRegistrationModel();

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                applicationRPMModel.Status = vendor.RegistrantStatus == null ? ApplicationStatusEnum.Approved :
                    (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), vendor.RegistrantStatus);

                applicationRPMModel.ActiveInactive.ApprovedDate = vendor.CreatedDate;

                if (vendor.ApplicationID.HasValue)
                {
                    var app = this.applicationRepository.FindApplicationByAppID(vendor.ApplicationID.Value);
                    applicationRPMModel.ID = app.ID;
                    applicationRPMModel.UserIPAddress = app.UserIP;
                    applicationRPMModel.SubmittedDate = app.SubmittedDate;
                    //OTSTM2-973
                    applicationRPMModel.TermsAndConditions.TermsAndConditionsID = app.TermsAndConditionsID;
                }

                applicationRPMModel.BusinessLocation.BusinessName = vendor.BusinessName;
                applicationRPMModel.BusinessLocation.OperatingName = vendor.OperatingName;
                applicationRPMModel.RPMDetails.BusinessStartDate = vendor.BusinessStartDate;
                applicationRPMModel.RPMDetails.BusinessNumber = vendor.BusinessNumber;
                applicationRPMModel.RPMDetails.CommercialLiabHstNumber = vendor.CommercialLiabHSTNumber;
                applicationRPMModel.RPMDetails.IsTaxExempt = vendor.IsTaxExempt;
                applicationRPMModel.RPMDetails.CommercialLiabInsurerName = vendor.CommercialLiabInsurerName;
                applicationRPMModel.RPMDetails.CommercialLiabInsurerExpDate = vendor.CommercialLiabInsurerExpDate;
                applicationRPMModel.RPMDetails.HIsGvwr = vendor.HIsGVWR;
                applicationRPMModel.RPMDetails.CvorExpiryDate = vendor.CVORExpiryDate;
                applicationRPMModel.RPMDetails.HasMoreThanOneEmp = vendor.HasMoreThanOneEmp;
                applicationRPMModel.RPMDetails.WsibNumber = vendor.WSIBNumber;
                applicationRPMModel.RPMDetails.BusinessFicalYearEndDate = vendor.CompanyYearEndDate;
                applicationRPMModel.RegistrationNumber = Convert.ToInt32(vendor.Number);

                applicationRPMModel.TermsAndConditions.SigningAuthorityFirstName = vendor.SigningAuthorityFirstName;
                applicationRPMModel.TermsAndConditions.SigningAuthorityLastName = vendor.SigningAuthorityLastName;
                applicationRPMModel.TermsAndConditions.SigningAuthorityPosition = vendor.SigningAuthorityPosition;
                applicationRPMModel.TermsAndConditions.FormCompletedByFirstName = vendor.FormCompletedByFirstName;
                applicationRPMModel.TermsAndConditions.FormCompletedByLastName = vendor.FormCompletedByLastName;
                applicationRPMModel.TermsAndConditions.FormCompletedByPhone = vendor.FormCompletedByPhone;
                applicationRPMModel.TermsAndConditions.AgreementAcceptedByFullName = vendor.AgreementAcceptedByFullName;
                applicationRPMModel.TermsAndConditions.AgreementAcceptedCheck = vendor.AgreementAcceptedCheck ?? true;
                applicationRPMModel.CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue));
            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine1 = vendorPrimaryAddress.Address1;
                applicationRPMModel.BusinessLocation.BusinessAddress.AddressLine2 = vendorPrimaryAddress.Address2;
                applicationRPMModel.BusinessLocation.BusinessAddress.City = vendorPrimaryAddress.City;
                applicationRPMModel.BusinessLocation.BusinessAddress.Province = vendorPrimaryAddress.Province;
                applicationRPMModel.BusinessLocation.BusinessAddress.Postal = vendorPrimaryAddress.PostalCode;
                applicationRPMModel.BusinessLocation.BusinessAddress.Country = vendorPrimaryAddress.Country;
                applicationRPMModel.BusinessLocation.Phone = vendorPrimaryAddress.Phone;
                applicationRPMModel.BusinessLocation.Email = vendorPrimaryAddress.Email;
                applicationRPMModel.BusinessLocation.Extension = vendorPrimaryAddress.Ext;
                applicationRPMModel.BusinessLocation.BusinessAddress.ID = vendorPrimaryAddress.ID;
            }

            var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
            if (vendorMailingAddress != null)
            {
                applicationRPMModel.BusinessLocation.MailingAddress.AddressLine1 = vendorMailingAddress.Address1;
                applicationRPMModel.BusinessLocation.MailingAddress.AddressLine2 = vendorMailingAddress.Address2;
                applicationRPMModel.BusinessLocation.MailingAddress.City = vendorMailingAddress.City;
                applicationRPMModel.BusinessLocation.MailingAddress.Province = vendorMailingAddress.Province;
                applicationRPMModel.BusinessLocation.MailingAddress.Postal = vendorMailingAddress.PostalCode;
                applicationRPMModel.BusinessLocation.MailingAddress.Country = vendorMailingAddress.Country;
                applicationRPMModel.BusinessLocation.MailingAddress.ID = vendorMailingAddress.ID;
                applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness = false;
            }
            else
            {
                applicationRPMModel.BusinessLocation.MailingAddressSameAsBusiness = true;
            }
            var contactRegistrations = new List<ContactRegistrationModel>();
            foreach (var contactAddress in vendor.VendorAddresses)
            {
                foreach (var contact in contactAddress.Contacts)
                {
                    ContactRegistrationModel contactRegistrationModel = new ContactRegistrationModel()
                    {
                        ContactInformation = new ContactModel()
                        {
                            ID = contact.ID,
                            AddressID = contact.AddressID,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Position = contact.Position,
                            PhoneNumber = contact.PhoneNumber,
                            Ext = contact.Ext,
                            AlternatePhoneNumber = contact.AlternatePhoneNumber,
                            Email = contact.Email,
                            IsPrimary = (contact.IsPrimary.HasValue ? contact.IsPrimary.Value : false)
                        },

                        ContactAddress = new ContactAddressModel()
                        {
                            AddressLine1 = contact.Address.Address1,
                            AddressLine2 = contact.Address.Address2,
                            City = contact.Address.City,
                            Province = contact.Address.Province,
                            Postal = contact.Address.PostalCode,
                            Country = contact.Address.Country,
                        },
                        ContactAddressSameAsBusinessAddress = (applicationRPMModel.BusinessLocation.BusinessAddress.ID == contact.AddressID)
                    };

                    //applicationRPMModel.ContactInformationList.Add(contactRegistrationModel);
                    contactRegistrations.Add(contactRegistrationModel);
                }
            }
            foreach (var contact in contactRegistrations.OrderBy(i => i.ContactInformation.ID).ToList())
            {
                applicationRPMModel.ContactInformationList.Add(contact);
            }

            applicationRPMModel.ContactInformationList = applicationRPMModel.ContactInformationList.OrderBy(c => c.ContactInformation.ID).ToList();

            foreach (var sortYard in vendor.VendorStorageSites)
            {
                RPMSortYardDetailsRegistrationModel sortYardDetailsRegistrationModel = new RPMSortYardDetailsRegistrationModel()
                {
                    SortYardAddress = new SortYardAddressModel()
                    {
                        ID = sortYard.ID,
                        AddressLine1 = sortYard.Address.Address1,
                        AddressLine2 = sortYard.Address.Address2,
                        City = sortYard.Address.City,
                        Province = sortYard.Address.Province,
                        Postal = sortYard.Address.PostalCode,
                        Country = sortYard.Address.Country
                    },

                    CertificateOfApprovalNumber = sortYard.CertificateOfApprovalNumber,
                    MaxStorageCapacity = sortYard.MaxStorageCapacity.ToString(),
                    StorageSiteType = sortYard.StorageSiteType,
                };

                applicationRPMModel.SortYardDetails.Add(sortYardDetailsRegistrationModel);
            }

            applicationRPMModel.TireDetails = new RPMTireDetailsRegistrationModel();
            applicationRPMModel.TireDetails.TireDetailsItemType = new List<RPMTireDetailsItemTypeModel>();

            var listOfProductsPurchased = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced).ToList();
            listOfProductsPurchased.ToList().ForEach(s =>
            {
                bool isChecked = false;
                if (vendor.Items.Any(r => r.ID == s.ID))
                {
                    isChecked = true;
                }

                RPMTireDetailsItemTypeModel tireDetailsItemTypeModel = new RPMTireDetailsItemTypeModel()
                {
                    ID = s.ID,
                    Description = s.Description,
                    Name = s.Name,
                    ShortName = s.ShortName,
                    isChecked = isChecked,
                    IsActive = s.IsActive,
                    ItemTypeID = s.ItemType,
                };
                applicationRPMModel.TireDetails.TireDetailsItemType.Add(tireDetailsItemTypeModel);
            });

            var listOfProductsProduced = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.RPMType).ToList();

            applicationRPMModel.TireDetails.ProductsProduceType = new List<RPMTireDetailsItemTypeModel>();

            foreach (var productsProduced in listOfProductsProduced)
            {
                bool isChecked = false;
                if (vendor.Items.Any(r => r.ID == productsProduced.ID))
                {
                    isChecked = true;
                }

                RPMTireDetailsItemTypeModel productsProducedTypeModel = new RPMTireDetailsItemTypeModel()
                {
                    ID = productsProduced.ID,
                    Description = productsProduced.Description,
                    Name = productsProduced.Name,
                    ShortName = productsProduced.ShortName,
                    isChecked = isChecked,
                    IsActive = productsProduced.IsActive,
                };

                applicationRPMModel.TireDetails.ProductsProduceType.Add(productsProducedTypeModel);
            }

            applicationRPMModel.TireDetails.OtherProcessProduct = vendor.OtherProcessProduct;
            applicationRPMModel.TireDetails.OtherRegistrantSubType = vendor.OtherRegistrantSubType;

            applicationRPMModel.SupportingDocuments = new SupportingDocumentsRegistrationModel();
            foreach (var supportingDocument in vendor.VendorSupportingDocuments)
            {
                SupportingDocumentTypeEnum supportingDocumentType;
                if (Enum.TryParse(supportingDocument.TypeID.ToString(), out supportingDocumentType))
                {
                    switch (supportingDocumentType)
                    {
                        case SupportingDocumentTypeEnum.MBL:
                            applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionMBL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CLI:
                            applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCLI = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.HST:
                            applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionHST = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.WSIB:
                            applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionWSIB = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CHQ:
                            applicationRPMModel.SupportingDocuments.SupportingDocumentsOptionCHQ = supportingDocument.Option;
                            break;
                        default:
                            break;
                    }
                }
            }

            var bankingInformation = vendor.BankInformations.FirstOrDefault();

            if (bankingInformation != null)
            {
                applicationRPMModel.BankingInformation.BankName = bankingInformation.BankName;
                applicationRPMModel.BankingInformation.TransitNumber = bankingInformation.TransitNumber;
                applicationRPMModel.BankingInformation.BankNumber = bankingInformation.BankNumber;
                applicationRPMModel.BankingInformation.AccountNumber = bankingInformation.AccountNumber;
                applicationRPMModel.BankingInformation.EmailPaymentTransferNotification = bankingInformation.EmailPaymentTransferNotification;
                applicationRPMModel.BankingInformation.NotifyToPrimaryContactEmail = bankingInformation.NotifyToPrimaryContactEmail;
            }

            return applicationRPMModel;
        }

        #endregion Workflow
    }
}