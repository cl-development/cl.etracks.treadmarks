﻿namespace CL.TMS.TMService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerTM = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerTM = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerTM
            // 
            this.serviceProcessInstallerTM.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.serviceProcessInstallerTM.Password = null;
            this.serviceProcessInstallerTM.Username = null;
            // 
            // serviceInstallerTM
            // 
            this.serviceInstallerTM.DisplayName = "TreadMarks Service";
            this.serviceInstallerTM.ServiceName = "TreadMarksService";
            this.serviceInstallerTM.Description = "TreadMarks Routine Task Service";
            this.serviceInstallerTM.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerTM,
            this.serviceInstallerTM});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerTM;
        private System.ServiceProcess.ServiceInstaller serviceInstallerTM;
    }
}