//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class surplus_deliveries_original
    {
        public long surplus_deliveries_id { get; set; }
        public string unifier { get; set; }
        public string ots_approve { get; set; }
        public string ots_doc_received { get; set; }
        public string ots_record_modified { get; set; }
        public string ots_comments { get; set; }
        public Nullable<System.DateTime> date_delivered { get; set; }
        public decimal ptr_form_number { get; set; }
        public decimal processor_number { get; set; }
        public string processor_name { get; set; }
        public string surplus_code { get; set; }
        public string scale_ticket_number { get; set; }
        public Nullable<decimal> claim_amount { get; set; }
        public Nullable<decimal> weight_variance { get; set; }
        public string ots_invalid { get; set; }
        public string ots_revised { get; set; }
        public string surplus_type { get; set; }
        public string authorization_number { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_user { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string modified_user { get; set; }
        public Nullable<int> scan_detail_id { get; set; }
        public string ots_nil { get; set; }
    }
}
