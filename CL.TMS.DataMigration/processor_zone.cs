//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class processor_zone
    {
        public processor_zone()
        {
            this.deliveries_to_ontario_processors = new HashSet<deliveries_to_ontario_processors>();
            this.deliveries_to_ontario_processors_original = new HashSet<deliveries_to_ontario_processors_original>();
            this.hauler_dot_premium = new HashSet<hauler_dot_premium>();
            this.hauler_dot_premium_rate = new HashSet<hauler_dot_premium_rate>();
            this.hauler_inventory_delivered = new HashSet<hauler_inventory_delivered>();
            this.hauler_northern_premium = new HashSet<hauler_northern_premium>();
            this.processor_fsa = new HashSet<processor_fsa>();
        }
    
        public int processor_zone_id { get; set; }
        public string name { get; set; }
        public string greater_zone { get; set; }
        public string long_name { get; set; }
    
        public virtual ICollection<deliveries_to_ontario_processors> deliveries_to_ontario_processors { get; set; }
        public virtual ICollection<deliveries_to_ontario_processors_original> deliveries_to_ontario_processors_original { get; set; }
        public virtual ICollection<hauler_dot_premium> hauler_dot_premium { get; set; }
        public virtual ICollection<hauler_dot_premium_rate> hauler_dot_premium_rate { get; set; }
        public virtual ICollection<hauler_inventory_delivered> hauler_inventory_delivered { get; set; }
        public virtual ICollection<hauler_northern_premium> hauler_northern_premium { get; set; }
        public virtual ICollection<processor_fsa> processor_fsa { get; set; }
    }
}
