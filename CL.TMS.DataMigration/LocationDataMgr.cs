﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class LocationDataMgr
    {
        private otsdbEntitiesLegacy dbContext;
        public LocationDataMgr()
        {
            dbContext = new otsdbEntitiesLegacy();
        }
   
        public IEnumerable<location> GetAllLocationsByRegNo(decimal regNo)
        {
            return this.dbContext.locations.Where(r => r.reg_no == regNo);
        }
    }
}


