//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class rpm_product_sale_original
    {
        public long rpm_product_sale_id { get; set; }
        public long rpm_claim_summary_id { get; set; }
        public string comment { get; set; }
        public System.DateTime date_sold { get; set; }
        public long product_type_id { get; set; }
        public string product_other_description { get; set; }
        public string sales_invoice_no { get; set; }
        public string purchaser { get; set; }
        public bool approve { get; set; }
        public bool documents_received { get; set; }
        public bool record_modified { get; set; }
        public System.DateTime created_date { get; set; }
        public string created_by { get; set; }
        public System.DateTime last_updated_date { get; set; }
        public string last_updated_by { get; set; }
    
        public virtual rpm_claim_summary_original rpm_claim_summary_original { get; set; }
    }
}
