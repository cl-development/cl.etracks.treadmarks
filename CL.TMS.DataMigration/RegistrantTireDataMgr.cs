﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class RegistrantTireDataMgr
    {
        private otsdbEntitiesLegacy dbContext;
        public RegistrantTireDataMgr()
        {
            dbContext = new otsdbEntitiesLegacy();
        }
   
        public IEnumerable<registrant_tire_lookup> GetAllRegistrantTireByRegNo(decimal regNo)
        {
            return this.dbContext.registrant_tire_lookup.Where(r => r.registrant_id == regNo);
        }

        //For RPM
        public IEnumerable<end_use_market_lookup> GetAllEndUserMarketByRegNo(decimal regNo)
        {
            return this.dbContext.end_use_market_lookup.Where(r => r.reg_no == regNo);
        }

        //For Processor
        public IEnumerable<registrant_product> GetAllRegProductsByRegNo(decimal regNo)
        {
            return this.dbContext.registrant_product.Where(r => r.registrant_id == regNo);
        }
    }
}


