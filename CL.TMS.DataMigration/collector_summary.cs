//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class collector_summary
    {
        public int summary_id { get; set; }
        public string claim_period { get; set; }
        public string registration_number { get; set; }
        public string collector_name { get; set; }
        public string uid { get; set; }
        public string user_email { get; set; }
        public string submit { get; set; }
        public string web_submission_date { get; set; }
        public Nullable<decimal> total_pay_pre_tax { get; set; }
        public Nullable<float> total_pay { get; set; }
    }
}
