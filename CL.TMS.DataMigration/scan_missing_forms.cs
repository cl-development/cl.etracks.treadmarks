//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class scan_missing_forms
    {
        public int id { get; set; }
        public int scan_summary_id { get; set; }
        public long form_number { get; set; }
    
        public virtual scan_summary scan_summary { get; set; }
    }
}
