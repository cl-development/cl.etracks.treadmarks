﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class BankInfoMgr
    {
        public BankInfo GetImportedBankInfo(int VendorIDTM)
        {
            var con = ConfigurationManager.ConnectionStrings["TMSDB"].ToString();

            BankInfo record = null;
            using (SqlConnection myConnection = new SqlConnection(con))
            {
                string oString = "Select * from BankingInfo20160404 where VendorIDTM=@VendorIDTM";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                oCmd.Parameters.AddWithValue("@VendorIDTM", VendorIDTM.ToString());
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    if(oReader.Read())
                    {
                        record = new BankInfo();
                        record.EmailToAddress = oReader["EmailToAddress"].ToString().Trim();
                        record.EmailCcAddress = oReader["EmailCcAddress"].ToString().Trim();
                        record.SERIES = oReader["SERIES"].ToString().Trim();
                        record.CustomerVendor_ID = oReader["CustomerVendor_ID"].ToString().Trim();
                        record.ADRSCODE = oReader["ADRSCODE"].ToString().Trim();
                        record.VENDORID = oReader["VENDORID"].ToString().Trim();
                        record.CUSTNMBR = oReader["CUSTNMBR"].ToString().Trim();
                        record.EFTUseMasterID = oReader["EFTUseMasterID"].ToString().Trim();
                        record.EFTBankType = oReader["EFTBankType"].ToString().Trim();
                        record.FRGNBANK = oReader["FRGNBANK"].ToString().Trim();
                        record.INACTIVE = oReader["INACTIVE"].ToString().Trim();
                        record.BANKNAME = oReader["BANKNAME"].ToString().Trim();
                        record.EFTBankAcct = oReader["EFTBankAcct"].ToString().Trim();
                        record.EFTBankBranch = oReader["EFTBankBranch"].ToString().Trim();
                        record.GIROPostType = oReader["GIROPostType"].ToString().Trim();
                        record.EFTBankCode = oReader["EFTBankCode"].ToString().Trim();
                        record.EFTBankBranchCode = oReader["EFTBankBranchCode"].ToString().Trim();
                        record.EFTBankCheckDigit = oReader["EFTBankCheckDigit"].ToString().Trim();
                        record.BSROLLNO = oReader["BSROLLNO"].ToString().Trim();
                        record.IntlBankAcctNum = oReader["IntlBankAcctNum"].ToString().Trim();
                        record.SWIFTADDR = oReader["SWIFTADDR"].ToString().Trim();
                        record.CustVendCountryCode = oReader["CustVendCountryCode"].ToString().Trim();
                        record.DeliveryCountryCode = oReader["DeliveryCountryCode"].ToString().Trim();
                        record.BNKCTRCD = oReader["BNKCTRCD"].ToString().Trim();
                        record.CBANKCD = oReader["CBANKCD"].ToString().Trim();
                        record.ADDRESS1 = oReader["ADDRESS1"].ToString().Trim();
                        record.ADDRESS2 = oReader["ADDRESS2"].ToString().Trim();
                        record.ADDRESS3 = oReader["ADDRESS3"].ToString().Trim();
                        record.ADDRESS4 = oReader["ADDRESS4"].ToString().Trim();
                        record.RegCode1 = oReader["RegCode1"].ToString().Trim();
                        record.RegCode2 = oReader["RegCode2"].ToString().Trim();
                        record.BankInfo7 = oReader["BankInfo7"].ToString().Trim();
                        record.EFTTransitRoutingNo = oReader["EFTTransitRoutingNo"].ToString().Trim();
                        record.CURNCYID = oReader["CURNCYID"].ToString().Trim();
                        record.EFTTransferMethod = oReader["EFTTransferMethod"].ToString().Trim();
                        record.EFTAccountType = oReader["EFTAccountType"].ToString().Trim();
                        record.EFTPrenoteDate = oReader["EFTPrenoteDate"].ToString().Trim();
                        record.EFTTerminationDate = oReader["EFTTerminationDate"].ToString().Trim();
                        record.DEX_ROW_ID = oReader["DEX_ROW_ID"].ToString().Trim();
                        record.EFTTransitRoutingNo_Str = oReader["EFTTransitRoutingNo_Str"].ToString().Trim();
                        record.VendorIDTM = oReader["VendorIDTM"].ToString().Trim();
                    }
                    myConnection.Close();
                }
            }
            return record;
            
        }
    }
}

//SELECT concat('record.', COLUMN_NAME,' = oReader["', COLUMN_NAME,'"].ToString().Trim();')
//FROM INFORMATION_SCHEMA.COLUMNS
//WHERE TABLE_NAME = 'bankinfo'
