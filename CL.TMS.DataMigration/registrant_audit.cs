//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class registrant_audit
    {
        public decimal registration_number { get; set; }
        public string business_name { get; set; }
        public string business_franchise_name { get; set; }
        public string business_number { get; set; }
        public string location_address_1 { get; set; }
        public string location_address_2 { get; set; }
        public string location_address_3 { get; set; }
        public string location_city { get; set; }
        public string location_province { get; set; }
        public string location_postal_code { get; set; }
        public string location_country { get; set; }
        public string location_phone { get; set; }
        public string location_fax { get; set; }
        public string mailing_address_1 { get; set; }
        public string mailing_address_2 { get; set; }
        public string mailing_address_3 { get; set; }
        public string mailing_city { get; set; }
        public string mailing_province { get; set; }
        public string mailing_postal_code { get; set; }
        public string mailing_country { get; set; }
        public System.DateTime business_start_date { get; set; }
        public string primary_contact_name { get; set; }
        public string primary_contact_position { get; set; }
        public string primary_contact_email { get; set; }
        public string primary_contact_address_1 { get; set; }
        public string primary_contact_address_2 { get; set; }
        public string primary_contact_address_3 { get; set; }
        public string primary_contact_city { get; set; }
        public string primary_contact_province { get; set; }
        public string primary_contact_postal_code { get; set; }
        public string primary_contact_country { get; set; }
        public string primary_contact_phone { get; set; }
        public string primary_contact_fax { get; set; }
        public string s_pst_number { get; set; }
        public string gst_number { get; set; }
        public Nullable<bool> authorized_sig_complete { get; set; }
        public string business_operating_name { get; set; }
        public string location_email_address { get; set; }
        public Nullable<System.DateTime> date_received { get; set; }
        public Nullable<System.DateTime> activation_date { get; set; }
        public Nullable<System.DateTime> confirmation_mailed_date { get; set; }
        public Nullable<long> preferred_communication { get; set; }
        public string form_completed_by_name { get; set; }
        public Nullable<decimal> form_completed_by_phone { get; set; }
        public string chpm_commercial_liability_insurer_name { get; set; }
        public Nullable<System.DateTime> chpm_commercial_liability_insurer_expiry { get; set; }
        public string chpm_worker_health_safety_cert_no { get; set; }
        public byte[] chpm_permits_certifications_description { get; set; }
        public Nullable<decimal> c_points_additional { get; set; }
        public Nullable<long> c_points_type { get; set; }
        public Nullable<decimal> c_points_garage { get; set; }
        public Nullable<decimal> c_points_used_vehicle { get; set; }
        public Nullable<decimal> c_points_other { get; set; }
        public Nullable<long> h_prim_bus_addr_sort_yards { get; set; }
        public Nullable<long> h_num_sort_yards { get; set; }
        public Nullable<long> h_max_sort_yards_storage_cap { get; set; }
        public Nullable<long> h_sort_yard_id { get; set; }
        public Nullable<long> hpm_prim_bus_addr_storage_cap { get; set; }
        public Nullable<long> pm_num_storage_sites { get; set; }
        public Nullable<long> pm_max_storage_sites_cap { get; set; }
        public Nullable<long> pm_storage_site_id { get; set; }
        public Nullable<decimal> schp_tires_handled_lookup { get; set; }
        public string c_points_other_desc { get; set; }
        public string hpm_cert_of_approval_num { get; set; }
        public Nullable<long> m_end_use_market { get; set; }
        public string m_end_user_market_desc { get; set; }
        public Nullable<System.DateTime> p_company_year_end { get; set; }
        public Nullable<long> p_products_produced { get; set; }
        public string p_products_produced_desc { get; set; }
        public long registrant_type_id { get; set; }
        public string registrant_sub_type_other_desc { get; set; }
        public long registrant_sub_type_id { get; set; }
        public string registrant_sub_type_desc { get; set; }
        public string registrant_type_desc { get; set; }
        public Nullable<bool> h_flag { get; set; }
        public Nullable<bool> c_flag { get; set; }
        public Nullable<bool> s_flag { get; set; }
        public Nullable<bool> p_flag { get; set; }
        public Nullable<bool> m_flag { get; set; }
        public string last_updated_by { get; set; }
        public Nullable<System.DateTime> last_updated_date { get; set; }
        public byte[] registrant_notes { get; set; }
        public Nullable<long> c_num_ltt { get; set; }
        public Nullable<long> c_num_mtt { get; set; }
        public Nullable<long> c_num_adt { get; set; }
        public Nullable<long> c_num_sst { get; set; }
        public Nullable<long> c_num_lst { get; set; }
        public Nullable<long> c_num_sotr { get; set; }
        public Nullable<long> c_num_motr { get; set; }
        public Nullable<long> c_num_lotr { get; set; }
        public Nullable<long> c_num_gotr { get; set; }
        public Nullable<bool> c_tires_in_storage { get; set; }
        public Nullable<sbyte> c_sub_c_flag { get; set; }
        public Nullable<sbyte> c_generator_flag { get; set; }
        public byte[] chpm_facility_description { get; set; }
        public Nullable<long> status { get; set; }
        public long checkout_num { get; set; }
        public System.DateTime created_date_ts { get; set; }
        public Nullable<long> in_batch { get; set; }
        public string record_state { get; set; }
        public int active_state { get; set; }
        public Nullable<System.DateTime> active_state_change_date { get; set; }
    }
}
