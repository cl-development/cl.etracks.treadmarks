//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class hauler_inventory_collected
    {
        public int id { get; set; }
        public string unifier { get; set; }
        public int pickup_zone_id { get; set; }
        public int transaction_type_id { get; set; }
        public int tire_type_id { get; set; }
        public Nullable<decimal> weight { get; set; }
    
        public virtual pickup_zone pickup_zone { get; set; }
        public virtual transaction_type transaction_type { get; set; }
    }
}
