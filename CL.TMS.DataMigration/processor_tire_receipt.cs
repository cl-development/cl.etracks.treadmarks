//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class processor_tire_receipt
    {
        public int processor_tire_receipt_id { get; set; }
        public System.DateTime date_delivered { get; set; }
        public string ptr_form_number { get; set; }
        public decimal hauler_number { get; set; }
        public string hauler_name { get; set; }
        public string guarunteed_out_of_province { get; set; }
        public Nullable<decimal> scale_weight { get; set; }
        public string scale_ticket_number { get; set; }
        public Nullable<decimal> estimated_weight { get; set; }
        public string ots_approve { get; set; }
        public string ots_doc_received { get; set; }
        public string ots_record_modified { get; set; }
        public string ots_comments { get; set; }
        public string unifier { get; set; }
        public Nullable<int> type_of_load { get; set; }
        public Nullable<int> accounting_code { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<decimal> weight_variance_total { get; set; }
        public string ots_revised { get; set; }
        public string ots_invalid { get; set; }
        public string created_user { get; set; }
        public string modified_user { get; set; }
        public Nullable<int> scan_detail_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public Nullable<decimal> claim_amount { get; set; }
        public string not_eligible_for_payment { get; set; }
        public string device_name { get; set; }
    
        public virtual registrant registrant { get; set; }
    }
}
