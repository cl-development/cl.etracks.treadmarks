//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class tire_type_rate
    {
        public long tire_type_rate_id { get; set; }
        public long tire_id { get; set; }
        public string rate_description { get; set; }
        public string tire_category { get; set; }
        public System.DateTime effective_start_date { get; set; }
        public System.DateTime effective_end_date { get; set; }
        public System.DateTime created_date { get; set; }
        public string created_user { get; set; }
        public System.DateTime modified_date { get; set; }
        public string modified_user { get; set; }
    }
}
