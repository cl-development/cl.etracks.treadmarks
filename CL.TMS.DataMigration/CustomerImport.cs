﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Repository.Registrant;
using CL.TMS.Repository.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class CustomerImport
    {
        public void ImportStewards()
        {
            RegistrantDataMgr regMgr = new RegistrantDataMgr();
            RegistrantTireDataMgr regTireMgr = new RegistrantTireDataMgr();
            DateTime startedAt = DateTime.Now;
            int importedCount = 0;

            var registrants = regMgr.GetStewards(3);

            VendorRepository vendorRepository = new VendorRepository();

            foreach (var reg in registrants)
            {
                var existingVendor = vendorRepository.GetVendorByNumber(reg.registration_number.ToString());
                if (existingVendor == null)
                {
                    var customer = new Customer();

                    customer.CustomerType = 1;
                    customer.Audit = false;
                    customer.RegistrationNumber = reg.registration_number.ToString();
                    customer.BusinessName = reg.business_name;
                    customer.FranchiseName = reg.business_franchise_name;
                    customer.OperatingName = reg.business_operating_name;
                    customer.BusinessNumber = reg.business_number;
                    customer.AuthSigComplete = reg.authorized_sig_complete != null ? reg.authorized_sig_complete.ToString() : null;
                    customer.ReceivedDate = (reg.date_received != null && reg.date_received != DateTime.MinValue) ? reg.date_received.Value.ToUniversalTime() : (DateTime?)null;
                    customer.GracePeriodStartDate = reg.grace_period_start_date != null ? reg.grace_period_start_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.GracePeriodEndDate = reg.grace_period_end_date != null ? reg.grace_period_end_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.CommercialLiabInsurerName = reg.chpm_commercial_liability_insurer_name;
                    customer.CommercialLiabInsurerExpDate = reg.chpm_commercial_liability_insurer_expiry;
                    customer.WorkerHealthSafetyCertNo = reg.chpm_worker_health_safety_cert_no;
                    customer.PermitsCertDescription = reg.chpm_permits_certifications_description.ToString();
                    customer.AmendedAgreement = reg.c_amended_agreement;//
                    customer.AmendedDate = (reg.c_amended_date != DateTime.MinValue && reg.c_amended_date != null) ? reg.c_amended_date.Value.ToUniversalTime() : (DateTime?)null; //
                    customer.IsTaxExempt = reg.is_tax_exempt;
                    
                    string primaryBusinessActivity = reg.registrant_sub_type_desc;

                    switch (reg.registrant_sub_type_desc)
                    {
                        case "Recreational Vehicle Manufacturer":
                        case "Industrial Vehicle Manufacturer":
                        case "Off-the-Road Equipment Manufacturer":
                            primaryBusinessActivity = "Off-the-Road Equipment Manufacturer";
                            break;
                        case "Car / Light Truck Dealer":
                        case "Commercial Truck Dealer":
                        case "Trailer Dealer":
                        case "Recreational Vehicle Dealer":
                        case "Agricultural Vehicle Dealer":
                        case "Motorcycle or Moped Dealer":
                        case "Industrial Vehicle Dealer":
                        case "Off-the-Road Equipment Dealer":
                        case "Mass Merchant (Retail)":
                        case "Tire Dealer (Retail)":
                        case "Tire Dealer (Wholesale)":
                            primaryBusinessActivity = "Tire Dealer";
                            break;
                        case "Agricultural Vehicle Manufacturer":
                        case "Motorcycle or Moped Manufacturer":
                        case "Other Tire Importer":
                            primaryBusinessActivity = "Other Tire Importer";
                            break;
                    }
                    customer.PrimaryBusinessActivity = primaryBusinessActivity ?? "Other Tire Importer";

                    var regActivityHistoryRecords = regMgr.GetRegActivityHistory(reg.registration_number);

                    foreach (var regActivityHistory in regActivityHistoryRecords.Select((value, index) => new { Value = value, Index = index }))
                    {
                        customer.CustomerActiveHistories.Add(new CustomerActiveHistory()
                        {
                            ActiveState = regActivityHistory.Value.active_state,
                            ActiveStateChangeDate = regActivityHistory.Value.active_state_change_date.ToUniversalTime(),
                            CreateDate = reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1),
                            IsCurrent = regActivityHistory.Index == 0 ? true : false,
                            Reason = regActivityHistory.Value.active_state ? string.Empty : "Other",
                            OtherReason = regActivityHistory.Value.active_state ? string.Empty : "Please see Legacy."               
                        });
                    }

                    if (regActivityHistoryRecords.Count() == 0)
                    {
                        customer.CustomerActiveHistories.Add(new CustomerActiveHistory()
                        {
                            ActiveState = reg.active_state_change_date == null || reg.active_state == 1,
                            ActiveStateChangeDate = reg.active_state_change_date ?? (reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1)),
                            CreateDate = reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1),
                            IsCurrent = true,
                            Reason = (reg.active_state_change_date == null || reg.active_state == 1) ? string.Empty : "Other",
                            OtherReason = (reg.active_state_change_date == null || reg.active_state == 1) ? string.Empty : "Please see Legacy."                
                        });
                    }
                    
                    //Looks like it is the BusinessActivityType used in Vendor and Customer table but name used is BusinessActivityId
                    // 1 for steward and 2 for collector defined in enum
                    customer.BusinessActivityID = 1;
                    customer.GSTNumber = reg.gst_number;
                    customer.AuthorizedSigComplete = reg.authorized_sig_complete.ToString();
                    customer.LocationEmailAddress = reg.location_email_address;
                    customer.DateReceived = reg.date_received != null ? reg.date_received.Value.ToUniversalTime() : (DateTime?)null;
                    customer.ConfirmationMailedDate = (reg.confirmation_mailed_date != null && reg.confirmation_mailed_date != DateTime.MinValue) ? reg.confirmation_mailed_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.PreferredCommunication = (int?)reg.preferred_communication;
                    customer.RegistrantTypeID = (int)reg.registrant_type_id;//
                    customer.RegistrantSubTypeID = (int)reg.registrant_sub_type_id;//
                    customer.LastUpdatedBy = reg.last_updated_by;
                    customer.LastUpdatedDate = reg.last_updated_date != null ? reg.last_updated_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.CheckoutNum = (int?)reg.checkout_num;
                    customer.CreatedDate = reg.created_date_ts != null ? reg.created_date_ts.Value : new DateTime(1970, 1, 1);
                    customer.InBatch = (int)reg.in_batch;
                    customer.ActiveStateChangeDate = reg.active_state_change_date != null ? reg.active_state_change_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.ActivationDate = (reg.activation_date != null && reg.activation_date != DateTime.MinValue) ? reg.activation_date.Value.ToUniversalTime() : (DateTime?)null;
                    customer.IsActive = reg.active_state_change_date == null || reg.active_state == 1;
                    customer.BusinessDesc = reg.registrant_type_desc;
                    customer.MOE = reg.moe_escalation;
                    customer.BusinessStartDate = (reg.business_start_date != null && reg.business_start_date != DateTime.MinValue && reg.business_start_date > new DateTime(1970, 1, 1)) ? reg.business_start_date.ToUniversalTime() : new DateTime(1970, 1, 1);
                    customer.RemitsPerYear = true;
                    customer.RegistrantStatus = ApplicationStatusEnum.Approved.ToString();
                    customer.CommercialLiabHSTNumber = reg.gst_number;
                    customer.ContactInfo = reg.primary_contact_name + ", " + reg.primary_contact_phone;
             

                    ImportTires(regTireMgr, vendorRepository, customer);

                    AddStewardAddresses(reg, customer);

                    vendorRepository.AddCustomer(customer);
                    importedCount += 1;
                    System.Console.WriteLine("Imoprted: " + reg.registration_number);                 
                }
            }
            System.Console.WriteLine("Started at: " + startedAt + ", Imoprted: " + importedCount + " records, Finish time: " + DateTime.Now);                 
        }

        private static void ImportTires(RegistrantTireDataMgr regTireMgr, VendorRepository vendorRepository, Customer customer)
        {
            var registrantTires = regTireMgr.GetAllRegistrantTireByRegNo(Convert.ToDecimal(customer.RegistrationNumber));

            var defaultItems = vendorRepository.GetDefaultItems((int)ItemCategoryEnum.TireType);
            foreach (var tire in registrantTires)
            {
                //check for values
                var result = defaultItems.Single(s => s.ID == (tire.tire_id + 1));
                if (result != null)
                {
                    customer.Items.Add(result);
                }
            }
        }

        private void AddStewardAddresses(registrant reg, Customer customer)
        {
            var addressBusiness = new Address();
            addressBusiness.AddressType = (int)AddressTypeEnum.Business;
            addressBusiness.Address1 = reg.location_address_1;
            addressBusiness.Address2 = reg.location_address_2;
            addressBusiness.Address3 = reg.location_address_3;
            addressBusiness.City = reg.location_city;
            addressBusiness.Country = reg.location_country;
            addressBusiness.Email = reg.location_email_address;
            addressBusiness.Fax = reg.location_fax;
            addressBusiness.Phone = reg.location_phone;
            addressBusiness.PostalCode = reg.location_postal_code;
            addressBusiness.Province = reg.location_province;
            customer.CustomerAddresses.Add(addressBusiness);

            var addressMail = new Address();
            addressMail.AddressType = (int)AddressTypeEnum.Mail;
            addressMail.Address1 = reg.mailing_address_1;
            addressMail.Address2 = reg.mailing_address_2;
            addressMail.Address3 = reg.mailing_address_3;
            addressMail.City = reg.mailing_city;
            addressMail.Country = reg.mailing_country;
            //addressMail.Email = reg;
            //addressMail.Fax = reg;
            //addressMail.Phone = reg;
            addressMail.PostalCode = reg.mailing_postal_code;
            addressMail.Province = reg.mailing_province;
            customer.CustomerAddresses.Add(addressMail);

            var addressContact = new Address();
            addressContact.AddressType = (int)AddressTypeEnum.Contact;
            addressContact.Address1 = reg.primary_contact_address_1;
            addressContact.Address2 = reg.primary_contact_address_2;
            addressContact.Address3 = reg.primary_contact_address_3;
            addressContact.City = reg.primary_contact_city;
            addressContact.Country = reg.primary_contact_country;
            addressContact.Email = reg.primary_contact_email;
            addressContact.Fax = reg.primary_contact_fax;
            addressContact.Phone = reg.primary_contact_phone;
            addressContact.PostalCode = reg.primary_contact_postal_code;
            addressContact.Province = reg.primary_contact_province;

            addressContact.Contacts.Add(new Contact()
            {
                Email = reg.primary_contact_email,
                FaxNumber = reg.primary_contact_fax,
                FirstName = reg.primary_contact_name,
                IsPrimary = true,
                Name = reg.primary_contact_name,
                PhoneNumber = reg.primary_contact_phone,
                Position = reg.primary_contact_position,
                PreferredContactMethod = reg.preferred_communication == 1 ? "Email" : (reg.preferred_communication == 2 ? "Fax" : "Mail"),
            });

            customer.CustomerAddresses.Add(addressContact);

        }

        public void ImportTSFRemittance()
        {
            RegistrantDataMgr regMgr = new RegistrantDataMgr();

            VendorRepository vendorRepo = new VendorRepository();
            var allCustomers = vendorRepo.GetAllCustomers().ToList();
            TSFRemittanceRepository tsfRepo = new TSFRemittanceRepository();
            var claimPeriods = tsfRepo.GetAllTSFClaimPeriods();
            
            foreach (Customer c in allCustomers)
            {
                var tsfSummaries = regMgr.GetTSFClaimSummary(Convert.ToDecimal(c.RegistrationNumber));

                foreach (tsf_claim_summary tsfSummary in tsfSummaries)
                {
                    var claimPeriod = claimPeriods.FirstOrDefault(p => p.StartDate == tsfSummary.reporting_period_start && p.EndDate == tsfSummary.reporting_period_end);

                    if (claimPeriod == null)
                        throw new Exception("No claim Period found!");

                    var existingTSFSummary = tsfRepo.GetTSFRemittance(claimPeriod.ID, c.ID);
                    if (existingTSFSummary == null)
                    {
                        var tsfClaim = new TSFClaim();

                        tsfClaim.PeriodID = claimPeriod.ID;

                        tsfClaim.CustomerID = c.ID;
                        tsfClaim.RegistrationNumber = c.RegistrationNumber;

                        tsfClaim.PreparedBy = tsfSummary.prepared_by;
                        //From front end
                        tsfClaim.TotalTSFDue = tsfSummary.total_remittance_payable;
                        tsfClaim.TotalRemittancePayable = tsfSummary.total_tsf_due;
                        tsfClaim.PaymentAmount = tsfSummary.payment_amount;
                        
                        tsfClaim.ApplicableTaxesGst = tsfSummary.applicable_taxes_gst;
                        tsfClaim.ApplicableTaxesHst = tsfSummary.applicable_taxes_hst;
                        tsfClaim.TSFDuePostHst = tsfSummary.tsf_due_post_hst;
                        tsfClaim.TSFDuePreHst = tsfSummary.tsf_due_pre_hst;
                        tsfClaim.Penalties = tsfSummary.penalties;
                        tsfClaim.Interest = tsfSummary.interest;
                        tsfClaim.PaymentType = tsfSummary.payment_type;
                        tsfClaim.PaymentReferenceNumber = tsfSummary.payment_reference_number;
                        tsfClaim.ReceiptDate = (tsfSummary.receipt_date != null && tsfSummary.receipt_date != DateTime.MinValue) ? tsfSummary.receipt_date : null;
                        tsfClaim.DepositDate = (tsfSummary.deposite_date != null && tsfSummary.deposite_date != DateTime.MinValue) ? (DateTime?)tsfSummary.deposite_date : null;
                        tsfClaim.RecordState = "Approved";//tsfSummary.record_state;
                        tsfClaim.CreatedDate = tsfSummary.created_date;
                        tsfClaim.CreatedUser = tsfSummary.created_user;
                        tsfClaim.ModifiedDate = tsfSummary.modified_date;
                        tsfClaim.ModifiedUser = tsfSummary.modified_user;
                        tsfClaim.InBatch = tsfSummary.in_batch;
                        tsfClaim.UniqueNumber = tsfSummary.unique_number;
                        tsfClaim.SubmissionDate = (tsfSummary.submission_date != null && tsfSummary.submission_date != DateTime.MinValue) ? tsfSummary.submission_date : null;
                        tsfClaim.IsCreatedInTm = tsfSummary.is_created_in_tm;
                        tsfClaim.ChequeReferenceNumber = tsfSummary.cheque_reference_number;
                        tsfClaim.Account = tsfSummary.account;

                        tsfClaim.IsPenaltyOverride = (byte)Convert.ToInt16(tsfSummary.is_penalty_override);
                        tsfClaim.IsTaxOverride = (byte)Convert.ToInt16(tsfSummary.is_tax_override);

                        
                        //tsfClaim.AssignToUser = tsfSummary.
                        //tsfClaim.PenaltiesManually = tsfSummary.
                        //tsfClaim.SupportingDocOption = tsfSummary.
                        //tsfClaim.StartedDate = tsfSummary.start
                        //tsfClaim.AssignedDate = tsfSummary.
                        tsfClaim.ApprovedDate = (tsfSummary.deposite_date != null && tsfSummary.deposite_date != DateTime.MinValue) ? (DateTime?)tsfSummary.deposite_date : null;
                        //tsfClaim.IsActive = tsfSummary.acti

                        tsfClaim.BalanceDue = tsfSummary.total_tsf_due;

                        var tsfDetails = regMgr.GetTSFClaimDetails(tsfSummary.tsf_claim_summary_id);
                        foreach (tsf_claim_detail tsfDetail in tsfDetails)
                        {
                            var tsfClaimDetail = new TSFClaimDetail();

                            int itemId = GetItemId(tsfSummary.reporting_period_start, (int)tsfDetail.tire_id);
                            if (itemId == 0)
                                throw new Exception("no Item Id found");
                            tsfClaimDetail.ItemID = itemId;
                            tsfClaimDetail.TireSupplied = (int)tsfDetail.tire_supplied;
                            tsfClaimDetail.NegativeAdjustment = (int)tsfDetail.negative_adjustment;
                            tsfClaimDetail.CountSupplied = (int)tsfDetail.count_supplied;
                            tsfClaimDetail.TSFRate = tsfDetail.tsf_rate;
                            tsfClaimDetail.TSFDue = tsfDetail.count_supplied * tsfDetail.tsf_rate; 
                            tsfClaimDetail.CreatedDate = tsfDetail.created_date;
                            tsfClaimDetail.CreatedUser = tsfDetail.created_user;
                            tsfClaimDetail.ModifiedDate = tsfDetail.modified_date;
                            tsfClaimDetail.ModifiedUser = tsfDetail.modified_user;
                            //tsfClaimDetail.AdjustmentReasonType = tsfDetail.ad
                            //tsfClaimDetail.OtherDesc = tsfDetail.
                            tsfClaim.TSFClaimDetails.Add(tsfClaimDetail);
                        }

                        var enhancedNotes = regMgr.GetEnhancedNotes("tsf_claim_summary", tsfSummary.tsf_claim_summary_id);

                        foreach (var enhancedNote in enhancedNotes)
                        {
                            tsfClaim.TSFRemittanceNotes.Add(new TSFRemittanceNote() { CreatedDate = enhancedNote.note_timestamp, Note = enhancedNote.note, UserId = null });
                        }

                        tsfRepo.AddTSFClaim(tsfClaim);
                        System.Console.WriteLine("Added Remittance for " + c.RegistrationNumber + ", Period: " + tsfSummary.reporting_period_start);
                    }
                }
            }

        }

        int GetItemId(DateTime reportingPeriod, int legacyItemId)
        {
            if (reportingPeriod.Date <= Convert.ToDateTime("2013-03-31").Date)
            {
                switch (legacyItemId)
                {
                    case 1:
                        return 39;
                    case 2:
                        return 40;
                    case 3:
                        return 41;
                    case 4:
                        return 42;
                    case 5:
                        return 43;
                    case 6:
                        return 44;
                    case 7:
                        return 45;
                    case 8:
                        return 46;
                }
            }
            else
            {
                switch (legacyItemId)
                {
                    case 1:
                        return 47;
                    case 2:
                        return 48;
                    case 3:
                        return 49;
                    case 4:
                        return 50;
                    case 5:
                        return 51;
                    case 6:
                        return 52;
                    case 7:
                        return 53;
                    case 8:
                        return 54;
                    case 9:
                        return 55;
                    case 10:
                        return 56;
                    case 11:
                        return 57;
                    case 12:
                        return 58;
                    case 13:
                        return 59;
                    case 14:
                        return 60;
                    case 15:
                        return 61;
                    case 16:
                        return 62;
                    case 17:
                        return 63;
                    case 18:
                        return 64;
                }
            }
            return 0;
        }
    }
}
//customer.TerritoryCode = reg.;
//customer.GlobalDimension1Code = reg;
//customer.GlobalDimension2Code = reg;
//customer.CreditLimit = reg;
//customer.TelexAnswerBack = reg;
//customer.TaxRegistrationNo = reg;
//customer.TaxRegistrationNo2 = reg;
//customer.HomePage = reg;
//customer.BusinessGroupCode = reg;
//customer.CustomerGroupCode = reg;
//customer.DepartmentCode = reg;
//customer.ProjectCode = reg;
//customer.PurchaserCode = reg;
//customer.SalesCampaignCode = reg;
//customer.SalesPersonCode = reg;
//customer.UpdateToFinancialSoftware = reg;
//customer.MailingAddressSameAsBusiness = reg.add;
//customer.ApplicationID = reg;
//customer.PayOTSFee = reg.pa;
//customer.MOESwitchDate = reg;
//customer.Audit = reg;
//customer.AuditSwitchDate = reg;
//customer.RemitsPerYear = reg;
//virtual ICollection<BusinessActivity> BusinessActivities = reg ;
//virtual ICollection<Item> Items = reg ;
//virtual Application Application = reg ;
//customer.RemitsPerYearSwitchDate = reg;
//customer.WSIBNumber = reg.chpm_worker_health_safety_cert_no;
//customer.HasMoreThanOneEmp = reg;
//customer.CommercialLiabHSTNumber = reg;
                
