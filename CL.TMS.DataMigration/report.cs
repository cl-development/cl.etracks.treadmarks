//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class report
    {
        public long report_id { get; set; }
        public string report_name { get; set; }
        public string report_description { get; set; }
        public string report_procedure { get; set; }
        public string report_softname { get; set; }
        public string report_page { get; set; }
        public Nullable<bool> set_dynamic_params { get; set; }
        public string url { get; set; }
        public Nullable<int> report_category_id { get; set; }
    }
}
