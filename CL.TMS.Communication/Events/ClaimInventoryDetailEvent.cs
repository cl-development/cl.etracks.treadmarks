﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Communication.Events
{
    /// <summary>
    /// Claim inventory detail event
    /// </summary>
    public class ClaimInventoryDetailEvent: PubSubEvent<ClaimReportPayload>
    {
    }

    public class ClaimReportPayload
    {
        /// <summary>
        /// ClaimId
        /// </summary>
        public int ClaimId { get; set; }
        public int TransactionId { get; set; }

        public bool IsSynchronous { get; set; }
    }
}
