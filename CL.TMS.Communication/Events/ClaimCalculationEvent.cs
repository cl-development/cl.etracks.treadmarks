﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;

namespace CL.TMS.Communication.Events
{
    /// <summary>
    /// Claim calculation event
    /// </summary>
    public class ClaimCalculationEvent : PubSubEvent<ClaimCalculationPayload>
    {

    }

    public class ClaimCalculationPayload
    {
        public int ClaimId { get; set; }
        public bool IsSynchronous { get; set; }
    }
}
