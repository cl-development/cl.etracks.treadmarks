﻿using System;
using System.Linq;
using CL.Framework.Logging;
using CL.TMS.ApplicaitonBLL;
using CL.TMS.Communication.Events;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository;
using CL.TMS.ServiceContracts;
using Microsoft.Practices.Prism.PubSubEvents;

namespace CL.TMS.ApplicationServices
{
    public class SystemService:ISystemService
    {
        private ISettingRepository settinRepository;
        private IInvitationRepository invitationRepository;
        private IEventAggregator eventAggregator;
        public SystemService(ISettingRepository settinRepository, IInvitationRepository invitationRepository, IEventAggregator eventAggregator)
        {
            this.settinRepository = settinRepository;
            this.invitationRepository = invitationRepository;
            this.eventAggregator = eventAggregator;
            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            eventAggregator.GetEvent<InvitationEvent>().Subscribe(DoAction, true);
        }

        private void DoAction(Invitation obj)
        {
            LogManager.LogInfo(string.Format("SystemService: Receive message from ServiceA - Failed to add invitation {0},{1}", obj.FirstName, obj.LastName));
        }
        public string GetSettingValue(string key)
        {
            //Get setting from cache first
            var appSettingLoader = new AppSettingsLoader(settinRepository);
            var setting = appSettingLoader.LoadAllSettings().FirstOrDefault(c => c.Key == key);
            return setting == null ? string.Empty : setting.Value;
        }


        public void AddInvitation(Invitation invitation)
        {
            try
            {
                var invitationBO = new InvitationBO(invitationRepository);
                invitationBO.AddInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow=ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
    }
}
