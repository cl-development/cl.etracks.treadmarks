﻿using CL.TMS.Communication.Events;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ServiceContracts;
using Microsoft.Practices.Prism.PubSubEvents;

namespace CL.TMS.ApplicationServices
{
    public class ServiceA:IServiceA
    {
        private IEventAggregator eventAggregator;

        public ServiceA(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }
        public void DoA()
        {           
            var invitation = new Invitation();
            invitation.FirstName = "Tony";
            invitation.LastName = "Cai";
            eventAggregator.GetEvent<InvitationEvent>().Publish(invitation);
        }
    }
}
