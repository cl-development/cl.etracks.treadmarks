﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using CL.TMS.IRepository.Claims;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common;
using CL.TMS.Communication.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.ClaimCalculator;
using System.Threading.Tasks;
using CL.TMS.DataContracts;
using CL.TMS.UI.Common.Helper;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.Framework.BLL;

namespace CL.TMS.SystemBLL
{
    public class ClaimsBO : BaseBO
    {
        private IClaimsRepository claimsRepository;
        private IUserRepository userRepository;
        private IVendorRepository vendorRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository; //OTSTM2-400
        private readonly IGpRepository gpRepository;  //OTSTM2-484     
        private ITransactionRepository transactionRepository;
        public ClaimsBO(IClaimsRepository claimsRepository, IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository, IGpRepository gpRepository, ITransactionRepository transactionRepository)
        {
            this.claimsRepository = claimsRepository;
            this.userRepository = userRepository;
            this.vendorRepository = vendorRepository;
            this.eventAggregator = eventAggregator;
            this.messageRepository = messageRepository; //OTSTM2-400   
            this.gpRepository = gpRepository; //OTSTM2-484
            this.transactionRepository = transactionRepository;
        }

        public List<StaffAllClaimsModel> GetExportDetails(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            return this.claimsRepository.GetExportDetails(searchText, orderBy, sortDirection, columnSearchText);
        }

        public PaginationDTO<StaffAllClaimsModel, Guid> LoadAllClaims(int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            int currUserId, routingCount = 0;
            currUserId = Convert.ToInt32(SecurityContextHelper.CurrentUser.Id);

            var claimsWrokflows = this.userRepository.GetUserClaimsWorkflowsByUserID(currUserId, "", TreadMarksConstants.RepresentativeWorkflow);
            routingCount = claimsWrokflows.Count;

            return claimsRepository.LoadAllClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, routingCount, columnSearchText);
        }

        public void UpdateAssignToUserIdForClaim(int? userId, int claimId)
        {
            Claim claim = this.claimsRepository.FindClaimByClaimId(claimId);
            if (userId != null)
            {
                claim.AssignToUserId = userId;
                claim.ReviewedBy = userId;
                claim.AssignDate = DateTime.UtcNow;
                claim.Status = EnumHelper.GetEnumDescription(ClaimStatus.UnderReview);
            }
            else
            {
                claim.Status = EnumHelper.GetEnumDescription(ClaimStatus.Submitted);
                claim.ReviewedBy = null;
                claim.AssignToUserId = null;
                claim.AssignDate = null;

                //Remove claim process
                claimsRepository.RemoveClaimProgress(claimId);

            }
            claimsRepository.UpdateClaim(claim);
        }
        public string CheckTransactionVendorStatus(SubmitClaimTransaction transaction, List<VendorStatusDateRangeModel> incomingVendorStatusDateRanges, List<VendorStatusDateRangeModel> outgoingVendorStatusDateRanges, UserReference user)
        {
            string warningVendorInactive = "[{0:D}, {1:HH:mm:ss}], {2} was inactive on transaction date.";
            string warningMsg = string.Empty;
            string returnVal = string.Empty;
            if (null != transaction)
            {
                //Check incoming vendor status
                if (transaction.IncomingVendorId != null)
                {
                    if (IsNotActive(incomingVendorStatusDateRanges, transaction.TransactionDate))
                    {
                        var vendorType = ((ClaimType)transaction.IncomingVendorType).ToString();
                        warningMsg = string.Format(warningVendorInactive, DateTime.Now, DateTime.Now, vendorType + " (" + transaction.IncomingVendorNumber + ")");
                        if (transactionRepository.UdpateTransactioProcessingStatus(transaction.TransactionId, TransactionProcessingStatus.Invalidated))
                        {
                            this.AddTransactionNote(transaction.TransactionId, warningMsg, null, TransactionNoteType.Warning);
                        }
                        returnVal += warningMsg;
                    }
                }
                //Check outgoing vendor status
                if (transaction.OutgoingVendorId != null)
                {
                    if (IsNotActive(outgoingVendorStatusDateRanges, transaction.TransactionDate))
                    {
                        var vendorType = ((ClaimType)transaction.OutgoingVendorType).ToString();
                        warningMsg = string.Format(warningVendorInactive, DateTime.Now, DateTime.Now, vendorType + " (" + transaction.OutgoingVendorNumber + ")");
                        if (transactionRepository.UdpateTransactioProcessingStatus(transaction.TransactionId, TransactionProcessingStatus.Invalidated))
                        {
                            this.AddTransactionNote(transaction.TransactionId, warningMsg, null, TransactionNoteType.Warning);
                        }
                        returnVal += warningMsg;
                    }
                }
            }
            return returnVal;
        }

        private bool IsNotActive(List<VendorStatusDateRangeModel> vendorStatusDateRanges, DateTime transactionDate)
        {
            var date = transactionDate.Date;
            var item = vendorStatusDateRanges.FirstOrDefault(c =>
                c.StartDate <= date && c.EndDate >= date
            );
            if (item != null)
            {
                return !item.isActive;
            }
            else
            {
                return true;
            }
        }

        public InternalNoteViewModel AddTransactionNote(int transactionId, string note, UserReference user, TransactionNoteType noteType = TransactionNoteType.General)
        {
            long? uId = null;
            string sAddedBy = "System Admin";
            if (user != null)
            {
                uId = user.Id;
                sAddedBy = user.FirstName + " " + user.LastName;
            }

            var internalNote = new TransactionNote()
            {
                TransactionId = transactionId,
                Note = note,
                CreatedDate = DateTime.UtcNow,
                UserId = uId,
                NoteType = noteType.ToString()
            };

            internalNote = transactionRepository.AddTransactionInternalNote(internalNote);

            return new InternalNoteViewModel() { Note = internalNote.Note, AddedBy = sAddedBy, AddedOn = internalNote.CreatedDate };
        }

        public void SubmitClaim(int claimId, bool isAutoApproved)
        {
            //check inactive vendor for claim
            var claimTransactions = claimsRepository.LoadClaimTransactions(claimId);

            //Load all vendor activehistory
            var vendorIds = new List<int>();
            vendorIds.AddRange(claimTransactions.Where(c => c.IncomingVendorId != null).Select(c => (int)c.IncomingVendorId).Distinct());
            vendorIds.AddRange(claimTransactions.Where(c => c.OutgoingVendorId != null).Select(c => (int)c.OutgoingVendorId).Distinct());
            var distinctVendorIds = vendorIds.Distinct().ToList();

            Dictionary<int, List<VendorStatusDateRangeModel>> vendorStatusDateRangesDic = new Dictionary<int, List<VendorStatusDateRangeModel>>();

            distinctVendorIds.ForEach(c =>
            {
                var result = vendorRepository.GetVendorStatusDateRanges(c).ToList();
                vendorStatusDateRangesDic.Add(c, result);

            });

            var user = SecurityContextHelper.CurrentUser;

            //Change to parallel to improve performance
            Parallel.ForEach(claimTransactions, (c) =>
            {
                if (c.ProcessingStatus != TransactionProcessingStatus.Invalidated.ToString())//skip all invalidated transaction
                {

                    var incomingVendorStatusDateRanges = new List<VendorStatusDateRangeModel>();
                    if ((c.IncomingVendorId != null) && (vendorStatusDateRangesDic.ContainsKey((int)c.IncomingVendorId)))
                    {
                        incomingVendorStatusDateRanges = vendorStatusDateRangesDic[(int)c.IncomingVendorId];
                    }
                    var outgoingVendorStatusDateRanges = new List<VendorStatusDateRangeModel>();
                    if ((c.OutgoingVendorId != null) && (vendorStatusDateRangesDic.ContainsKey((int)c.OutgoingVendorId)))
                    {
                        outgoingVendorStatusDateRanges = vendorStatusDateRangesDic[(int)c.OutgoingVendorId];
                    }
                    string noteText = CheckTransactionVendorStatus(c, incomingVendorStatusDateRanges, outgoingVendorStatusDateRanges, user);
                }
            });

            //Trigger claim calculation
            ClaimCalculationHelper.CalculateClaim(claimId);

            Claim claim = this.claimsRepository.FindClaimByClaimId(claimId);

            if (claim != null && ClaimStatus.Open.ToString() == claim.Status)
            {
                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //1. Update claim common properties
                    var isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499
                    var isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                    UpdateClaim(claim, isPreviousClaimOnHold, isPreviousAuditOnHold);

                    //OTSTM2-551 Add Activity for submission
                    AddActivity(claim, false);

                    //2. Do auto approve for mobile transaction
                    DoAutoApproveMobileTransaction(claim);

                    //3. Do late claim
                    //Fixed late claim (datetime boundary issue), only compare date portion
                    if (claim.ClaimPeriod.SubmitEnd.Date < DateTime.Now.Date)
                    {
                        //Do late claim calculation
                        //reset to be auto approve if it is late claim
                        isAutoApproved = true;
                        DoLateClaim(claimId, isAutoApproved, claim);
                        //Update claim
                        claimsRepository.UpdateClaim(claim);
                    }
                    else
                    {
                        if (claim.Participant.VendorType == 2)
                        {
                            //Execute Collector claim submission rules
                            ExecuteCollectorClaimSubmissionRules(claim, isAutoApproved);
                        }
                        //Update claim 
                        claimsRepository.UpdateClaim(claim);
                    }

                    transationScope.Complete();
                }

                //Triger claim inventory report calculation
                var claimReportPayload = new ClaimReportPayload { ClaimId = claimId, TransactionId = 0 };
                eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

                string emailTo = GetPrimaryContactByClaimId(claimId);
                if (EmailContentHelper.IsEmailEnabled("ClaimSubmitted") && emailTo != "")
                {
                    var emailModel = new ApplicationEmailModel()
                    {
                        RegistrationNumber = claim.Participant.Number,
                        BusinessName = claim.Participant.BusinessName,
                        MonthYear = claim.ClaimPeriod.ShortName,
                        Email = emailTo
                    };
                    SendEmail("ClaimSubmitted", emailModel);
                }

                //Send Approval email if Auto Approved
                if (claim.Status == "Approved")
                    SendApprovedEmail(claim);
            }
        }

        //OTSTM2-551
        private void AddActivity(Claim claim, bool isAutoApproved)
        {
            var activity = new Activity();
            activity.Initiator = isAutoApproved ? AppSettings.Instance.SystemUser.UserName : SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = isAutoApproved ? AppSettings.Instance.SystemUser.FirstName + " " + AppSettings.Instance.SystemUser.LastName : SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = string.Empty;
            activity.AssigneeName = string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.ClaimActivity;
            switch (claim.ClaimType)
            {
                case 2:
                    activity.ActivityArea = TreadMarksConstants.Collector;
                    break;
                case 3:
                    activity.ActivityArea = TreadMarksConstants.Hauler;
                    break;
                case 4:
                    activity.ActivityArea = TreadMarksConstants.Processor;
                    break;
                case 5:
                    activity.ActivityArea = TreadMarksConstants.RPM;
                    break;
            }
            activity.ObjectId = claim.ID;
            if (isAutoApproved)
            {
                activity.Message = string.Format("{0} <strong>approved</strong> this claim.", activity.InitiatorName);

                //TODO Note: below SystemLog entry is for debugging OTSTM2-942 
                List<ClaimTireOrigin> tireOrigins = claimsRepository.LoadClaimTireOrigins(claim.ID);
                var collectorClaimSummary = new CollectorClaimSummaryModel();

                //Load Tire Origin
                PopulateInbound(collectorClaimSummary, tireOrigins);

                //Load outbound
                List<Item> items = DataLoader.Items;
                List<ClaimDetailViewModel> claimDetails = claimsRepository.LoadClaimDetails(claim.ID, items);
                var collectorInventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claim.ID);
                PopulateOutbound(collectorClaimSummary, claimDetails, collectorInventoryAdjustments);
                ItemRow TotalInbound = collectorClaimSummary.TotalInbound;
                ItemRow TotalOutbound = collectorClaimSummary.TotalOutbound;

                string verboseTirecount =
                    "ClaimID:" + claim.ID.ToString() + " " +
                    "PLT:" + TotalInbound.PLT.ToString() + "=" + TotalOutbound.PLT.ToString() + " " +
                    "MT:" + TotalInbound.MT.ToString() + "=" + TotalOutbound.MT.ToString() + " " +
                    "AGLS:" + TotalInbound.AGLS.ToString() + "=" + TotalOutbound.AGLS.ToString() + " " +
                    "IND:" + TotalInbound.IND.ToString() + "=" + TotalOutbound.IND.ToString() + " " +
                    "SOTR:" + TotalInbound.SOTR.ToString() + "=" + TotalOutbound.SOTR.ToString() + " " +
                    "MOTR" + TotalInbound.MOTR.ToString() + "=" + TotalOutbound.MOTR.ToString() + " " +
                    "LOTR" + TotalInbound.LOTR.ToString() + "=" + TotalOutbound.LOTR.ToString() + " " +
                    "GOTR" + TotalInbound.GOTR.ToString() + "=" + TotalOutbound.GOTR.ToString();

                LogManager.LogInfo(string.Format("Collector Auto Approve Trace: UTC time {0} : [in/out {1}]", DateTime.UtcNow.ToString(), verboseTirecount));
            }
            else
            {
                activity.Message = string.Format("Participant <strong>submitted</strong> the claim.");
            }

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(claim.ID);
        }

        private void ExecuteCollectorClaimSubmissionRules(Claim claim, bool isAutoApproved)
        {
            //If the collector type is Generator (ON), Auto dismantler, these collectors claim payment will be Zero
            var claimPayments = claimsRepository.LoadClaimPayments(claim.ID);
            var internalAdjust = CheckZeroClaimForCollector(claim.ID, claimPayments);
            if (internalAdjust != null)
            {
                CalculateClaim(claim, claimPayments, new List<ClaimInternalPaymentAdjust> { internalAdjust });
                claimsRepository.AddInternalPaymentAdjustment(internalAdjust);
            }

            //Check if it is auto approve
            if (isAutoApproved && IsAutoApproveCollectorClaim(claim))
            {
                claim.ApprovedBy = SecurityContextHelper.CurrentUser.Id;
                claim.ApprovalDate = DateTime.UtcNow;
                claim.Status = "Approved";
                //Approve all associated transactions
                claimsRepository.ApproveAllAssociatedTransactions(claim.ID);

                //OTSTM2-551 Add Activity for auto approval (<1000)
                AddActivity(claim, true);

                //Fixed the issue if the claim payment summary is already there -- OTSTM2-354
                if (claim.ClaimPaymentSummary == null)
                {
                    //Create ClaimPaymentSummary
                    var claimPaymentSummary = new ClaimPaymentSummary
                    {
                        Claim = claim,
                        CreateDate = DateTime.UtcNow,
                    };
                    claim.ClaimPaymentSummary = claimPaymentSummary;
                }
            }
        }

        private void DoLateClaim(int claimId, bool isAutoApproved, Claim claim)
        {
            var claimPayments = claimsRepository.LoadClaimPayments(claim.ID);
            //Fixed OTSTM2-534 Load system internal adjustment only
            var allClaimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claim.ID).ToList();
            var dbClaimPaymentAdjustments = allClaimPaymentAdjustments.Where(c => c.AdjustBy == AppSettings.Instance.SystemUser.ID).ToList();
            var staffClaimPaymentAdjustments = allClaimPaymentAdjustments.Where(c => c.AdjustBy != AppSettings.Instance.SystemUser.ID).ToList();

            if (!claim.ClaimsAmountTotal.HasValue)
            {
                claim.ClaimsAmountTotal = 0;
            }
            //OTSTM2-210 add late claim flag            
            AddSystemClaimNotes(claim.ID, new List<string> { LateClaimRule.ValidationMessage });

            if (claim.ClaimsAmountTotal > 0)
            {
                var claimPaymentAdjustments = LateClaimSubmitCalculation(claim, claimPayments, dbClaimPaymentAdjustments);

                //Check if it is auto approve for collector
                if (claim.Participant.VendorType == 2 && isAutoApproved && IsAutoApproveCollectorLateClaim(claim))
                {
                    //OTSTM2-534: Add internal payment adjustment after auto approve condition check
                    //Add claim internal adjustment
                    if (claimPaymentAdjustments.Count > 0)
                    {
                        claimPaymentAdjustments.ForEach(c =>
                        {
                            claimsRepository.AddInternalPaymentAdjustment(c);

                            //OTSTM2-738
                            claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                            {
                                ClaimInternalPaymentAdjustId = c.ID,
                                Note = LateClaimRule.ValidationMessage,
                                CreatedDate = DateTime.UtcNow,
                                UserId = AppSettings.Instance.SystemUser.ID
                            });
                        });

                        //Adding staff internal adjustment back for claim summary recalculation
                        claimPaymentAdjustments.AddRange(staffClaimPaymentAdjustments);
                        CalculateClaim(claim, claimPayments, claimPaymentAdjustments);
                    }

                    claim.ApprovedBy = SecurityContextHelper.CurrentUser.Id;
                    claim.ApprovalDate = DateTime.UtcNow;
                    claim.Status = "Approved";
                    //Approve all associated transactions
                    claimsRepository.ApproveAllAssociatedTransactions(claimId);

                    //OTSTM2-551 Add Activity for auto approval (late claim)
                    AddActivity(claim, true);
                }
                else
                {
                    if (claimPaymentAdjustments.Count > 0)
                    {
                        claimPaymentAdjustments.ForEach(c =>
                        {
                            claimsRepository.AddInternalPaymentAdjustment(c);

                            //OTSTM2-738
                            claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                            {
                                ClaimInternalPaymentAdjustId = c.ID,
                                Note = LateClaimRule.ValidationMessage,
                                CreatedDate = DateTime.UtcNow,
                                UserId = AppSettings.Instance.SystemUser.ID
                            });
                        });

                        //Adding staff internal adjustment back for claim summary recalculation
                        claimPaymentAdjustments.AddRange(staffClaimPaymentAdjustments);
                        CalculateClaim(claim, claimPayments, claimPaymentAdjustments);
                    }
                }
            }
            else
            {
                CalculateClaim(claim, claimPayments, allClaimPaymentAdjustments);

                //Check if it is auto approve for collector
                if (claim.Participant.VendorType == 2 && isAutoApproved && IsAutoApproveCollectorLateClaim(claim))
                {
                    claim.ApprovedBy = SecurityContextHelper.CurrentUser.Id;
                    claim.ApprovalDate = DateTime.UtcNow;
                    claim.Status = "Approved";
                    //Approve all associated transactions
                    claimsRepository.ApproveAllAssociatedTransactions(claimId);

                    //OTSTM2-551 Add Activity for auto approval (late claim)
                    AddActivity(claim, true);
                }
            }
        }

        private static void UpdateClaim(Claim claim, bool isPreviousClaimOnHold, bool isPreviousAuditOnHold)
        {
            claim.Status = EnumHelper.GetEnumDescription(ClaimStatus.Submitted);
            claim.SubmissionDate = DateTime.UtcNow;
            claim.SubmittedBy = SecurityContextHelper.CurrentUser.Id;
            var claimReviewDueDays = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Claims.ReviewDueDays"));
            var claimChequeDueDays = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Claims.ChequeDueDays"));
            claim.ReviewDueDate = DateTime.UtcNow.AddDays(claimReviewDueDays);
            claim.ChequeDueDate = DateTime.UtcNow.AddDays(claimChequeDueDays);

            //OTSTM2-499
            if (isPreviousClaimOnHold && (ClaimType)claim.ClaimType != ClaimType.Collector)
            {
                claim.ClaimOnhold = true;
                claim.ClaimOnHoldDate = claim.SubmissionDate;
                claim.ClaimOnHoldBy = AppSettings.Instance.SystemUser.ID;
            }

            if (isPreviousAuditOnHold && (ClaimType)claim.ClaimType != ClaimType.Collector)
            {
                claim.AuditOnhold = true;
                claim.AuditOnHoldDate = claim.SubmissionDate;
                claim.AuditOnHoldBy = AppSettings.Instance.SystemUser.ID;
            }
        }

        private void DoAutoApproveMobileTransaction(Claim claim)
        {
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppDOT") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "DOT");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppHIT") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "HIT");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppPIT") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "PIT");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppPTR") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "PTR");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppSTC") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "STC");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppTCR") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "TCR");
            if (AppSettings.Instance.GetSettingValue("Threshold.CBAutoAppUCR") == "1")
                claimsRepository.ApproveMobileTransactions(claim.ID, "UCR");
        }

        private bool IsAutoApproveCollectorLateClaim(Claim claim)
        {
            //all the transactions within the claim are already ‘Approved’ 
            var isAllTransactionApproved = claimsRepository.AllTransactionIsApproved(claim.ID);

            //if exists internal adjustment, not allow auto approve.
            var hasNoInternalAdjust = !claimsRepository.HasAnyInternalAdjustment(claim.ID);

            //if appSetting for CollectorAutoApprove checked
            var appSettingCollectorAutoApprove = (AppSettings.Instance.GetSettingValue("Threshold.CBCollectorAutoApprove") == "1");

            return appSettingCollectorAutoApprove && isAllTransactionApproved && hasNoInternalAdjust && !(claim.ClaimSupportingDocuments.Any(i => i.SelectedOption == "optionfax" || i.SelectedOption == "optionupload"));
        }
        /// <summary>
        /// Checks if collector is collector type "Generator"; if true add internal adjustment to zero out claim
        /// </summary>
        /// <param name="claimId"></param>
        /// <param name="claimPayments"></param>
        /// <returns></returns>
        private ClaimInternalPaymentAdjust CheckZeroClaimForCollector(int claimId, List<ClaimPayment> claimPayments)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            if (claim.Participant.PrimaryBusinessActivity == "Generator")
            {
                //Add one internal payment adjustment
                //OTSTM2-961 for System generated Internal Payment adjustments only account for PLT amount and not other item types when added.
                var paymentTypes = claimPayments.Select(x => x.PaymentType).Distinct().ToList();
                decimal adjustmentAmount = 0;
                foreach (var paymentType in paymentTypes)
                {
                    adjustmentAmount += Math.Round(claimPayments.Where(c => c.PaymentType == paymentType).Sum(c => c.Rate * c.Weight), 3, MidpointRounding.AwayFromZero);
                }
                if (adjustmentAmount > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - adjustmentAmount;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.Overall;
                    return claimInternalPaymentAdjustment;
                }
            }
            return null;
        }

        private bool IsAutoApproveCollectorClaim(Claim claim)
        {
            var claimAmount = claim.ClaimsAmountTotal != null ? (decimal)claim.ClaimsAmountTotal : 0;
            var autoApproveAmount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.CollectorAutoApproveAmount"));
            var isLowerForAutoApprove = (AppSettings.Instance.GetSettingValue("Threshold.CBCollectorAutoApprove") == "1") && ((claimAmount <= autoApproveAmount) && (claimAmount >= 0));
            var noExceptionForAutoApprove = CheckNoExceptionForAutoApprove(claim);
            //commenting out and moving code to CheckNoExceptionForAutoApprove method
            //var isValidTireOriginReport = IsValidTireOriginReport(claim);
            var isAutoDismantler = IsAutoDismantler(claim);
            return isLowerForAutoApprove && noExceptionForAutoApprove && !isAutoDismantler && !(claim.ClaimSupportingDocuments.Any(i => i.SelectedOption == "optionfax" || i.SelectedOption == "optionupload"));
        }

        //private bool IsValidTireOriginReport(Claim claim)
        //{
        //    //OTSTM2-62 set claim to draft if tire origin + tire types reported match algorithm below
        //    var tireOrigins = claimsRepository.LoadClaimTireOrigins(claim.ID);
        //    var allTireOrigins = AppDefinitions.Instance.TypeDefinitions["TireOrigin"].ToList();
        //    var arr = new int[] { 1, 2, 3, 4, 5, 6, 9, 10 };
        //    var tireOriginsValidations = allTireOrigins.Where(i => Array.IndexOf(arr, i.DefinitionValue) > -1).ToList();
        //    var result = true;
        //    foreach (var tireOrigin in tireOriginsValidations)
        //    {
        //        //Tires removed from purchased vehicle/equipment recycling (i.e Auto Dismantlers)
        //        if (tireOrigin.DefinitionValue == 9)
        //        {
        //            var invalidAutoDismanterTireTypes = new string[] { "MT", "IND", "SOTR", "MOTR", "LOTR", "GOTR" };
        //            if (tireOrigins.Any(i => Array.IndexOf(invalidAutoDismanterTireTypes, i.Item.ShortName) > -1 && i.TireOriginValue == 9))
        //            {
        //                result = false;
        //                break;
        //            }
        //        }
        //        else if (tireOrigins.Any(i => i.TireOriginValue == tireOrigin.DefinitionValue && i.Quantity > 0))
        //        {
        //            result = false;
        //            break;
        //        }
        //    }
        //    if (!result)
        //    {
        //        AddSystemClaimNotes(claim.ID, new List<string> { "Please review tire origin report." });
        //    }
        //    return result;
        //}

        private bool IsAutoDismantler(Claim claim)
        {
            //OTSTM2-32 if collector is auto dismantler automatically put claim in draft
            var isAutoDismantler = string.Equals(claim.Participant.PrimaryBusinessActivity, "Auto dismantler", StringComparison.InvariantCultureIgnoreCase);
            if (isAutoDismantler)
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "Participant is an auto dismantler.  Claim will go into draft status." });
            }
            return isAutoDismantler;
        }

        private bool CheckNoExceptionForAutoApprove(Claim claim)
        {
            var result = true;
            //1. If it is first time ever the collector submits a claim
            if (claimsRepository.IsFirstClaim(claim.ParticipantId, claim.ID))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "It is first time ever the collector submits a claim." });
                return false;
            }
            //2. If collector is not active
            if (!claim.Participant.IsActive)
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "The collector is not active." });
                return false;
            }
            //3. If collector has non-finalized previous claim submission
            if (!claimsRepository.IsPreviousClaimApproved(claim.ParticipantId, claim.ClaimPeriod.StartDate))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "The collector has non-finalized previous claim submission." });
                return false;
            }
            //4. If previous claim is On-Hold
            if (claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "Previous claim is On-Hold." });
                return false;
            }

            //4.1 If previous claim is On-Hold
            if (claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "Previous claim is On-Hold for Audit." });
                return false;
            }

            //5.  If claim has any ‘Unreviewed status’ transactions (mobile or paper)  paper form then the claim will go into draft status
            if (claimsRepository.HasUnreviewedTransactions(claim.ID))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "If claim has any ‘Unreviewed status’ transactions (mobile or paper)  paper form then the claim will go into draft status." });
                return false;
            }

            //7. Collector claim will come into draft if the tire types collected does not match the tire types they reported to be collected in the application (based on their Collector application record).
            var vendorItems = vendorRepository.GetVendorItems(claim.ParticipantId);
            var transactionItemShortNames = claimsRepository.GetTransactionItemShortName(claim.ID);
            var notMatched = false;
            foreach (var shortName in transactionItemShortNames)
            {
                if (!vendorItems.Any(c => c.ShortName == shortName))
                {
                    notMatched = true;
                    break;
                }
            }

            if (notMatched)
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "Collector claim will come into draft if the tire types collected does not match the tire types they reported to be collected in the application (based on their Collector application record)." });
                return false;
            }

            //7.1- 7.2 If Tire Origin report has any of the following selected, the claim will come into draft with internal warning message
            if (!claimsRepository.TireOriginCheckForAutoApproval(claim.ID))
            {
                var sb = new StringBuilder();
                sb.AppendLine("If Tire Origin report has any of the following selected, the claim will come into draft with internal warning message.");

                sb.AppendLine("1. Tires are Generated.");
                sb.AppendLine("2. Tires are from an unknown source dropped off at my business.");
                sb.AppendLine("3. Tires were collected from my approved Sub-Collector(s)");
                sb.AppendLine("4. Tires dropped  off by a consumer/ resident or farmer.");
                sb.AppendLine("5. Tires picked up from another business.");
                sb.AppendLine("6. Tires dropped off by another business.");
                sb.AppendLine("7. Tires removed from your owned or leased vehicles or equipment.");
                sb.AppendLine("8. Tires removed from a purchased vehicle / equipment recycling(i.e Auto Dismantlers). for tire types MT, IND, SOTR, MOTR, LOTR, GOTR");
                sb.AppendLine("9. Hosted collection event");

                AddSystemClaimNotes(claim.ID, new List<string> { sb.ToString() });
                return false;
            }
            //8. if claim has any internal adjustment added by staff, if yes , place it on "Submitted" status upon submission (OTSTM2-354)
            if (claimsRepository.HasAnyInternalAdjustment(claim.ID))
            {
                AddSystemClaimNotes(claim.ID, new List<string> { "Collector claim will come into draft if it has any internal adjustments" });
                return false;
            }
            return result;
        }

        private List<ClaimInternalPaymentAdjust> LateClaimSubmitCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            var claimInternalPaymentAdjustments = new List<ClaimInternalPaymentAdjust>();
            //OTSTM2-961 for System generated Internal Payment adjustments only account for PLT amount and not other item types when added.
            List<int> paymentTypes = claimPayments.Select(x => x.PaymentType).Distinct().ToList();
            //Collector claim
            if (claim.Participant.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, TreadMarksConstants.Collector).DefinitionValue)
            {
                //Add one internal payment adjustment
                foreach (var paymentType in paymentTypes)
                {
                    var amount = claimPayments.Where(c => c.PaymentType == paymentType).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == paymentType).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var adjustment = Math.Round(amount + adjustmentAmount, 3, MidpointRounding.AwayFromZero);
                    if (adjustment > 0)
                    {
                        var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                        claimInternalPaymentAdjustment.ClaimId = claim.ID;
                        claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                        claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                        claimInternalPaymentAdjustment.AdjustmentAmount = 0 - adjustment;
                        claimInternalPaymentAdjustment.PaymentType = paymentType;
                        claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                    }
                }
            }
            //Hauler claim
            if (claim.Participant.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, TreadMarksConstants.Hauler).DefinitionValue)
            {
                //Do internal payment adjustment
                var northernPremiumAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.NorthernPremium).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var dotPremiumAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOTPremium).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var stcPremiumAmount = Math.Round(claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.STCPremium).Sum(c => c.Rate * c.Weight), 3, MidpointRounding.AwayFromZero);

                var northernPremiumAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.NorthernPremium).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                var dotPremiumAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOTPremium).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                var stcPremiumAdjustmentAmount = claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.STCPremium).Sum(c => c.AdjustmentAmount);

                var northernPremiumAdjustment = Math.Round(northernPremiumAmount + northernPremiumAdjustmentAmount, 3, MidpointRounding.AwayFromZero);
                var dotPremiumAdjustment = Math.Round(dotPremiumAmount + dotPremiumAdjustmentAmount, 3, MidpointRounding.AwayFromZero);
                var stcPremiumAdjustment = Math.Round(stcPremiumAmount + stcPremiumAdjustmentAmount, 3, MidpointRounding.AwayFromZero);

                if (northernPremiumAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - northernPremiumAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.NorthernPremium;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }
                if (dotPremiumAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - dotPremiumAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.DOTPremium;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }
                if (stcPremiumAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - stcPremiumAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.STCPremium;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }
            }
            //Processor claim
            if (claim.Participant.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, TreadMarksConstants.Processor).DefinitionValue)
            {
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var pitAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));

                var ptrAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                var spsAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                var pitAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

                var ptrAdjustment = Math.Round(ptrAmount + ptrAdjustmentAmount, 3, MidpointRounding.AwayFromZero);
                var spsAdjustment = Math.Round(spsAmount + spsAdjustmentAmount, 3, MidpointRounding.AwayFromZero);
                var pitAdjustment = Math.Round(pitAmount + pitAdjustmentAmount, 3, MidpointRounding.AwayFromZero);


                if (ptrAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - ptrAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.PTR;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }
                if (spsAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - spsAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.SPS;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }
                if (pitAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - pitAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.PITOutbound;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }

            }
            //RPM claim
            if (claim.Participant.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, TreadMarksConstants.RPM).DefinitionValue)
            {
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.RPMSPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.RPMSPS).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                var spsAdjustment = spsAmount + spsAdjustmentAmount;

                if (spsAdjustment > 0)
                {
                    var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                    claimInternalPaymentAdjustment.ClaimId = claim.ID;
                    claimInternalPaymentAdjustment.AdjustBy = AppSettings.Instance.SystemUser.ID;
                    claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInternalPaymentAdjustment.AdjustmentAmount = 0 - spsAdjustment;
                    claimInternalPaymentAdjustment.PaymentType = (int)ClaimPaymentType.RPMSPS;
                    claimInternalPaymentAdjustments.Add(claimInternalPaymentAdjustment);
                }

            }

            return claimInternalPaymentAdjustments;
        }
        private void CalculateClaim(Claim claim, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            var taxRate = 0.13m;
            if (claim.ClaimType == (int)ClaimType.Collector) //2
            {
                if (claim.Participant.IsTaxExempt)
                {
                    //No HST
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            if (claim.ClaimType == (int)ClaimType.Hauler) //3)
            {
                if (claim.Participant.IsTaxExempt)
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var paymentAmount = paymentAmountPositive - paymentAmountNegative;
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var noTaxAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var taxAdjustment = adjustmentAmount - noTaxAdjustmentAmount;
                    var taxAmount = Math.Round((paymentAmountPositive + taxAdjustment) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmountPositive - paymentAmountNegative + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            if (claim.ClaimType == (int)ClaimType.Processor)//4)
            {
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var pitAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var dorAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var paymentAmount = ptrAmount + spsAmount + pitAmount + dorAmount;
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
            }

            if (claim.ClaimType == (int)ClaimType.RPM) //5)
            {
                var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
            }
        }

        public StaffAllClaimsModel FilterClaimByType(int userId)
        {
            return claimsRepository.GetTopClaimForMe(userId);
        }

        public Claim GetClaimForTransaction(int vendorId, DateTime transactionDate, bool forStaff)
        {
            var claim = claimsRepository.GetClaimForTransaction(vendorId, transactionDate, forStaff);

            return claim;
        }

        //OTSTM2-1313
        public Claim GetClaimForDORTransaction(int transactionId)
        {
            var claim = claimsRepository.GetClaimForDORTransaction(transactionId);

            return claim;
        }
        public Claim GetCurrentClaim(int vendorId, DateTime transactionDate)
        {
            return claimsRepository.GetCurrentClaim(vendorId, transactionDate);
        }
        public Claim GetNextAvailableOpenClaimForTransaction(int vendorId, DateTime transactionDate, bool forStaff)
        {
            var claim = claimsRepository.GetNextAvailableOpenClaimForTransaction(vendorId, transactionDate, forStaff);

            return claim;
        }

        public void AddClaimDetails(ClaimDetail claimDetail)
        {
            claimsRepository.AddClaimDetail(claimDetail);
        }

        //OTSTM2-81
        public void AddClaimDetailsRange(List<ClaimDetail> claimDetailList)
        {
            claimsRepository.AddClaimDetailRange(claimDetailList);
        }

        public Period GetClaimPeriod(int claimId)
        {
            return claimsRepository.GetClaimPeriod(claimId);
        }

        public string GetClaimPeriodShortName(Func<int, Period> val, int claimId)
        {
            var period = val(claimId);
            return period.ShortName;
        }

        public IList GetClaimPeriodForBreadCrumb(int vendorId)
        {
            return this.claimsRepository.GetClaimPeriodForBreadCrumb(vendorId);
        }

        public bool AddInventoryAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            //id=1 weight -- claiminventoryadjustment
            //id=2, 3 tirecounts or yardcount -- claiminventoryadjustment and claiminventoryadjustitem
            //id=4 payment claiminternalpaymentadjust

            if (modalResult.SelectedItem.Id == 1)
            {
                AddInventoryAdjustmentForWeight(claimId, modalResult);
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
                return true;
            }
            if ((modalResult.SelectedItem.Id == 2) || (modalResult.SelectedItem.Id == 3))
            {
                AddInventoryAdjustmentForTireYardCount(claimId, modalResult);
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
                return true;
            }
            if (modalResult.SelectedItem.Id == 4)
            {
                AddInternalPaymentAdjustment(claimId, modalResult);

                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
                return true;
            }
            return false;
        }

        public void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            AddActivity(claimId, adjustmentType, internalAdjustmentId, "removed");
            claimsRepository.RemoveInternalAdjustment(adjustmentType, internalAdjustmentId);
            var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
            eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            //ClaimCalculationHelper.CalculateClaim(claimId);
        }

        private void AddActivity(int claimId, int adjustmentType, int internalAdjustmentId, string activeType = "", string toValue = "")
        {
            var activity = new Activity();
            activity.Initiator = SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = string.Empty;
            activity.AssigneeName = string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.ClaimActivity;
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            GetActivityMessage(claim.ClaimType, adjustmentType, internalAdjustmentId, activity, activeType, toValue);

            activity.ObjectId = claim.ID;

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(claim.ID);
        }

        private void GetActivityMessage(int ClaimType, int adjustmentType, int internalAdjustmentId, Activity activity, string activeType = "", string toValue = "")
        {
            string strActivityMessage = "";
            switch (ClaimType)
            {
                case 2:
                    activity.ActivityArea = TreadMarksConstants.Collector;
                    break;
                case 3:
                    activity.ActivityArea = TreadMarksConstants.Hauler;
                    break;
                case 4:
                    activity.ActivityArea = TreadMarksConstants.Processor;
                    break;
                case 5:
                    activity.ActivityArea = TreadMarksConstants.RPM;
                    break;
            }

            //Payment Adjustment
            if (adjustmentType == 4)
            {
                var internalAdjustment = claimsRepository.GetClaimInternalPaymentAdjust(internalAdjustmentId);
                switch (activeType)
                {
                    case "added":
                        strActivityMessage = string.Format("{0} <strong>added</strong> adjustment for <strong>{1} ({2})</strong> of <strong>{3}</strong>.",
                           SecurityContextHelper.CurrentUser.FullName,
                           "Payment",
                           EnumHelper.GetEnumDescription((ClaimPaymentType)internalAdjustment.PaymentType).ToString(),
                           string.Format("{0:$#,##0.00}", Math.Round(internalAdjustment.AdjustmentAmount, 2, MidpointRounding.AwayFromZero)));
                        break;
                    case "updated":
                        strActivityMessage = string.Format("{0} <strong>updated</strong> {1}'s adjustment for <strong>{2} ({3})</strong> from <strong>{4}</strong> to <strong>{5}</strong>.",
                           SecurityContextHelper.CurrentUser.FullName,
                           internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                           "Payment",
                           EnumHelper.GetEnumDescription((ClaimPaymentType)internalAdjustment.PaymentType).ToString(),
                           string.Format("{0:$#,##0.00}", Math.Round(internalAdjustment.AdjustmentAmount, 2, MidpointRounding.AwayFromZero)),
                           toValue
                           );
                        break;
                    case "removed":
                        strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of <strong>{4}</strong>.",
                           SecurityContextHelper.CurrentUser.FullName,
                           internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                           "Payment",
                           EnumHelper.GetEnumDescription((ClaimPaymentType)internalAdjustment.PaymentType).ToString(),
                           string.Format("{0:$#,##0.00}", Math.Round(internalAdjustment.AdjustmentAmount, 2, MidpointRounding.AwayFromZero)));
                        break;
                }
            }
            else
            {
                var internalAdjustment = claimsRepository.GetClaimInventoryAdjustment(internalAdjustmentId);
                List<string> listTires = new List<string>();
                if (internalAdjustment.ClaimInventoryAdjustItems.Count > 0)
                {
                    internalAdjustment.ClaimInventoryAdjustItems.ToList().ForEach(c =>
                    {
                        var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == c.ItemId);
                        listTires.Add(" " + c.QtyAdjustment.ToString() + " " + item.ShortName);
                    });
                }
                string strTires = String.Join(",", listTires.ToArray());
                string strDirection = "";

                switch (internalAdjustment.Direction)
                {
                    case 0:
                        strDirection = "Overall";
                        break;
                    case 1:
                        strDirection = "Inbound";
                        break;
                    case 2:
                        strDirection = "Outbound";
                        break;
                    default:
                        strDirection = "Overall";
                        break;
                }

                switch (ClaimType)
                {
                    case 2:
                        strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of<strong>{4}</strong>.",
                            SecurityContextHelper.CurrentUser.FullName,
                            internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                            EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                            internalAdjustment.IsEligible ? "Eligible" : "Ineligible",
                            strTires);
                        break;

                    case 3:
                        switch (internalAdjustment.AdjustmentType)
                        {
                            case ((int)InternalAdjustmentType.TireCount):
                                {
                                    strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3}, {4})</strong> of<strong>{5}</strong>.",
                                        SecurityContextHelper.CurrentUser.FullName,
                                        internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                                        EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                                        strDirection,
                                        internalAdjustment.IsEligible ? "Eligible" : "Ineligible",
                                        strTires);

                                }
                                break;
                            case ((int)InternalAdjustmentType.Weight):
                                {
                                    strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3}, {4})</strong> of <strong>{5} Kg (On-road), {6} Kg (Off-road)</strong>.",
                                        SecurityContextHelper.CurrentUser.FullName,
                                        internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                                        EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                                        strDirection,
                                        internalAdjustment.IsEligible ? "Eligible" : "Ineligible",
                                        GetUIWeight(internalAdjustment.UnitType, internalAdjustment.AdjustmentWeightOnroad),
                                        GetUIWeight(internalAdjustment.UnitType, internalAdjustment.AdjustmentWeightOffroad));
                                }
                                break;
                            case ((int)InternalAdjustmentType.YardCount):
                                {
                                    strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of<strong>{4}</strong>.",
                                        SecurityContextHelper.CurrentUser.FullName,
                                        internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                                        EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                                        internalAdjustment.IsEligible ? "Eligible" : "Ineligible",
                                        strTires);
                                }
                                break;
                        }
                        break;
                    case 4:
                    case 5:
                        switch (internalAdjustment.AdjustmentType)
                        {
                            case ((int)InternalAdjustmentType.Weight):
                                {
                                    strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of <strong>{4} Tonnes</strong>.",
                                        SecurityContextHelper.CurrentUser.FullName,
                                        internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                                        EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                                        strDirection,
                                        GetUIWeight(internalAdjustment.UnitType, internalAdjustment.AdjustmentWeightOnroad));
                                }
                                break;
                            //not applicable to case 5 RPM but this
                            //condition would never meet
                            case ((int)InternalAdjustmentType.TireCount):
                                {
                                    strActivityMessage = string.Format("{0} <strong>removed</strong> {1}'s adjustment for <strong>{2} ({3})</strong> of<strong>{4}</strong>.",
                                        SecurityContextHelper.CurrentUser.FullName,
                                        internalAdjustment.AdjustUser.FirstName + " " + internalAdjustment.AdjustUser.LastName,
                                        EnumHelper.GetEnumDescription(((InternalAdjustmentType)internalAdjustment.AdjustmentType)).ToString(),
                                        strDirection,
                                        strTires);

                                }
                                break;
                        }
                        break;
                }
            }
            activity.Message = strActivityMessage;
        }

        public ClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId)
        {
            var result = new ClaimAdjustmentModalResult();
            if (internalAdjustType == 4)
            {
                var claimInternalPaymentAdjust = claimsRepository.GetClaimInternalPaymentAdjust(internalAdjustId);
                result.SelectedItem.Id = 4;
                result.SelectedItem.Name = "Payment";
                result.PaymentType = ((ClaimPaymentType)claimInternalPaymentAdjust.PaymentType).ToString();
                result.Amount = Math.Round(claimInternalPaymentAdjust.AdjustmentAmount, 2, MidpointRounding.AwayFromZero);
                result.AdjustmentId = claimInternalPaymentAdjust.ID;
                result.IsAdd = false;
            }
            else
            {
                var claimInventoryAdjust = claimsRepository.GetClaimInventoryAdjustment(internalAdjustId);
                result.AdjustmentId = claimInventoryAdjust.ID;
                result.IsAdd = false;
                result.SelectedItem.Id = claimInventoryAdjust.AdjustmentType;
                result.Eligibility = claimInventoryAdjust.IsEligible ? "eligible" : "ineligible";
                result.UnitType = claimInventoryAdjust.UnitType;
                switch (claimInventoryAdjust.Direction)
                {
                    case 0:
                        result.Direction = "overall";
                        break;
                    case 1:
                        result.Direction = "inbound";
                        break;
                    case 2:
                        result.Direction = "outbound";
                        break;
                    default:
                        result.Direction = "overall";
                        break;
                }
                result.Onroad = GetUIWeight(claimInventoryAdjust.UnitType, claimInventoryAdjust.AdjustmentWeightOnroad);
                result.Offroad = GetUIWeight(claimInventoryAdjust.UnitType, claimInventoryAdjust.AdjustmentWeightOffroad);
                if (claimInventoryAdjust.ClaimInventoryAdjustItems.Count > 0)
                {
                    claimInventoryAdjust.ClaimInventoryAdjustItems.ToList().ForEach(c =>
                    {
                        var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == c.ItemId);
                        switch (item.ShortName)
                        {
                            case TreadMarksConstants.PLT:
                                result.PLT = c.QtyAdjustment;
                                //OTSTM2-489 changes for yard count internal
                                //adjustment
                                result.PLTBeforeAdj = c.QtyBeforeAdjustment;
                                result.PLTAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.MT:
                                result.MT = c.QtyAdjustment;
                                result.MTBeforeAdj = c.QtyBeforeAdjustment;
                                result.MTAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.AGLS:
                                result.AGLS = c.QtyAdjustment;
                                result.AGLSBeforeAdj = c.QtyBeforeAdjustment;
                                result.AGLSAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.IND:
                                result.IND = c.QtyAdjustment;
                                result.INDBeforeAdj = c.QtyBeforeAdjustment;
                                result.INDAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.SOTR:
                                result.SOTR = c.QtyAdjustment;
                                result.SOTRBeforeAdj = c.QtyBeforeAdjustment;
                                result.SOTRAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.MOTR:
                                result.MOTR = c.QtyAdjustment;
                                result.MOTRBeforeAdj = c.QtyBeforeAdjustment;
                                result.MOTRAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.LOTR:
                                result.LOTR = c.QtyAdjustment;
                                result.LOTRBeforeAdj = c.QtyBeforeAdjustment;
                                result.LOTRAfterAdj = c.QtyAfterAdjustment;
                                break;
                            case TreadMarksConstants.GOTR:
                                result.GOTR = c.QtyAdjustment;
                                result.GOTRBeforeAdj = c.QtyBeforeAdjustment;
                                result.GOTRAfterAdj = c.QtyAfterAdjustment;
                                break;
                        }
                    });
                }
            }
            return result;
        }

        public void EditInternalAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            if (modalResult.SelectedItem.Id == 4)
            {
                ////update claim payment adjustment
                //claimsRepository.UpdateInternalPaymentAdjust(modalResult.AdjustmentId, modalResult, SecurityContextHelper.CurrentUser.Id);

                //OTSTM2-270
                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    string toValue = string.Format("{0:$#,##0.00}", Math.Round(modalResult.Amount, 2, MidpointRounding.AwayFromZero));
                    AddActivity(claimId, modalResult.SelectedItem.Id, modalResult.AdjustmentId, "updated", toValue);

                    claimsRepository.UpdateInternalPaymentAdjust(modalResult.AdjustmentId, modalResult, SecurityContextHelper.CurrentUser.Id);

                    claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                    {
                        ClaimInternalPaymentAdjustId = modalResult.AdjustmentId,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });

                    transationScope.Complete();
                }
            }
            else
            {
                //Updated claim inventory adjustment
                var claimInventoryAdjustment = new ClaimInventoryAdjustment();
                if (modalResult.SelectedItem.Id == 1)
                {
                    claimInventoryAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
                    claimInventoryAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInventoryAdjustment.IsEligible = modalResult.IsEligible;
                    claimInventoryAdjustment.AdjustmentType = modalResult.SelectedItem.Id;
                    claimInventoryAdjustment.Direction = modalResult.DirectionValue;
                    claimInventoryAdjustment.AdjustmentWeightOnroad = GetDBWeight(modalResult.UnitType, modalResult.Onroad);
                    claimInventoryAdjustment.AdjustmentWeightOffroad = GetDBWeight(modalResult.UnitType, modalResult.Offroad);
                }
                else
                {
                    var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
                    claimInventoryAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
                    claimInventoryAdjustment.AdjustDate = DateTime.UtcNow;
                    claimInventoryAdjustment.IsEligible = modalResult.IsEligible;
                    claimInventoryAdjustment.AdjustmentType = modalResult.SelectedItem.Id;
                    claimInventoryAdjustment.Direction = modalResult.DirectionValue;
                    claimInventoryAdjustment.AdjustmentWeightOnroad = 0;
                    claimInventoryAdjustment.AdjustmentWeightOffroad = 0;
                    PopulateInventoryAdjustItems(modalResult, claimPeriod, claimInventoryAdjustment);
                }
                //claimsRepository.UpdateInventoryPaymentAdjust(modalResult.AdjustmentId, claimInventoryAdjustment);

                //OTSTM2-270
                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    claimsRepository.UpdateInventoryPaymentAdjust(modalResult.AdjustmentId, claimInventoryAdjustment);

                    claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                    {
                        ClaimInternalAdjustmentId = modalResult.AdjustmentId,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });

                    transationScope.Complete();
                }
            }
            var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
            eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            //ClaimCalculationHelper.CalculateClaim(claimId);
        }

        public DateTime? UpdateClaimOnhold(int claimId, bool isOnhold)
        {
            var userId = SecurityContextHelper.CurrentUser.Id;
            var systemUserId = AppSettings.Instance.SystemUser.ID;
            return claimsRepository.UpdateClaimOnhold(claimId, isOnhold, userId, systemUserId);
        }

        public DateTime? UpdateAuditOnhold(int claimId, bool isOnhold)
        {
            var userId = SecurityContextHelper.CurrentUser.Id;
            var systemUserId = AppSettings.Instance.SystemUser.ID;
            return claimsRepository.UpdateAuditOnhold(claimId, isOnhold, userId, systemUserId);
        }

        public string GetPrimaryContactByClaimId(int claimId)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var primaryContact = Repository.RepositoryHelper.FindPrimaryContact(claim.Participant.VendorAddresses.ToList());
            return primaryContact != null ? primaryContact.Email : "";
        }

        public ClaimWorkflowViewModel LoadClaimWorkflowViewModel(int claimId, decimal? claimsAmountTotal = null)
        {
            var claimWorkflowViewModel = new ClaimWorkflowViewModel();
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            claimWorkflowViewModel.ClaimsAmountTotal = claimsAmountTotal != null ? claimsAmountTotal : claim.ClaimsAmountTotal;

            int approver1Amount = 0;
            bool isApprover1Reqd = false;
            int approver2Amount = 0;
            bool isApprover2Reqd = false;
            string negativeClaimApprover = string.Empty;// "Approver1" || "Approver2";
            bool isNegativeClaimReqd = false;

            switch (claim.ClaimType)
            {
                case 2:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.CollectorApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.CollectorApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CollectorNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorNegativeClaimReqd").Value == "1";
                    break;
                case 3:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.HaulerApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.HaulerApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.HaulerNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerNegativeClaimReqd").Value == "1";
                    break;
                case 4:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.ProcessorApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.ProcessorApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.ProcessorNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorNegativeClaimReqd").Value == "1";
                    break;
                case 5:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.RPMApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.RPMApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.RPMNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMNegativeClaimReqd").Value == "1";
                    break;
            }

            //Set up claim progress bar
            SetupClaimProgressbar(claimWorkflowViewModel, approver1Amount, approver2Amount, isApprover1Reqd, isApprover2Reqd, isNegativeClaimReqd, negativeClaimApprover);

            //If claim is approved
            if (claim.Status == "Approved" || claim.Status == "Receive Payment")
            {
                SetupWorkflowViewModelForApprovedClaim(claimWorkflowViewModel, claimId);
            }
            else
            {
                //Check claim process status
                ClaimProcess claimProcess = claimsRepository.GetClaimProcess(claimId);
                SetupClaimWorkflowView(claimWorkflowViewModel, claim, approver1Amount, approver2Amount, claimProcess, isApprover1Reqd, isApprover2Reqd, isNegativeClaimReqd, negativeClaimApprover);

                //Set workflow button disable if there is no associated user defined in the system
                VerifyWorkflowButtons(claimWorkflowViewModel);

                //Disable approve button if the previous claim is not approved 
                //1.You cannot click the approve button to finalize the claim UNTIL either the previous claim has a GP Batch number that is POSTED or previous claim is a zero dollar claim and previous claim status is Approved
                //2.First claim: April 2016 claim is the first claim, allow April claims to be approved as is
                var claimPeriodStartDate = claim.ClaimPeriod.StartDate;
                var vendorId = claim.ParticipantId;
                var isReadyForApproval = claimsRepository.IsReadyForApproval(vendorId, claimPeriodStartDate);
                if (!isReadyForApproval)
                {
                    //disable approve button
                    claimWorkflowViewModel.IsApproverBtnDisable = true;
                }
            }
            return claimWorkflowViewModel;
        }

        private void SetupWorkflowViewModelForApprovedClaim(ClaimWorkflowViewModel claimWorkflowViewModel, int claimId)
        {
            ClaimProcess claimProcess = claimsRepository.GetClaimProcess(claimId);
            if (claimProcess != null)
            {
                if (claimProcess.Approver2 != null && claimProcess.Approver2Date != null)
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = true;
                    claimWorkflowViewModel.IsApprover1BtnVisible = false;
                    claimWorkflowViewModel.IsApproverBtnVisible = false;
                }
                else if (claimProcess.Approver1 != null && claimProcess.Approver1Date != null)
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = false;
                    claimWorkflowViewModel.IsApprover1BtnVisible = true;
                    claimWorkflowViewModel.IsApproverBtnVisible = false;
                }
                else
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = false;
                    claimWorkflowViewModel.IsApprover1BtnVisible = false;
                    claimWorkflowViewModel.IsApproverBtnVisible = true;
                }
            }

            claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.ApprovePay;
            claimWorkflowViewModel.RepresentativeView = false;
            claimWorkflowViewModel.LeadView = false;
            claimWorkflowViewModel.SupervisorView = false;
            claimWorkflowViewModel.SupervisorWithoutApproverView = false;
            claimWorkflowViewModel.Approver1View = false;
            claimWorkflowViewModel.Approver2View = false;
        }

        private void VerifyWorkflowButtons(ClaimWorkflowViewModel claimWorkflowViewModel)
        {
            var allClaimUsers = userRepository.LoadAllClaimWorkflowUsers();
            var hasRepresentativeUsers = allClaimUsers.Any(c => c.Workflow == TreadMarksConstants.RepresentativeWorkflow);
            var hasLeadUsers = allClaimUsers.Any(c => c.Workflow == TreadMarksConstants.LeadWorkflow);
            var hasSupervisorUsers = allClaimUsers.Any(c => c.Workflow == TreadMarksConstants.SupervisorWorkflow);
            var hasApprover1Users = allClaimUsers.Any(c => c.Workflow == TreadMarksConstants.Approver1Workflow);
            var hasApprover2Users = allClaimUsers.Any(c => c.Workflow == TreadMarksConstants.Approver2Workflow);
            claimWorkflowViewModel.IsRepresentativeBtnDisable = !hasRepresentativeUsers;
            claimWorkflowViewModel.IsLeadBtnDisable = !hasLeadUsers;
            claimWorkflowViewModel.IsSupervisorBtnDisable = !hasSupervisorUsers;
            claimWorkflowViewModel.IsApprover1BtnDisable = !hasApprover1Users;
            claimWorkflowViewModel.IsApprover2BtnDisable = !hasApprover2Users;
        }

        public string ValidateWorkflowProcess(int claimId)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var isAllTransactionIsReviewed = claimsRepository.AllTransactionIsReviewed(claim.ID);
            if (!isAllTransactionIsReviewed)
            {
                return "Claim cannot be sent further for review. Please verify that all the transactions have been reviewed.";
            }
            if (claimsRepository.HasPendingTransactionAndAdjust(claimId)) //OTSTM2-403
            {
                return "Claim cannot be sent further for review. Please verify pending transaction adjustments.";
            }
            if ((claim.ClaimOnhold != null) && ((bool)claim.ClaimOnhold))
            {
                return "Workflow cannot be moved back or forward until the claim is taken off hold. Please refer to claim period(s) on hold.";
            }

            if ((claim.AuditOnhold != null) && ((bool)claim.AuditOnhold))
            {
                return "Workflow cannot be moved back or forward until the claim is taken off hold for Audit. Please refer to claim period(s) on hold.";
            }

            if (!claimsRepository.IsPreviousClaimApproved(claim.ParticipantId, claim.ClaimPeriod.StartDate) || !claimsRepository.IsPreviousClaimPosted(claim.ParticipantId, claim.ClaimPeriod.StartDate))
            {
                return "You cannot move this claim forward in the review process because this registrant’s previous claim has not yet been approved or posted";
            }

            return string.Empty;
        }
        private static void SetupClaimWorkflowView(ClaimWorkflowViewModel claimWorkflowViewModel, Claim claim, int approver1Amount, int approver2Amount, ClaimProcess claimProcess,
            bool isApprover1Reqd, bool isApprover2Reqd, bool isNegativeClaimReqd, string negativeClaimApprover)
        {
            string sSendBackMsg = "Please send the Claim back because it's value requires a lower level of approval.";
            if (isNegativeClaimReqd && claimWorkflowViewModel.ClaimsAmountTotal < 0)
            {
                switch (negativeClaimApprover)
                {
                    case "Approver2":
                        claimWorkflowViewModel.ClaimsAmountTotal = approver2Amount + 1;
                        isApprover1Reqd = true;//(isApprover1Reqd == false && isApprover2Reqd == true) is illegal status
                        isApprover2Reqd = true;
                        break;
                    case "Approver1":
                        claimWorkflowViewModel.ClaimsAmountTotal = approver1Amount + 0.1M;//a value less then approver1Amount + 1 
                        isApprover1Reqd = true;
                        break;
                    default:
                        break;
                }
            }

            if (claimProcess == null)
            {
                //claim is open and assigned, representative review in-progress
                if (claim.ReviewedBy != null)
                    claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Representative;
                else
                    claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.None;
            }
            else
            {
                if (claim.Status == ClaimStatus.Approved.ToString())
                {
                    claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.ApprovePay;
                }
                else
                {
                    if (claimProcess.RepresentativeReview != null)
                    {
                        claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Lead;
                    }
                    else
                    {
                        claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Representative;
                    }

                    if (claimProcess.LeadReview != null)
                    {
                        claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Supervisor;
                    }

                    if (claimProcess.SupervisorReview != null)
                    {
                        if (isApprover1Reqd)
                        {
                            claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Approver1;
                            if (claimWorkflowViewModel.ClaimsAmountTotal >= approver1Amount)
                            {
                                claimWorkflowViewModel.SendBackInfo = string.Empty;
                            }
                            else
                            {
                                ///"Please send the Claim back because it's value requires a lower level of approval."
                                ///caused by change of EIGHER Threshold.XXXApprover1ReqdAmount(raise) OR claim.ClaimsAmountTotal(decrease)
                                claimWorkflowViewModel.SendBackInfo = sSendBackMsg;
                            }
                        }
                        else//not required Approver 1 to review (check box not checked); then Approver 2's review is NOT required NEIGHER
                        {
                            ///Supervisor already reviewed and not require Approver 1's review, 
                            ///means this claim is already passed final review(approved) OR after Supervisor's review, amdin user unchecked Require 'Approver 1' approval 
                            if (claim.Status == ClaimStatus.Approved.ToString())
                            {
                                claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.ApprovePay;
                            }
                            else
                            {
                                claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Approver1;
                                claimWorkflowViewModel.SendBackInfo = sSendBackMsg;
                            }
                        }
                    }

                    if (claimProcess.Approver1 != null)
                    {
                        if (isApprover2Reqd)
                        {
                            claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Approver2;
                            if (claimWorkflowViewModel.ClaimsAmountTotal >= approver2Amount)
                            {
                                claimWorkflowViewModel.SendBackInfo = string.Empty;
                            }
                            else
                            {
                                ///"Please send the Claim back because it's value requires a lower level of approval."
                                ///caused by change of EIGHER Threshold.XXXApprover2ReqdAmount(raise) OR claim.ClaimsAmountTotal(decrease)
                                claimWorkflowViewModel.SendBackInfo = sSendBackMsg;
                            }
                        }
                        else//not required Approver 2 to review (check box not checked)
                        {
                            ///Approver 1 already reviewed and not require Approver 2's review, 
                            ///means this claim is already passed final review(approved) OR after Approver 1's review, amdin user unchecked Require 'Approver 2' approval 
                            if (claim.Status == ClaimStatus.Approved.ToString())
                            {
                                claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.ApprovePay;
                            }
                            else
                            {
                                claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.Approver2;
                                claimWorkflowViewModel.SendBackInfo = sSendBackMsg;
                            }
                        }
                    }

                    if (claimProcess.Approver2 != null)
                    {
                        claimWorkflowViewModel.ClaimWorkflowStatus = ClaimWorkflowStatus.ApprovePay;
                    }
                }
            }

            claimWorkflowViewModel.RepresentativeView = false;
            claimWorkflowViewModel.LeadView = false;
            claimWorkflowViewModel.SupervisorView = false;
            claimWorkflowViewModel.SupervisorWithoutApproverView = false;
            claimWorkflowViewModel.Approver1View = false;
            claimWorkflowViewModel.Approver2View = false;
            if ((claim.ReviewedBy != null) && (claim.ReviewedBy == SecurityContextHelper.CurrentUser.Id))
            {
                switch (claimWorkflowViewModel.ClaimWorkflowStatus)
                {
                    case ClaimWorkflowStatus.Representative:
                        claimWorkflowViewModel.RepresentativeView = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.RepresentativeWorkflow;
                        break;
                    case ClaimWorkflowStatus.Lead:
                        claimWorkflowViewModel.LeadView = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.LeadWorkflow;
                        break;
                    case ClaimWorkflowStatus.Supervisor:
                        if (isApprover1Reqd)
                        {
                            if (claimWorkflowViewModel.ClaimsAmountTotal.HasValue && claimWorkflowViewModel.ClaimsAmountTotal >= approver1Amount)
                            {
                                claimWorkflowViewModel.SupervisorView = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.SupervisorWorkflow;
                            }
                            else
                            {
                                claimWorkflowViewModel.SupervisorWithoutApproverView = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.SupervisorWorkflow;
                            }
                        }
                        else//not require Approver 1 review(check box not checked)
                        {
                            claimWorkflowViewModel.SupervisorWithoutApproverView = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.SupervisorWorkflow;
                        }
                        break;
                    case ClaimWorkflowStatus.Approver1:
                        if (isApprover2Reqd)
                        {
                            if (claimWorkflowViewModel.ClaimsAmountTotal.HasValue && claimWorkflowViewModel.ClaimsAmountTotal >= approver2Amount)
                            {
                                claimWorkflowViewModel.Approver1View = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.Approver1Workflow;
                            }
                            else
                            {
                                claimWorkflowViewModel.approver1WithoutApprover2View = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.Approver1Workflow;
                            }
                        }
                        else//not require Approver 2 review(check box not checked)
                        {
                            claimWorkflowViewModel.approver1WithoutApprover2View = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.Approver1Workflow;
                        }
                        break;
                    case ClaimWorkflowStatus.Approver2:
                        claimWorkflowViewModel.Approver2View = GetCurrentWorkflowForUser(claim) == TreadMarksConstants.Approver2Workflow;
                        break;
                    default:
                        break;
                }
            }
        }

        private static string GetCurrentWorkflowForUser(Claim claim)
        {
            switch (claim.ClaimType)
            {
                case (int)ClaimType.Collector:
                    return claim.AssignedToUser.UserClaimsWorkflows.Where(u => u.AccountName == TreadMarksConstants.CollectorAccount).FirstOrDefault().Workflow;
                case (int)ClaimType.Hauler:
                    return claim.AssignedToUser.UserClaimsWorkflows.Where(u => u.AccountName == TreadMarksConstants.HaulerAccount).FirstOrDefault().Workflow;
                case (int)ClaimType.Processor:
                    return claim.AssignedToUser.UserClaimsWorkflows.Where(u => u.AccountName == TreadMarksConstants.ProcessorAccount).FirstOrDefault().Workflow;
                case (int)ClaimType.RPM:
                    return claim.AssignedToUser.UserClaimsWorkflows.Where(u => u.AccountName == TreadMarksConstants.RPMAccount).FirstOrDefault().Workflow;
                default:
                    return string.Empty;
            }
        }

        private static void SetupClaimProgressbar(ClaimWorkflowViewModel claimWorkflowViewModel,
            int approver1Amount, int approver2Amount, bool isApprover1Reqd, bool isApprover2Reqd, bool isNegativeClaimReqd, string negativeClaimApprover)
        {
            System.Diagnostics.Debug.WriteLine("claimWorkflowViewModel.ClaimsAmountTotal " + claimWorkflowViewModel.ClaimsAmountTotal);
            if (claimWorkflowViewModel.ClaimsAmountTotal == null)
            {

                //unified workflow, follow existing collector process
                claimWorkflowViewModel.IsApprover2BtnVisible = false;
                claimWorkflowViewModel.IsApprover1BtnVisible = false;
                claimWorkflowViewModel.IsApproverBtnVisible = true;
            }
            else
            {
                //Approver 2
                if (isApprover2Reqd && claimWorkflowViewModel.ClaimsAmountTotal >= approver2Amount)
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = true;
                    claimWorkflowViewModel.IsApprover1BtnVisible = false;
                    claimWorkflowViewModel.IsApproverBtnVisible = false;
                }
                //Approver 1
                else if (isApprover1Reqd && claimWorkflowViewModel.ClaimsAmountTotal >= approver1Amount)
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = false;
                    claimWorkflowViewModel.IsApprover1BtnVisible = true;
                    claimWorkflowViewModel.IsApproverBtnVisible = false;
                }
                //Approver
                else
                {
                    claimWorkflowViewModel.IsApprover2BtnVisible = false;
                    claimWorkflowViewModel.IsApprover1BtnVisible = false;
                    claimWorkflowViewModel.IsApproverBtnVisible = true;
                }

                if (isNegativeClaimReqd && claimWorkflowViewModel.ClaimsAmountTotal < 0)
                {
                    switch (negativeClaimApprover)
                    {
                        case "Approver2":
                            claimWorkflowViewModel.IsApprover2BtnVisible = true;
                            claimWorkflowViewModel.IsApprover1BtnVisible = false;
                            claimWorkflowViewModel.IsApproverBtnVisible = false;
                            break;
                        case "Approver1":
                            claimWorkflowViewModel.IsApprover2BtnVisible = false;
                            claimWorkflowViewModel.IsApprover1BtnVisible = true;
                            claimWorkflowViewModel.IsApproverBtnVisible = false;
                            break;
                        default:
                            //don't need to do any thing (When Unchecked: will be approved by the Default Approver...)
                            break;
                    }
                }
            }
        }

        public void UpdateClaimWorkflowStatus(int claimId, long reviewedBy, int fromStatus, int toStatus)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var claimProcess = claimsRepository.GetClaimProcess(claimId);
            var isAdd = false;
            if (claimProcess == null)
            {
                //Add a new claim process
                claimProcess = new ClaimProcess();
                isAdd = true;
            }
            claimProcess.Claim = claim;
            var previousReviewBy = claim.ReviewedBy;
            claim.ReviewedBy = reviewedBy;
            claim.AssignToUserId = reviewedBy;

            //From representative to lead
            if ((fromStatus == 1) && (toStatus == 2))
            {
                claimProcess.RepresentativeReview = previousReviewBy;
                claimProcess.RepresentativeReviewDate = DateTime.UtcNow;
                DoClaimCalculation(claim);
            }
            //From lead back to representative
            if ((fromStatus == 2) && (toStatus == 1))
            {
                claimProcess.RepresentativeReview = null;
                claimProcess.RepresentativeReviewDate = null;
            }
            //From lead to supervisor
            if ((fromStatus == 2) && (toStatus == 3))
            {
                claimProcess.LeadReview = previousReviewBy;
                claimProcess.LeadReviewDate = DateTime.UtcNow;
                DoClaimCalculation(claim);
            }
            //Form supervisor back to lead
            if ((fromStatus == 3) && (toStatus == 2))
            {
                claimProcess.LeadReview = null;
                claimProcess.LeadReviewDate = null;
            }

            //Form supervisor to approver1
            if ((fromStatus == 3) && (toStatus == 4))
            {
                claimProcess.SupervisorReview = previousReviewBy;
                claimProcess.SupervisorReviewDate = DateTime.UtcNow;
                claim.ReviewEndDate = DateTime.UtcNow;
                DoClaimCalculation(claim);
            }

            //Form approver1 back to supervisor
            if ((fromStatus == 4) && (toStatus == 3))
            {
                claimProcess.SupervisorReview = null;
                claimProcess.SupervisorReviewDate = null;
                claim.ReviewEndDate = null;
            }

            //Form approver1 to approver2
            if ((fromStatus == 4) && (toStatus == 5))
            {
                claimProcess.Approver1 = previousReviewBy;
                claimProcess.Approver1Date = DateTime.UtcNow;
                DoClaimCalculation(claim);
            }

            //Form approver2 back to approver1
            if ((fromStatus == 5) && (toStatus == 4))
            {
                claimProcess.Approver1 = null;
                claimProcess.Approver1Date = null;
            }

            claimsRepository.UpdateClaimStatus(claimProcess, isAdd);
        }

        public void UpdateHST(int claimId, decimal totalTax, bool isTaxApplicable = true)
        {
            claimsRepository.UpdateHST(claimId, totalTax, isTaxApplicable);
        }

        private void DoClaimCalculation(Claim claim)
        {
            //Triger claim calculation when submitting a claim -- OTSTM2-480
            var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claim.ID };
            eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
        }

        //OTSTM2-1084 
        public bool ConcurrentUsersValidationBeforeApproving(int claimId, int fromStatus)
        {
            var result = false;
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var claimAmountTotal = claim.ClaimsAmountTotal;

            int approver1Amount = 0;
            bool isApprover1Reqd = false;
            int approver2Amount = 0;
            bool isApprover2Reqd = false;
            string negativeClaimApprover = string.Empty;// "Approver1" || "Approver2";
            bool isNegativeClaimReqd = false;

            switch (claim.ClaimType)
            {
                case 2:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.CollectorApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.CollectorApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CollectorNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBCollectorNegativeClaimReqd").Value == "1";
                    break;
                case 3:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.HaulerApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.HaulerApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.HaulerNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBHaulerNegativeClaimReqd").Value == "1";
                    break;
                case 4:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.ProcessorApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.ProcessorApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.ProcessorNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBProcessorNegativeClaimReqd").Value == "1";
                    break;
                case 5:
                    approver1Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.RPMApprover1ReqdAmount"));
                    isApprover1Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMApprover1Reqd").Value == "1";
                    approver2Amount = Convert.ToInt32(AppSettings.Instance.GetSettingValue("Threshold.RPMApprover2ReqdAmount"));
                    isApprover2Reqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMApprover2Reqd").Value == "1";
                    negativeClaimApprover = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.RPMNegativeClaimApprover").Value;
                    isNegativeClaimReqd = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBRPMNegativeClaimReqd").Value == "1";
                    break;
            }

            switch (fromStatus)
            {
                case 3: //workflow is "Supervisor"                             
                    if (claimAmountTotal < 0)
                    {
                        if (isNegativeClaimReqd)
                        {
                            result = true;
                            return result;
                        }
                    }
                    else
                    {
                        if (isApprover1Reqd)
                        {
                            if (claimAmountTotal >= approver1Amount)
                            {
                                result = true;
                                return result;
                            }
                        }
                    }
                    break;
                case 4: //workflow is "Approver1"                                      
                    if (claimAmountTotal < 0)
                    {
                        if (isNegativeClaimReqd)
                        {
                            if (negativeClaimApprover == "Approver2")
                            {
                                result = true;
                                return result;
                            }
                        }
                        else
                        {
                            result = true;
                            return result;
                        }
                    }
                    else
                    {
                        if (isApprover2Reqd)
                        {
                            if (claimAmountTotal >= approver2Amount)
                            {
                                result = true;
                                return result;
                            }
                        }
                        if (isApprover1Reqd)
                        {
                            if (claimAmountTotal < approver1Amount)
                            {
                                result = true;
                                return result;
                            }
                        }
                        else
                        {
                            result = true;
                            return result;
                        }
                    }
                    break;
                case 5: //workflow is "Approver2"                                      
                    if (claimAmountTotal < 0)
                    {
                        if (isNegativeClaimReqd)
                        {
                            if (negativeClaimApprover == "Approver1")
                            {
                                result = true;
                                return result;
                            }
                        }
                        else
                        {
                            result = true;
                            return result;
                        }
                    }
                    else
                    {
                        if (isApprover2Reqd)
                        {
                            if (claimAmountTotal < approver2Amount)
                            {
                                result = true;
                                return result;
                            }
                        }
                        else
                        {
                            result = true;
                            return result;
                        }
                    }
                    break;
            }

            return result;
        }

        #region  //Approve Claim action
        public string ApproveClaim(int claimId, int fromStatus, int toStatus)
        {
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            //#942 last step validate checking
            string checkingResult = businessRuleCheckingBeforeApproveClaim(claim);
            if (!string.IsNullOrEmpty(checkingResult))
            {
                return checkingResult;
            }
            var claimProcess = claimsRepository.GetClaimProcess(claimId);
            var previousReviewBy = claim.ReviewedBy;
            claim.ApprovedBy = SecurityContextHelper.CurrentUser.Id;
            claim.ApprovalDate = DateTime.UtcNow;
            claim.Status = "Approved";
            //From Supervisor to final approve
            if ((fromStatus == 3) && (toStatus == 6))
            {
                claimProcess.SupervisorReview = previousReviewBy;
                claimProcess.SupervisorReviewDate = DateTime.UtcNow;
                claim.ReviewEndDate = DateTime.UtcNow;
            }

            //Form approver1 to final approve
            if ((fromStatus == 4) && (toStatus == 6))
            {
                claimProcess.Approver1 = previousReviewBy;
                claimProcess.Approver1Date = DateTime.UtcNow;
            }

            //Form approver2 to final approve
            if ((fromStatus == 5) && (toStatus == 6))
            {
                claimProcess.Approver2 = previousReviewBy;
                claimProcess.Approver2Date = DateTime.UtcNow;
            }

            //Create ClaimPaymentSummary
            var claimPaymentSummary = new ClaimPaymentSummary
            {
                Claim = claim,
                CreateDate = DateTime.UtcNow,
            };
            claim.ClaimPaymentSummary = claimPaymentSummary;

            //Always recalculate claim summary
            UpdateClaimSummary(claim);

            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                claimsRepository.UpdateClaim(claim);
                claimsRepository.UpdateClaimStatus(claimProcess, false);
                DoClaimApprovalProcess(claim);
                transactionScope.Complete();
            }

            //Add notification if the claim is Nil
            addNotificationForNilNotification(claim);

            //After approving claim, trigger claim calculation for all subsequent claims - OTSTM2-480
            DoClaimCalculationforSubsequentClaims(claim);

            SendApprovedEmail(claim);
            return string.Empty;
        }
        private string businessRuleCheckingBeforeApproveClaim(Claim claim)
        {
            string result = string.Empty;
            switch (claim.ClaimType)
            {
                //collector checking inbound outbound mismatching 
                case (int)ClaimType.Collector:
                    var collectorClaimSummary = new CollectorClaimSummaryModel();
                    //Load Tire Origin
                    var collectorTireOrigins = claimsRepository.LoadClaimTireOrigins(claim.ID);
                    PopulateInbound(collectorClaimSummary, collectorTireOrigins);

                    //Load outbound                  
                    var collectorClaimDetails = claimsRepository.LoadClaimDetails(claim.ID, DataLoader.Items);
                    var collectorInventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claim.ID);
                    PopulateOutbound(collectorClaimSummary, collectorClaimDetails, collectorInventoryAdjustments);

                    var collectorSubmitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

                    CreateBusinessRuleSet(collectorSubmitClaimRuleSetStrategy);

                    var ruleContext = new Dictionary<string, object>{
                        {"claimRepository", claimsRepository},
                        {"transactionRepository", transactionRepository},
                        {"TotalInbound", collectorClaimSummary.TotalInbound},
                        {"TotalOutbound", collectorClaimSummary.TotalOutbound}
                    };
                    this.BusinessRuleSet.ClearBusinessRules();

                    this.BusinessRuleSet.AddBusinessRule(new CollectorInboundOutboundMatchClaimRule());

                    ExecuteBusinessRules(claim, ruleContext);

                    if (BusinessRuleExecutioResult.Errors.Count > 0)
                    {
                        result = "The total inbound doesn't match with total outbound.";
                    }
                    break;
                case (int)ClaimType.Hauler:
                    //TODO: add Hauler business ruler checking here                                    
                    break;
                case (int)ClaimType.Processor:
                    //TODO: add Processor business ruler checking here     
                    break;
                case (int)ClaimType.RPM:
                    //TODO: add RPM business ruler checking here     
                    break;
                case (int)ClaimType.Steward:
                    //TODO: add Steward business ruler checking here     
                    break;
            }
            return result;
        }
        private void PopulateInbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimTireOrigin> tireOrigins)
        {
            var query = tireOrigins.GroupBy(c => new { c.TireOriginValue })
                    .Select(c => new { Name = c.Key, ItemList = c.ToList() });
            query.ToList().ForEach(c =>
            {
                var tireOriginItemRow = new TireOriginItemRow();
                tireOriginItemRow.TireOriginValue = c.Name.TireOriginValue;
                tireOriginItemRow.TireOrigin = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).Description;
                tireOriginItemRow.DisplayOrder = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).DisplayOrder;
                c.ItemList.ForEach(i =>
                {
                    var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == i.ItemId);
                    var propertyInfo = tireOriginItemRow.GetType().GetProperty(item.ShortName);
                    propertyInfo.SetValue(tireOriginItemRow, i.Quantity, null);
                });
                collectorClaimSummary.InboundList.Add(tireOriginItemRow);
            });

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            collectorClaimSummary.TotalInbound.PLT = tireOrigins.Where(c => c.ItemId == pltItem.ID).Sum(c => c.Quantity);

            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            collectorClaimSummary.TotalInbound.MT = tireOrigins.Where(c => c.ItemId == mtItem.ID).Sum(c => c.Quantity);

            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            collectorClaimSummary.TotalInbound.AGLS = tireOrigins.Where(c => c.ItemId == aglsItem.ID).Sum(c => c.Quantity);

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            collectorClaimSummary.TotalInbound.IND = tireOrigins.Where(c => c.ItemId == indItem.ID).Sum(c => c.Quantity);

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            collectorClaimSummary.TotalInbound.SOTR = tireOrigins.Where(c => c.ItemId == sotrItem.ID).Sum(c => c.Quantity);

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            collectorClaimSummary.TotalInbound.MOTR = tireOrigins.Where(c => c.ItemId == motrItem.ID).Sum(c => c.Quantity);

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            collectorClaimSummary.TotalInbound.LOTR = tireOrigins.Where(c => c.ItemId == lotrItem.ID).Sum(c => c.Quantity);

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            collectorClaimSummary.TotalInbound.GOTR = tireOrigins.Where(c => c.ItemId == gotrItem.ID).Sum(c => c.Quantity);

        }
        private void PopulateOutbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            #region Load TCR
            var tcrClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR).ToList();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.PLT += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MT += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.AGLS += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.IND += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.SOTR += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region  Load DOT
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT).ToList();
            var motrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItemDot.ID).Sum(q => q.Quantity);
            });
            var lotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItemDot.ID).Sum(q => q.Quantity);
            });
            var gotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItemDot.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region Load TotalEligible
            var tcrClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & c.IsEligible).ToList();
            int pltEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                pltEligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.PLT = pltEligible;

            int mtEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                mtEligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MT = mtEligible;

            int aglsEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                aglsEligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.AGLS = aglsEligible;

            int indEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                indEligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.IND = indEligible;

            int sotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                sotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.SOTR = sotrEligible;

            int motrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                motrEligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrEligibleDot = 0;
            var dotClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & c.IsEligible).ToList();
            dotClaimDetailsEligible.ForEach(c =>
            {
                motrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MOTR = motrEligible + motrEligibleDot;

            int lotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                lotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                lotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.LOTR = lotrEligible + lotrEligibleDot;

            int gotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                gotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                gotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.GOTR = gotrEligible + gotrEligibleDot;

            #endregion

            #region Load TotalIneligible
            var tcrClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & !c.IsEligible).ToList();
            int pltIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                pltIneligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.PLT = pltIneligible;

            int mtIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                mtIneligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MT = mtIneligible;

            int aglsIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                aglsIneligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.AGLS = aglsIneligible;

            int indIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                indIneligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.IND = indIneligible;

            int sotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                sotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.SOTR = sotrIneligible;

            int motrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrIneligibleDot = 0;
            var dotClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & !c.IsEligible).ToList();
            dotClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MOTR = motrIneligible + motrIneligibleDot;

            int lotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.LOTR = lotrIneligible + lotrIneligibleDot;

            int gotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.GOTR = gotrIneligible + gotrIneligibleDot;

            #endregion

            #region Load TotalOutbound
            collectorClaimSummary.TotalOutbound.PLT = collectorClaimSummary.TotalEligible.PLT + collectorClaimSummary.TotalIneligible.PLT;
            collectorClaimSummary.TotalOutbound.MT = collectorClaimSummary.TotalEligible.MT + collectorClaimSummary.TotalIneligible.MT;
            collectorClaimSummary.TotalOutbound.AGLS = collectorClaimSummary.TotalEligible.AGLS + collectorClaimSummary.TotalIneligible.AGLS;
            collectorClaimSummary.TotalOutbound.IND = collectorClaimSummary.TotalEligible.IND + collectorClaimSummary.TotalIneligible.IND;
            collectorClaimSummary.TotalOutbound.SOTR = collectorClaimSummary.TotalEligible.SOTR + collectorClaimSummary.TotalIneligible.SOTR;
            collectorClaimSummary.TotalOutbound.MOTR = collectorClaimSummary.TotalEligible.MOTR + collectorClaimSummary.TotalIneligible.MOTR;
            collectorClaimSummary.TotalOutbound.LOTR = collectorClaimSummary.TotalEligible.LOTR + collectorClaimSummary.TotalIneligible.LOTR;
            collectorClaimSummary.TotalOutbound.GOTR = collectorClaimSummary.TotalEligible.GOTR + collectorClaimSummary.TotalIneligible.GOTR;
            #endregion

        }

        private void SendApprovedEmail(Claim claim)
        {
            string emailTo = GetPrimaryContactByClaimId(claim.ID);
            if (EmailContentHelper.IsEmailEnabled("ClaimApproved") && emailTo != "")
            {
                var emailModel = new ApplicationEmailModel()
                {
                    RegistrationNumber = claim.Participant.Number,
                    BusinessName = claim.Participant.BusinessName,
                    MonthYear = claim.ClaimPeriod.ShortName,
                    Email = emailTo
                };
                SendEmail("ClaimApproved", emailModel);
            }
        }
        #endregion

        private void addNotificationForNilNotification(Claim claim)
        {
            bool isNilClaim = false;
            if (claim.ClaimsAmountTotal == 0)
            {
                isNilClaim = true;
            }

            if (isNilClaim)
            {
                AddNotification(claim);
            }
        }

        private void AddNotification(Claim claim)
        {
            var currentPeriod = claim.ClaimPeriod;
            var participantId = claim.ParticipantId;
            var nextClaim = this.claimsRepository.GetNextClaimByVendorId(participantId, currentPeriod);
            if (nextClaim != null)
            {
                if (nextClaim.Status == "Under Review")
                {
                    var notification = new Notification();
                    notification.Assigner = SecurityContextHelper.CurrentUser.UserName;
                    notification.AssignerName = SecurityContextHelper.CurrentUser.FullName;
                    notification.AssignerInitial = string.Format("{0}{1}", SecurityContextHelper.CurrentUser.FirstName[0], SecurityContextHelper.CurrentUser.LastName[0]);
                    notification.Assignee = nextClaim.AssignedToUser.UserName;
                    notification.AssigneeName = string.Format("{0} {1}", nextClaim.AssignedToUser.FirstName, nextClaim.AssignedToUser.LastName);
                    notification.CreatedTime = DateTime.Now;
                    notification.NotificationType = TreadMarksConstants.ClaimNotification;
                    notification.Status = "Unread";
                    switch (claim.ClaimType)
                    {
                        case 2:
                            notification.NotificationArea = TreadMarksConstants.Collector;
                            break;
                        case 3:
                            notification.NotificationArea = TreadMarksConstants.Hauler;
                            break;
                        case 4:
                            notification.NotificationArea = TreadMarksConstants.Processor;
                            break;
                        case 5:
                            notification.NotificationArea = TreadMarksConstants.RPM;
                            break;
                    }
                    notification.ObjectId = nextClaim.ID;
                    notification.Message = string.Format("<strong>{0}</strong> approved {1} - {2} Claim. You may proceed with <strong>{3} - {4} Claim review</strong>.", SecurityContextHelper.CurrentUser.FullName, claim.Participant.Number, claim.ClaimPeriod.ShortName, nextClaim.Participant.Number, nextClaim.ClaimPeriod.ShortName);
                    this.messageRepository.AddNotification(notification);
                    //Publish notification event
                    var notificationPayload = new NotificationPayload
                    {
                        NotificationId = notification.ID,
                        Receiver = notification.Assignee
                    };
                    eventAggregator.GetEvent<NotificationEvent>().Publish(notificationPayload);
                }
            }
        }

        private void DoClaimCalculationforSubsequentClaims(Claim claim)
        {
            var claimsIds = claimsRepository.GetNextClaimsIds(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            claimsIds.ForEach(c =>
            {
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = c };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            });
        }

        private void DoClaimApprovalProcess(Claim claim)
        {
            //To Calculate Claim inventory and update vendor inventory
            //Collector claim
            if (claim.Participant.VendorType == 2)
            {
                //Nothing needs to do since collector doesn't have inventory
            }
            else
            {
                //Hauler claim && Processor claim && RPM claim
                AddVendorAndClaimInventoryAfterClaimApproval(claim);
            }
        }

        private void AddVendorAndClaimInventoryAfterClaimApproval(Claim claim)
        {
            //Add vendor inventory
            //Calculate inbound, outbound and inventory adjustment
            List<InventoryItem> claimInventoryItems;
            if (claim.ClaimType == (int)ClaimType.Hauler)
                claimInventoryItems = claimsRepository.LoadHaulerClaimItems(claim.ID);
            else
                claimInventoryItems = claimsRepository.LoadClaimItems(claim.ID);

            var query = claimInventoryItems
                .GroupBy(c => new { c.ItemId, c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var inventoryItems = query.Select(c => new Inventory
            {
                VendorId = claim.ParticipantId,
                ItemId = c.Name.ItemId,
                IsEligible = c.Name.IsEligible,
                Qty = c.Name.Direction ? c.Qty : 0 - c.Qty,
                Weight = c.Name.Direction ? c.Weight : 0 - c.Weight,
                ActualWeight = c.Name.Direction ? c.ActualWeight : 0 - c.ActualWeight
            }).ToList();

            var newInventoryItemsQuery = inventoryItems.GroupBy(c => new { c.ItemId, c.IsEligible }).
                Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var newInventoryItems = newInventoryItemsQuery.Select(c =>
                new Inventory
                {
                    VendorId = claim.ParticipantId,
                    ItemId = c.Name.ItemId,
                    IsEligible = c.Name.IsEligible,
                    Qty = c.Qty,
                    Weight = c.Weight,
                    ActualWeight = c.ActualWeight,
                    UpdatedBy = SecurityContextHelper.CurrentUser.Id,
                    UpdatedDate = DateTime.Now
                }
             ).ToList();

            claimsRepository.UpdateVendorInventory(newInventoryItems, claim.ParticipantId);

            //Add claim inventory
            var previousClaimInventoryItems = claimsRepository.LoadPreviousApprovedClaimInventories(claim.ParticipantId, claim.ClaimPeriod.EndDate);

            previousClaimInventoryItems.AddRange(claimInventoryItems);

            var queryClaimInventory = previousClaimInventoryItems
                .GroupBy(c => new { c.ItemId, c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var inventoryItemsForClaim = queryClaimInventory.Select(c => new ClaimInventory
            {
                ClaimId = claim.ID,
                VendorId = claim.ParticipantId,
                ItemId = c.Name.ItemId,
                IsEligible = c.Name.IsEligible,
                Qty = c.Name.Direction ? c.Qty : 0 - c.Qty,
                Weight = c.Name.Direction ? c.Weight : 0 - c.Weight,
                ActualWeight = c.Name.Direction ? c.ActualWeight : 0 - c.ActualWeight
            }).ToList();

            var newInventoryItemsQueryForClaim = inventoryItemsForClaim.GroupBy(c => new { c.ItemId, c.IsEligible }).
                Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var newInventoryItemsForClaim = newInventoryItemsQueryForClaim.Select(c =>
                new ClaimInventory
                {
                    ClaimId = claim.ID,
                    VendorId = claim.ParticipantId,
                    ItemId = c.Name.ItemId,
                    IsEligible = c.Name.IsEligible,
                    Qty = c.Qty,
                    Weight = c.Weight,
                    ActualWeight = c.ActualWeight,
                    UpdatedDate = DateTime.Now
                }
             ).ToList();

            claimsRepository.RecalculateClaimInventory(newInventoryItemsForClaim, claim.ID);
        }

        private void UpdateClaimSummary(Claim claim)
        {
            if (claim.ClaimType != 2)
            {
                var claimSummary = claim.ClaimSummary;

                //Create a claimsummary if it is not existing
                if (claimSummary == null)
                {
                    claimSummary = new ClaimSummary();
                    claimSummary.Claim = claim;
                    claimSummary.UpdatedDate = DateTime.UtcNow;
                }

                //Load invenotry opening result
                var inventoryOpeningResult = LoadInventoryOpeningResult(claim);
                //Load inventory adjustments
                var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claim.ID);

                //Load claim details
                var items = DataLoader.Items;
                var claimDetails = claimsRepository.LoadClaimDetails(claim.ID, items);

                if (claim.ClaimType == 3)
                {
                    PopulateHaulerClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                }
                if (claim.ClaimType == 4)
                {
                    PopulateProcessClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                }
                if (claim.ClaimType == 5)
                {
                    PopulateRPMClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                }
            }
        }

        private void PopulateProcessClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;

            var query = claimDetails
                .GroupBy(c => new { c.TransactionType, c.Direction })
                .Select(
                    c =>
                        new
                        {
                            Name = c.Key,
                            ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero)),
                            extraDOR = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)i.ExtraDORTotal), 4, MidpointRounding.AwayFromZero))
                        });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight + c.extraDOR;
                }

            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateRPMClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;
            var query = claimDetails
             .GroupBy(c => new { c.TransactionType, c.Direction })
             .Select(c => new
             {
                 Name = c.Key,
                 ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero))
             });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPSR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateHaulerClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustEligibleOverallOnRoad = 0;
            decimal adjustEligibleOverallOffRoad = 0;
            decimal adjustIneligibleOverallOnRoad = 0;
            decimal adjustIneligibleOverallOffRoad = 0;
            decimal eligibleAdjustOnRoad = 0;
            decimal eligibleAdjustOffRoad = 0;
            decimal ineligibleAdjustOnRoad = 0;
            decimal ineligibleAdjustOffRoad = 0;
            decimal outEligibleAdjustOnRoad = 0;
            decimal outEligibleAdjustOffRoad = 0;
            decimal outIneligibleAdjustOnRoad = 0;
            decimal outIneligibleAdjustOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustEligibleOverallOnRoad = c.OnRoad;
                    adjustEligibleOverallOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustIneligibleOverallOnRoad = c.OnRoad;
                    adjustIneligibleOverallOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 1)
                {
                    eligibleAdjustOnRoad = c.OnRoad;
                    eligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 1)
                {
                    ineligibleAdjustOnRoad = c.OnRoad;
                    ineligibleAdjustOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outEligibleAdjustOnRoad = c.OnRoad;
                    outEligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outIneligibleAdjustOnRoad = c.OnRoad;
                    outIneligibleAdjustOffRoad = c.OffRoad;
                }
            });
            var totalEligibleAdjustOnRoad = adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - outEligibleAdjustOnRoad;
            var totalEligibleAdjustOffRoad = adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - outEligibleAdjustOffRoad;
            var totalIneligibleAdjustOnRoad = adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - outIneligibleAdjustOnRoad;
            var totalIneligibleAdjustOffRoad = adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - outIneligibleAdjustOffRoad;

            decimal eligibleOnRoad = 0;
            decimal eligibleOffRoad = 0;
            decimal ineligibleOnRoad = 0;
            decimal ineligibleOffRoad = 0;
            decimal outEligibleOnRoad = 0;
            decimal outEligibleOffRoad = 0;
            decimal outIneligibleOnRoad = 0;
            decimal outIneligibleOffRoad = 0;
            decimal eligibleOnRoadEstimated = 0;
            decimal eligibleOffRoadEstimated = 0;
            decimal ineligibleOnRoadEstimated = 0;
            decimal ineligibleOffRoadEstimated = 0;
            decimal outEligibleOnRoadEstimated = 0;
            decimal outEligibleOffRoadEstimated = 0;
            decimal outIneligibleOnRoadEstimated = 0;
            decimal outIneligibleOffRoadEstimated = 0;

            claimDetails.ForEach(c =>
            {
                //Inbound Eligible
                if (c.Direction && (c.TransactionType == "TCR" || c.TransactionType == "DOT" || c.TransactionType == "STC"))
                {
                    eligibleOnRoad += c.OnRoad;
                    eligibleOffRoad += c.OffRoad;
                    eligibleOnRoadEstimated += c.EstimatedOnRoad;
                    eligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Inbound Ineligible
                if (c.Direction && (c.TransactionType == "UCR" || c.TransactionType == "HIT"))
                {
                    ineligibleOnRoad += c.OnRoad;
                    ineligibleOffRoad += c.OffRoad;
                    ineligibleOnRoadEstimated += c.EstimatedOnRoad;
                    ineligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Eligible
                if (!c.Direction && c.TransactionType == "PTR")
                {
                    outEligibleOnRoad += c.OnRoad;
                    outEligibleOffRoad += c.OffRoad;
                    outEligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outEligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Ineligible
                if (!c.Direction && (c.TransactionType == "RTR" || c.TransactionType == "HIT"))
                {
                    outIneligibleOnRoad += c.OnRoad;
                    outIneligibleOffRoad += c.OffRoad;
                    outIneligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outIneligibleOffRoadEstimated += c.EstimatedOffRoad;
                }
            });

            //Calculate outbound total eligible and ineligible
            var totalInboundOnroad = inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad + inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad;
            var eligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutboundOnroad = outEligibleOnRoad + outIneligibleOnRoad + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroad = Math.Round(totalOutboundOnroad * eligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad * ineligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroad == 0)
            {
                totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOnroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated;
            var eligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutboundOnroadEstimated = outEligibleOnRoadEstimated + outIneligibleOnRoadEstimated + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * eligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * ineligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroadEstimated == 0)
            {
                totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroad = inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad + inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad;
            var eligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutboundOffroad = outEligibleOffRoad + outIneligibleOffRoad + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroad = Math.Round(totalOutboundOffroad * eligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad * ineligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroad == 0)
            {
                totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated;
            var eligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutboundOffroadEstimated = outEligibleOffRoadEstimated + outIneligibleOffRoadEstimated + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * eligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * ineligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroadEstimated == 0)
            {
                totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            claimSummary.EligibleClosingOnRoad = claimSummary.EligibleOpeningOnRoad + eligibleOnRoad + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroad;
            claimSummary.EligibleClosingOffRoad = claimSummary.EligibleOpeningOffRoad + eligibleOffRoad + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroad;
            claimSummary.IneligibleClosingOnRoad = claimSummary.IneligibleOpeningOnRoad + ineligibleOnRoad + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroad;
            claimSummary.IneligibleClosingOffRoad = claimSummary.IneligibleOpeningOffRoad + ineligibleOffRoad + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroad;

            claimSummary.EligibleClosingOnRoadEstimated = claimSummary.EligibleOpeningOnRoadEstimated + eligibleOnRoadEstimated + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroadEstimated;
            claimSummary.EligibleClosingOffRoadEstimated = claimSummary.EligibleOpeningOffRoadEstimated + eligibleOffRoadEstimated + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroadEstimated;
            claimSummary.IneligibleClosingOnRoadEstimated = claimSummary.IneligibleOpeningOnRoadEstimated + ineligibleOnRoadEstimated + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroadEstimated;
            claimSummary.IneligibleClosingOffRoadEstimated = claimSummary.IneligibleOpeningOffRoadEstimated + ineligibleOffRoadEstimated + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroadEstimated;

            claimSummary.TotalClosing = claimSummary.EligibleClosingOnRoad + claimSummary.EligibleClosingOffRoad + claimSummary.IneligibleClosingOnRoad + claimSummary.IneligibleClosingOffRoad;
            claimSummary.TotalClosingEstimated = claimSummary.EligibleClosingOnRoadEstimated + claimSummary.EligibleClosingOffRoadEstimated + claimSummary.IneligibleClosingOnRoadEstimated + claimSummary.IneligibleClosingOffRoadEstimated;

        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if (previousClaimSummary != null)
            {
                //Set current claim opening based on previous claim summary
                inventoryOpeningSummary.TotalEligibleOpeningOnRoad = previousClaimSummary.EligibleClosingOnRoad != null ? (decimal)previousClaimSummary.EligibleClosingOnRoad : 0;
                inventoryOpeningSummary.TotalEligibleOpeningOffRoad = previousClaimSummary.EligibleClosingOffRoad != null ? (decimal)previousClaimSummary.EligibleClosingOffRoad : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = previousClaimSummary.IneligibleClosingOnRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoad : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = previousClaimSummary.IneligibleClosingOffRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoad : 0;
                inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
            }
            return inventoryOpeningSummary;
        }

        public void ClaimBackToParticipant(int claimId)
        {
            claimsRepository.ClaimBackToParticipant(claimId);
            string emailTo = GetPrimaryContactByClaimId(claimId);
            if (EmailContentHelper.IsEmailEnabled("ClaimBacktoParticipant") && emailTo != "")
            {
                Claim claim = this.claimsRepository.FindClaimByClaimId(claimId);
                var emailModel = new ApplicationEmailModel()
                {
                    RegistrationNumber = claim.Participant.Number,
                    BusinessName = claim.Participant.BusinessName,
                    MonthYear = claim.ClaimPeriod.ShortName,
                    Email = emailTo
                };
                SendEmail("ClaimBacktoParticipant", emailModel);
            }
        }

        public List<ClaimWorkflowUser> LoadClaimWorkflowUsers(string Workflow, string AccountName)
        {
            return userRepository.LoadClaimWorkflowUsers(Workflow, AccountName);
        }

        public void UpdatedClaimMailDate(int claimId, DateTime mailDate)
        {
            claimsRepository.UpdateClaimMailDate(claimId, mailDate);
        }

        private void AddInternalPaymentAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            if (modalResult.Amount != 0)
            {
                if (string.IsNullOrWhiteSpace(modalResult.PaymentType))
                    modalResult.PaymentType = "Overall";
                var claimInternalPaymentAdjustment = new ClaimInternalPaymentAdjust();
                claimInternalPaymentAdjustment.ClaimId = claimId;
                claimInternalPaymentAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
                claimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                claimInternalPaymentAdjustment.AdjustmentAmount = modalResult.Amount;
                claimInternalPaymentAdjustment.PaymentType = (int)(modalResult.PaymentType.ToEnum<ClaimPaymentType>());
                //claimsRepository.AddInternalPaymentAdjustment(claimInternalPaymentAdjustment);

                //OTSTM2-270
                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    claimsRepository.AddInternalPaymentAdjustment(claimInternalPaymentAdjustment);
                    AddActivity(claimId, modalResult.SelectedItem.Id, claimInternalPaymentAdjustment.ID, "added");
                    claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                    {
                        ClaimInternalPaymentAdjustId = claimInternalPaymentAdjustment.ID,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });

                    transationScope.Complete();
                }
            }
        }

        private void AddInventoryAdjustmentForTireYardCount(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var inventoryAdjust = new ClaimInventoryAdjustment
            {
                ClaimId = claimId,
                AdjustBy = SecurityContextHelper.CurrentUser.Id,
                AdjustDate = DateTime.UtcNow,
                IsEligible = modalResult.IsEligible,
                AdjustmentType = modalResult.SelectedItem.Id,
                Direction = modalResult.DirectionValue,
                AdjustmentWeightOnroad = 0,
                AdjustmentWeightOffroad = 0
            };

            PopulateInventoryAdjustItems(modalResult, claimPeriod, inventoryAdjust);

            //OTSTM2-270
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                if ((inventoryAdjust.AdjustmentWeightOnroad != 0) || (inventoryAdjust.AdjustmentWeightOffroad != 0) || (inventoryAdjust.ClaimInventoryAdjustItems.Where(i => i.QtyAdjustment != 0).Count() != 0))
                {
                    claimsRepository.AddInventoryAdjustment(inventoryAdjust);
                    claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                    {
                        ClaimInternalAdjustmentId = inventoryAdjust.ID,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });
                }

                transationScope.Complete();
            }
        }

        private void PopulateInventoryAdjustItems(ClaimAdjustmentModalResult modalResult, Period claimPeriod, ClaimInventoryAdjustment inventoryAdjust)
        {
            //PLT
            //if (modalResult.PLT != 0)
            {
                var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == pltItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.PLT;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = pltItem.ID,
                    QtyAdjustment = modalResult.PLT,
                    QtyBeforeAdjustment = modalResult.PLTBeforeAdj,
                    QtyAfterAdjustment = modalResult.PLTAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (pltItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }

            //MT
            //if (modalResult.MT != 0)
            {
                var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == mtItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.MT;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = mtItem.ID,
                    QtyAdjustment = modalResult.MT,
                    QtyBeforeAdjustment = modalResult.MTBeforeAdj,
                    QtyAfterAdjustment = modalResult.MTAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (mtItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //AGLS
            //if (modalResult.AGLS != 0)
            {
                var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == aglsItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.AGLS;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = aglsItem.ID,
                    QtyAdjustment = modalResult.AGLS,
                    QtyBeforeAdjustment = modalResult.AGLSBeforeAdj,
                    QtyAfterAdjustment = modalResult.AGLSAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (aglsItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //IND
            //if (modalResult.IND != 0)
            {
                var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == indItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.IND;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = indItem.ID,
                    QtyAdjustment = modalResult.IND,
                    QtyBeforeAdjustment = modalResult.INDBeforeAdj,
                    QtyAfterAdjustment = modalResult.INDAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);

                if (indItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //SOTR
            //if (modalResult.SOTR != 0)
            {
                var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == sotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.SOTR;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = sotrItem.ID,
                    QtyAdjustment = modalResult.SOTR,
                    QtyBeforeAdjustment = modalResult.SOTRBeforeAdj,
                    QtyAfterAdjustment = modalResult.SOTRAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (sotrItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //MOTR
            //if (modalResult.MOTR != 0)
            {
                var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == motrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.MOTR;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = motrItem.ID,
                    QtyAdjustment = modalResult.MOTR,
                    QtyBeforeAdjustment = modalResult.MOTRBeforeAdj,
                    QtyAfterAdjustment = modalResult.MOTRAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (motrItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //LOTR
            //if (modalResult.LOTR != 0)
            {
                var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == lotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.LOTR;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = lotrItem.ID,
                    QtyAdjustment = modalResult.LOTR,
                    QtyBeforeAdjustment = modalResult.LOTRBeforeAdj,
                    QtyAfterAdjustment = modalResult.LOTRAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (lotrItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
            //GOTR
            //if (modalResult.GOTR != 0)
            {
                var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == gotrItem.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate).StandardWeight;
                var adjustWeight = itemWeight * modalResult.GOTR;
                var claimInventoryAdjustItem = new ClaimInventoryAdjustItem
                {
                    ItemId = gotrItem.ID,
                    QtyAdjustment = modalResult.GOTR,
                    QtyBeforeAdjustment = modalResult.GOTRBeforeAdj,
                    QtyAfterAdjustment = modalResult.GOTRAfterAdj,
                    WeightAdjustment = adjustWeight
                };
                inventoryAdjust.ClaimInventoryAdjustItems.Add(claimInventoryAdjustItem);
                if (gotrItem.ItemType == 1)
                {
                    inventoryAdjust.AdjustmentWeightOnroad += adjustWeight;
                }
                else
                {
                    inventoryAdjust.AdjustmentWeightOffroad += adjustWeight;
                }
            }
        }

        private void AddInventoryAdjustmentForWeight(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            var claimInventoryAdjustment = new ClaimInventoryAdjustment();
            claimInventoryAdjustment.ClaimId = claimId;
            claimInventoryAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
            claimInventoryAdjustment.AdjustDate = DateTime.UtcNow;
            claimInventoryAdjustment.IsEligible = modalResult.IsEligible;
            claimInventoryAdjustment.AdjustmentType = modalResult.SelectedItem.Id;
            claimInventoryAdjustment.Direction = modalResult.DirectionValue;
            claimInventoryAdjustment.AdjustmentWeightOnroad = GetDBWeight(modalResult.UnitType, modalResult.Onroad);
            claimInventoryAdjustment.AdjustmentWeightOffroad = GetDBWeight(modalResult.UnitType, modalResult.Offroad);
            claimInventoryAdjustment.UnitType = modalResult.UnitType;

            //OTSTM2-270
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                if ((claimInventoryAdjustment.AdjustmentWeightOnroad != 0) || (claimInventoryAdjustment.AdjustmentWeightOffroad != 0))
                {
                    claimsRepository.AddInventoryAdjustment(claimInventoryAdjustment);
                    claimsRepository.AddInternalAdjustmentNote(new ClaimInternalAdjustmentNote()
                    {
                        ClaimInternalAdjustmentId = claimInventoryAdjustment.ID,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });
                }

                transationScope.Complete();
            }
        }

        //OTSTM2-270 
        public InternalNoteViewModel AddNotesHandlerInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string notes)
        {
            var note = new ClaimInternalAdjustmentNote()
            {
                ClaimInternalAdjustmentId = ClaimInternalAdjustmentId,
                ClaimInternalPaymentAdjustId = ClaimInternalPaymentAdjustId,
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserId = SecurityContextHelper.CurrentUser.Id
            };

            claimsRepository.AddInternalAdjustmentNote(note);

            return new InternalNoteViewModel() { Note = note.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = note.CreatedDate };
        }

        //OTSTM2-270
        public List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId)
        {
            List<ClaimInternalAdjustmentNote> internalNoteList = null;

            if (ClaimInternalAdjustmentId.HasValue)
            {
                //internal adjustment
                internalNoteList = claimsRepository.LoadClaimInternalNotesForInventory(ClaimInternalAdjustmentId.Value).OrderByDescending(s => s.CreatedDate).ToList();
            }
            else if (ClaimInternalPaymentAdjustId.HasValue)
            {
                //claim payment adjustment
                internalNoteList = claimsRepository.LoadClaimInternalNotesForPayment(ClaimInternalPaymentAdjustId.Value).OrderByDescending(s => s.CreatedDate).ToList();
            }

            var list = new List<InternalNoteViewModel>();
            foreach (var internalNote in internalNoteList)
            {
                var user = internalNote.AddedBy;
                list.Add(new InternalNoteViewModel() { AddedOn = internalNote.CreatedDate, AddedBy = user.FirstName + " " + user.LastName, Note = internalNote.Note });
            }

            return list;
        }
        //OTSTM2-270
        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            return claimsRepository.ExportToExcelInternalAdjustmentNotes(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);
        }


        /// <summary>
        /// Always store KG to DB
        /// </summary>
        /// <param name="unitType"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        private decimal GetDBWeight(int unitType, decimal weight)
        {
            switch (unitType)
            {
                case 1:
                    return DataConversion.ConvertLbToKg((double)weight);
                case 2:
                    return weight;
                case 3:
                    return DataConversion.ConvertTonToKg((double)weight);
            }
            return weight;
        }

        private decimal GetUIWeight(int unitType, decimal weight)
        {
            switch (unitType)
            {
                case 1:
                    return DataConversion.ConvertKgToLb((double)weight);
                case 2:
                    return weight;
                case 3:
                    return DataConversion.ConvertKgToTon((double)weight);
            }
            return weight;
        }

        #region participant claims
        public PaginationDTO<ClaimViewModel, int> LoadVendorClaims(int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var result = claimsRepository.LoadVendorClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, columnSearchText);
            //OTSTM2-484
            //OTSTM2-1139 No talk back needed
            //result.DTOCollection.ForEach(c =>
            //{
            //    if (c.IsClaimPost)
            //    {
            //        var claimPaymentSummary = UpdateGpBatchForClaim(c.ID);
            //        c.EftNumber = claimPaymentSummary != null ? (claimPaymentSummary.ChequeNumber != null ? claimPaymentSummary.ChequeNumber : c.EftNumber) : c.EftNumber;
            //        c.PaymentDate = claimPaymentSummary != null ? (claimPaymentSummary.PaidDate != null ? claimPaymentSummary.PaidDate : c.PaymentDate) : c.PaymentDate;
            //    }
            //});
            return result;
        }
        public List<ClaimViewModel> GetExportDetailsParticipantClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            return this.claimsRepository.GetVendorParticipantClaimsList(searchText, orderBy, sortDirection, vendorId, columnSearchText);
        }

        #endregion

        #region staff claims
        public PaginationDTO<ClaimViewModel, int> LoadVendorStaffClaims(int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            var result = claimsRepository.LoadVendorStaffClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, columnSearchText);
            //OTSTM2-484
            //OTSTM2-1139 No talk back needed
            //result.DTOCollection.ForEach(c =>
            //{
            //    if (c.IsClaimPost)
            //    {
            //        var claimPaymentSummary = UpdateGpBatchForClaim(c.ID);
            //        c.EftNumber = claimPaymentSummary != null ? (claimPaymentSummary.ChequeNumber != null ? claimPaymentSummary.ChequeNumber : c.EftNumber) : c.EftNumber;
            //        c.PaymentDate = claimPaymentSummary != null ? (claimPaymentSummary.PaidDate != null ? claimPaymentSummary.PaidDate : c.PaymentDate) : c.PaymentDate;
            //    }
            //});
            return result;
        }

        //OTSTM2-484
        //OTSTM2-1139 No talk back needed
        //private ClaimPaymentSummary UpdateGpBatchForClaim(int claimId)
        //{
        //    var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);

        //    if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
        //    {
        //        var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
        //        var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
        //        if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
        //        {
        //            return claimsRepository.UpdateAndReturnClaimPaymentSummary(gpPaymentSummary, claimId);
        //        }
        //    }
        //    return null;
        //}
        public List<ClaimViewModel> GetExportDetailsStaffClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            return this.claimsRepository.GetVendorStaffClaimsList(searchText, orderBy, sortDirection, vendorId, columnSearchText);
        }

        #endregion

        #region Internal Adjust
        public PaginationDTO<ClaimInternalAdjustment, int> LoadClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            return this.claimsRepository.LoadClaimInternalAdjustments(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, isStaff);//LoadClaimInternalAdjustments
        }

        #endregion

        #region TransactionAdjustment
        public PaginationDTO<TransactionAdjustmentViewModel, int> LoadClaimTransactionAdjustment(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            return this.claimsRepository.LoadClaimTransactionAdjustment(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
        }
        #endregion

        public bool IsTransactionInSameClaim(int claimId, int transactionId)
        {
            return this.claimsRepository.IsTransactionInSameClaim(claimId, transactionId);
        }

        public string GetClaimStatus(int claimId)
        {
            return this.claimsRepository.GetClaimStatus(claimId);
        }
        public bool IsClaimOpen(int claimId)
        {
            return this.claimsRepository.IsClaimOpen(claimId);
        }
        public bool IsClaimAllowAddNewTransaction(int claimId)
        {
            return this.claimsRepository.IsClaimAllowAddNewTransaction(claimId);
        }

        public TransactionClaimAssociationDetailViewModel GetTransactionClaimAssociationDetail(int transactionId)
        {
            return this.claimsRepository.GetTransactionClaimAssociationDetailDataModel(transactionId);
        }

        public List<TransactionSearchListViewModel> GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType, int direction, int vendorId)
        {
            return this.claimsRepository.GetTransactionSearchList(claimId, transactionFormat, processingStatus, transactionType, direction, vendorId);
        }

        public VendorReference GetVendorRefernce(string regNumber)
        {
            return vendorRepository.GetVendorByNumber(regNumber);
        }

        public VendorReference GetVendorReference(int claimId)
        {
            return claimsRepository.GetVendorReference(claimId);
        }
        public VendorReference GetVendorById(int vendorId)
        {
            return vendorRepository.GetVendorById(vendorId);
        }
        public IEnumerable<int> GetAllClaimsToReCalculate(int? vendorType = null)
        {
            return claimsRepository.GetAllClaimsToReCalculate(vendorType);
        }

        //Notification Load test only
        public IEnumerable<int> GetAllUnderReviewClaimsIds()
        {
            return claimsRepository.GetAllUnderReviewClaimsIds();
        }

        public List<int> GetAllApprovedClaimIdsForInventoryCal(int? vendorId)
        {
            return claimsRepository.GetAllApprovedClaimIdsForInventoryCal(vendorId);
        }

        public void RecalculateClaimInvenotry(int claimId)
        {
            var claim = claimsRepository.GetClaimWithSummaryPeriod(claimId);
            var claimInventoryItems = claimsRepository.LoadAllClaimInventoryItems(claim.ParticipantId, claim.ClaimPeriod.EndDate);

            var query = claimInventoryItems
              .GroupBy(c => new { c.ItemId, c.IsEligible, c.Direction })
              .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var claimInventories = query.Select(c => new ClaimInventory
            {
                ClaimId = claimId,
                VendorId = claim.ParticipantId,
                ItemId = c.Name.ItemId,
                IsEligible = c.Name.IsEligible,
                Qty = c.Name.Direction ? c.Qty : 0 - c.Qty,
                Weight = c.Name.Direction ? c.Weight : 0 - c.Weight,
                ActualWeight = c.Name.Direction ? c.ActualWeight : 0 - c.ActualWeight
            }).ToList();

            var newInventoryItemsQuery = claimInventories.GroupBy(c => new { c.ItemId, c.IsEligible }).
                Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty), Weight = c.Sum(i => i.Weight), ActualWeight = c.Sum(i => i.ActualWeight) });

            var newInventoryItems = newInventoryItemsQuery.Select(c =>
                new ClaimInventory
                {
                    ClaimId = claimId,
                    VendorId = claim.ParticipantId,
                    ItemId = c.Name.ItemId,
                    IsEligible = c.Name.IsEligible,
                    Qty = c.Qty,
                    Weight = c.Weight,
                    ActualWeight = c.ActualWeight,
                    UpdatedDate = DateTime.Now
                }
             ).ToList();

            claimsRepository.RecalculateClaimInventory(newInventoryItems, claim.ID);
        }

        #region Internal Notes
        public List<InternalNoteViewModel> LoadClaimInternalNotes(int claimId)
        {
            var internalNoteList = claimsRepository.LoadClaimInternalNotes(claimId).OrderByDescending(s => s.CreatedDate);

            var list = new List<InternalNoteViewModel>();
            foreach (var internalNote in internalNoteList)
            {
                var user = internalNote.User;
                list.Add(new InternalNoteViewModel() { AddedOn = internalNote.CreatedDate, AddedBy = user.FirstName + " " + user.LastName, Note = internalNote.Note });
            }

            return list;
        }

        public InternalNoteViewModel AddClaimNote(int claimId, string note)
        {
            var claimNote = new ClaimNote
            {
                ClaimId = Convert.ToInt32(claimId),
                Note = note,
                CreatedDate = DateTime.UtcNow,
                UserId = SecurityContextHelper.CurrentUser.Id
            };

            claimsRepository.AddClaimNote(claimNote);

            return new InternalNoteViewModel() { Note = claimNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = claimNote.CreatedDate };
        }

        public void AddSystemClaimNotes(int claimId, List<string> messages)
        {
            var noteList = new List<ClaimNote>();
            messages.ForEach(c =>
            {
                var claimNote = new ClaimNote
                {
                    ClaimId = Convert.ToInt32(claimId),
                    Note = c,
                    CreatedDate = DateTime.UtcNow,
                    UserId = AppSettings.Instance.SystemUser.ID
                };
                noteList.Add(claimNote);
            });
            claimsRepository.AddClaimNotes(noteList);
        }

        public List<InternalNoteViewModel> LoadClaimNotesForExport(int claimId, string searchText, string sortcolumn, bool sortReverse)
        {
            return claimsRepository.LoadClaimNotesForExport(claimId, searchText, sortcolumn, sortReverse);
        }

        public int? GetTransactionIdByFriendlyId(long friendlyId, int claimId)
        {
            //#1172 filter UCR transactions for collector.
            return claimsRepository.GetTransactionIdByFriendlyId(friendlyId, claimId);
        }
        #endregion

        public string checkOtherClaimStatus(int claimId, int transactionId)
        {
            return claimsRepository.checkOtherClaimStatus(claimId, transactionId);
        }

        public StaffAllClaimsModel FindClaimById(int claimId)
        {
            return claimsRepository.FindClaimById(claimId);
        }

        public Claim FindClaimByClaimId(int claimId)
        {
            return claimsRepository.FindClaimByClaimId(claimId);
        }

        public void ApproveLateCollectorClaimForSupport(string registrationNumber, int claimPeriodId)
        {
            var claim = claimsRepository.GetClaimByNumberAndPeriod(registrationNumber, claimPeriodId);
            var isAllTransactionReviewed = claimsRepository.AllTransactionIsReviewed(claim.ID);
            if (isAllTransactionReviewed)
            {
                claim.ApprovedBy = 340; //clima@rethinktires.ca on behalf of Christina to approve collector late claims
                claim.ApprovalDate = DateTime.UtcNow;
                claim.Status = "Approved";
                claimsRepository.UpdateClaim(claim);
                //Approve all associated transactions
                claimsRepository.ApproveAllAssociatedTransactions(claim.ID);
            }
        }

        public List<int> GetMissingReportDataClaimIds()
        {
            return claimsRepository.GetMissingReportDataClaimIds();
        }

        public List<int> GetMissingCollectorClaimIds()
        {
            return claimsRepository.GetMissingCollectorClaimIds();
        }

        public List<int> LoadAllClaimIdsByVendorTypeAndStatus(int vendorType = 2, CL.TMS.Common.Enum.ClaimStatus claimStatus = CL.TMS.Common.Enum.ClaimStatus.Approved)
        {
            return claimsRepository.LoadAllClaimIdsByVendorTypeAndStatus(vendorType, claimStatus);
        }

        public List<ClaimTireOrigin> LoadClaimTireOrigins(int claimId)
        {
            return claimsRepository.LoadClaimTireOrigins(claimId);
        }
        public List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId)
        {
            return claimsRepository.LoadClaimInventoryAdjustments(claimId);
        }

        public List<ClaimDetailViewModel> LoadClaimDetails(int claimId, List<Item> items)
        {
            return claimsRepository.LoadClaimDetails(claimId, items);
        }

        public IEnumerable<int> GetAllClaimsToReCalculateByStatus(int? vendorType = null, string status = "")
        {
            return claimsRepository.GetAllClaimsToReCalculateByStatus(vendorType, status);
        }

        public List<int> getAllVenderByType(int vType)
        {
            return claimsRepository.getAllVenderByType(vType);
        }
        public List<int> LoadAllClaimIdsByVendorID(int vendorID)
        {
            return claimsRepository.LoadAllClaimIdsByVendorID(vendorID);
        }
        private bool SendEmail(string emailName, ApplicationEmailModel emailModel)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(emailModel.Email);
            MailMessage message = new MailMessage(from, to) { };

            string emailBody = EmailContentHelper.GetCompleteEmailByName(emailName);
            string subject = EmailContentHelper.GetSubjectByName(emailName);
            string loginURL = string.Format("{0}{1}", SiteSettings.Instance.DomainName, "/Account/Login");

            emailBody = emailBody
                .Replace(EmailTemplateResource.siteURL, loginURL)
                .Replace(StewardApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(StewardApplicationEmailTemplPlaceHolders.RegistrationNumber, emailModel.RegistrationNumber)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessName, emailModel.BusinessName)
                .Replace(EmailTemplateResource.ClaimPeriod, emailModel.MonthYear)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString());

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"));
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            Task.Factory.StartNew(() =>
            {
                //emailer.SendEmail(null, emailTo, ccEmailAddr, bccEmailAddr, subject, emailBody, model, null, alternateViewHTML);
                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, null, subject, emailBody, null, null, alternateViewHTML);
            });
            return true;
        }
    }
}
