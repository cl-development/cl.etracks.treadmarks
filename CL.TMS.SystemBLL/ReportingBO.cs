﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Metric;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.SystemBLL
{
    public class ReportingBO
    {
        #region Repositories
        private IReportingRepository reportingRepository;
        private IUserRepository userRepository;
        #endregion

        public ReportingBO(IReportingRepository reportingRepository, IUserRepository userRepository)
        {
            this.reportingRepository = reportingRepository;
            this.userRepository = userRepository;
        }
        public IEnumerable<ReportCategory> GetAllReportCategories(int Id = 0)
        {
            return reportingRepository.GetAllReportCategories(Id);
        }
        public List<ReportCategory> GetPermittedReportCategories()
        {
            var allReports = reportingRepository.GetAllReportCategories(0);
            List<ReportCategory> permittedReports = new List<ReportCategory>();

            foreach (ReportCategory report in allReports)
            {
                if (Convert.ToInt16(ReportCategoryEnum.Steward) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsSteward) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
                if (Convert.ToInt16(ReportCategoryEnum.Collector) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsCollector) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
                if (Convert.ToInt16(ReportCategoryEnum.Hauler) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsHauler) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
                if (Convert.ToInt16(ReportCategoryEnum.Processor) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsProcessor) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
                if (Convert.ToInt16(ReportCategoryEnum.RPM) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsRPM) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
                if (Convert.ToInt16(ReportCategoryEnum.Registrant) == report.ID && ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ReportsAllReportsRegistrant) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    permittedReports.Add(report);
            }

            return permittedReports;
        }
        public IEnumerable<Report> GetReportsByCategoryId(int Id)
        {
            return reportingRepository.GetReportsByCategoryId(Id);
        }
        public IEnumerable<RegistrantWeeklyReport> GetRegistrantWeeklyReport()
        {
            return reportingRepository.GetRegistrantWeeklyReport();
        }
        public IEnumerable<SpRptTsfExtractInBatchOnlyReportGp> GetTsfExtrtactInBatchOnlyReportGp()
        {
            return reportingRepository.GetTsfExtrtactInBatchOnlyReportGp();
        }
        public IEnumerable<SpRptProcessorTIPIReport> GetProcessorTIPIReport(DateTime? startDate, DateTime? endDate)
        {
            return reportingRepository.GetProcessorTIPIReport(startDate, endDate);
        }
        public IEnumerable<SpRptHaulerCollectorComparisonReport> GetHaulerCollectorComparisonReport(DateTime? startDate, DateTime? endDate, string registrationNumber)
        {
            return reportingRepository.GetHaulerCollectorComparisonReport(startDate, endDate, registrationNumber);
        }
        public Report GetReport(int Id)
        {
            return reportingRepository.GetReport(Id);
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireVM> GetStewardRevenueSupplyNewTireTypes(ReportDetailsViewModel detail)
        {
            return reportingRepository.GetStewardRevenueSupplyNewTireTypes(detail);
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVM> GetStewardRevenueSupplyNewTireCreditTypes(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            return reportingRepository.GetStewardRevenueSupplyNewTireCreditTypes(detail, TSFNegAdjSwitchDate);
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVMsp> GetStewardRevenueSupplyNewTireCreditTypes_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            return reportingRepository.GetStewardRevenueSupplyNewTireCreditType_SP(detail, TSFNegAdjSwitchDate);
        }
        public IEnumerable<WebListingCollectorReportPostalVM> GetWebListingCollectorReportPostal(int reportId)
        {
            string postalCode = string.Empty;
            switch (reportId)
            {
                case 9:
                    postalCode = "K";
                    break;
                case 10:
                    postalCode = "L";
                    break;
                case 11:
                    postalCode = "M";
                    break;
                case 12:
                    postalCode = "N";
                    break;
                case 13:
                    postalCode = "P";
                    break;
                default:
                    break;
            }
            return reportingRepository.GetWebListingCollectorReportPostal(postalCode);
        }
        public IEnumerable<WebListingHaulerReport> GetWebListingHaulerReport()
        {
            return reportingRepository.GetWebListingHaulerReport();
        }

        public IEnumerable<HaulerVolumeReport> GetHaulerVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            return reportingRepository.GetHaulerVolumeReport(startDate, endDate);
        }
        public IEnumerable<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> GetDetailHaulerVolumeReportBasedOnScaleWeight(DateTime? startDate, DateTime? endDate)
        {
            return reportingRepository.GetDetailHaulerVolumeReportBasedOnScaleWeight(startDate, endDate);
        }
        public IEnumerable<SpRptProcessorVolumeReport> GetProcessorVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            return reportingRepository.GetProcessorVolumeReport(startDate, endDate);
        }
        public IEnumerable<RpmData> GetRpmData(ReportDetailsViewModel detail)
        {
            return reportingRepository.GetRpmData(detail);
        }
        public IEnumerable<RpmCumulative> GetRpmCumulative(ReportDetailsViewModel detail)
        {
            return reportingRepository.GetRpmCumulative(detail);
        }
        public IEnumerable<ProcessorDispositionOfResidual> GetProcessorDispositionOfResidual()
        {
            return reportingRepository.GetProcessorDispositionOfResidual(DataLoader.DispositionReasonList, DataLoader.MaterialTypeList);
        }
        public IEnumerable<StewardNonFilers> GetStewardNonFilers(DateTime? startDate, DateTime? endDate)
        {
            return reportingRepository.GetStewardNonFilers(startDate, endDate);
        }
        public IEnumerable<TSFRptOnlinePaymentsOutstandingVM> GetTSFOnlinePaymentsOutstanding(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate)
        {
            return reportingRepository.GetTSFOnlinePaymentsOutstanding(model, TSFNegAdjSwitchDate);
        }
        public IEnumerable<CollectorCumulativeReportVM> GetCollectorCumulativeReport(ReportDetailsViewModel model)
        {
            return reportingRepository.GetCollectorCumulativeReport(model);
        }

        public IEnumerable<WebListingStewardReport> GetWebListingStewardReportPostal(int reportId)
        {
            string postalCode = string.Empty;
            return reportingRepository.GetWebListingStewardReportPostal(postalCode);
        }

        public IEnumerable<WebListingProcessorReport> GetWebListingProcessorReportPostal(int reportId)
        {
            string postalCode = string.Empty;
            return reportingRepository.GetWebListingProcessorReportPostal(postalCode);
        }

        public IEnumerable<WebListingRPMReport> GetWebListingRPMReportPostal(int reportId)
        {
            string postalCode = string.Empty;
            return reportingRepository.GetWebListingRPMReportPostal(postalCode);
        }

        public IEnumerable<HaulerTireMovementReport> GetHaulerTireMovementReport(ReportDetailsViewModel model)
        {
            IEnumerable<HaulerTireMovementReport> returnval2 = reportingRepository.GetHaulerTireMovementReport_sp(model);

            return returnval2;
        }

        public IEnumerable<HaulerTireMovementReportVM> GetHaulerTireMovementReportVM(ReportDetailsViewModel model)
        {
            return reportingRepository.GetHaulerTireMovementReportVM(model);
        }
        public IEnumerable<CollectorTireOriginReport> GetCollectorTireOriginReport(ReportDetailsViewModel model)
        {
            return reportingRepository.GetCollectorTireOriginReport(model);
        }
        public IEnumerable<HaulerTireCollectionReport> GetHaulerTireCollectionReport(ReportDetailsViewModel model)
        {
            return reportingRepository.GetHaulerTireCollectionReport(model);
        }

        public List<StaffUserNameAndEmailViewModel> GetUserNameBySearchText(string searchText)
        {
            return reportingRepository.GetUserNameBySearchText(searchText);
        }

        private List<UserPermissionViewModel> GetAllChildrenList(List<UserPermissionViewModel> userPermissionTempList, int displayOrder, bool isLevelZero)
        {
            var childrenList = new List<UserPermissionViewModel>();
            if (isLevelZero)
            {
                var index = displayOrder.ToString().Substring(0, 2);
                childrenList = userPermissionTempList.Where(c => c.DisplayOrder.ToString().Substring(0, 2) == index && c.DisplayOrder > displayOrder).ToList();
            }
            else
            {
                var index = displayOrder.ToString().Substring(0, 4);
                childrenList = userPermissionTempList.Where(c => c.DisplayOrder.ToString().Substring(0, 4) == index && c.DisplayOrder > displayOrder).ToList();
            }
            return childrenList;
        }

        public List<UserPermissionExportViewModel> GetStaffUserRolePermissionMatrix(string userEmail)
        {
            var userPermissionTempList = userRepository.LoadRolePermissionsForUser(userEmail);

            //if it is custom, we need to review all children to re-set value
            userPermissionTempList.ForEach(c =>
            {
                if (c.MaxPermissionLevel == 2)
                {
                    //load all children with level 2  
                    var childrenList = GetAllChildrenList(userPermissionTempList, c.DisplayOrder, c.ResourceLevel == 0);

                    if (!childrenList.Any(i => i.MaxPermissionLevel == 0 || i.MaxPermissionLevel == 3))
                        c.MaxPermissionLevel = 1;
                }
            });

            var userClaimWorkflowTempList = userRepository.LoadClaimsWorkflowForUser(userEmail);
            var userApplicationWorkflowTempList = userRepository.LoadApplicationsWorkflowForUser(userEmail);
            userPermissionTempList.ForEach(c =>
            {
                if (c.Menu == "Claims")
                {
                    if (c.Screen != string.Empty)
                    {
                        switch (c.Screen.Split(' ')[0])
                        {
                            case "Collector":
                                c.Account = "Collector";
                                break;
                            case "Hauler":
                                c.Account = "Hauler";
                                break;
                            case "Processor":
                                c.Account = "Processor";
                                break;
                            case "RPM":
                                c.Account = "RPM";
                                break;
                            case "Claims":
                                c.Account = string.Empty;
                                break;
                        }
                    }
                    else
                    {
                        c.Account = string.Empty;
                    }
                }
                else if (c.Menu == "Remittance")
                {
                    c.Account = "Steward";
                }
                else if (c.Menu == "Applications")
                {
                    switch (c.Screen.Split(' ')[0])
                    {
                        case "Collector":
                            c.Account = "Collector";
                            break;
                        case "Hauler":
                            c.Account = "Hauler";
                            break;
                        case "Processor":
                            c.Account = "Processor";
                            break;
                        case "RPM":
                            c.Account = "RPM";
                            break;
                        case "Steward":
                            c.Account = "Steward";
                            break;
                    }
                }
                else
                {
                    c.Account = string.Empty;
                }
                c.Workflow = string.Empty;
                c.ActionGear = false;
            });

            bool isAllClaimsActionGearChecked = true;
            bool isAnyClaimWorkflowRepresentitive = false;
            userClaimWorkflowTempList.ForEach(c =>
            {
                switch (c.AccountName)
                {
                    case "Collector":
                        var indexOfClaimCollector = userPermissionTempList.FindIndex(u => u.DisplayOrder == 101110);
                        userPermissionTempList.Insert(indexOfClaimCollector, new UserPermissionViewModel
                        {
                            Account = "Collector",
                            Menu = "Claims",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Workflow,
                            ActionGear = c.ActionGear
                        });
                        isAllClaimsActionGearChecked = isAllClaimsActionGearChecked && c.ActionGear;
                        isAnyClaimWorkflowRepresentitive = isAnyClaimWorkflowRepresentitive || c.Workflow == "Representative";
                        break;
                    case "Hauler":
                        var indexOfClaimHauler = userPermissionTempList.FindIndex(u => u.DisplayOrder == 101210);
                        userPermissionTempList.Insert(indexOfClaimHauler, new UserPermissionViewModel
                        {
                            Account = "Hauler",
                            Menu = "Claims",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Workflow,
                            ActionGear = c.ActionGear
                        });
                        isAllClaimsActionGearChecked = isAllClaimsActionGearChecked && c.ActionGear;
                        isAnyClaimWorkflowRepresentitive = isAnyClaimWorkflowRepresentitive || c.Workflow == "Representative";
                        break;
                    case "Processor":
                        var indexOfClaimProcessor = userPermissionTempList.FindIndex(u => u.DisplayOrder == 101310);
                        userPermissionTempList.Insert(indexOfClaimProcessor, new UserPermissionViewModel
                        {
                            Account = "Processor",
                            Menu = "Claims",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Workflow,
                            ActionGear = c.ActionGear
                        });
                        isAllClaimsActionGearChecked = isAllClaimsActionGearChecked && c.ActionGear;
                        isAnyClaimWorkflowRepresentitive = isAnyClaimWorkflowRepresentitive || c.Workflow == "Representative";
                        break;
                    case "RPM":
                        var indexOfClaimRPM = userPermissionTempList.FindIndex(u => u.DisplayOrder == 101410);
                        userPermissionTempList.Insert(indexOfClaimRPM, new UserPermissionViewModel
                        {
                            Account = "RPM",
                            Menu = "Claims",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Workflow,
                            ActionGear = c.ActionGear
                        });
                        isAllClaimsActionGearChecked = isAllClaimsActionGearChecked && c.ActionGear;
                        isAnyClaimWorkflowRepresentitive = isAnyClaimWorkflowRepresentitive || c.Workflow == "Representative";
                        break;
                    case "Steward":
                        var indexOfClaimSteward = userPermissionTempList.FindIndex(u => u.DisplayOrder == 120000);
                        userPermissionTempList.Insert(indexOfClaimSteward, new UserPermissionViewModel
                        {
                            Account = "Steward",
                            Menu = "Remittance",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Workflow,
                            ActionGear = c.ActionGear
                        });
                        break;
                }
            });

            //if (isAllClaimsActionGearChecked)
            //{
            //    userPermissionTempList.FirstOrDefault(c => c.DisplayOrder == 101000).ActionGear = true;
            //}

            if (isAnyClaimWorkflowRepresentitive)
            {
                userPermissionTempList.FirstOrDefault(c => c.DisplayOrder == 101000).Workflow = "X";
            }

            bool isAllApplicationsActionGearChecked = true;
            bool isAnyApplicationRoutingChecked = false;
            userApplicationWorkflowTempList.ForEach(c =>
            {
                switch (c.AccountName)
                {
                    case "Collector":
                        var indexOfClaimCollector = userPermissionTempList.FindIndex(u => u.DisplayOrder == 131200);
                        userPermissionTempList.Insert(indexOfClaimCollector, new UserPermissionViewModel
                        {
                            Account = "Collector",
                            Menu = "Applications",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Routing ? "X" : string.Empty,
                            ActionGear = c.ActionGear
                        });
                        isAllApplicationsActionGearChecked = isAllApplicationsActionGearChecked && c.ActionGear;
                        isAnyApplicationRoutingChecked = isAnyApplicationRoutingChecked || c.Routing;
                        break;
                    case "Hauler":
                        var indexOfClaimHauler = userPermissionTempList.FindIndex(u => u.DisplayOrder == 131300);
                        userPermissionTempList.Insert(indexOfClaimHauler, new UserPermissionViewModel
                        {
                            Account = "Hauler",
                            Menu = "Applications",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Routing ? "X" : string.Empty,
                            ActionGear = c.ActionGear
                        });
                        isAllApplicationsActionGearChecked = isAllApplicationsActionGearChecked && c.ActionGear;
                        isAnyApplicationRoutingChecked = isAnyApplicationRoutingChecked || c.Routing;
                        break;
                    case "Processor":
                        var indexOfClaimProcessor = userPermissionTempList.FindIndex(u => u.DisplayOrder == 131400);
                        userPermissionTempList.Insert(indexOfClaimProcessor, new UserPermissionViewModel
                        {
                            Account = "Processor",
                            Menu = "Applications",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Routing ? "X" : string.Empty,
                            ActionGear = c.ActionGear
                        });
                        isAllApplicationsActionGearChecked = isAllApplicationsActionGearChecked && c.ActionGear;
                        isAnyApplicationRoutingChecked = isAnyApplicationRoutingChecked || c.Routing;
                        break;
                    case "RPM":
                        var indexOfClaimRPM = userPermissionTempList.FindIndex(u => u.DisplayOrder == 131500);
                        userPermissionTempList.Insert(indexOfClaimRPM, new UserPermissionViewModel
                        {
                            Account = "RPM",
                            Menu = "Applications",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Routing ? "X" : string.Empty,
                            ActionGear = c.ActionGear
                        });
                        isAllApplicationsActionGearChecked = isAllApplicationsActionGearChecked && c.ActionGear;
                        isAnyApplicationRoutingChecked = isAnyApplicationRoutingChecked || c.Routing;
                        break;
                    case "Steward":
                        var indexOfClaimSteward = userPermissionTempList.FindIndex(u => u.DisplayOrder == 131100);
                        userPermissionTempList.Insert(indexOfClaimSteward, new UserPermissionViewModel
                        {
                            Account = "Steward",
                            Menu = "Applications",
                            Screen = string.Empty,
                            Panel = string.Empty,
                            Workflow = c.Routing ? "X" : string.Empty,
                            ActionGear = c.ActionGear
                        });
                        isAllApplicationsActionGearChecked = isAllApplicationsActionGearChecked && c.ActionGear;
                        isAnyApplicationRoutingChecked = isAnyApplicationRoutingChecked || c.Routing;
                        break;
                }
            });

            //if (isAllApplicationsActionGearChecked)
            //{
            //    userPermissionTempList.FirstOrDefault(c => c.DisplayOrder == 131010).ActionGear = true;
            //}

            //if (isAnyApplicationRoutingChecked)
            //{
            //    userPermissionTempList.FirstOrDefault(c => c.DisplayOrder == 131010).Workflow = "X";
            //}

            var userPermissionList = new List<UserPermissionExportViewModel>();
            //var userPermission = new UserPermissionExportViewModel();
            userPermissionTempList.ForEach(c =>
            {
                var userPermission = new UserPermissionExportViewModel();
                userPermission.Account = c.Account;
                userPermission.Menu = c.Menu;
                userPermission.Screen = c.Screen;
                userPermission.Panel = c.Panel;
                userPermission.NoAccess = c.DisplayOrder == 0 ? string.Empty : (c.IsNoAccess ? "X" : string.Empty);
                userPermission.ReadOnly = c.IsReadOnly ? "X" : string.Empty;
                userPermission.EditSave = c.IsEditSave ? "X" : string.Empty;
                userPermission.Custom = c.IsCustom ? "X" : string.Empty;
                userPermission.Workflow = c.Workflow;
                userPermission.ActionGear = c.ActionGear ? "X" : string.Empty;

                userPermissionList.Add(userPermission);
            });

            return userPermissionList;
        }
        public IEnumerable<StewardRptRevenueSupplyOldTireCreditVMsp> GetStewardRevenueSupplyOldTireCreditTypes_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            return reportingRepository.GetStewardRevenueSupplyOldTireCreditType_SP(detail, TSFNegAdjSwitchDate);
        }
        public IEnumerable<StewardRptRevenueSupplyOldTireVM> GetStewardRevenueSupplyOldTireTypes(ReportDetailsViewModel detail)
        {
            return reportingRepository.GetStewardRevenueSupplyOldTireTypes(detail);
        }

        #region OTSTM2-215
        public List<CollectionGenerationOfTiresBasedOnFSAReportVM> GetCollectionGenerationOfTiresBasedOnFSAReport(CollectionGenerationOfTiresBasedOnFSAReportSelectionVM selectionModel)
        {
            List<CollectionGenerationOfTiresBasedOnFSAReportFSAVM> FSAList = new List<CollectionGenerationOfTiresBasedOnFSAReportFSAVM>();

            if (selectionModel.IsCentral)
            {
                var tempFSA = new CollectionGenerationOfTiresBasedOnFSAReportFSAVM();
                tempFSA.RegionName = "Southcentral";
                List<string> postalList = new List<string>();
                var tempRegion = DataLoader.Regions.Where(r => r.Name == "Southcentral").FirstOrDefault();
                if (selectionModel.IsZoneZero)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "0").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneOne)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "1").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneTwo)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "2").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneThree)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "3").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFour)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "4").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFive)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "5").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSix)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "6").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSeven)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "7").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneEight)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "8").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneNine)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "9").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                tempFSA.ZoneNameList = postalList;
                FSAList.Add(tempFSA);
            }
            if (selectionModel.IsEastern)
            {
                var tempFSA = new CollectionGenerationOfTiresBasedOnFSAReportFSAVM();
                tempFSA.RegionName = "Southeast";
                List<string> postalList = new List<string>();
                var tempRegion = DataLoader.Regions.Where(r => r.Name == "Southeast").FirstOrDefault();
                if (selectionModel.IsZoneZero)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "0").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneOne)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "1").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneTwo)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "2").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneThree)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "3").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFour)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "4").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFive)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "5").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSix)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "6").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSeven)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "7").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneEight)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "8").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneNine)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "9").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                tempFSA.ZoneNameList = postalList;
                FSAList.Add(tempFSA);
            }
            if (selectionModel.IsNorthern)
            {
                var tempFSA = new CollectionGenerationOfTiresBasedOnFSAReportFSAVM();
                tempFSA.RegionName = "North";
                List<string> postalList = new List<string>();
                var tempRegion = DataLoader.Regions.Where(r => r.Name == "North").FirstOrDefault();
                if (selectionModel.IsZoneZero)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "0").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneOne)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "1").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneTwo)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "2").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneThree)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "3").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFour)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "4").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFive)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "5").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSix)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "6").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSeven)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "7").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneEight)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "8").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneNine)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "9").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                tempFSA.ZoneNameList = postalList;
                FSAList.Add(tempFSA);
            }
            if (selectionModel.IsSouthWestern)
            {
                var tempFSA = new CollectionGenerationOfTiresBasedOnFSAReportFSAVM();
                tempFSA.RegionName = "Southwest";
                List<string> postalList = new List<string>();
                var tempRegion = DataLoader.Regions.Where(r => r.Name == "Southwest").FirstOrDefault();
                if (selectionModel.IsZoneZero)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "0").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneOne)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "1").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneTwo)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "2").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneThree)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "3").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFour)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "4").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFive)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "5").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSix)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "6").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSeven)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "7").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneEight)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "8").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneNine)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "9").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                tempFSA.ZoneNameList = postalList;
                FSAList.Add(tempFSA);
            }
            if (selectionModel.IsGTA)
            {
                var tempFSA = new CollectionGenerationOfTiresBasedOnFSAReportFSAVM();
                tempFSA.RegionName = "GTA";
                List<string> postalList = new List<string>();
                var tempRegion = DataLoader.Regions.Where(r => r.Name == "GTA").FirstOrDefault();
                if (selectionModel.IsZoneZero)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "0").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneOne)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "1").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneTwo)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "2").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneThree)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "3").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFour)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "4").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneFive)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "5").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSix)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "6").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneSeven)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "7").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneEight)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "8").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                if (selectionModel.IsZoneNine)
                {
                    var tempPostalList = tempRegion.RegionDetails.Where(d => d.FSA.ToString().Substring(1, 1) == "9").Select(d => d.FSA.ToString().Substring(0, 2)).ToList().Distinct();
                    postalList.AddRange(tempPostalList);
                }
                tempFSA.ZoneNameList = postalList.ConvertAll(c => c.ToUpper());
                FSAList.Add(tempFSA);
            }

            List<string> allFSAList = new List<string>();
            FSAList.ForEach(c => { allFSAList.AddRange(c.ZoneNameList); }); //put the FSA list for different Region into one list

            return populateResult(FSAList, reportingRepository.GetCollectionGenerationOfTiresBasedOnFSAReport(selectionModel.FromDate, selectionModel.ToDate, allFSAList));
        }

        private List<CollectionGenerationOfTiresBasedOnFSAReportVM> populateResult(List<CollectionGenerationOfTiresBasedOnFSAReportFSAVM> FSAList, List<CollectionGenerationOfTiresFSAProcessing> result)
        {
            var resultList = new List<CollectionGenerationOfTiresBasedOnFSAReportVM>();

            FSAList.ForEach(c =>
            {
                if (c.RegionName == "Southwest")
                {
                    var subtotalResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                    {
                        Region = "SouthWestern",
                        Zone = "",
                        PLT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "PLT" && r.RegionId == 1).Sum(r => r.Quantity),
                        MT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MT" && r.RegionId == 1).Sum(r => r.Quantity),
                        AGLS = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "AGLS" && r.RegionId == 1).Sum(r => r.Quantity),
                        IND = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "IND" && r.RegionId == 1).Sum(r => r.Quantity),
                        SOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "SOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                        MOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                        LOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "LOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                        GOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "GOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                    };
                    resultList.Add(subtotalResult);
                    c.ZoneNameList.ForEach(z =>
                    {
                        var tempResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                        {
                            Region = "SouthWestern",
                            Zone = z,
                            PLT = result.Where(r => r.FromPostalCode == z && r.ShortName == "PLT" && r.RegionId == 1).Sum(r => r.Quantity),
                            MT = result.Where(r => r.FromPostalCode == z && r.ShortName == "MT" && r.RegionId == 1).Sum(r => r.Quantity),
                            AGLS = result.Where(r => r.FromPostalCode == z && r.ShortName == "AGLS" && r.RegionId == 1).Sum(r => r.Quantity),
                            IND = result.Where(r => r.FromPostalCode == z && r.ShortName == "IND" && r.RegionId == 1).Sum(r => r.Quantity),
                            SOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "SOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                            MOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "MOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                            LOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "LOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                            GOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "GOTR" && r.RegionId == 1).Sum(r => r.Quantity),
                        };
                        resultList.Add(tempResult);
                    });
                }

                if (c.RegionName == "Southeast")
                {
                    var subtotalResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                    {
                        Region = "Eastern",
                        Zone = "",
                        PLT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "PLT" && r.RegionId == 2).Sum(r => r.Quantity),
                        MT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MT" && r.RegionId == 2).Sum(r => r.Quantity),
                        AGLS = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "AGLS" && r.RegionId == 2).Sum(r => r.Quantity),
                        IND = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "IND" && r.RegionId == 2).Sum(r => r.Quantity),
                        SOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "SOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                        MOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                        LOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "LOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                        GOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "GOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                    };
                    resultList.Add(subtotalResult);
                    c.ZoneNameList.ForEach(z =>
                    {
                        var tempResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                        {
                            Region = "Eastern",
                            Zone = z,
                            PLT = result.Where(r => r.FromPostalCode == z && r.ShortName == "PLT" && r.RegionId == 2).Sum(r => r.Quantity),
                            MT = result.Where(r => r.FromPostalCode == z && r.ShortName == "MT" && r.RegionId == 2).Sum(r => r.Quantity),
                            AGLS = result.Where(r => r.FromPostalCode == z && r.ShortName == "AGLS" && r.RegionId == 2).Sum(r => r.Quantity),
                            IND = result.Where(r => r.FromPostalCode == z && r.ShortName == "IND" && r.RegionId == 2).Sum(r => r.Quantity),
                            SOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "SOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                            MOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "MOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                            LOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "LOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                            GOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "GOTR" && r.RegionId == 2).Sum(r => r.Quantity),
                        };
                        resultList.Add(tempResult);
                    });
                }

                if (c.RegionName == "Southcentral")
                {
                    var subtotalResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                    {
                        Region = "Central",
                        Zone = "",
                        PLT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "PLT" && r.RegionId == 3).Sum(r => r.Quantity),
                        MT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MT" && r.RegionId == 3).Sum(r => r.Quantity),
                        AGLS = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "AGLS" && r.RegionId == 3).Sum(r => r.Quantity),
                        IND = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "IND" && r.RegionId == 3).Sum(r => r.Quantity),
                        SOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "SOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                        MOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                        LOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "LOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                        GOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "GOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                    };
                    resultList.Add(subtotalResult);
                    c.ZoneNameList.ForEach(z =>
                    {
                        var tempResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                        {
                            Region = "Central",
                            Zone = z,
                            PLT = result.Where(r => r.FromPostalCode == z && r.ShortName == "PLT" && r.RegionId == 3).Sum(r => r.Quantity),
                            MT = result.Where(r => r.FromPostalCode == z && r.ShortName == "MT" && r.RegionId == 3).Sum(r => r.Quantity),
                            AGLS = result.Where(r => r.FromPostalCode == z && r.ShortName == "AGLS" && r.RegionId == 3).Sum(r => r.Quantity),
                            IND = result.Where(r => r.FromPostalCode == z && r.ShortName == "IND" && r.RegionId == 3).Sum(r => r.Quantity),
                            SOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "SOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                            MOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "MOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                            LOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "LOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                            GOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "GOTR" && r.RegionId == 3).Sum(r => r.Quantity),
                        };
                        resultList.Add(tempResult);
                    });
                }

                if (c.RegionName == "North")
                {
                    var subtotalResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                    {
                        Region = "Northern",
                        Zone = "",
                        PLT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "PLT" && r.RegionId == 4).Sum(r => r.Quantity),
                        MT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MT" && r.RegionId == 4).Sum(r => r.Quantity),
                        AGLS = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "AGLS" && r.RegionId == 4).Sum(r => r.Quantity),
                        IND = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "IND" && r.RegionId == 4).Sum(r => r.Quantity),
                        SOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "SOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                        MOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                        LOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "LOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                        GOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "GOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                    };
                    resultList.Add(subtotalResult);
                    c.ZoneNameList.ForEach(z =>
                    {
                        var tempResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                        {
                            Region = "Northern",
                            Zone = z,
                            PLT = result.Where(r => r.FromPostalCode == z && r.ShortName == "PLT" && r.RegionId == 4).Sum(r => r.Quantity),
                            MT = result.Where(r => r.FromPostalCode == z && r.ShortName == "MT" && r.RegionId == 4).Sum(r => r.Quantity),
                            AGLS = result.Where(r => r.FromPostalCode == z && r.ShortName == "AGLS" && r.RegionId == 4).Sum(r => r.Quantity),
                            IND = result.Where(r => r.FromPostalCode == z && r.ShortName == "IND" && r.RegionId == 4).Sum(r => r.Quantity),
                            SOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "SOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                            MOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "MOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                            LOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "LOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                            GOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "GOTR" && r.RegionId == 4).Sum(r => r.Quantity),
                        };
                        resultList.Add(tempResult);
                    });
                }

                if (c.RegionName == "GTA")
                {
                    var subtotalResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                    {
                        Region = "GTA",
                        Zone = "",
                        PLT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "PLT" && r.RegionId == 5).Sum(r => r.Quantity),
                        MT = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MT" && r.RegionId == 5).Sum(r => r.Quantity),
                        AGLS = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "AGLS" && r.RegionId == 5).Sum(r => r.Quantity),
                        IND = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "IND" && r.RegionId == 5).Sum(r => r.Quantity),
                        SOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "SOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                        MOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "MOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                        LOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "LOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                        GOTR = result.Where(r => c.ZoneNameList.Contains(r.FromPostalCode) && r.ShortName == "GOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                    };
                    resultList.Add(subtotalResult);
                    c.ZoneNameList.ForEach(z =>
                    {
                        var tempResult = new CollectionGenerationOfTiresBasedOnFSAReportVM
                        {
                            Region = "GTA",
                            Zone = z,
                            PLT = result.Where(r => r.FromPostalCode == z && r.ShortName == "PLT" && r.RegionId == 5).Sum(r => r.Quantity),
                            MT = result.Where(r => r.FromPostalCode == z && r.ShortName == "MT" && r.RegionId == 5).Sum(r => r.Quantity),
                            AGLS = result.Where(r => r.FromPostalCode == z && r.ShortName == "AGLS" && r.RegionId == 5).Sum(r => r.Quantity),
                            IND = result.Where(r => r.FromPostalCode == z && r.ShortName == "IND" && r.RegionId == 5).Sum(r => r.Quantity),
                            SOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "SOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                            MOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "MOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                            LOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "LOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                            GOTR = result.Where(r => r.FromPostalCode == z && r.ShortName == "GOTR" && r.RegionId == 5).Sum(r => r.Quantity),
                        };
                        resultList.Add(tempResult);
                    });
                }
            });

            return resultList;

        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string region, string zone, string fromDate, string toDate)
        {
            List<string> postalList = new List<string>();
            switch (region)
            {
                case "SouthWestern":
                    postalList = DataLoader.Regions.Where(r => r.Name == "Southwest").FirstOrDefault().RegionDetails.Where(d => d.FSA.Contains(zone)).Select(d => d.FSA).ToList();
                    break;
                case "Eastern":
                    postalList = DataLoader.Regions.Where(r => r.Name == "Southeast").FirstOrDefault().RegionDetails.Where(d => d.FSA.Contains(zone)).Select(d => d.FSA).ToList();
                    break;
                case "Central":
                    postalList = DataLoader.Regions.Where(r => r.Name == "Southcentral").FirstOrDefault().RegionDetails.Where(d => d.FSA.Contains(zone)).Select(d => d.FSA).ToList();
                    break;
                case "Northern":
                    postalList = DataLoader.Regions.Where(r => r.Name == "North").FirstOrDefault().RegionDetails.Where(d => d.FSA.Contains(zone)).Select(d => d.FSA).ToList();
                    break;
                case "GTA":
                    postalList = DataLoader.Regions.Where(r => r.Name == "GTA").FirstOrDefault().RegionDetails.Where(d => d.FSA.Contains(zone)).Select(d => d.FSA).ToList();
                    break;
                default:
                    break;
            }
            postalList = postalList.ConvertAll(c => c.ToUpper());
            return reportingRepository.GetReportDetailHandler(pageIndex, pageSize, searchText, orderBy, sortDirection, postalList, fromDate, toDate);
        }

        #endregion

        #region Metric
        public MetricVM LoadMetricDetailsByID(MetricParams paramModel)
        {
            var result = reportingRepository.LoadMetricDetailsByID(paramModel);
            return result;
        }
        #endregion
        #region OTSTM2-1226
        public List<CollectionGenerationOfTiresBasedOnRateGroupVM> GetCollectionGenerationOfTiresBasedOnRateGroup(CollectionGenerationOfTiresBasedOnRateGroupSelectionVM selectionModel)
        {
            if (selectionModel.FromDate == null || selectionModel.ToDate == null)
            {
                List<CollectionGenerationOfTiresBasedOnRateGroupVM> result = new List<CollectionGenerationOfTiresBasedOnRateGroupVM>();
                return result;
            }


            DateTime tmp;
            DateTime firstDayOfStartDate = DateTime.Now, lastDatOfEndDate = DateTime.Now;
            if (DateTime.TryParse(selectionModel.FromDate.ToString(), out tmp))
            {
                firstDayOfStartDate = new DateTime(tmp.Year, tmp.Month, 1);
            }
            if (DateTime.TryParse(selectionModel.ToDate.ToString(), out tmp))
            {
                lastDatOfEndDate = new DateTime(tmp.Year, tmp.Month, 1).AddMonths(1).AddSeconds(-1);
            }

            //Get available RateTransactions based on Datetime selection
            var startRateTransaction = reportingRepository.GetStartRateTransaction(firstDayOfStartDate);
            var endRateTransaction = reportingRepository.GetEndRateTransaction(lastDatOfEndDate);
            var firstRateTransaction = reportingRepository.GetFirstRateTransaction();
            var rateTransactionList = new List<RateTransaction>();
            if (firstRateTransaction.EffectiveStartDate <= lastDatOfEndDate)
            {
                if (startRateTransaction != null && endRateTransaction != null)
                {
                    rateTransactionList = reportingRepository.GetRateTransactionList(startRateTransaction, endRateTransaction);
                }
                else
                {
                    rateTransactionList = reportingRepository.GetRateTransactionList(null, endRateTransaction);
                }
            }
            else //OTSTM2-1265, enhance the code, enable user to select any date period before introducing Rate Group and Group (for example 2017-xx-xx to 2017-xx-xx) on UI
            {
                List<CollectionGenerationOfTiresBasedOnRateGroupVM> result = new List<CollectionGenerationOfTiresBasedOnRateGroupVM>();
                return result;
            }

            var rateGroupVendorGroupList = new List<CollectionGenerationOfTiresRateGroupVM>();
            rateTransactionList.ForEach(r =>
            {
                var rateGroupVendorGroup = new CollectionGenerationOfTiresRateGroupVM();
                var VendorRateGroupList = reportingRepository.GetVendorRateGroupList(r.PickupRateGroupId.Value);
                rateGroupVendorGroup.RateGroupId = r.PickupRateGroupId.Value;
                rateGroupVendorGroup.RateGroupName = VendorRateGroupList.FirstOrDefault(c => c.RateGroupId == r.PickupRateGroupId).RateGroup.RateGroupName;
                rateGroupVendorGroup.EffectiveStartDate = r.EffectiveStartDate;
                rateGroupVendorGroup.EffectiveEndDate = r.EffectiveEndDate;
                rateGroupVendorGroup.VendorGroups = new List<CollectionGenerationOfTiresVendorGroupVM>();
                var VendorGroupIdList = VendorRateGroupList.Where(c => c.RateGroupId == r.PickupRateGroupId).Select(c => c.GroupId).Distinct().ToList();
                VendorGroupIdList.ForEach(g =>
                {
                    var VendorGroupDetails = new CollectionGenerationOfTiresVendorGroupVM();
                    VendorGroupDetails.GroupName = VendorRateGroupList.FirstOrDefault(c => c.GroupId == g).Group.GroupName;
                    VendorGroupDetails.GroupId = VendorRateGroupList.FirstOrDefault(c => c.GroupId == g).GroupId;
                    VendorGroupDetails.VendorIds = VendorRateGroupList.Where(c => c.GroupId == g).Select(c => c.VendorId.Value).ToList(); //vendorId list may not needed
                    rateGroupVendorGroup.VendorGroups.Add(VendorGroupDetails);
                });
                rateGroupVendorGroupList.Add(rateGroupVendorGroup);
            });

            var RateGroupIdList = new List<int>();
            rateGroupVendorGroupList.ForEach(c => { RateGroupIdList.Add(c.RateGroupId); });

            return reportingRepository.GetCollectorFSAReport(selectionModel.FromDate.Value, selectionModel.ToDate.Value, String.Join(",", RateGroupIdList));
        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailByRateGroupHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string pickupRateGroup, string collectorGroup, string fromDate, string toDate)
        {
            var vendorIdList = reportingRepository.GetVendorIdList(pickupRateGroup, collectorGroup);

            return reportingRepository.GetReportDetailByRateGroupHandler(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorIdList, fromDate, toDate);
        }
        #endregion
    }
}
