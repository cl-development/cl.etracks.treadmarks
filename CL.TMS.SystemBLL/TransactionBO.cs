﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Configuration;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using Transaction = CL.TMS.DataContracts.DomainEntities.Transaction;
using MobileTransaction = CL.TMS.DataContracts.Mobile.Transaction;
using TransactionStatus = CL.TMS.Common.Enum.TransactionStatus;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.Registrant;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.ReverseGeocode;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using System.Text.RegularExpressions;
using CL.TMS.Common.Converters;
using CL.TMS.Common.Extension;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;
using System.IO;
using System.Globalization;
using System.Data.SqlTypes;
using CL.Framework.BLL;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.Framework.RuleEngine;
using CL.TMS.RuleEngine.TransactionBusinessRules.BusinessRules;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.RuleEngine.TransactionBusinessRules;
using CL.Framework.Common;
using CL.TMS.DataContracts.ViewModel.RPM;
using CsvHelper;
using CL.TMS.ClaimCalculator;
using CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels;
using CL.TMS.Security;
using CL.TMS.DataContracts.Communication;

namespace CL.TMS.SystemBLL
{
    public class TransactionBO : BaseBO
    {
        private ITransactionRepository transactionRepository;
        private IMobileTransactionRepository mobileTransactionRepository;
        private IClaimsRepository claimsRepository;
        private IUserRepository userRepository;
        private IVendorRepository vendorRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository;
        private IGpRepository gpRepository;
        private ISettingRepository settingRepository;
        private IConfigurationsRepository configurationsRepository;

        public TransactionBO(ITransactionRepository transactionRepository, IMobileTransactionRepository mobileTransactionRepository, IClaimsRepository claimsRepository, IUserRepository userRepository, IEventAggregator eventAggregator, IVendorRepository vendorRepository, ISettingRepository settingRepository, IConfigurationsRepository configurationsRepository)
        {
            this.transactionRepository = transactionRepository;
            this.mobileTransactionRepository = mobileTransactionRepository;
            this.claimsRepository = claimsRepository;
            this.userRepository = userRepository;
            this.vendorRepository = vendorRepository;
            this.eventAggregator = eventAggregator;
            this.settingRepository = settingRepository;
            this.configurationsRepository = configurationsRepository;
        }


        #region Add Paper Form Transaction
        public TransactionCreationNotificationDetailViewModel AddHaulerPapeFormTransaction(IHaulerTransactionViewModel transactionVM)
        {
            Func<IHaulerTransactionViewModel, Transaction> transDataAdaperFunc = (vm) =>
            {
                var now = DateTime.UtcNow;

                #region Common
                var localTransactionVM = vm as HaulerTransactionViewModel;

                // Trasaction
                var transaction = preparePaperFormTransactionForAddTransaction(
                    localTransactionVM.TransactionId,
                    localTransactionVM.FormType,
                    localTransactionVM.FormDate.On.Value.Date,
                    localTransactionVM.FormNumber,
                    localTransactionVM.Inbound,
                    localTransactionVM.Outbound,
                    now
                );

                // Supporting Document      
                PrepareTransactionSupportingDocumentForAddTransaction(transaction, localTransactionVM.SupportingDocumentList);

                // Supporting Document Attachment
                PrepareTransactionAttachmentForAddTransaction(transaction, localTransactionVM.UploadDocumentList);

                // Transaction Item
                PrepareTransactionItemForAddTransaction(transaction, localTransactionVM.TireTypeList, localTransactionVM.FormDate.On.Value);
                #endregion

                #region TCR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.TCR)
                {
                    var specificTransactionVM = vm as HaulerTCRTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.IsGenerateTires = specificTransactionVM.GenerateTires;
                    transaction.IsEligible = !transaction.IsGenerateTires;
                }
                #endregion

                #region DOT Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.DOT)
                {
                    var specificTransactionVM = vm as HaulerDOTTransactionViewModel;

                    //OTSTM2-91 Scale Ticket validation
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.DOT);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.IsGenerateTires = specificTransactionVM.GenerateTires;
                    transaction.IsEligible = !transaction.IsGenerateTires;

                    // Scale Ticket
                    if (!string.IsNullOrEmpty(specificTransactionVM.ScaleTicket.TicketNumber))
                    {
                        var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                        transaction.ScaleTickets.Add(scaleTicket);

                        //Override transaction item actual weight
                        if (scaleTicket.InboundWeight != null) DistributeScaleWeight(transaction, scaleTicket.InboundWeight.Value);
                    }
                }
                #endregion

                #region STC Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.STC)
                {
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;

                    var specificTransactionVM = vm as HaulerSTCTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.CompanyInfo.PostalCode);
                    transaction.EventNumber = specificTransactionVM.EventNumber;
                    transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                    transaction.FromPostalCode = transaction.CompanyInfo.Address.PostalCode;//fix for claim calculation trigger
                }
                #endregion

                #region UCR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.UCR)
                {
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;
                    transaction.IsEligible = false;

                    var specificTransactionVM = vm as HaulerUCRTransactionViewModel;

                    transaction.PaymentEligible = int.Parse(specificTransactionVM.PaymentEligible);
                    if (transaction.PaymentEligible == 4) transaction.PaymentEligibleOther = specificTransactionVM.PaymentEligibleOther;

                    // Unregistered Collector
                    if (specificTransactionVM.PickedUpFrom.ToLower() == "unregistered")
                    {
                        transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                        //transaction.FromPostalCode = transaction.CompanyInfo.Address.PostalCode;
                        transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.CompanyInfo.PostalCode);
                    }
                    else
                    {
                        var vendor = vendorRepository.GetVendorByNumber(specificTransactionVM.CollectorNumber.Value.ToString());

                        transaction.VendorInfoId = vendor.VendorId;
                        transaction.OutgoingId = vendor.VendorId; //OTSTM2-215
                        transaction.FromPostalCode = vendor.Address.PostalCode; //OTSTM2-215
                        transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(vendor.Address.PostalCode);
                    }
                }
                #endregion

                #region HIT Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.HIT)
                {
                    var specificTransactionVM = vm as HaulerHITTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.IsEligible = false;
                }
                #endregion

                #region RTR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.RTR)
                {
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;
                    transaction.IsEligible = false;

                    var specificTransactionVM = vm as HaulerRTRTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.CompanyInfo.PostalCode);

                    transaction.MarketLocation = specificTransactionVM.TireMarketInfo.Location == "Other" ? specificTransactionVM.TireMarketInfo.LocationOther : specificTransactionVM.TireMarketInfo.Location;
                    transaction.TireMarketType = specificTransactionVM.TireMarketInfo.Type;
                    transaction.BillofLoading = specificTransactionVM.TireMarketInfo.Location == "USA" ? specificTransactionVM.TireMarketInfo.BillofLoading : null;
                    transaction.InvoiceNumber = specificTransactionVM.InvoiceNumber;
                    transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                    //transaction.ToPostalCode = transaction.CompanyInfo.Address.PostalCode;
                }
                #endregion

                #region PTR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.PTR)
                {
                    var specificTransactionVM = vm as HaulerPTRTransactionViewModel;

                    //OTSTM2-91 Scale Ticket validation
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.PTR);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();

                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Inbound.Address.PostalCode);

                    // Scale Ticket
                    var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                    transaction.ScaleTickets.Add(scaleTicket);

                    //Override transaction item actual weight
                    if (scaleTicket.InboundWeight != null) DistributeScaleWeight(transaction, scaleTicket.InboundWeight.Value);
                }
                #endregion

                return transaction;
            };

            return AddHaulerTransaction(transactionVM, transDataAdaperFunc);
        }

        public TransactionCreationNotificationDetailViewModel AddCollectorPapeFormTransaction(ICollectorTransactionViewModel transactionVM)
        {
            Func<ICollectorTransactionViewModel, Transaction> transDataAdaperFunc = (vm) =>
            {
                var now = DateTime.UtcNow;

                #region Common
                var localTransactionVM = vm as CollectorTransactionViewModel;

                // Trasaction
                var transaction = preparePaperFormTransactionForAddTransaction(
                    localTransactionVM.TransactionId,
                    localTransactionVM.FormType,
                    localTransactionVM.FormDate.On.Value.Date,
                    localTransactionVM.FormNumber,
                    localTransactionVM.Inbound,
                    localTransactionVM.Outbound,
                    now
                );

                // Supporting Document      
                PrepareTransactionSupportingDocumentForAddTransaction(transaction, localTransactionVM.SupportingDocumentList);

                // Supporting Document Attachment
                PrepareTransactionAttachmentForAddTransaction(transaction, localTransactionVM.UploadDocumentList);

                // Transaction Item
                PrepareTransactionItemForAddTransaction(transaction, localTransactionVM.TireTypeList, localTransactionVM.FormDate.On.Value);
                #endregion

                #region TCR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.TCR)
                {
                    var specificTransactionVM = vm as CollectorTCRTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.IsGenerateTires = specificTransactionVM.GenerateTires;
                    transaction.IsEligible = !transaction.IsGenerateTires;
                }
                #endregion

                #region DOT Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.DOT)
                {
                    var specificTransactionVM = vm as CollectorDOTTransactionViewModel;

                    //OTSTM2-91 Scale Ticket validation
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.DOT);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.IsGenerateTires = specificTransactionVM.GenerateTires;
                    transaction.IsEligible = !transaction.IsGenerateTires;

                    // Scale Ticket
                    if (!string.IsNullOrEmpty(specificTransactionVM.ScaleTicket.TicketNumber))
                    {
                        var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                        transaction.ScaleTickets.Add(scaleTicket);

                        //Override transaction item actual weight
                        if (scaleTicket.InboundWeight != null) DistributeScaleWeight(transaction, scaleTicket.InboundWeight.Value);
                    }
                }
                #endregion

                return transaction;
            };

            return AddCollectorTransaction(transactionVM, transDataAdaperFunc);
        }

        public TransactionCreationNotificationDetailViewModel AddProcessorPapeFormTransaction(IProcessorTransactionViewModel transactionVM)
        {
            Func<IProcessorTransactionViewModel, Transaction> transDataAdaperFunc = (vm) =>
            {
                var now = DateTime.UtcNow;

                #region Common
                var localTransactionVM = vm as ProcessorTransactionViewModel;

                // Trasaction
                var transaction = preparePaperFormTransactionForAddTransaction(
                    localTransactionVM.TransactionId,
                    localTransactionVM.FormType,
                    localTransactionVM.FormDate.On.Value.Date,
                    localTransactionVM.FormNumber,
                    localTransactionVM.Inbound,
                    localTransactionVM.Outbound,
                    now
                );

                // Supporting Document      
                PrepareTransactionSupportingDocumentForAddTransaction(transaction, localTransactionVM.SupportingDocumentList);

                // Supporting Document Attachment
                PrepareTransactionAttachmentForAddTransaction(transaction, localTransactionVM.UploadDocumentList);
                #endregion

                #region PTR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.PTR)
                {
                    // Transaction Item (Tire Type transacttion items)                    
                    PrepareTransactionItemForAddTransaction(transaction, localTransactionVM.TireTypeList, localTransactionVM.FormDate.On.Value);

                    var specificTransactionVM = vm as ProcessorPTRTransactionViewModel;

                    //------------------OTSTM2-91 Scale Ticket validation--------------------
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.PTR);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }
                    //---------------------------------------------------------------------


                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Inbound.Address.PostalCode);

                    // Scale Ticket
                    var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                    transaction.ScaleTickets.Add(scaleTicket);

                    //Override transaction item actual weight
                    if (scaleTicket.InboundWeight != null) DistributeScaleWeight(transaction, scaleTicket.InboundWeight.Value);
                }
                #endregion

                #region PIT Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.PIT)
                {
                    var specificTransactionVM = vm as ProcessorPITTransactionViewModel;

                    //------------------OTSTM2-91 Scale Ticket validation--------------------
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.PIT);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }
                    //---------------------------------------------------------------------

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);

                    // Scale Ticket
                    var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                    transaction.ScaleTickets.Add(scaleTicket);

                    // Product Transaction Item
                    transaction.TransactionItems.Add(PrepareProductTransactionItem(specificTransactionVM.ProductDetail, scaleTicket.InboundWeight, localTransactionVM.FormType));
                }
                #endregion

                #region SPS Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.SPS)
                {
                    var specificTransactionVM = vm as ProcessorSPSTransactionViewModel;

                    //------------------OTSTM2-91 Scale Ticket validation--------------------
                    var previousScaleTicket = new PreviousScaleTicketModel();
                    previousScaleTicket.FormType = specificTransactionVM.FormType;
                    previousScaleTicket.Inbound = specificTransactionVM.Inbound;
                    previousScaleTicket.Outbound = specificTransactionVM.Outbound;
                    previousScaleTicket.ScaleTicket = specificTransactionVM.ScaleTicket.TicketNumber;

                    List<string> result = ProcessAddPaperPrevScaleTicket(previousScaleTicket, TreadMarksConstants.SPS);

                    if (result != null && result.Count > 0)
                    {
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError = new List<string>();
                        specificTransactionVM.CommonBusinessRulesContainer.PreviousScaleTicketError.AddRange(result);
                    }
                    //---------------------------------------------------------------------

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.InvoiceNumber = specificTransactionVM.InvoiceNumber;

                    // Sold To Unregistered RPM
                    if (specificTransactionVM.SoldTo.ToLower() == "unregistered")
                    {
                        transaction.Status = TransactionStatus.Completed.ToString();
                        transaction.IsSingle = true;
                        transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                        //transaction.ToPostalCode = transaction.CompanyInfo.Address.PostalCode;
                    }

                    // Scale Ticket
                    var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                    transaction.ScaleTickets.Add(scaleTicket);

                    // Product Transaction Item
                    transaction.TransactionItems.Add(PrepareProductTransactionItem(specificTransactionVM.ProductDetail, scaleTicket.InboundWeight, localTransactionVM.FormType));
                }
                #endregion

                #region DOR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.DOR)
                {
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;

                    var specificTransactionVM = vm as ProcessorDORTransactionViewModel;

                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.Outbound.Address.PostalCode);
                    transaction.MaterialType = specificTransactionVM.ProductDetail.MaterialTypeId;
                    transaction.DispositionReason = specificTransactionVM.ProductDetail.DispositionReasonId;
                    transaction.InvoiceNumber = specificTransactionVM.InvoiceNumber;

                    // Transaction Item (Tire Type transacttion items) for only if Material Type is Used Tire Sale
                    if (specificTransactionVM.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue)
                    {
                        PrepareTransactionItemForAddTransaction(transaction, localTransactionVM.TireTypeList, localTransactionVM.FormDate.On.Value);

                        var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                        transaction.ScaleTickets.Add(scaleTicket);

                        //Override transaction item actual weight
                        if (scaleTicket.InboundWeight != null) DistributeScaleWeight(transaction, scaleTicket.InboundWeight.Value);
                    }

                    if (specificTransactionVM.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Trash").DefinitionValue ||
                        specificTransactionVM.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Steel").DefinitionValue ||
                        specificTransactionVM.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Fibre").DefinitionValue)
                    {
                        // Scale Ticket
                        var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(specificTransactionVM.ScaleTicket, ScaleTicketTypeEnum.Both, now);
                        transaction.ScaleTickets.Add(scaleTicket);
                    }

                    if (specificTransactionVM.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
                    {
                        transaction.PTRNumber = specificTransactionVM.ProductDetail.PTRNumber;

                        if (specificTransactionVM.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Kg) transaction.OnRoadWeight = Convert.ToDecimal(specificTransactionVM.ProductDetail.OnRoadScaleWeight.Weight.Value);
                        if (specificTransactionVM.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Ton) transaction.OnRoadWeight = DataConversion.ConvertTonToKg(specificTransactionVM.ProductDetail.OnRoadScaleWeight.Weight.Value);
                        if (specificTransactionVM.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Lb) transaction.OnRoadWeight = DataConversion.ConvertLbToKg(specificTransactionVM.ProductDetail.OnRoadScaleWeight.Weight.Value);

                        if (specificTransactionVM.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Kg) transaction.OffRoadWeight = Convert.ToDecimal(specificTransactionVM.ProductDetail.OffRoadScaleWeight.Weight.Value);
                        if (specificTransactionVM.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Ton) transaction.OffRoadWeight = DataConversion.ConvertTonToKg(specificTransactionVM.ProductDetail.OffRoadScaleWeight.Weight.Value);
                        if (specificTransactionVM.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Lb) transaction.OffRoadWeight = DataConversion.ConvertLbToKg(specificTransactionVM.ProductDetail.OffRoadScaleWeight.Weight.Value);
                    }

                    if (specificTransactionVM.SoldTo.ToLower() == "unregistered") // Sold To Unregistered Participant 
                    {
                        transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                        //transaction.ToPostalCode = transaction.CompanyInfo.Address.PostalCode;
                    }
                    else // Sold To Registered Participant
                    {
                        var vendor = vendorRepository.GetVendorByNumber(specificTransactionVM.RegNumber.Value.ToString());

                        transaction.VendorInfoId = vendor.VendorId;
                        transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(vendor.Address.PostalCode);
                    }

                }
                #endregion

                return transaction;
            };

            return AddProcessorTransaction(transactionVM, transDataAdaperFunc);
        }

        //OTSTM2-81
        public int ImportSPSTransactions(StreamReader InputStream, int claimId)
        {
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            var RN = SecurityContextHelper.CurrentVendor.RegistrationNumber;  //OTSTM2-421
            List<Transaction> transactionList = new List<Transaction>();

            //OTSTM2-531
            List<RPMErrorSpsImportModel> errorList = new List<RPMErrorSpsImportModel>();
            int row = 0;
            bool hasErrorData = false;

            int count = 0;

            var reader = new CsvReader(InputStream);
            IEnumerable<RPMImportDataRecordModel> records = reader.GetRecords<RPMImportDataRecordModel>();

            foreach (RPMImportDataRecordModel record in records)
            {
                if (string.IsNullOrWhiteSpace(record.RegistrationNumber) && string.IsNullOrWhiteSpace(record.InvoiceDate) && string.IsNullOrWhiteSpace(record.InvoiceNumber) && string.IsNullOrWhiteSpace(record.Percentage)
                    && string.IsNullOrWhiteSpace(record.TotalTDPWeight) && string.IsNullOrWhiteSpace(record.ProductType) && string.IsNullOrWhiteSpace(record.ProductCode) && string.IsNullOrWhiteSpace(record.ProductSoldDescription)
                    && string.IsNullOrWhiteSpace(record.SoldToBusinessName) && string.IsNullOrWhiteSpace(record.Phone) && string.IsNullOrWhiteSpace(record.Address1) && string.IsNullOrWhiteSpace(record.Address2)
                    && string.IsNullOrWhiteSpace(record.City) && string.IsNullOrWhiteSpace(record.ProvinceState) && string.IsNullOrWhiteSpace(record.PostalZip) && string.IsNullOrWhiteSpace(record.Country))
                {
                    continue;
                }
                row++;

                //Validate Column A: "RegistrationNumber"
                if (string.IsNullOrWhiteSpace(record.RegistrationNumber) || record.RegistrationNumber.Trim() != RN)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "A", Row = row });
                    hasErrorData = true;
                }

                //Validate Column B: "InvoiceDate"
                DateTime invoiceDate = DateTime.Now;
                if (string.IsNullOrWhiteSpace(record.InvoiceDate))
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "B", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    if (!DateTime.TryParseExact(record.InvoiceDate.Trim(), "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out invoiceDate))
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "B", Row = row });
                        hasErrorData = true;
                    }
                    else if (invoiceDate < claimPeriod.StartDate || invoiceDate > claimPeriod.EndDate)
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "B", Row = row });
                        hasErrorData = true;
                    }
                }

                //Validate Column C: "InvoiceNumber"
                string invoiceNumber = string.Empty;
                if (string.IsNullOrWhiteSpace(record.InvoiceNumber) || record.InvoiceNumber.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "C", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    invoiceNumber = record.InvoiceNumber.Trim();
                }

                //Validate Column D: "Percentage"
                decimal percentage = 0;
                if (string.IsNullOrWhiteSpace(record.Percentage))
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "D", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    if (!decimal.TryParse(record.Percentage.Trim(), out percentage))
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "D", Row = row });
                        hasErrorData = true;
                    }
                    else
                    {
                        SqlDecimal sqlDecimal = new SqlDecimal(percentage);
                        int precision = sqlDecimal.Precision;
                        int scale = sqlDecimal.Scale;
                        if (precision > 5 || scale > 2 || percentage > 100 || percentage <= 0)
                        {
                            errorList.Add(new RPMErrorSpsImportModel { Column = "D", Row = row });
                            hasErrorData = true;
                        }
                    }
                }

                //Validate Column E: "TotalTDPWeight"
                decimal totalTDPWeight = 0;
                if (string.IsNullOrWhiteSpace(record.TotalTDPWeight))
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "E", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    if (!decimal.TryParse(record.TotalTDPWeight.Trim(), out totalTDPWeight))
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "E", Row = row });
                        hasErrorData = true;
                    }
                    else
                    {
                        SqlDecimal sqlDecimal = new SqlDecimal(totalTDPWeight);
                        int precision = sqlDecimal.Precision;
                        int scale = sqlDecimal.Scale;
                        if (precision > 18 || scale > 4 || totalTDPWeight <= 0)
                        {
                            errorList.Add(new RPMErrorSpsImportModel { Column = "E", Row = row });
                            hasErrorData = true;
                        }
                    }
                }

                //Validate Column F: "ProductType"
                int productTypeId = 0;
                if (string.IsNullOrWhiteSpace(record.ProductType))
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "F", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    if (record.ProductType.Trim().ToLower() != "calendared" && record.ProductType.Trim().ToLower() != "extruded" && record.ProductType.Trim().ToLower() != "molded products")
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "F", Row = row });
                        hasErrorData = true;
                    }

                    if (record.ProductType.Trim().ToLower() == "calendared")
                    {
                        productTypeId = 101;
                    }
                    else if (record.ProductType.Trim().ToLower() == "extruded")
                    {
                        productTypeId = 102;
                    }
                    else
                    {
                        productTypeId = 103;
                    }
                }

                //Validate Column G: "ProductCode"
                string productCode = string.Empty;
                if (string.IsNullOrWhiteSpace(record.ProductCode) || record.ProductCode.Trim().Length > 26)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "G", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    productCode = record.ProductCode.Trim();
                }

                //Validate Column H: "ProductSoldDescription"
                string productSoldDescription = string.Empty;
                if (string.IsNullOrWhiteSpace(record.ProductSoldDescription) || record.ProductSoldDescription.Trim().Length > 4000)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "H", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    productSoldDescription = record.ProductSoldDescription.Trim();
                }

                //Validate Column I: "SoldToBusinessName"
                string soldToBusinessName = string.Empty;
                if (string.IsNullOrWhiteSpace(record.SoldToBusinessName) || record.SoldToBusinessName.Trim().Length > 255)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "I", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    soldToBusinessName = record.SoldToBusinessName.Trim();
                }

                //Validate Column P: "Country", validate country firstly because it's useful for the validation of phone and postcode
                string country = string.Empty;
                if (string.IsNullOrWhiteSpace(record.Country) || record.Country.Trim().Length > 100)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "P", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    country = record.Country.Trim();
                    List<string> countryList = DataLoader.CountryNames; //OTSTM2-1016
                    if (!countryList.Contains(country))
                    {
                        errorList.Add(new RPMErrorSpsImportModel { Column = "P", Row = row });
                        hasErrorData = true;
                    }
                }

                //Validate Column J: "Phone"
                string phone = string.Empty;
                if (string.IsNullOrWhiteSpace(record.Phone) || record.Phone.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "J", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    phone = record.Phone.Trim();
                    if (country.Trim().ToLower() == "canada" || country.Trim().ToLower() == "united states of america (the)")
                    {
                        string regexPhoneNumber = @"^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$";
                        if (!Regex.Match(phone, regexPhoneNumber).Success)
                        {
                            errorList.Add(new RPMErrorSpsImportModel { Column = "J", Row = row });
                            hasErrorData = true;
                        }
                    }
                }

                //Validate Column K: "Address1"
                string address1 = string.Empty;
                if (string.IsNullOrWhiteSpace(record.Address1) || record.Address1.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "K", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    address1 = record.Address1.Trim();
                }

                //Validate Column L: "Address2"
                string address2 = string.Empty;
                if (record.Address2.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "L", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    address2 = record.Address2.Trim();
                }

                //Validate Column M: "City"
                string city = string.Empty;
                if (string.IsNullOrWhiteSpace(record.City) || record.City.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "M", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    city = record.City.Trim();
                }

                //Validate Column N: "ProvinceState"
                string provinceState = string.Empty;
                if (string.IsNullOrWhiteSpace(record.ProvinceState) || record.ProvinceState.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "N", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    provinceState = record.ProvinceState.Trim();
                }

                //Validate Column O: "PostalZip"
                string postalZip = string.Empty;
                if (string.IsNullOrWhiteSpace(record.PostalZip) || record.PostalZip.Trim().Length > 50)
                {
                    errorList.Add(new RPMErrorSpsImportModel { Column = "O", Row = row });
                    hasErrorData = true;
                }
                else
                {
                    postalZip = record.PostalZip.Trim();
                    if (country.Trim().ToLower() == "canada")
                    {
                        string regexPostalZip = @"^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]$";
                        if (!Regex.Match(postalZip, regexPostalZip).Success)
                        {
                            errorList.Add(new RPMErrorSpsImportModel { Column = "O", Row = row });
                            hasErrorData = true;
                        }
                    }
                }

                if (hasErrorData)
                {
                    continue;
                }
                else
                {
                    Address address = this.IncomingAddress(address1, address2, city, provinceState, country, postalZip);

                    Transaction transaction = this.Transaction(invoiceNumber, totalTDPWeight, productTypeId, invoiceDate, productCode, productSoldDescription, percentage, claimPeriod, address, soldToBusinessName, phone);

                    //other transaction information
                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(postalZip);
                    transaction.TransactionId = Guid.NewGuid();
                    transaction.TransactionSupportingDocuments.Add(new TransactionSupportingDocument() { DocumentName = "Invoice", DocumentFormat = 1 });
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;

                    transaction.FromPostalCode = SecurityContextHelper.CurrentVendor.Address.PostalCode;
                    transaction.OutgoingId = SecurityContextHelper.CurrentVendor.VendorId;
                    transaction.OutgoingAddressId = SecurityContextHelper.CurrentVendor.Address.ID;

                    transactionList.Add(transaction);
                    count++;
                }
            }

            if (hasErrorData)
            {
                Exception e = new Exception("data error");
                e.Data.Add("Data_Error_List", errorList);
                throw e;
            }
            else
            {
                var friendlyIdStart = transactionRepository.GetNextAutoPaperFormFriendlyID(90000000, new string[] { TreadMarksConstants.SPS, TreadMarksConstants.SPSR, TreadMarksConstants.DOR });
                foreach (var transaction in transactionList)
                {
                    transaction.FriendlyId = friendlyIdStart;
                    friendlyIdStart++;
                }

                transactionRepository.AddTransactionRange(transactionList);

                List<ClaimDetail> outClaimDetailList = new List<ClaimDetail>();
                foreach (var transaction in transactionList)
                {
                    var outClaimDetail = new ClaimDetail
                    {
                        ClaimId = claimId,
                        TransactionId = transaction.ID,
                        Direction = false
                    };
                    outClaimDetailList.Add(outClaimDetail);
                }
                var claimBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);
                claimBO.AddClaimDetailsRange(outClaimDetailList);

                //Triger claim calculation
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);

                //Triger claim inventory report calculation
                var claimReportPayload = new ClaimReportPayload { ClaimId = claimId, TransactionId = 0 };
                eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

                return count;
            }
        }

        private Address IncomingAddress(string address1, string address2, string city, string provinceState, string country, string postalZipCode)
        {
            Address address = new Address();
            address.AddressType = 1;
            address.Address1 = address1;
            address.Address2 = address2;
            address.City = city;
            address.Province = provinceState;
            address.Country = country;
            address.PostalCode = Regex.Replace(postalZipCode.ToUpper(), @"\s+", "");

            return address;
        }

        private Transaction Transaction(string invoiceNumber, decimal totalTDPWeight, int productTypeId, DateTime invoiceDate, string productCode, string productSoldDescription, decimal percentage, Period claimPeriod, Address address, string soldToBusinessName, string phone)
        {
            //transaction basic information
            var transaction = new Transaction
            {
                //Status = TransactionStatus.Pending.ToString(),
                MobileFormat = false,
                CreatedUserId = SecurityContextHelper.CurrentUser.Id,
                CreatedVendorId = SecurityContextHelper.CurrentVendor.VendorId,
                CreatedDate = DateTime.UtcNow,
                IsEligible = true,
                DeviceName = "Imported Paper Form",
                ProcessingStatus = TransactionProcessingStatus.Unreviewed.ToString(),
                UnitType = TreadMarksConstants.Kg,
                IsSingle = false,
                IsAddedByStaff = SecurityContextHelper.IsStaff()
            };
            transaction.TransactionDate = invoiceDate;
            transaction.TransactionTypeCode = TreadMarksConstants.SPSR;
            transaction.InvoiceNumber = invoiceNumber;
            //transaction.FriendlyId = transactionRepository.GetNextAutoPaperFormFriendlyID(90000000, new string[] { TreadMarksConstants.SPS, TreadMarksConstants.SPSR, TreadMarksConstants.DOR });

            //transaction item
            decimal weightInKg = Math.Round(DataConversion.ConvertTonToKg(Convert.ToDouble(totalTDPWeight)), 4, MidpointRounding.AwayFromZero);
            var transactionItem = new TransactionItem()
            {
                ItemID = productTypeId,
                UnitType = TreadMarksConstants.Ton,
                Quantity = 1,
                Rate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == productTypeId && c.EffectiveStartDate <= claimPeriod.StartDate && c.EffectiveEndDate >= claimPeriod.EndDate).ItemRate,
                ActualWeight = weightInKg,
            };

            transactionItem.ItemCode = productCode;
            transactionItem.ItemDescription = productSoldDescription;
            transactionItem.RecyclablePercentage = percentage;

            //transaction company information
            transaction.CompanyInfo = new CompanyInfo();
            transaction.CompanyInfo.Address = address;
            transaction.CompanyInfo.CompanyName = soldToBusinessName;
            transaction.CompanyInfo.PhoneNumber = phone;
            transaction.TransactionItems.Add(transactionItem);

            return transaction;
        }

        public TransactionCreationNotificationDetailViewModel AddRPMPapeFormTransaction(IRPMTransactionViewModel transactionVM)
        {
            Func<IRPMTransactionViewModel, Transaction> transDataAdaperFunc = (vm) =>
            {
                var now = DateTime.UtcNow;

                #region Common
                var localTransactionVM = vm as RPMTransactionViewModel;

                // Trasaction
                var transaction = preparePaperFormTransactionForAddTransaction(
                    localTransactionVM.TransactionId,
                    localTransactionVM.FormType,
                    localTransactionVM.FormDate.On.Value.Date,
                    localTransactionVM.FormNumber,
                    localTransactionVM.Inbound,
                    localTransactionVM.Outbound,
                    now
                );

                transaction.TransactionId = localTransactionVM.TransactionId;
                transaction.TransactionTypeCode = localTransactionVM.FormType;
                transaction.TransactionDate = localTransactionVM.FormDate.On.Value.Date;

                // Supporting Document      
                PrepareTransactionSupportingDocumentForAddTransaction(transaction, localTransactionVM.SupportingDocumentList);

                // Supporting Document Attachment
                PrepareTransactionAttachmentForAddTransaction(transaction, localTransactionVM.UploadDocumentList);
                #endregion

                #region SPSR Form Specific
                if (localTransactionVM.FormType == TreadMarksConstants.SPSR)
                {
                    transaction.Status = TransactionStatus.Completed.ToString();
                    transaction.IsSingle = true;

                    var specificTransactionVM = vm as RPMSPSRTransactionViewModel;
                    transaction.InvoiceNumber = specificTransactionVM.InvoiceNumber;
                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(specificTransactionVM.CompanyInfo.PostalCode);
                    transaction.CompanyInfo = PrepareUnregisteredCompany(specificTransactionVM.CompanyInfo);
                    transaction.RetailPrice = ((CL.TMS.DataContracts.ViewModel.Transaction.RPMSPSRTransactionViewModel)(localTransactionVM)).ProductDetail.RetailPrice ?? 0;
                    //transaction.ToPostalCode = transaction.CompanyInfo.Address.PostalCode;

                    // Product Transaction Item
                    decimal? weightInKg = null;
                    if (specificTransactionVM.TotalTDPWeight.HasValue)
                    {

                        switch (specificTransactionVM.UnitType)
                        {
                            case TreadMarksConstants.Lb://1
                                weightInKg = Math.Round(DataConversion.ConvertLbToKg(specificTransactionVM.TotalTDPWeight.Value), 4, MidpointRounding.AwayFromZero);
                                break;
                            case TreadMarksConstants.Kg://2
                                weightInKg = Math.Round((decimal)specificTransactionVM.TotalTDPWeight.Value, 4, MidpointRounding.AwayFromZero);
                                break;
                            case TreadMarksConstants.Ton://3
                                weightInKg = Math.Round(DataConversion.ConvertTonToKg(specificTransactionVM.TotalTDPWeight.Value), 4, MidpointRounding.AwayFromZero);
                                break;
                            default:
                                break;
                        }
                    }
                    transaction.TransactionItems.Add(PrepareProductTransactionItem(specificTransactionVM.ProductDetail, weightInKg, localTransactionVM.FormType, specificTransactionVM.UnitType));
                }
                #endregion

                return transaction;
            };

            return AddRPMTransaction(transactionVM, transDataAdaperFunc);
        }

        private TransactionCreationNotificationDetailViewModel AddHaulerTransaction(IHaulerTransactionViewModel transactionViewModel, Func<IHaulerTransactionViewModel, Transaction> dataAdapterFunc)
        {
            //initialize OTSTM2-91 Scale Ticket validation
            var haulerTransModel = (HaulerTransactionViewModel)transactionViewModel;
            InitalizeBusinessRule(haulerTransModel);

            var transaction = dataAdapterFunc(transactionViewModel);

            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //Add transaction
                transactionRepository.AddTransaction(transaction);

                //OTSTM2-1214 Add transaction Id into SpecialItemRateList Table, associate to event number
                if (transaction.TransactionTypeCode == TreadMarksConstants.STC)
                    transactionRepository.AddTransactionIdIntoEvent(transaction.ID, transaction.EventNumber);

                //Update Attachment Description
                if (transactionViewModel.UploadDocumentList != null) transactionRepository.UpdateAttachmentDescription(transactionViewModel.UploadDocumentList);

                // Add Transaction Internal Note
                if (!string.IsNullOrEmpty(transactionViewModel.InternalNote)) AddTransactionNote(transaction.ID, transactionViewModel.InternalNote);

                //OTSTM2-91 Scale Ticket validation
                if (haulerTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError != null &&
                    haulerTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.Count > 0)
                {
                    haulerTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.ForEach(c =>
                    {
                        AddTransactionNote(transaction.ID, c);
                    });
                }

                // If the transaction status is "Completed", add the transaction to Claim automatically.
                if (transaction.Status == TransactionStatus.Completed.ToString())
                {
                    AddTransactionToClaim(transaction.ID);
                }

                transactionScope.Complete();
            }
            return GetCreationNotificationDetail(transaction);
        }

        private TransactionCreationNotificationDetailViewModel AddCollectorTransaction(ICollectorTransactionViewModel transactionViewModel, Func<ICollectorTransactionViewModel, Transaction> dataAdapterFunc)
        {
            //initialize
            var collectorTransModel = (CollectorTransactionViewModel)transactionViewModel;
            InitalizeBusinessRule(collectorTransModel);

            var transaction = dataAdapterFunc(transactionViewModel);
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //Add transaction
                transactionRepository.AddTransaction(transaction);

                //Update Attachment Description
                if (transactionViewModel.UploadDocumentList != null) transactionRepository.UpdateAttachmentDescription(transactionViewModel.UploadDocumentList);

                // Add Transaction Internal Note
                if (!string.IsNullOrEmpty(transactionViewModel.InternalNote)) AddTransactionNote(transaction.ID, transactionViewModel.InternalNote);

                //OTSTM2-91 Scale Ticket validation
                if (collectorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError != null &&
                    collectorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.Count > 0)
                {
                    collectorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.ForEach(c =>
                    {
                        AddTransactionNote(transaction.ID, c);
                    });
                }

                // If the transaction status is "Completed", add the transaction to Claim automatically.
                if (transaction.Status == TransactionStatus.Completed.ToString())
                {
                    AddTransactionToClaim(transaction.ID);
                }
                transactionScope.Complete();
            }
            return GetCreationNotificationDetail(transaction);
        }

        private TransactionCreationNotificationDetailViewModel AddProcessorTransaction(IProcessorTransactionViewModel transactionViewModel, Func<IProcessorTransactionViewModel, Transaction> dataAdapterFunc)
        {
            //initialize
            var processorTransModel = (ProcessorTransactionViewModel)transactionViewModel;
            InitalizeBusinessRule(processorTransModel);

            var transaction = dataAdapterFunc(transactionViewModel);
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                if (transactionViewModel.FormType == TreadMarksConstants.SPS || transactionViewModel.FormType == TreadMarksConstants.DOR)
                {
                    transaction.FriendlyId = transactionRepository.GetNextAutoPaperFormFriendlyID(90000000, new string[] { TreadMarksConstants.SPS, TreadMarksConstants.SPSR, TreadMarksConstants.DOR });
                }

                //Add transaction
                transactionRepository.AddTransaction(transaction);

                //Update Attachment Description
                if (transactionViewModel.UploadDocumentList != null) transactionRepository.UpdateAttachmentDescription(transactionViewModel.UploadDocumentList);

                // Add Transaction Internal Note
                if (!string.IsNullOrEmpty(transactionViewModel.InternalNote)) AddTransactionNote(transaction.ID, transactionViewModel.InternalNote);

                //OTSTM2-91 Scale Ticket validation
                if (processorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError != null &&
                    processorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.Count > 0)
                {
                    processorTransModel.CommonBusinessRulesContainer.PreviousScaleTicketError.ForEach(c =>
                    {
                        AddTransactionNote(transaction.ID, c);
                    });
                }

                // If the transaction status is "Completed", add the transaction to Claim automatically.
                if (transaction.Status == TransactionStatus.Completed.ToString())
                {
                    AddTransactionToClaim(transaction.ID);
                }

                transactionScope.Complete();
            }
            return GetCreationNotificationDetail(transaction);
        }

        private TransactionCreationNotificationDetailViewModel AddRPMTransaction(IRPMTransactionViewModel transactionViewModel, Func<IRPMTransactionViewModel, Transaction> dataAdapterFunc)
        {
            var transaction = dataAdapterFunc(transactionViewModel);
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                if (transactionViewModel.FormType == TreadMarksConstants.SPSR)
                {
                    transaction.FriendlyId = transactionRepository.GetNextAutoPaperFormFriendlyID(90000000, new string[] { TreadMarksConstants.SPS, TreadMarksConstants.SPSR, TreadMarksConstants.DOR });
                }

                //Add transaction
                transactionRepository.AddTransaction(transaction);

                //Update Attachment Description
                if (transactionViewModel.UploadDocumentList != null) transactionRepository.UpdateAttachmentDescription(transactionViewModel.UploadDocumentList);

                // Add Transaction Internal Note
                if (!string.IsNullOrEmpty(transactionViewModel.InternalNote)) AddTransactionNote(transaction.ID, transactionViewModel.InternalNote);

                // If the transaction status is "Completed", add the transaction to Claim automatically.
                if (transaction.Status == TransactionStatus.Completed.ToString())
                {
                    AddTransactionToClaim(transaction.ID);
                }

                transactionScope.Complete();
            }
            return GetCreationNotificationDetail(transaction);
        }

        private Transaction preparePaperFormTransactionForAddTransaction(Guid id, string typeCode, DateTime date, long? friendlyId, VendorReference inbound, VendorReference outbound, DateTime addedOn)
        {
            var transaction = new Transaction
            {
                Status = TransactionStatus.Pending.ToString(),
                MobileFormat = false,
                CreatedUserId = SecurityContextHelper.CurrentUser.Id,
                CreatedVendorId = SecurityContextHelper.CurrentVendor.VendorId,
                CreatedDate = addedOn,
                IsEligible = true,
                DeviceName = "Paper Form",
                ProcessingStatus = TransactionProcessingStatus.Unreviewed.ToString(),
                UnitType = TreadMarksConstants.Kg,
                IsSingle = false,
                IsAddedByStaff = SecurityContextHelper.IsStaff()
            };

            transaction.TransactionId = id;
            transaction.TransactionTypeCode = typeCode;
            transaction.TransactionDate = date;
            if (friendlyId.HasValue) transaction.FriendlyId = friendlyId.Value;

            if (inbound != null)
            {
                transaction.ToPostalCode = inbound.Address.PostalCode;
                transaction.IncomingId = inbound.VendorId;
                transaction.IncomingAddressId = inbound.Address.ID;
            }

            if (outbound != null)
            {
                transaction.FromPostalCode = outbound.Address.PostalCode;
                transaction.OutgoingId = outbound.VendorId;
                transaction.OutgoingAddressId = outbound.Address.ID;
            }

            return transaction;
        }

        private void PrepareTransactionItemForAddTransaction(Transaction transaction, List<TransactionTireTypeViewModel> tireTypeList, DateTime effectiveOn)
        {
            if (tireTypeList != null)
            {
                transaction.TransactionItems.AddRange(PrepareTransactionItemList(tireTypeList, effectiveOn));

                transaction.OnRoadWeight = GetOnRoadWeight(transaction.TransactionItems);
                transaction.OffRoadWeight = GetOffRoadWeight(transaction.TransactionItems);

                transaction.EstimatedOnRoadWeight = transaction.OnRoadWeight;
                transaction.EstimatedOffRoadWeight = transaction.OffRoadWeight;
            }
        }

        private void PrepareTransactionSupportingDocumentForAddTransaction(Transaction transaction, List<TransactionSupportingDocumentViewModel> supportingDocumentList)
        {
            if (supportingDocumentList != null)
            {
                supportingDocumentList.ForEach(doc =>
                {
                    if (doc.Required) transaction.TransactionSupportingDocuments.Add(new TransactionSupportingDocument() { DocumentName = doc.DocumentName, DocumentFormat = doc.DocumentFormat });
                });
            }
        }

        private void PrepareTransactionAttachmentForAddTransaction(Transaction transaction, List<TransactionFileUploadModel> UploadDocumentList)
        {
            if (UploadDocumentList != null)
            {
                UploadDocumentList.ForEach(uploadDocument =>
                {
                    if (uploadDocument.AttachmentId.HasValue) transaction.TransactionAttachments.Add(new TransactionAttachment { AttachmentId = uploadDocument.AttachmentId.Value, Description = uploadDocument.Description });
                });
            }
        }

        private ScaleTicket PrepareScaleTicketFromTransactionScaleTicketViewModel(TransactionScaleTicketViewModel vm, ScaleTicketTypeEnum ticketType, DateTime addedOn)
        {
            var scaleTicket = new ScaleTicket()
            {
                InboundWeight = null,
                OutboundWeight = 0.0000M,
                TicketNumber = vm.TicketNumber,
                SortIndex = 0,
                CreateDate = addedOn,
                ScaleTicketType = (int)ticketType,
                UnitType = vm.WeightType,
            };
            if (vm.WeightType == TreadMarksConstants.Kg) scaleTicket.InboundWeight = Math.Round(Convert.ToDecimal(vm.Weight.Value), 4, MidpointRounding.AwayFromZero);
            if (vm.WeightType == TreadMarksConstants.Ton) scaleTicket.InboundWeight = Math.Round(DataConversion.ConvertTonToKg(vm.Weight.Value), 4, MidpointRounding.AwayFromZero);
            if (vm.WeightType == TreadMarksConstants.Lb) scaleTicket.InboundWeight = Math.Round(DataConversion.ConvertLbToKg(vm.Weight.Value), 4, MidpointRounding.AwayFromZero);

            return scaleTicket;
        }

        private List<ScaleTicket> PrepareScaleTicketFromTransactionScaleTicketViewModel(TransactionViewScaleTicketDetailViewModel vm, DateTime addedOn)
        {
            var scaleTickets = new List<ScaleTicket>();

            if (vm.SingleTicketType)
            {
                var ticketModel = vm.Tickets.FirstOrDefault(i => i.TicketType == (int)ScaleTicketTypeEnum.Both);

                if (ticketModel != null) scaleTickets.Add(PrepareScaleTicketFromTransactionViewScaleTicketViewModel(ticketModel, vm.WeightType, ScaleTicketTypeEnum.Both, addedOn));
            }

            if (!vm.SingleTicketType)
            {
                var inboundTicketModel = vm.Tickets.FirstOrDefault(i => i.TicketType == (int)ScaleTicketTypeEnum.Inbound);
                if (inboundTicketModel != null) scaleTickets.Add(PrepareScaleTicketFromTransactionViewScaleTicketViewModel(inboundTicketModel, vm.WeightType, ScaleTicketTypeEnum.Inbound, addedOn));

                var outboundTicketModel = vm.Tickets.FirstOrDefault(i => i.TicketType == (int)ScaleTicketTypeEnum.Outbound);
                if (outboundTicketModel != null) scaleTickets.Add(PrepareScaleTicketFromTransactionViewScaleTicketViewModel(outboundTicketModel, vm.WeightType, ScaleTicketTypeEnum.Outbound, addedOn));
            }

            return scaleTickets;
        }

        private ScaleTicket PrepareScaleTicketFromTransactionViewScaleTicketViewModel(TransactionViewScaleTicketViewModel vm, int weightType, ScaleTicketTypeEnum ticketType, DateTime addedOn)
        {
            var scaleTicket = new ScaleTicket()
            {
                InboundWeight = vm.InboundWeight.HasValue ? vm.InboundWeight.Value : 0.0000M,
                OutboundWeight = vm.OutboundWeight.HasValue ? vm.OutboundWeight.Value : 0.0000M,
                TicketNumber = vm.TicketNumber,
                SortIndex = 0,
                CreateDate = addedOn,
                ScaleTicketType = (int)ticketType,
                UnitType = weightType,
            };

            if (ticketType == ScaleTicketTypeEnum.Inbound) scaleTicket.OutboundWeight = 0.0000M;
            if (ticketType == ScaleTicketTypeEnum.Outbound) scaleTicket.InboundWeight = 0.0000M;

            if (weightType == TreadMarksConstants.Kg) scaleTicket.InboundWeight = Math.Round(Convert.ToDecimal(scaleTicket.InboundWeight), 4, MidpointRounding.AwayFromZero);
            if (weightType == TreadMarksConstants.Ton) scaleTicket.InboundWeight = Math.Round(DataConversion.ConvertTonToKg((double)scaleTicket.InboundWeight), 4, MidpointRounding.AwayFromZero);
            if (weightType == TreadMarksConstants.Lb) scaleTicket.InboundWeight = Math.Round(DataConversion.ConvertLbToKg((double)scaleTicket.InboundWeight), 4, MidpointRounding.AwayFromZero);

            if (weightType == TreadMarksConstants.Kg) scaleTicket.OutboundWeight = Math.Round(Convert.ToDecimal(scaleTicket.OutboundWeight), 4, MidpointRounding.AwayFromZero);
            if (weightType == TreadMarksConstants.Ton) scaleTicket.OutboundWeight = Math.Round(DataConversion.ConvertTonToKg((double)scaleTicket.OutboundWeight), 4, MidpointRounding.AwayFromZero);
            if (weightType == TreadMarksConstants.Lb) scaleTicket.OutboundWeight = Math.Round(DataConversion.ConvertLbToKg((double)scaleTicket.OutboundWeight), 4, MidpointRounding.AwayFromZero);

            return scaleTicket;
        }

        private void DistributeScaleWeight(Transaction transaction, Decimal scaleWeight)
        {
            var totalAverageWeight = transaction.TransactionItems.Sum(c => c.AverageWeight);

            // Distribute the weight
            transaction.TransactionItems.ForEach(c =>
            {
                c.ActualWeight = Math.Round((scaleWeight * (c.AverageWeight / totalAverageWeight)), 4, MidpointRounding.AwayFromZero);
            });

            // After distribution find the total actual weight and if it is not equal to scale weight, then apply the difference to the highest weight transaction item
            var totalActualWeight = transaction.TransactionItems.Sum(c => c.ActualWeight);
            if (totalActualWeight != scaleWeight)
            {
                var difference = (scaleWeight - totalActualWeight);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.0001M);
                if (numberOfDifferenceDistribution > transaction.TransactionItems.Count) throw new Exception("Distribution of weight could not be processed");

                var transactionItemList = transaction.TransactionItems.OrderByDescending(i => i.ActualWeight).ToList();
                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                {
                    transactionItemList[i].ActualWeight += (difference / numberOfDifferenceDistribution);
                }
            }

            transaction.OnRoadWeight = GetOnRoadWeight(transaction.TransactionItems);
            transaction.OffRoadWeight = GetOffRoadWeight(transaction.TransactionItems);
        }

        private CompanyInfo PrepareUnregisteredCompany(TransactionUnregisteredCompanyViewModel companyInfoModel)
        {
            var company = new CompanyInfo()
            {
                CompanyName = companyInfoModel.CompanyName,
                PhoneNumber = companyInfoModel.PhoneNumber,

                Address = new Address
                {
                    AddressType = 1,
                    Address1 = companyInfoModel.Address1,
                    Address2 = companyInfoModel.Address2,
                    City = companyInfoModel.City,
                    Province = companyInfoModel.Province,
                    PostalCode = companyInfoModel.PostalCode,
                    Country = string.IsNullOrEmpty(companyInfoModel.Country) ? "Canada" : companyInfoModel.Country
                }
            };

            return company;
        }

        private TransactionCreationNotificationDetailViewModel GetCreationNotificationDetail(Transaction transaction)
        {
            if (transaction == null) return null;
            var createdVendor = SecurityContextHelper.CurrentVendor;

            var notificationDetailModel = new TransactionCreationNotificationDetailViewModel()
            {
                TransactionType = transaction.TransactionTypeCode,
                TransactionNumber = transaction.FriendlyId.ToString(),
                CreatedOn = transaction.TransactionDate.ToString("yyyy-MM-dd"),
                CreatedBy = transaction.IsAddedByStaff ? "Staff for Reg# " + createdVendor.RegistrationNumber : "Reg# " + createdVendor.RegistrationNumber
            };

            if (!transaction.IsSingle)
            {
                var forVendorId = (SecurityContextHelper.CurrentVendor.VendorId != transaction.IncomingId) ? transaction.IncomingId : transaction.OutgoingId;
                var createdForVendor = vendorRepository.GetVendorById(forVendorId.Value);

                if (createdForVendor != null) notificationDetailModel.Email = createdForVendor.PrimaryContact.Email;
            }
            else notificationDetailModel.Email = null;

            return notificationDetailModel;
        }

        #endregion

        #region BusinessRulesForAddTransaction
        //OTSTM2-91
        private void InitalizeBusinessRule(ICommonTransaction val)
        {

            val.CommonBusinessRulesContainer = new CommonTransactionBusinessRules();

            var submitClaimRuleSetStrategy = TransactionRuleStrategyFactory.LoadTransactionRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            this.BusinessRuleSet.ClearBusinessRules();
        }

        //main function that deals with both adding paper and also adjusting paper transaction
        private List<string> ProcessAddPaperPrevScaleTicket(PreviousScaleTicketModel previousScaleTicketModel, string transactionType)
        {
            this.BusinessRuleSet.AddBusinessRule(new PreviousScaleTicketNumberRule());

            previousScaleTicketModel.TransactionTypeCode = transactionType;
            if (string.Equals(transactionType, TreadMarksConstants.SPS))
            {
                previousScaleTicketModel.PeriodType = GetPeriodType(previousScaleTicketModel.Outbound.VendorType);
            }
            else
            {
                previousScaleTicketModel.PeriodType = GetPeriodType(previousScaleTicketModel.Inbound.VendorType);
            }

            var ruleContext = new Dictionary<string, object>{
                {"transactionRepository", transactionRepository},
                {"previousPaperScaleTicketModel", previousScaleTicketModel}
            };

            ExecuteBusinessRules(previousScaleTicketModel, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                return BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList();
            }

            return null;
        }

        //transaction adjustment paper
        private List<string> ProcessTransAdjScaleTicketPaper(Transaction transaction, string scaleTicket = null)
        {
            //---------------OTSTM2-91 Scale Ticket validation ----------------------------          
            if (transaction.TransactionTypeCode == TreadMarksConstants.DOT || transaction.TransactionTypeCode == TreadMarksConstants.PTR ||
                transaction.TransactionTypeCode == TreadMarksConstants.PIT || transaction.TransactionTypeCode == TreadMarksConstants.SPS ||
                transaction.TransactionTypeCode == TreadMarksConstants.SPSR)
            {
                var submitClaimRuleSetStrategy = TransactionRuleStrategyFactory.LoadTransactionRuleStrategy();
                CreateBusinessRuleSet(submitClaimRuleSetStrategy);
                this.BusinessRuleSet.ClearBusinessRules();

                this.BusinessRuleSet.AddBusinessRule(new PreviousScaleTicketNumberRule());

                VendorReference inbound = null;
                VendorReference outbound = null;

                if (transaction.IncomingId != null)
                {
                    inbound = vendorRepository.GetVendorById(transaction.IncomingId.Value);
                }

                if (transaction.OutgoingId != null)
                {
                    outbound = vendorRepository.GetVendorById(transaction.OutgoingId.Value);
                }

                var previousScaleTicketModel = new PreviousScaleTicketModel();
                previousScaleTicketModel.FormType = transaction.TransactionTypeCode;
                previousScaleTicketModel.Inbound = inbound;
                previousScaleTicketModel.Outbound = outbound;
                previousScaleTicketModel.TransactionTypeCode = transaction.TransactionTypeCode;


                if ((string.Equals(transaction.TransactionTypeCode, TreadMarksConstants.SPS) ||
                    string.Equals(transaction.TransactionTypeCode, TreadMarksConstants.SPSR)
                    )
                    && previousScaleTicketModel.Inbound == null)
                {
                    previousScaleTicketModel.ScaleTicket = scaleTicket;
                    previousScaleTicketModel.PeriodType = GetPeriodType(previousScaleTicketModel.Outbound.VendorType);
                }
                else
                {
                    var ticket = transaction.AdjustmentRecord.ScaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both);
                    previousScaleTicketModel.ScaleTicket = (ticket != null ? ticket.TicketNumber : null);
                    previousScaleTicketModel.PeriodType = GetPeriodType(previousScaleTicketModel.Inbound.VendorType);
                }

                var ruleContext = new Dictionary<string, object>{
                    {"transactionRepository", transactionRepository},
                    {"previousPaperScaleTicketModel", previousScaleTicketModel}
                };

                ExecuteBusinessRules(previousScaleTicketModel, ruleContext);

                if (!BusinessRuleExecutioResult.IsValid)
                {
                    return BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList();
                }
            }
            return null;
        }

        //transaction adjustment mobile
        private List<string> ProcessTransAdjScaleTicketMobile(Transaction transaction, string scaleTicket = null)
        {
            //---------------OTSTM2-91 Scale Ticket validation ----------------------------          
            if (transaction.TransactionTypeCode == "DOT" || transaction.TransactionTypeCode == "PTR" ||
                transaction.TransactionTypeCode == "PIT")
            {
                var submitClaimRuleSetStrategy = TransactionRuleStrategyFactory.LoadTransactionRuleStrategy();
                CreateBusinessRuleSet(submitClaimRuleSetStrategy);
                this.BusinessRuleSet.ClearBusinessRules();

                this.BusinessRuleSet.AddBusinessRule(new PreviousScaleTicketMobileRule());

                VendorReference inboundVendor = null;
                VendorReference outboundVendor = null;

                if (transaction.IncomingId != null)
                {
                    inboundVendor = vendorRepository.GetVendorById(transaction.IncomingId.Value);
                }

                if (transaction.OutgoingId != null)
                {
                    outboundVendor = vendorRepository.GetVendorById(transaction.OutgoingId.Value);
                }

                var previousScaleTicketModel = new PreviousScaleTicketMobile();
                previousScaleTicketModel.FormType = transaction.TransactionTypeCode;
                previousScaleTicketModel.Inbound = inboundVendor;
                previousScaleTicketModel.Outbound = outboundVendor;
                previousScaleTicketModel.PeriodType = GetPeriodType(inboundVendor.VendorType);

                //check if its mobile with two scale tickets with ScaleTicketType as 1 & 2
                if (transaction.AdjustmentRecord.ScaleTickets.Any(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Inbound) &&
                    transaction.AdjustmentRecord.ScaleTickets.Any(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Outbound))
                {
                    previousScaleTicketModel.IsSingleScaleticket = false;
                    previousScaleTicketModel.InboundScaleTicket = transaction.AdjustmentRecord.ScaleTickets.First(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Inbound).TicketNumber;
                    previousScaleTicketModel.OutboundScaleTicket = transaction.AdjustmentRecord.ScaleTickets.First(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Outbound).TicketNumber;
                }
                //single scale ticket for mobile with ScaleTicketType as 3
                else
                {
                    previousScaleTicketModel.IsSingleScaleticket = true;
                    var ticket = transaction.AdjustmentRecord.ScaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both);
                    previousScaleTicketModel.SingleScaleTicket = (ticket != null ? ticket.TicketNumber : null);
                }

                var ruleContext = new Dictionary<string, object>{
                    {"transactionRepository", transactionRepository},
                    {"previousPaperScaleTicketModel", previousScaleTicketModel}
                };

                ExecuteBusinessRules(previousScaleTicketModel, ruleContext);

                if (!BusinessRuleExecutioResult.IsValid)
                {
                    return BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList();
                }
            }
            return null;
        }

        private int? GetPeriodType(int vendorType)
        {
            if (vendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
            {
                return AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.PeriodType, "HaulerClaimPeriod").DefinitionValue;
            }
            else if (vendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
            {
                return AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.PeriodType, "CollectorClaimPeriod").DefinitionValue;
            }
            else if (vendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
            {
                return AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.PeriodType, "ProcessClaimsPeriod").DefinitionValue;
            }
            else if (vendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
            {
                return AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.PeriodType, "RPMClaimPeriod").DefinitionValue;
            }
            else if (vendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Steward").DefinitionValue)
            {
                return AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.PeriodType, "StewardClaimPeriod").DefinitionValue;
            }

            return null;
        }

        #endregion


        #region Get Transaction Detail (For Both Paper & Mobile)
        public ITransactionDetailViewModel GetTransactionHandlerData(int transactionId, bool adjRequested, int? transactionAdjustmentId = null, int? claimId = 0)
        {
            var isStaff = SecurityContextHelper.IsStaff();
            int? currentVendorId = null;
            string vendorTypeCode = string.Empty;

            if (SecurityContextHelper.CurrentVendor != null)
            {
                currentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
                vendorTypeCode = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, SecurityContextHelper.CurrentVendor.VendorType).Code;
            }

            var transaction = transactionRepository.GetTransactionById(transactionId);
            if (transaction.Status == TransactionStatus.Incomplete.ToString())
            {
                return new TransactionDetailViewModel()
                {
                    IsIncomplete = true,
                    IsMobile = transaction.IsMobile,
                    DisplayTypeCode = transaction.TransactionTypeCode,
                    //DeviceName = transaction.IsMobile ? transaction.DeviceName : "Paper Form",
                    DeviceName = transaction.DeviceName, //OTSTM2-81
                    Badge = transaction.IsMobile ? transaction.Badge : string.Empty,
                    FriendlyNumber = transaction.FriendlyId
                };
            }


            var directionForVendor = (currentVendorId.HasValue && transaction.OutgoingId == currentVendorId.Value) ? TreadMarksConstants.Outbound : TreadMarksConstants.Inbound;

            var claimAssociation = claimsRepository.GetTransactionClaimAssociationDetailDataModel(transactionId);
            var period = GetTransactionPeriod(transaction, claimAssociation, currentVendorId ?? 0);//this period is CLAIM period of the transaction
            //TransactionPeriodViewModel transactionPeriod = new TransactionPeriodViewModel();//this period is TRANSACTION period, may be earlier then CLAIM period, transaction adjust should use this period to load rate/weight
            //transactionPeriod.StartDate = new DateTime(transaction.TransactionDate.Year, transaction.TransactionDate.Month, 1);
            //transactionPeriod.EndDate = new DateTime(transaction.TransactionDate.Year, transaction.TransactionDate.Month, 1).AddMonths(1).AddDays(-1);

            var adjustmentPending = transaction.IsAdjustmentPending;
            var adjustmentAccepted = transaction.IsAdjustmentAccepted;
            var activeAdjustmentExists = (adjustmentPending || adjustmentAccepted);

            var adjustmentAllowedInClaim = IsAllClaimAssociationInOpenStatus(claimAssociation, isStaff);
            var loadAdjustmentModel = adjRequested && adjustmentAllowedInClaim && transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString() && !adjustmentPending;
            var adjustmentReviewAllowed = false;
            var adjustmentRecallAllowed = false;
            var adjustmentRejectAllowed = false;

            TransactionAdjustment adjustmentRecord;
            if (transactionAdjustmentId != null)
            {
                adjustmentRecord = transactionRepository.GetTransactionAdjustmentById(transactionAdjustmentId.Value);
                transaction.AdjustmentRecord = adjustmentRecord;

                if (adjustmentRecord.Status == TransactionAdjustmentStatus.Pending.ToString())
                {
                    adjustmentReviewAllowed = currentVendorId.HasValue ? IsAdjustmentReviewAllowed(isStaff, currentVendorId.Value, adjustmentRecord.CreatedVendorId.Value, adjustmentPending, claimAssociation) : false;
                    adjustmentRecallAllowed = currentVendorId.HasValue ? IsAdjustmentRecallAllowed(isStaff, currentVendorId.Value, adjustmentRecord.CreatedVendorId.Value, adjustmentPending, claimAssociation) : false;
                }

                adjustmentRejectAllowed = isStaff && adjustmentRecord.Status == TransactionAdjustmentStatus.Accepted.ToString() && !loadAdjustmentModel;
            }
            else if (activeAdjustmentExists)
            {
                adjustmentRecord = transactionRepository.GetTransactionAdjustmentById(transaction.ActiveAdjustmentId);
                transaction.AdjustmentRecord = adjustmentRecord;

                adjustmentReviewAllowed = currentVendorId.HasValue ? IsAdjustmentReviewAllowed(isStaff, currentVendorId.Value, adjustmentRecord.CreatedVendorId.Value, adjustmentPending, claimAssociation) : false;

                adjustmentRecallAllowed = currentVendorId.HasValue ? IsAdjustmentRecallAllowed(isStaff, currentVendorId.Value, adjustmentRecord.CreatedVendorId.Value, adjustmentPending, claimAssociation) : false;

                adjustmentRejectAllowed = isStaff && adjustmentAccepted && !loadAdjustmentModel;
            }
            else adjustmentRecord = null;

            adjustmentRecallAllowed = adjustmentRecallAllowed && (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.TransactionsAllTransactions) == SecurityResultType.EditSave);
            adjustmentRejectAllowed = adjustmentRejectAllowed && (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.TransactionsAllTransactions) == SecurityResultType.EditSave);

            ITransactionDetailViewModel transactionDetailData = null;

            switch (transaction.TransactionTypeCode)
            {
                case TreadMarksConstants.DOT:
                    transactionDetailData = new DOTTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.TCR:
                    transactionDetailData = new TCRTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.HIT:
                    transactionDetailData = new HITTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.STC:
                    transactionDetailData = new STCTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.PTR:
                    transactionDetailData = new PTRTransactionDetailViewModel();

                    var vmPTR = transactionDetailData as PTRTransactionDetailViewModel;
                    if (string.IsNullOrEmpty(vendorTypeCode)) vmPTR.FormDateLabel = "Date Received";
                    if (vendorTypeCode == TreadMarksConstants.Hauler) vmPTR.FormDateLabel = "Date Delivered";
                    if (vendorTypeCode == TreadMarksConstants.Processor) vmPTR.FormDateLabel = "Date Received";
                    break;

                case TreadMarksConstants.PIT:
                    transactionDetailData = new PITTransactionDetailViewModel();

                    var vmPIT = transactionDetailData as PITTransactionDetailViewModel;
                    vmPIT.FormDateLabel = (directionForVendor == TreadMarksConstants.Outbound) ? "Date Delivered" : "Date Received";
                    break;

                case TreadMarksConstants.UCR:
                    transactionDetailData = new UCRTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.RTR:
                    transactionDetailData = new RTRTransactionDetailViewModel() { TireMarketInfo = new TransactionTireMarketDetailViewModel() };

                    var vmRTR = transactionDetailData as RTRTransactionDetailViewModel;
                    vmRTR.TireMarketInfo.Location = transaction.MarketLocation;
                    vmRTR.TireMarketInfo.Type = transaction.TireMarketType ?? 1;
                    vmRTR.TireMarketInfo.BillofLoading = string.IsNullOrEmpty(transaction.BillofLoading) ? "" : transaction.BillofLoading;
                    vmRTR.InvoiceNumber = string.IsNullOrEmpty(transaction.InvoiceNumber) ? "" : transaction.InvoiceNumber;
                    break;

                case TreadMarksConstants.SPS:
                    transactionDetailData = new SPSTransactionDetailViewModel();

                    var vmSPS = transactionDetailData as SPSTransactionDetailViewModel;
                    if (transaction.CompanyInfo != null) vmSPS.SoldTo = "unregistered";
                    else vmSPS.SoldTo = "registered";
                    vmSPS.AsSIR = (vendorTypeCode == TreadMarksConstants.RPM);
                    break;

                case TreadMarksConstants.SPSR:
                    transactionDetailData = new SPSRTransactionDetailViewModel();
                    break;

                case TreadMarksConstants.DOR:
                    transactionDetailData = new DORTransactionDetailViewModel();

                    var vmDOR = transactionDetailData as DORTransactionDetailViewModel;
                    if (transaction.CompanyInfo != null) vmDOR.SoldTo = "unregistered";
                    else vmDOR.SoldTo = "registered";
                    break;
            }

            // Common detail loading...
            transactionDetailData.IsIncomplete = false;
            transactionDetailData.Id = transaction.ID;
            transactionDetailData.TransactionId = transaction.TransactionId;
            transactionDetailData.Status = transaction.Status.ToString();
            transactionDetailData.Direction = directionForVendor;
            transactionDetailData.TypeCode = transaction.TransactionTypeCode;
            transactionDetailData.IncomingId = transaction.IncomingId;
            transactionDetailData.OutgoingId = transaction.OutgoingId;
            transactionDetailData.DisplayTypeCode = transaction.TransactionTypeCode;
            transactionDetailData.TransactionDate = transaction.TransactionDate;
            transactionDetailData.TransactionEndDate = transaction.TransactionEndDate;
            transactionDetailData.TransactionCreatedDate = transaction.TransactionDate;
            transactionDetailData.FriendlyNumber = transaction.FriendlyId;
            //transactionDetailData.DeviceName = "Paper Form";
            transactionDetailData.DeviceName = transaction.DeviceName; //OTSTM2-81
            transactionDetailData.Badge = string.Empty;
            transactionDetailData.IsMobile = transaction.MobileFormat;
            transactionDetailData.MobileSyncDate = null;
            transactionDetailData.DisplayOriginal = true;
            transactionDetailData.RetailPrice = transaction.RetailPrice ?? 0;
            transactionDetailData.TransactionProcessingStatus = transaction.ProcessingStatus; //OTSTM2-660

            if (adjRequested && adjustmentAllowedInClaim && transaction.ProcessingStatus == TransactionProcessingStatus.Unreviewed.ToString() && !adjustmentPending)
                transactionDetailData.CurrentMode = "adjust";
            else
                transactionDetailData.CurrentMode = "view";

            // Override "DisplayTypeCode" for the following transaction types
            if (transaction.TransactionTypeCode == TreadMarksConstants.SPSR)
                transactionDetailData.DisplayTypeCode = TreadMarksConstants.SPS;

            if (vendorTypeCode == TreadMarksConstants.RPM && transaction.TransactionTypeCode == TreadMarksConstants.SPS)
                transactionDetailData.DisplayTypeCode = TreadMarksConstants.SIR;

            // Adjustment related
            transactionDetailData.AdjustmentAllowedInClaim = adjustmentAllowedInClaim;
            transactionDetailData.ActiveAdjustmentExits = activeAdjustmentExists;
            //OTSTM2-391
            //transactionDetailData.PendingAdjustmentExits = activeAdjustmentExists;
            transactionDetailData.PendingAdjustmentExits = adjustmentPending;


            transactionDetailData.AdjustmentRecallAllowed = adjustmentRecallAllowed;
            transactionDetailData.AdjustmentReviewAllowed = adjustmentReviewAllowed;
            transactionDetailData.AdjustmentRejectAllowed = adjustmentRejectAllowed;


            if (activeAdjustmentExists)
            {
                transactionDetailData.DisplayOriginal = false; // show the adjustment by default
                transactionDetailData.AdjustmentDetail = GetAdjustmentDetail(adjustmentRecord);
            }

            transactionDetailData.AdjModel = GetTransactionAdjustmentModel(transaction, loadAdjustmentModel, period);
            //OTSTM2-1160
            //GoogleGeocodeAPI.Location location = new GoogleGeocodeAPI.Location();
            if (transaction.IncomingVendor != null)
            {
                transactionDetailData.InboundParticipant = GetParticipantDetailsFromVendor(transaction.IncomingVendor, transaction.TransactionTypeCode, TreadMarksConstants.Inbound, transaction.IncomingGpsLog, transaction.IsMobile);
            }

            if (transaction.OutgoingVendor != null)
            {
                transactionDetailData.OutboundParticipant = GetParticipantDetailsFromVendor(transaction.OutgoingVendor, transaction.TransactionTypeCode, TreadMarksConstants.Outbound, transaction.OutgoingGpsLog, transaction.IsMobile);
                //OTSTM2-1160
                // location = ReverseAddressLookup.RetrieveCoordinates(transaction.OutgoingVendor.VendorAddresses.FirstOrDefault(i => i.AddressType == (int)AddressTypeEnum.Business).PostalCode);
            }

            transactionDetailData.Original = GetTransactionOriginalDetailModel(transaction, loadAdjustmentModel, vendorTypeCode, period);

            if (adjustmentRecord != null)
                transactionDetailData.Adjusted = GetTransactionAdjustedDetailModel(transaction, adjustmentRecord, vendorTypeCode, period);

            // For mobile transaction only
            if (transaction.IsMobile)
            {
                GoogleGeocodeAPI.Location incomingSignatureLocation = new GoogleGeocodeAPI.Location();
                GoogleGeocodeAPI.Location outgoingSignatureLocation = new GoogleGeocodeAPI.Location();

                transactionDetailData.MobileSyncDate = transaction.SyncDate;
                transactionDetailData.DeviceName = transaction.DeviceName;
                transactionDetailData.Badge = transaction.Badge;

                // Loading photos                
                var photoList = transaction.TransactionPhotos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.Photo && p.TransactionAdjustmentId == null).Select(p => p.Photo).ToList();
                foreach (var photo in photoList) transactionDetailData.PhotoList.Add(new TransactionViewPhotoViewModel() { Src = photo.FileName, Comment = photo.Comments });

                // Loading comments
                foreach (var comment in transaction.TransactionComments) transactionDetailData.CommentList.Add(comment.Text);

                // Loading signatures
                transactionDetailData.Signature = new TransactionViewSignatureViewModel() { IncomingExists = false, OutgoingExists = false };

                var incomingSignature = transaction.TransactionPhotos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.IncomingSignature && p.TransactionAdjustmentId == null).Select(p => p.Photo).FirstOrDefault();
                if (incomingSignature != null)
                {
                    transactionDetailData.Signature.IncomingExists = true;
                    transactionDetailData.Signature.IncomingName = string.IsNullOrEmpty(transaction.IncomingSignatureName) ? "" : transaction.IncomingSignatureName;
                    transactionDetailData.Signature.IncomingSrc = incomingSignature.FileName;

                    if (transaction.IncomingVendor != null)
                    {
                        transactionDetailData.Signature.IncomingLabel = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, transaction.IncomingVendor.VendorType).Name;

                        if (transaction.TransactionTypeCode == TreadMarksConstants.PIT) transactionDetailData.Signature.IncomingLabel = "Inbound Processor";
                    }
                    else
                    {
                        if (transaction.TransactionTypeCode == TreadMarksConstants.RTR) transactionDetailData.Signature.IncomingLabel = "Buyer";
                    }
                    incomingSignatureLocation.lat = (double)(incomingSignature.Latitude ?? 0);
                    incomingSignatureLocation.lng = (double)(incomingSignature.Longitude ?? 0);
                }

                var outgoingSignature = transaction.TransactionPhotos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.OutgoingSignature && p.TransactionAdjustmentId == null).Select(p => p.Photo).FirstOrDefault();
                if (outgoingSignature != null)
                {
                    transactionDetailData.Signature.OutgoingExists = true;
                    transactionDetailData.Signature.OutgoingName = string.IsNullOrEmpty(transaction.OutgoingSignatureName) ? "" : transaction.OutgoingSignatureName;
                    transactionDetailData.Signature.OutgoingSrc = outgoingSignature.FileName;

                    if (transaction.OutgoingVendor != null)
                    {
                        transactionDetailData.Signature.OutgoingLabel = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, transaction.OutgoingVendor.VendorType).Name;

                        if (transaction.TransactionTypeCode == TreadMarksConstants.PIT) transactionDetailData.Signature.OutgoingLabel = "Outbound Processor";
                    }
                    else
                    {
                        if (transaction.TransactionTypeCode == TreadMarksConstants.STC) transactionDetailData.Signature.OutgoingLabel = "Event Representative";
                        if (transaction.TransactionTypeCode == TreadMarksConstants.UCR) transactionDetailData.Signature.OutgoingLabel = "Unregistered Collector";
                    }
                    outgoingSignatureLocation.lat = (double)(outgoingSignature.Latitude ?? 0);
                    outgoingSignatureLocation.lng = (double)(outgoingSignature.Longitude ?? 0);
                }

                if (transaction.Status != TransactionStatus.Voided.ToString())
                {
                    Address retrievedAddress = new Address();
                    string sAddr = string.Empty;
                    //Registered address : Green (Business location of outbound Registrant - comes from application)
                    //OTSTM2-1160 Green Pin missing issue
                    if (transaction.OutgoingVendor != null)
                    {
                        var vendorAddr = transaction.OutgoingVendor.VendorAddresses.FirstOrDefault(i => i.AddressType == (int)AddressTypeEnum.Business);

                        var location = new GoogleGeocodeAPI.Location();
                        if (vendorAddr != null && vendorAddr.GPSCoordinate1.HasValue && vendorAddr.GPSCoordinate1.Value != 0 && vendorAddr.GPSCoordinate2.HasValue && vendorAddr.GPSCoordinate2.Value != 0)
                        {
                            location.lat = (double)vendorAddr.GPSCoordinate1.Value;
                            location.lng = (double)vendorAddr.GPSCoordinate2.Value;
                        }
                        else
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", vendorAddr.Address1, vendorAddr.City, vendorAddr.Province + " " + vendorAddr.PostalCode, vendorAddr.Country);
                            location = ReverseAddressLookup.RetrieveCoordinates(sAddr);
                            //update address 
                            if (location != null)
                            {
                                vendorRepository.UpdateAddressGPS(vendorAddr.ID, (decimal)location.lat, (decimal)location.lng);
                            }
                        }
                        if (location != null && (location.lat != 0 || location.lng != 0))
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", transactionDetailData.OutboundParticipant.Address1, transactionDetailData.OutboundParticipant.City, transactionDetailData.OutboundParticipant.Province + " " + transactionDetailData.OutboundParticipant.PostalCode, transactionDetailData.OutboundParticipant.Country);
                            transactionDetailData.MobileTransactionGPS.Add(new MobileTransactionGPS { LocationName = "Registered address", IconName = "green-dot.png", Latitude = (decimal)location.lat, Longitude = (decimal)location.lng, AddressText = sAddr });
                        }
                    }
                    //QR scan : Red  (Location of the Outbound QR Code was scanned)
                    if (transaction.OutgoingGpsLog != null && (transaction.OutgoingGpsLog.Latitude != 0 || transaction.OutgoingGpsLog.Longitude != 0))
                    {
                        retrievedAddress = ReverseAddressLookup.RetrieveAddress((decimal)transaction.OutgoingGpsLog.Latitude, (decimal)transaction.OutgoingGpsLog.Longitude);
                        if (retrievedAddress != null)
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", retrievedAddress.Address1 + " " + retrievedAddress.Address2, retrievedAddress.City, retrievedAddress.Province + " " + retrievedAddress.PostalCode, retrievedAddress.Country);
                        }
                        transactionDetailData.MobileTransactionGPS.Add(new MobileTransactionGPS { LocationName = "QR scan", IconName = "red-dot.png", Latitude = transaction.OutgoingGpsLog.Latitude, Longitude = transaction.OutgoingGpsLog.Longitude, GPSDateTime = transaction.OutgoingGpsLog.SyncDate, AddressText = sAddr });
                    }

                    //Inbound signature : Purple  (Location of the Inbound signature was done)
                    if (incomingSignatureLocation != null && (incomingSignatureLocation.lat != 0 || incomingSignatureLocation.lng != 0))
                    {
                        retrievedAddress = ReverseAddressLookup.RetrieveAddress((decimal)incomingSignatureLocation.lat, (decimal)incomingSignatureLocation.lng);
                        if (retrievedAddress != null)
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", retrievedAddress.Address1 + " " + retrievedAddress.Address2, retrievedAddress.City, retrievedAddress.Province + " " + retrievedAddress.PostalCode, retrievedAddress.Country);
                        }
                        transactionDetailData.MobileTransactionGPS.Add(new MobileTransactionGPS { LocationName = "Inbound signature", IconName = "purple-dot.png", Latitude = (decimal)incomingSignatureLocation.lat, Longitude = (decimal)incomingSignatureLocation.lng, AddressText = sAddr });
                    }

                    //Transaction start : Blue  (Location of the transaction originated)
                    if (transaction.IncomingGpsLog != null && (transaction.IncomingGpsLog.Latitude != 0 || transaction.IncomingGpsLog.Longitude != 0))
                    {
                        retrievedAddress = ReverseAddressLookup.RetrieveAddress((decimal)transaction.IncomingGpsLog.Latitude, (decimal)transaction.IncomingGpsLog.Longitude);
                        if (retrievedAddress != null)
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", retrievedAddress.Address1 + " " + retrievedAddress.Address2, retrievedAddress.City, retrievedAddress.Province + " " + retrievedAddress.PostalCode, retrievedAddress.Country);
                        }
                        transactionDetailData.MobileTransactionGPS.Add(new MobileTransactionGPS { LocationName = "Transaction start", IconName = "blue-dot.png", Latitude = transaction.IncomingGpsLog.Latitude, Longitude = transaction.IncomingGpsLog.Longitude, GPSDateTime = transaction.IncomingGpsLog.SyncDate, AddressText = sAddr });
                    }

                    //Outbound signature : Orange (Location of the Outbound signature was done)
                    if (outgoingSignatureLocation != null && (outgoingSignatureLocation.lat != 0 || outgoingSignatureLocation.lng != 0))
                    {
                        retrievedAddress = ReverseAddressLookup.RetrieveAddress((decimal)outgoingSignatureLocation.lat, (decimal)outgoingSignatureLocation.lng);
                        if (retrievedAddress != null)
                        {
                            sAddr = string.Format("{0}, {1}, {2}, {3}", retrievedAddress.Address1 + " " + retrievedAddress.Address2, retrievedAddress.City, retrievedAddress.Province + " " + retrievedAddress.PostalCode, retrievedAddress.Country);
                        }
                        transactionDetailData.MobileTransactionGPS.Add(new MobileTransactionGPS { LocationName = "Outbound signature", IconName = "orange-dot.png", Latitude = (decimal)outgoingSignatureLocation.lat, Longitude = (decimal)outgoingSignatureLocation.lng, AddressText = sAddr });
                    }

                }
            }

            // For internal note
            if (isStaff)
            {
                var internalNoteList = transaction.TransactionNotes.OrderByDescending(i => i.CreatedDate).ToList();

                foreach (var internalNote in internalNoteList)
                {
                    var user = internalNote.User;
                    //OTSTM2-765
                    transactionDetailData.InternalNote.NoteList.Add(new InternalNoteViewModel() { AddedOn = internalNote.CreatedDate, AddedBy = (user != null) ? (internalNote.Note.Contains("This transaction is invalidated by") ? AppSettings.Instance.SystemUser.FirstName + " " + AppSettings.Instance.SystemUser.LastName : user.FirstName + " " + user.LastName) : "System Admin", Note = internalNote.Note });
                }

                transactionDetailData.InternalNote.TransactionId = transactionId;
                transactionDetailData.InternalNote.NewNote = "";
            }
            //transactionDetailData.OtherClaimStatus = claimsRepository.checkOtherClaimStatus(claimId ?? 0, transactionId);

            //OTSTM2-660
            if (claimId != 0)
            {
                transactionDetailData.TransactionReviewToolBar = getUpdatedReviewToolBar(transactionId, claimId);

                //OTSTM2-653 Reject adj not allowed when either of the Claim is approved
                var currentClaimStatus = claimAssociation.Associations.Where(c => c.ClaimId == claimId).FirstOrDefault().Status;
                string otherClaimStatus = claimsRepository.checkOtherClaimStatus(claimId ?? 0, transactionId);

                transactionDetailData.AdjustmentRejectAllowed = adjustmentRejectAllowed && currentClaimStatus != ClaimStatus.Approved.ToString() && otherClaimStatus != ClaimStatus.Approved.ToString();

                //var currentClaimStatus = claimAssociation.Associations.Where(c => c.ClaimId == claimId).FirstOrDefault().Status.Replace(" ", "");
                //if (currentClaimStatus == ClaimStatus.UnderReview.ToString())
                //{
                //    transactionDetailData.TransactionReviewToolBar = getUpdateReviewToolBar(transactionId, claimId);

                //    //transactionDetailData.TransactionReviewToolBar.hasPendingAdjustments = transaction.IsAdjustmentPending;
                //    //string otherClaimStatus = claimsRepository.checkOtherClaimStatus(claimId ?? 0, transactionId);
                //    //transactionDetailData.TransactionReviewToolBar.IsOtherClaimApproved = string.Equals(otherClaimStatus, "Approved");
                //}
            }

            return transactionDetailData;
        }

        private ITransactionAdjustableDetailViewModel GetTransactionOriginalDetailModel(Transaction transaction, bool inAdjMode, string vendorTypeCode, TransactionPeriodViewModel period)
        {
            ITransactionAdjustableDetailViewModel model = null;

            switch (transaction.TransactionTypeCode)
            {
                case TreadMarksConstants.DOT:
                    model = new DOTTransactionAdjustableDetailViewModel();
                    var localModelDOT = model as DOTTransactionAdjustableDetailViewModel;

                    localModelDOT.GenerateTires = inAdjMode ? transaction.EffectiveGenerateTires : transaction.IsGenerateTires;

                    if (inAdjMode)
                    {
                        localModelDOT.ScaleTicketDetail = GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight);
                    }
                    else
                    {
                        localModelDOT.ScaleTicketDetail = GetScaleTicketDetails(transaction.OriginalScaleTickets, transaction.OriginalTransactionPhotos, transaction.OriginalEstimatedWeight);
                    }
                    break;

                case TreadMarksConstants.TCR:
                    model = new TCRTransactionAdjustableDetailViewModel();
                    var localModelTCR = model as TCRTransactionAdjustableDetailViewModel;

                    localModelTCR.GenerateTires = inAdjMode ? transaction.EffectiveGenerateTires : transaction.IsGenerateTires;
                    break;

                case TreadMarksConstants.HIT:
                    model = new HITTransactionAdjustableDetailViewModel();
                    break;

                case TreadMarksConstants.STC:
                    model = new STCTransactionAdjustableDetailViewModel();
                    var localModelSTC = model as STCTransactionAdjustableDetailViewModel;

                    localModelSTC.EventNumber = inAdjMode ? transaction.EffectiveEventNumber : transaction.EventNumber;

                    if (inAdjMode)
                    {
                        localModelSTC.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Group / Individual Name");
                    }
                    else
                    {
                        localModelSTC.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Group / Individual Name");
                    }
                    break;

                case TreadMarksConstants.PTR:
                    model = new PTRTransactionAdjustableDetailViewModel();
                    var localModelPTR = model as PTRTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelPTR.ScaleTicketDetail = GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight);
                        localModelPTR.CalculationSummary = (vendorTypeCode == TreadMarksConstants.Processor) ? GetProcessorCalculationSummaryDetails(transaction.IncomingId, period, transaction.EffectiveTransactionItems) : null;
                    }
                    else
                    {
                        localModelPTR.ScaleTicketDetail = GetScaleTicketDetails(transaction.OriginalScaleTickets, transaction.OriginalTransactionPhotos, transaction.OriginalEstimatedWeight);
                        localModelPTR.CalculationSummary = (vendorTypeCode == TreadMarksConstants.Processor) ? GetProcessorCalculationSummaryDetails(transaction.IncomingId, period, transaction.OriginalTransactionItems) : null;
                    }
                    break;

                case TreadMarksConstants.PIT:
                    model = new PITTransactionAdjustableDetailViewModel();
                    var localModelPIT = model as PITTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelPIT.ScaleTicketDetail = GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight, true);
                        localModelPIT.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault());
                    }
                    else
                    {
                        localModelPIT.ScaleTicketDetail = GetScaleTicketDetails(transaction.OriginalScaleTickets, transaction.OriginalTransactionPhotos, transaction.OriginalEstimatedWeight, true);
                        localModelPIT.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.OriginalTransactionItems.FirstOrDefault());
                    }
                    break;


                case TreadMarksConstants.UCR:
                    model = new UCRTransactionAdjustableDetailViewModel();
                    var localModelUCR = model as UCRTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelUCR.PaymentEligible = GetPaymentEligibleStr(transaction.EffectivePaymentEligible, transaction.EffectivePaymentEligibleOther);

                        if (transaction.EffectiveCompanyInfo != null) localModelUCR.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Unregistered Site Name");
                        else localModelUCR.OutboundParticipant = GetParticipantDetailsFromVendor(transaction.EffectiveVendorInfo, transaction.TransactionTypeCode, TreadMarksConstants.Outbound, transaction.OutgoingGpsLog, transaction.IsMobile);
                    }
                    else
                    {
                        localModelUCR.PaymentEligible = GetPaymentEligibleStr(transaction.PaymentEligible, transaction.PaymentEligibleOther);

                        if (transaction.CompanyInfo != null) localModelUCR.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Unregistered Site Name");
                        else localModelUCR.OutboundParticipant = GetParticipantDetailsFromVendor(transaction.VendorInfo, transaction.TransactionTypeCode, TreadMarksConstants.Outbound, transaction.OutgoingGpsLog, transaction.IsMobile);
                    }

                    break;

                case TreadMarksConstants.RTR:
                    model = new RTRTransactionAdjustableDetailViewModel();
                    var localModelRTR = model as RTRTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelRTR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Buyer Name");
                    }
                    else
                    {
                        localModelRTR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Buyer Name");
                    }
                    break;

                case TreadMarksConstants.SPS:
                    model = new SPSTransactionAdjustableDetailViewModel();
                    var localModelSPS = model as SPSTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelSPS.InvoiceNumber = transaction.EffectiveInvoiceNumber;

                        localModelSPS.ScaleTicketDetail = GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight, true);
                        localModelSPS.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault());

                        if (transaction.EffectiveCompanyInfo != null) localModelSPS.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Unregistered RPM");
                    }
                    else
                    {
                        localModelSPS.InvoiceNumber = transaction.InvoiceNumber;

                        localModelSPS.ScaleTicketDetail = GetScaleTicketDetails(transaction.OriginalScaleTickets, transaction.OriginalTransactionPhotos, transaction.OriginalEstimatedWeight, true);
                        localModelSPS.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.OriginalTransactionItems.FirstOrDefault());

                        if (transaction.CompanyInfo != null) localModelSPS.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Unregistered RPM");
                    }
                    if (transaction.CompanyInfo == null) localModelSPS.InboundParticipant = GetParticipantDetailsFromVendor(transaction.IncomingVendor, transaction.TransactionTypeCode, TreadMarksConstants.Inbound, transaction.IncomingGpsLog, transaction.IsMobile);
                    break;

                case TreadMarksConstants.SPSR:
                    model = new SPSRTransactionAdjustableDetailViewModel();
                    var localModelSPSR = model as SPSRTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelSPSR.InvoiceNumber = transaction.EffectiveInvoiceNumber;
                        localModelSPSR.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault());
                        localModelSPSR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Sold to Business Name");
                    }
                    else
                    {
                        localModelSPSR.InvoiceNumber = transaction.InvoiceNumber;
                        localModelSPSR.ProductDetail = GetProductDetail(transaction.TransactionTypeCode, transaction.OriginalTransactionItems.FirstOrDefault());
                        localModelSPSR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Sold to Business Name");
                    }
                    localModelSPSR.ProductDetail.RetailPrice = transaction.RetailPrice ?? 0;
                    break;

                case TreadMarksConstants.DOR:
                    model = new DORTransactionAdjustableDetailViewModel();
                    var localModelDOR = model as DORTransactionAdjustableDetailViewModel;

                    if (inAdjMode)
                    {
                        localModelDOR.ScaleTicketDetail = GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight, true);

                        localModelDOR.InvoiceNumber = transaction.EffectiveInvoiceNumber;
                        localModelDOR.MaterialTypeId = transaction.EffectiveMaterialType;
                        localModelDOR.DispositionReasonId = transaction.EffectiveDispositionReason;

                        if (localModelDOR.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue) localModelDOR.UsedTireSaleDetail = GetDORUsedTireSaleDetail(transaction, transaction.EffectiveTransactionItems, period);

                        if (transaction.EffectiveCompanyInfo != null) localModelDOR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.EffectiveCompanyInfo, "Unregistered Participant");
                        else localModelDOR.InboundParticipant = GetParticipantDetailsFromVendor(transaction.VendorInfo, transaction.TransactionTypeCode, TreadMarksConstants.Inbound, transaction.IncomingGpsLog, transaction.IsMobile);
                    }
                    else
                    {
                        localModelDOR.ScaleTicketDetail = GetScaleTicketDetails(transaction.OriginalScaleTickets, transaction.OriginalTransactionPhotos, transaction.OriginalEstimatedWeight, true);

                        localModelDOR.InvoiceNumber = transaction.InvoiceNumber;
                        localModelDOR.MaterialTypeId = transaction.MaterialType;
                        localModelDOR.DispositionReasonId = transaction.DispositionReason;

                        if (localModelDOR.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue) localModelDOR.UsedTireSaleDetail = GetDORUsedTireSaleDetail(transaction, transaction.OriginalTransactionItems, period);

                        if (transaction.CompanyInfo != null) localModelDOR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(transaction.CompanyInfo, "Unregistered Participant");
                        else localModelDOR.InboundParticipant = GetParticipantDetailsFromVendor(transaction.VendorInfo, transaction.TransactionTypeCode, TreadMarksConstants.Inbound, transaction.IncomingGpsLog, transaction.IsMobile);
                    }

                    if (localModelDOR.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
                    {
                        string ptrNumber;
                        decimal onRoadWeight, offRoadWeight;

                        ptrNumber = inAdjMode ? transaction.EffectivePTRNumber : transaction.PTRNumber;

                        if (inAdjMode) onRoadWeight = (transaction.EffectiveOnRoadWeight.HasValue) ? transaction.EffectiveOnRoadWeight.Value : 0.0000M;
                        else onRoadWeight = (transaction.OnRoadWeight.HasValue) ? transaction.OnRoadWeight.Value : 0.0000M;

                        if (inAdjMode) offRoadWeight = (transaction.EffectiveOffRoadWeight.HasValue) ? transaction.EffectiveOffRoadWeight.Value : 0.0000M;
                        else offRoadWeight = (transaction.OffRoadWeight.HasValue) ? transaction.OffRoadWeight.Value : 0.0000M;

                        localModelDOR.TireRimDetail = GetDORTireRimDetail(transaction.OutgoingId, ptrNumber, onRoadWeight, offRoadWeight, period);
                    }
                    break;
            }

            if (model != null)
            {
                if (inAdjMode)
                {
                    // Loading Transaction Item (Tire Type Count) detail
                    model.TireTypeList = GetTireTypeCountDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems);

                    // Loading supporting document detail
                    model.SupportingDocumentDetail.Display = transaction.EffectiveTransactionSupportingDocuments.Any();
                    model.SupportingDocumentDetail.SubmittedBy = GetSupportingDocumentsSubmittedBy(transaction.EffectiveTransactionSupportingDocuments);
                    model.SupportingDocumentDetail.DocumentList = GetSupportingDocumentAttachmentList(transaction.EffectiveTransactionAttachments, false);
                }
                else
                {
                    // Loading Transaction Item (Tire Type Count) detail
                    model.TireTypeList = GetTireTypeCountDetail(transaction.TransactionTypeCode, transaction.OriginalTransactionItems);

                    // Loading supporting document detail
                    model.SupportingDocumentDetail.Display = transaction.OriginalTransactionSupportingDocuments.Any();
                    model.SupportingDocumentDetail.SubmittedBy = GetSupportingDocumentsSubmittedBy(transaction.OriginalTransactionSupportingDocuments);
                    model.SupportingDocumentDetail.DocumentList = GetSupportingDocumentAttachmentList(transaction.OriginalTransactionAttachments, false);
                }
            }

            return model;
        }

        private ITransactionAdjustableDetailViewModel GetTransactionAdjustedDetailModel(Transaction originalTransaction, TransactionAdjustment adjustmentRecord, string vendorTypeCode, TransactionPeriodViewModel period)
        {
            ITransactionAdjustableDetailViewModel model = null;

            switch (originalTransaction.TransactionTypeCode)
            {
                case TreadMarksConstants.DOT:
                    model = new DOTTransactionAdjustableDetailViewModel();
                    var localModelDOT = model as DOTTransactionAdjustableDetailViewModel;

                    localModelDOT.GenerateTires = adjustmentRecord.IsGenerateTires;

                    localModelDOT.ScaleTicketDetail = GetScaleTicketDetails(adjustmentRecord.ScaleTickets, adjustmentRecord.TransactionPhotos, adjustmentRecord.EstimatedWeight);
                    break;

                case TreadMarksConstants.TCR:
                    model = new TCRTransactionAdjustableDetailViewModel();
                    var localModelTCR = model as TCRTransactionAdjustableDetailViewModel;

                    localModelTCR.GenerateTires = adjustmentRecord.IsGenerateTires;
                    break;

                case TreadMarksConstants.HIT:
                    model = new HITTransactionAdjustableDetailViewModel();
                    break;

                case TreadMarksConstants.STC:
                    model = new STCTransactionAdjustableDetailViewModel();
                    var localModelSTC = model as STCTransactionAdjustableDetailViewModel;

                    localModelSTC.EventNumber = adjustmentRecord.EventNumber;

                    localModelSTC.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Group / Individual Name");
                    break;

                case TreadMarksConstants.PTR:
                    model = new PTRTransactionAdjustableDetailViewModel();
                    var localModelPTR = model as PTRTransactionAdjustableDetailViewModel;

                    localModelPTR.ScaleTicketDetail = GetScaleTicketDetails(adjustmentRecord.ScaleTickets, adjustmentRecord.TransactionPhotos, adjustmentRecord.EstimatedWeight);
                    localModelPTR.CalculationSummary = (vendorTypeCode == TreadMarksConstants.Processor) ? GetProcessorCalculationSummaryDetails(originalTransaction.IncomingId, period, adjustmentRecord.TransactionItems) : null;
                    break;

                case TreadMarksConstants.PIT:
                    model = new PITTransactionAdjustableDetailViewModel();
                    var localModelPIT = model as PITTransactionAdjustableDetailViewModel;

                    localModelPIT.ScaleTicketDetail = GetScaleTicketDetails(adjustmentRecord.ScaleTickets, adjustmentRecord.TransactionPhotos, adjustmentRecord.EstimatedWeight, true);
                    localModelPIT.ProductDetail = GetProductDetail(originalTransaction.TransactionTypeCode, adjustmentRecord.TransactionItems.FirstOrDefault());
                    break;

                case TreadMarksConstants.UCR:
                    model = new UCRTransactionAdjustableDetailViewModel();
                    var localModelUCR = model as UCRTransactionAdjustableDetailViewModel;

                    localModelUCR.PaymentEligible = GetPaymentEligibleStr(adjustmentRecord.PaymentEligible, adjustmentRecord.PaymentEligibleOther);

                    if (adjustmentRecord.CompanyInfo != null) localModelUCR.OutboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Unregistered Site Name");
                    else localModelUCR.OutboundParticipant = GetParticipantDetailsFromVendor(adjustmentRecord.VendorInfo, originalTransaction.TransactionTypeCode, TreadMarksConstants.Outbound, originalTransaction.OutgoingGpsLog, originalTransaction.IsMobile);
                    break;

                case TreadMarksConstants.RTR:
                    model = new RTRTransactionAdjustableDetailViewModel();
                    var localModelRTR = model as RTRTransactionAdjustableDetailViewModel;

                    localModelRTR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Buyer Name");
                    break;

                case TreadMarksConstants.SPS:
                    model = new SPSTransactionAdjustableDetailViewModel();
                    var localModelSPS = model as SPSTransactionAdjustableDetailViewModel;

                    localModelSPS.InvoiceNumber = adjustmentRecord.InvoiceNumber;

                    localModelSPS.ScaleTicketDetail = GetScaleTicketDetails(adjustmentRecord.ScaleTickets, adjustmentRecord.TransactionPhotos, adjustmentRecord.EstimatedWeight, true);
                    localModelSPS.ProductDetail = GetProductDetail(originalTransaction.TransactionTypeCode, adjustmentRecord.TransactionItems.FirstOrDefault());

                    if (adjustmentRecord.CompanyInfo != null) localModelSPS.InboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Unregistered RPM");
                    if (adjustmentRecord.CompanyInfo == null) localModelSPS.InboundParticipant = GetParticipantDetailsFromVendor(originalTransaction.IncomingVendor, originalTransaction.TransactionTypeCode, TreadMarksConstants.Inbound, originalTransaction.IncomingGpsLog, originalTransaction.IsMobile);
                    break;

                case TreadMarksConstants.SPSR:
                    model = new SPSRTransactionAdjustableDetailViewModel();
                    var localModelSPSR = model as SPSRTransactionAdjustableDetailViewModel;

                    localModelSPSR.InvoiceNumber = adjustmentRecord.InvoiceNumber;
                    localModelSPSR.ProductDetail = GetProductDetail(originalTransaction.TransactionTypeCode, adjustmentRecord.TransactionItems.FirstOrDefault());
                    localModelSPSR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Sold to Business Name");
                    break;

                case TreadMarksConstants.DOR:
                    model = new DORTransactionAdjustableDetailViewModel();
                    var localModelDOR = model as DORTransactionAdjustableDetailViewModel;

                    localModelDOR.ScaleTicketDetail = GetScaleTicketDetails(adjustmentRecord.ScaleTickets, adjustmentRecord.TransactionPhotos, adjustmentRecord.EstimatedWeight, true);
                    localModelDOR.InvoiceNumber = adjustmentRecord.InvoiceNumber;
                    localModelDOR.MaterialTypeId = adjustmentRecord.MaterialType;
                    localModelDOR.DispositionReasonId = adjustmentRecord.DispositionReason;

                    if (localModelDOR.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue) localModelDOR.UsedTireSaleDetail = GetDORUsedTireSaleDetail(originalTransaction, adjustmentRecord.TransactionItems, period);

                    if (adjustmentRecord.CompanyInfo != null) localModelDOR.InboundParticipant = GetParticipantDetailsFromCompanyInfo(adjustmentRecord.CompanyInfo, "Unregistered Participant");
                    else localModelDOR.InboundParticipant = GetParticipantDetailsFromVendor(adjustmentRecord.VendorInfo, originalTransaction.TransactionTypeCode, TreadMarksConstants.Inbound, originalTransaction.IncomingGpsLog, originalTransaction.IsMobile);

                    if (localModelDOR.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
                    {
                        string ptrNumber = adjustmentRecord.PTRNumber;
                        decimal onRoadWeight = (adjustmentRecord.OnRoadWeight.HasValue) ? adjustmentRecord.OnRoadWeight.Value : 0.0000M;
                        decimal offRoadWeight = (adjustmentRecord.OffRoadWeight.HasValue) ? adjustmentRecord.OffRoadWeight.Value : 0.0000M;

                        localModelDOR.TireRimDetail = GetDORTireRimDetail(originalTransaction.OutgoingId, ptrNumber, onRoadWeight, offRoadWeight, period);
                    }
                    break;
            }

            if (model != null)
            {
                // Loading Transaction Item (Tire Type Count) detail
                model.TireTypeList = GetTireTypeCountDetail(originalTransaction.TransactionTypeCode, adjustmentRecord.TransactionItems, originalTransaction.OriginalTransactionItems);

                // Loading supporting document detail
                model.SupportingDocumentDetail.Display = adjustmentRecord.TransactionSupportingDocuments.Any();
                model.SupportingDocumentDetail.SubmittedBy = GetSupportingDocumentsSubmittedBy(adjustmentRecord.TransactionSupportingDocuments);
                model.SupportingDocumentDetail.DocumentList = GetSupportingDocumentAttachmentList(adjustmentRecord.TransactionAttachments, false);
            }

            return model;
        }

        private TransactionViewParticipantViewModel GetParticipantDetailsFromVendor(Vendor vendor, string type, int loadingVendorDirection, GpsLog gpsLog, bool isMobile)
        {
            string participantLabel = null;
            var participantDetail = new TransactionViewParticipantViewModel() { ValidInfo = true, ParticipantLabel = participantLabel };

            if (vendor == null)
            {
                participantDetail.ValidInfo = false;
                participantDetail.ParticipantLabel = (loadingVendorDirection == TreadMarksConstants.Inbound) ? "Inbound Participant" : "Outbound Participant";

                return participantDetail;
            }

            if (loadingVendorDirection == TreadMarksConstants.Inbound)
            {
                if (type == TreadMarksConstants.HIT) participantLabel = "Inbound Hauler";
                if (type == TreadMarksConstants.PIT) participantLabel = "Inbound Processor";
            }

            if (loadingVendorDirection == TreadMarksConstants.Outbound)
            {
                if (type == TreadMarksConstants.HIT) participantLabel = "Outbound Hauler";
                if (type == TreadMarksConstants.PIT) participantLabel = "Outbound Processor";
            }

            participantDetail.ParticipantLabel = string.IsNullOrEmpty(participantLabel) ? AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.VendorType, vendor.VendorType).Name : participantLabel;
            participantDetail.ParticipantNumber = vendor.Number;
            participantDetail.CompanyName = vendor.BusinessName;
            participantDetail.DisplayParticipantNumber = true;

            var vendorAddress = vendor.VendorAddresses.Any(i => i.AddressType == (int)AddressTypeEnum.Business) ? vendor.VendorAddresses.FirstOrDefault(i => i.AddressType == (int)AddressTypeEnum.Business) : vendor.VendorAddresses.FirstOrDefault();
            if (vendorAddress != null)
            {
                participantDetail.PhoneNumber = vendorAddress.Phone;
                participantDetail.Address1 = vendorAddress.Address1;
                participantDetail.Address2 = vendorAddress.Address2;
                participantDetail.City = vendorAddress.City;
                participantDetail.Province = vendorAddress.Province;
                participantDetail.PostalCode = vendorAddress.PostalCode;
                participantDetail.Country = vendorAddress.Country;
            }

            // GPS marker display detail
            if (isMobile)
            {
                if ((loadingVendorDirection == TreadMarksConstants.Outbound) && (type == TreadMarksConstants.DOT || type == TreadMarksConstants.TCR))
                {
                    participantDetail.ShowMapMarker = true;
                    if (gpsLog != null) participantDetail.Gps = gpsLog.Latitude + ", " + gpsLog.Longitude;
                }

                if ((loadingVendorDirection == TreadMarksConstants.Inbound) && (type == TreadMarksConstants.PTR))
                {
                    participantDetail.ShowMapMarker = true;
                    if (gpsLog != null) participantDetail.Gps = gpsLog.Latitude + ", " + gpsLog.Longitude;
                }

                if ((type == TreadMarksConstants.HIT || type == TreadMarksConstants.PIT) && loadingVendorDirection == TreadMarksConstants.Inbound)
                {
                    participantDetail.ShowMapMarker = true;
                    if (gpsLog != null) participantDetail.Gps = gpsLog.Latitude + ", " + gpsLog.Longitude;
                }
            }
            else participantDetail.ShowMapMarker = false;

            return participantDetail;
        }

        private TransactionViewParticipantViewModel GetParticipantDetailsFromCompanyInfo(CompanyInfo companyInfo, string participantLabel, bool showMapMarker = false)
        {
            var participantDetail = new TransactionViewParticipantViewModel() { ValidInfo = true, ParticipantLabel = participantLabel };

            if (companyInfo == null)
            {
                participantDetail.ValidInfo = false;

                return participantDetail;
            }

            participantDetail.CompanyName = companyInfo.CompanyName;
            participantDetail.PhoneNumber = companyInfo.PhoneNumber;
            participantDetail.DisplayParticipantNumber = false;
            participantDetail.ShowMapMarker = showMapMarker;

            var address = companyInfo.Address;
            if (address != null)
            {
                participantDetail.Address1 = address.Address1;
                participantDetail.Address2 = address.Address2;
                participantDetail.City = address.City;
                participantDetail.Province = address.Province;
                participantDetail.PostalCode = address.PostalCode;
                participantDetail.Country = address.Country;
            }

            return participantDetail;
        }

        private List<TransactionViewTireTypeViewModel> GetTireTypeCountDetail(string type, List<TransactionItem> transactionItems, List<TransactionItem> originalTransactionItems = null)
        {
            var tireTypeCountList = new List<TransactionViewTireTypeViewModel>();

            var availableTireTypeList = TransactionHelper.GetAvailableTireTypeListTireTypeList(type);
            foreach (var tireType in availableTireTypeList)
            {
                var tireTypeId = TransactionHelper.GetTransactionTireTypeId(tireType);
                var transactionItem = transactionItems.FirstOrDefault(i => i.ItemID == tireTypeId);
                var tireTypeCount = (transactionItem != null && transactionItem.Quantity.HasValue) ? transactionItem.Quantity.Value : 0;

                var changed = (originalTransactionItems != null) ? originalTransactionItems.Any(i => i.ItemID == tireTypeId && i.Quantity != tireTypeCount) : false;
                tireTypeCountList.Add(new TransactionViewTireTypeViewModel() { TireType = tireType, Count = tireTypeCount, Changed = changed });
            }

            return tireTypeCountList;
        }

        private TransactionViewScaleTicketDetailViewModel GetScaleTicketDetails(IEnumerable<ScaleTicket> scaleTickets, IEnumerable<TransactionPhoto> photos, decimal estimatedWeight, bool inTon = false)
        {
            ScaleTicket scaleTicket;
            var netScaleWeight = GetNetScaleTicketWeight(scaleTickets);

            var scaleTicketDetailViewModel = new TransactionViewScaleTicketDetailViewModel()
            {
                SingleTicketType = scaleTickets.Any(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both),
                WeightType = inTon ? TreadMarksConstants.Ton : TreadMarksConstants.Kg,
                Unit = inTon ? "TON" : "KG",
                NetWeight = inTon ? Math.Round(DataConversion.ConvertKgToTon((double)netScaleWeight), 4, MidpointRounding.AwayFromZero) : netScaleWeight,
                Variance = GetVariance(estimatedWeight, netScaleWeight, scaleTickets.Any())
            };

            if (scaleTicketDetailViewModel.SingleTicketType)
            {
                scaleTicket = scaleTickets.First(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both);
                scaleTicketDetailViewModel.Tickets.Add(GetScaleTicketViewModel(scaleTicket, inTon));
            }
            else
            {
                scaleTicket = scaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Inbound);
                if (scaleTicket != null) scaleTicketDetailViewModel.Tickets.Add(GetScaleTicketViewModel(scaleTicket, inTon));

                scaleTicket = scaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Outbound);
                if (scaleTicket != null) scaleTicketDetailViewModel.Tickets.Add(GetScaleTicketViewModel(scaleTicket, inTon));
            }

            if (photos != null) scaleTicketDetailViewModel.TicketPhotos.AddRange(photos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.ScaleTicket).Select(p => p.Photo.FileName).ToList());

            return scaleTicketDetailViewModel;
        }

        private TransactionViewProductViewModel GetProductDetail(string type, TransactionItem transactionItem)
        {
            if (transactionItem == null) return null;

            Item item = null;
            if (type == TreadMarksConstants.PIT) item = DataLoader.ProcessorPITTransactionProducts.SingleOrDefault(i => i.ID == transactionItem.ItemID);
            if (type == TreadMarksConstants.SPS) item = DataLoader.ProcessorSPSTransactionProducts.SingleOrDefault(i => i.ID == transactionItem.ItemID);
            if (type == TreadMarksConstants.SPSR) item = DataLoader.RPMSPSTransactionProducts.SingleOrDefault(i => i.ID == transactionItem.ItemID);
            if (item == null) return null;

            TransactionViewProductViewModel productDetail = new TransactionViewProductViewModel()
            {
                ShortName = item.Description,
                Name = item.Name,

                Rate = Math.Round(transactionItem.Rate.Value, 3, MidpointRounding.AwayFromZero),
                Amount = Math.Round((transactionItem.Rate.Value * Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero)), 2, MidpointRounding.AwayFromZero)
            };

            if (type == TreadMarksConstants.SPS)
            {
                productDetail.MeshRangeStart = transactionItem.MeshRangeStart;
                productDetail.MeshRangeEnd = transactionItem.MeshRangeEnd;
                productDetail.IsFreeOfSteel = transactionItem.IsFreeOfSteel;
                productDetail.ProductDescription = transactionItem.ItemDescription;
            }

            if (type == TreadMarksConstants.SPSR)
            {
                productDetail.RecyclablePercentage = transactionItem.RecyclablePercentage;
                productDetail.TotalTDPWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                productDetail.ProductCode = transactionItem.ItemCode;
                productDetail.ProductDescription = transactionItem.ItemDescription;
            }

            return productDetail;
        }

        private TransactionViewDORTireRimViewModel GetDORTireRimDetail(int? outgoingId, string ptrNumber, decimal onRoadWeight, decimal offRoadWeight, TransactionPeriodViewModel period)
        {
            var claimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
            var paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;
            var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
            var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;

            //Rate Group Rate changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(period.StartDate, period.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            var onRoadRate = 0.000m;
            var offRoadRate = 0.000m;

            if (outgoingId != null)
            {
                var deliveryGroup = vendorRateGroups.FirstOrDefault(q => q.VendorId == outgoingId);
                if (deliveryGroup != null)
                {
                    var onRoadGroupRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == paymentTypeId && r.ItemType == OnRoadItemTypeId && r.ProcessorGroupId == deliveryGroup.GroupId);
                    onRoadRate = onRoadGroupRate != null ? onRoadGroupRate.Rate : 0;
                    var offRoadGroupRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == paymentTypeId && r.ItemType == OffRoadItemTypeId && r.ProcessorGroupId == deliveryGroup.GroupId);
                    offRoadRate = offRoadGroupRate != null ? offRoadGroupRate.Rate : 0;
                }
            }

            var dorTireRimDetail = new TransactionViewDORTireRimViewModel()
            {
                PTRNumber = ptrNumber,
                OnRoadScaleWeight = Math.Round(DataConversion.ConvertKgToTon((double)onRoadWeight), 4, MidpointRounding.AwayFromZero),
                OffRoadScaleWeight = Math.Round(DataConversion.ConvertKgToTon((double)offRoadWeight), 4, MidpointRounding.AwayFromZero),

                OnRoadRate = Math.Round(DataConversion.ConvertTonToKg((double)onRoadRate), 3, MidpointRounding.AwayFromZero),
                OffRoadRate = Math.Round(DataConversion.ConvertTonToKg((double)offRoadRate), 3, MidpointRounding.AwayFromZero),

                Amount = Math.Round(((onRoadWeight * onRoadRate) + (offRoadWeight * offRoadRate)), 2, MidpointRounding.AwayFromZero)
            };

            return dorTireRimDetail;
        }

        private TransactionViewDORUsedTireSaleViewModel GetDORUsedTireSaleDetail(Transaction transaction, List<TransactionItem> transactionItems, TransactionPeriodViewModel period)
        {
            var dorUsedTireSaleDetail = new TransactionViewDORUsedTireSaleViewModel()
            {
                TireTypeList = GetTireTypeCountDetail(transaction.TransactionTypeCode, transactionItems),
                CalculationSummary = GetProcessorCalculationSummaryDetails(transaction.OutgoingId, period, transactionItems, true)
            };

            return dorUsedTireSaleDetail;
        }

        private ProcessorCalculationSummaryViewModel GetProcessorCalculationSummaryDetails(int? incomingId, TransactionPeriodViewModel period, IEnumerable<TransactionItem> transactionItems, bool inTon = false)
        {
            if (period == null) return null;

            var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
            var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;

            var claimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
            var paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;

            //Rate Group Rate Changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(period.StartDate, period.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            var onRoadRate = 0.000m;
            var offRoadRate = 0.000m;

            if (incomingId != null)
            {
                var deliveryGroup = vendorRateGroups.FirstOrDefault(q => q.VendorId == incomingId);
                if (deliveryGroup != null)
                {
                    var onRoadGroupRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == paymentTypeId && r.ItemType == OnRoadItemTypeId && r.ProcessorGroupId == deliveryGroup.GroupId);
                    onRoadRate = onRoadGroupRate != null ? onRoadGroupRate.Rate : 0;
                    var offRoadGroupRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == paymentTypeId && r.ItemType == OffRoadItemTypeId && r.ProcessorGroupId == deliveryGroup.GroupId);
                    offRoadRate = offRoadGroupRate != null ? offRoadGroupRate.Rate : 0;
                }
            }

            var calculationItemList = new List<ProcessorCalculationItemViewModel>();
            foreach (var ti in transactionItems)
            {
                var item = DataLoader.TransactionItems.Single(c => c.ID == ti.ItemID);

                var calculationItem = new ProcessorCalculationItemViewModel()
                {
                    Name = item.ShortName,
                    EstWeight = inTon ? DataConversion.ConvertKgToTon((double)ti.AverageWeight) : ti.AverageWeight,
                    ScaleWeight = inTon ? DataConversion.ConvertKgToTon((double)ti.ActualWeight) : ti.ActualWeight
                };

                if (item.ItemType == OnRoadItemTypeId) calculationItem.Rate = onRoadRate;
                if (item.ItemType == OffRoadItemTypeId) calculationItem.Rate = offRoadRate;

                calculationItem.Rate = (inTon) ? Math.Round(DataConversion.ConvertTonToKg((double)calculationItem.Rate), 3, MidpointRounding.AwayFromZero) : Math.Round(calculationItem.Rate, 3, MidpointRounding.AwayFromZero);

                calculationItem.Amount = Math.Round((calculationItem.ScaleWeight * calculationItem.Rate), 4, MidpointRounding.AwayFromZero);

                calculationItemList.Add(calculationItem);
            }

            var tireTypeOrderedList = new List<string>() { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR };
            var model = new ProcessorCalculationSummaryViewModel() { ItemList = new List<ProcessorCalculationItemViewModel>() };
            tireTypeOrderedList.ForEach(c =>
            {
                model.ItemList.Add(calculationItemList.Find(i => i.Name == c));
            });
            model.Amount = Math.Round(model.ItemList.Sum(i => i.Amount), 2, MidpointRounding.AwayFromZero);

            return model;
        }

        public TransactionReviewToolBar getUpdatedReviewToolBar(int transactionId, int? claimId = 0)
        {
            var claimAssociation = claimsRepository.GetTransactionClaimAssociationDetailDataModel(transactionId);

            var currentClaimStatus = claimAssociation.Associations.Where(c => c.ClaimId == claimId).FirstOrDefault().Status.Replace(" ", "");

            if (currentClaimStatus == ClaimStatus.UnderReview.ToString())
            {
                var reviewToolBar = new TransactionReviewToolBar();
                var transaction = transactionRepository.GetTransactionById(transactionId);
                reviewToolBar.hasPendingAdjustments = transaction.IsAdjustmentPending;
                string otherClaimStatus = claimsRepository.checkOtherClaimStatus(claimId ?? 0, transactionId);
                reviewToolBar.IsOtherClaimApproved = string.Equals(otherClaimStatus, "Approved");
                return reviewToolBar;
            }

            return null;
        }
        #endregion


        #region Transaction Adjustments
        public ITransactionAdjViewModel GetTransactionAdjustmentModel(int transactionId, TransactionPeriodViewModel period = null)
        {
            var currentVendorId = SecurityContextHelper.CurrentVendor.VendorId;

            var transaction = transactionRepository.GetTransactionById(transactionId);

            //OTSTM2-391
            if (transaction.TransactionAdjustments.Any())
            {
                bool hasPendingOrAccepted = transaction.TransactionAdjustments.Where(i => i.Status == TransactionAdjustmentStatus.Pending.ToString() || i.Status == TransactionAdjustmentStatus.Accepted.ToString()).Any();

                if (hasPendingOrAccepted)
                {
                    transaction.AdjustmentRecord = transactionRepository.GetTransactionAdjustmentById(transaction.ActiveAdjustmentId);
                }
            }

            var claimAssociation = claimsRepository.GetTransactionClaimAssociationDetailDataModel(transactionId);
            period = GetTransactionPeriod(transaction, claimAssociation, currentVendorId);

            return GetTransactionAdjustmentModel(transaction, true, period);
        }

        public void AdjustTransaction(ITransactionAdjViewModel model, Period claimPeriod)
        {
            var transaction = transactionRepository.GetTransactionById(model.TransactionId);
            var acceptedAdjustmentExists = transaction.IsAdjustmentAccepted;
            TransactionAdjustment adjustment = null;

            switch (model.TransactionType)
            {
                case TreadMarksConstants.DOT:
                    adjustment = AdjustDOTTransaction(model as DOTTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.TCR:
                    adjustment = AdjustTCRTransaction(model as TCRTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.HIT:
                    adjustment = AdjustHITTransaction(model as HITTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.STC:
                    adjustment = AdjustSTCTransaction(model as STCTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.PTR:
                    adjustment = AdjustPTRTransaction(model as PTRTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.PIT:
                    adjustment = AdjustPITTransaction(model as PITTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.UCR:
                    adjustment = AdjustUCRTransaction(model as UCRTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.RTR:
                    adjustment = AdjustRTRTransaction(model as RTRTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.SPS:
                    adjustment = AdjustSPSTransaction(model as SPSTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.SPSR:
                    adjustment = AdjustSPSRTransaction(model as SPSRTransactionAdjViewModel, transaction, claimPeriod);
                    break;

                case TreadMarksConstants.DOR:
                    adjustment = AdjustDORTransaction(model as DORTransactionAdjViewModel, transaction, claimPeriod);
                    break;
            }

            // Adjusted transaction "System Rejested" by adding a new adjustment, recalculate claim (add new adjustment, system rejests any existing accepted adjustment)
            if (acceptedAdjustmentExists || (adjustment != null && adjustment.Status == TransactionAdjustmentStatus.Accepted.ToString())) RecalculateClaims(model.TransactionId);
        }

        private TransactionAdjustment AdjustDOTTransaction(DOTTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.IsGenerateTires = model.GenerateTires;
            adjustment.IsEligible = !model.GenerateTires;

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Scale Ticket adjustment    
            // Paper
            if (transaction.IsPaper && model.ScaleTicket != null && !string.IsNullOrEmpty(model.ScaleTicket.TicketNumber))
            {
                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);

                //Override transaction item actual weight
                if (scaleTicket.InboundWeight != null) DistributeAdjustmentScaleWeight(adjustment, scaleTicket.InboundWeight.Value);
            }

            // Mobile
            if (transaction.IsMobile && model.ScaleTicketDetail != null)
            {
                var scaleTickets = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicketDetail, adjustment.AdjustmentDate);
                scaleTickets.ForEach(i => i.TransactionId = model.TransactionId);
                adjustment.ScaleTickets.AddRange(scaleTickets);

                //Override transaction item actual weight
                var totalWeight = scaleTickets.Sum(i => i.InboundWeight.Value - i.OutboundWeight.Value);
                DistributeAdjustmentScaleWeight(adjustment, totalWeight);
            }

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustTCRTransaction(TCRTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.IsGenerateTires = model.GenerateTires;
            adjustment.IsEligible = !model.GenerateTires;

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustHITTransaction(HITTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustSTCTransaction(STCTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

            // Event Number adjustment
            adjustment.EventNumber = model.EventNumber;

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Unregistered Company adjustment
            ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            //OTSTM2-940
            var fromPostalCode = adjustment.CompanyInfo.Address.PostalCode;//fix for claim calculation trigger

            adjustment = transactionRepository.AddSTCTransactionAdjustment(adjustment, fromPostalCode);

            return adjustment;
        }

        private TransactionAdjustment AdjustPTRTransaction(PTRTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Scale Ticket adjustment    
            // Paper
            if (transaction.IsPaper && model.ScaleTicket != null && !string.IsNullOrEmpty(model.ScaleTicket.TicketNumber))
            {
                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);

                //Override transaction item actual weight
                if (scaleTicket.InboundWeight != null) DistributeAdjustmentScaleWeight(adjustment, scaleTicket.InboundWeight.Value);
            }

            // Mobile
            if (transaction.IsMobile && model.ScaleTicketDetail != null)
            {
                var scaleTickets = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicketDetail, adjustment.AdjustmentDate);
                scaleTickets.ForEach(i => i.TransactionId = model.TransactionId);
                adjustment.ScaleTickets.AddRange(scaleTickets);

                //Override transaction item actual weight
                var totalWeight = scaleTickets.Sum(i => i.InboundWeight.Value - i.OutboundWeight.Value);
                DistributeAdjustmentScaleWeight(adjustment, totalWeight);
            }

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustPITTransaction(PITTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);

            // Scale Ticket adjustment    
            // Paper
            if (transaction.IsPaper && model.ScaleTicket != null && !string.IsNullOrEmpty(model.ScaleTicket.TicketNumber))
            {
                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);
            }

            // Mobile
            if (transaction.IsMobile && model.ScaleTicketDetail != null)
            {
                var scaleTickets = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicketDetail, adjustment.AdjustmentDate);
                scaleTickets.ForEach(i => i.TransactionId = model.TransactionId);
                adjustment.ScaleTickets.AddRange(scaleTickets);
            }

            //OTSTM2-1198 over-writing the itemrate for Inbound PIT for product selected from UI as it should use outbound processor's rates
            decimal ItemRate = 0.00M;
            var specificRate = DataLoader.ProcessorSpecificRates.FirstOrDefault(c => c.ItemID == model.ProductDetail.ProductTypeId && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= claimPeriod.StartDate.Date && c.EffectiveEndDate >= claimPeriod.EndDate.Date && c.VendorID == transaction.OutgoingId);
            if (specificRate != null)
            {
                ItemRate = specificRate.ItemRate;
            }
            else
            {
                Rate globalRate;
                globalRate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == model.ProductDetail.ProductTypeId && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= claimPeriod.StartDate.Date && c.EffectiveEndDate >= claimPeriod.EndDate.Date);
                ItemRate = globalRate != null ? globalRate.ItemRate : 0.00M;
            }

            model.ProductDetail.ProductRate = ItemRate;

            // Product Transaction Item adjustment
            var transactionItem = PrepareProductTransactionItem(model.ProductDetail, GetNetScaleTicketWeight(adjustment.ScaleTickets), model.TransactionType);
            transactionItem.TransactionId = model.TransactionId;
            adjustment.TransactionItems.Add(transactionItem);

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustUCRTransaction(UCRTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

            // Payment Eligible adjustment
            adjustment.PaymentEligible = int.Parse(model.PaymentEligible);
            if (transaction.PaymentEligible == 4) adjustment.PaymentEligibleOther = model.PaymentEligibleOther;

            // PickedUp From adjustment
            if (model.PickedUpFrom.ToLower() == "unregistered") ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);
            else
            {
                var vendor = vendorRepository.GetVendorByNumber(model.CollectorNumber);
                adjustment.VendorInfoId = vendor.VendorId;
            }

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Transaction Photo adjustment
            if (transaction.IsMobile)
            {
                var photoList = transaction.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionPhoto() { TransactionId = i.TransactionId, PhotoId = i.PhotoId }).ToList();
                if (photoList.Any()) adjustment.TransactionPhotos.AddRange(photoList);
            }

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustRTRTransaction(RTRTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

            // Transaction Item adjustment
            ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

            // Unregistered Company adjustment
            ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustSPSTransaction(SPSTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            List<string> result = null;

            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            //InvoiceNumber
            adjustment.InvoiceNumber = model.InvoiceNumber;

            // Unregistered Company adjustment 
            if (model.SoldTo == "unregistered")
            {
                adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

                ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);

                //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 
                result = ProcessTransAdjScaleTicketPaper(transaction, model.ScaleTicket.TicketNumber);
                //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 
            }

            // Scale Ticket adjustment                
            if (transaction.IsPaper && model.ScaleTicket != null && !string.IsNullOrEmpty(model.ScaleTicket.TicketNumber))
            {
                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);
            }

            // Product Transaction Item adjustment
            var transactionItem = PrepareProductTransactionItem(model.ProductDetail, GetNetScaleTicketWeight(adjustment.ScaleTickets), model.TransactionType);
            transactionItem.TransactionId = model.TransactionId;
            adjustment.TransactionItems.Add(transactionItem);

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Unregistered Company adjustment 
            if (model.SoldTo == "unregistered")
            {
                //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 
                if (result != null && result.Count > 0)
                {
                    result.ForEach(note =>
                    {
                        AddTransactionNote(model.TransactionId, note);
                    });
                }
            }

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustSPSRTransaction(SPSRTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

            //InvoiceNumber
            adjustment.InvoiceNumber = model.InvoiceNumber;

            // Unregistered Company adjustment
            ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);

            // Product Transaction Item adjustment
            decimal? weightInKg = null;
            if (model.ProductDetail.TotalTDPWeight.HasValue)
            {

                switch (model.ProductDetail.UnitType)
                {
                    case TreadMarksConstants.Lb://1
                        weightInKg = Math.Round(DataConversion.ConvertLbToKg((double)model.ProductDetail.TotalTDPWeight.Value), 4, MidpointRounding.AwayFromZero);
                        break;
                    case TreadMarksConstants.Kg://2
                        weightInKg = Math.Round(Convert.ToDecimal((double)model.ProductDetail.TotalTDPWeight.Value), 4, MidpointRounding.AwayFromZero);
                        break;
                    case TreadMarksConstants.Ton://3
                        weightInKg = Math.Round(DataConversion.ConvertTonToKg((double)model.ProductDetail.TotalTDPWeight.Value), 4, MidpointRounding.AwayFromZero);
                        break;
                    default:
                        break;
                }
            }
            var transactionItem = PrepareProductTransactionItem(model.ProductDetail, weightInKg, model.TransactionType);
            transactionItem.TransactionId = model.TransactionId;
            adjustment.TransactionItems.Add(transactionItem);

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        private TransactionAdjustment AdjustDORTransaction(DORTransactionAdjViewModel model, Transaction transaction, Period claimPeriod)
        {
            var adjustment = PrepareTransactionAdjustment(model.TransactionId);
            adjustment.Status = TransactionAdjustmentStatus.Accepted.ToString();

            //InvoiceNumber
            adjustment.InvoiceNumber = model.InvoiceNumber;
            adjustment.MaterialType = model.ProductDetail.MaterialTypeId;
            adjustment.DispositionReason = model.ProductDetail.DispositionReasonId;

            // Transaction Item (Tire Type transacttion items) for only if Material Type is Used Tire Sale
            if (model.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue)
            {
                ApplyTransactionItemAdjustment(adjustment, model.TireTypeList, model.TransactionId, transaction.TransactionDate);

                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);

                //Override transaction item actual weight
                if (scaleTicket.InboundWeight != null) DistributeAdjustmentScaleWeight(adjustment, scaleTicket.InboundWeight.Value);
            }

            if (model.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Trash").DefinitionValue ||
                model.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Steel").DefinitionValue ||
                model.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "Fibre").DefinitionValue)
            {
                var scaleTicket = PrepareScaleTicketFromTransactionScaleTicketViewModel(model.ScaleTicket, ScaleTicketTypeEnum.Both, adjustment.AdjustmentDate);
                scaleTicket.TransactionId = model.TransactionId;
                adjustment.ScaleTickets.Add(scaleTicket);
            }

            if (model.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
            {
                transaction.PTRNumber = model.ProductDetail.PTRNumber;
                adjustment.PTRNumber = model.ProductDetail.PTRNumber;

                if (model.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Kg) transaction.OnRoadWeight = Convert.ToDecimal(model.ProductDetail.OnRoadScaleWeight.Weight.Value);
                if (model.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Ton) transaction.OnRoadWeight = DataConversion.ConvertTonToKg(model.ProductDetail.OnRoadScaleWeight.Weight.Value);
                if (model.ProductDetail.OnRoadScaleWeight.WeightType == TreadMarksConstants.Lb) transaction.OnRoadWeight = DataConversion.ConvertLbToKg(model.ProductDetail.OnRoadScaleWeight.Weight.Value);
                adjustment.OnRoadWeight = transaction.OnRoadWeight;

                if (model.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Kg) transaction.OffRoadWeight = Convert.ToDecimal(model.ProductDetail.OffRoadScaleWeight.Weight.Value);
                if (model.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Ton) transaction.OffRoadWeight = DataConversion.ConvertTonToKg(model.ProductDetail.OffRoadScaleWeight.Weight.Value);
                if (model.ProductDetail.OffRoadScaleWeight.WeightType == TreadMarksConstants.Lb) transaction.OffRoadWeight = DataConversion.ConvertLbToKg(model.ProductDetail.OffRoadScaleWeight.Weight.Value);
                adjustment.OffRoadWeight = transaction.OffRoadWeight;
            }

            if (model.SoldTo.ToLower() == "unregistered") ApplyUnregisteredCompanyAdjustment(adjustment, model.CompanyInfo);

            // Transaction Supporting Document adjustment
            ApplySupportingDocumentAdjustment(adjustment, model.SupportingDocumentList, model.TransactionId);

            // Transaction Supporting Document Attachment adjustment
            ApplySupportingDocumentAttachmentAdjustment(adjustment, model.AttachmentList, model.UploadDocumentList, model.TransactionId);

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(model.AdjustmentNote)) AddTransactionNote(model.TransactionId, model.AdjustmentNote);

            // Add adjustment
            adjustment = transactionRepository.AddTransactionAdjustment(adjustment);

            return adjustment;
        }

        public void SubmitAdjustmentStatusChange(int transactionId, TransactionAdjustmentStatus reviewStatus, bool getUpdatedData, string notes)
        {
            List<string> result = null;

            var transaction = transactionRepository.GetTransactionById(transactionId);

            var tiggerClaimCalculation = false;
            var adjustmentRecord = transactionRepository.GetTransactionAdjustmentById(transaction.ActiveAdjustmentId);
            if (adjustmentRecord != null)
            {
                transaction.AdjustmentRecord = adjustmentRecord;

                tiggerClaimCalculation = (reviewStatus == TransactionAdjustmentStatus.Accepted) || (reviewStatus != TransactionAdjustmentStatus.Accepted && adjustmentRecord.Status == TransactionAdjustmentStatus.Accepted.ToString());

                //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 
                if (transaction.IsPaper)
                {
                    result = ProcessTransAdjScaleTicketPaper(transaction);
                }
                else
                {
                    //if mobile
                    result = ProcessTransAdjScaleTicketMobile(transaction);
                }
                //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 

                transactionRepository.ChangeTransactionAdjustmentStatus(adjustmentRecord.ID, reviewStatus, SecurityContextHelper.CurrentUser.Id, transactionId);
            }

            // Add Transaction Adjustment Note
            if (!string.IsNullOrEmpty(notes)) AddTransactionNote(transactionId, notes);

            //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 
            if (result != null && result.Count > 0)
            {
                result.ForEach(note =>
                {
                    AddTransactionNote(transactionId, note);
                });
            }
            //---------------OTSTM2-91 Scale Ticket validation ---------------------------- 

            // Recalculate claim
            if (tiggerClaimCalculation) RecalculateClaimsForTransactionAdjustment(transactionId); //OTSTM2-1142
            //if (tiggerClaimCalculation) RecalculateClaims(transactionId); 
        }

        private TransactionAdjustment PrepareTransactionAdjustment(int adjustedTransactionId)
        {
            var adjustment = new TransactionAdjustment()
            {
                AdjustmentDate = DateTime.UtcNow,
                StartedBy = SecurityContextHelper.CurrentUser.Id,
                CreatedVendorId = SecurityContextHelper.CurrentVendor.VendorId,
                Status = TransactionAdjustmentStatus.Pending.ToString(),
                OriginalTransactionId = adjustedTransactionId,
                IsAdjustByStaff = SecurityContextHelper.IsStaff()
            };

            return adjustment;
        }

        private void ApplyTransactionItemAdjustment(TransactionAdjustment adjustment, List<TransactionTireTypeViewModel> tireTypeList, int originalTransactionId, DateTime effectiveOn)
        {
            adjustment.TransactionItems.AddRange(PrepareTransactionItemList(tireTypeList, effectiveOn));
            adjustment.TransactionItems.ForEach(i => { i.TransactionId = originalTransactionId; });

            adjustment.OnRoadWeight = GetOnRoadWeight(adjustment.TransactionItems);
            adjustment.OffRoadWeight = GetOffRoadWeight(adjustment.TransactionItems);

            adjustment.EstimatedOnRoadWeight = adjustment.OnRoadWeight;
            adjustment.EstimatedOffRoadWeight = adjustment.OffRoadWeight;
        }

        private void ApplyUnregisteredCompanyAdjustment(TransactionAdjustment adjustment, TransactionUnregisteredCompanyViewModel companyInfo)
        {
            adjustment.CompanyInfo = PrepareUnregisteredCompany(companyInfo);
        }

        private void ApplySupportingDocumentAdjustment(TransactionAdjustment adjustment, List<TransactionSupportingDocumentViewModel> supportingDocumentList, int originalTransactionId)
        {
            foreach (var document in supportingDocumentList)
            {
                if (document.Required) adjustment.TransactionSupportingDocuments.Add(new TransactionSupportingDocument() { DocumentName = document.DocumentName, DocumentFormat = document.DocumentFormat });
            }

            adjustment.TransactionSupportingDocuments.ForEach(i => { i.TransactionId = originalTransactionId; });
        }

        private void ApplySupportingDocumentAttachmentAdjustment(TransactionAdjustment adjustment, List<TransactionViewSupportingDocumentAttachmentViewModel> AttachmentList, List<TransactionFileUploadModel> UploadDocumentList, int originalTransactionId)
        {
            if (AttachmentList != null)
            {
                AttachmentList.ForEach(i =>
                {
                    if (!i.IsRemoved) adjustment.TransactionAttachments.Add(new TransactionAttachment { AttachmentId = i.AttachmentId, Description = i.Description });
                });
            }

            if (UploadDocumentList != null)
            {
                UploadDocumentList.ForEach(i =>
                {
                    if (i.AttachmentId.HasValue) adjustment.TransactionAttachments.Add(new TransactionAttachment { AttachmentId = i.AttachmentId.Value, Description = i.Description });
                });
            }

            adjustment.TransactionAttachments.ForEach(i => { i.TransactionId = originalTransactionId; });

            // Also, if there is any new attachments, update the descriptions
            if (UploadDocumentList != null) transactionRepository.UpdateAttachmentDescription(UploadDocumentList);
        }

        private ITransactionAdjViewModel GetTransactionAdjustmentModel(Transaction transaction, bool loadAdjustmentModel, TransactionPeriodViewModel period)//period: claim period
        {
            ITransactionAdjViewModel model = null;
            if (!loadAdjustmentModel) return model;

            switch (transaction.TransactionTypeCode)
            {
                case TreadMarksConstants.DOT:
                    model = new DOTTransactionAdjViewModel()
                    {
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems),
                        GenerateTires = transaction.EffectiveGenerateTires,
                        ScaleTicket = (transaction.IsPaper) ? GetScaleTicketAdjustmentModel(transaction.EffectiveScaleTickets.FirstOrDefault()) : null,
                        ScaleTicketDetail = (transaction.IsMobile) ? GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight) : null
                    };
                    break;

                case TreadMarksConstants.TCR:
                    model = new TCRTransactionAdjViewModel()
                    {
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems),
                        GenerateTires = transaction.EffectiveGenerateTires
                    };
                    break;

                case TreadMarksConstants.HIT:
                    model = new HITTransactionAdjViewModel()
                    {
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems)
                    };
                    break;

                case TreadMarksConstants.STC:
                    model = new STCTransactionAdjViewModel()
                    {
                        EventNumber = transaction.EffectiveEventNumber,
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems),
                        CompanyInfo = GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo)
                    };
                    break;

                case TreadMarksConstants.PTR:
                    model = new PTRTransactionAdjViewModel()
                    {
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems),
                        ScaleTicket = (transaction.IsPaper) ? GetScaleTicketAdjustmentModel(transaction.EffectiveScaleTickets.FirstOrDefault()) : null,
                        ScaleTicketDetail = (transaction.IsMobile) ? GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight) : null
                    };
                    break;

                case TreadMarksConstants.PIT:
                    model = new PITTransactionAdjViewModel()
                    {
                        ScaleTicket = (transaction.IsPaper) ? GetScaleTicketAdjustmentModel(transaction.EffectiveScaleTickets.FirstOrDefault(), true) : null,
                        ScaleTicketDetail = (transaction.IsMobile) ? GetScaleTicketDetails(transaction.EffectiveScaleTickets, transaction.EffectiveTransactionPhotos, transaction.EffectiveEstimatedWeight, true) : null,
                        ProductDetail = (ProcessorPITProductViewModel)GetProductAdjustmentDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault()),
                        ProductList = TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue, transaction.OutgoingId.Value, period.StartDate, period.EndDate)
                    };
                    break;

                case TreadMarksConstants.UCR:
                    model = new UCRTransactionAdjViewModel()
                    {
                        PaymentEligible = transaction.EffectivePaymentEligible.ToString(),
                        PaymentEligibleOther = transaction.EffectivePaymentEligibleOther,
                        CollectorNumber = (transaction.EffectiveVendorInfo != null) ? transaction.EffectiveVendorInfo.Number : "",
                        CompanyInfo = GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo),
                        PickedUpFrom = transaction.EffectiveVendorInfoId.HasValue ? "registered" : "unregistered",
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems)
                    };
                    break;

                case TreadMarksConstants.RTR:
                    model = new RTRTransactionAdjViewModel()
                    {
                        TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems),
                        CompanyInfo = GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo),
                        CountryList = new List<string>()
                    };

                    var localRTR = model as RTRTransactionAdjViewModel;
                    localRTR.CountryList.AddRange(DataLoader.GetCountriesByIso3166());
                    break;

                case TreadMarksConstants.SPS:
                    model = new SPSTransactionAdjViewModel()
                    {
                        SoldTo = transaction.EffectiveCompanyInfo == null ? "registered" : "unregistered",
                        CompanyInfo = transaction.EffectiveCompanyInfo == null ? null : GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo),
                        InvoiceNumber = transaction.EffectiveInvoiceNumber,
                        ScaleTicket = (transaction.IsPaper) ? GetScaleTicketAdjustmentModel(transaction.EffectiveScaleTickets.FirstOrDefault(), true) : null,
                        ProductDetail = (ProcessorSPSProductViewModel)GetProductAdjustmentDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault()),
                        ProductList = TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue, transaction.OutgoingId.Value, period.StartDate, period.EndDate),
                        CountryList = DataLoader.GetCountriesByIso3166()
                    };
                    break;

                case TreadMarksConstants.SPSR:
                    model = new SPSRTransactionAdjViewModel()
                    {
                        InvoiceNumber = transaction.EffectiveInvoiceNumber,
                        ProductDetail = (RPMSPSRProductViewModel)GetProductAdjustmentDetail(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems.FirstOrDefault()),
                        CompanyInfo = GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo),
                        ProductList = TransactionHelper.GetTransactionProductViewModelList(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue, transaction.OutgoingId.Value, period.StartDate, period.EndDate),
                        CountryList = DataLoader.CountryNames //OTSTM2-1016
                    };
                    ((RPMSPSRProductViewModel)(((SPSRTransactionAdjViewModel)model).ProductDetail)).RetailPrice = transaction.RetailPrice ?? 0;
                    break;

                case TreadMarksConstants.DOR:
                    model = new DORTransactionAdjViewModel()
                    {
                        InvoiceNumber = transaction.EffectiveInvoiceNumber,
                        ScaleTicket = (transaction.IsPaper) ? GetScaleTicketAdjustmentModel(transaction.EffectiveScaleTickets.FirstOrDefault(), true) : null,
                        TireTypeList = TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.DOR)),

                        ProductDetail = new ProcessorDORProductViewModel()
                        {
                            MaterialTypeId = transaction.EffectiveMaterialType,
                            DispositionReasonId = transaction.EffectiveDispositionReason,
                            OffRoadScaleWeight = new ProcessorScaleWeightViewModel() { WeightType = TreadMarksConstants.Ton },
                            OnRoadScaleWeight = new ProcessorScaleWeightViewModel() { WeightType = TreadMarksConstants.Ton }
                        },

                        SoldTo = transaction.EffectiveCompanyInfo == null ? "registered" : "unregistered",
                        CompanyInfo = transaction.EffectiveCompanyInfo == null ? null : GetUnregisteredCompanyAdjustmentModel(transaction.EffectiveCompanyInfo),

                        CountryList = DataLoader.GetCountriesByIso3166(),
                        WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg, TreadMarksConstants.Ton },
                        RateDetail = new ProcessorRateDetailViewModel()
                    };

                    var localModelDOR = model as DORTransactionAdjViewModel;
                    if (localModelDOR.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue)
                    {
                        localModelDOR.TireTypeList = GetTireTypeCountAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionItems);
                    }

                    if (localModelDOR.ProductDetail.MaterialTypeId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
                    {
                        localModelDOR.ProductDetail.PTRNumber = transaction.EffectivePTRNumber;
                        localModelDOR.ProductDetail.OffRoadScaleWeight.Weight = (double)Math.Round(DataConversion.ConvertKgToTon((double)transaction.EffectiveOffRoadWeight.Value), 4, MidpointRounding.AwayFromZero);
                        localModelDOR.ProductDetail.OnRoadScaleWeight.Weight = (double)Math.Round(DataConversion.ConvertKgToTon((double)transaction.EffectiveOnRoadWeight.Value), 4, MidpointRounding.AwayFromZero);
                        if (!string.IsNullOrEmpty(localModelDOR.ProductDetail.PTRNumber)) localModelDOR.ProductDetail.PTRWeightDetail = GetPTRWeightDetail(long.Parse(localModelDOR.ProductDetail.PTRNumber));
                    }

                    var processorClaimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
                    var paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;
                    var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
                    var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;
                    var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(transaction.FromPostalCode, 2);
                    Rate OnRoadRate, OffRoadRate;

                    OnRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == processorClaimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OnRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= period.StartDate && r.EffectiveEndDate >= period.StartDate);
                    if (OnRoadRate != null)
                    {
                        var rateInTon = Math.Round(DataConversion.ConvertTonToKg((double)OnRoadRate.ItemRate), 3, MidpointRounding.AwayFromZero);
                        localModelDOR.RateDetail.OnRoadRate = rateInTon;
                        localModelDOR.ProductDetail.OnRoadRate = rateInTon;
                    }

                    OffRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == processorClaimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OffRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= period.StartDate && r.EffectiveEndDate >= period.StartDate);
                    if (OffRoadRate != null)
                    {
                        var rateInTon = Math.Round(DataConversion.ConvertTonToKg((double)OffRoadRate.ItemRate), 3, MidpointRounding.AwayFromZero);
                        localModelDOR.RateDetail.OffRoadRate = rateInTon;
                        localModelDOR.ProductDetail.OffRoadRate = rateInTon;
                    }
                    break;
            }

            if (model != null)
            {
                model.TransactionId = transaction.ID;
                model.TransactionGuid = transaction.TransactionId;
                model.TransactionNumber = transaction.FriendlyId;
                model.Preview = false;
                model.TransactionType = transaction.TransactionTypeCode;

                model.SupportingDocumentList = GetSupportingDocumentAdjustmentModel(transaction.TransactionTypeCode, transaction.EffectiveTransactionSupportingDocuments);
                model.AttachmentList = GetSupportingDocumentAttachmentList(transaction.EffectiveTransactionAttachments, true);
                model.UploadDocumentList = new List<TransactionFileUploadModel>();
            }

            return model;
        }

        private List<TransactionTireTypeViewModel> GetTireTypeCountAdjustmentModel(string transactionType, List<TransactionItem> transactionItems)
        {
            var tireTypeList = new List<TransactionTireTypeViewModel>();
            tireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(transactionType)));

            foreach (var tireTypeObj in tireTypeList)
            {
                var tireTypeId = TransactionHelper.GetTransactionTireTypeId(tireTypeObj.TireType);
                var transactionItem = transactionItems.FirstOrDefault(i => i.ItemID == tireTypeId);
                tireTypeObj.Count = (transactionItem != null && transactionItem.Quantity.HasValue) ? transactionItem.Quantity.Value : 0;
                tireTypeObj.OriginalCount = tireTypeObj.Count;
            }

            return tireTypeList;
        }

        private TransactionScaleTicketViewModel GetScaleTicketAdjustmentModel(ScaleTicket scaleTicket, bool inTon = false)
        {
            var model = new TransactionScaleTicketViewModel() { Weight = null, TicketNumber = null, WeightType = TreadMarksConstants.Kg, WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg } };

            if (scaleTicket != null && scaleTicket.ScaleTicketType == (int)ScaleTicketTypeEnum.Both)
            {
                model.TicketNumber = scaleTicket.TicketNumber;
                model.Weight = (double)(scaleTicket.InboundWeight.Value - scaleTicket.OutboundWeight.Value);

                if (inTon)
                {
                    model.WeightType = TreadMarksConstants.Ton;
                    model.Weight = (double)Math.Round(DataConversion.ConvertKgToTon(model.Weight.Value), 4, MidpointRounding.AwayFromZero);
                }
            }

            if (inTon) model.WeightTypeList = new List<int> { TreadMarksConstants.Ton, TreadMarksConstants.Lb, TreadMarksConstants.Kg };

            return model;
        }

        private TransactionUnregisteredCompanyViewModel GetUnregisteredCompanyAdjustmentModel(CompanyInfo companyInfo)
        {
            if (companyInfo == null) return new TransactionUnregisteredCompanyViewModel();

            var model = new TransactionUnregisteredCompanyViewModel()
            {
                CompanyName = companyInfo.CompanyName,
                PhoneNumber = companyInfo.PhoneNumber
            };

            var address = companyInfo.Address;
            if (address != null)
            {
                model.Address1 = address.Address1;
                model.Address2 = address.Address2;
                model.City = address.City;
                model.Province = address.Province;
                model.PostalCode = address.PostalCode;
                model.Country = address.Country;
            }

            return model;
        }

        private List<TransactionSupportingDocumentViewModel> GetSupportingDocumentAdjustmentModel(string transactionType, List<TransactionSupportingDocument> supportingDocuments)
        {
            var supportingDocumentList = new List<TransactionSupportingDocumentViewModel>();
            supportingDocumentList.AddRange(TransactionHelper.GetAvailableSupportingDocumentList(transactionType));

            supportingDocumentList.ForEach(sd =>
            {
                var existingSD = supportingDocuments.SingleOrDefault(i => i.DocumentName == sd.DocumentName);

                if (existingSD != null)
                {
                    sd.OriginalDocumentFormat = existingSD.DocumentFormat;
                    sd.DocumentFormat = existingSD.DocumentFormat;
                }
            });

            return supportingDocumentList;
        }

        private ITransactionProductBaseViewModel GetProductAdjustmentDetail(string type, TransactionItem transactionItem)
        {
            ITransactionProductBaseViewModel model = null;

            if (type == TreadMarksConstants.PIT) model = new ProcessorPITProductViewModel();
            if (type == TreadMarksConstants.SPS) model = new ProcessorSPSProductViewModel();
            if (type == TreadMarksConstants.SPSR) model = new RPMSPSRProductViewModel();

            if (transactionItem != null)
            {
                model.ProductTypeId = transactionItem.ItemID;
                model.ProductRate = transactionItem.Rate;

                if (type == TreadMarksConstants.SPS)
                {
                    var spsModel = model as ProcessorSPSProductViewModel;

                    spsModel.MeshRangeStart = transactionItem.MeshRangeStart;
                    spsModel.MeshRangeEnd = transactionItem.MeshRangeEnd;
                    spsModel.IsFreeOfSteel = transactionItem.IsFreeOfSteel;
                    spsModel.ProductDescription = transactionItem.ItemDescription;
                }

                if (type == TreadMarksConstants.SPSR)
                {
                    var spsrModel = model as RPMSPSRProductViewModel;

                    spsrModel.TotalTDPWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                    spsrModel.Percentage = transactionItem.RecyclablePercentage;
                    spsrModel.ProductCode = transactionItem.ItemCode;
                    spsrModel.ProductDescription = transactionItem.ItemDescription;
                }
            }

            return model;
        }

        private TransactionAdjStatusViewModel GetAdjustmentDetail(TransactionAdjustment adjustmentRecord)
        {
            if (adjustmentRecord == null) return null;

            if (adjustmentRecord.StartUser == null) adjustmentRecord.StartUser = userRepository.FindUserByUserId(adjustmentRecord.StartedBy);
            var adjustmentDetail = new TransactionAdjStatusViewModel()
            {
                StartedBy = adjustmentRecord.StartUser.FirstName + " " + adjustmentRecord.StartUser.LastName,
                Status = adjustmentRecord.Status,
                AdjustedOn = adjustmentRecord.AdjustmentDate
            };

            return adjustmentDetail;
        }

        private void DistributeAdjustmentScaleWeight(TransactionAdjustment adjustment, Decimal scaleWeight)
        {
            var totalAverageWeight = adjustment.TransactionItems.Sum(c => c.AverageWeight);

            // Distribute the weight
            adjustment.TransactionItems.ForEach(c =>
            {
                c.ActualWeight = Math.Round((scaleWeight * (c.AverageWeight / totalAverageWeight)), 4, MidpointRounding.AwayFromZero);
            });

            // After distribution find the total actual weight and if it is not equal to scale weight, then apply the difference to the highest weight transaction item
            var totalActualWeight = adjustment.TransactionItems.Sum(c => c.ActualWeight);
            if (totalActualWeight != scaleWeight)
            {
                var difference = (scaleWeight - totalActualWeight);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.0001M);
                if (numberOfDifferenceDistribution > adjustment.TransactionItems.Count) throw new Exception("Distribution of weight could not be processed");

                var transactionItemList = adjustment.TransactionItems.OrderByDescending(i => i.ActualWeight).ToList();
                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                {
                    transactionItemList[i].ActualWeight += (difference / numberOfDifferenceDistribution);
                }
            }

            adjustment.OnRoadWeight = GetOnRoadWeight(adjustment.TransactionItems);
            adjustment.OffRoadWeight = GetOffRoadWeight(adjustment.TransactionItems);
        }

        private bool IsAdjustmentReviewAllowed(bool isStaff, int currentVendorId, int initiatorVendorId, bool pendingAdjustmentExists, TransactionClaimAssociationDetailViewModel claimAssociation)
        {
            var allow = false;

            if (pendingAdjustmentExists && currentVendorId != initiatorVendorId)
            {
                allow = IsAllClaimAssociationInOpenStatus(claimAssociation, isStaff);
            }

            return allow;
        }

        private bool IsAdjustmentRecallAllowed(bool isStaff, int currentVendorId, int initiatorVendorId, bool pendingAdjustmentExists, TransactionClaimAssociationDetailViewModel claimAssociation)
        {
            var allow = false;

            if (pendingAdjustmentExists && currentVendorId == initiatorVendorId)
            {
                allow = IsAllClaimAssociationInOpenStatus(claimAssociation, isStaff);
            }

            return allow;
        }
        #endregion


        #region Private helper methods
        private List<TransactionItem> PrepareTransactionItemList(List<TransactionTireTypeViewModel> tireTypeList, DateTime effectiveOn)
        {
            var list = new List<TransactionItem>();

            foreach (var tireTypeObj in tireTypeList)
            {
                if (tireTypeObj.Count.HasValue && tireTypeObj.Count.Value >= 0)
                {
                    var item = DataLoader.TransactionItems.Single(c => c.ShortName == tireTypeObj.TireType);

                    var transactionItem = new TransactionItem()
                    {
                        ItemID = item.ID,
                        UnitType = item.UnitType,
                        Quantity = tireTypeObj.Count.Value
                    };

                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(c => c.ItemID == item.ID && c.EffectiveStartDate <= effectiveOn && c.EffectiveEndDate >= effectiveOn);
                    if (itemWeight != null)
                    {
                        transactionItem.AverageWeight = Math.Round((tireTypeObj.Count.Value * itemWeight.StandardWeight), 4, MidpointRounding.AwayFromZero);
                        transactionItem.ActualWeight = transactionItem.AverageWeight;
                    }

                    list.Add(transactionItem);
                }
            }

            return list;
        }

        private TransactionItem PrepareProductTransactionItem(ITransactionProductBaseViewModel productModel, decimal? weight, string type, int unitType = 3)
        {
            var transactionItem = new TransactionItem()
            {
                ItemID = productModel.ProductTypeId.Value,
                UnitType = unitType,
                Quantity = 1,
                Rate = productModel.ProductRate,
                ActualWeight = weight.HasValue ? weight.Value : 0.0000M
            };
            transactionItem.AverageWeight = transactionItem.ActualWeight;

            if (type == TreadMarksConstants.PIT)
            {
                // Nothing specific
            }

            if (type == TreadMarksConstants.SPS)
            {
                var transactionProduct = DataLoader.ProcessorSPSTransactionProducts.Single(i => i.ID == productModel.ProductTypeId.Value);

                var productModelSPS = productModel as ProcessorSPSProductViewModel;

                if (transactionProduct.ShortName == "TDP1" || transactionProduct.ShortName == "TDP2" || transactionProduct.ShortName == "TDP3")
                {
                    transactionItem.MeshRangeStart = productModelSPS.MeshRangeStart;
                    transactionItem.MeshRangeEnd = productModelSPS.MeshRangeEnd;
                }

                if (transactionProduct.ShortName == "TDP1" || transactionProduct.ShortName == "TDP2") transactionItem.IsFreeOfSteel = productModelSPS.IsFreeOfSteel;

                if (transactionProduct.ShortName == "TDP4") transactionItem.ItemDescription = productModelSPS.ProductDescription;
            }

            if (type == TreadMarksConstants.SPSR)
            {
                var productModelSPSR = productModel as RPMSPSRProductViewModel;

                transactionItem.ItemCode = productModelSPSR.ProductCode;
                transactionItem.ItemDescription = productModelSPSR.ProductDescription;
                transactionItem.RecyclablePercentage = productModelSPSR.Percentage;
            }

            return transactionItem;
        }

        private decimal GetOnRoadWeight(List<TransactionItem> transactionItemList)
        {
            decimal onRoadWeight = 0.0000M;

            transactionItemList.ForEach(c =>
            {
                var item = DataLoader.TransactionItems.Single(i => i.ID == c.ItemID);
                if (item.ItemType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue) onRoadWeight += c.ActualWeight;
            });

            return Math.Round(onRoadWeight, 4, MidpointRounding.AwayFromZero);
        }

        private decimal GetOffRoadWeight(List<TransactionItem> transactionItemList)
        {
            decimal offRoadWeight = 0.0000M;

            transactionItemList.ForEach(c =>
            {
                var item = DataLoader.TransactionItems.Single(i => i.ID == c.ItemID);
                if (item.ItemType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue) offRoadWeight += c.ActualWeight;
            });

            return Math.Round(offRoadWeight, 4, MidpointRounding.AwayFromZero);
        }

        private Decimal GetNetScaleTicketWeight(IEnumerable<ScaleTicket> scaleTickets)
        {
            ScaleTicket scaleTicket;
            Decimal netWeight = 0.00M;

            var singleTicketType = scaleTickets.Any(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both);

            if (singleTicketType)
            {
                scaleTicket = scaleTickets.First(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Both); // There should be only one Scale Ticket of this type can exist

                if (scaleTicket.InboundWeight.HasValue) netWeight += scaleTicket.InboundWeight.Value;
                if (scaleTicket.OutboundWeight.HasValue) netWeight -= scaleTicket.OutboundWeight.Value;
            }
            else
            {
                scaleTicket = scaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Inbound); // There should be only one Scale Ticket of this type can exist
                if (scaleTicket != null && scaleTicket.InboundWeight.HasValue) netWeight += scaleTicket.InboundWeight.Value;

                scaleTicket = scaleTickets.FirstOrDefault(i => i.ScaleTicketType == (int)ScaleTicketTypeEnum.Outbound); // There should be only one Scale Ticket of this type can exist
                if (scaleTicket != null && scaleTicket.OutboundWeight.HasValue) netWeight -= scaleTicket.OutboundWeight.Value;
            }

            return netWeight;
        }

        private Decimal GetVariance(decimal estimatedWeight, decimal netScaleTicketWeight, bool ticketsExists)
        {
            if (estimatedWeight == 0 || !ticketsExists) return 0.00M;

            return Math.Round((((netScaleTicketWeight - estimatedWeight) / estimatedWeight) * 100), 2, MidpointRounding.AwayFromZero);
        }

        private TransactionViewScaleTicketViewModel GetScaleTicketViewModel(ScaleTicket scaleTicket, bool inTon)
        {
            TransactionViewScaleTicketViewModel scaleTicketModel = new TransactionViewScaleTicketViewModel()
            {
                TicketType = scaleTicket.ScaleTicketType,
                TicketNumber = scaleTicket.TicketNumber,
                CreatedOn = scaleTicket.CreateDate,
                InboundWeight = scaleTicket.InboundWeight,
                OutboundWeight = scaleTicket.OutboundWeight
            };
            if (inTon)
            {
                if (scaleTicketModel.InboundWeight.HasValue) scaleTicketModel.InboundWeight = Math.Round(DataConversion.ConvertKgToTon((double)scaleTicketModel.InboundWeight), 4, MidpointRounding.AwayFromZero);
                if (scaleTicketModel.OutboundWeight.HasValue) scaleTicketModel.OutboundWeight = Math.Round(DataConversion.ConvertKgToTon((double)scaleTicketModel.OutboundWeight), 4, MidpointRounding.AwayFromZero);
            }

            return scaleTicketModel;
        }

        private bool IsAllClaimAssociationInOpenStatus(TransactionClaimAssociationDetailViewModel claimAssociation, bool isStaff)
        {
            if (claimAssociation != null && claimAssociation.Associations != null)
            {
                if (isStaff)
                    return claimAssociation.Associations.All(i => i.Status == "Open" || i.Status == "Submitted" || i.Status == "Under Review");
                else
                    return claimAssociation.Associations.All(i => i.Status == "Open");
            }

            return false;
        }

        private string GetSupportingDocumentsSubmittedBy(List<TransactionSupportingDocument> supportingDocuments)
        {
            var uploadByList = new List<string>();
            foreach (var sd in supportingDocuments)
            {
                if (sd.DocumentFormat == 1 && !uploadByList.Where(i => i == "Fax").Any()) uploadByList.Add("Fax");
                if (sd.DocumentFormat == 2 && !uploadByList.Where(i => i == "Mail").Any()) uploadByList.Add("Mail");
                if (sd.DocumentFormat == 3 && !uploadByList.Where(i => i == "Upload").Any()) uploadByList.Add("Upload");
            }

            return string.Join("|", uploadByList.ToArray());
        }

        private string GetPaymentEligibleStr(int? paymentEligible, string paymentEligibleOther)
        {
            if (!paymentEligible.HasValue) return "";

            if (paymentEligible == 1) return "Non-registered Collector";
            if (paymentEligible == 2) return "Charged a disposal fee";
            if (paymentEligible == 3) return "Pre-program tires";
            if (paymentEligible == 4) return string.IsNullOrEmpty(paymentEligibleOther) ? "" : paymentEligibleOther;

            return "";
        }

        private List<TransactionViewSupportingDocumentAttachmentViewModel> GetSupportingDocumentAttachmentList(List<TransactionAttachment> supportingDocumentAttachments, bool asAjustmentReady)
        {
            string fileUploadPreviewDomainName = settingRepository.GetSettingValue("Settings.FileUploadPreviewDomainName");
            var list = new List<TransactionViewSupportingDocumentAttachmentViewModel>();

            foreach (var supportingDocumentAttachment in supportingDocumentAttachments)
            {
                var attachment = supportingDocumentAttachment.Attachemnt;
                var fileType = attachment.FileType.ToLower();
                var src = "/img/image-unavailable.png";
                if (fileType == "image/jpeg" || fileType == "image/png" || fileType == "image/gif" || fileType == "image/png") src = fileUploadPreviewDomainName + attachment.FilePath;

                var supportingDocumentAttachmentModel = new TransactionViewSupportingDocumentAttachmentViewModel() { ActualSrc = fileUploadPreviewDomainName + attachment.FilePath, Src = src, FileName = attachment.FileName, IsRemoved = false };
                if (asAjustmentReady)
                {
                    supportingDocumentAttachmentModel.AttachmentId = attachment.ID;
                    supportingDocumentAttachmentModel.Description = supportingDocumentAttachment.Description;
                    supportingDocumentAttachmentModel.OriginalDescription = supportingDocumentAttachment.Description;
                }
                else supportingDocumentAttachmentModel.Description = string.IsNullOrEmpty(supportingDocumentAttachment.Description) ? "---" : supportingDocumentAttachment.Description;

                list.Add(supportingDocumentAttachmentModel);
            }

            return list;
        }

        private TransactionPeriodViewModel GetTransactionPeriod(Transaction transaction, TransactionClaimAssociationDetailViewModel claimAssociation, int currentVendorId)
        {
            if (currentVendorId == 0)
            {
                if (transaction.MaterialType != null && transaction.MaterialType == 4)
                {
                    return GetDORTransactionClaimAssociationPeriod(transaction.ID); //OTSTM2-1313 Fix the issue (In All transaction page, cannot open pop up modal for DOR transaction with material type 4). Period is needed for the further method to populate rates
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var currentVendorClaimAssociation = claimAssociation.Associations.SingleOrDefault(i => i.ParticipantID == currentVendorId);
                if (currentVendorClaimAssociation != null)
                {
                    return currentVendorClaimAssociation.Period;
                }
                else
                {
                    return GetPossibleTransactionClaimAssociationPeriod(transaction, currentVendorId);
                }
            }
            //if (currentVendorId == 0) return null;

            //var currentVendorClaimAssociation = claimAssociation.Associations.SingleOrDefault(i => i.ParticipantID == currentVendorId);
            //if (currentVendorClaimAssociation != null) return currentVendorClaimAssociation.Period;

            //return GetPossibleTransactionClaimAssociationPeriod(transaction, currentVendorId);
        }

        private TransactionPeriodViewModel GetPossibleTransactionClaimAssociationPeriod(Transaction transaction, int currentVendorId)
        {
            TransactionPeriodViewModel period = null;

            var claimBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);

            var claim = claimBO.GetClaimForTransaction(currentVendorId, transaction.TransactionDate, transaction.IsAddedByStaff);
            if (claim == null) claim = claimBO.GetNextAvailableOpenClaimForTransaction(currentVendorId, transaction.TransactionDate, transaction.IsAddedByStaff);

            if (claim != null) period = new TransactionPeriodViewModel() { StartDate = claim.ClaimPeriod.StartDate, EndDate = claim.ClaimPeriod.EndDate };

            return period;
        }

        //OTSTM2-1313
        private TransactionPeriodViewModel GetDORTransactionClaimAssociationPeriod(int transactionId)
        {
            TransactionPeriodViewModel period = null;

            var claimBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);

            var claim = claimBO.GetClaimForDORTransaction(transactionId);

            if (claim != null) period = new TransactionPeriodViewModel() { StartDate = claim.ClaimPeriod.StartDate, EndDate = claim.ClaimPeriod.EndDate };

            return period;
        }
        #endregion


        public Transaction GetTransactionByFriendlyID(long friendlyId)
        {
            return transactionRepository.GetTransactionByFriendlyID(friendlyId);
        }

        public List<TransactionDisabledDateRangeViewModel> GetVendorInactiveDateRanges(int vendorId)
        {
            var inactiveDateRangeList = this.vendorRepository.GetVendorInactiveDateRanges(vendorId);

            var list = new List<TransactionDisabledDateRangeViewModel>();
            foreach (var inactiveDateRange in inactiveDateRangeList)
            {
                list.Add(new TransactionDisabledDateRangeViewModel() { StartDate = inactiveDateRange.StartDate.Date, EndDate = inactiveDateRange.EndDate });
            }

            return list;
        }

        public bool IsVendorActive(int vendorId, DateTime on)
        {
            return this.vendorRepository.IsVendorActive(vendorId, on);
        }

        public IEnumerable<VendorStatusDateRangeModel> GetVendorStatusDateRanges(int vendorId)
        {
            return vendorRepository.GetVendorStatusDateRanges(vendorId);
        }

        public InternalNoteViewModel AddTransactionNote(int transactionId, string note, TransactionNoteType noteType = TransactionNoteType.General)
        {
            var internalNote = new TransactionNote()
            {
                TransactionId = transactionId,
                Note = note,
                CreatedDate = DateTime.UtcNow,
                UserId = SecurityContextHelper.CurrentUser.Id,
                NoteType = noteType.ToString()
            };

            internalNote = transactionRepository.AddTransactionInternalNote(internalNote);

            //OTSTM2-765
            var model = new InternalNoteViewModel();
            model.Note = internalNote.Note;
            model.AddedOn = internalNote.CreatedDate;
            if (internalNote.Note.Contains("This transaction is invalidated by"))
            {
                model.AddedBy = AppSettings.Instance.SystemUser.FirstName + " " + AppSettings.Instance.SystemUser.LastName;
            }
            else
            {
                model.AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName;
            }
            return model;
        }

        //OTSTM2-765
        public List<InternalNoteViewModel> GetAllTransactionNote(int transactionId)
        {
            var internalNoteList = new List<InternalNoteViewModel>();
            var noteListTmp = transactionRepository.GetAllTransactionInternalNote(transactionId);
            noteListTmp.ForEach(c =>
            {
                var internalNote = new InternalNoteViewModel();
                internalNote.Note = c.Note;
                internalNote.AddedOn = c.CreatedDate;
                if (c.Note.Contains("This transaction is invalidated by"))
                {
                    internalNote.AddedBy = AppSettings.Instance.SystemUser.FirstName + " " + AppSettings.Instance.SystemUser.LastName;
                }
                else
                {
                    internalNote.AddedBy = c.User.FirstName + " " + c.User.LastName;
                }
                internalNoteList.Add(internalNote);
            });
            return internalNoteList;
        }

        public ProcessorPTRWeightDetailViewModel GetPTRWeightDetail(long number)
        {
            var transaction = transactionRepository.GetTransactionByFriendlyID(number);
            if (transaction == null) return null;

            var ptrWeightDetail = new ProcessorPTRWeightDetailViewModel()
            {
                OnRoadWeight = transaction.OnRoadWeight,
                OffRoadWeight = transaction.OffRoadWeight
            };

            return ptrWeightDetail;
        }

        public bool UpdateTransactionStatus(int transactionId, string oldStatus, string newStatus)
        {
            return transactionRepository.UpdateTransactionStatus(transactionId, oldStatus, newStatus);
        }

        public string CheckTransactionVendorStatus(int transactionId, bool addLog)
        {
            Transaction transaction = transactionRepository.GetTransactionById(transactionId);
            string warningVendorInactive = "[{0:D}, {1:HH:mm:ss}], {2} was inactive on transaction date.";
            string warningMsg = string.Empty;
            int vId;
            string returnVal = string.Empty;
            if (null != transaction)
            {
                vId = transaction.IncomingId ?? default(int);
                if (vId > 0 && vendorRepository.IsNotActive(vId, transaction.TransactionDate))
                {
                    VendorReference vendor = vendorRepository.GetVendorById(vId);
                    if (null != vendor)
                    {
                        ClaimType vendorName;
                        if (Enum.TryParse<ClaimType>(vendor.VendorType.ToString(), out vendorName))
                        {
                            warningMsg = string.Format(warningVendorInactive, DateTime.Now, DateTime.Now, vendorName + " (" + vendor.RegistrationNumber + ")");
                            if (addLog && transactionRepository.UdpateTransactioProcessingStatus(transactionId, TransactionProcessingStatus.Invalidated))
                            {
                                transactionRepository.AddTransactionInternalNote(new TransactionNote()
                                {
                                    TransactionId = transactionId,
                                    Note = warningMsg,
                                    CreatedDate = DateTime.UtcNow,
                                    UserId = null,
                                    NoteType = TransactionNoteType.Warning.ToString(),
                                });
                            }
                        }
                    }
                    returnVal += warningMsg;
                }
                vId = transaction.OutgoingId ?? default(int);
                if (vId > 0 && vendorRepository.IsNotActive(vId, transaction.TransactionDate))
                {
                    VendorReference vendor = vendorRepository.GetVendorById(vId);
                    if (null != vendor)
                    {
                        ClaimType vendorName;
                        if (Enum.TryParse<ClaimType>(vendor.VendorType.ToString(), out vendorName))
                        {
                            warningMsg = string.Format(warningVendorInactive, DateTime.Now, DateTime.Now, vendorName + " (" + vendor.RegistrationNumber + ")");
                            if (addLog && transactionRepository.UdpateTransactioProcessingStatus(transactionId, TransactionProcessingStatus.Invalidated))
                            {
                                transactionRepository.AddTransactionInternalNote(new TransactionNote()
                                {
                                    TransactionId = transactionId,
                                    Note = warningMsg,
                                    CreatedDate = DateTime.UtcNow,
                                    UserId = null,
                                    NoteType = TransactionNoteType.Warning.ToString(),
                                });
                            }
                        }
                    }
                    returnVal += warningMsg;
                }
            }
            return returnVal;
        }
        public bool UpdateSPSRTransactionStatus(int transactionId, string oldStatus, string newStatus)
        {
            if (transactionRepository.UpdateSPSRTransactionStatus(transactionId, oldStatus, newStatus))
            {
                RemoveTransactionFromClaim(transactionId);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateTransactionProcessingStatus(int transactionId, TransactionProcessingStatus status)
        {
            var updated = transactionRepository.UdpateTransactioProcessingStatus(transactionId, status);

            if ((status == TransactionProcessingStatus.Invalidated) || (status == TransactionProcessingStatus.Approved) || (status == TransactionProcessingStatus.Unreviewed))  //OTSTM2-465 add condition for Unreviewed
                RecalculateClaims(transactionId);

            //OTSTM2-765
            if (status == TransactionProcessingStatus.Invalidated)
            {
                string warningStringTransInvalidate = "[{0:D}, {1:HH:mm:ss}] This transaction is invalidated by {2} (Staff User).";
                string warningMsg = string.Format(warningStringTransInvalidate, DateTime.Now, DateTime.Now, SecurityContextHelper.CurrentUser.FullName);

                AddTransactionNote(transactionId, warningMsg, TransactionNoteType.Warning);
            }

            return updated;
        }
        public bool UpdateTransactionProcessingStatusNoRecalculateClaims(int transactionId, TransactionProcessingStatus status)
        {
            return transactionRepository.UdpateTransactioProcessingStatus(transactionId, status);
        }
        public bool AddTransactionRetailPrice(int transactionId, decimal retailPrice)
        {
            var updated = transactionRepository.AddTransactionRetailPrice(transactionId, retailPrice);

            return updated;
        }

        private void RecalculateClaims(int transactionId)
        {
            var claimIds = claimsRepository.GetClaimsByTransactionId(transactionId);
            claimIds.ForEach(c =>
            {
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = c };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);

                //Triger claim inventory report calculation
                var claimReportPayload = new ClaimReportPayload { ClaimId = c, TransactionId = transactionId };
                eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

            });
        }

        //OTSTM2-1142
        private void RecalculateClaimsForTransactionAdjustment(int transactionId)
        {
            var claimIds = claimsRepository.GetClaimsByTransactionId(transactionId);
            claimIds.ForEach(c =>
            {
                ClaimCalculationHelper.CalculateClaim(c);

                //Triger claim inventory report calculation
                var claimReportPayload = new ClaimReportPayload { ClaimId = c, TransactionId = transactionId };
                eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

            });
        }

        public void RemoveTransactionFromClaim(int transactionId)
        {
            var claimIds = claimsRepository.RemoveClaimDetail(transactionId);
            claimIds.ForEach(c =>
            {
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = c };
                eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);

                //Triger claim inventory report calculation
                var claimReportPayload = new ClaimReportPayload { ClaimId = c, TransactionId = transactionId };
                eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

            });
        }
        #region Add Transaction to claims
        public void AddTransactionToClaim(int transactionId)
        {
            var transaction = transactionRepository.GetTransactionById(transactionId);
            AddTransactionToClaim(transaction);
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddTransactionToClaim(Transaction transaction)
        {
            var claimBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);

            //Add inbound claim
            if (transaction.IncomingId != null)
            {
                //OTSTM2-337, OTSTM2-482 Insert transaction respective claim
                //transaction.IsAddedByStaff || transaction.MobileFormat);
                var claimIn = claimBO.GetClaimForTransaction((int)transaction.IncomingId, transaction.TransactionDate, true);

                if (claimIn == null)
                {
                    var currentClaim = claimBO.GetCurrentClaim((int)transaction.IncomingId, transaction.TransactionDate);
                    //Check the current claim is existing
                    if (currentClaim != null)
                    {
                        claimIn = claimBO.GetNextAvailableOpenClaimForTransaction((int)transaction.IncomingId, transaction.TransactionDate, true);
                    }
                }

                if (claimIn != null)
                {
                    if (Validate(transaction, claimIn, true))
                    {
                        var inClaimDetail = new ClaimDetail
                        {
                            ClaimId = claimIn.ID,
                            TransactionId = transaction.ID,
                            Direction = true
                        };
                        claimBO.AddClaimDetails(inClaimDetail);

                        //Calculating claim in synchronous mode
                        ClaimCalculationHelper.CalculateClaim(claimIn.ID);

                        //Calculating claim in asynchronous mode
                        //var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimIn.ID };
                        //eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);

                        //Triger claim inventory report calculation
                        var claimReportPayload = new ClaimReportPayload { ClaimId = claimIn.ID, TransactionId = transaction.ID };
                        eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

                    }
                }
            }

            //Add outbound claim
            if (transaction.OutgoingId != null)
            {
                var claimOut = claimBO.GetClaimForTransaction((int)transaction.OutgoingId, transaction.TransactionDate, true);

                if (claimOut == null)
                {
                    var currentClaim = claimBO.GetCurrentClaim((int)transaction.OutgoingId, transaction.TransactionDate);
                    if (currentClaim != null)
                    {
                        claimOut = claimBO.GetNextAvailableOpenClaimForTransaction((int)transaction.OutgoingId, transaction.TransactionDate, true);
                    }
                }

                if (claimOut != null)
                {
                    if (Validate(transaction, claimOut, false))
                    {
                        var outClaimDetail = new ClaimDetail
                        {
                            ClaimId = claimOut.ID,
                            TransactionId = transaction.ID,
                            Direction = false
                        };
                        claimBO.AddClaimDetails(outClaimDetail);

                        //Calculating claim in synchronous mode
                        ClaimCalculationHelper.CalculateClaim(claimOut.ID);

                        //Calculating claim in asynchronous mode
                        //var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimOut.ID };
                        //eventAggregator.GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);

                        //Triger claim inventory report calculation
                        var claimReportPayload = new ClaimReportPayload { ClaimId = claimOut.ID, TransactionId = transaction.ID };
                        eventAggregator.GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);

                    }
                }
            }
        }

        private static void RunClaimRules(Transaction transaction, ClaimsBO claimBO, Claim claim)
        {
            List<string> warningMessages = new List<string>();

            //Kamal already wrote a method while adding trans to claim
            //if (claim.ClaimPeriod.StartDate > transaction.TransactionDate || claim.ClaimPeriod.EndDate < transaction.TransactionDate)
            //  warningMessages.Add(string.Format("{0:D}, {1:HH:mm:ss}] This
            //  transaction does not belong to this claim period.",
            //                DateTime.Now, DateTime.Now));

            foreach (string warning in warningMessages)
                claimBO.AddClaimNote(claim.ID, warning);
        }

        private bool Validate(Transaction transaction, Claim claim, bool isIncomingClaim)
        {
            //Use it for future when we need a hard stop
            List<string> violationMessages = new List<string>();

            List<string> warningMessages = new List<string>();


            if (claim.ClaimPeriod.StartDate.Date > transaction.TransactionDate.Date || claim.ClaimPeriod.EndDate.Date < transaction.TransactionDate.Date)
            {
                string warningStringTransOutofClaimPeriod = "[{0:D}, {1:HH:mm:ss}] This transaction does not belong to {2} claim period for participant {3}.";

                string warningMsg = string.Format(warningStringTransOutofClaimPeriod, DateTime.Now, DateTime.Now, claim.ClaimPeriod.StartDate.ToString("MMMM"), isIncomingClaim ? vendorRepository.GetVendorNumber(transaction.IncomingId.Value) : vendorRepository.GetVendorNumber(transaction.OutgoingId.Value));
                if (transaction.MobileFormat)
                    AddMobileTransactionNote(transaction, warningMsg);
                else
                    //not sure why there is a special warning for paper only
                    AddTransactionNote(transaction.ID, warningMsg, TransactionNoteType.Warning);

            }

            if (claim.Status == EnumHelper.GetEnumDescription(ClaimStatus.Submitted) || claim.Status == EnumHelper.GetEnumDescription(ClaimStatus.UnderReview))
            {
                string warningStringTransSyncedLate = "[{0:D}, {1:HH:mm:ss}] This transaction is synced/added after the claim was submitted by participant {2}.";
                string warningMsg = string.Format(warningStringTransSyncedLate, DateTime.Now, DateTime.Now, isIncomingClaim ? vendorRepository.GetVendorNumber(transaction.IncomingId.Value) : vendorRepository.GetVendorNumber(transaction.OutgoingId.Value));

                if (transaction.MobileFormat)
                    AddMobileTransactionNote(transaction, warningMsg);
                else
                    AddTransactionNote(transaction.ID, warningMsg, TransactionNoteType.Warning);

            }

            return violationMessages.Count == 0;
        }

        private void AddMobileTransactionNote(Transaction transaction, string warning)
        {
            var internalNote = new TransactionNote()
            {
                TransactionId = transaction.ID,
                Note = warning,
                CreatedDate = DateTime.UtcNow,
                UserId = null,
                NoteType = TransactionNoteType.Warning.ToString()
            };
            transactionRepository.AddTransactionInternalNote(internalNote);
        }

        #endregion


        #region Import Transaction from central DB

        /// <summary>
        /// Import mobile transaction
        /// </summary>
        /// <param name="transactionId"></param>
        public void ImportMobileTransaction(Guid transactionId)
        {
            var mobileTransaction = mobileTransactionRepository.GetTransaction(transactionId);

            List<string> warnings = new List<string>();

            //Setting the basic properties to be saved for incomplete
            var transaction = SetTransactionForSave(mobileTransaction, ref warnings);

            if (mobileTransaction.TransactionStatusTypeId == (long)TransactionStatusEnum.COMPLETE)
            {
                //Validate completed transaction
                List<string> violations = new List<string>();
                if (!Validate(mobileTransaction, violations))
                    throw new Exception(string.Join("\n", violations.ToArray()));

                //Add notes only for completed transactions
                AddNotes(warnings, transaction);

                ImportCompletedTransaction(mobileTransaction, transaction);
            }
            //Need to check for Incomplete as well, dont sync error and void
            else //if (mobileTransaction.TransactionStatusTypeId == (long)TransactionStatusEnum.INCOMPLETE) sync all now
            {
                //Tried to do a short cut but this code did not work for various reasons. Mapper gave entity framwork errors and For PropertyCopier it did'nt work for Readonly/calculated values
                //transactionExisting = AutoMapper.Mapper.Map<Transaction, Transaction>(transaction, transactionExisting);
                //PropertyCopy.Copy(transaction, transactionExisting);

                //Resolved the above - UdpateTransaction modified to copy only modifiend values

                //Update existing Add new
                transactionRepository.UdpateIncompleteTransaction(transaction);
            }
        }

        private void ImportCompletedTransaction(MobileTransaction mobileTransaction, Transaction transaction)
        {
            transaction = PrepareCompletedForInsert(mobileTransaction, transaction);

            CheckDuplicateScaleTickets(transaction);

            //Update completed transaction
            transactionRepository.UpdateCompletedTransaction(transaction);

            //Open claim if it is not existing
            OpenClaim(transaction);

            if (transaction.TransactionTypeCode == TreadMarksConstants.STC)
                RunEventNumberRule(transaction);

            //Fixed PIT transaction sync issues
            AddTransactionToClaim(transaction);

        }

        private void OpenClaim(Transaction transaction)
        {
            var claimBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);
            if (transaction.IncomingId != null)
            {
                var currentClaim = claimBO.GetCurrentClaim((int)transaction.IncomingId, transaction.TransactionDate);
                if (currentClaim == null)
                {
                    //ET-37 Stop creating claims
                    //claimsRepository.InitializeClaimForVendor((int)transaction.IncomingId);
                }
            }
            if (transaction.OutgoingId != null)
            {
                var currentClaim = claimBO.GetCurrentClaim((int)transaction.OutgoingId, transaction.TransactionDate);
                if (currentClaim == null)
                {
                    //ET-37 Stop creating claims
                    //claimsRepository.InitializeClaimForVendor((int)transaction.OutgoingId);
                }
            }
        }

        private static void AddNotes(List<string> warnings, Transaction transaction)
        {
            var notes = new List<TransactionNote>();
            foreach (string warning in warnings)
            {
                //Adding warning notes for incoming mobile transaction
                notes.Add(new TransactionNote { Note = warning, CreatedDate = DateTime.UtcNow, UserId = null, NoteType = TransactionNoteType.Warning.ToString() });
            }
            transaction.TransactionNotes.AddRange(notes);
        }

        private bool Validate(MobileTransaction mobileTransaction, List<string> violationMessages)
        {
            if (vendorRepository.GetVendorByNumber(mobileTransaction.IncomingRegistrationNumber.ToString()) == null)
                violationMessages.Add("There is no Vendor for incoming registration number.");

            if (!mobileTransaction.TransactionType.ShortNameKey.Equals(TreadMarksConstants.STC) && !mobileTransaction.TransactionType.ShortNameKey.Equals(TreadMarksConstants.UCR))
                if (mobileTransaction.OutgoingRegistrationNumber != 0 && vendorRepository.GetVendorByNumber(mobileTransaction.OutgoingRegistrationNumber.ToString()) == null)
                    violationMessages.Add("There is no Vendor for outgoing registration number.");
            if (transactionRepository.GetTransactionById(mobileTransaction.ID) != null && claimsRepository.TransactionExistsInClaim(mobileTransaction.ID))
                violationMessages.Add("Transaction already exists.");

            return violationMessages.Count == 0;
        }

        private Transaction PrepareCompletedForInsert(MobileTransaction mobileTransaction, Transaction transaction)
        {
            transaction.Status = TransactionStatus.Completed.ToString();

            AddTiresPhotosComments(mobileTransaction, transaction);

            AddGpsLogs(mobileTransaction, transaction);

            decimal scaleWeight;

            switch (mobileTransaction.TransactionType.ShortNameKey)
            {
                //Business logic is same for TCR and DOT
                case TreadMarksConstants.TCR:
                case TreadMarksConstants.DOT:
                    AddEligibility(mobileTransaction, transaction);
                    transaction.FromPostalCode = mobileTransaction.PostalCode1 + mobileTransaction.PostalCode2;
                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(mobileTransaction.PostalCode1);

                    if (transaction.OutgoingGpsLog != null && transaction.OutgoingVendorRef != null)
                        CheckLocationVariance(transaction);

                    //Check Scale tickets for DOT
                    if (mobileTransaction.TransactionType.ShortNameKey == TreadMarksConstants.DOT)
                        AddScaleTickets(mobileTransaction, transaction, out scaleWeight);
                    break;

                case TreadMarksConstants.STC:
                    var mobileAuthorization = mobileTransaction.Authorizations.FirstOrDefault();
                    AddCompanyInfo(transaction, mobileAuthorization);
                    transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(transaction.CompanyInfo.Address.PostalCode);
                    transaction.EventNumber = mobileAuthorization != null ? mobileAuthorization.AuthorizationNumber : string.Empty;
                    transaction.FromPostalCode = string.IsNullOrEmpty(mobileTransaction.PostalCode1) ? transaction.CompanyInfo.Address.PostalCode : mobileTransaction.PostalCode1 + mobileTransaction.PostalCode2;
                    break;

                case TreadMarksConstants.UCR:
                    // Unregistered Collector                    
                    if (mobileTransaction.OutgoingRegistrationNumber == 9999999)
                    {
                        var mobileCompany = mobileTransactionRepository.GetCompany(mobileTransaction.ID);
                        if (mobileCompany != null)
                        {
                            transaction.CompanyInfo = new CompanyInfo()
                            {
                                CompanyName = mobileCompany.Name,
                                PhoneNumber = mobileCompany.Phone,
                                Address = new Address
                                {
                                    AddressType = 1,
                                    Address1 = mobileCompany.Address1 ?? "",
                                    Address2 = mobileCompany.Address2,
                                    City = mobileCompany.City ?? "",
                                    Province = mobileCompany.Province ?? "",
                                    PostalCode = mobileCompany.PostalCode ?? "",
                                    Country = mobileCompany.Country ?? ""
                                }
                            };
                            transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(mobileCompany.PostalCode);
                            transaction.FromPostalCode = string.IsNullOrEmpty(mobileTransaction.PostalCode1) ? transaction.CompanyInfo.Address.PostalCode : mobileTransaction.PostalCode1 + mobileTransaction.PostalCode2;
                        }
                    }
                    else
                    {
                        //Kamal's front end needs this change
                        transaction.IsSingle = true;
                        transaction.VendorInfoId = transaction.OutgoingId;
                        transaction.OutgoingId = null;
                        transaction.OutgoingAddressId = null;
                    }
                    AddEligibility(mobileTransaction, transaction);
                    break;
                case TreadMarksConstants.PTR:
                case TreadMarksConstants.PIT:
                    AddScaleTickets(mobileTransaction, transaction, out scaleWeight);
                    ///PIT Materials
                    if (mobileTransaction.TransactionType.ShortNameKey == TreadMarksConstants.PIT)
                        foreach (var pair in mobileTransaction.TransactionMaterialTypes)
                        {
                            int mappedProductID = EnumTypeConverter.ConvertToProductType((int)pair.MaterialType.ID);
                            if (mappedProductID == 0) throw new Exception("Product ID not found!");
                            var item = DataLoader.ProcessorPITTransactionProducts.FirstOrDefault(p => p.ID == mappedProductID);
                            if (item != null)
                            {
                                //OTSTM2-1198 : ProcessorPIT Transaction need to check if specific PI rate is existing
                                decimal ItemRate = 0.00M;
                                //not used data loader as ws cache is not cleared upon changing rates
                                var specificRate = settingRepository.LoadSpecificRates().FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= DateTime.Now.Date && c.EffectiveEndDate >= DateTime.Now.Date && c.VendorID == transaction.OutgoingId);
                                if (specificRate != null)
                                {
                                    ItemRate = specificRate.ItemRate;
                                }
                                else
                                {
                                    Rate globalRate;
                                    //not used data loader as ws cache is not cleared upon changing rates
                                    globalRate = settingRepository.LoadRates(DateTime.Now).FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= DateTime.Now.Date && c.EffectiveEndDate >= DateTime.Now.Date);
                                    ItemRate = globalRate != null ? globalRate.ItemRate : 0.00M;
                                }

                                var tItem = new TransactionItem();
                                tItem.ItemID = item.ID;
                                tItem.UnitType = TreadMarksConstants.Ton;
                                tItem.Quantity = 1;
                                tItem.Rate = ItemRate;
                                tItem.ActualWeight = mobileTransaction.ScaleTickets.Count() > 0 ? scaleWeight : 0.00M;
                                tItem.AverageWeight = tItem.ActualWeight;
                                transaction.TransactionItems.Add(tItem);
                            }
                        }
                    break;
            }
            return transaction;
        }

        private void CheckLocationVariance(Transaction transaction)
        {
            //Add warning notes for duplicate scale tickets
            List<string> warnings = new List<string>();

            double distance = DistanceLookup.GetDistance((double)transaction.OutgoingGpsLog.Latitude, (double)transaction.OutgoingGpsLog.Longitude, transaction.OutgoingVendorRef.Address.PostalCode);

            string DistanceVariance = settingRepository.GetSettingValue("DistanceVariance");

            if (distance >= Convert.ToDouble(DistanceVariance))
            {
                string note = string.Format("Location variance ({0} Km) detected. Please check the map to confirm the transaction was completed at the Collector's registered address.", string.Format("{0:N2}", (distance / 1000)));
                warnings.Add(note);
            }
            //Add notes only for completed transactions
            AddNotes(warnings, transaction);
        }

        private void CheckDuplicateScaleTickets(Transaction transaction)
        {
            //Add warning notes for duplicate scale tickets
            List<string> warnings = new List<string>();

            foreach (ScaleTicket s in transaction.ScaleTickets)
            {
                var vendorType = vendorRepository.GetVendorById(transaction.IncomingId.Value).VendorType;
                var result = transactionRepository.GetDuplicateScaleTicket(transaction.TransactionTypeCode, transaction.IncomingId.Value, s.TicketNumber, GetPeriodType(vendorType));
                if (result.Count > 0)
                {
                    result.ToList().ForEach(c =>
                    {
                        string dupScaleTicket = string.Format("{0} has used the scale ticket #{1} before with {2} in {3} claim Trans # {4}", c.IncomingVendor.Number, c.TicketNumber, c.OutgoingVendor.Number, c.PeriodShortName, c.TransactionNumber);
                        warnings.Add(dupScaleTicket);
                    });
                }
            }
            //Add notes only for completed transactions
            AddNotes(warnings, transaction);
        }

        private Transaction SetTransactionForSave(MobileTransaction mobileTransaction, ref List<string> warnings)
        {
            var incomingVendor = vendorRepository.GetVendorByNumber(mobileTransaction.IncomingRegistrationNumber.ToString());
            var outGoingVendor = vendorRepository.GetVendorByNumber(mobileTransaction.OutgoingRegistrationNumber.ToString());
            var TransactionType = DataLoader.TransactionTypes.Single(c => c.Code == mobileTransaction.TransactionType.ShortNameKey);

            string[] deviceNameMatches = Regex.Matches(mobileTransaction.DeviceName, @"\[([^]]*)\]").Cast<Match>().Select(x => x.Groups[1].Value).ToArray();

            //Common fields
            var transaction = new Transaction
            {
                TransactionId = mobileTransaction.ID,
                MobileFormat = true,
                TransactionTypeCode = mobileTransaction.TransactionType.ShortNameKey,
                FriendlyId = mobileTransaction.FriendlyId,
                //OTSM-1567 device started putting transaction end date in TransactionDate
                TransactionDate = mobileTransaction.CreatedDate,
                TransactionEndDate = mobileTransaction.TransactionDate,
                //Status = TransactionStatus.Incomplete.ToString(),
                IsEligible = TransactionType != null ? TransactionType.IsFinancialEligible : false,
                //App User Id 
                //CreatedUserId = mobileTransaction.CreatedUserId,
                CreatedDate = mobileTransaction.CreatedDate,
                SyncDate = mobileTransaction.SyncDate,
                IncomingSignatureName = mobileTransaction.IncomingSignatureName,
                OutgoingSignatureName = mobileTransaction.OutgoingSignatureName,
                //Everything is saved in Kgs
                UnitType = TreadMarksConstants.Kg,
                //If not in correct format, put everything in deviceName and keep deviceId empty
                DeviceName = deviceNameMatches.Length > 1 ? deviceNameMatches[1] : mobileTransaction.DeviceName,
                DeviceId = deviceNameMatches.Length > 1 ? deviceNameMatches[0] : string.Empty,
                ProcessingStatus = TransactionProcessingStatus.Unreviewed.ToString()
            };

            switch (mobileTransaction.TransactionStatusTypeId)
            {
                case (long)TransactionStatusEnum.ISSUES:
                    transaction.Status = TransactionStatus.Error.ToString();
                    break;
                case (long)TransactionStatusEnum.INCOMPLETE:
                    transaction.Status = TransactionStatus.Incomplete.ToString();
                    break;
                case (long)TransactionStatusEnum.COMPLETE:
                    transaction.Status = TransactionStatus.Completed.ToString();
                    break;
                case (long)TransactionStatusEnum.DELETED:
                    transaction.Status = TransactionStatus.Voided.ToString();
                    break;
            }

            //Can be null for incomplete ttansactions
            if (incomingVendor != null)
            {
                transaction.IncomingId = incomingVendor.VendorId;
                transaction.IncomingAddressId = incomingVendor.Address.ID;
                transaction.ToPostalCode = incomingVendor.Address.PostalCode;
                transaction.Badge = string.Format("{0}-{1}", incomingVendor.RegistrationNumber, mobileTransaction.CreatedUserId);
                transaction.IncomingVendorRef = incomingVendor;
            }

            if (outGoingVendor != null)
            {
                transaction.OutgoingId = outGoingVendor.VendorId;
                transaction.OutgoingAddressId = outGoingVendor.Address.ID;
                transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(outGoingVendor.Address.PostalCode);
                transaction.FromPostalCode = string.IsNullOrEmpty(mobileTransaction.PostalCode1) ? outGoingVendor.Address.PostalCode : mobileTransaction.PostalCode1 + mobileTransaction.PostalCode2;
                transaction.OutgoingVendorRef = outGoingVendor;
            }

            switch (mobileTransaction.TransactionType.ShortNameKey)
            {
                //Business logic is same for TCR and DOT
                case TreadMarksConstants.TCR:
                case TreadMarksConstants.DOT:
                    //Add warnings
                    if (outGoingVendor != null && !outGoingVendor.Address.PostalCode.ToLowerInvariant().StartsWith(mobileTransaction.PostalCode1.ToLowerInvariant()))
                        warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] Does not match Postal Code on file. Please review or contact us if you require further assistance.", DateTime.Now, DateTime.Now));
                    break;
                case TreadMarksConstants.PTR:
                case TreadMarksConstants.PIT:
                    //Flip outgoing to incoming in order to fix devices bug
                    if (incomingVendor != null && outGoingVendor != null)
                    {
                        transaction.OutgoingId = incomingVendor.VendorId;
                        transaction.OutgoingAddressId = incomingVendor.Address.ID;
                        transaction.IncomingId = outGoingVendor.VendorId;
                        transaction.IncomingAddressId = outGoingVendor.Address.ID;
                        transaction.ZoneId = ClaimHelper.GetZoneIdByPostalCode(outGoingVendor.Address.PostalCode);
                        transaction.Badge = string.Format("{0}-{1}", outGoingVendor.RegistrationNumber, mobileTransaction.CreatedUserId);
                        transaction.FromPostalCode = string.IsNullOrEmpty(mobileTransaction.PostalCode1) ? incomingVendor.Address.PostalCode : mobileTransaction.PostalCode1 + mobileTransaction.PostalCode2;
                        transaction.ToPostalCode = outGoingVendor.Address.PostalCode;

                        //flip signatures
                        transaction.IncomingSignatureName = mobileTransaction.OutgoingSignatureName;
                        transaction.OutgoingSignatureName = mobileTransaction.IncomingSignatureName;
                    }

                    break;
            }

            CheckActiveStatus(ref warnings, incomingVendor, outGoingVendor, transaction, mobileTransaction);

            GetDuplicatePaperTransactions(ref warnings, (int)mobileTransaction.FriendlyId);

            return transaction;
        }

        //public bool HasAnyDuplicatePaperTransaction(int friendlyId)
        private void GetDuplicatePaperTransactions(ref List<string> warnings, int friendlyId)
        {
            if (transactionRepository.HasAnyDuplicatePaperTransaction(friendlyId))
            {
                warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] This transaction number already exists in the system.", DateTime.Now, DateTime.Now));
            }

            //OTSTM2-414 First version of implementation used reg# and the claim periods, later changed to include all transactions not only completed ones
            //var duplicateTrans = transactionRepository.GetDuplicatePaperTransactions(friendlyId);
            //foreach (var trans in duplicateTrans)
            //{
            //    if (trans.OutgoingVendorTypeInt == 0)
            //        warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] Duplicated with Registration #: {2}, Claim Period: {3}"
            //        , DateTime.Now, DateTime.Now, trans.IncomingRegistrationNumber, trans.IncomingPeriodShortName));
            //    else
            //        warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] Duplicated with a Transaction between Registration #: {2}, Claim Period: {3} and Registration #: {4}, Claim Period: {5}"
            //            , DateTime.Now, DateTime.Now, trans.IncomingRegistrationNumber, trans.IncomingPeriodShortName, trans.OutgoingRegistrationNumber, trans.OutgoingPeriodShortName));
            //}
        }

        private void RunEventNumberRule(Transaction transaction)
        {
            STCEventNumberValidationStatus result = EventNumberValidationCheck(transaction.EventNumber, transaction.TransactionDate);

            //Add warning notes for duplicate scale tickets
            string warning = "";

            if (result == STCEventNumberValidationStatus.IsUsed)
                warning = "Event number is already in use.";
            else if (result == STCEventNumberValidationStatus.NotExisting || result == STCEventNumberValidationStatus.NotInDateRange)
                warning = "Event number does not exist.";

            if (warning != "")
            {
                //Add notes only for completed transactions
                AddMobileTransactionNote(transaction, warning);
                transactionRepository.UdpateTransactioProcessingStatus(transaction.ID, TransactionProcessingStatus.Invalidated);
            }
            //Valid event number, update the event record for correct transaction Id
            else
                transactionRepository.AddTransactionIdIntoEvent(transaction.ID, transaction.EventNumber);
        }

        private void CheckActiveStatus(ref List<string> warnings, VendorReference incomingVendor, VendorReference outGoingVendor, DataContracts.DomainEntities.Transaction transaction, MobileTransaction mobileTransaction)
        {
            DateTime transDate = new DateTime(mobileTransaction.TransactionDate.ToLocalTime().Year, mobileTransaction.TransactionDate.ToLocalTime().Month, mobileTransaction.TransactionDate.ToLocalTime().Day);

            if (incomingVendor != null && vendorRepository.IsNotActive(incomingVendor.VendorId, transDate))
            {
                warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] {2} ({3}) was inactive on transaction date.", DateTime.Now, DateTime.Now, GetVendorCode(incomingVendor.VendorType), incomingVendor.RegistrationNumber));
                transaction.ProcessingStatus = TransactionProcessingStatus.Invalidated.ToString();
            }
            if (outGoingVendor != null && vendorRepository.IsNotActive(outGoingVendor.VendorId, transDate))
            {
                warnings.Add(string.Format("[{0:D}, {1:HH:mm:ss}] {2} ({3}) was inactive on transaction date.", DateTime.Now, DateTime.Now, GetVendorCode(outGoingVendor.VendorType), outGoingVendor.RegistrationNumber));
                transaction.ProcessingStatus = TransactionProcessingStatus.Invalidated.ToString();
            }
        }

        private string GetVendorCode(int VendorType)
        {
            switch (VendorType)
            {
                case 2:
                    return TreadMarksConstants.Collector;
                case 3:
                    return TreadMarksConstants.Hauler;
                case 4:
                    return TreadMarksConstants.Processor;
                case 5:
                    return TreadMarksConstants.RPM;
                default:
                    return "Registrant";
            }
        }

        private void AddGpsLogs(MobileTransaction mobileTransaction, Transaction transaction)
        {
            var incomingGps = mobileTransaction.IncomingGpsLogId != null ? mobileTransactionRepository.GetGps(mobileTransaction.IncomingGpsLogId.Value) : null;
            if (incomingGps != null)
            {
                transaction.IncomingGpsLog = new GpsLog()
                {
                    Latitude = incomingGps.Latitude,
                    Longitude = incomingGps.Longitude,
                    SyncDate = incomingGps.SyncDate
                };
            }
            else
            {
                var incomingSignaturePhoto = transaction.TransactionPhotos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.IncomingSignature).FirstOrDefault();
                if (incomingSignaturePhoto != null && incomingSignaturePhoto.Photo.Latitude != null && incomingSignaturePhoto.Photo.Longitude != null)
                {
                    transaction.IncomingGpsLog = new GpsLog()
                    {
                        Latitude = incomingSignaturePhoto.Photo.Latitude.Value,
                        Longitude = incomingSignaturePhoto.Photo.Longitude.Value,
                        SyncDate = mobileTransaction.SyncDate
                    };

                    //Add warning notes for missing GPS
                    transaction.TransactionNotes.Add(new TransactionNote { Note = "Inbound Signature GPS in lieu of Inbound Scan GPS.", CreatedDate = DateTime.UtcNow, UserId = null, NoteType = TransactionNoteType.General.ToString() });
                }
            }

            var outgoingGps = mobileTransactionRepository.GetGps(mobileTransaction.OutgoingGpsLogId);
            if (outgoingGps != null)
            {
                transaction.OutgoingGpsLog = new GpsLog()
                {
                    Latitude = outgoingGps.Latitude,
                    Longitude = outgoingGps.Longitude,
                    SyncDate = outgoingGps.SyncDate
                };
            }
            else
            {
                //not required for STC transactions 
                if (mobileTransaction.TransactionType.ShortNameKey != TreadMarksConstants.STC)
                {
                    var outgoingSignaturePhoto = transaction.TransactionPhotos.Where(p => p.Photo.PhotoType == (int)PhotoTypeEnum.OutgoingSignature).FirstOrDefault();
                    if (outgoingSignaturePhoto != null && outgoingSignaturePhoto.Photo.Latitude != null && outgoingSignaturePhoto.Photo.Longitude != null)
                    {
                        transaction.OutgoingGpsLog = new GpsLog()
                        {
                            Latitude = outgoingSignaturePhoto.Photo.Latitude.Value,
                            Longitude = outgoingSignaturePhoto.Photo.Longitude.Value,
                            SyncDate = mobileTransaction.SyncDate
                        };

                        //Add warning notes for missing GPS
                        transaction.TransactionNotes.Add(new TransactionNote { Note = "Outbound Signature GPS in lieu of Outbound Scan GPS.", CreatedDate = DateTime.UtcNow, UserId = null, NoteType = TransactionNoteType.General.ToString() });
                    }
                }
            }
        }

        private static void AddEligibility(MobileTransaction mobileTransaction, Transaction transaction)
        {
            bool isGenerated = false;
            var mobileTransactionEligibilities = mobileTransaction.TransactionEligibilities.FirstOrDefault();

            foreach (var eligibility in mobileTransaction.TransactionEligibilities)
            {
                //Old code for TCR and DOT from josh
                if (eligibility.EligibilityId == 1)
                {
                    isGenerated = !eligibility.Value;
                }
                else if (eligibility.EligibilityId == 2)
                {
                    isGenerated = eligibility.Value;
                }

                //for UCR, there is a mispatch in Ids that we use in central and New TM
                if (eligibility.Value)
                    switch (eligibility.EligibilityId)
                    {
                        case 3:
                            transaction.PaymentEligible = 2;
                            break;
                        case 4:
                            transaction.PaymentEligible = 3;
                            break;
                    }
            }
            transaction.IsGenerateTires = isGenerated;
            transaction.IsEligible = !isGenerated;
        }

        private void AddCompanyInfo(Transaction transaction, DataContracts.Mobile.Authorization mobileAuthorization)
        {
            //get STC location from central DB
            var stcLocation = mobileTransactionRepository.GetLocationByAuthorizationId(mobileAuthorization.ID);

            var company = new CompanyInfo();
            company.CompanyName = stcLocation.Name;
            company.PhoneNumber = stcLocation.Phone;

            if (stcLocation.Latitude.HasValue && stcLocation.Longitude.HasValue)
            {
                var retrievedAddress = ReverseAddressLookup.RetrieveAddress(stcLocation.Latitude.Value, stcLocation.Longitude.Value);

                // Company Info
                retrievedAddress.AddressType = 1;
                company.Address = retrievedAddress;
            }
            else
            {
                //Set not null fields
                company.Address = new Address() { AddressType = 1, Address1 = "", Country = "", City = "" };
            }

            //company.Address = new Address
            //{
            //    AddressType = 1,
            //    Address1 = stcLocation.Address1 ?? "",
            //    Address2 = stcLocation.Address2,
            //    City = stcLocation.City ?? "",
            //    Province = stcLocation.Province ?? "",
            //    PostalCode = stcLocation.PostalCode ?? "",
            //    Country = stcLocation.Country ?? "",
            //    GPSCoordinate1 = stcLocation.Latitude,
            //    GPSCoordinate2 = stcLocation.Longitude
            //};
            transaction.CompanyInfo = company;
        }

        //Has to be called after AddTiresAndPhotos as we need to overrite the weights 
        //and TransactionItems should already be there in transaction object
        private void AddScaleTickets(MobileTransaction mobileTransaction, Transaction transaction, out decimal scaleWeight)
        {
            decimal InboundWeight = 0, OutboundWeight = 0;

            // Scale Ticket 
            foreach (var mobileScaleTicket in mobileTransaction.ScaleTickets)
            {
                //need to recalculate ActualWeight for each item so calculate scaleWeight as Inbound minus Oubound
                switch (mobileScaleTicket.ScaleTicketTypeId)
                {
                    case (int)ScaleTicketTypeEnum.Inbound:
                        InboundWeight = (mobileScaleTicket.UnitTypeId == TreadMarksConstants.Lb) ? DataConversion.ConvertLbToKg((double)mobileScaleTicket.InboundWeight.Value) : mobileScaleTicket.InboundWeight.Value;
                        break;
                    case (int)ScaleTicketTypeEnum.Outbound:
                        OutboundWeight = (mobileScaleTicket.UnitTypeId == TreadMarksConstants.Lb) ? DataConversion.ConvertLbToKg((double)mobileScaleTicket.OutboundWeight.Value) : mobileScaleTicket.OutboundWeight.Value;
                        break;
                    case (int)ScaleTicketTypeEnum.Both:
                        InboundWeight = (mobileScaleTicket.UnitTypeId == TreadMarksConstants.Lb) ? DataConversion.ConvertLbToKg((double)mobileScaleTicket.InboundWeight.Value) : mobileScaleTicket.InboundWeight.Value;
                        OutboundWeight = (mobileScaleTicket.UnitTypeId == TreadMarksConstants.Lb) ? DataConversion.ConvertLbToKg((double)mobileScaleTicket.OutboundWeight.Value) : mobileScaleTicket.OutboundWeight.Value;
                        break;
                }

                var scaleTicket = new ScaleTicket()
                {
                    InboundWeight = InboundWeight,
                    OutboundWeight = OutboundWeight,
                    TicketNumber = mobileScaleTicket.TicketNumber,
                    SortIndex = (int)mobileScaleTicket.SortIndex,
                    CreateDate = mobileScaleTicket.Date,
                    ScaleTicketType = (int)mobileScaleTicket.ScaleTicketTypeId,
                    UnitType = (int)mobileScaleTicket.UnitTypeId,
                    //TransactionId = transaction.ID, // added by entity framework
                };

                transaction.ScaleTickets.Add(scaleTicket);
            }

            scaleWeight = InboundWeight - OutboundWeight;

            //Override transaction item actual weight // Don't do it for PIT as per issue found by Fahad "Distribution of weight could not be processed"
            if (transaction.ScaleTickets.Count > 0 && mobileTransaction.TransactionType.ShortNameKey != TreadMarksConstants.PIT)
                DistributeScaleWeight(transaction, scaleWeight);

            ////Override transaction item actual weight
            //decimal onRoadWeight = 0, offRoadWeight = 0;
            //var totalAverageWeight = transaction.TransactionItems.Sum(c => c.AverageWeight);
            //transaction.TransactionItems.ForEach(c =>
            //{
            //    //c.ActualWeight = ((decimal)scaleTicket.InboundWeight) * (c.AverageWeight / totalAverageWeight);
            //    c.ActualWeight = (scaleWeight) * (c.AverageWeight / totalAverageWeight);
            //    var item = DataLoader.TransactionItems.Single(i => i.ID == c.ItemID);
            //    if (item.ItemType == 1) onRoadWeight += c.ActualWeight;
            //    if (item.ItemType == 2) offRoadWeight += c.ActualWeight;
            //});
            //transaction.OnRoadWeight = Math.Round(onRoadWeight, 4, MidpointRounding.AwayFromZero);
            //transaction.OffRoadWeight = Math.Round(offRoadWeight, 4, MidpointRounding.AwayFromZero);
        }

        private void AddTiresPhotosComments(MobileTransaction mobileTransaction, Transaction transaction)
        {
            // Transaction Item
            decimal onRoadWeight = 0, offRoadWeight = 0;
            foreach (var pair in mobileTransaction.TransactionTireTypes)
            {
                var item = DataLoader.TransactionItems.Single(c => c.ShortName == pair.TireType.ShortNameKey);

                var transactionItem = new TransactionItem();
                transactionItem.ItemID = item.ID;
                transactionItem.UnitType = item.UnitType;
                transactionItem.Quantity = (int?)pair.Quantity;

                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(c => c.ItemID == item.ID && c.EffectiveStartDate <= mobileTransaction.TransactionDate.Date && c.EffectiveEndDate >= mobileTransaction.TransactionDate.Date);
                if (itemWeight != null)
                {
                    transactionItem.AverageWeight = Math.Round((pair.Quantity * itemWeight.StandardWeight), 4, MidpointRounding.AwayFromZero);
                    transactionItem.ActualWeight = transactionItem.AverageWeight;
                }
                transaction.TransactionItems.Add(transactionItem);

                if (item.ItemType == 1) onRoadWeight += transactionItem.ActualWeight;
                else offRoadWeight += transactionItem.ActualWeight;
            }
            transaction.OnRoadWeight = Math.Round(onRoadWeight, 4, MidpointRounding.AwayFromZero);
            transaction.OffRoadWeight = Math.Round(offRoadWeight, 4, MidpointRounding.AwayFromZero);
            transaction.EstimatedOnRoadWeight = Math.Round(onRoadWeight, 4, MidpointRounding.AwayFromZero);
            transaction.EstimatedOffRoadWeight = Math.Round(offRoadWeight, 4, MidpointRounding.AwayFromZero);

            //Add photos
            foreach (var mobilePhoto in mobileTransaction.Photos)
            {
                var photo = new Photo()
                {
                    Comments = mobilePhoto.Comments,
                    CreatedDate = mobilePhoto.CreatedDate,
                    Latitude = mobilePhoto.Latitude,
                    Longitude = mobilePhoto.Longitude,
                    //PhotoType = mobilePhoto.PhotoTypeId,
                    FileName = mobilePhoto.UniqueFileName,
                };

                switch (mobilePhoto.PhotoTypeId)
                {
                    case (int)LegacyPhotoTypeEnum.Photo:
                        photo.PhotoType = (int)PhotoTypeEnum.Photo;
                        break;
                    //The whole phototype enum is being created for tracking incoming and outgoing signature photos
                    case (int)LegacyPhotoTypeEnum.Signature:
                        photo.PhotoType = mobilePhoto.ID == mobileTransaction.IncomingSignaturePhotoId ? (int)PhotoTypeEnum.IncomingSignature : (int)PhotoTypeEnum.OutgoingSignature;

                        //Flip in case of PIT and PTR
                        if (mobileTransaction.TransactionType.ShortNameKey == TreadMarksConstants.PIT || mobileTransaction.TransactionType.ShortNameKey == TreadMarksConstants.PTR)
                        {
                            photo.PhotoType = mobilePhoto.ID == mobileTransaction.IncomingSignaturePhotoId ? (int)PhotoTypeEnum.OutgoingSignature : (int)PhotoTypeEnum.IncomingSignature;
                        }

                        break;
                    case (int)LegacyPhotoTypeEnum.Document:
                        photo.PhotoType = (int)PhotoTypeEnum.Document;
                        break;
                    case (int)LegacyPhotoTypeEnum.ScaleTicket:
                        photo.PhotoType = (int)PhotoTypeEnum.ScaleTicket;
                        break;
                }

                //transaction.Photos.Add(photo);
                transaction.TransactionPhotos.Add(new TransactionPhoto() { Photo = photo });
            }

            //Add comments
            var mobileComments = new List<TransactionComment>();
            foreach (var mobileComment in mobileTransaction.Comments)
            {
                var transactionComment = new TransactionComment()
                {
                    Text = mobileComment.Text,
                    CreatedDate = mobileComment.CreatedDate
                };
                mobileComments.Add(transactionComment);
            }
            transaction.TransactionComments = mobileComments;

        }

        public MobileTransaction GetMobileTransaction(Guid transactionId)
        {
            return mobileTransactionRepository.GetTransaction(transactionId);
        }

        #endregion

        #region List & Export related methods

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                if (c.VendorId != 0)
                {
                    var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                    c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                    c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
                }
                else
                {
                    c.VendorGroupName = "";
                    c.VendorRateGroupName = "";
                }

            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerUCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                if (c.VendorId != 0)
                {
                    var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                    c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                    c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
                }
                else
                {
                    c.VendorGroupName = "";
                    c.VendorRateGroupName = "";
                }
            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType)
        {
            var result = transactionRepository.LoadHaulerHITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, SecurityContextHelper.CurrentVendor.VendorId, dataSubType);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerSTCTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                if (c.VendorId != 0)
                {
                    var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                    c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                    c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
                }
                else
                {
                    c.VendorGroupName = "";
                    c.VendorRateGroupName = "";
                }
            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.DeliveryGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerRTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);

            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffUCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                if (c.VendorId != 0)
                {
                    var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                    c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                    c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
                }
                else
                {
                    c.VendorGroupName = "";
                    c.VendorRateGroupName = "";
                }
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType)
        {
            var result = transactionRepository.LoadHaulerStaffHITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, SecurityContextHelper.CurrentVendor.VendorId, dataSubType);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffSTCTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                if (c.VendorId != 0)
                {
                    var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                    c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                    c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
                }
                else
                {
                    c.VendorGroupName = "";
                    c.VendorRateGroupName = "";
                }
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.DeliveryGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }
        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadHaulerStaffRTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            result.DTOCollection.ForEach(c =>
            {
                var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
                c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
                c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            });
            return result;
        }


        #region All Transactions
        public PaginationDTO<AllTransactionsViewModel, int> LoadAllTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt = "")
        {
            PaginationDTO<AllTransactionsViewModel, int> PageofTransactionList = transactionRepository.LoadAllTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, queryFilter, sExtendSearchTxt);

            PageofTransactionList.DTOCollection.ForEach(c => c.ClaimReviewStatus = (c.Status == TransactionAdjustmentStatus.Pending.ToString()) ? this.claimsRepository.GetClaimStatusByTransactionId(Int32.Parse(c.TransactionId)) : c.ClaimReviewStatus);

            return PageofTransactionList;
        }
        #endregion

        public List<AllTransactionsViewModel> GetTransactionDetailsExport(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, string subType, string queryFilter)
        {
            return this.transactionRepository.LoadAllTransactionsList(searchText, orderBy, sortDirection, vendorId, queryFilter);
        }

        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            return transactionRepository.LoadCollectorTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            return transactionRepository.LoadCollectorDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadCollectorStaffTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            //Not required for Collector for now, Collector can have 3 different TI rates (which means 3 different rate/collector groups)
            // effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            //result.DTOCollection.ForEach(c =>
            //{
            //    if (c.VendorId != 0)
            //    {
            //        var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
            //        c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
            //        c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            //    }
            //    else
            //    {
            //        c.VendorGroupName = "";
            //        c.VendorRateGroupName = "";
            //    }
            //});
            return result;
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadCollectorStaffDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);

            //Not required for Collector for now, Collector can have 3 different TI rates (which means 3 different rate/collector groups)
            //var effectiveGroups = configurationsRepository.LoadEffectiveVendorGroups(claimPeriod.StartDate, claimPeriod.EndDate, TreadMarksConstants.PickupGroup);

            //result.DTOCollection.ForEach(c =>
            //{
            //    if (c.VendorId != 0)
            //    {
            //        var effectiveGroup = effectiveGroups.Where(v => v.VendorId == c.VendorId).FirstOrDefault();
            //        c.VendorGroupName = effectiveGroup != null ? effectiveGroup.GroupName : "";
            //        c.VendorRateGroupName = effectiveGroup != null ? effectiveGroup.RateGroupName : "";
            //    }
            //    else
            //    {
            //        c.VendorGroupName = "";
            //        c.VendorRateGroupName = "";
            //    }
            //});
            return result;
        }

        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;

            //Rate Group Rate Changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(claimPeriod.StartDate, claimPeriod.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            var result = transactionRepository.LoadProcessorPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            result.DTOCollection.ForEach(c =>
            {
                c.AmountDollar = ClaimHelper.GetPTRClaimAmount(c.TireCountList, claimPeriod, c.IncomingId, rateGroupRates, vendorRateGroups);
            });
            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;

            List<TypeDefinition> DispositionReasonList = DataLoader.DispositionReasonList;

            var result = transactionRepository.LoadProcessorDORTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, DispositionReasonList);

            //Rate Group Rate Changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(claimPeriod.StartDate, claimPeriod.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            result.DTOCollection.ForEach(c =>
            {
                c.AmountDollar = ClaimHelper.GetDORClaimAmount(c, c.OutgoingId, rateGroupRates, vendorRateGroups);
            });
            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var result = transactionRepository.LoadProcessorPITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, vendorId, dataSubType);
            result.DTOCollection.ForEach(c =>
            {
                c.ScaleWeight = DataConversion.ConvertKgToTon((double)c.ScaleWeight);
                c.AmountDollar = (c.Rate ?? 0) * (c.ScaleWeight ?? 0);
            });
            transactionRepository.TransactionItemAdjustment(result.DTOCollection, TreadMarksConstants.PIT);
            result.DTOCollection.ForEach(c =>
            {
                c.ProductType = (null != DataLoader.Items.FirstOrDefault(i => i.ID == Int32.Parse(string.IsNullOrEmpty(c.ProductType) ? "0" : c.ProductType))) ? DataLoader.Items.FirstOrDefault(i => i.ID == Int32.Parse(c.ProductType)).ShortName : string.Empty;
            });
            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadProcessorSPSTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            result.DTOCollection.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    i.Item = DataLoader.Items.FirstOrDefault(q => q.ID == i.ItemID);
                });
                c.ProductType = c.TransactionItems.FirstOrDefault().Item.ShortName ?? string.Empty;
                c.MeshRange = string.Concat(c.TransactionItems.FirstOrDefault().MeshRangeStart,
                    (null != c.TransactionItems.FirstOrDefault().MeshRangeStart ? " to " : string.Empty), c.TransactionItems.FirstOrDefault().MeshRangeEnd);
                c.Rate = c.TransactionItems.FirstOrDefault().Rate;
                c.TireCountList = c.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                c.TotalWeight = c.TransactionItems.FirstOrDefault().ActualWeight;

                c.ScaleWeight = DataConversion.ConvertKgToTon((double)c.ScaleWeight);
                c.EstWeight = DataConversion.ConvertKgToTon((double)c.EstWeight);
                c.AmountDollar = ClaimHelper.GetSPSClaimAmount(c);

            });
            transactionRepository.TransactionItemAdjustment(result.DTOCollection, TreadMarksConstants.SPS);

            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;
            var result = transactionRepository.LoadProcessorStaffPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);

            //Rate Group Rate Changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(claimPeriod.StartDate, claimPeriod.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            result.DTOCollection.ForEach(c =>
            {
                c.AmountDollar = ClaimHelper.GetPTRClaimAmount(c.TireCountList, claimPeriod, c.IncomingId, rateGroupRates, vendorRateGroups);
            });
            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;
            List<TypeDefinition> DispositionReasonList = DataLoader.DispositionReasonList;

            var result = transactionRepository.LoadProcessorStaffDORTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, DispositionReasonList);

            //Rate Group Rate Changes
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(claimPeriod.StartDate, claimPeriod.EndDate);
            var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
            var vendorRateGroups = new List<VendorRateGroup>();
            deliveryRateGroupIds.ForEach(c =>
            {
                vendorRateGroups.AddRange(configurationsRepository.LoadVendorRateGroups((int)c));
            });

            result.DTOCollection.ForEach(c =>
            {
                c.AmountDollar = ClaimHelper.GetDORClaimAmount(c, c.OutgoingId, rateGroupRates, vendorRateGroups);
            });

            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            var result = transactionRepository.LoadProcessorStaffPITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, vendorId, dataSubType);
            result.DTOCollection.ForEach(c =>
            {
                c.ScaleWeight = Math.Round(DataConversion.ConvertKgToTon((double)c.ScaleWeight), 4, MidpointRounding.AwayFromZero);
                c.AmountDollar = Math.Round((c.Rate ?? 0) * (c.ScaleWeight ?? 0), 2, MidpointRounding.AwayFromZero);
            });
            transactionRepository.TransactionItemAdjustment(result.DTOCollection, TreadMarksConstants.PIT);
            result.DTOCollection.ForEach(c =>
            {
                c.ProductType = (null != DataLoader.Items.FirstOrDefault(i => i.ID == Int32.Parse(string.IsNullOrEmpty(c.ProductType) ? "0" : c.ProductType))) ? DataLoader.Items.FirstOrDefault(i => i.ID == Int32.Parse(c.ProductType)).ShortName : string.Empty;
            });
            return result;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var result = transactionRepository.LoadProcessorStaffSPSTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);

            result.DTOCollection.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    i.Item = DataLoader.Items.FirstOrDefault(q => q.ID == i.ItemID);
                });
                c.ProductType = c.TransactionItems.FirstOrDefault().Item.ShortName ?? string.Empty;
                c.MeshRange = string.Concat(c.TransactionItems.FirstOrDefault().MeshRangeStart,
                    (null != c.TransactionItems.FirstOrDefault().MeshRangeStart ? " to " : string.Empty), c.TransactionItems.FirstOrDefault().MeshRangeEnd);
                c.Rate = c.TransactionItems.FirstOrDefault().Rate;
                c.TireCountList = c.TransactionItems.Where(i => i.TransactionAdjustmentId == null).Select(i => new TransactionItemListDetailViewModel() { ItemID = i.ItemID, Quantity = i.Quantity, ShortName = i.Item.ShortName, ActualWeight = i.ActualWeight, AverageWeight = i.ActualWeight }).ToList();
                c.TotalWeight = c.TransactionItems.FirstOrDefault().ActualWeight;

                c.ScaleWeight = DataConversion.ConvertKgToTon((double)(c.ScaleWeight ?? 0));
                c.EstWeight = DataConversion.ConvertKgToTon((double)c.EstWeight);
                c.AmountDollar = ClaimHelper.GetSPSClaimAmount(c);
            });

            transactionRepository.TransactionItemAdjustment(result.DTOCollection, TreadMarksConstants.SPS);
            return result;
        }

        public PaginationDTO<DataContracts.ViewModel.Transaction.RPM.RPMCommonTransactionListViewModel, int> LoadRPMStaffTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, string dataSubType, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;

            return transactionRepository.LoadRPMStaffTransactionList(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
        }

        public PaginationDTO<DataContracts.ViewModel.Transaction.RPM.RPMCommonTransactionListViewModel, int> LoadRPMVendorTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, string dataSubType, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            TimeSpan interval = new TimeSpan(23, 59, 59);
            claimPeriod.EndDate = claimPeriod.EndDate.Date + interval;

            return transactionRepository.LoadRPMTransactionList(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
        }

        public List<DataContracts.ViewModel.Transaction.RPM.RPMCommonTransactionListViewModel> GetRPMExportDetailsParticipantClaims(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, string dataSubType, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            List<RPMCommonTransactionListViewModel> query = new List<RPMCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.SPSR:
                    {
                        query = this.transactionRepository.GetRPMSPSTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
                        break;
                    }
                case TreadMarksConstants.SPS:
                    {
                        query = this.transactionRepository.GetRPMSIRTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
                        break;
                    }
                default:
                    return null;
            }
            //if (TreadMarksConstants.SPS == transactionType)
            //{
            //    this.transactionRepository.TransactionItemAdjustment(query);
            //    query.ForEach(c =>
            //    {
            //        c.TotalWeight = Math.Round(DataConversion.ConvertKgToTon((double)c.TotalWeight), 4, MidpointRounding.AwayFromZero);
            //    });
            //}
            return query;
        }

        public List<DataContracts.ViewModel.Transaction.RPM.RPMCommonTransactionListViewModel> GetRPMExportDetailsStaffClaims(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, string dataSubType, int claimId)
        {
            Period claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            List<RPMCommonTransactionListViewModel> query = new List<RPMCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.SPSR:
                    {
                        query = this.transactionRepository.GetRPMSPSTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
                        break;
                    }
                case TreadMarksConstants.SPS:
                    {
                        query = this.transactionRepository.GetRPMSIRTransactionList(searchText, orderBy, sortDirection, vendorId, transactionType, claimPeriod, claimId, dataSubType);
                        break;
                    }
                default:
                    return null;
            }
            //if (TreadMarksConstants.SPS == transactionType)
            //{
            //    this.transactionRepository.TransactionItemAdjustment(query);
            //    query.ForEach(c =>
            //    {
            //        c.TotalWeight = Math.Round(DataConversion.ConvertKgToTon((double)c.TotalWeight), 4, MidpointRounding.AwayFromZero);
            //    });
            //}
            return query;
        }
        #endregion

        //OTSTM2-620
        public bool IsTransAdjPendingAndUnreviewed(int claimId, int transactionId)
        {
            var transaction = transactionRepository.GetTransactionById(transactionId);

            var transAdj = transactionRepository.GetMostRecentTransactionAdjustment(claimId, transactionId);

            if (transAdj != null)
            {
                //check if most recent transaction adjustment is pending            
                return (string.Equals(transAdj.Status, "Pending") && string.Equals(transaction.ProcessingStatus, "Unreviewed"));
            }

            return false;
        }

        #region STC Premium Implementation
        public STCEventNumberValidationStatus EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate)
        {
            return transactionRepository.EventNumberValidationCheck(eventNumber, transactionStartDate);
        }
        #endregion
    }
}
