﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.System;
using CL.TMS.IRepository.Claims;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.RPM;

namespace CL.TMS.SystemBLL
{
    public class FileUploadBO
    {
        private IApplicationRepository applicationRepository;
        private IClaimsRepository claimsRepository;
        private ITSFRemittanceRepository tsfclaimsRepository;
        private ITransactionRepository transactionRepository;
        private IVendorRepository vendorRepository;
        private IRetailConnectionRepository retailConnectioRepository;


        public FileUploadBO(IApplicationRepository applicationRepository, IClaimsRepository claimsRepository, ITSFRemittanceRepository tsfclaimsRepository, ITransactionRepository transactionRepository, IVendorRepository vendorRepository, IRetailConnectionRepository retailConnectioRepository)
        {
            this.applicationRepository = applicationRepository;
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.tsfclaimsRepository = tsfclaimsRepository;
            this.vendorRepository = vendorRepository;
            this.retailConnectioRepository = retailConnectioRepository;
        }
        
        #region Application File Upload
        public void AddApplicationAttachments(int applicationId, IEnumerable<AttachmentModel> attachments)
        {
            applicationRepository.AddApplicationAttachments(applicationId, attachments);
        }
        public void RemoveApplicationAttachment(int applicationId, string fileUniqueName, string deletedBy)
        {
            this.applicationRepository.RemoveApplicationAttachment(applicationId, fileUniqueName);
        }
        public IEnumerable<AttachmentModel> GetApplicationAttachments(int applicationId, bool bBankInfo = false)
        {
            return applicationRepository.GetApplicationAttachments(applicationId, bBankInfo);
        }
        public AttachmentModel GetApplicationAttachment(int applicationId, string fileUniqueName)
        {
            return applicationRepository.GetApplicationAttachment(applicationId, fileUniqueName);
        }
        public void UpdateAttachmentDescription(string fileUniqueName, string description)
        {
            applicationRepository.UpdateAttachmentDescription(fileUniqueName, description);
        }
        #endregion


        #region VendorAttachmentService
        public void AddVendorAttachments(int vendorId, IEnumerable<AttachmentModel> attachments)
        {
            vendorRepository.AddVendorAttachments(vendorId, attachments);
        }
        public void RemoveVendorAttachment(int vendorId, string fileUniqueName, string deletedBy)
        {
            this.vendorRepository.RemoveVendorAttachment(vendorId, fileUniqueName);
        }
        public IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo = false)
        {
            return vendorRepository.GetVendorAttachments(vendorId, bBankInfo);
        }
        public AttachmentModel GetVendorAttachment(int vendorId, string fileUniqueName)
        {
            return vendorRepository.GetVendorAttachment(vendorId, fileUniqueName);
        }
        #endregion


        #region CustomerAttachmentService
        public void AddCustomerAttachments(int customerId, IEnumerable<AttachmentModel> attachments)
        {
            this.vendorRepository.AddCustomerAttachments(customerId, attachments);
        }
        public void RemoveCustomerAttachment(int customerId, string fileUniqueName, string deletedBy)
        {
            this.vendorRepository.RemoveCustomerAttachment(customerId, fileUniqueName);
        }
        public IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo = false)
        {
            return vendorRepository.GetCustomerAttachments(customerId, bBankInfo);
        }
        public AttachmentModel GetCustomerAttachment(int customerId, string fileUniqueName)
        {
            return vendorRepository.GetCustomerAttachment(customerId, fileUniqueName);
        }
        #endregion


        #region Claims File Upload
        public void AddClaimAttachments(int claimId, IEnumerable<AttachmentModel> attachments)
        {
            this.claimsRepository.AddClaimAttachments(claimId, attachments);
        }
        public void RemoveClaimAttachment(int claimId, string fileUniqueName, string deletedBy)
        {
            this.claimsRepository.RemoveClaimAttachment(claimId, fileUniqueName);
        }
        public IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo = false)
        {
            return this.claimsRepository.GetClaimAttachments(claimId, bBankInfo);
        }
        public AttachmentModel GetClaimAttachment(int claimId, string fileUniqueName)
        {
            return this.claimsRepository.GetClaimAttachment(claimId, fileUniqueName);
        }
        #endregion


        #region TSFClaims File Upload
        public void AddTSFClaimAttachments(int tsfclaimId, IEnumerable<AttachmentModel> attachments)
        {
            this.tsfclaimsRepository.AddTSFClaimAttachments(tsfclaimId, attachments);
        }
        public void RemoveTSFClaimAttachment(int tsfclaimId, string fileUniqueName, string deletedBy)
        {
            this.tsfclaimsRepository.RemoveTSFClaimAttachment(tsfclaimId, fileUniqueName);
        }
        public IEnumerable<AttachmentModel> GetTSFClaimAttachments(int tsfclaimId, bool bBankInfo = false)
        {
            return this.tsfclaimsRepository.GetTSFClaimAttachments(tsfclaimId, bBankInfo);
        }
        public AttachmentModel GetTSFClaimAttachment(int tsfclaimId, string fileUniqueName)
        {
            return this.tsfclaimsRepository.GetTSFClaimAttachment(tsfclaimId, fileUniqueName);
        }
        #endregion


        #region Transaction File Upload
        public void AddTransactionAttachments(List<AttachmentModel> attachmentModelList)
        {
            this.transactionRepository.AddTransactionAttachments(attachmentModelList);
        }

        public AttachmentModel GetTransactionAttachment(int attachmentId)
        {
            return this.transactionRepository.GetTransactionAttachment(attachmentId);
        }

        public void RemoveTemporaryTransactionAttachment(int attachmentId)
        {
            this.transactionRepository.RemoveTemporaryTransactionAttachment(attachmentId);
        }

        public void RemoveTemporaryTransactionAttachments(List<int> attachmentIdList)
        {
            this.transactionRepository.RemoveTemporaryTransactionAttachments(attachmentIdList);
        }
        #endregion        

        #region Product File Upload
        public void AddProductAttachment(AttachmentModel attachmentModel)
        {
            this.retailConnectioRepository.AddProductAttachment(attachmentModel);
        }
        public AttachmentModel GetProductAttachment(int attachmentId)
        {
            return this.retailConnectioRepository.GetProductAttachment(attachmentId);
        }

        public void RemoveTemporaryProductAttachment(int attachmentId)
        {
            this.retailConnectioRepository.RemoveTemporaryProductAttachment(attachmentId);
        }

        #endregion
    }
}
