﻿using CL.TMS.Caching;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.EmailSettings;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.UI.Common.Helper;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CL.TMS.SystemBLL
{
    public class EmailSettingsBO
    {
        
        private IEmailSettingsRepository emailSettingsRepository;

        public EmailSettingsBO(IEmailSettingsRepository emailSettingsRepository)
        {           
            this.emailSettingsRepository = emailSettingsRepository;
        }

        //Email Settings - General
        public EmailSettingsGeneralVM LoadEmailSettingsGeneralInformation()
        {
            return emailSettingsRepository.LoadEmailSettingsGeneralInformation();
        }

        public bool UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM)
        {
            emailSettingsRepository.UpdateEmailSettingsGeneralInformation(emailSettingsGeneralVM);
            //refresh cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
            return true;
        }

        //Email Settings - Content
        public Dictionary<int, string> GetEmailDisplayNamesList()
        {
            return emailSettingsRepository.GetEmailDisplayNamesList();
        }
        public EmailVM GetEmailByID(int emailID)
        {
            var vm = new EmailVM();

            var result = emailSettingsRepository.GetEmailByID(emailID);
            if (result != null)
            {
                vm.ID = result.ID;
                vm.Body = result.Body;
                vm.Name = result.Name;
                vm.DisplayName = result.DisplayName;
                vm.Subject = result.Subject;
                vm.IsEnabled = result.IsEnabled;
                vm.ButtonLabel = result.ButtonLabel;
            }
            return vm;
        }

        public bool SaveEmailContent(EmailVM emailContent)
        {
            emailSettingsRepository.SaveEmailContent(emailContent);
            EmailContentHelper.Reset();
            return true;
        }
        public string GetReadyToSendEmailByID(int emailID, bool isPreview = false)
        {
            //Construct email with EmailElements
            var email = emailSettingsRepository.GetEmailByID(emailID);
            var emailElements = emailSettingsRepository.GetEmailElements();

            StringBuilder sbEmail = new StringBuilder();
            sbEmail.Append(emailElements.Where(e => e.ElementType == "Container").FirstOrDefault().Content);
            sbEmail.Replace("[*PageTitle*]", email.Title);
            sbEmail.Replace("[*TopHeader*]", emailElements.Where(e => e.ElementType == "TopHeader").FirstOrDefault().Content);

            //Show hide Logos on the top green bar
            if(SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                sbEmail.Replace("[*CompanyLogo*]", emailElements.Where(e => e.ElementType == "CompanyLogo").FirstOrDefault().Content);
            else
                sbEmail.Replace("[*CompanyLogo*]", "");

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                sbEmail.Replace("[*FacebookLogo*]", emailElements.Where(e => e.ElementType == "FacebookLogo").FirstOrDefault().Content);
            else
                sbEmail.Replace("[*FacebookLogo*]", "");

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                sbEmail.Replace("[*TwitterLogo*]", emailElements.Where(e => e.ElementType == "TwitterLogo").FirstOrDefault().Content);
            else
                sbEmail.Replace("[*TwitterLogo*]", "");

            sbEmail.Replace("[*Header*]", emailElements.Where(e => e.ElementType == "Header").FirstOrDefault().Content);
            sbEmail.Replace("[*Footer*]", emailElements.Where(e => e.ElementType == "Footer").FirstOrDefault().Content);
            sbEmail.Replace("[*ButtonSection*]", email.ButtonSection ?? "");
            sbEmail.Replace("[*HeaderSection*]", email.HeaderSection ?? "");

            //links
            sbEmail.Replace(HaulerApplicationEmailTemplPlaceHolders.TwitterURL, AppSettings.Instance.GetSettingValue("Company.TwitterURL"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.FacebookURL, SiteSettings.Instance.GetSettingValue("Company.FacebookURL"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.WebsiteURL, SiteSettings.Instance.GetSettingValue("Company.WebsiteURL"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.AddressLine1, SiteSettings.Instance.GetSettingValue("Company.AddressLine1"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.AddressLine2, SiteSettings.Instance.GetSettingValue("Company.AddressLine2"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyCity, SiteSettings.Instance.GetSettingValue("Company.City"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyProvince, SiteSettings.Instance.GetSettingValue("Company.Province"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyPostalCode, SiteSettings.Instance.GetSettingValue("Company.PostalCode"))
                        .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyPhone, SiteSettings.Instance.GetSettingValue("Company.Phone"));

            //if isPreview, these fields are coming from UI
            if (!isPreview)
            {
                sbEmail.Replace("[*Body*]", email.Body);
                sbEmail.Replace("[*EmailTitle*]", email.Title);
                sbEmail.Replace("@ButtonLabel", email.ButtonLabel);
            }

            return sbEmail.ToString();
        }

        public EmailVM GetEmailByName(string emailName)
        {
            var vm = new EmailVM();
            var result = emailSettingsRepository.GetEmailByName(emailName);
            if (result != null)
            {
                vm.ID = result.ID;
                vm.Body = result.Body;
                vm.Name = result.Name;
                vm.DisplayName = result.DisplayName;
                vm.Subject = result.Subject;
                vm.IsEnabled = result.IsEnabled;
                vm.ButtonLabel = result.ButtonLabel;
            }
            return vm;            
        }
        public string GetReadyToSendEmailByName(string emailName)
        {
            var email = emailSettingsRepository.GetEmailByName(emailName);
            return email != null ? GetReadyToSendEmailByID(email.ID) : null;
        }

        public bool IsEmailEnabled(string emailName)
        {
            return emailSettingsRepository.IsEmailEnabled(emailName);            
        }
    }
}