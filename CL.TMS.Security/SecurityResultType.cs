﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Security
{
    public enum SecurityResultType
    {
        NoAccess = 0,
        ReadOnly,
        Custom,
        EditSave
    }
}
