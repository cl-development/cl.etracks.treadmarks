﻿using CL.TMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.Security.Authorization
{
    public static class ResourceActionAuthorizationHandler
    {
        public static SecurityResultType CheckUserSecurity(string resource)
        {
            if (string.IsNullOrEmpty(resource))
            {
                throw new ArgumentNullException("Arguments resource is null or empty");
            }

            var user = HttpContext.Current.User as ClaimsPrincipal;
            var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith(resource + ",")).ToList();
            if (rolePermissionsClaims.Count > 0)
            {
                var result = SecurityResultType.NoAccess;
                var rolPermissionsClaim = rolePermissionsClaims.FirstOrDefault();
                if (rolPermissionsClaim.Value.Contains(",1"))
                {
                    var currentValue = SecurityResultType.ReadOnly;
                    if (currentValue > result)
                    {
                        result = currentValue;
                    }
                }
                if (rolPermissionsClaim.Value.Contains(",2"))
                {
                    var currentValue = SecurityResultType.Custom;
                    if (currentValue > result)
                    {
                        result = currentValue;
                    }
                }
                if (rolPermissionsClaim.Value.Contains(",3"))
                {
                    var currentValue = SecurityResultType.EditSave;
                    if (currentValue > result)
                    {
                        result = currentValue;
                    }
                }
                return result;
            }
            else
            {
                if (SecurityContextHelper.IsStaff())
                {
                    return SecurityResultType.NoAccess;
                }
                else
                {
                    if (SecurityContextHelper.IsReadOnly())
                    {
                        return SecurityResultType.ReadOnly;
                    }
                    else
                    {
                        //OTSTM2-846 Users with Submit should have edit/save on participant side 
                        if (SecurityContextHelper.HasEditable || SecurityContextHelper.IsSubmit())
                        {
                            return SecurityResultType.EditSave;
                        }
                        return SecurityResultType.ReadOnly;
                    }
                }
            }
        }

        public static bool IsNoAccess
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions).ToList();
                return rolePermissionsClaims.Count == 0;
            }
        }

        public static bool CheckResourcePermission(string resource, int permissionLevel)
        {
            if (string.IsNullOrEmpty(resource))
            {
                throw new ArgumentNullException("Arguments resource or action are null or empty");
            }
            var user = HttpContext.Current.User as ClaimsPrincipal;
            if (user.HasClaim(TreadMarksConstants.RolePermissions, string.Format("{0},{1}", resource, permissionLevel)))
            {
                return true;
            }
            return false;
        }

        public static string GetResourceValue(string resource)
        {
            string claimValue = string.Empty;

            if (!string.IsNullOrEmpty(resource))
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                Claim claim = user.FindFirst(resource);
                if (claim != null)
                {
                    claimValue = claim.Value;
                }
            }
            return claimValue;
        }

        #region Need to remove
        public static bool CheckResourceAction(string resource, string permission)
        {
            if (string.IsNullOrEmpty(resource) || string.IsNullOrEmpty(permission))
            {
                throw new ArgumentNullException("Arguments resource or action are null or empty");
            }
            var user = HttpContext.Current.User as ClaimsPrincipal;
            if (user.HasClaim(TreadMarksConstants.RolePermissions, string.Format("{0},{1}", resource, permission)))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
