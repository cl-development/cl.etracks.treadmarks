﻿using CL.TMS.Security.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Security
{
    public class UserPolicy
    {
        public const string UserNameRegex = @"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$";
        public const string PasswordRegex = @"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^+=]).*$";
        [Obsolete("Replaced by AppSetting[UserPolicy.MaxLoginAttemptBeforeLock]")]
        public const int MaxLoginAttemptsBeforeLock = 5;

        [Obsolete("Replaced by AppSetting[UserPolicy.MaxLoginAttemptsBeforePasswordReset]")]
        public const int MaxLoginAttemptsBeforePasswordReset = 3;

        //For QA and Prod
#if DEBUG
        public const int NonPersistentAuthTicketExpiresInMinutes = 20000;
#else
        public const int NonPersistentAuthTicketExpiresInMinutes = 20;
#endif

        //For UAT and Succ, set it to 2 hours 
        //public const int NonPersistentAuthTicketExpiresInMinutes = 180;


        [Obsolete("Replaced by AppSetting[UserPolicy.PasswordExpiresInDays]")]
        public const int PasswordExpiresInDays = 180;

        public const int RememberUserNameExpiresInDays = 365;

        [Obsolete("Replaced by AppSetting[UserPolicy.PasswordResetTokenExpiresInMinutes]")]
        public const int PasswordResetTokenExpiresInMinutes = 168;
    }
}
