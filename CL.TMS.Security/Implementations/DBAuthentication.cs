﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository;
using CL.TMS.Security.Interfaces;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.System;

namespace CL.TMS.Security.Implementations
{
    public class DBAuthentication : IAuthentication
    {
        private readonly IAuthenticationRepository respository;

        public DBAuthentication(IAuthenticationRepository authenticationRepository)
        {
            respository = authenticationRepository;
        }
        public AuthenticationUser VerifyUser(string userName, string userPassword, bool shouldLockOut, int maxLoginAttempts)
        {
            var user = respository.FindUserByUserName(userName);
            if (user == null)
            {
                user = new AuthenticationUser { Status = SignInStatus.InvalidUserName }; //Empty authenticationUser
            }
            else
            {
                if (user.IsLocked) { user.Status = SignInStatus.LockedOut; }
                else if (user.Inactive) { user.Status = SignInStatus.Inactive; }
                else if (user.PasswordExpirationDate <= DateTime.UtcNow) { user.Status = SignInStatus.PasswordResetRequired; }
                else if (PBKDF2PasswordHasher.Compare(userPassword, user.Password))
                {
                    user.Status = SignInStatus.Success;
                    respository.SetLoginDateTimeAndResetLockHandler(user.ID);
                }
                else
                {
                    user.Status = SignInStatus.InvalidPassword;
                    if (shouldLockOut)
                    {
                        bool isLocked = respository.InvalidLoginAttemptHandler(user.ID, maxLoginAttempts);
                        if (isLocked) { user.Status = SignInStatus.LockedOut; }
                    }
                }
            }
            return user;
        }

        public ClaimsIdentity CreateIdentity(AuthenticationUser authenticationUser, string authenticationType)
        {
            var roles = respository.GetUserRoles(authenticationUser.ID);
            var claims = respository.GetUserClaims(authenticationUser.ID);
            return ClaimsIdentityFactory.Create(authenticationUser, authenticationType, roles, claims);
        }


        public string GetAfterLoginRedirectUrl(long userId)
        {
            return respository.GetAfterLoginRedirectUrl(userId);
        }

        public bool ConfirmPassword(string userLoginName, string password)
        {
            var user = respository.FindUserByUserName(userLoginName);
            if (user == null)
            {
                return false;
            }
            else
            {
                if (PBKDF2PasswordHasher.Compare(password, user.Password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
