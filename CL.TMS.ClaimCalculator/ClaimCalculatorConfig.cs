﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.IRepository.Claims;
using Microsoft.Practices.Prism.PubSubEvents;

namespace CL.TMS.ClaimCalculator
{
    public static class ClaimCalculatorConfig
    {
        public static void InitializeCalimCalculator(IEventAggregator eventAggregator)
        {
            ClaimCalculationManager.Instance.InitializeClaimCalculationManager(eventAggregator);
        }
    }
}
