﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    /// <summary>
    /// A view model for helping transportation premium calculation
    /// </summary>
    public class TransportationPremiumDelivery
    {
        public List<Tuple<int, decimal, decimal>> OnRoadData { get; set; } //Delivery group name, onroad weight, onroad percentage
        public List<Tuple<int, decimal, decimal>> OffRoadData { get; set; } //Delivery group name, offroad weight, offroad percentage

        public decimal TotalOnRoad { get; set; }
        public decimal TotalOffRoad { get; set; }
    }
}
