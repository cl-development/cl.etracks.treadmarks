﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Communication.Events;
using CL.TMS.IRepository.Claims;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using System.Transactions;
using CL.TMS.Configuration;
using CL.TMS.Common;
using CL.TMS.Repository.Claims;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.Framework.Logging;
using System.Collections.Concurrent;
using CL.TMS.Repository.System;

namespace CL.TMS.ClaimCalculator
{
    /// <summary>
    /// Singleton object to listen ClaimCalculation event
    /// </summary>
    public class ClaimCalculationManager
    {
        private static readonly Lazy<ClaimCalculationManager> lazy = new Lazy<ClaimCalculationManager>(() => new ClaimCalculationManager());
        private IEventAggregator eventAggregator;
        private List<int> claimIds;

        private ClaimCalculationManager()
        {

        }

        public static ClaimCalculationManager Instance
        {
            get { return lazy.Value; }
        }

        public void InitializeClaimCalculationManager(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<ClaimCalculationEvent>().Subscribe(StartCalculateClaim, true);
            claimIds = new List<int>();
        }

        private void StartCalculateClaim(ClaimCalculationPayload claimCalculationPayload)
        {
            if (claimIds.Contains(claimCalculationPayload.ClaimId))
            {
                //Do nothing if the claim calculation is still in processing)
                LogManager.LogWarning(string.Format("Claim {0} is in calculation", claimCalculationPayload.ClaimId));
            }
            else
            {
                claimIds.Add(claimCalculationPayload.ClaimId);
                if (claimCalculationPayload.IsSynchronous)
                {
                    var task = Task.Run(() =>
                    {
                        try
                        {
                            CalculateClaim(claimCalculationPayload.ClaimId);
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimCalculationPayload", Value = claimCalculationPayload } });
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                    });
                    task.Wait();
                }
                else
                {
                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            CalculateClaim(claimCalculationPayload.ClaimId);
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimCalculationPayload", Value = claimCalculationPayload } });
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                    });
                }
            }
        }

        private void CalculateClaim(int claimId)
        {
            //Prepare data for calculation
            var claimsRepository = new ClaimsRepository();
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetailsForCalculation(claimId, items);

            //Result tables: Claim, ClaimSummary and Claim Payments, claim payment details
            var claimSummary = new ClaimSummary();
            var claimPayments = new List<ClaimPayment>();
            var claimPaymentDetails = new List<ClaimPaymentDetail>();

            //Load invenotry opening result
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);

            //Load inventory adjustments
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);

            //Populate Claim Summary Common properties
            claimSummary.Claim = claim;
            claimSummary.UpdatedDate = DateTime.UtcNow;

            //Calculate claim payment
            switch (claim.ClaimType)
            {
                case 2:
                    {
                        //No claim summary need to update
                        claimPayments = CalculateCollectorClaimPayments(claim, claimDetails, inventoryAdjustments, claimPaymentDetails);
                        break;
                    }
                case 3:
                    {
                        PopulateHaulerClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        var configurationRepository = new ConfigurationsRepository();
                        var rateGroupRates = configurationRepository.LoadRateGroupRates(claim.ClaimPeriod.StartDate, claim.ClaimPeriod.EndDate);
                        var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
                        var deliveryVendorRateGroups = new List<VendorRateGroup>();
                        deliveryRateGroupIds.ForEach(c =>
                        {
                            deliveryVendorRateGroups.AddRange(configurationRepository.LoadVendorRateGroups((int)c));
                        });
                        var pickupRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.PickupRateGroupId).Distinct().ToList();
                        var pickupVendorRateGroups = new List<VendorRateGroup>();
                        pickupRateGroupIds.ForEach(c =>
                        {
                            pickupVendorRateGroups.AddRange(configurationRepository.LoadVendorRateGroups((int)c));
                        });
                        var vendorGroups = configurationRepository.LoadVendorGroups();

                        claimPayments = CalculateHaulerClaimPayments(claim, claimDetails, inventoryOpeningResult, inventoryAdjustments, claimPaymentDetails, rateGroupRates, deliveryVendorRateGroups, pickupVendorRateGroups, vendorGroups);
                        break;
                    }
                case 4:
                    {
                        PopulateProcessClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        var configurationRepository = new ConfigurationsRepository();
                        var rateGroupRates = configurationRepository.LoadRateGroupRates(claim.ClaimPeriod.StartDate, claim.ClaimPeriod.EndDate);
                        var deliveryRateGroupIds = rateGroupRates.Select(c => c.RateTransaction.DeliveryRateGroupId).Distinct().ToList();
                        var vendorRateGroups = new List<VendorRateGroup>();
                        deliveryRateGroupIds.ForEach(c =>
                        {
                            vendorRateGroups.AddRange(configurationRepository.LoadVendorRateGroups((int)c));
                        });

                        claimPayments = CalculateProcessClaimPayments(claim, claimDetails, claimPaymentDetails, rateGroupRates, vendorRateGroups);
                        break;
                    }
                case 5:
                    {
                        PopulateRPMClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        claimPayments = CalculateRPMClaimPayments(claim, claimDetails, claimPaymentDetails);
                        break;
                    }
            }

            //Calculate Claim
            var isClaimTaxExempt = false;
            if ((claim.ClaimType == 2) || (claim.ClaimType == 3))
            {
                isClaimTaxExempt = claimsRepository.IsTaxExempt(claimId);
            }
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            CalculateClaim(claim, claimPayments, claimPaymentAdjustments, isClaimTaxExempt);

            //Save to database
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //Fixed -480: Exclude approved and in batch claim calculation
                if (!((claim.Status == "Approved") || (claim.Status == "Receive Payment")))
                {
                    claimsRepository.UpdateClaimSummary(claimSummary);
                    claimsRepository.UpdateClaimPayments(claim.ID, claimPayments);
                    claimsRepository.UpdateClaimPaymentDetails(claim.ID, claimPaymentDetails);
                    claimsRepository.UpdateClaimForCalculation(claim);
                }
                else
                {
                    claimsRepository.UpdateClaimPaymentDetails(claim.ID, claimPaymentDetails);
                }

                transactionScope.Complete();
            }
        }

        private void PopulateProcessClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;

            var query = claimDetails
                .GroupBy(c => new { c.TransactionType, c.Direction })
                .Select(
                    c =>
                        new
                        {
                            Name = c.Key,
                            ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero)),
                            extraDOR = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)i.ExtraDORTotal), 4, MidpointRounding.AwayFromZero))
                        });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight + c.extraDOR;
                }

            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateRPMClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;
            var query = claimDetails
             .GroupBy(c => new { c.TransactionType, c.Direction })
             .Select(c => new
             {
                 Name = c.Key,
                 ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero))
             });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPSR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateHaulerClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustEligibleOverallOnRoad = 0;
            decimal adjustEligibleOverallOffRoad = 0;
            decimal adjustIneligibleOverallOnRoad = 0;
            decimal adjustIneligibleOverallOffRoad = 0;
            decimal eligibleAdjustOnRoad = 0;
            decimal eligibleAdjustOffRoad = 0;
            decimal ineligibleAdjustOnRoad = 0;
            decimal ineligibleAdjustOffRoad = 0;
            decimal outEligibleAdjustOnRoad = 0;
            decimal outEligibleAdjustOffRoad = 0;
            decimal outIneligibleAdjustOnRoad = 0;
            decimal outIneligibleAdjustOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustEligibleOverallOnRoad = c.OnRoad;
                    adjustEligibleOverallOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustIneligibleOverallOnRoad = c.OnRoad;
                    adjustIneligibleOverallOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 1)
                {
                    eligibleAdjustOnRoad = c.OnRoad;
                    eligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 1)
                {
                    ineligibleAdjustOnRoad = c.OnRoad;
                    ineligibleAdjustOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outEligibleAdjustOnRoad = c.OnRoad;
                    outEligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outIneligibleAdjustOnRoad = c.OnRoad;
                    outIneligibleAdjustOffRoad = c.OffRoad;
                }
            });
            var totalEligibleAdjustOnRoad = adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - outEligibleAdjustOnRoad;
            var totalEligibleAdjustOffRoad = adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - outEligibleAdjustOffRoad;
            var totalIneligibleAdjustOnRoad = adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - outIneligibleAdjustOnRoad;
            var totalIneligibleAdjustOffRoad = adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - outIneligibleAdjustOffRoad;

            decimal eligibleOnRoad = 0;
            decimal eligibleOffRoad = 0;
            decimal ineligibleOnRoad = 0;
            decimal ineligibleOffRoad = 0;
            decimal outEligibleOnRoad = 0;
            decimal outEligibleOffRoad = 0;
            decimal outIneligibleOnRoad = 0;
            decimal outIneligibleOffRoad = 0;
            decimal eligibleOnRoadEstimated = 0;
            decimal eligibleOffRoadEstimated = 0;
            decimal ineligibleOnRoadEstimated = 0;
            decimal ineligibleOffRoadEstimated = 0;
            decimal outEligibleOnRoadEstimated = 0;
            decimal outEligibleOffRoadEstimated = 0;
            decimal outIneligibleOnRoadEstimated = 0;
            decimal outIneligibleOffRoadEstimated = 0;

            claimDetails.ForEach(c =>
            {
                //Inbound Eligible
                if (c.Direction && (c.TransactionType == "TCR" || c.TransactionType == "DOT" || c.TransactionType == "STC"))
                {
                    eligibleOnRoad += c.OnRoad;
                    eligibleOffRoad += c.OffRoad;
                    eligibleOnRoadEstimated += c.EstimatedOnRoad;
                    eligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Inbound Ineligible
                if (c.Direction && (c.TransactionType == "UCR" || c.TransactionType == "HIT"))
                {
                    ineligibleOnRoad += c.OnRoad;
                    ineligibleOffRoad += c.OffRoad;
                    ineligibleOnRoadEstimated += c.EstimatedOnRoad;
                    ineligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Eligible
                if (!c.Direction && c.TransactionType == "PTR")
                {
                    outEligibleOnRoad += c.OnRoad;
                    outEligibleOffRoad += c.OffRoad;
                    outEligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outEligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Ineligible
                if (!c.Direction && (c.TransactionType == "RTR" || c.TransactionType == "HIT"))
                {
                    outIneligibleOnRoad += c.OnRoad;
                    outIneligibleOffRoad += c.OffRoad;
                    outIneligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outIneligibleOffRoadEstimated += c.EstimatedOffRoad;
                }
            });

            //Calculate outbound total eligible and ineligible
            var totalInboundOnroad = inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad + inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad;
            var eligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutboundOnroad = outEligibleOnRoad + outIneligibleOnRoad + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroad = Math.Round(totalOutboundOnroad * eligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad * ineligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroad == 0)
            {
                totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOnroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated;
            var eligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutboundOnroadEstimated = outEligibleOnRoadEstimated + outIneligibleOnRoadEstimated + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * eligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * ineligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroadEstimated == 0)
            {
                totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroad = inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad + inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad;
            var eligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutboundOffroad = outEligibleOffRoad + outIneligibleOffRoad + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroad = Math.Round(totalOutboundOffroad * eligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad * ineligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroad == 0)
            {
                totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated;
            var eligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutboundOffroadEstimated = outEligibleOffRoadEstimated + outIneligibleOffRoadEstimated + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * eligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * ineligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroadEstimated == 0)
            {
                totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            claimSummary.EligibleClosingOnRoad = claimSummary.EligibleOpeningOnRoad + eligibleOnRoad + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroad;
            claimSummary.EligibleClosingOffRoad = claimSummary.EligibleOpeningOffRoad + eligibleOffRoad + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroad;
            claimSummary.IneligibleClosingOnRoad = claimSummary.IneligibleOpeningOnRoad + ineligibleOnRoad + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroad;
            claimSummary.IneligibleClosingOffRoad = claimSummary.IneligibleOpeningOffRoad + ineligibleOffRoad + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroad;

            claimSummary.EligibleClosingOnRoadEstimated = claimSummary.EligibleOpeningOnRoadEstimated + eligibleOnRoadEstimated + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroadEstimated;
            claimSummary.EligibleClosingOffRoadEstimated = claimSummary.EligibleOpeningOffRoadEstimated + eligibleOffRoadEstimated + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroadEstimated;
            claimSummary.IneligibleClosingOnRoadEstimated = claimSummary.IneligibleOpeningOnRoadEstimated + ineligibleOnRoadEstimated + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroadEstimated;
            claimSummary.IneligibleClosingOffRoadEstimated = claimSummary.IneligibleOpeningOffRoadEstimated + ineligibleOffRoadEstimated + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroadEstimated;

            claimSummary.TotalClosing = claimSummary.EligibleClosingOnRoad + claimSummary.EligibleClosingOffRoad + claimSummary.IneligibleClosingOnRoad + claimSummary.IneligibleClosingOffRoad;
            claimSummary.TotalClosingEstimated = claimSummary.EligibleClosingOnRoadEstimated + claimSummary.EligibleClosingOffRoadEstimated + claimSummary.IneligibleClosingOnRoadEstimated + claimSummary.IneligibleClosingOffRoadEstimated;

        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, ClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoad = currentClaimSummary.EligibleOpeningOnRoad != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoad = currentClaimSummary.EligibleOpeningOffRoad != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = currentClaimSummary.IneligibleOpeningOnRoad != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = currentClaimSummary.IneligibleOpeningOffRoad != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = currentClaimSummary.TotalOpeningEstimated != null ? (decimal)currentClaimSummary.TotalOpeningEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = currentClaimSummary.TotalOpening != null ? (decimal)currentClaimSummary.TotalOpening : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoad = previousClaimSummary.EligibleClosingOnRoad != null ? (decimal)previousClaimSummary.EligibleClosingOnRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoad = previousClaimSummary.EligibleClosingOffRoad != null ? (decimal)previousClaimSummary.EligibleClosingOffRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = previousClaimSummary.IneligibleClosingOnRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = previousClaimSummary.IneligibleClosingOffRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoad : 0;
                    inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
                }
            }
            return inventoryOpeningSummary;
        }

        private void CalculateClaim(Claim claim, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments, bool isClaimTaxExempt)
        {
            var taxRate = 0.13m;
            if (claim.ClaimType == 2)
            {
                if (claim.Participant.IsTaxExempt)
                {
                    //No HST
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            if (claim.ClaimType == 3)
            {
                if (claim.Participant.IsTaxExempt)
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);
                    var paymentAmount = paymentAmountPositive - paymentAmountNegative;
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);
                    var noTaxAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);
                    var taxAdjustment = adjustmentAmount - noTaxAdjustmentAmount;
                    var taxAmount = Math.Round((paymentAmountPositive + taxAdjustment) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmountPositive - paymentAmountNegative + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
            }
            if (claim.ClaimType == 4)
            {
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var pitAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var dorAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var paymentAmount = ptrAmount + spsAmount + pitAmount + dorAmount;
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }

            }

            if (claim.ClaimType == 5)
            {
                var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount),  2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
            }
        }

        private List<ClaimPayment> CalculateRPMClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            //SPS MI
            var spsClaimDetails = claimDetails.Where(c => c.TransactionType == "SPSR").ToList();
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.RPMSPS,
                    ItemType = transactionItem.Item.ItemType,
                    Rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            });

            AddClaimPaymentDetailForRPM(claim, spsClaimDetails, claimPaymentDetails);
            return claimPayments;
        }

        private void AddClaimPaymentDetailForRPM(Claim claim, List<ClaimDetailViewModel> spsClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var item = transactionItem.Item;
                var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.AverageWeight), 4, MidpointRounding.AwayFromZero);
                var rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m;
                var qty = transactionItem.Quantity ?? 0;
                var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.RPMSPS);
                //Fixed RPM report issue
                claimPaymentDetail.ItemDescription = transactionItem.ItemDescription;
                claimPaymentDetail.ItemCode = transactionItem.ItemCode;
                claimPaymentDetails.Add(claimPaymentDetail);
            });
        }

        private List<ClaimPayment> CalculateProcessClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<VendorRateGroup> vendorRateGroups)
        {
            var claimPayments = new List<ClaimPayment>();
            //PTR TI Calculation
            //OTSTM2-1185
            var deliveryGroup = vendorRateGroups.FirstOrDefault(q => q.VendorId == claim.Participant.ID);
            if (deliveryGroup != null)
            {
                var deliveryGroupId = deliveryGroup.GroupId;
                var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR").ToList();
                ptrClaimDetails.ForEach(c =>
                {
                    var query = c.TransactionItems.GroupBy(i => i.Item.ItemType)
                                .Select(o => new { ItemType = o.Key, Weight = o.Sum(p => p.ActualWeight) });
                    query.ToList().ForEach(m =>
                    {
                        var ptrItemRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == m.ItemType && r.ProcessorGroupId == deliveryGroupId);
                        var ptrRate = ptrItemRate != null ? ptrItemRate.Rate : 0m;
                        var claimPayment = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(m.Weight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.PTR,
                            ItemType = m.ItemType,
                            Rate = ptrRate,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPayment);
                    });
                });
                AddClaimPaymentDetailForPTRTI(claim, deliveryGroupId, ptrClaimDetails, claimPaymentDetails, rateGroupRates);
            }


            //SPS PI
            var spsClaimDetails = claimDetails.Where(c => c.TransactionType == "SPS").ToList();
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.SPS,
                    ItemType = transactionItem.Item.ItemType,
                    Rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            });

            AddClaimPaymentDetailForSPSPI(claim, spsClaimDetails, claimPaymentDetails);

            //PIT Outbound
            var pitClaimDetails = claimDetails.Where(c => c.TransactionType == "PIT" && !c.Direction).ToList();
            pitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    var claimPayment = new ClaimPayment
                    {
                        ClaimID = claim.ID,
                        Weight = Math.Round(DataConversion.ConvertKgToTon((double)i.ActualWeight), 4, MidpointRounding.AwayFromZero),
                        PaymentType = (int)ClaimPaymentType.PITOutbound,
                        ItemType = i.Item.ItemType,
                        Rate = i.Rate != null ? (decimal)i.Rate : 0m,
                        CreatedDate = DateTime.UtcNow
                    };
                    claimPayments.Add(claimPayment);
                });
            });

            AddClaimPaymentDetailForPIT(claim, pitClaimDetails, claimPaymentDetails);

            //DOR PI
            if (deliveryGroup != null)
            {
                var deliveryGroupId = deliveryGroup.GroupId;
                var dorClaimDetails = claimDetails.Where(c => c.TransactionType == "DOR").ToList();
                dorClaimDetails.ForEach(c =>
                {
                    //When tire rims selected
                    if (c.Transaction.MaterialType == 4)
                    {
                        var ptrItemRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 1 && r.ProcessorGroupId == deliveryGroupId);
                        var ptrRate = ptrItemRate != null ? ptrItemRate.Rate : 0;
                        var onroadWeight = c.OnroadExtraDORForTireRims;
                        var claimPayment = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(onroadWeight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.DOR,
                            ItemType = 1,
                            Rate = 0 - ptrRate,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPayment);

                        var ptrItemRateOffroad = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 2 && r.ProcessorGroupId == deliveryGroupId);
                        var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.Rate : 0;
                        var offroadWeight = c.OffroadExtraDORForTireRims;
                        var claimPaymentOffroad = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(offroadWeight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.DOR,
                            ItemType = 2,
                            Rate = 0 - ptrRateOffroad,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPaymentOffroad);
                    }
                    //When ‘Used Tire Sale’ selected
                    if (c.Transaction.MaterialType == 5)
                    {
                        var query = c.TransactionItems.GroupBy(i => i.Item.ItemType)
                            .Select(o => new { ItemType = o.Key, Weight = o.Sum(p => p.ActualWeight) });
                        query.ToList().ForEach(m =>
                        {
                            var ptrItemRate = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == m.ItemType && r.ProcessorGroupId == deliveryGroupId);
                            var ptrRate = ptrItemRate != null ? ptrItemRate.Rate : 0;
                            var claimPayment = new ClaimPayment
                            {
                                ClaimID = claim.ID,
                                Weight = Math.Round(m.Weight, 4, MidpointRounding.AwayFromZero),
                                PaymentType = (int)ClaimPaymentType.DOR,
                                ItemType = m.ItemType,
                                Rate = 0 - ptrRate,
                                CreatedDate = DateTime.UtcNow
                            };
                            claimPayments.Add(claimPayment);
                        });
                    }
                });

                AddClaimPaymentDetailForDORPI(claim, deliveryGroupId, dorClaimDetails, claimPaymentDetails, rateGroupRates);
            }

            return claimPayments;
        }

        private void AddClaimPaymentDetailForDORPI(Claim claim, int deliveryGroupId, List<ClaimDetailViewModel> dorClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates)
        {
            var ptrItemRateOnroad = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 1 && r.ProcessorGroupId == deliveryGroupId);
            var ptrRateOnroad = ptrItemRateOnroad != null ? ptrItemRateOnroad.Rate : 0;
            var ptrItemRateOffroad = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 2 && r.ProcessorGroupId == deliveryGroupId);
            var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.Rate : 0;
            ptrRateOnroad = 0 - ptrRateOnroad;
            ptrRateOffroad = 0 - ptrRateOffroad;

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dorClaimDetails.ForEach(c =>
            {
                // Fixed OTSTM2-458  Trash, Steel and Fiber
                if ((c.Transaction.MaterialType == 1) || (c.Transaction.MaterialType == 2) || (c.Transaction.MaterialType == 3))
                {
                    //Add claim payment detail at transaction level with itemid=0
                    var item = new Item();
                    item.ID = 0;
                    item.ItemType = 1;
                    item.ShortName = "";
                    var dorTotal = c.ExtraDORTotal;
                    var onroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item, 0, dorTotal, dorTotal, 0, 0, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(onroadClaimPaymentDetail);
                }

                if (c.Transaction.MaterialType == 4)
                {
                    //Add claim payment detail at transaction level with itemid=0
                    var item = new Item();
                    item.ID = 0;
                    item.ItemType = 1;
                    item.ShortName = "";
                    var onroadWeight = c.OnroadExtraDORForTireRims;
                    var onroadAverageWeight = onroadWeight;
                    var amount = Math.Round(onroadWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var onroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item, ptrRateOnroad, onroadWeight, onroadAverageWeight, 0, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(onroadClaimPaymentDetail);

                    var item1 = new Item();
                    item1.ID = 0;
                    item1.ItemType = 2;
                    item1.ShortName = "";
                    var offroadWeight = c.OffroadExtraDORForTireRims;
                    var offroadAverageWeight = offroadWeight;
                    var offroadAmount = Math.Round(offroadWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var offroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item1, ptrRateOffroad, offroadWeight, offroadAverageWeight, 0, offroadAmount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(offroadClaimPaymentDetail);

                }
                //When ‘Used Tire Sale’ selected
                if (c.Transaction.MaterialType == 5)
                {
                    if (c.PLTActualWeight > 0)
                    {
                        var amount = Math.Round(c.PLTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                        var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, ptrRateOnroad, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(pltClaimPaymentDetail);
                    }
                    if (c.MTActualWeight > 0)
                    {
                        var amount = Math.Round(c.MTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                        var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, ptrRateOnroad, c.MTActualWeight, c.MTAverageWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(mtClaimPaymentDetail);
                    }
                    if (c.AGLSActualWeight > 0)
                    {
                        var amount = Math.Round(c.AGLSActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, ptrRateOffroad, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(aglsClaimPaymentDetail);
                    }
                    if (c.INDActualWeight > 0)
                    {
                        var amount = Math.Round(c.INDActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, ptrRateOffroad, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(indClaimPaymentDetail);
                    }
                    if (c.SOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.SOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, ptrRateOffroad, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(sotrClaimPaymentDetail);
                    }
                    if (c.MOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.MOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, ptrRateOffroad, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(motrClaimPaymentDetail);
                    }
                    if (c.LOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.LOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, ptrRateOffroad, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(lotrClaimPaymentDetail);
                    }
                    if (c.GOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.GOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, ptrRateOffroad, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(gotrClaimPaymentDetail);
                    }
                }

            });
        }

        private void AddClaimPaymentDetailForPIT(Claim claim, List<ClaimDetailViewModel> pitClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            pitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    var item = i.Item;
                    var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)i.ActualWeight), 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)i.AverageWeight), 4, MidpointRounding.AwayFromZero);
                    var rate = i.Rate != null ? (decimal)i.Rate : 0m;
                    var qty = i.Quantity ?? 0;
                    var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.PITOutbound);
                    claimPaymentDetails.Add(claimPaymentDetail);
                });
            });
        }
        private void AddClaimPaymentDetailForSTC(Claim claim, List<ClaimDetailViewModel> stcClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            stcClaimDetails.ForEach(c =>
            {
                var configurationRepository = new ConfigurationsRepository();
                var specialItemRates = configurationRepository.GetSTCRatesByTransactionID(c.Transaction.ID);
                decimal onRoadRate = 0m, offRoadRate = 0m;

                if (specialItemRates != null)
                {
                    onRoadRate = specialItemRates.OnRoadRate;
                    offRoadRate = specialItemRates.OffRoadRate;
                }
                c.TransactionItems.ForEach(i =>
                {
                    var item = i.Item;
                    var actualWeight = Math.Round(i.ActualWeight, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(i.AverageWeight, 4, MidpointRounding.AwayFromZero);
                    var rate = i.Item.ItemType == 1 ? onRoadRate : offRoadRate;
                    var qty = i.Quantity ?? 0;
                    var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, i.UnitType, (int)ClaimPaymentType.STCPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                });
            });
        }

        private void AddClaimPaymentDetailForSPSPI(Claim claim, List<ClaimDetailViewModel> spsClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var item = transactionItem.Item;
                var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.AverageWeight), 4, MidpointRounding.AwayFromZero);
                var rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m;
                var qty = transactionItem.Quantity ?? 0;
                var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.SPS);
                claimPaymentDetails.Add(claimPaymentDetail);
            });
        }

        private void AddClaimPaymentDetailForPTRTI(Claim claim, int deliveryGroupId, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates)
        {
            var ptrItemRateOnroad = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 1 && r.ProcessorGroupId == deliveryGroupId);
            var ptrRateOnroad = ptrItemRateOnroad != null ? ptrItemRateOnroad.Rate : 0;
            var ptrItemRateOffroad = rateGroupRates.FirstOrDefault(r => r.PaymentType == 3 && r.ItemType == 2 && r.ProcessorGroupId == deliveryGroupId);
            var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.Rate : 0;

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            ptrClaimDetails.ForEach(c =>
            {
                if (c.PLTActualWeight > 0)
                {
                    var amount = Math.Round(c.PLTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, ptrRateOnroad, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(pltClaimPaymentDetail);
                }
                if (c.MTActualWeight > 0)
                {
                    var amount = Math.Round(c.MTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, ptrRateOnroad, c.MTActualWeight, c.MTAverageWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(mtClaimPaymentDetail);
                }
                if (c.AGLSActualWeight > 0)
                {
                    var amount = Math.Round(c.AGLSActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, ptrRateOffroad, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(aglsClaimPaymentDetail);
                }
                if (c.INDActualWeight > 0)
                {
                    var amount = Math.Round(c.INDActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, ptrRateOffroad, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(indClaimPaymentDetail);
                }
                if (c.SOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.SOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, ptrRateOffroad, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(sotrClaimPaymentDetail);
                }
                if (c.MOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.MOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, ptrRateOffroad, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(motrClaimPaymentDetail);
                }
                if (c.LOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.LOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, ptrRateOffroad, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(lotrClaimPaymentDetail);
                }
                if (c.GOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.GOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, ptrRateOffroad, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(gotrClaimPaymentDetail);
                }
            });
        }

        #region Collector Claim Calculation
        private List<ClaimPayment> CalculateCollectorClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            var eligibleClaimDetails = claimDetails.Where(c => c.IsEligible).ToList();
            var eligibleClaimInventoryAdjustments = claimInventoryAdjustments.Where(c => c.IsEligible).ToList();
            //Add PLT Payment
            //Get PLT Payment Rate
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var pltItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == pltItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var pltRate = pltItemRate != null ? pltItemRate.ItemRate : 0;
            var pltItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
                pltItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * pltRate, 3, MidpointRounding.AwayFromZero);
                    var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, pltRate, c.PLTActualWeight, c.PLTAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PLT);
                    claimPaymentDetails.Add(pltClaimPaymentDetail);
                }
            });

            var pltClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = pltItemQty,
                PaymentType = (int)ClaimPaymentType.PLT,
                ItemType = 1,
                Rate = pltRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(pltClaimPayment);

            var pltItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                pltItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == pltItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (pltItemAdjustQty != 0)
            {
                var pltAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = pltItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.PLT,
                    ItemType = 1,
                    Rate = pltRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(pltAdjustClaimPayment);
            }

            //Add MT to GOTR payment
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var mtItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == mtItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var mtRate = mtItemRate != null ? mtItemRate.ItemRate : 0;
            var mtItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
                mtItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * mtRate, 3, MidpointRounding.AwayFromZero);
                    var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, mtRate, c.MTActualWeight, c.MTAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.MT);
                    claimPaymentDetails.Add(mtClaimPaymentDetail);
                }
            });

            var mtClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = mtItemQty,
                PaymentType = (int)ClaimPaymentType.MT,
                ItemType = 1,
                Rate = mtRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(mtClaimPayment);

            var mtItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                mtItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == mtItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (mtItemAdjustQty != 0)
            {
                var mtAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = mtItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.MT,
                    ItemType = 1,
                    Rate = mtRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(mtAdjustClaimPayment);
            }


            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var aglsItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == aglsItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var aglsRate = aglsItemRate != null ? aglsItemRate.ItemRate : 0;
            var aglsItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
                aglsItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * aglsRate, 3, MidpointRounding.AwayFromZero);
                    var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, aglsRate, c.AGLSActualWeight, c.AGLSAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.AGLS);
                    claimPaymentDetails.Add(aglsClaimPaymentDetail);
                }
            });


            var aglsClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = aglsItemQty,
                PaymentType = (int)ClaimPaymentType.AGLS,
                ItemType = 1,
                Rate = aglsRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(aglsClaimPayment);

            var aglsItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                aglsItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == aglsItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (aglsItemAdjustQty != 0)
            {
                var aglsAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = aglsItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.AGLS,
                    ItemType = 1,
                    Rate = aglsRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(aglsAdjustClaimPayment);
            }

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var indItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == indItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var indRate = indItemRate != null ? indItemRate.ItemRate : 0;
            var indItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
                indItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * indRate, 3, MidpointRounding.AwayFromZero);
                    var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, indRate, c.INDActualWeight, c.INDAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IND);
                    claimPaymentDetails.Add(indClaimPaymentDetail);
                }
            });


            var indClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = indItemQty,
                PaymentType = (int)ClaimPaymentType.IND,
                ItemType = 1,
                Rate = indRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(indClaimPayment);

            var indItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                indItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == indItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (indItemAdjustQty != 0)
            {
                var indAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = indItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.IND,
                    ItemType = 1,
                    Rate = indRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(indAdjustClaimPayment);
            }

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var sotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == sotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var sotrRate = sotrItemRate != null ? sotrItemRate.ItemRate : 0;
            var sotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
                sotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * sotrRate, 3, MidpointRounding.AwayFromZero);
                    var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, sotrRate, c.SOTRActualWeight, c.SOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.SOTR);
                    claimPaymentDetails.Add(sotrClaimPaymentDetail);
                }
            });


            var sotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = sotrItemQty,
                PaymentType = (int)ClaimPaymentType.SOTR,
                ItemType = 1,
                Rate = sotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(sotrClaimPayment);

            var sotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                sotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == sotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (sotrItemAdjustQty != 0)
            {
                var sotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = sotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.SOTR,
                    ItemType = 1,
                    Rate = sotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(sotrAdjustClaimPayment);
            }

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var motrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == motrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var motrRate = motrItemRate != null ? motrItemRate.ItemRate : 0;
            var motrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
                motrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * motrRate, 3, MidpointRounding.AwayFromZero);
                    var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, motrRate, c.MOTRActualWeight, c.MOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.MOTR);
                    claimPaymentDetails.Add(motrClaimPaymentDetail);
                }
            });


            var motrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = motrItemQty,
                PaymentType = (int)ClaimPaymentType.MOTR,
                ItemType = 1,
                Rate = motrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(motrClaimPayment);

            var motrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                motrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == motrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (motrItemAdjustQty != 0)
            {
                var motrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = motrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.MOTR,
                    ItemType = 1,
                    Rate = motrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(motrAdjustClaimPayment);
            }

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var lotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == lotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var lotrRate = lotrItemRate != null ? lotrItemRate.ItemRate : 0;
            var lotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
                lotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * lotrRate, 3, MidpointRounding.AwayFromZero);
                    var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, lotrRate, c.LOTRActualWeight, c.LOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.LOTR);
                    claimPaymentDetails.Add(lotrClaimPaymentDetail);
                }
            });


            var lotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = lotrItemQty,
                PaymentType = (int)ClaimPaymentType.LOTR,
                ItemType = 1,
                Rate = lotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(lotrClaimPayment);

            var lotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                lotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == lotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (lotrItemAdjustQty != 0)
            {
                var lotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = lotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.LOTR,
                    ItemType = 1,
                    Rate = lotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(lotrAdjustClaimPayment);
            }

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            var gotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == gotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var gotrRate = gotrItemRate != null ? gotrItemRate.ItemRate : 0;
            var gotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
                gotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * gotrRate, 3, MidpointRounding.AwayFromZero);
                    var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, gotrRate, c.GOTRActualWeight, c.GOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.GOTR);
                    claimPaymentDetails.Add(gotrClaimPaymentDetail);
                }
            });


            var gotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = gotrItemQty,
                PaymentType = (int)ClaimPaymentType.GOTR,
                ItemType = 1,
                Rate = gotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(gotrClaimPayment);

            var gotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                gotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == gotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (gotrItemAdjustQty != 0)
            {
                var gotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = gotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.GOTR,
                    ItemType = 1,
                    Rate = gotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(gotrAdjustClaimPayment);
            }

            return claimPayments;
        }

        private ClaimPaymentDetail AddClaimPaymentDetail(Claim claim, ClaimDetailViewModel c, Item item, decimal rate, decimal actualWeight, decimal averageWeight, int qty, decimal amount, int unitType, int paymentType)
        {
            var claimPaymentDetail = new ClaimPaymentDetail();
            claimPaymentDetail.ClaimId = claim.ID;
            claimPaymentDetail.TransactionId = c.Transaction.ID;
            claimPaymentDetail.ItemId = item.ID;
            claimPaymentDetail.ItemType = item.ItemType;
            claimPaymentDetail.ItemName = item.ShortName;
            claimPaymentDetail.Quantity = qty;
            claimPaymentDetail.AverageWeight = averageWeight;
            claimPaymentDetail.ActualWeight = actualWeight;
            claimPaymentDetail.Rate = rate;
            claimPaymentDetail.Amount = amount;
            claimPaymentDetail.UnitType = unitType;
            claimPaymentDetail.PaymentType = paymentType;
            claimPaymentDetail.TransactionTypeCode = c.Transaction.TransactionTypeCode;
            claimPaymentDetail.ClaimPeriodId = claim.ClaimPeriodId;
            claimPaymentDetail.PeriodName = claim.ClaimPeriod.ShortName;
            claimPaymentDetail.PeriodStartDate = claim.ClaimPeriod.StartDate;
            claimPaymentDetail.PeriodEndDate = claim.ClaimPeriod.EndDate;
            claimPaymentDetail.VendorId = claim.ParticipantId;
            claimPaymentDetail.VendorType = claim.Participant.VendorType;
            claimPaymentDetail.BusinessName = claim.Participant.BusinessName;
            claimPaymentDetail.RegistrationNumber = claim.Participant.Number;
            claimPaymentDetail.CreatedDate = DateTime.UtcNow;
            return claimPaymentDetail;
        }
        #endregion

        #region Hauler Claim Calculation
        private List<ClaimPayment> CalculateHaulerClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, InventoryOpeningSummary inventoryOpeningResult, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<VendorRateGroup> deliveryVendorRateGroups, List<VendorRateGroup> pickupVendorRateGroups, List<Group> vendorGroups)
        {
            var claimPayments = new List<ClaimPayment>();
            var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR").ToList();
            if (ptrClaimDetails.Count > 0)
            {
                var claimPeriodDate = claim.ClaimPeriod.StartDate;

                //Transportation Premium Calculation
                TransportationPremiumCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails, rateGroupRates, deliveryVendorRateGroups, pickupVendorRateGroups, vendorGroups);

                //DOT Premium Calculation
                DOTPremiumCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails, rateGroupRates, deliveryVendorRateGroups, pickupVendorRateGroups, vendorGroups);

                //TI Clawback Calculation
                TIClawbackCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, inventoryOpeningResult, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails, rateGroupRates, deliveryVendorRateGroups, vendorGroups);
            }

            //RTR ClaimPaymentDetail (weight only)
            var rtrClaimDetails = claimDetails.Where(c => c.TransactionType == "RTR").ToList();

            if (rtrClaimDetails.Count > 0)
            {
                AddClaimPaymentDetailForRTRTransactions(claim, rtrClaimDetails, claimPaymentDetails);
            }

            //HIT ClaimPaymentDetail (weight only)
            var hitClaimDetails = claimDetails.Where(c => c.TransactionType == "HIT" && !c.Direction).ToList();
            if (hitClaimDetails.Count > 0)
            {
                AddClaimPaymentDetailForHITTransactions(claim, hitClaimDetails, claimPaymentDetails);
            }

            //STC
            var stcClaimDetails = claimDetails.Where(c => (c.TransactionType == "STC") && c.Direction).ToList();
            stcClaimDetails.ForEach(c =>
            {
                var configurationRepository = new ConfigurationsRepository();
                var specialItemRates = configurationRepository.GetSTCRatesByTransactionID(c.Transaction.ID);
                decimal onRoadRate = 0m, offRoadRate = 0m;

                if (specialItemRates != null)
                {
                    onRoadRate = specialItemRates.OnRoadRate;
                    offRoadRate = specialItemRates.OffRoadRate;
                }
                c.TransactionItems.ForEach(i =>
                {
                    var claimPayment = new ClaimPayment
                    {
                        ClaimID = claim.ID,
                        Weight = Math.Round(i.ActualWeight, 4, MidpointRounding.AwayFromZero),
                        PaymentType = (int)ClaimPaymentType.STCPremium,
                        ItemType = i.Item.ItemType,
                        Rate = i.Item.ItemType == 1 ? onRoadRate : offRoadRate,
                        CreatedDate = DateTime.UtcNow
                    };
                    claimPayments.Add(claimPayment);
                });
            });

            AddClaimPaymentDetailForSTC(claim, stcClaimDetails, claimPaymentDetails);

            return claimPayments;
        }

        private void AddClaimPaymentDetailForHITTransactions(Claim claim, List<ClaimDetailViewModel> hitClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            hitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    if (i.ActualWeight > 0)
                    {
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, i.Item, 0, i.ActualWeight, i.AverageWeight, i.Quantity ?? 0, 0, TreadMarksConstants.Kg, 0);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                });
            });
        }

        private void AddClaimPaymentDetailForRTRTransactions(Claim claim, List<ClaimDetailViewModel> rtrClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            rtrClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    if (i.ActualWeight > 0)
                    {
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, i.Item, 0, i.ActualWeight, i.AverageWeight, i.Quantity ?? 0, 0, TreadMarksConstants.Kg, 0);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                });
            });
        }

        private void TIClawbackCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, InventoryOpeningSummary inventoryOpeningResult, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<VendorRateGroup> deliveryVendorRateGroups, List<Group> vendorGroups)
        {
            var claimId = claim.ID;
            //1. Get delivery weight percentage for onroad nad offroad based on delivery zone
            var clawbackDelivery = GetClawbackDelivery(ptrClaimDetails, claimInventoryAdjustments, deliveryVendorRateGroups);

            //2. Get ineligible transactions ---Fix TI calculation with using estimated weight for percentage 
            var ineligibleClaimDetails = claimDetails.Where(c => c.Direction && (c.TransactionType == "UCR" || c.TransactionType == "HIT")).ToList();
            var totalIneligibleOnRoad = ineligibleClaimDetails.Sum(c => c.EstimatedOnRoad);
            var totalIneligibleOffRoad = ineligibleClaimDetails.Sum(c => c.EstimatedOffRoad);

            var inboundOnroadAdjust = claimInventoryAdjustments.Where(c => c.Direction == 1).Sum(c => c.AdjustmentWeightOnroad);
            var inboundOffroadAdjust = claimInventoryAdjustments.Where(c => c.Direction == 1).Sum(c => c.AdjustmentWeightOffroad);

            var inboundOnroadAdjustIneligible = claimInventoryAdjustments.Where(c => c.Direction == 1 && !c.IsEligible).Sum(c => c.AdjustmentWeightOnroad);
            var inboundOffroadAdjustIneligible = claimInventoryAdjustments.Where(c => c.Direction == 1 && !c.IsEligible).Sum(c => c.AdjustmentWeightOffroad);

            var ineligibleOnRoad = totalIneligibleOnRoad + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + inboundOnroadAdjustIneligible;
            var ineligibleOffRoad = totalIneligibleOffRoad + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + inboundOffroadAdjustIneligible;

            var inboundTotalOnRoad = claimDetails.Where(c => c.Direction).Sum(c => c.EstimatedOnRoad);
            var inboundTotalOffRoad = claimDetails.Where(c => c.Direction).Sum(c => c.EstimatedOffRoad);
            var totalOnRoad = inboundTotalOnRoad + inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + inboundOnroadAdjust;
            var totalOffRoad = inboundTotalOffRoad + inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + inboundOffroadAdjust;

            decimal onroadWeightPercentage = 0m;
            if (totalOnRoad > 0)
                onroadWeightPercentage = ineligibleOnRoad / totalOnRoad;
            decimal offroadWeightPercentage = 0m;
            if (totalOffRoad > 0)
                offroadWeightPercentage = ineligibleOffRoad / totalOffRoad;

            AddClawbackClaimPayment(claimId, clawbackDelivery, onroadWeightPercentage, offroadWeightPercentage, rateGroupRates, claimPayments, vendorGroups);

            //Add claim payment detail
            AddClawbackClaimPaymentDetails(claim, ineligibleClaimDetails, clawbackDelivery, totalOnRoad, totalOffRoad, claimPaymentDetails, rateGroupRates, vendorGroups);

        }

        private void AddClawbackClaimPaymentDetails(Claim claim, List<ClaimDetailViewModel> ineligibleClaimDetails, ClawbackDelivery clawbackDelivery, decimal totalOnRoad, decimal totalOffRoad, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            ineligibleClaimDetails.ForEach(t =>
            {
                clawbackDelivery.OnRoadData.ForEach(c =>
                {
                    if (c.Item2 > 0)
                    {
                        var rate = rateGroupRates.FirstOrDefault(r => r.ProcessorGroupId == c.Item1 && r.ItemType == 1 && r.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment);
                        var rateValue = (rate != null) ? rate.Rate : 0m;
                        AddTIOnroadClaimPaymentDetails(claim, c.Item3, c.Item2, totalOnRoad, rateValue, claimPaymentDetails, t, pltItem, mtItem);
                    }
                });

                clawbackDelivery.OffRoadData.ForEach(c =>
                {
                    if (c.Item2 > 0)
                    {
                        var rate = rateGroupRates.FirstOrDefault(r => r.ProcessorGroupId == c.Item1 && r.ItemType == 2 && r.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment);
                        var rateValue = (rate != null) ? rate.Rate : 0m;
                        ADDTIOffroadClaimPaymentDetails(claim, c.Item3, c.Item2, totalOffRoad, rateValue, claimPaymentDetails, t, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                });
            });
        }

        private void ADDTIOffroadClaimPaymentDetails(Claim claim, decimal offRoadEstimatedWeight, decimal offRoadActualWeight, decimal totalOffRoad, decimal zoneOffRoadRate, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            if (c.AGLSAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.AGLSAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, zoneOffRoadRate, weight, estimatedWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.INDAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.INDAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, zoneOffRoadRate, weight, estimatedWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.SOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.SOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, zoneOffRoadRate, weight, estimatedWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.MOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, zoneOffRoadRate, weight, estimatedWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.LOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.LOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, zoneOffRoadRate, weight, estimatedWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.GOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.GOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, zoneOffRoadRate, weight, estimatedWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddTIOnroadClaimPaymentDetails(Claim claim, decimal onroadEstimatedWeight, decimal onroadActualWeight, decimal totalOnRoad, decimal zoneOnRoadRate, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item pltItem, Item mtItem)
        {
            if (c.PLTAverageWeight > 0)
            {
                decimal onroadWeightPercentage = 0m;
                if (totalOnRoad > 0)
                    onroadWeightPercentage = c.PLTAverageWeight / totalOnRoad;
                var weight = Math.Round(onroadActualWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOnRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(onroadEstimatedWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, zoneOnRoadRate, weight, estimatedWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MTAverageWeight > 0)
            {
                decimal onroadWeightPercentage = 0m;
                if (totalOnRoad > 0)
                    onroadWeightPercentage = c.MTAverageWeight / totalOnRoad;
                var weight = Math.Round(onroadActualWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOnRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(onroadEstimatedWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, zoneOnRoadRate, weight, estimatedWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddClawbackClaimPayment(int claimId, ClawbackDelivery clawbackDelivery, decimal onroadWeightPercentage, decimal offroadWeightPercentage, List<RateGroupRate> rateGroupRates, List<ClaimPayment> claimPayments, List<Group> vendorGroups)
        {
            clawbackDelivery.OnRoadData.ForEach(c =>
            {
                var rate = rateGroupRates.FirstOrDefault(r => r.ProcessorGroupId == c.Item1 && r.ItemType == 1 && r.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment);
                var rateValue = (rate != null) ? rate.Rate : 0m;
                var deliveryGroup = vendorGroups.FirstOrDefault(q => q.Id == c.Item1);
                if (deliveryGroup != null)
                {
                    var weight = Math.Round(c.Item2 * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                    var claimPayment = new ClaimPayment
                    {
                        ClaimID = claimId,
                        Weight = weight,
                        PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                        ItemType = 1,
                        Rate = rateValue,
                        SourceZone = string.Empty,
                        DeliveryZone = deliveryGroup.GroupName,
                        CreatedDate = DateTime.UtcNow
                    };
                    claimPayments.Add(claimPayment);
                }
            });

            clawbackDelivery.OffRoadData.ForEach(c =>
            {
                var rate = rateGroupRates.FirstOrDefault(r => r.ProcessorGroupId == c.Item1 && r.ItemType == 2 && r.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment);
                var rateValue = (rate != null) ? rate.Rate : 0m;
                var deliveryGroup = vendorGroups.FirstOrDefault(q => q.Id == c.Item1);
                if (deliveryGroup != null)
                {
                    var weight = Math.Round(c.Item2 * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                    var claimPayment = new ClaimPayment
                    {
                        ClaimID = claimId,
                        Weight = weight,
                        PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                        ItemType = 2,
                        Rate = rateValue,
                        SourceZone = string.Empty,
                        DeliveryZone = deliveryGroup.GroupName,
                        CreatedDate = DateTime.UtcNow
                    };
                    claimPayments.Add(claimPayment);
                }
            });
        }

        private ClawbackDelivery GetClawbackDelivery(List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<VendorRateGroup> vendorRateGroups)
        {
            var result = new ClawbackDelivery();
            var query = ptrClaimDetails
            .Where(c => c.Transaction.IncomingId != null)
            .GroupBy(c => new { c.Transaction.IncomingId })
            .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.OnRoad), OffRoad = c.Sum(i => i.OffRoad), AverageOnRoad = c.Sum(i => i.EstimatedOnRoad), AverageOffRoad = c.Sum(i => i.EstimatedOffRoad) });

            var secondQuery = query.Select(c => new
            {
                DeliveryGroupId = vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId) != null ? vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId).GroupId : 0,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad,
                AverageOnRoad = c.AverageOnRoad,
                AverageOffRoad = c.AverageOffRoad
            }).GroupBy(c => new { c.DeliveryGroupId })
              .Select(c => new
              {
                  Name = c.Key,
                  OnRoad = c.Sum(i => i.OnRoad),
                  OffRoad = c.Sum(i => i.OffRoad),
                  AverageOnRoad = c.Sum(i => i.AverageOnRoad),
                  AverageOffRoad = c.Sum(i => i.AverageOffRoad)
              });

            result.TotalOnRoadAverageWeight = secondQuery.Sum(c => c.AverageOnRoad);
            result.TotalOnRoadActualWeight = secondQuery.Sum(c => c.OnRoad);
            result.TotalOffRoadActualWeight = secondQuery.Sum(c => c.OffRoad);
            result.TotalOffRoadAverageWeight = secondQuery.Sum(c => c.AverageOffRoad);

            //Add inventory adjustment
            var outboundIneligibleOnroadAdjust = claimInventoryAdjustments.Where(c => !c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOnroad);
            var outboundIneligibleOffroadAdjust = claimInventoryAdjustments.Where(c => !c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);
            result.TotalOnRoadActualWeight = result.TotalOnRoadActualWeight + outboundIneligibleOnroadAdjust;
            result.TotalOnRoadActualWeight = result.TotalOffRoadActualWeight + outboundIneligibleOffroadAdjust;
            result.TotalOnRoadAverageWeight = result.TotalOnRoadAverageWeight + outboundIneligibleOnroadAdjust;
            result.TotalOffRoadAverageWeight = result.TotalOffRoadAverageWeight + outboundIneligibleOffroadAdjust;

            result.OnRoadData = new List<Tuple<int, decimal, decimal>>();
            result.OffRoadData = new List<Tuple<int, decimal, decimal>>();

            secondQuery.ToList().ForEach(c =>
            {
                result.OnRoadData.Add(new Tuple<int, decimal, decimal>(c.Name.DeliveryGroupId, c.OnRoad, c.AverageOnRoad));
                result.OffRoadData.Add(new Tuple<int, decimal, decimal>(c.Name.DeliveryGroupId, c.OffRoad, c.AverageOffRoad));
            });

            return result;
        }

        private void DOTPremiumCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<VendorRateGroup> deliveryVendorRateGroups, List<VendorRateGroup> pickupVendorRateGroups, List<Group> vendorGroups)
        {
            var claimId = claim.ID;
            //1. Get delivery weight percentage for offroad based on delivery zone
            var dotPremiumDelivery = GetDOTPremiumDelivery(ptrClaimDetails, claimInventoryAdjustments, deliveryVendorRateGroups);

            //2. Get DOT transactions
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == "DOT").ToList();

            var query = dotClaimDetails.GroupBy(c => new { c.Transaction.OutgoingId })
                 .Select(c => new
                 {
                     Name = c.Key,
                     OffRoad = c.Sum(i => i.OffRoad)
                 });

            var secondQuery = query.Select(c => new
            {
                PickupGroupId = pickupVendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.OutgoingId) != null ? pickupVendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.OutgoingId).GroupId : 0,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.PickupGroupId })
             .Select(c => new DOTPremiumPickup
             {
                 PickupGroupId = c.Key.PickupGroupId,
                 OffRoad = c.Sum(i => i.OffRoad)
             });

            var pickupGroupResult = secondQuery.ToList();

            var tempClaimPayments = new List<ClaimPayment>();
            pickupGroupResult.ForEach(c =>
            {
                //Offroad payment
                dotPremiumDelivery.OffRoadData.ForEach(d =>
                {
                    var rate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == c.PickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.DOTPremium && r.ItemType == 2);
                    var weight = c.OffRoad * d.Item3;
                    var pickupGroup = vendorGroups.FirstOrDefault(g => g.Id == c.PickupGroupId);
                    var deliveryGroup = vendorGroups.FirstOrDefault(q => q.Id == d.Item1);
                    var rateValue = rate != null ? rate.Rate : 0m;
                    if (pickupGroup != null && deliveryGroup != null)
                    {
                        AddDOTPremiumClaimPayment(claimId, weight, rateValue, 2, pickupGroup.GroupName, deliveryGroup.GroupName, tempClaimPayments);
                    }
                });
            });

            //Fixed DOT Distributions Rounding issues
            var totalOffRoad = pickupGroupResult.Sum(c => c.OffRoad);
            var distributedWeight = tempClaimPayments.Sum(c => c.Weight);
            var difference = (distributedWeight - totalOffRoad);
            var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.0001M);
            if (numberOfDifferenceDistribution < tempClaimPayments.Count)
            {
                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                {
                    tempClaimPayments[i].Weight -= (difference / numberOfDifferenceDistribution);
                }
            }
            claimPayments.AddRange(tempClaimPayments);

            //Adding claim payment details
            AddDOTPremiumClaimPaymentDetails(claim, dotClaimDetails, dotPremiumDelivery, claimPaymentDetails, rateGroupRates, vendorGroups);
        }

        private void AddDOTPremiumClaimPaymentDetails(Claim claim, List<ClaimDetailViewModel> dotClaimDetails, DOTPremiumDelivery dotPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            dotClaimDetails.ForEach(c =>
            {
                var pickupGroup = vendorGroups.FirstOrDefault(g => g.Id == c.Transaction.OutgoingId);
                if (pickupGroup != null)
                {
                    AddDOTPickupGroupClaimPaymentDetails(claim, rateGroupRates, pickupGroup.Id, dotPremiumDelivery, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
            });
        }

        private void AddDOTPremiumClaimPayment(int claimId, decimal weight, decimal rate, int itemType, string pickupGroup, string deliveryGroup, List<ClaimPayment> claimPayments)
        {
            var claimPayment = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = weight,
                PaymentType = (int)ClaimPaymentType.DOTPremium,
                ItemType = itemType,
                Rate = rate,
                SourceZone = pickupGroup,
                DeliveryZone = deliveryGroup,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment);
        }
        private void AddDOTPickupGroupClaimPaymentDetails(Claim claim, List<RateGroupRate> rateGroupRates, int pickupGroupId, DOTPremiumDelivery dotPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            dotPremiumDelivery.OffRoadData.ForEach(d =>
            {
                if (d.Item2 > 0)
                {
                    var groupRate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == pickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.DOTPremium && r.ItemType == 2);
                    var rate = groupRate != null ? groupRate.Rate : 0m;
                    if (c.AGLSActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.AGLSActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.AGLSAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.AGLSQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.INDActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.INDActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.INDAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.INDQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.SOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.SOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.SOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.SOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.MOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.MOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.MOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.MOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.LOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.LOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.LOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.LOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.GOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.GOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.GOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.GOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                }
            });
        }


        private void AddDOTPremiumClaimPayment(string sourceZone, int claimId, decimal offRoad, decimal rate, List<ClaimPayment> claimPayments)
        {
            var claimPayment = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = Math.Round(offRoad, 4, MidpointRounding.AwayFromZero),
                PaymentType = (int)ClaimPaymentType.DOTPremium,
                ItemType = 2,
                Rate = rate,
                SourceZone = sourceZone,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment);
        }

        private void TransportationPremiumCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<VendorRateGroup> deliveryVendorRateGroups, List<VendorRateGroup> pickupVendorRateGroups, List<Group> vendorGroups)
        {
            var claimId = claim.ID;

            //1. Get delivery weight percentage for onroad and offroad based on delivery group
            var transportationPremiumDelivery = GetTransportationPremiumDelivery(ptrClaimDetails, claimInventoryAdjustments, deliveryVendorRateGroups);

            //2. Get inbound transactions
            var inboundClaimDetails = claimDetails.Where(c => (c.TransactionType == "TCR" || c.TransactionType == "DOT") && c.Direction && c.Transaction.OutgoingId != null).ToList();
            var query = inboundClaimDetails.GroupBy(c => new { c.Transaction.OutgoingId })
                .Select(c => new
                {
                    Name = c.Key,
                    OnRoad = c.Sum(i => i.OnRoad),
                    OffRoad = c.Sum(i => i.OffRoad)
                });

            var secondQuery = query.Select(c => new
            {
                PickupGroupId = pickupVendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.OutgoingId) != null ? pickupVendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.OutgoingId).GroupId : 0,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.PickupGroupId })
             .Select(c => new TransportationPremiumPickup
             {
                 PickupGroupId = c.Key.PickupGroupId,
                 OnRoad = c.Sum(i => i.OnRoad),
                 OffRoad = c.Sum(i => i.OffRoad)
             });

            var pickupGroupResult = secondQuery.ToList();
            pickupGroupResult.ForEach(c =>
            {
                //Calculating onroad payment
                transportationPremiumDelivery.OnRoadData.ForEach(d =>
                {
                    var rate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == c.PickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.NorthernPremium && r.ItemType == 1);
                    var weight = c.OnRoad * d.Item3;
                    var pickupGroup = vendorGroups.FirstOrDefault(g => g.Id == c.PickupGroupId);
                    var deliveryGroup = vendorGroups.FirstOrDefault(q => q.Id == d.Item1);
                    var rateValue = rate != null ? rate.Rate : 0m;
                    if (pickupGroup != null && deliveryGroup != null)
                    {
                        AddTransportationPremiumClaimPayment(pickupGroup.GroupName, deliveryGroup.GroupName, 1, rateValue, weight, claimId, claimPayments);
                    }
                });

                //Offroad payment
                transportationPremiumDelivery.OffRoadData.ForEach(d =>
                {
                    var rate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == c.PickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.NorthernPremium && r.ItemType == 2);
                    var weight = c.OffRoad * d.Item3;
                    var pickupGroup = vendorGroups.FirstOrDefault(g => g.Id == c.PickupGroupId);
                    var deliveryGroup = vendorGroups.FirstOrDefault(q => q.Id == d.Item1);
                    var rateValue = rate != null ? rate.Rate : 0m;
                    if (pickupGroup != null && deliveryGroup != null)
                    {
                        AddTransportationPremiumClaimPayment(pickupGroup.GroupName, deliveryGroup.GroupName, 2, rateValue, weight, claimId, claimPayments);
                    }
                });
            });

            //4. Add claim payment details
            AddTransaportationPremiumClaimPaymentDetais(claim, inboundClaimDetails, transportationPremiumDelivery, claimPaymentDetails, rateGroupRates, vendorGroups);
        }

        private void AddTransaportationPremiumClaimPaymentDetais(Claim claim, List<ClaimDetailViewModel> inboundClaimDetails, TransportationPremiumDelivery transportationPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            inboundClaimDetails.ForEach(c =>
            {
                var pickupGroup = vendorGroups.FirstOrDefault(g => g.Id == c.Transaction.OutgoingId);
                if (pickupGroup != null)
                {
                    AddPickupGroupClaimPaymentDetails(claim, rateGroupRates, pickupGroup.Id, transportationPremiumDelivery, claimPaymentDetails, c, pltItem, mtItem, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
            });
        }

        private void AddPickupGroupClaimPaymentDetails(Claim claim, List<RateGroupRate> rateGroupRates, int pickupGroupId, TransportationPremiumDelivery transportationPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item pltItem, Item mtItem, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            transportationPremiumDelivery.OnRoadData.ForEach(d =>
            {
                if (d.Item2 > 0)
                {
                    var groupRate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == pickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.NorthernPremium && r.ItemType == 1);
                    var rate = groupRate != null ? groupRate.Rate : 0m;
                    if (c.PLTActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.PLTActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.PLTAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.PLTQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.MTActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.MTActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.MTAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.MTQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.AGLSActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.AGLSActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.AGLSAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.AGLSQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.INDActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.INDActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.INDAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.INDQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.SOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.SOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.SOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.SOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.MOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.MOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.MOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.MOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.LOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.LOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.LOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.LOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.GOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.GOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.GOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.GOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                }
            });

            transportationPremiumDelivery.OffRoadData.ForEach(d =>
            {
                if (d.Item2 > 0)
                {
                    var groupRate = rateGroupRates.FirstOrDefault(r => r.CollectorGroupId == pickupGroupId && r.ProcessorGroupId == d.Item1 && r.PaymentType == (int)ClaimPaymentType.NorthernPremium && r.ItemType == 2);
                    var rate = groupRate != null ? groupRate.Rate : 0m;
                    if (c.PLTActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.PLTActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.PLTAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.PLTQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.MTActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.MTActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.MTAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.MTQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.AGLSActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.AGLSActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.AGLSAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.AGLSQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.INDActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.INDActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.INDAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.INDQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.SOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.SOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.SOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.SOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.MOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.MOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.MOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.MOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.LOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.LOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.LOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.LOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                    if (c.GOTRActualWeight > 0)
                    {
                        var actualWeight = Math.Round(c.GOTRActualWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var averageWeight = Math.Round(c.GOTRAverageWeight * d.Item3, 4, MidpointRounding.AwayFromZero);
                        var qty = (int)Math.Round(c.GOTRQty * d.Item3, 0, MidpointRounding.AwayFromZero);
                        var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                }
            });
        }

        private void AddTransportationPremiumClaimPayment(string pickupGroup, string deliveryGroup, int itemType, decimal rate, decimal weight, int claimId, List<ClaimPayment> claimPayments)
        {
            var claimPayment = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = weight,
                PaymentType = (int)ClaimPaymentType.NorthernPremium,
                ItemType = itemType,
                Rate = rate,
                SourceZone = pickupGroup,
                DeliveryZone = deliveryGroup,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment);
        }
        private TransportationPremiumDelivery GetTransportationPremiumDelivery(List<ClaimDetailViewModel> ptrClaimDetials, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<VendorRateGroup> vendorRateGroups)
        {
            var result = new TransportationPremiumDelivery();
            var query = ptrClaimDetials
                .Where(c => c.Transaction.IncomingId != null)
                .GroupBy(c => new { c.Transaction.IncomingId })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.OnRoad), OffRoad = c.Sum(i => i.OffRoad) });

            var secondQuery = query.Select(c => new
            {
                DeliveryGroupId = vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId) != null ? vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId).GroupId : 0,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.DeliveryGroupId })
            .Select(c => new
            {
                Name = c.Key,
                OnRoad = c.Sum(i => i.OnRoad),
                OffRoad = c.Sum(i => i.OffRoad)
            });

            result.TotalOnRoad = ptrClaimDetials.Sum(c => c.OnRoad);
            result.TotalOffRoad = ptrClaimDetials.Sum(c => c.OffRoad);
            //Add inventory adjustment
            var outboundEligibleOnroadAdjust = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOnroad);
            var outboundEligibleOffroadAdjust = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);
            result.TotalOnRoad = result.TotalOnRoad + outboundEligibleOnroadAdjust;
            result.TotalOffRoad = result.TotalOffRoad + outboundEligibleOffroadAdjust;

            //Calculating percentage
            result.OnRoadData = new List<Tuple<int, decimal, decimal>>();
            result.OffRoadData = new List<Tuple<int, decimal, decimal>>();
            secondQuery.ToList().ForEach(c =>
            {
                var onRoadPercentage = result.TotalOnRoad == 0 ? 0 : c.OnRoad / result.TotalOnRoad;
                var offRoadPercentage = result.TotalOffRoad == 0 ? 0 : c.OffRoad / result.TotalOffRoad;
                result.OnRoadData.Add(new Tuple<int, decimal, decimal>(c.Name.DeliveryGroupId, c.OnRoad, onRoadPercentage));
                result.OffRoadData.Add(new Tuple<int, decimal, decimal>(c.Name.DeliveryGroupId, c.OffRoad, offRoadPercentage));
            });
            return result;
        }

        private DOTPremiumDelivery GetDOTPremiumDelivery(List<ClaimDetailViewModel> ptrClaimDetials, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<VendorRateGroup> vendorRateGroups)
        {
            var result = new DOTPremiumDelivery();

            var query = ptrClaimDetials
            .Where(c => c.Transaction.IncomingId != null)
            .GroupBy(c => new { c.Transaction.IncomingId })
            .Select(c => new { Name = c.Key, OffRoad = c.Sum(i => i.OffRoad) });

            var secondQuery = query.Select(c => new
            {
                DeliveryGroupId = vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId) != null ? vendorRateGroups.FirstOrDefault(q => q.VendorId == c.Name.IncomingId).GroupId : 0,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.DeliveryGroupId })
              .Select(c => new
              {
                  Name = c.Key,
                  OffRoad = c.Sum(i => i.OffRoad)
              });

            result.TotalOffRoad = ptrClaimDetials.Sum(c => c.OffRoad);
            //Inventory adjustment
            var eligibleOutboundOffroad = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);
            result.TotalOffRoad = result.TotalOffRoad + eligibleOutboundOffroad;

            result.OffRoadData = new List<Tuple<int, decimal, decimal>>();
            secondQuery.ToList().ForEach(c =>
            {
                var offRoadPercentage = result.TotalOffRoad == 0 ? 0 : c.OffRoad / result.TotalOffRoad;
                result.OffRoadData.Add(new Tuple<int, decimal, decimal>(c.Name.DeliveryGroupId, c.OffRoad, offRoadPercentage));
            });
            return result;
        }

        #endregion
    }
}
