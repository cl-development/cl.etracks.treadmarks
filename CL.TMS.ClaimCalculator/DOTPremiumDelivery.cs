﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    public class DOTPremiumDelivery
    {
        public List<Tuple<int, decimal, decimal>> OffRoadData { get; set; } //Delivery group name, offroad weight, offroad percentage

        public decimal TotalOffRoad { get; set; }
    }
}
