﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    public class ClawbackDelivery
    {
        public List<Tuple<int, decimal, decimal>> OnRoadData { get; set; } // Delivery group id, actualweight, avarageweight
        public List<Tuple<int, decimal, decimal>> OffRoadData { get; set; } 

        public decimal TotalOnRoadActualWeight { get; set; }
        public decimal TotalOffRoadActualWeight { get; set; }
        public decimal TotalOnRoadAverageWeight { get; set; }
        public decimal TotalOffRoadAverageWeight { get; set; }
    }
}
