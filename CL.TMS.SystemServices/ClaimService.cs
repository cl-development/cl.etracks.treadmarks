﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using CL.TMS.IRepository.Claims;
using CL.TMS.Common.Enum;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.Framework.Common;

namespace CL.TMS.SystemServices
{
    public class ClaimService : IClaimService
    {
        private readonly ClaimsBO claimsBO;

        public ClaimService(IClaimsRepository claimsRepository, IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository, IGpRepository gpRepository, ITransactionRepository transactionRepository)
        {
            this.claimsBO = new ClaimsBO(claimsRepository, userRepository, vendorRepository, eventAggregator, messageRepository, gpRepository, transactionRepository);
        }

        public PaginationDTO<StaffAllClaimsModel, Guid> LoadAllClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                var result = claimsBO.LoadAllClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<StaffAllClaimsModel> GetExportDetails(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return claimsBO.GetExportDetails(searchText, orderBy, sortDirection, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public void UpdateAssignToUserIdForClaim(int? userId, int claimId)
        {
            try
            {
                claimsBO.UpdateAssignToUserIdForClaim(userId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public StaffAllClaimsModel FilterClaimByType(int userId)
        {
            try
            {
                return this.claimsBO.FilterClaimByType(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void SubmitClaim(int claimId, bool isAutoApproved)
        {
            try
            {
                claimsBO.SubmitClaim(claimId, isAutoApproved);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Period GetClaimPeriod(int claimId)
        {
            try
            {
                return claimsBO.GetClaimPeriod(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IList GetClaimPeriodForBreadCrumb(int vendorId)
        {
            try
            {
                return claimsBO.GetClaimPeriodForBreadCrumb(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool AddInventoryAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            try
            {
                return claimsBO.AddInventoryAdjustment(claimId, modalResult);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            try
            {
                claimsBO.RemoveInternalAdjustment(claimId, adjustmentType, internalAdjustmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public ClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId)
        {
            try
            {
                return claimsBO.GetClaimAdjustmentModalResult(internalAdjustType, internalAdjustId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void EditInternalAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            try
            {
                claimsBO.EditInternalAdjustment(claimId, modalResult);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        #region Internal Adjust
        public PaginationDTO<ClaimInternalAdjustment, int> LoadClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            try
            {
                return claimsBO.LoadClaimInternalAdjustments(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, isStaff);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #endregion

        #region TransactionAdjusment
        public PaginationDTO<TransactionAdjustmentViewModel, int> LoadClaimTransactionAdjustment(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return claimsBO.LoadClaimTransactionAdjustment(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region participant claims
        public PaginationDTO<ClaimViewModel, int> LoadVendorClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return claimsBO.LoadVendorClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ClaimViewModel> GetExportDetailsParticipantClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return claimsBO.GetExportDetailsParticipantClaims(searchText, orderBy, sortDirection, vendorId, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        #endregion

        #region staff all claims

        public PaginationDTO<ClaimViewModel, int> LoadStaffVendorClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return claimsBO.LoadVendorStaffClaims(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ClaimViewModel> GetExportDetailsStaffClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return claimsBO.GetExportDetailsStaffClaims(searchText, orderBy, sortDirection, vendorId, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        #endregion

        public bool IsTransactionInSameClaim(int claimId, int transactionId)
        {
            try
            {
                return claimsBO.IsTransactionInSameClaim(claimId, transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return false;
        }

        public ClaimStatus GetClaimStatus(int claimId)
        {
            try
            {
                var claimStatus = claimsBO.GetClaimStatus(claimId);
                if (claimStatus == null)
                {
                    return ClaimStatus.Unknown;
                }
                return EnumHelper.GetValueFromDescription<ClaimStatus>(claimStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return ClaimStatus.Unknown;
        }

        public bool IsClaimOpen(int claimId)
        {
            try
            {
                return claimsBO.IsClaimOpen(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return false;
        }

        public bool IsClaimAllowAddNewTransaction(int claimId)
        {
            try
            {
                return claimsBO.IsClaimAllowAddNewTransaction(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return false;
        }

        public TransactionClaimAssociationDetailViewModel GetTransactionClaimAssociationDetail(int transactionId)
        {
            try
            {
                return claimsBO.GetTransactionClaimAssociationDetail(transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public List<TransactionSearchListViewModel> GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType, int direction, int vendorId)
        {
            try
            {
                return claimsBO.GetTransactionSearchList(claimId, transactionFormat, processingStatus, transactionType, direction, vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public VendorReference GetVendorRefernce(string regNumber)
        {
            try
            {
                return claimsBO.GetVendorRefernce(regNumber);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public VendorReference GetVendorById(int vendorId)
        {
            try
            {
                return claimsBO.GetVendorById(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public DateTime? UpdateClaimOnhold(int claimId, bool isOnhold)
        {
            try
            {
                return claimsBO.UpdateClaimOnhold(claimId, isOnhold);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public DateTime? UpdateAuditOnhold(int claimId, bool isOnhold)
        {
            try
            {
                return claimsBO.UpdateAuditOnhold(claimId, isOnhold);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ClaimWorkflowViewModel LoadClaimWorkflowViewModel(int claimId, decimal? claimsAmountTotal = null)
        {
            try
            {
                return claimsBO.LoadClaimWorkflowViewModel(claimId, claimsAmountTotal);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateClaimWorkflowStatus(int claimId, long reviewedBy, int fromStatus, int toStatus)
        {
            try
            {
                claimsBO.UpdateClaimWorkflowStatus(claimId, reviewedBy, fromStatus, toStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void UpdateHST(int claimId, decimal totalTax, bool isTaxApplicable=true)
        {
            try
            {
                claimsBO.UpdateHST(claimId, totalTax, isTaxApplicable);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        private void AddNotification(int claimId, long reviewedBy, int fromStatus, int toStatus)
        {

        }

        public string ApproveClaim(int claimId, int fromStatus, int toStatus)
        {
            try
            {
               return claimsBO.ApproveClaim(claimId, fromStatus, toStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {                  
                    throw;
                }
            }
            return "Approve failure";
        }

        //OTSTM2-1084 
        public bool ConcurrentUsersValidationBeforeApproving(int claimId, int fromStatus)
        {
            try
            {
                return claimsBO.ConcurrentUsersValidationBeforeApproving(claimId, fromStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return false;
        }

        public List<ClaimWorkflowUser> LoadClaimWorkflowUsers(string Workflow, string AccountName)
        {
            try
            {
                return claimsBO.LoadClaimWorkflowUsers(Workflow, AccountName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void ClaimBackToParticipant(int claimId)
        {
            try
            {
                claimsBO.ClaimBackToParticipant(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<InternalNoteViewModel> LoadClaimInternalNotes(int claimId)
        {
            try
            {
                return claimsBO.LoadClaimInternalNotes(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public InternalNoteViewModel AddClaimNote(int claimId, string note)
        {
            try
            {
                return claimsBO.AddClaimNote(claimId, note);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> LoadClaimNotesForExport(int claimId, string searchText, string sortcolumn, bool sortReverse)
        {
            try
            {
                return claimsBO.LoadClaimNotesForExport(claimId, searchText, sortcolumn, sortReverse);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public string ValidateWorkflowProcess(int claimId)
        {
            try
            {
                return claimsBO.ValidateWorkflowProcess(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public void UpdatedClaimMailDate(int claimId, DateTime mailDate)
        {
            try
            {
                claimsBO.UpdatedClaimMailDate(claimId, mailDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddSystemClaimNotes(int claimId, List<string> messages)
        {
            try
            {
                claimsBO.AddSystemClaimNotes(claimId, messages);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public int? GetTransactionIdByFriendlyId(long friendlyId, int claimId)
        {
            try
            {
                //#1172 filter UCR transactions for collector.
                return claimsBO.GetTransactionIdByFriendlyId(friendlyId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public VendorReference GetVendorReference(int claimId)
        {
            try
            {
                return claimsBO.GetVendorReference(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<int> GetAllClaimsToReCalculate(int? vendorType)
        {
            try
            {
                return claimsBO.GetAllClaimsToReCalculate(vendorType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<int> GetAllClaimsToReCalculateByStatus(int? vendorType = null, string status = "")
        {
            try
            {
                return claimsBO.GetAllClaimsToReCalculateByStatus(vendorType, status);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //Notification Load test only
        public IEnumerable<int> GetAllUnderReviewClaimsIds()
        {
            try
            {
                return claimsBO.GetAllUnderReviewClaimsIds();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<int> GetAllApprovedClaimIdsForInventoryCal(int? vendorId)
        {
            try
            {
                return claimsBO.GetAllApprovedClaimIdsForInventoryCal(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void RecalculateVendorInventory(int claimId)
        {
            try
            {
                claimsBO.RecalculateClaimInvenotry(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void RecalculateClaimInventory(int claimId)
        {
            try
            {
                claimsBO.RecalculateClaimInvenotry(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public string checkOtherClaimStatus(int claimId, int transactionId)
        {
            try
            {
                return claimsBO.checkOtherClaimStatus(claimId, transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public StaffAllClaimsModel FindClaimById(int claimId)
        {
            try
            {
                return claimsBO.FindClaimById(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-551
        public Claim FindClaimByClaimId(int claimId)
        {
            try
            {
                return claimsBO.FindClaimByClaimId(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void ApproveLateCollectorClaimForSupport(string registrationNumber, int claimPeriodId)
        {
            try
            {
                claimsBO.ApproveLateCollectorClaimForSupport(registrationNumber, claimPeriodId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<int> GetMissingReportDataClaimIds()
        {
            try
            {
                return claimsBO.GetMissingReportDataClaimIds();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-270 
        public InternalNoteViewModel AddNotesHandlerInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string notes)
        {
            try
            {
                return claimsBO.AddNotesHandlerInternalAdjustment(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        //OTSTM2-270 
        public List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId)
        {
            try
            {
                return claimsBO.GetInternalNotesInternalAdjustment(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        //OTSTM2-270 
        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            try
            {
                return claimsBO.ExportToExcelInternalAdjustmentNotes(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<int> GetMissingCollectorClaimIds()
        {
            try
            {
                return claimsBO.GetMissingCollectorClaimIds();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<int> LoadAllClaimIdsByVendorTypeAndStatus(int vendorType = 2, CL.TMS.Common.Enum.ClaimStatus claimStatus = CL.TMS.Common.Enum.ClaimStatus.Approved)
        {
            try
            {
                return claimsBO.LoadAllClaimIdsByVendorTypeAndStatus(vendorType, claimStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region Internal diagnostic method
        public List<ClaimTireOrigin> LoadClaimTireOrigins(int claimId)
        {
            try
            {
                return claimsBO.LoadClaimTireOrigins(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<ClaimDetailViewModel> LoadClaimDetails(int claimId, List<Item> items)
        {
            try
            {
                return claimsBO.LoadClaimDetails(claimId, items);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId)
        {
            try
            {
                return claimsBO.LoadClaimInventoryAdjustments(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<int> getAllVenderByType(int vType)
        {
            try
            {
                return claimsBO.getAllVenderByType(vType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public List<int> LoadAllClaimIdsByVendorID(int vType)
        {
            try
            {
                return claimsBO.LoadAllClaimIdsByVendorID(vType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public string GetPrimaryContactByClaimId(int claimId)
        {
            try
            {
                return claimsBO.GetPrimaryContactByClaimId(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion
    }
}
