﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using CL.TMS.IRepository.Claims;
using CL.TMS.Common.Enum;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.Framework.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Repository.System;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.GroupRate;

namespace CL.TMS.SystemServices
{
    public class ConfigurationsServices : IConfigurationsServices
    {
        private readonly ConfigurationsBO configurationsBO;

        public ConfigurationsServices(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository, IConfigurationsRepository configurationsRepository, ISettingRepository settingRepository)
        {
            this.configurationsBO = new ConfigurationsBO(userRepository, vendorRepository, eventAggregator, messageRepository, configurationsRepository, settingRepository);
        }

        public PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return configurationsBO.LoadRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return configurationsBO.LoadPIRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public RateDetailsVM LoadRateDetailsByTransactionID(int rateTransactionID, int category, bool isSpecific)
        {
            try
            {
                return configurationsBO.LoadRateDetailsByTransactionID(rateTransactionID, category, isSpecific);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public bool AddNewRate(RateDetailsVM newRate, long userId, int category)
        {
            try
            {
                return configurationsBO.AddNewRate(newRate, userId, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool updateRate(RateDetailsVM rateVM, long userId)
        {
            try
            {
                return configurationsBO.updateRate(rateVM, userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool RemoveRateTransaction(int rateTransactionID, int category)
        {
            try
            {
                return configurationsBO.RemoveRateTransaction(rateTransactionID, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool IsFutureRateExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureRateExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsFutureRateExists(int category, bool isSpecific)
        {
            try
            {
                return configurationsBO.IsFutureRateExists(category, isSpecific);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsFutureCollectorRateExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureCollectorRateExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsCollectorRateTransactionExists(int category)
        {
            try
            {
                return configurationsBO.IsCollectorRateTransactionExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool IsFutureCollectorRateTransactionExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureCollectorRateTransactionExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }


        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            try
            {
                return configurationsBO.AddTransactionNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public InternalNoteViewModel AddTransactionNote(string transactionID, string notes)
        {
            try
            {
                return configurationsBO.AddTransactionNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            try
            {
                return configurationsBO.LoadRateTransactionNoteByID(rateTransactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return configurationsBO.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return configurationsBO.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public AppSettingsVM LoadAppSettings()
        {
            {
                try
                {
                    return configurationsBO.LoadAppSettings();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }

        public List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType)
        {
            {
                try
                {
                    return configurationsBO.LoadAppSettingNotesByType(noteType);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }

        public bool updateAppSettings(AppSettingsVM appSettingsVM, long userId)
        {
            {
                try
                {
                    return configurationsBO.updateAppSettings(appSettingsVM, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }


        #region // Account Thresholds
        public AccountThresholdsVM LoadAccountThresholds()
        {
            {
                try
                {
                    return configurationsBO.LoadAccountThresholds();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }
        public bool UpdateAccountThresholds(AccountThresholdsVM vm)
        {
            try
            {
                return configurationsBO.UpdateAccountThresholds(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        #endregion

        #region Transaction Thresholds
        public TransactionThresholdsVM LoadTransactionThresholds()
        {
            try
            {
                return configurationsBO.LoadTransactionThresholds();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            try
            {
                return configurationsBO.UpdateTransactionThresholds(transactionThresholdsVM);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        #endregion
        #region Vendor Group
        public PaginationDTO<VendorGroupListViewModel, int> LoadVendorGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category)
        {
            try
            {
                return configurationsBO.LoadVendorGroupList(pageIndex, pageSize, searchText, orderBy, sortDirection, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<VendorGroupListViewModel> LoadEffectiveVendorGroupList(DateTime effectiveDate, string category)
        {
            try
            {
                return configurationsBO.LoadEffectiveVendorGroupList(effectiveDate, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public bool AddNewVendorGroup(VendorGroupDetailCommonViewModel newGroup, long userId, string category)
        {
            {
                try
                {
                    return configurationsBO.AddNewVendorGroup(newGroup, userId, category);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool UpdateVendorGroup(VendorGroupDetailCommonViewModel updatedGroup, long userId)
        {
            {
                try
                {
                    return configurationsBO.UpdateVendorGroup(updatedGroup, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool RemoveVendorGroup(int groupId)
        {
            {
                try
                {
                    return configurationsBO.RemoveVendorGroup(groupId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool VendorGroupUniqueNameCheck(string name, string category)
        {
            try
            {
                return configurationsBO.VendorGroupUniqueNameCheck(name, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool IsGroupMappingExisting(int id)
        {
            try
            {
                return configurationsBO.IsGroupMappingExisting(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        //public bool IsGroupMappingExisting(int? RateGroupId, int? GroupId, int? VendorId)
        //{
        //    try
        //    {
        //        return configurationsBO.IsGroupMappingExisting(RateGroupId, GroupId, VendorId);
        //    }
        //    catch (Exception ex)
        //    {
        //        var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
        //        if (rethrow)
        //        {
        //            throw;
        //        }
        //    }
        //    return false;
        //}

        #endregion

        #region Rate Group
        public PaginationDTO<RateGroupListViewModel, int> LoadRateGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category)
        {
            try
            {
                return configurationsBO.LoadRateGroupList(pageIndex, pageSize, searchText, orderBy, sortDirection, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<ItemModel<string>> GetVendorGroupList(string category)
        {
            try
            {
                return configurationsBO.GetVendorGroupList(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<ItemModel<string>> GetAssociatedVendorGroupList(int rateGroupId)
        {
            try
            {
                return configurationsBO.GetAssociatedVendorGroupList(rateGroupId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<ItemModel<int>> LoadVendors(int vendorGroupId, int rateGroupId)
        {
            try
            {
                return configurationsBO.LoadVendors(vendorGroupId, rateGroupId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return new List<ItemModel<int>>();
        }

        public List<ItemModel<int>> LoadDefaultVendors(int vendorGroupId, string category)
        {
            try
            {
                return configurationsBO.LoadDefaultVendors(vendorGroupId, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return new List<ItemModel<int>>();
        }

        public bool RateGroupUniqueNameCheck(string name, string category)
        {
            try
            {
                return configurationsBO.RateGroupUniqueNameCheck(name, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool CreateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId)
        {
            try
            {
                configurationsBO.CreateRateGroup(rateGroupViewModel, userId);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public bool UpdateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId)
        {
            try
            {
                configurationsBO.UpdateRateGroup(rateGroupViewModel, userId);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public bool RemoveVendorRateGroupMapping(int RateGroupId)
        {
            {
                try
                {
                    return configurationsBO.RemoveVendorRateGroupMapping(RateGroupId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        //public RateGroupViewModel LoadRateGroup(int id)
        //{
        //    try
        //    {
        //        return configurationsBO.LoadRateGroup(id);
        //    }
        //    catch (Exception ex)
        //    {
        //        var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
        //        if (rethrow)
        //        {
        //            throw;
        //        }
        //    }
        //    return null;
        //}

        //public bool DeleteRateGroup(int id)
        //{
        //    try
        //    {
        //        configurationsBO.DeleteRateGroup(id);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
        //        if (rethrow)
        //        {
        //            throw;
        //        }
        //        return false;
        //    }
        //}
        #endregion

        public List<ItemModel<string>> LoadRateGroups(string category)
        {
            try
            {
                return configurationsBO.LoadRateGroups(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public TransportationIncentiveViewModel LoadNewTransportationIncentiveRates(int pickupRateGroupId, int deliveryRateGroupId)
        {
            try
            {
                return configurationsBO.LoadNewTransportationIncentiveRates(pickupRateGroupId, deliveryRateGroupId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool SaveNewTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId)
        {
            try
            {
                configurationsBO.SaveNewTransportationIncentiveRates(transportationIncentiveViewModel, userId);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public TransportationIncentiveViewModel LoadTransportationIncentiveViewModel(int rateTransactionId)
        {
            try
            {
                return configurationsBO.LoadTransportationIncentiveViewModel(rateTransactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool UpdateTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId)
        {
            try
            {
                configurationsBO.UpdateTransportationIncentiveRates(transportationIncentiveViewModel, userId);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public List<RateGroupRate> LoadRateGroupRates(DateTime periodStartDate, DateTime periodEndDate)
        {
            try
            {
                return configurationsBO.LoadRateGroupRates(periodStartDate, periodEndDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<VendorRateGroup> LoadVendorRateGroups(int rateGroupId)
        {
            try
            {
                return configurationsBO.LoadVendorRateGroups(rateGroupId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<VendorClaimRateGroupInfo> GetVendorGroupInfoByDateVid(int vendorId, DateTime claimPeriodStart, DateTime? claimPeriodEnd, string rateGroupCategory)
        {
            try
            {
                return configurationsBO.GetVendorGroupInfoByDateVid(vendorId, claimPeriodStart, claimPeriodEnd, rateGroupCategory);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region STC Premium
        public PaginationDTO<STCPremiumListViewModel, int> LoadSTCPremiumList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return configurationsBO.LoadSTCPremiumList(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public STCEventsCountViewModel LoadSTCEventsCount()
        {
            try
            {
                return configurationsBO.LoadSTCEventsCount();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public STCPremiumListViewModel LoadSTCEventDetailsByEventNumber(string eventNumber)
        {
            try
            {
                return configurationsBO.LoadSTCEventDetailsByEventNumber(eventNumber);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool AddNewSTCEvent(STCPremiumListViewModel newEvent, long userId)
        {
            {
                try
                {
                    return configurationsBO.AddNewSTCEvent(newEvent, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool UpdateSTCEvent(STCPremiumListViewModel updatedEvent, long userId)
        {
            {
                try
                {
                    return configurationsBO.UpdateSTCEvent(updatedEvent, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool RemoveSTCEvent(int id, string eventNumber)
        {
            {
                try
                {
                    return configurationsBO.RemoveSTCEvent(id, eventNumber);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }
        #endregion


        #region TCR Service Threshold
        public TCRServiceThresholdListViewModel LoadTCRServiceThresholdList()
        {
            try
            {
                return configurationsBO.LoadTCRServiceThresholdList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            {
                try
                {
                    return configurationsBO.AddTCRServiceThreshold(vm, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            {
                try
                {
                    return configurationsBO.UpdateTCRServiceThreshold(vm, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool RemoveTCRServiceThresohold(int id, long userId)
        {
            {
                try
                {
                    return configurationsBO.RemoveTCRServiceThresohold(id, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public IEnumerable<TCRListViewModel> ExportTCRServiceThresholds()
        {
            {
                try
                {
                    return configurationsBO.ExportTCRServiceThresholds();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }
        #endregion

    }
}