﻿using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using System.Data;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.Framework.DTO;
using CL.TMS.ExceptionHandling;
using CL.TMS.Configuration;
using CL.Framework.Common;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Metric;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.SystemServices
{
    public class ReportingService : IReportingService
    {
        private readonly ReportingBO reportingBO;

        #region Queries
        public ReportingService(IReportingRepository reportingRepository, IUserRepository userRepository)
        {
            this.reportingBO = new ReportingBO(reportingRepository,userRepository);
        }
        public IEnumerable<ReportCategory> GetAllReportCategories()
        {
            return reportingBO.GetAllReportCategories();
        }
        public List<ReportCategory> GetPermittedReportCategories()
        {
            return reportingBO.GetPermittedReportCategories();
        }

        public IEnumerable<Report> GetReportsByReportCategory(int Id)
        {
            return reportingBO.GetReportsByCategoryId(Id);
        }
        public IEnumerable<RegistrantWeeklyReport> GetRegistrantWeeklyReport()
        {
            return reportingBO.GetRegistrantWeeklyReport();
        }
        public IEnumerable<SpRptTsfExtractInBatchOnlyReportGp> GetTsfExtrtactInBatchOnlyReportGp()
        {
            return reportingBO.GetTsfExtrtactInBatchOnlyReportGp();
        }
        public IEnumerable<SpRptProcessorTIPIReport> GetProcessorTIPIReport(DateTime? startDate, DateTime? endDate) {
            var reports=reportingBO.GetProcessorTIPIReport(startDate, endDate).ToList();
            // the subtotal include hst
            reports.ForEach(x => {
            		x.Subtotal = x.Subtotal - x.HST;
            	 	x.PI = x.PI - x.HST;
            	}); 
            return reports;
        }
        public IEnumerable<SpRptHaulerCollectorComparisonReport> GetHaulerCollectorComparisonReport(DateTime? startDate, DateTime? endDate, string registrationNumber)
        {
            return reportingBO.GetHaulerCollectorComparisonReport(startDate, endDate, registrationNumber);
        }
        public Report GetReport(int Id)
        {
            return reportingBO.GetReport(Id);
        }

        public IEnumerable<StewardRptRevenueSupplyNewTireVM> GetStewardRevenueSupplyNewTireTypes(ReportDetailsViewModel detail)
        {
            try
            {
                return reportingBO.GetStewardRevenueSupplyNewTireTypes(detail);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVM> GetStewardRevenueSupplyNewTireCreditTypes(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            try
            {
                return reportingBO.GetStewardRevenueSupplyNewTireCreditTypes(detail, TSFNegAdjSwitchDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<StewardRptRevenueSupplyNewTireCreditVMsp> GetStewardRevenueSupplyNewTireCreditTypes_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            try
            {
                return reportingBO.GetStewardRevenueSupplyNewTireCreditTypes_SP(detail, TSFNegAdjSwitchDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<WebListingCollectorReportPostalVM> GetWebListingCollectorReportPostal(int reportId)
        {
            try
            {
                return reportingBO.GetWebListingCollectorReportPostal(reportId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<WebListingStewardReport> GetWebListingStewardReportPostal(int reportId)
        {
            try
            {
                return reportingBO.GetWebListingStewardReportPostal(reportId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<WebListingProcessorReport> GetWebListingProcessorReportPostal(int reportId)
        {
            try
            {
                return reportingBO.GetWebListingProcessorReportPostal(reportId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<WebListingRPMReport> GetWebListingRPMReportPostal(int reportId)
        {
            try
            {
                return reportingBO.GetWebListingRPMReportPostal(reportId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<WebListingHaulerReport> GetWebListingHaulerReport()
        {
            try
            {
                return reportingBO.GetWebListingHaulerReport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<HaulerTireMovementReport> GetHaulerTireMovementReport(ReportDetailsViewModel model)
        {
            try
            {
                return reportingBO.GetHaulerTireMovementReport(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        
        public IEnumerable<HaulerVolumeReport> GetHaulerVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            return reportingBO.GetHaulerVolumeReport(startDate, endDate);
        }
        public IEnumerable<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> GetDetailHaulerVolumeReportBasedOnScaleWeight(DateTime? startDate, DateTime? endDate)
        {
            return reportingBO.GetDetailHaulerVolumeReportBasedOnScaleWeight(startDate, endDate);
        }
        public IEnumerable<SpRptProcessorVolumeReport> GetProcessorVolumeReport(DateTime? startDate, DateTime? endDate)
        {
            return reportingBO.GetProcessorVolumeReport(startDate, endDate);
        }
        public IEnumerable<RpmData> GetRpmData(ReportDetailsViewModel detail)
        {
            try
            {
                var result = reportingBO.GetRpmData(detail);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<RpmCumulative> GetRpmCumulative(ReportDetailsViewModel detail)
        {
            try
            {
                var result = reportingBO.GetRpmCumulative(detail);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<ProcessorDispositionOfResidual> GetProcessorDispositionOfResidual()
        {
            return reportingBO.GetProcessorDispositionOfResidual();
        }

        public IEnumerable<StewardNonFilers> GetStewardNonFilers(DateTime? startDate, DateTime? endDate)
        {
            return reportingBO.GetStewardNonFilers(startDate, endDate);
        }

        public IEnumerable<TSFRptOnlinePaymentsOutstandingVM> GetTSFOnlinePaymentsOutstanding(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate)
        {
            try
            {
                return reportingBO.GetTSFOnlinePaymentsOutstanding(model, TSFNegAdjSwitchDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion
        public IEnumerable<CollectorCumulativeReportVM> GetCollectorCumulativeReport(ReportDetailsViewModel model)
        {
            try
            {
                return reportingBO.GetCollectorCumulativeReport(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<HaulerTireMovementReportVM> GetHaulerTireMovementReportVM(ReportDetailsViewModel model)
        {
            try
            {
                return reportingBO.GetHaulerTireMovementReportVM(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<CollectorTireOriginReport> GetCollectorTireOriginReport(ReportDetailsViewModel model)
        {
            try
            {
                return reportingBO.GetCollectorTireOriginReport(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<HaulerTireCollectionReport> GetHaulerTireCollectionReport(ReportDetailsViewModel model)
        {
            try
            {
                return reportingBO.GetHaulerTireCollectionReport(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<StaffUserNameAndEmailViewModel> GetUserNameBySearchText(string searchText)
        {
            try
            {
                return reportingBO.GetUserNameBySearchText(searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<UserPermissionExportViewModel> GetStaffUserRolePermissionMatrix(string userEmail)
        {
            try
            {
                return reportingBO.GetStaffUserRolePermissionMatrix(userEmail);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region OTSTM2-215
        public List<CollectionGenerationOfTiresBasedOnFSAReportVM> GetCollectionGenerationOfTiresBasedOnFSAReport(CollectionGenerationOfTiresBasedOnFSAReportSelectionVM selectionModel)
        {
            try
            {
                return reportingBO.GetCollectionGenerationOfTiresBasedOnFSAReport(selectionModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string region, string zone, string fromDate, string toDate)
        {
            try
            {
                return reportingBO.GetReportDetailHandler(pageIndex, pageSize, searchText, orderBy, sortDirection, region, zone, fromDate, toDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region Metric Reports
        public MetricVM LoadMetricDetailsByID(MetricParams paramModel)
        {
            try
            {
                return reportingBO.LoadMetricDetailsByID(paramModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region OTSTM2-1226
        public List<CollectionGenerationOfTiresBasedOnRateGroupVM> GetCollectionGenerationOfTiresBasedOnRateGroup(CollectionGenerationOfTiresBasedOnRateGroupSelectionVM selectionModel)
        {
            try
            {
                return reportingBO.GetCollectionGenerationOfTiresBasedOnRateGroup(selectionModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailByRateGroupHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string pickupRateGroup, string collectorGroup, string fromDate, string toDate)
        {
            try
            {
                return reportingBO.GetReportDetailByRateGroupHandler(pageIndex, pageSize, searchText, orderBy, sortDirection, pickupRateGroup, collectorGroup, fromDate, toDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<StewardRptRevenueSupplyOldTireCreditVMsp> GetStewardRevenueSupplyOldTireCreditTypes_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate)
        {
            try
            {
                return reportingBO.GetStewardRevenueSupplyOldTireCreditTypes_SP(detail, TSFNegAdjSwitchDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<StewardRptRevenueSupplyOldTireVM> GetStewardRevenueSupplyOldTireTypes(ReportDetailsViewModel detail)
        {
            try
            {
                return reportingBO.GetStewardRevenueSupplyOldTireTypes(detail);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #endregion
    }
}
