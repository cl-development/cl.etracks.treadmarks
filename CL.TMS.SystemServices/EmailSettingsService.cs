﻿using CL.TMS.DataContracts.ViewModel.EmailSettings;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.SystemServices
{
    public class EmailSettingsService : IEmailSettingsService
    {
        private readonly EmailSettingsBO emailSettingsBO;

        public EmailSettingsService(IEmailSettingsRepository emailSettingsRepository)
        {
            this.emailSettingsBO = new EmailSettingsBO(emailSettingsRepository);
        }

        //Email Settings - General
        public EmailSettingsGeneralVM LoadEmailSettingsGeneralInformation()
        {
            try
            {
                return emailSettingsBO.LoadEmailSettingsGeneralInformation();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM)
        {
            try
            {
                return emailSettingsBO.UpdateEmailSettingsGeneralInformation(emailSettingsGeneralVM);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        //Email Settings - Content
        public Dictionary<int, string> GetEmailDisplayNamesList()
        {
            return emailSettingsBO.GetEmailDisplayNamesList();
        }

        public EmailVM GetEmailByID(int emailID)
        {
            try
            {
                return emailSettingsBO.GetEmailByID(emailID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public EmailVM GetEmailByName(string emailName)
        {
            try
            {
                return emailSettingsBO.GetEmailByName(emailName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public string GetPreviewEmailByID(int emailID)
        {
            try
            {
                return emailSettingsBO.GetReadyToSendEmailByID(emailID, true);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public string GetCompleteEmailByName(string emailName)
        {
            try
            {
                return emailSettingsBO.GetReadyToSendEmailByName(emailName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public bool SaveEmailContent(EmailVM emailContent)
        {
            try
            {
                return emailSettingsBO.SaveEmailContent(emailContent);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsEmailEnabled(string emailName)
        {
            try
            {
                return emailSettingsBO.IsEmailEnabled(emailName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
    }
}
