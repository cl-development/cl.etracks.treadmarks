﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using Transaction = CL.TMS.DataContracts.DomainEntities.Transaction;
using MobileTransaction = CL.TMS.DataContracts.Mobile.Transaction;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.IRepository.Registrant;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;
using CL.TMS.DataContracts.ViewModel.Common;
using System.IO;
using CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels;

namespace CL.TMS.SystemServices
{
    public class TransactionService : ITransactionService
    {
        private TransactionBO transactionBO;

        public TransactionService(ITransactionRepository transactionRepository, IMobileTransactionRepository mobileTransactionRepository, IClaimsRepository claimsRepository, IUserRepository userRepository, IEventAggregator eventAggregator, IVendorRepository vendorRepository, ISettingRepository settingRepository, IConfigurationsRepository configurationsRepository)
        {
            transactionBO = new TransactionBO(transactionRepository, mobileTransactionRepository, claimsRepository, userRepository, eventAggregator, vendorRepository, settingRepository, configurationsRepository);
        }

        public TransactionCreationNotificationDetailViewModel AddHaulerPapeFormTransaction(IHaulerTransactionViewModel model)
        {
            try
            {
                return transactionBO.AddHaulerPapeFormTransaction(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public TransactionCreationNotificationDetailViewModel AddCollectorPapeFormTransaction(ICollectorTransactionViewModel model)
        {
            try
            {
                return transactionBO.AddCollectorPapeFormTransaction(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public TransactionCreationNotificationDetailViewModel AddProcessorPapeFormTransaction(IProcessorTransactionViewModel model)
        {
            try
            {
                return transactionBO.AddProcessorPapeFormTransaction(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public TransactionCreationNotificationDetailViewModel AddRPMPapeFormTransaction(IRPMTransactionViewModel model)
        {
            try
            {
                return transactionBO.AddRPMPapeFormTransaction(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        //OTSTM2-81
        public int ImportSPSTransactions(StreamReader InputStream, int claimId)
        {
            int count = 0;
            try
            {
                count = transactionBO.ImportSPSTransactions(InputStream, claimId);
            }
            catch (Exception ex)
            {
                if (ex.Message == "data error")
                {
                    throw ex;
                }
                else
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
            }
            return count;
        }

        public ITransactionAdjViewModel GetTransactionAdjustmentModel(int transactionId)
        {
            try
            {
                return transactionBO.GetTransactionAdjustmentModel(transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AdjustTransaction(ITransactionAdjViewModel model, Period claimPeriod)
        {
            try
            {
                transactionBO.AdjustTransaction(model, claimPeriod);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void SubmitAdjustmentStatusChange(int id, TransactionAdjustmentStatus reviewStatus, bool getUpdatedData, string notes)
        {
            try
            {
                transactionBO.SubmitAdjustmentStatusChange(id, reviewStatus, getUpdatedData, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ImportMobileTransaction(Guid transactionId)
        {
            try
            {
                //Task.Factory.StartNew(() =>
                {
                    transactionBO.ImportMobileTransaction(transactionId);
                }//);
            }
            catch (Exception ex)
            {
                //var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                //if (rethrow)
                {
                    throw ex;
                }
            }
        }
        public MobileTransaction GetMobileTransaction(Guid transactionId)
        {
            try
            {
                return transactionBO.GetMobileTransaction(transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ITransactionDetailViewModel GetTransactionHandlerData(int transactionId, string vendorType, bool adjRequested, int? transactionAdjustmentId = null, int? claimId = 0)
        {
            try
            {
                return transactionBO.GetTransactionHandlerData(transactionId, adjRequested, transactionAdjustmentId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #region All Transactions
        public void AcceptTransaction(int transactionId)
        {
            try
            {
                if (transactionBO.UpdateTransactionStatus(transactionId, TransactionStatus.Pending.ToString(), TransactionStatus.Completed.ToString()))
                {
                    string noteText = CheckTransactionVendorStatus(transactionId, true);
                    if (!string.IsNullOrEmpty(noteText))//one of vendor is inactive
                    {
                        //avoid re-calculate 
                        transactionBO.UpdateTransactionProcessingStatusNoRecalculateClaims(transactionId, TransactionProcessingStatus.Invalidated);
                    }
                    transactionBO.AddTransactionToClaim(transactionId);//re-calculate will be triggered here
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void RejectTransaction(int transactionId)
        {
            try
            {
                transactionBO.UpdateTransactionStatus(transactionId, TransactionStatus.Pending.ToString(),
                    TransactionStatus.Rejected.ToString());
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void VoidTransaction(int transactionId)
        {
            try
            {
                transactionBO.UpdateTransactionStatus(transactionId, TransactionStatus.Pending.ToString(),
                    TransactionStatus.Voided.ToString());
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void VoidSPSRTransaction(int transactionId)
        {
            try
            {
                transactionBO.UpdateSPSRTransactionStatus(transactionId, TransactionProcessingStatus.Unreviewed.ToString(),
                    TransactionStatus.Voided.ToString());
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void InvalidTransaction(int transactionId)
        {
            try
            {
                transactionBO.UpdateTransactionProcessingStatus(transactionId, TransactionProcessingStatus.Invalidated);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void AddTransactionRetailPrice(int transactionId, decimal retailPrice)
        {
            try
            {
                transactionBO.AddTransactionRetailPrice(transactionId, retailPrice);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ApproveTransaction(int transactionId)
        {
            try
            {
                transactionBO.UpdateTransactionProcessingStatus(transactionId, TransactionProcessingStatus.Approved);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public bool UpdateTransactionProcessingStatus(int transactionId, TransactionProcessingStatus status)
        {
            try
            {
                return transactionBO.UpdateTransactionProcessingStatus(transactionId, status);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public PaginationDTO<AllTransactionsViewModel, int> LoadAllTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt = "")
        {
            try
            {
                return transactionBO.LoadAllTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, queryFilter, sExtendSearchTxt);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        public PaginationDTO<TransactionViewModel, Guid> LoadTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            try
            {
                return new PaginationDTO<TransactionViewModel, Guid>();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<AllTransactionsViewModel> GetTransactionDetailsExport(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType, string queryFilter)
        {
            try
            {
                return transactionBO.GetTransactionDetailsExport(searchText, orderBy, sortDirection, vendorId, transactionType, "", queryFilter);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public Transaction GetTransactionByFriendlyID(long friendlyId)
        {
            try
            {
                return transactionBO.GetTransactionByFriendlyID(friendlyId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool IsVendorActive(int vendorId, DateTime on)
        {
            try
            {
                return transactionBO.IsVendorActive(vendorId, on);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return true;
        }

        public List<TransactionDisabledDateRangeViewModel> GetVendorInactiveDateRanges(int vendorId)
        {
            try
            {
                return transactionBO.GetVendorInactiveDateRanges(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<VendorStatusDateRangeModel> GetVendorStatusDateRanges(int vendorId)
        {
            try
            {
                return transactionBO.GetVendorStatusDateRanges(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public InternalNoteViewModel AddTransactionNote(int transactionId, string note)
        {
            try
            {
                return transactionBO.AddTransactionNote(transactionId, note);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-765
        public List<InternalNoteViewModel> GetAllTransactionNote(int transactionId)
        {
            try
            {
                return transactionBO.GetAllTransactionNote(transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMStaffTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType = "")
        {
            try
            {
                return transactionBO.LoadRPMStaffTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, transactionType, dataSubType, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMVendorTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType = "")
        {
            try
            {
                return transactionBO.LoadRPMVendorTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId, transactionType, dataSubType, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RPMCommonTransactionListViewModel> GetRPMExportDetailsStaffTransactions(string search, string sortcolumn, string sortdirection, int vendorId, string transactionType, int claimId, string dataSubType)
        {
            try
            {
                return transactionBO.GetRPMExportDetailsStaffClaims(search, sortcolumn, sortdirection, vendorId, transactionType, dataSubType, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RPMCommonTransactionListViewModel> GetRPMExportDetailsParticipantTransactions(string search, string sortcolumn, string sortdirection, int vendorId, string transactionType, int claimId, string dataSubType)
        {
            try
            {
                return transactionBO.GetRPMExportDetailsParticipantClaims(search, sortcolumn, sortdirection, vendorId, transactionType, dataSubType, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProcessorPTRWeightDetailViewModel GetPTRWeightDetail(long number)
        {
            try
            {
                return transactionBO.GetPTRWeightDetail(number);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerUCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType)
        {
            try
            {
                return transactionBO.LoadHaulerHITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, dataSubType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerSTCTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerRTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffUCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType)
        {
            try
            {
                return transactionBO.LoadHaulerStaffHITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, dataSubType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffSTCTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadHaulerStaffRTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadCollectorDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadCollectorTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadCollectorStaffDOTTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadCollectorStaffTCRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorDORTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            try
            {
                return transactionBO.LoadProcessorPITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, vendorId, dataSubType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorSPSTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorStaffPTRTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorStaffDORTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType)
        {
            try
            {
                return transactionBO.LoadProcessorStaffPITTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, vendorId, dataSubType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return transactionBO.LoadProcessorStaffSPSTransactions(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-620
        public bool IsTransAdjPendingAndUnreviewed(int claimId, int transactionId)
        {
            try
            {
                return transactionBO.IsTransAdjPendingAndUnreviewed(claimId, transactionId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return true;
        }
        //OTSTM2-660
        public TransactionReviewToolBar getUpdatedReviewToolBar(int transactionId, int? claimId)
        {
            try
            {
                return transactionBO.getUpdatedReviewToolBar(transactionId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public string CheckTransactionVendorStatus(int transactionId, bool addLog)
        {
            try
            {
                return transactionBO.CheckTransactionVendorStatus(transactionId, addLog);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return string.Empty;
        }

        #region STC Premium Implementation
        public STCEventNumberValidationStatus EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate)
        {
            try
            {
                return transactionBO.EventNumberValidationCheck(eventNumber, transactionStartDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return STCEventNumberValidationStatus.Pass;
        }
        #endregion
    }
}
