﻿using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Framework.DTO;
using CL.TMS.HaulerBLL;
using CL.TMS.ServiceContracts.HaulerServices;

namespace CL.TMS.SystemServices
{
    public class DashBoardService : IDashBoardService
    {
        private readonly ApplicatoinBriefBO applicatoinBriefBO;
        private readonly RegistrantManageBO registrantManageBO;
        
        public DashBoardService(IApplicationBriefRepository applicationBriefRepository, IRegistrantManageRepository registrantManageRepository)
        {
            this.applicatoinBriefBO = new ApplicatoinBriefBO(applicationBriefRepository);
            this.registrantManageBO = new RegistrantManageBO(registrantManageRepository);
        }
        public PaginationDTO<ApplicationViewModel,int> GetAllApplicatoinBriefByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                var result= applicatoinBriefBO.GetAllApplicationBrief(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<RegistrantManageModel,long> GetAllRegistrantByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                var result= registrantManageBO.GetAllRegistrantBrief(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RegistrantManageModel> LoadRegistrants(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return registrantManageBO.LoadRegistrants(searchText, sortcolumn, sortdirection, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ApplicationViewModel> LoadApplications(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return applicatoinBriefBO.LoadApplications(searchText, sortcolumn, sortdirection, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ApplicationViewModel GetFirstSubmitApplication()
        {
            try
            {
                return applicatoinBriefBO.GetFirstSubmitApplication();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }
    }
}
