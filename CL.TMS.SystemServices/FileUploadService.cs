﻿using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.RPM;

namespace CL.TMS.SystemServices
{
    public class FileUploadService : IFileUploadService
    {
        private FileUploadBO fileUploadBO;

        public FileUploadService(IApplicationRepository applicationRepository, IClaimsRepository claimRepository, ITSFRemittanceRepository tsfclaimRepository, ITransactionRepository transactioRepository, IVendorRepository vendorRepository, IRetailConnectionRepository retailConnectioRepository)
        {
            this.fileUploadBO = new FileUploadBO(applicationRepository, claimRepository, tsfclaimRepository, transactioRepository, vendorRepository, retailConnectioRepository);
        }


        #region ApplicationUploadService
        public void AddApplicationAttachments(int applicationId, IEnumerable<AttachmentModel> attachments)
        {
            try
            {
                this.fileUploadBO.AddApplicationAttachments(applicationId, attachments);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public IEnumerable<AttachmentModel> GetApplicationAttachments(int applicationId, bool bBankInfo = false)
        {
            try
            {
                return this.fileUploadBO.GetApplicationAttachments(applicationId, bBankInfo);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public AttachmentModel GetApplicationAttachment(int applicationId, string fileUniqueName)
        {
            try
            {
                return this.fileUploadBO.GetApplicationAttachment(applicationId, fileUniqueName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public void RemoveApplicationAttachment(int applicationId, string fileUniqueName, string deletedBy)
        {
            try
            {
                this.fileUploadBO.RemoveApplicationAttachment(applicationId, fileUniqueName, deletedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Attachment {0} deleted from Application by {1} on {2}", fileUniqueName, deletedBy, DateTime.Now), deletedBy);
            #endregion
        }

        public void UpdateAttachmentDescription(string fileUniqueName, string description)
        {
            this.fileUploadBO.UpdateAttachmentDescription(fileUniqueName, description);
        }

        #endregion


        #region VendorAttachmentService

        public void AddVendorAttachments(int vendorId, IEnumerable<AttachmentModel> attachments)
        {
            try
            {
                this.fileUploadBO.AddVendorAttachments(vendorId, attachments);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo = false)
        {
            try
            {
                return this.fileUploadBO.GetVendorAttachments(vendorId, bBankInfo);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public AttachmentModel GetVendorAttachment(int vendorId, string fileUniqueName)
        {
            try
            {
                return this.fileUploadBO.GetVendorAttachment(vendorId, fileUniqueName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public void RemoveVendorAttachment(int vendorId, string fileUniqueName, string deletedBy)
        {
            try
            {
                this.fileUploadBO.RemoveVendorAttachment(vendorId, fileUniqueName, deletedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Attachment {0} deleted from Application by {1} on {2}", fileUniqueName, deletedBy, DateTime.Now), deletedBy);
            #endregion
        }

        #endregion


        #region CustomerAttachmentService

        public void AddCustomerAttachments(int customerId, IEnumerable<AttachmentModel> attachments)
        {
            try
            {
                this.fileUploadBO.AddCustomerAttachments(customerId, attachments);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo = false)
        {
            try
            {
                return this.fileUploadBO.GetCustomerAttachments(customerId, bBankInfo);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public AttachmentModel GetCustomerAttachment(int customerId, string fileUniqueName)
        {
            try
            {
                return this.fileUploadBO.GetCustomerAttachment(customerId, fileUniqueName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void RemoveCustomerAttachment(int customerId, string fileUniqueName, string deletedBy)
        {
            try
            {
                this.fileUploadBO.RemoveCustomerAttachment(customerId, fileUniqueName, deletedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion


        #region Claim Upload Service
        public void AddClaimAttachments(int claimId, IEnumerable<AttachmentModel> attachments)
        {
            try
            {
                this.fileUploadBO.AddClaimAttachments(claimId, attachments);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo = false)
        {
            try
            {
                return this.fileUploadBO.GetClaimAttachments(claimId, bBankInfo);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public AttachmentModel GetClaimAttachment(int claimId, string fileUniqueName)
        {
            try
            {
                return this.fileUploadBO.GetClaimAttachment(claimId, fileUniqueName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public void RemoveClaimAttachment(int claimId, string fileUniqueName, string deletedBy)
        {
            try
            {
                this.fileUploadBO.RemoveClaimAttachment(claimId, fileUniqueName, deletedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Attachment {0} deleted from Application by {1} on {2}", fileUniqueName, deletedBy, DateTime.Now), deletedBy);
            #endregion
        }
        #endregion


        #region TSFClaim Upload Service
        public void AddTSFClaimAttachments(int TSFClaimId, IEnumerable<AttachmentModel> attachments)
        {
            try
            {
                this.fileUploadBO.AddTSFClaimAttachments(TSFClaimId, attachments);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public IEnumerable<AttachmentModel> GetTSFClaimAttachments(int TSFClaimId, bool bBankInfo = false)
        {
            try
            {
                return this.fileUploadBO.GetTSFClaimAttachments(TSFClaimId, bBankInfo);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public AttachmentModel GetTSFClaimAttachment(int TSFClaimId, string fileUniqueName)
        {
            try
            {
                return this.fileUploadBO.GetTSFClaimAttachment(TSFClaimId, fileUniqueName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

            #region Activity Logging
            //if (attachments.Count() > 0)
            //{
            //    IAttachment attach = attachments.First();
            //    LogActivity(applicationId, null, String.Format("Attachments added to Application by {0} on {1}", attach.CreatedBy, attach.CreatedDate), attach.CreatedBy);
            //}
            #endregion
        }
        public void RemoveTSFClaimAttachment(int TSFClaimId, string fileUniqueName, string deletedBy)
        {
            try
            {
                this.fileUploadBO.RemoveTSFClaimAttachment(TSFClaimId, fileUniqueName, deletedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Attachment {0} deleted from Application by {1} on {2}", fileUniqueName, deletedBy, DateTime.Now), deletedBy);
            #endregion
        }
        #endregion


        #region Transaction Upload Service
        public void AddTransactionAttachments(List<AttachmentModel> attachmentModelList)
        {
            try
            {
                this.fileUploadBO.AddTransactionAttachments(attachmentModelList);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public AttachmentModel GetTransactionAttachment(int attachmentId)
        {
            try
            {
                return this.fileUploadBO.GetTransactionAttachment(attachmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }

        public void RemoveTemporaryTransactionAttachment(int attachmentId)
        {
            try
            {
                this.fileUploadBO.RemoveTemporaryTransactionAttachment(attachmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void RemoveTemporaryTransactionAttachments(List<int> attachmentIdList)
        {
            try
            {
                this.fileUploadBO.RemoveTemporaryTransactionAttachments(attachmentIdList);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }       
        #endregion

        #region Product Upload Service
        public void AddProductAttachment(AttachmentModel attachmentModel)
        {
            try
            {
                this.fileUploadBO.AddProductAttachment(attachmentModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public AttachmentModel GetProductAttachment(int attachmentId)
        {
            try
            {
                return this.fileUploadBO.GetProductAttachment(attachmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            return null;
        }
        public void RemoveTemporaryProductAttachment(int attachmentId)
        {
            try
            {
                this.fileUploadBO.RemoveTemporaryProductAttachment(attachmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion
    }
}
