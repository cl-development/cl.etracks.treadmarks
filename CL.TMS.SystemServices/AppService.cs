﻿using CL.TMS.IRepository;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Configuration;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;

namespace CL.TMS.SystemServices
{
    public class AppService : IAppService
    {
        public string GetSettingValue(string key)
        {
            try
            {
                var setting = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == key);
                return setting == null ? string.Empty : setting.Value;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
    }
}
