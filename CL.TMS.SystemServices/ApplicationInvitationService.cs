﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using CL.TMS.SystemBLL;
using CL.TMS.ExceptionHandling;

namespace CL.TMS.SystemServices
{
    public class ApplicationInvitationService : IApplicationInvitationService
    {
        private readonly InvitationBO invitationBO;
        private IApplicationInvitationRepository appInvitationRepository;

        public ApplicationInvitationService(IApplicationRepository appRepository, IApplicationInvitationRepository appInvitationRepository)
        {
            this.invitationBO = new InvitationBO(appRepository, appInvitationRepository);
        }
        public ApplicationInvitation GetApplicationInvitationByEmailID(string email, string participantType)
        {
            try
            {
                return invitationBO.GetApplicationInvitationByEmailID(email, participantType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }
        public int GetParticipantTypeID(string participantName)
        {
            try
            {
                return invitationBO.GetParticipantTypeID(participantName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return 0;
        }

        public ApplicationInvitation AddApplicationInvitation(ApplicationInvitation appInvitation)
        {
            try
            {
                return invitationBO.AddApplicationInvitation(appInvitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ApplicationInvitation GetByTokenID(Guid guid)
        {
            try
            {
                return invitationBO.GetById(guid);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public void Update(ApplicationInvitation appInvitation)
        {
            try
            {
                invitationBO.Update(appInvitation);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Guid GetTokenByApplicationId(int? applicationId)
        {
            try
            {
                int id = applicationId ?? 0;
                return invitationBO.GetTokenByApplicationId(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return Guid.Empty;
        }

        public string GetEmailByApplicationId(int applicationId)
        {
            try
            {
                return invitationBO.GetEmailByApplicationId(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return string.Empty;
        }

        public ApplicationInvitation UpdateInvitationDatetime(int applicationId)
        {
            try
            {
                return invitationBO.UpdateInvitationDatetime(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
    }
}