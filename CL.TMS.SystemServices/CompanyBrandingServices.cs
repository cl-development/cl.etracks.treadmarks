﻿using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.SystemBLL;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;

namespace CL.TMS.SystemServices.CompanyBrandingServices
{
    public class CompanyBrandingServices : ICompanyBrandingServices
    {
        private readonly CompanyBrandingBO companyBrandingBO;

        public CompanyBrandingServices(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository, ICompanyBrandingRepository companyBrandingRepository)
        {
            this.companyBrandingBO = new CompanyBrandingBO(userRepository, vendorRepository, eventAggregator, messageRepository, companyBrandingRepository);
        }

        public TermAndConditionVM LoadDetailsByID(int applicationTypeID)
        {
            try
            {
                return companyBrandingBO.LoadDetailsByID(applicationTypeID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public string LoadTermsAndConditionContent(int? termsAndCondtionsID,string applicationType)
        {
            try
            {
                return companyBrandingBO.LoadTermsAndConditionsContent(termsAndCondtionsID, applicationType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public int GetCurrentTermsAndConditionID(string applicationType)
        {
            try
            {
                return companyBrandingBO.GetCurrentTermsAndConditionsID(applicationType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return 0;
        }
        public bool SaveTermCondition(TermAndConditionVM vm, long userId)
        {
            try
            {
                return companyBrandingBO.SaveTermCondition(vm, userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }   

        public InternalNoteViewModel AddNote(int transactionID, string notes)
        {
            try
            {
                return companyBrandingBO.AddNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> LoadNoteByID(int transactionID)
        {
            try
            {
                return companyBrandingBO.LoadNoteByID(transactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> ExportNotesToExcel(int transactionID, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return companyBrandingBO.ExportNotesToExcel(transactionID, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public CompanyInformationVM LoadCompanyInformation()
        {
            try
            {
                return companyBrandingBO.LoadCompanyInformation();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath)
        {
            try
            {
                companyBrandingBO.UpdateCompanyInformation(companyInformationVM, logoFilePath);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }          
        }

        public string LoadCompanyLogoUrl()
        {
            try
            {
                return companyBrandingBO.LoadCompanyLogoUrl();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }       
    }
}