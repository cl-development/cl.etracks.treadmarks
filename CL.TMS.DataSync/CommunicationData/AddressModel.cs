﻿using CL.TMS.DataContracts.Communication;
using System;
using System.Runtime.Serialization;

namespace CL.TMS.DataSync.CommunicationData
{
    /// <summary>
    /// AddressModel model class.
    /// Used for IN and OUT self-validated, data structure parameter by the web services.
    /// </summary>
    [DataContract]
    public class AddressModel : IAddressModel
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int AddressTypeID { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Province { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public Nullable<decimal> GPSCoordinate1 { get; set; } //latitude
        [DataMember]
        public Nullable<decimal> GPSCoordinate2 { get; set; }    //longitude 
       
    }
}
