﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using CL.TMS.DataSync.CommunicationData;
using CL.TMS.DataContracts.Communication;
using CL.TMS.Common.Enum;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.RegistrantServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling.Exceptions;
namespace CL.TMS.DataSync.ResponseMessages
{
    /// <summary>
    /// AppUser response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(CL.TMS.DataSync.CommunicationData.AppUserModel))]
    public class AppUserResponse : IAppUserMsg
    {
        #region IAppUserMsg Members

      
        /// <summary>
        /// Retrieves AppUser list updated after passed date (For OTS-Mobile)
        /// </summary>
        /// <param name="registrationNumber">Registration number.</param>
        public void GetAllUpdatedAppUsers(DateTime lastUpdate)
        {
            //IRegistrantService appUserService = new RegistrantService();
            //IList<AppUser> listOfAppUsers = appUserService.GetUpdatedAppUserList(lastUpdated);
      
            ////IAppUser appUserService = new AppUser();
            ////IList<IAppUser> listOfAppUsers = appUserService.GetUpdatedAppUserList(lastUpdated);
            //IList<AppUserModel> list = new List<AppUserModel>();

            //try
            //{ 
            //    if (listOfAppUsers != null)
            //    {
            //        foreach (var appUser in listOfAppUsers)
            //        {
            //            var appuser = new AppUserModel()
            //            {
            //                ID = appUser.ID,
            //                VendorID = appUser.VendorID,
            //                VendorAppUserID = appUser.VendorAppUserID,
            //                Number = appUser.Number,
            //                Name = appUser.Name,
            //                EMail = appUser.EMail,
            //                AccessTypeID = appUser.AccessTypeID,
            //                CreatedOn = appUser.CreatedOn,
            //                ModifiedOn = appUser.ModifiedOn,
            //                Metadata = appUser.Metadata,
            //                LastAccessOn = appUser.LastAccessOn,
            //                SyncOn = appUser.SyncOn,
            //                Active = appUser.Active
            //            };

            //            list.Add(appuser);
            //        }
            //    }
            //    UpdatedAppUserList = list;
            //    this.responseStatus = WebServiceResponseStatus.OK;
            //}
            //catch (ArgumentException aEx)
            //{
            //    this.responseMessages.Add(aEx.Message);
            //    this.responseStatus = WebServiceResponseStatus.ERROR;
            //}
            //catch (CLDuplicateWarningException ovEx)
            //{
            //    this.ResponseMessages.Add(ovEx.Message);
            //    this.responseStatus = WebServiceResponseStatus.WARNING;
            //}
            //catch (Exception)
            //{
            //    this.responseStatus = WebServiceResponseStatus.ERROR;
            //}
        }

        #endregion

        #region IResponseMessage<IAppUserModel> Members

        private IEnumerable<IAppUserModel> responseContent;
        public IEnumerable<IAppUserModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<IAppUserModel>();
                if (SingleEntity != null)
                    ((List<IAppUserModel>)this.responseContent).Add(SingleEntity);

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private WebServiceResponseStatus responseStatus = WebServiceResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (WebServiceResponseStatus)Enum.Parse(typeof(WebServiceResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public IAppUserModel SingleEntity { get; set; }

        [DataMember]
        public ICollection<CL.TMS.DataSync.CommunicationData.AppUserModel> UpdatedAppUserList { get; set; }
        
        #endregion
    }
}
