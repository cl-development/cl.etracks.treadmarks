﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;

namespace TreadMarks_Mobile.Data
{
    public class ScaleTicketMgr
    {
        private TM_Entities dbContext;
        public ScaleTicketMgr()
        {
            dbContext = new TM_Entities();
        }
        public ScaleTicket GetById(Guid transactionId)
        {
            var query = this.dbContext.ScaleTickets.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public ScaleTicket GetInboundById(Guid transactionId)
        {
            var query = this.dbContext.ScaleTickets.Where(t => t.transactionId == transactionId && t.scaleTicketTypeId == 1);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public ScaleTicket GetOutboundById(Guid transactionId)
        {
            var query = this.dbContext.ScaleTickets.Where(t => t.transactionId == transactionId && t.scaleTicketTypeId == 2);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<ScaleTicket> GetAllById(Guid transactionId)
        {
            return this.dbContext.ScaleTickets.Where(t => t.transactionId == transactionId).ToList();
        }

        public DateTime Save(IScaleTicketModel[] scaleTicketInfo)
        {
            try
            {
                DateTime syncDate;
                ScaleTicket entityExisting = GetById(scaleTicketInfo[0].TransactionId);
                if (entityExisting != null)
                    Delete(scaleTicketInfo[0].TransactionId);
                syncDate = DateTime.Now;
                foreach (IScaleTicketModel scaleTicket in scaleTicketInfo)
                {
                    ScaleTicket entity = null;
                    if (entity == null)
                    {
                        entity = new ScaleTicket();
                        this.dbContext.ScaleTickets.Add((ScaleTicket)entity);                       
                    }
                    entity.scaleTicketId = scaleTicket.ScaleTicketId;
                    entity.date = scaleTicket.Date.ToLocalTime();
                    entity.inboundWeight = scaleTicket.InboundWeight;
                    entity.outboundWeight = scaleTicket.OutboundWeight;
                    entity.photoId = scaleTicket.PhotoId;
                    entity.scaleTicketTypeId = scaleTicket.ScaleTicketTypeId;
                    entity.sortIndex = scaleTicket.SortIndex;
                    entity.ticketNumber = scaleTicket.TicketNumber;
                    entity.unitTypeId = scaleTicket.UnitTypeId;
                    entity.transactionId = scaleTicket.TransactionId;
                    entity.syncDate = syncDate;
                    this.dbContext.SaveChanges();                   
                }
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid transactionId)
        {
            this.dbContext.ScaleTickets.RemoveRange(this.dbContext.ScaleTickets.Where(t => t.transactionId == transactionId));
        }
    }
}
