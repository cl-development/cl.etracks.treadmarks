//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TreadMarks_Mobile.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegistrantType
    {
        public long registrantTypeId { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string descriptionKey { get; set; }
        public string fileName { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
        public string shortDescriptionKey { get; set; }
        public Nullable<System.DateTime> syncDate { get; set; }
    }
}
