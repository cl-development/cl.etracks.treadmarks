USE [TreadMarks-Mobile]
GO

/****** Object:  StoredProcedure [dbo].[DBCleanUp]    Script Date: 2/23/2016 11:25:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DBCleanUp]
AS
BEGIN

	SET NOCOUNT ON;

	delete from [Authorization];
	delete from [Comment];
	delete from [Document];
	delete from [GpsLog];
	delete from [Location];
	delete from [Message];
	delete from [Module];
	delete from [Photo];
	delete from [Registrant];
	delete from [ScaleTicket];
	delete from [STCAuthorization];
	delete from [Transaction];
	delete from [Transaction_Eligibility];
	delete from [Transaction_TireType];
	delete from [TransactionType_Module];
	delete from [TransactionType_RegistrantType];
	delete from [TransactionType_TireType];
	delete from [User];
	delete from [Transaction_MaterialType];
	delete from [TransactionType_MaterialType];
	delete from [Company];

END

GO

