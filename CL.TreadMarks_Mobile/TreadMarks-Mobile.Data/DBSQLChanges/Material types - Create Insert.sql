USE [TreadMarks-Mobile]
GO
/****** Object:  Table [dbo].[MaterialType]    Script Date: 12/8/2015 3:52:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialType](
	[materialTypeId] [bigint] NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[shortNameKey] [varchar](64) NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[effectiveStartDate] [datetime] NOT NULL,
	[effectiveEndDate] [datetime] NOT NULL,
	[syncDate] [datetime] NULL,
 CONSTRAINT [PK_Material] PRIMARY KEY CLUSTERED 
(
	[materialTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction_MaterialType]    Script Date: 12/8/2015 3:52:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction_MaterialType](
	[transactionMaterialTypeId] [uniqueidentifier] NOT NULL,
	[materialTypeId] [bigint] NOT NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
	[syncDate] [datetime] NULL,
 CONSTRAINT [PK_Transaction_MaterialType] PRIMARY KEY CLUSTERED 
(
	[transactionMaterialTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionType_MaterialType]    Script Date: 12/8/2015 3:52:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType_MaterialType](
	[transactionTypeMaterialTypeId] [bigint] NOT NULL,
	[materialTypeId] [bigint] NOT NULL,
	[transactionTypeId] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
 CONSTRAINT [PK_TransactionType_MaterialType] PRIMARY KEY CLUSTERED 
(
	[transactionTypeMaterialTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (1, 1, N'TDP5FT
', N'Made from Off-the-Road Whole Tires', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (2, 2, N'TDP5NT', N'Made from On-the-Road Whole Tires', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (3, 2, N'TD6NP
', N'"TD6NP, Made from Off-the-Road Whole Tires"
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (4, 2, N'TD6FP
', N'"TD6FP, Made from On-the-Road Whole Tires"
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (5, 2, N'Transfer-Fiber Rubber
', N'Fiber with Rubber sent to Processor 4000050 for extraction
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (6, 2, N'TDP1
', N'"TDP1, On road transfer, Rubber from Fiber."
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (7, 2, N'TDP2', N'"TDP2, On road transfer, Rubber from Fiber."
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[MaterialType] ([materialTypeId], [sortIndex], [shortNameKey], [nameKey], [effectiveStartDate], [effectiveEndDate], [syncDate]) VALUES (8, 2, N'TDP3', N'"TDP3, On road transfer, Rubber from Fiber."
', CAST(0x0000A41300000000 AS DateTime), CAST(0x002D247F00000000 AS DateTime), CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (1, 8, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (2, 3, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (3, 1, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (4, 6, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (5, 7, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (6, 4, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (7, 5, 4, CAST(0x0000A56800ED3412 AS DateTime))
INSERT [dbo].[TransactionType_MaterialType] ([transactionTypeMaterialTypeId], [materialTypeId], [transactionTypeId], [syncDate]) VALUES (8, 2, 4, CAST(0x0000A56800ED3412 AS DateTime))
