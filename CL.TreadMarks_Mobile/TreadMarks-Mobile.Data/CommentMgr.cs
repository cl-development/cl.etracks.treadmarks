﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;

namespace TreadMarks_Mobile.Data
{
    public class CommentMgr
    {
        private TM_Entities dbContext;
        public CommentMgr()
        {
            dbContext = new TM_Entities();
        }
        public Comment GetById(Guid commentId)
        {
            var query = this.dbContext.Comments.Where(t => t.commentId == commentId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<Comment> GetByTransactionId(Guid transactionId)
        {
            return this.dbContext.Comments.Where(t => t.transactionId == transactionId).ToList();    
        }

        public DateTime Save(ICommentModel commentInfo)
        {
            try
            {
                DateTime syncDate;
                Comment entity = GetById(commentInfo.commentId);
                syncDate = DateTime.Now;
                if (entity == null)
                {                   
                    entity = new Comment();
                    this.dbContext.Comments.Add((Comment)entity);
                }
                entity.commentId = commentInfo.commentId;
                entity.createdDate = commentInfo.createdDate.ToLocalTime();
                entity.text = commentInfo.text;
                entity.transactionId = commentInfo.transactionId;
                entity.syncDate = syncDate;
                entity.userId = commentInfo.UserId;               
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch
            {
                throw;
            }
        }

        //OTSM-1337 save comment array
        public DateTime Save(ICommentModel[] commentInfo)
        {
            try
            {
                DateTime syncDate;
                syncDate = DateTime.Now;
                foreach (ICommentModel comment in commentInfo)
                {

                    Comment entity = GetById(comment.commentId);
                    if (entity == null)
                    {
                        entity = new Comment();
                        this.dbContext.Comments.Add((Comment)entity);
                    }
                    entity.commentId = comment.commentId;
                    entity.createdDate = comment.createdDate.ToLocalTime();
                    entity.text = comment.text;
                    entity.transactionId = comment.transactionId;
                    entity.syncDate = syncDate;
                    entity.userId = comment.UserId;
                    this.dbContext.SaveChanges();
                }

                return syncDate;
            }
            catch
            {
                throw;
            }
        }

    }
}
