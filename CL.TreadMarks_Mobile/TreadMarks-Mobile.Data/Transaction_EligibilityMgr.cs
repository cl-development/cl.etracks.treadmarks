﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;

namespace TreadMarks_Mobile.Data
{
    public class Transaction_EligibilityMgr
    {
        private TM_Entities dbContext;
        public Transaction_EligibilityMgr()
        {
            dbContext = new TM_Entities();
        }
        public Transaction_Eligibility GetById(Guid transactionId)
        {
            var query = this.dbContext.Transaction_Eligibility.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<Transaction_Eligibility> GetAllById(Guid transactionId)
        {
            return this.dbContext.Transaction_Eligibility.Where(t => t.transactionId == transactionId).ToList();
        }

        public DateTime Save(ITransaction_EligibilityModel[] transactionInfoList)
        {
            try
            {
                DateTime syncDate;
                Transaction_Eligibility entityExisting = GetById(transactionInfoList[0].TransactionId);
                if (entityExisting != null)
                    Delete(transactionInfoList[0].TransactionId);
                syncDate = DateTime.Now;
                foreach (ITransaction_EligibilityModel transactionEligibilityInfo in transactionInfoList)
                {
                    Transaction_Eligibility entity = null;
                    if (entity == null)
                    {
                        entity = new Transaction_Eligibility();
                        this.dbContext.Transaction_Eligibility.Add((Transaction_Eligibility)entity);                       
                    }
                    entity.transactionEligibilityId = transactionEligibilityInfo.TransactionEligibilityId;
                    entity.eligibilityId = transactionEligibilityInfo.EligibilityId;
                    entity.syncDate = syncDate;
                    entity.value = transactionEligibilityInfo.Value;
                    entity.transactionId = transactionEligibilityInfo.TransactionId;                        
                    this.dbContext.SaveChanges();                   
                }
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid transactionId)
        {
            this.dbContext.Transaction_Eligibility.RemoveRange(this.dbContext.Transaction_Eligibility.Where(t => t.transactionId == transactionId));
        }
    }
}
