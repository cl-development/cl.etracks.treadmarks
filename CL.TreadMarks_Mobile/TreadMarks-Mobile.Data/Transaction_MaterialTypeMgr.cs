﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;

namespace TreadMarks_Mobile.Data
{
    public class Transaction_MaterialTypeMgr
    {
        private TM_Entities dbContext;
        public Transaction_MaterialTypeMgr()
        {
            dbContext = new TM_Entities();
        }
        public Transaction_MaterialType GetById(Guid transactionId)
        {
            var query = this.dbContext.Transaction_MaterialType.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<Transaction_MaterialType> GetAllByTransactionId(Guid transactionId)
        {
            return this.dbContext.Transaction_MaterialType.Where(t => t.transactionId == transactionId).ToList();            
        }

        public DateTime Save(ITransaction_MaterialTypeModel transactionInfo)
        {
            try
            {
                DateTime syncDate;
                //Transaction_MaterialType entityExisting = GetById(transactionInfoList[0].TransactionId);
                //if (entityExisting != null)
                    //Delete(transactionInfoList[0].TransactionId);
                syncDate = DateTime.Now;
                //foreach (ITransaction_MaterialTypeModel transactionMaterialTypeInfo in transactionInfoList)
                //{
                //    //Transaction_MaterialType entity = GetById(transactionInfo.MaterialTypeId);
                    Transaction_MaterialType entity = null;
                    if (entity == null)
                    {
                        entity = new Transaction_MaterialType();
                        this.dbContext.Transaction_MaterialType.Add((Transaction_MaterialType)entity);                       
                    }
                    entity.transactionMaterialTypeId = transactionInfo.TransactionMaterialTypeId;
                    entity.syncDate = syncDate;
                    entity.materialTypeId = transactionInfo.MaterialTypeId;
                    entity.transactionId = transactionInfo.TransactionId;                        
                    this.dbContext.SaveChanges();                   
                //}
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid transactionId)
        {
            this.dbContext.Transaction_MaterialType.RemoveRange(this.dbContext.Transaction_MaterialType.Where(t => t.transactionId == transactionId));
        }
    }
}
