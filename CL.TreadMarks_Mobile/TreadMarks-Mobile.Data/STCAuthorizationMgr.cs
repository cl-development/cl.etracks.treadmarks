﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class STCAuthorizationMgr
    {
        private TM_Entities dbContext;
        public STCAuthorizationMgr()
        {
            dbContext = new TM_Entities();
        }
        public Authorization GetAuthorizationById(Guid id)
        {
            var query = this.dbContext.Authorizations.Where(t => t.authorizationId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }
        public Authorization GetAuthorizationByTransactionId(Guid transactionId)
        {
            var query = this.dbContext.Authorizations.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }
        
        public STCAuthorization GetSTCAuthorizationById(Guid id)
        {
            var query = this.dbContext.STCAuthorizations.Where(t => t.authorizationId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<Authorization> GetList(DateTime appSyncDate)
        {
            var query = this.dbContext.Authorizations.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public DateTime? Save(ISTCAuthorizationModel authorizationInfo)
        {
            try
            {
                DateTime syncDate = DateTime.Now;

                //Save Authorization
                Authorization entityAuth = GetAuthorizationById(authorizationInfo.Authorization.authorizationId);
                if (entityAuth == null)
                {
                    entityAuth = new Authorization();
                    this.dbContext.Authorizations.Add((Authorization)entityAuth);
                }
                entityAuth.authorizationId = authorizationInfo.Authorization.authorizationId;
                entityAuth.authorizationNumber = authorizationInfo.Authorization.authorizationNumber;
                entityAuth.expiryDate = authorizationInfo.Authorization.expiryDate.HasValue ? (DateTime?) authorizationInfo.Authorization.expiryDate.Value.ToLocalTime() : null;
                entityAuth.transactionId = authorizationInfo.Authorization.transactionId;
                entityAuth.transactionTypeId = authorizationInfo.Authorization.transactionTypeId;
                entityAuth.userId = authorizationInfo.Authorization.userId;

                //Save Location if any
                if (authorizationInfo.Location != null)
                {
                    LocationMgr locMgr = new LocationMgr();
                    authorizationInfo.Location.IsUserGenerated = true;
                    locMgr.Save(authorizationInfo.Location);
                }

                //Save STCAuthorization
                STCAuthorization entitySTCAuth = GetSTCAuthorizationById(authorizationInfo.Authorization.authorizationId);
                if (entitySTCAuth == null)
                {
                    entitySTCAuth = new STCAuthorization();
                    this.dbContext.STCAuthorizations.Add((STCAuthorization)entitySTCAuth);
                }
             
                entitySTCAuth.authorizationId = authorizationInfo.Authorization.authorizationId;
                entitySTCAuth.locationId = authorizationInfo.Location != null ? (Guid?)authorizationInfo.Location.LocationId : null;

                entityAuth.syncDate = syncDate;
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
