﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class LocationMgr
    {
        private TM_Entities dbContext;
        public LocationMgr()
        {
            dbContext = new TM_Entities();
        }
        public Location GetById(Guid id)
        {
            var query = this.dbContext.Locations.Where(t => t.locationId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<Location> GetList(DateTime lastUpdated)
        {
            // User generated locations are not eligible for pull
            var query = this.dbContext.Locations.Where(t => t.syncDate > lastUpdated && t.isUserGenerated == false);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }
        public ILocationModel Save(ILocationModel locationInfo)
        {
            try
            {
                Location entity = GetById(locationInfo.LocationId);
                if (entity == null)
                {
                    entity = new Location();
                    this.dbContext.Locations.Add((Location)entity);
                }                
                entity.address1 = locationInfo.Address1;
                entity.address2 = locationInfo.Address2;
                entity.address3 = locationInfo.Address3;
                entity.city = locationInfo.City;
                entity.country = locationInfo.Country;
                entity.fax = locationInfo.Fax;
                entity.latitude = locationInfo.Latitude;
                entity.locationId = locationInfo.LocationId;
                entity.longitude = locationInfo.Longitude;
                entity.name = locationInfo.Name;
                entity.phone = locationInfo.Phone;
                entity.postalCode = locationInfo.PostalCode;
                entity.province = locationInfo.Province;
                entity.syncDate = DateTime.Now;
                entity.isUserGenerated = locationInfo.IsUserGenerated;
                this.dbContext.SaveChanges();
                locationInfo.SyncDate = entity.syncDate;
                return locationInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
