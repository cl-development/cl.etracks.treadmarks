﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;
//using TreadMarks_MobileToTM;
using TreadMarksContracts.Common.Enums;
using System.Collections;
using TreadMarks_MobileToTM;
using System.Configuration;

namespace TreadMarks_Mobile.Data
{
    public class TransactionMgr
    {
        private TM_Entities dbContext;
        public TransactionMgr()
        {
            dbContext = new TM_Entities();
        }
        public Transaction GetById(System.Guid id)
        {
            var query = this.dbContext.Transactions.Where(t => t.transactionId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<Transaction> GetList()
        {
            var query = this.dbContext.Transactions;
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }
        public long GetTransactionSyncStatusById(System.Guid transactionId)
        {
            var query = this.dbContext.Transactions.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return 0;
            else
                return query.First().transactionSyncStatusTypeId;
        }
        public ITransactionModel Save(ITransactionModel transactionInfo)
        {
            try
            {
                Transaction entity = GetById(transactionInfo.TransactionID);

                if (entity == null)
                {
                    entity = new Transaction();
                    this.dbContext.Transactions.Add((Transaction)entity);
                }

                entity.createdUserId = transactionInfo.CreatedUserId;
                entity.createdDate = transactionInfo.CreatedDate.ToLocalTime();
                entity.modifiedUserId = transactionInfo.ModifiedUserId;
                entity.modifiedDate = transactionInfo.ModifiedDate.HasValue ? (DateTime?) transactionInfo.ModifiedDate.Value.ToLocalTime() : null;
                entity.transactionId = transactionInfo.TransactionID;
                entity.transactionStatusTypeId = transactionInfo.TransactionStatusTypeId;
                entity.transactionSyncStatusTypeId = transactionInfo.TransactionSyncStatusTypeId;
                entity.transactionTypeId = transactionInfo.TransactionTypeId;
                entity.transactionDate = transactionInfo.TransactionDate.ToLocalTime();
                entity.friendlyId = transactionInfo.FriendlyId;
                entity.incomingGpsLogId = transactionInfo.IncomingGpsLogId;
                entity.outgoingGpsLogId = transactionInfo.OutgoingGpsLogId;
                entity.incomingRegistrationNumber = transactionInfo.RegistrationNumber;
                entity.incomingSignatureName = transactionInfo.IncomingSignatureName;
                entity.incomingSignaturePhotoId = transactionInfo.IncomingSignaturePhotoId;
                entity.outgoingSignaturePhotoId = transactionInfo.OutgoingSignaturePhotoId;
                entity.outgoingSignatureName = transactionInfo.OutgoingSignatureName;
                entity.incomingUserId = transactionInfo.IncomingUserId;
                entity.outgoingUserId = transactionInfo.OutgoingUserId;
                entity.syncDate = DateTime.Now;
                entity.trailerLocationId = transactionInfo.TrailerLocationId;
                entity.trailerNumber = transactionInfo.TrailerNumber;
                entity.postalCode1 = transactionInfo.PostalCode1;
                entity.postalCode2 = transactionInfo.PostalCode2;
                entity.deviceName = transactionInfo.DeviceName;
                entity.outgoingRegistrationNumber = transactionInfo.OutgoingRegistrationNumber;
                entity.responseComments += transactionInfo.ResponseComments;
                entity.versionBuild = transactionInfo.VersionBuild;
                entity.deviceId = transactionInfo.DeviceId;

                if (transactionInfo.Company != null)
                {
                    CompanyMgr companyMgr = new CompanyMgr();
                    companyMgr.Save(transactionInfo.Company);
                }              
                this.dbContext.SaveChanges();
                transactionInfo.SyncDate = entity.syncDate;
                return transactionInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ITransactionModel PushToTM(ITransactionModel t)
        {
            List<string> responseMessages = new List<string>();
            // For UCR, to save company information
            this.Save(t);
            try
            {
                Communicator comm = new Communicator(ConfigurationManager.AppSettings["BaseAddress"], ConfigurationManager.AppSettings["ServiceUserName"], ConfigurationManager.AppSettings["ServicePassword"]);
                TreadMarks_MobileToTM.CommunicationSrv.IResponseMessage<object> result = comm.Push(t);

                foreach (string message in result.ResponseMessages)
                    responseMessages.Add(DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + " " + message);

                if (result.ResponseStatus == (int)WSResponseStatus.OK || result.ResponseStatus == (int)WSResponseStatus.WARNING)
                {
                    //Done it to pass the case where completed transaction is already pushed and device is sent back OK without pushing
                    if (t.TransactionStatusTypeId == (int)TransactionStatuses.COMPLETE)
                        t.TransactionSyncStatusTypeId = (int)SyncStatuses.Repository_Sync_TM;
                    else
                        //Cant use Repository_Sent_To_TM coz it shows up as Red on device
                        t.TransactionSyncStatusTypeId = (int)SyncStatuses.Device_Sync_Repository;

                    responseMessages.Add(DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + " Success");
                }
                else
                    t.TransactionSyncStatusTypeId = (int)SyncStatuses.Repository_Sync_Fail_TM;
               
            }
            catch (Exception ex)
            {
                t.TransactionSyncStatusTypeId = (int)SyncStatuses.Repository_Sync_Fail_TM;
                responseMessages.Add(DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + ex.Message);
            }

            //Save validation messages in comments field
            return SaveTransactionResult(t, responseMessages);
        }

        private ITransactionModel SaveTransactionResult(ITransactionModel t, List<string> responseMessages)
        {
            t.ResponseComments = string.Join("<br />", responseMessages.ToArray());
            return this.Save(t);
        }

        //Had to change it to list (putting all in memory) as an enumrable won't allow me to use more than one transactions to update 
        public List<Transaction> GetSync_Fail_TMList()
        {
            //var query = this.dbContext.Transactions.Where(t => t.transactionStatusTypeId == (int)TransactionStatuses.COMPLETE
              //  && t.transactionSyncStatusTypeId == (int)SyncStatuses.Repository_Sync_Fail_TM && t.friendlyId == 766386296);

            var query = this.dbContext.Transactions.Where(t => t.transactionStatusTypeId == (int)TransactionStatuses.COMPLETE
              && t.friendlyId == 766386296);
            
            if (query.Count() == 0)
                return null;
            else
                return query.ToList();
        }    
        
    }
}
