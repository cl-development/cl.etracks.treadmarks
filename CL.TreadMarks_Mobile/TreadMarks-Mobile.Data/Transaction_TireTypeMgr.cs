﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;
using System.Data.OleDb;

namespace TreadMarks_Mobile.Data
{
    public class Transaction_TireTypeMgr
    {
        private TM_Entities dbContext;
        public Transaction_TireTypeMgr()
        {
            dbContext = new TM_Entities();
        }
        public Transaction_TireType GetById(Guid transactionId)
        {
            var query = this.dbContext.Transaction_TireType.Where(t => t.transactionId == transactionId);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<Transaction_TireType> GetAllByTransactionId(Guid transactionId)
        {
            return this.dbContext.Transaction_TireType.Where(t => t.transactionId == transactionId).ToList();            
        }

        public DateTime Save(ITransaction_TireTypeModel[] transactionInfoList)
        {
            try
            {
                DateTime syncDate;
                Transaction_TireType entityExisting = GetById(transactionInfoList[0].TransactionId);
                if (entityExisting != null)
                    Delete(transactionInfoList[0].TransactionId);
                syncDate = DateTime.Now;
                foreach (ITransaction_TireTypeModel transactionTireTypeInfo in transactionInfoList)
                {
                    //Transaction_TireType entity = GetById(transactionInfo.tireTypeId);
                    Transaction_TireType entity = null;
                    if (entity == null)
                    {
                        entity = new Transaction_TireType();
                        this.dbContext.Transaction_TireType.Add((Transaction_TireType)entity);                       
                    }
                    entity.transactionTireTypeId = transactionTireTypeInfo.TransactionTireTypeId;
                    entity.quantity = transactionTireTypeInfo.Quantity;
                    entity.syncDate = syncDate;
                    entity.tireTypeId = transactionTireTypeInfo.TireTypeId;
                    entity.transactionId = transactionTireTypeInfo.TransactionId;                        
                    this.dbContext.SaveChanges();                   
                }
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid transactionId)
        {
            this.dbContext.Transaction_TireType.RemoveRange(this.dbContext.Transaction_TireType.Where(t => t.transactionId == transactionId));
        }
    }
}
