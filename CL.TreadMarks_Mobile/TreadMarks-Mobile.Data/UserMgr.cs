﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class UserMgr
    {
        private TM_Entities dbContext;
        public UserMgr()
        {
            dbContext = new TM_Entities();
        }
        public User GetById(long id)
        {
            var query = this.dbContext.Users.Where(t => t.userId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<User> GetList(DateTime appSyncDate)
        {
            var query = this.dbContext.Users.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public DateTime? Save(IUserModel userInfo)
        {
            try
            {
                DateTime syncDate = DateTime.Now;
                User entity = GetById(userInfo.UserId);
                if (entity == null)
                {
                    entity = new User();
                    this.dbContext.Users.Add((User)entity);
                }
                entity.userId = userInfo.UserId;
                entity.accessTypeId = userInfo.AccessTypeId;
                entity.createdDate = userInfo.CreatedDate.HasValue ? (DateTime?)userInfo.CreatedDate.Value : null;
                entity.email = userInfo.Email;
                entity.lastAccessDate = userInfo.LastAccessDate.HasValue ? (DateTime?) userInfo.LastAccessDate.Value : null;
                //entity.metadata = userInfo.Metadata;
                entity.modifiedDate =  userInfo.ModifiedDate.HasValue ? (DateTime?) userInfo.ModifiedDate.Value : null;
                entity.name = userInfo.Name;
                entity.registrationNumber = userInfo.RegistrationNumber;
                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DateTime GetMaxLastUpdatedDate()
        {
            return this.dbContext.Users.Max(t => t.syncDate) != null ? this.dbContext.Users.Max(t => t.syncDate).Value : new DateTime(2000, 01, 01);
        }

        public DateTime? SaveAppUser(AppUserModel appUser)
        {
            try
            {
                DateTime syncDate = DateTime.Now;
                User entity = GetByIdAndReg(appUser.VendorAppUserID, Convert.ToInt32(appUser.Number.Split('-')[0]));
                if (entity == null)
                {
                    entity = new User();
                    this.dbContext.Users.Add((User)entity);
                }
                entity.userId = appUser.VendorAppUserID;
                //Device understands only 3 so take it as 3 
                //entity.accessTypeId = appUser.AccessTypeID;
                entity.accessTypeId = 3;
                entity.createdDate = appUser.CreatedOn;
                entity.email = appUser.EMail ?? "";
                entity.lastAccessDate = appUser.LastAccessOn;
                //Discussed with Aditya (Mobile team lead) to store alphanumeric number in metadata 2014-04-24
                //Device is not programmed yet to handle metadata so commenting it 2015-0427
                //entity.metadata = appUser.Number;
                entity.modifiedDate = appUser.ModifiedOn;
                entity.name = appUser.Name ?? "";
                
                //Alphanumeric handling not done on device so need this stupid code                                
                //entity.registrationNumber = appUser.VendorID;
                entity.registrationNumber = Convert.ToDecimal(appUser.Number.Split('-')[0]);
                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetByIdAndReg(long id, int reg)
        {
            var query = this.dbContext.Users.Where(t => t.userId == id && t.registrationNumber == reg);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }


    }
}
