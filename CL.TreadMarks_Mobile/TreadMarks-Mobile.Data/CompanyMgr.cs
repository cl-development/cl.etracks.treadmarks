﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class CompanyMgr
    {
        private TM_Entities dbContext;
        public CompanyMgr()
        {
            dbContext = new TM_Entities();
        }
        public Company GetById(int id)
        {
            var query = this.dbContext.Companies.Where(t => t.companyId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<Company> GetList(DateTime lastUpdated)
        {
            var query = this.dbContext.Companies.Where(t => t.syncDate > lastUpdated);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }
        public ICompanyModel Save(ICompanyModel CompanyInfo)
        {
            try
            {
                Company entity = GetById(CompanyInfo.CompanyId);
                if (entity == null)
                {
                    entity = new Company();
                    this.dbContext.Companies.Add((Company)entity);
                }                
                entity.address1 = CompanyInfo.Address1;
                entity.address2 = CompanyInfo.Address2;
                entity.address3 = CompanyInfo.Address3;
                entity.city = CompanyInfo.City;
                entity.country = CompanyInfo.Country;
                entity.name = CompanyInfo.Name;
                entity.phone = CompanyInfo.Phone;
                entity.postalCode = CompanyInfo.PostalCode;
                entity.province = CompanyInfo.Province;
                entity.transactionId = CompanyInfo.TransactionId;
                entity.syncDate = DateTime.Now;
                this.dbContext.SaveChanges();
                CompanyInfo.SyncDate = entity.syncDate;
                return CompanyInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
