﻿using TreadMarksContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreadMarks_MobileToTM.ServiceResponseModels
{
    public class AppUserResponse
    {
        public int Code { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<AppUserModel> Data { get; set; }
    }
}