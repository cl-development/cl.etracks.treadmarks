﻿using TreadMarksContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreadMarks_MobileToTM.ServiceResponseModels
{
    public class VendorResponse
    {
        public int Code { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<VendorModel> Data { get; set; }
    }
}