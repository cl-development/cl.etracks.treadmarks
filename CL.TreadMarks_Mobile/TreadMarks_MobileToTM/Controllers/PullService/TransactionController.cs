﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_MobileToTM.CommunicationSrv;
using TreadMarks_MobileToTM.ServiceResponseModels;
using TreadMarksContracts;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

namespace TreadMarks_MobileToTM.Controllers
{
    public class TransactionController
    {
        #region Properties

        public string BaseAddress { get; set; }

        #endregion

        #region Constructors

        public TransactionController(string baseAddress)
        {
            BaseAddress = baseAddress + "api/v1/";
        }

        #endregion

        #region Methods      
    
        public class TransactionModel
        {
            public Guid Id { set; get; }
        }


        public async Task<IResponseMessage<object>> SaveTrans(Guid transactionId, string token)
        {
            BooleanResponse result = new BooleanResponse();
      
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);  
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                
                //Commenting Token as remote host is not working due to this            
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                //var response = client.GetAsync(string.Format("Transaction/SaveTransaction?input={0}", lastUpdated.ToString())).Result;


                var input = new TransactionModel()
                {
                    Id = transactionId
                };//new TransactionModelExtended();

                string jsonData = JsonConvert.SerializeObject(input);//string.Format("={0}", JsonConvert.SerializeObject(input));
                var content = new StringContent(
                        jsonData,
                        Encoding.UTF8,
                        "application/json");//application/x-www-form-urlencoded

                var response = await client.PostAsync("Transactions/SaveTransaction", content);
              
                //resp.EnsureSuccessStatusCode();    // Throw if not a success code. 
                    //if (response.IsSuccessStatusCode)
                    {
                        //var message1 = await response.Content.ReadAsStringAsync().Result;
                    }    
                    
                var message = response.Content.ReadAsStringAsync().Result;
                var wsResult = JsonConvert.DeserializeObject<VendorResponse>(message);

                result.ResponseContent = wsResult.Data;
                result.ResponseMessages = new List<string>() { wsResult.Message }; 
                result.ResponseStatus = wsResult.Code;

                return result;
            }
        }

        #endregion
    }
}
