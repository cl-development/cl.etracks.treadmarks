﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using WS = OTS_MobileToTM.RegistrantServiceReference;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;

namespace OTS_MobileToTM.Controllers
{
    public class RegistrantController : IRegistrantController
    {
        #region Properties

        private WS.RegistrantCommunicationClient client;

        public ServiceCredentials cred { get; set; }

        #endregion

        #region Constructors

        public RegistrantController()
        {
            new RegistrantController(string.Empty, string.Empty);
        }

        public RegistrantController(string username, string password)
        {
            client = new WS.RegistrantCommunicationClient();
            cred = new ServiceCredentials();
            cred.RegistrantServiceCredentials.UserName = username;
            cred.RegistrantServiceCredentials.Password = password;
        }

        #endregion

        #region Methods

        public IResponseMessage<object> GetRegistrantByRegNumber(ITransactionModel transaction)
        {
            WS.RegistrantResponse response = (WS.RegistrantResponse)client.GetRegistrantByRegNumber(Convert.ToString(transaction.RegistrationNumber), cred.RegistrantServiceCredentials);

            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = null;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = response.SingleEntity;

            return result;
        }

        public IEnumerable<IRegistrantModelExtended> GetUpdatedRegistrantList(DateTime lastUpdated)
        {
            WS.RegistrantResponse response = (WS.RegistrantResponse)client.GetUpdatedRegistrantList(lastUpdated, cred.RegistrantServiceCredentials);

            IEnumerable<IRegistrantModelExtended> regList = new List<IRegistrantModelExtended>();
            if (response.UpdatedRegistrantList != null)
                regList = (from reg in response.UpdatedRegistrantList
                           select new RegistrantModel()
                           {
                               BusinessName = reg.BusinessName,
                               RegistrationNumber = reg.RegistrationNumber,
                               ActivationDate = reg.ActivationDate,
                               IsActive = reg.IsActive,
                               ActiveState = reg.ActiveState,
                               ActiveStateChangeDate = reg.ActiveStateChangeDate,
                               LastUpdatedDate = reg.LastUpdatedDate,
                               LocationAddress1 = reg.LocationAddress1,
                               LocationAddress2 = reg.LocationAddress2,
                               LocationAddress3 = reg.LocationAddress3,
                               LocationFax = reg.LocationFax,
                               LocationPhone = reg.LocationPhone,
                               PostalCode = reg.PostalCode,
                               Province = reg.Province,
                               Country = reg.Country,
                               City = reg.City,
                               CreatedDate = reg.CreatedDateTS,
                               RegistrantTypeId = reg.RegistrantTypeID
                           }).ToList();

            return regList;
        }
        #endregion
    }
}
