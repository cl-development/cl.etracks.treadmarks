﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.HaulerServiceReference;

namespace OTS_MobileToTM.Controllers
{
    interface IHaulerClaimSummaryController : IController
    {

        IResponseMessage<object> GetHaulerClaimsByFilter(ITransactionModel transaction, string recordStatus);

        IResponseMessage<object> GetHaulerClaimsByFilter(DateTime? claimStartDate, DateTime? claimEndDate, string registrationNumber, string recordStatus);

        IResponseMessage<object> GetHaulerClaimMostRecent(string registrationNumber, DateTime? claimStartDate, DateTime? claimEndDate);
        IResponseMessage<object> Insert(ITransactionModelExtended model);

        IResponseMessage<object> GetHaulerClaimByID(int haulerSummaryId);

        void Close();

    }
}
