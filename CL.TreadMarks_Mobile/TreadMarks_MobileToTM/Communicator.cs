﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarksContracts;
using TreadMarks_MobileToTM.CommunicationSrv;
using TreadMarks_MobileToTM.ServiceResponseModels;
using TreadMarks_MobileToTM.Controllers;
using TreadMarksContracts.Common.Enums;
using System.Net.Http;
using Newtonsoft.Json;

namespace TreadMarks_MobileToTM
{
    public enum WebServiceResponseStatus
    {
        NONE = 0,
        OK = 1,
        ERROR = 2,
        WARNING = 3

    }

    public class Communicator : ICommunicator
    {
        #region Properties

        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }
        private string _baseAddress;
        public string BaseAddress
        {
            get
            {
                return _baseAddress;
            }
            set
            {
                _baseAddress = value;
            }
        }

        string Token = string.Empty;

        private static readonly Dictionary<int, Object> _lockers = new Dictionary<int, object>();

        #endregion

        #region Properties Controllers

        private readonly IPullServiceController _pullServiceController;
        private readonly TransactionController _transactionController;
        
        #endregion

        #region Constants

        private const string PREPAREDBY = "System";
    
        #endregion

        #region Constructors
        public Communicator()
        {
            Username = string.Empty;
            Password = string.Empty;
           
        }
        public Communicator(string baseAddress, string username, string password)
        {
            Username = username;
            Password = password;
            BaseAddress = baseAddress;
            _pullServiceController = new PullServiceController(BaseAddress);
            _transactionController = new TransactionController(BaseAddress);
            //Commenting Token as remote host is not working due to this
            Token = GetTokenDictionary(Username, Password);
        }

        #endregion

        #region Methods


        string GetTokenDictionary(string userName, string password)
        {
            var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>( "grant_type", "password" ), 
                    new KeyValuePair<string, string>( "username", userName ), 
                    new KeyValuePair<string, string> ( "Password", password )
                };
            var content = new FormUrlEncodedContent(pairs);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);  
                var response = client.PostAsync("Token", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                // Deserialize the JSON into a Dictionary<string, string>
                Dictionary<string, string> tokenDictionary =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                return tokenDictionary["access_token"];
            }
        }     
      
        public bool Validate(ref string[] errors)
        {
            //not yet implemented
            //can add any validations here!
            List<string> errorList = new List<string>();
            errors = errorList.ToArray();
            return (errorList.ToArray().Length == 0);
        }     

        #endregion

        #region Methods

        public IResponseMessage<object> Push(ITransactionModel transaction)
        {
            lock (GetLocker(Convert.ToInt32(transaction.RegistrationNumber)))
            {
                var result = SaveTrans(transaction.TransactionID);
                if (result.ResponseStatus == (int)WebServiceResponseStatus.ERROR)
                {
                    //Rollback
                    return result;
                }
                return result;
            }
        }

        private IResponseMessage<object> SaveTrans(Guid transactionId)
        {
            return _transactionController.SaveTrans(transactionId, Token).Result;
        }

        public IEnumerable<AppUserModel> GetUpdatedAppUsersList(DateTime lastUpdated)
        {
            return _pullServiceController.GetUpdatedAppUsersList(lastUpdated, Token);
        }
        public IEnumerable<VendorModel> GetUpdatedVendorsList(DateTime lastUpdated)
        {
            return _pullServiceController.GetUpdatedVendorsList(lastUpdated, Token);
        }

        /// <summary>
        /// returns a lock key based on the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private object GetLocker(int id)
        {
            lock (_lockers)
            {
                if (!_lockers.ContainsKey(id))
                    _lockers.Add(id, new object());
                return _lockers[id];
            }
        }


        //outgoing if filename is like xxxx-1.png
        private bool isOutgoingRegistrant(string fullfilePath)
        {
            string fileName = System.IO.Path.GetFileNameWithoutExtension(fullfilePath);
            string isOutgoingRegistrant = fileName.Substring(fileName.Length - 1, 1);
            return isOutgoingRegistrant == "1";
        }

        #endregion

    }
}
