﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_MobileToTM.CommunicationSrv;
using TreadMarksContracts;
using WSR = TreadMarks_MobileToTM.PullService;

namespace TreadMarks_MobileToTM
{
    public class ServiceCredentials
    {    
        private WSR.ServiceCredentials _pullServiceCredentials;
        public WSR.ServiceCredentials PullServiceCredentials
        {
            get
            {
                return _pullServiceCredentials;
            }
            set
            {
                _pullServiceCredentials = value;
            }
        }


        public ServiceCredentials()
        {
            _pullServiceCredentials = new WSR.ServiceCredentials();
        }
    }
}
