﻿using System;
using System.Diagnostics;

namespace CustomTraceListeners
{
	public class EmailTraceListener : TraceListener
	{
		ITraceEmailer _traceEmailer;

		#region overrides
		public override void Write(string message)
		{
            //Stop sending >>>> Logger Error: 2 : <<<< in separate emails
			//CurrentTraceEmailer.SendTraceEmail(message);
		}

		public override void WriteLine(string message)
		{
			CurrentTraceEmailer.SendTraceEmail(message);
		}

		protected override string[] GetSupportedAttributes()
		{
			return new string[] { "fromAddress", "toAddress", "subject" };
		}
		#endregion

		#region properties
		public ITraceEmailer CurrentTraceEmailer
		{
			get
			{
				if (_traceEmailer == null)
				{
					_traceEmailer = new TraceEmailer(
						Attributes["fromAddress"],
						Attributes["toAddress"],
						Attributes["subject"]);
				}

				return _traceEmailer;
			}
		}
		#endregion 
	}
}
