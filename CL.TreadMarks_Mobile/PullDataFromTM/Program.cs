﻿using TreadMarks_Mobile.Data;
using TreadMarks_MobileToTM;
using TreadMarksContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PullDataFromTM
{
    class Program : BaseProgram
    {
        Program(string[] args)
            : base(args)
        {
        }
        static int Main(string[] args)
        {
            return Main(new Program(args));
        }
        protected override void Run()
        {
            Communicator c = new Communicator(ConfigurationManager.AppSettings["BaseAddress"], ConfigurationManager.AppSettings["ServiceUserName"], ConfigurationManager.AppSettings["ServicePassword"]);
            UpdateRegistrants(c); 
            UpdateAppUsers(c);       
            //SaveTrans(c);
        }

        private static void SaveTrans(Communicator c)
        {
           //System.Console.WriteLine(c.SaveTrans());
           //System.Console.ReadKey();
        }


        private static void UpdateRegistrants(Communicator c)
        {
            RegistrantMgr registrantMgr = new RegistrantMgr();

            IEnumerable<VendorModel> vendorList = c.GetUpdatedVendorsList(registrantMgr.GetMaxLastUpdatedDate());

            foreach (VendorModel vendor in vendorList)
            {
                try
                {
                    Console.WriteLine("Updating Vendor: " + vendor.Number.ToString());
                    registrantMgr.SaveRegistrantWithLocation(vendor);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        private static void UpdateAppUsers(Communicator c)
        {
            UserMgr userMgr = new UserMgr();
            IEnumerable<AppUserModel> appUserList = c.GetUpdatedAppUsersList(userMgr.GetMaxLastUpdatedDate());

            foreach (AppUserModel appUser in appUserList)
            {
                try
                {
                    Console.WriteLine("Updating AppUser: " + appUser.Number.ToString());
                    userMgr.SaveAppUser(appUser);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }
    }
}
