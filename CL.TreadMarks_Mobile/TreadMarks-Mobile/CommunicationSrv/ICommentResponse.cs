﻿using System;
using TreadMarksContracts;
using System.IO;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarksContracts.CommunicationSrv
{
    /// <summary>
    /// Communication entity for ITransactionResponseMsg
    /// * Defines the applicable actions and returned data collection of a web service call(s).
    /// </summary>
    public interface ICommentResponse : IResponseMessage<CommentModel>
    {
        void SyncComment(ICommentModel commentInfo);             
    }
}
