﻿using System;
using TreadMarks_Mobile.CommunicationData;
using System.IO;

namespace TreadMarksContracts.CommunicationSrv
{
    /// <summary>
    /// Communication entity for ITransactionResponseMsg
    /// * Defines the applicable actions and returned data collection of a web service call(s).
    /// </summary>
    public interface ITransaction_EligibilityResponse : IResponseMessage<Transaction_EligibilityModel>
    {
        void SyncTransaction_Eligibility(Transaction_EligibilityModel[] transaction_EligibilityList);             
    }
}
