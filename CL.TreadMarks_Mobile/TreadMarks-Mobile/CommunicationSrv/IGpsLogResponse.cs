﻿using System;
using TreadMarks_Mobile.CommunicationData;
using System.IO;

namespace TreadMarksContracts.CommunicationSrv
{
    /// <summary>
    /// Communication entity for ITransactionResponseMsg
    /// * Defines the applicable actions and returned data collection of a web service call(s).
    /// </summary>
    public interface IGpsLogResponse : IResponseMessage<GpsLogModel>
    {
        void SyncGpsLog(IGpsLogModel[] gpsLogInfo);             
    }
}
