﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="name">
    <TABLE width="100%" border="0" cellspacing="0" cellpadding="10" bgcolor="#7aa83e">
      <TR>
        <TD align="center">
          <H1>
            <xsl:text>TreadMarks_Mobile Help Page</xsl:text>
          </H1>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>
  
  <xsl:template match="doc">
    <xsl:apply-templates select="./assembly/name" />
    <H2>
      APIs
    </H2>
    <DL>
      <xsl:apply-templates select="./members/member[starts-with(@name,'T:TreadMarks_Mobile.TransactionCommunication')]" />
    </DL>    
  </xsl:template>
  <xsl:template match="member[starts-with(@name,'T:TreadMarks_Mobile.TransactionCommunication')]">
    <DT>
      <A>
        <xsl:attribute name="href">
          HelpPageMemberList.aspx?Type=<xsl:value-of select="substring-after(@name,':')" />
        </xsl:attribute>
        <B>
          <xsl:value-of select="substring-after(@name,'.')" />
        </B>
      </A>
    </DT>

    <DD>
      <xsl:apply-templates select="remarks | summary" />
    </DD>
    <P></P>
  </xsl:template>
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
