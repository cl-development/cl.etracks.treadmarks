﻿using System.Xml;
using System.Xml.Xsl;
using System;
using System.Configuration;

namespace TreadMarks_Mobile
{
    /// <remarks>
    /// Dhana test: Class that contains functions to do transformations to help files. hahaha
    /// The source XML is loaded into the <see cref="SourceXML"/> property (e.g. 
    /// <c><I>obj</I>.SourceXML = "<I>XML goes here</I>"</c>). One of the 
    /// associated member functions (<see cref="GiveTypeListHTMLHelp"/>,
    /// <see cref="GiveMemberListHTMLHelp"/>, <see cref="GiveMemberHTMLHelp"/>) 
    /// is called to initiate and then return the transformation.
    /// <para>
    /// <list type="table">
    /// <listheader>
    /// <term>Help Page</term>
    /// <description>Function to call</description>
    /// </listheader>
    /// <item><term>List of Types</term>
    /// <description>GiveTypeListHTMLHelp</description></item>
    /// <item><term>List of members</term>
    /// <description>GiveMemberListHTMLHelp</description></item>
    /// <item><term>Help for a single member</term>
    /// <description>GiveMemberHTMLHelp</description></item>
    /// </list>
    /// </para>
    /// </remarks>
    /// <permission cref="System.Security.PermissionSet">public</permission>
    /// <example><code>
    /// // create the class that does translations
    /// GiveHelp.GiveHelpTransforms ght = new GiveHelp.GiveHelpTransforms();
    /// // have it load our XML into the SourceXML property
    /// ght.LoadXMLFromFile("C:\\Inetpub\\wwwroot\\GiveHelp\\GiveHelpDoc.xml");
    ///
    /// // do the translation and then write out the string
    /// Response.Write( ght.GiveMemberHTMLHelp(Request.QueryString.Get("Type"),
    /// Request.QueryString.Get("Member")) );
    /// </code></example>
    public class HelpPageTransform
    {
        /// <summary>
        /// The constructor does nothing special and is here for example.
        /// </summary>
       
        /// <summary>
        /// This XmlDocument based attribute contains the 
        /// XML documentation for the project.
        /// </summary>
        /// <permission cref="System.Security.PermissionSet">private</permission>
        private XmlDocument m_xdSourceXML = new XmlDocument();

        /// <value>
        /// The SourceXML property contains the XML that will be used in the
        /// transformations by the member functions for this class.
        /// </value>
        /// <permission cref="System.Security.PermissionSet">public</permission>
        public string SourceXML
        {
            get
            {
                return m_xdSourceXML.InnerXml;
            }
            set
            {
                m_xdSourceXML.LoadXml(SourceXML);
            }
        }

        public void LoadXMLFromFile(string strFilePath)
        {
            m_xdSourceXML.Load(strFilePath);
        }

        public string HelpAPIListHTMLPage()
        {
            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageAPI.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                    new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            xslTransformFile.Transform(m_xdSourceXML, null, tw);

            return tw.ToString();
        }

        public string HelpMemberListHtmlPage(string strType)
        {
            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageMemberList.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                    new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            // the strType parameter is passed into the stylesheet
            XsltArgumentList arglist = new XsltArgumentList();
            arglist.AddParam("WhichType", "", strType);

            xslTransformFile.Transform(m_xdSourceXML, arglist, tw);

            return tw.ToString();
        }

        public string HelpMemberHTMLHelp(string strMember)
        {
            // A small kludge to get around an encoding problem.
            // #ctor gets striped as it looks like a bookmark.
            // If we get a member that ends in '.', we append the constructor 
            // value
            if (strMember.EndsWith(".") == true)
            {
                strMember += "#ctor";
            }

            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageMember.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            // the strType and strMember parameters is passed into the stylesheet
            XsltArgumentList arglist = new XsltArgumentList();
            arglist.AddParam("WhichMember", "", strMember);

            xslTransformFile.Transform(m_xdSourceXML, arglist, tw);

            return tw.ToString();
        }

    }
}