﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TreadMarks_Mobile.HelpPage
{
    public partial class HelpPageAPI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // create the class that does translations

            HelpPageTransform hpt = new HelpPageTransform();
            
            // have it load our XML into the SourceXML property
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            hpt.LoadXMLFromFile(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPageDoc.xml");

            // do the translation and then write out the string
            Response.Write(hpt.HelpAPIListHTMLPage());
        }
    }
}