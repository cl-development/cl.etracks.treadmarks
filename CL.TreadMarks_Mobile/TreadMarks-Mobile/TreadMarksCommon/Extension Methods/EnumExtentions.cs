﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Web;
using System.Web.UI.WebControls;

namespace otscommon.ExtensionMethods
{
    /// <summary>
    /// Utility class with various extension methods
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// Returns the enum's DescriptionAttribute value
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetEnumDescription(this Enum enumValue)
        {
            FieldInfo fInfo = enumValue.GetType().GetField(enumValue.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes == null || attributes.Length == 0)
                return enumValue.ToString();
            else
                return attributes[0].Description;
        }

        public static int? GetEnumByDescription<TEnum>(this Type enumType, string description)
            where TEnum : struct
        {
            int? enumValue = null;

            foreach (var value in Enum.GetValues(enumType))
            {
                if (((Enum)value).GetEnumDescription() != description)
                    continue;

                enumValue = Convert.ToInt32(value);
                break;
            }

            return enumValue;
        }

        public static ListItem[] GetListItemsByEnum(this Type enumType, string[] exceptionIDs)
        {
            ListItem[] items = { };

            foreach (var value in Enum.GetValues(enumType))
            {
                string itemValue = string.Empty;
                try
                {
                    itemValue = ((int)(Enum.Parse(enumType, value.ToString()))).ToString();
                }
                catch
                {
                    itemValue = ((byte)(Enum.Parse(enumType, value.ToString()))).ToString();
                }

                if (exceptionIDs == null || !exceptionIDs.Contains(itemValue))
                {
                    Array.Resize(ref items, items.Length + 1);

                    items[items.Length - 1] = (new ListItem(((Enum)value).GetEnumDescription(), itemValue));
                }
            }
            return items;
        }     
    }
}
