﻿using System;
using System.Collections.Generic;
using System.Linq;
using TreadMarksContracts;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Collections.Specialized;
using System.Data;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;

namespace otscommon.ExtensionMethods
{
    /// <summary>
    /// Utility class with various extension methods
    /// </summary>
    public static partial class Extensions
    {
        //Methods added to get all underlying exception messages to track the error
        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem,
            Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static string GetAllMessages(this Exception exception)
        {
            var messages = exception.FromHierarchy(ex => ex.InnerException).Select(ex => ex.Message);
            return String.Join(Environment.NewLine, messages);
        }
        private static T Deserialize<T>(this string jsonStr)
        {
            var s = new System.Web.Script.Serialization.JavaScriptSerializer();
            return s.Deserialize<T>(jsonStr);
        }
    }
}
