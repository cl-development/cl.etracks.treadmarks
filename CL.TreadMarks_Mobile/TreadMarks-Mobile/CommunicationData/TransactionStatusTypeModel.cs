namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionStatusTypeModel  : ITransactionStatusTypeModel
    {
        public TransactionStatusTypeModel()
        {            
        }

        [DataMember(Name = "transactionStatusTypeId")]
        public long TransactionStatusTypeID { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }    
        
    }
}
