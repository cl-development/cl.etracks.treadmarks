namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionType_MaterialTypeModel : ITransactionType_MaterialTypeModel
    {
        public TransactionType_MaterialTypeModel()
        { }
        [DataMember(Name = "transactionTypeMaterialTypeId")]
        public long TransactionTypeMaterialTypeId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "MaterialTypeId")]
        public long MaterialTypeId { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
    }
}
