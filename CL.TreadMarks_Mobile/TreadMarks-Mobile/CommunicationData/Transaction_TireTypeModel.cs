namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class Transaction_TireTypeModel : ITransaction_TireTypeModel
    {
        public Transaction_TireTypeModel()
        {}
        [DataMember(Name = "transactionTireTypeId")]
        public System.Guid TransactionTireTypeId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "quantity")]
        public long Quantity { get; set; }
        [DataMember(Name = "tireTypeId")]
        public long TireTypeId { get; set; }
        [DataMember(Name = "transactionId")]
        public System.Guid TransactionId { get; set; }
    }
}

