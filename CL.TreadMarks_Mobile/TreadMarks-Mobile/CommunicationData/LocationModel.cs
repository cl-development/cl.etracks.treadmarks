namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class LocationModel :ILocationModel
    {
        public LocationModel()
        {
            this.Authorizations = new HashSet<IAuthorizationModel>();
        }

        [DataMember(Name = "locationId")]
        public Guid LocationId { get; set; }
        [DataMember(Name = "address1")]
        public string Address1 { get; set; }
        [DataMember(Name = "address2")]
        public string Address2 { get; set; }
        [DataMember(Name = "address3")]
        public string Address3 { get; set; }
        [DataMember(Name = "city")]
        public string City { get; set; }
        [DataMember(Name = "country")]
        public string Country { get; set; }
        [DataMember(Name = "fax")]
        public string Fax { get; set; }
        [DataMember(Name = "latitude")]
        public decimal? Latitude { get; set; }
        [DataMember(Name = "longitude")]
        public decimal? Longitude { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "phone")]
        public string Phone { get; set; }
        [DataMember(Name = "postalCode")]
        public string PostalCode { get; set; }
        [DataMember(Name = "province")]
        public string Province { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        public bool IsUserGenerated { get; set; }
    
        public virtual ICollection<IAuthorizationModel> Authorizations { get; set; }
    }
}
