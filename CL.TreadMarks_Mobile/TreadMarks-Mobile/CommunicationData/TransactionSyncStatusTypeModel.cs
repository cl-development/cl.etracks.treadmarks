namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class TransactionSyncStatusTypeModel : ITransactionSyncStatusTypeModel
    {
        public TransactionSyncStatusTypeModel()
        {}
        [DataMember(Name = "transactionSyncStatusTypeId")]
        public long TransactionSyncStatusTypeId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "internalDescription")]
        public string internalDescription { get; set; }
        [DataMember(Name = "userMessage")]
        public string userMessage { get; set; }
    }
}
