namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

   [DataContract]
    public partial class RegistrantModel : IRegistrantModel
    {
        public RegistrantModel()
        { }

        [DataMember(Name = "registrationNumber")]
        public decimal RegistrationNumber { get; set; }
        [DataMember(Name = "businessName")]
        public string BusinessName { get; set; }
        [DataMember(Name = "createdDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember(Name = "metaData")]
        public string Metadata { get; set; }
        [DataMember(Name = "modifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "locationId")]
        public Guid? LocationId { get; set; }
        [DataMember(Name = "registrantTypeId")]
        public long RegistrantTypeId { get; set; }
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }
        [DataMember(Name = "activationDate")]
        public Nullable<System.DateTime> ActivationDate { get; set; }
        [DataMember(Name = "activeStateChangeDate")]
        public Nullable<System.DateTime> ActiveStateChangeDate { get; set; }
        [DataMember(Name = "lastUpdatedDate")]
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        [DataMember(Name = "location")]
        public LocationModel Location { get; set; }
    }
}
