﻿namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class Transaction_EligibilityModel : ITransaction_EligibilityModel
    {
        public Transaction_EligibilityModel()
        { }
        [DataMember(Name = "transactionEligibilityId")]
        public System.Guid TransactionEligibilityId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "value")]
        public bool Value { get; set; }
        [DataMember(Name = "eligibilityId")]
        public long EligibilityId { get; set; }
        [DataMember(Name = "transactionId")]
        public System.Guid TransactionId { get; set; }

    }
}
