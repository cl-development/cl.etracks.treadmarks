namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]    
    public partial class AuthorizationModel : IAuthorizationModel
    {
        public AuthorizationModel()
        {
            this.Locations = new HashSet<ILocationModel>();
        }

        [DataMember(Name = "authorizationId")]
        public Guid authorizationId { get; set; }
        [DataMember(Name = "authorizationNumber")]
        public string authorizationNumber { get; set; }
        [DataMember(Name = "expiryDate")]
        public Nullable<System.DateTime> expiryDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> syncDate { get; set; }
        [DataMember(Name = "userId")]
        public long userId { get; set; }
        [DataMember(Name = "transactionId")]
        public Nullable<System.Guid> transactionId { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public Nullable<long> transactionTypeId { get; set; }
    
        public virtual ICollection<ILocationModel> Locations { get; set; }
    }
}
