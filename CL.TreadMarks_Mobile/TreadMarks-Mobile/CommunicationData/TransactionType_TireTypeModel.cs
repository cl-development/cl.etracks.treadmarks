﻿namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class TransactionType_TireTypeModel : ITransactionType_TireTypeModel
    {
        public TransactionType_TireTypeModel()
        { }

        [DataMember(Name = "transactionTypeTireTypeId")]
        public long TransactionTypeTireTypeId { get; set; }
        [DataMember(Name = "tireTypeId")]
        public long TireTypeId { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }

    }
}
