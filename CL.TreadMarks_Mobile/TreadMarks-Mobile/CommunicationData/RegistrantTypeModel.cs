namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class RegistrantTypeModel : IRegistrantTypeModel
    {
        public RegistrantTypeModel()
        {
            this.Registrants = new HashSet<IRegistrantModel>();
        }

        [DataMember(Name = "registrantTypeId")]
        public long RegistrantTypeID { get; set; }
        [DataMember(Name = "descriptionKey")]
        public string DescriptionKey { get; set; }
        [DataMember(Name = "shortDescriptionKey")]
        public string ShortDescriptionKey { get; set; }
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
        [DataMember(Name = "createdDate")]
        public System.DateTime? CreatedDate { get; set; }
        [DataMember(Name = "modifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    
        public virtual ICollection<IRegistrantModel> Registrants { get; set; }
    }
}
