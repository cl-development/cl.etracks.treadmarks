namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class UserModel  : IUserModel
    {
        public UserModel()
        { }

        [DataMember(Name = "userId")]
        public long UserId { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }
        [DataMember(Name = "accessTypeId")]
        public long AccessTypeId { get; set; }
        [DataMember(Name = "registrationNumber")]
        public decimal RegistrationNumber { get; set; }
        [DataMember(Name = "metaData")]
        public string Metadata { get; set; }
        [DataMember(Name = "lastAccessDate")]
        public Nullable<System.DateTime> LastAccessDate { get; set; }
        [DataMember(Name = "createdDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember(Name = "modifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
