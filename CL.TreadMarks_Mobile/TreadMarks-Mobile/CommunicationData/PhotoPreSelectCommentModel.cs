namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class PhotoPreselectCommentModel : IPhotoPreselectCommentModel
    {
        public PhotoPreselectCommentModel()
        { }
        [DataMember(Name = "photoPreselectCommentId")]
        public long PhotoPreSelectCommentId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
