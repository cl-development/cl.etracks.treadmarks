namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class MaterialTypeModel : IMaterialTypeModel
    {
        public MaterialTypeModel()
        {            
        }
        [DataMember(Name = "materialTypeId")]
        public long MaterialTypeId { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "shortNameKey")]
        public string ShortNameKey { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "effectiveStartDate")]
        public System.DateTime EffectiveStartDate { get; set; }
        [DataMember(Name = "effectiveEndDate")]
        public System.DateTime EffectiveEndDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
