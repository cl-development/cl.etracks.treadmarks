namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class MessageModel : IMessageModel
    {
        public MessageModel()
        { }
        [DataMember(Name = "messageId")]
        public long MessageId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "syncDate")]
        public System.DateTime SyncDate { get; set; }
    }
}
