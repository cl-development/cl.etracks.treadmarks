namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionType_ModuleModel : ITransactionType_ModuleModel
    {
        public TransactionType_ModuleModel()
        { }
        [DataMember(Name = "transactionTypeModuleId")]
        public long TransactionTypeModuleId { get; set; }
        [DataMember(Name = "step")]
        public long Step { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "moduleId")]
        public long ModuleId { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
        [DataMember(Name = "isRequired")]
        public bool IsRequired { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
