namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class EligibilityModel : IEligibilityModel
    {
        public EligibilityModel()
        {}
        [DataMember(Name = "eligibilityId")]
        public long EligibilityId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
    }
}
