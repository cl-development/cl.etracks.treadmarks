namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class ScaleTicketModel : IScaleTicketModel
    {
        public ScaleTicketModel()
        { }

        [DataMember(Name = "scaleTicketId")]
        public Guid ScaleTicketId { get; set; }
        [DataMember(Name = "date")]
        public System.DateTime Date { get; set; }
        [DataMember(Name = "inboundWeight")]
        public Nullable<decimal> InboundWeight { get; set; }
        [DataMember(Name = "outboundWeight")]
        public Nullable<decimal> OutboundWeight { get; set; }
        [DataMember(Name = "ticketNumber")]
        public string TicketNumber { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "photoId")]
        public System.Guid PhotoId { get; set; }
        [DataMember(Name = "scaleTicketTypeId")]
        public long ScaleTicketTypeId { get; set; }
        [DataMember(Name = "transactionId")]
        public System.Guid TransactionId { get; set; }
        [DataMember(Name = "unitTypeId")]
        public long UnitTypeId { get; set; }
    }
}
