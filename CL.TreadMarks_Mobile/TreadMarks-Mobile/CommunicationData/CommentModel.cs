﻿namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class CommentModel : ICommentModel
    {
        public CommentModel()
        { }
        [DataMember(Name = "commentId")]
        public System.Guid commentId { get; set; }
        [DataMember(Name = "createdDate")]
        public System.DateTime createdDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> syncDate { get; set; }
        [DataMember(Name = "text")]
        public string text { get; set; }
        [DataMember(Name = "transactionId")]
        public System.Guid transactionId { get; set; }

        [DataMember(Name = "userId")]
        public long UserId { get; set; }    


    }
}
