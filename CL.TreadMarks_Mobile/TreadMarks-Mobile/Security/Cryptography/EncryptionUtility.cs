﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace TreadMarks_Mobile.Security.Cryptography
{
    public class EncryptionUtility
    {
        internal static byte[] Encrypt(byte[] data)
        {
            ///	973ac15a09fa425db002077b4d83193876a3e382eb975906e0befbe9f1582d9f
            byte[] key = new byte[] {
				0x97, 0x3a, 0xc1, 0x5a, 0x09, 0xfa, 0x42, 0x5d, 0xb0, 0x02, 0x07, 0x7b, 0x4d, 0x83, 0x19, 0x38,
				0x76, 0xa3, 0xe3, 0x82, 0xeb, 0x97, 0x59, 0x06, 0xe0, 0xbe, 0xfb, 0xe9, 0xf1, 0x58, 0x2d, 0x9f
			};
            byte[] encryptedBytes;

            using (AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider())
            {
                byte[] iv = cryptoServiceProvider.IV;

                cryptoServiceProvider.Key = key;
                cryptoServiceProvider.IV = iv;
                cryptoServiceProvider.Padding = PaddingMode.Zeros;
                cryptoServiceProvider.Mode = CipherMode.ECB;

                ICryptoTransform transform = cryptoServiceProvider.CreateEncryptor();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                    }

                    encryptedBytes = stream.ToArray();
                }
            }

            return encryptedBytes;
        }
    }
}