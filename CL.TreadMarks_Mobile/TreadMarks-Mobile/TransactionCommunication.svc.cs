﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TreadMarks_Mobile.ResponseMessages;
using otscommon.Enums;
using TreadMarksContracts;
using TreadMarksContracts.CommunicationSrv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    /// <summary>
    /// Transaction API - Sync Transactions
    /// </summary>
    [AspNetCompatibilityRequirements(
             RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [GlobalExceptionHandlerBehaviour(typeof(GlobalExceptionHandler))]
    public class TransactionCommunication : ITransactionCommunication
    {

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public static void AppInitialize()
        {

        }

        /// <summary>
        /// Transaction API - SaveTransaction
        /// </summary>
        /// <param name="transactionInfo"></param>
        /// <param name="credentials"></param>
        /// <returns>ITransactionResponseMsg</returns>
        /// <example>
        /// URL:http://192.168.2.93/testTransactionNew/TransactionCommunication.svc/rest/SaveTransaction
        /// Verb: POST
        /// Header: Content-Type: application/json; charset=utf-8
        /// Body:
        ///  {"transactionInfo": {"TransactionID":"93A6D01F-9ABD-4d9d-80C7-02AF85C822A8","TransactionStatusTypeId":"1","TransactionSyncStatusTypeId":"1","TransactionTypeId":"2","CreatedDate":"\/Date(1317303882420+0500)\/", "CreatedUserId":"3","Date":"\/Date(1317303882420+0500)\/",
        /// "FriendlyID":"1", "IncomingGpsLogId":"4", "IncomingRegistrantId":"1", "IncomingSignatureName":"","IncomingSignaturePhotoId":"1",   "IncomingUserId":"3","ModifiedDate":"\/Date(1317303882420+0500)\/",
        /// "ModifiedUserId":"3", "OutgoingGpsLogId":"4", "OutgoingSignatureName":"","OutgoingSignaturePhotoId":"1",  "OutgoingUserId":"3", "SyncDate":"\/Date(1317303882420+0500)\/", "TrailerLocationId":"4",
        /// "TrailerNumber":"1"   },
        /// "credentials": { "UserName":"ignat", "Password":"kurzalevski" } }
        /// </example>

        public ITransactionResponse SyncTransaction(TransactionModel transactionInfo, ServiceCredentials credentials)
        {
            TransactionResponse response = new TransactionResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                //response.TransactionService = new commonweb.TransactionService();
                response.SyncTransaction(transactionInfo);
            }

            return response;
        }

        public ISTCAuthorizationResponse SyncSTCAuthorization(STCAuthorizationModel authorizationInfo, ServiceCredentials credentials)
        {
            STCAuthorizationResponse response = new STCAuthorizationResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                //response.TransactionService = new commonweb.TransactionService();
                response.SyncSTCAuthorization(authorizationInfo);
            }

            return response;
        }
        /// <summary>
        /// Transaction API - FileUpload
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns>ITransactionResponseMsg</returns>
        /// <example>
        /// URL:http://192.168.2.93/testTransactionNew/TransactionCommunication.svc/rest/FileUpload
        /// Verb: POST
        /// Header: Content-Type: multipart/form-data;
        /// Body:
        /// {fileStream with user credentials: 'UserName', 'Password' and 'image' object}
        /// </example>
        public IPhotoResponse FileUpload(Stream fileStream)
        {
            PhotoResponse response = new PhotoResponse();
            response.FileUpload(fileStream);

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }

        /// <summary>
        /// Transaction API - GetTransactionById
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns>ITransactionResponseMsg</returns>
        /// <example>
        /// URL:http://192.168.2.93/testTransactionNew/TransactionCommunication.svc/rest/GetTransactionById?transactionId=[ID]
        /// Verb: POST
        /// Header: Content-Type: application/json; charset=utf-8
        /// </example>
        public ITransactionResponse GetTransactionById(Guid transactionId)
        {
            TransactionResponse response = new TransactionResponse();
            response.GetTransactionById(transactionId);
            return response;
        }

        /// <summary>
        /// Transaction API - GetTransactionList
        /// </summary>
        /// <returns>ITransactionResponseMsg</returns>
        /// <example>
        /// URL:http://192.168.2.93/testTransactionNew/TransactionCommunication.svc/rest/GetTransactionList
        /// Verb: POST
        /// Header: Content-Type: application/json; charset=utf-8
        /// </example>
        public ITransactionResponse GetTransactionList()
        {
            TransactionResponse response = new TransactionResponse();
            response.GetTransactionList();
            return response;
        }
        public ITransaction_TireTypeResponse SyncTransaction_TireType(Transaction_TireTypeModel[] tiresInfo, ServiceCredentials credentials)
        {
            //Transaction_TireTypeResponse response = new Transaction_TireTypeResponse();
            Transaction_TireTypeResponse response = new Transaction_TireTypeResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                //response.TransactionService = new commonweb.TransactionService();
                response.SyncTransaction_TireType(tiresInfo);
            }

            return response;
        }

        public ITransaction_EligibilityResponse SyncTransaction_Eligibility(Transaction_EligibilityModel[] transaction_EligibilityInfo, ServiceCredentials credentials)
        {
            Transaction_EligibilityResponse response = new Transaction_EligibilityResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncTransaction_Eligibility(transaction_EligibilityInfo);
            }

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }

        public IScaleTicketResponse SyncScaleTicket(ScaleTicketModel[] scaleTicketInfo, ServiceCredentials credentials)
        {
            ScaleTicketResponse response = new ScaleTicketResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncScaleTicket(scaleTicketInfo);
            }

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }

        public ICommentResponse SyncComment(CommentModel commentInfo, ServiceCredentials credentials)
        {
            CommentResponse response = new CommentResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncComment(commentInfo);
            }

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }

        //OTSM-1337 save comment array
        public ICommentResponse SyncComments(CommentModel[] commentInfo, ServiceCredentials credentials)
        {
            CommentResponse response = new CommentResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncComment(commentInfo);
            }

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }
        
        public IPhotoResponse SyncPhoto(PhotoModel photoInfo, ServiceCredentials credentials)
        {
            PhotoResponse response = new PhotoResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncPhoto(photoInfo);
            }

            if (response.ResponseStatus == (int)MobileWSResponseStatus.ERROR)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }

            return response;
        }

        public ILocationResponse SyncLocation(LocationModel locationInfo, ServiceCredentials credentials)
        {
            LocationResponse response = new LocationResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncLocation(locationInfo);
            }

            return response;
        }

        public IGpsLogResponse SyncGpsLog(GpsLogModel[] gpsLogInfo, ServiceCredentials credentials)
        {
            GpsLogResponse response = new GpsLogResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.SyncGpsLog(gpsLogInfo);
            }

            return response;
        }

        public ITransaction_MaterialTypeResponse SyncTransaction_MaterialType(Transaction_MaterialTypeModel materialsInfo, ServiceCredentials credentials)
        {
            //Transaction_TireTypeResponse response = new Transaction_TireTypeResponse();
            Transaction_MaterialTypeResponse response = new Transaction_MaterialTypeResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                //response.TransactionService = new commonweb.TransactionService();
                response.SyncTransaction_MaterialType(materialsInfo);
            }

            return response;
        }
        public IUserResponse GetUserList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            UserResponse response = new UserResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetUserList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public IRegistrantResponse GetRegistrantList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            RegistrantResponse response = new RegistrantResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetRegistrantList(dateInfo.lastUpdated.ToLocalTime());
            }            
            return response;
        }
        public ILocationResponse GetLocationList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            LocationResponse response = new LocationResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetLocationList(dateInfo.lastUpdated.ToLocalTime());
            }
            return response;
        }
        public DocumentTypeResponse GetDocumentTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            DocumentTypeResponse response = new DocumentTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetDocumentTypeList(dateInfo.lastUpdated.ToLocalTime());
            } 
            return response;
        }
        public EligibilityResponse GetEligibilityList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            EligibilityResponse response = new EligibilityResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetEligibilityList(dateInfo.lastUpdated.ToLocalTime());
            }            
            return response;
        }
        public MessageResponse GetMessageList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            MessageResponse response = new MessageResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetMessageList(dateInfo.lastUpdated.ToLocalTime());
            }            
            return response;
        }
        public PhotoPreselectCommentResponse GetPhotoPreselectCommentList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            PhotoPreselectCommentResponse response = new PhotoPreselectCommentResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetPhotoPreSelectCommentList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public PhotoTypeResponse GetPhotoTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            PhotoTypeResponse response = new PhotoTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetPhotoTypeList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public RegistrantTypeResponse GetRegistrantTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            RegistrantTypeResponse response = new RegistrantTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetRegistrantTypeList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public ScaleTicketTypeResponse GetScaleTicketTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            ScaleTicketTypeResponse response = new ScaleTicketTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetScaleTicketTypeList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public TireTypeResponse GetTireTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TireTypeResponse response = new TireTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTireTypeList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public TransactionStatusTypeResponse GetTransactionStatusTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionStatusTypeResponse response = new TransactionStatusTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionStatusTypeList(dateInfo.lastUpdated.ToLocalTime());
            } 
            
            return response;
        }
        public TransactionTypeResponse GetTransactionTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionTypeResponse response = new TransactionTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionTypeList(dateInfo.lastUpdated.ToLocalTime());
            } 
            
            return response;
        }
       
        public TransactionSyncStatusTypeResponse GetTransactionSyncStatusTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionSyncStatusTypeResponse response = new TransactionSyncStatusTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionSyncStatusTypeList(dateInfo.lastUpdated.ToLocalTime());
            } 
            
            return response;
        }
        public TransactionType_RegistrantTypeResponse GetTransactionType_RegistrantTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionType_RegistrantTypeResponse response = new TransactionType_RegistrantTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionType_RegistrantTypeList(dateInfo.lastUpdated.ToLocalTime());
            }             
            return response;
        }
        public TransactionType_MaterialTypeResponse GetTransactionType_MaterialTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionType_MaterialTypeResponse response = new TransactionType_MaterialTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionType_MaterialTypeList(dateInfo.lastUpdated.ToLocalTime());
            }
            return response;
        }
        public TransactionType_TireTypeResponse GetTransactionType_TireTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            TransactionType_TireTypeResponse response = new TransactionType_TireTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetTransactionType_TireTypeList(dateInfo.lastUpdated.ToLocalTime());
            } 
            
            return response;
        }

        public UnitTypeResponse GetUnitTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            UnitTypeResponse response = new UnitTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetUnitTypeList(dateInfo.lastUpdated.ToLocalTime());
            }            
            return response;
        }
        public MaterialTypeResponse GetMaterialTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials)
        {
            MaterialTypeResponse response = new MaterialTypeResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetMaterialTypeList(dateInfo.lastUpdated.ToLocalTime());
            }

            return response;
        }
        public SyncDatesResponse GetSyncDatesList(ServiceCredentials credentials)
        {
            SyncDatesResponse response = new SyncDatesResponse();

            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.GetSyncDatesList(); 
            }            
            return response;
        }

        public TransactionResponse PushFailedListToTM(Guid tId, ServiceCredentials credentials)
        {
            TransactionResponse response = new TransactionResponse();
            if (!credentials.Validate())
            {
                response.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                response.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
            }
            else
            {
                response.PushFailedListToTM(tId);
            }
            return response;
        }
    }
}
