﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace TreadMarks_Mobile
{
    public partial class RegistrantMapper : System.Web.UI.Page
    {

        #region Properties

        private const int PAGESIZE = 20;

        private int CurrentPage
        {
            get
            {
                object objPage = ViewState["_CurrentPage"];
                int _CurrentPage = 0;
                if (objPage == null)
                {
                    _CurrentPage = 0;
                }
                else
                {
                    _CurrentPage = (int)objPage;
                }
                return _CurrentPage;
            }
            set { ViewState["_CurrentPage"] = value; }
        }
        private int firstIndex
        {
            get
            {

                int _FirstIndex = 0;
                if (ViewState["_FirstIndex"] == null)
                {
                    _FirstIndex = 0;
                }
                else
                {
                    _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
                }
                return _FirstIndex;
            }
            set { ViewState["_FirstIndex"] = value; }
        }
        private int lastIndex
        {
            get
            {

                int _LastIndex = 0;
                if (ViewState["_LastIndex"] == null)
                {
                    _LastIndex = 0;
                }
                else
                {
                    _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
                }
                return _LastIndex;
            }
            set { ViewState["_LastIndex"] = value; }
        }
 

        #endregion

        #region Page Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.txtSearch.Text = ( Request.QueryString["reg"] ?? string.Empty).ToString();
                this.txtRegAdd.Text = (Request.QueryString["reg"] ?? string.Empty).ToString();
                if (!string.IsNullOrWhiteSpace(this.txtSearch.Text))
                {
                    this.BindItemsList();
                }

            }
        }

        #endregion

        #region Page Control Methods

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            CurrentPage = 0;
            Clear();
            this.BindItemsList();
        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {

            Save(this.txtUserIdAdd.Text.ToString(), this.txtNameAdd.Text.ToString(), this.txtEmailAdd.Text.ToString(), this.txtRegAdd.Text.ToString(), this.txtAccessTypeIdAdd.Text.ToString());
            //SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString);
            this.txtSearch.Text = this.txtRegAdd.Text.ToString();
            Clear();
            this.BindItemsList();
        }

        protected void rptRegistrantMapping_OnItemDataBound(object sender, RepeaterItemEventArgs e) 
        {

            if(e.Item.ItemType == ListItemType.Footer)
            {
                if (this.rptRegistrantMapping.Items.Count == 0)
                {
                    RepeaterItem footer = e.Item;
                    int regValue = 0;
                    //if exists
                    if (Int32.TryParse(this.txtSearch.Text.Trim(), out regValue))
                    {
                        if (GetRegistrant(regValue).Rows.Count > 0)
                        {
                            Label lblEmptyMsg = footer.FindControl("lblEmptyMsg") as Label;
                            lblEmptyMsg.Text = "0 results Found";
                        }                    
                    }
                    footer.Visible = true;
                }
                else 
                {
                    RepeaterItem footer = e.Item;
                    footer.Visible = false;                
                }
                pnlPager.Visible = this.rptRegistrantMapping.Items.Count > 0;
            } 
        }

        protected void rptRegistrantMapping_OnItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Save((e.Item.FindControl("txtUserId") as TextBox).Text.ToString(), (e.Item.FindControl("txtName") as TextBox).Text.ToString(), (e.Item.FindControl("TxtEmail") as TextBox).Text.ToString(),
                    (e.Item.FindControl("txtRegistrationNumber") as TextBox).Text.ToString(), (e.Item.FindControl("txtAccessTypeId") as TextBox).Text.ToString());
                this.BindItemsList();
            }
        }

        protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
            if (lnkbtnPage.CommandArgument.ToString() == CurrentPage.ToString())
            {
                lnkbtnPage.Enabled = false;
                lnkbtnPage.Style.Add("fone-size", "14px");
                lnkbtnPage.Font.Bold = true;
            }
        }

        protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName.Equals("Paging"))
            {

                CurrentPage = Convert.ToInt16(e.CommandArgument.ToString());
                this.BindItemsList();
            }

        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            CurrentPage += 1;
            this.BindItemsList();

        }
        protected void lbtnPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage -= 1;
            this.BindItemsList();

        }
        protected void lbtnLast_Click(object sender, EventArgs e)
        {

            CurrentPage = (Convert.ToInt32(ViewState["TotalPages"]) - 1);
            this.BindItemsList();

        }
        protected void lbtnFirst_Click(object sender, EventArgs e)
        {

            CurrentPage = 0;
            this.BindItemsList();

        }

        #endregion

        #region Helper Methods

        protected void Save(string userId, string name, string email, string registrationNumber, string accessTypeId)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString);
            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            // check if exists
            cmd.CommandText = "Select * from [User] where registrationNumber = @registrationNumber and userId = @userId";
            cmd.Parameters.Add(new SqlParameter("registrationNumber", registrationNumber));
            cmd.Parameters.Add(new SqlParameter("userId", userId));
            DataTable dt = new DataTable();
            ad.SelectCommand = cmd;
            ad.Fill(dt);
            cmd.Parameters.Clear();
            if (dt.Rows.Count > 0)
            {
                //update
                cmd.CommandText = "update [User] set name = @name, email = @email, accessTypeId = @accessTypeId where registrationNumber = @registrationNumber and userId = @userId";
                //cmd.Parameters.Add(new SqlParameter("syncDate", DateTime.Now));
            }
            else
            {
                //insert
                cmd.CommandText = "Insert into [User] (userId, name, email, registrationNumber, accessTypeId, syncDate) values (@userId, @name, @email, @registrationNumber, @accessTypeId, @syncDate)";
                cmd.Parameters.Add(new SqlParameter("syncDate", DateTime.Now));
            }
            cmd.Parameters.Add(new SqlParameter("userId", userId));
            cmd.Parameters.Add(new SqlParameter("name", name));
            cmd.Parameters.Add(new SqlParameter("email", email));
            cmd.Parameters.Add(new SqlParameter("registrationNumber", registrationNumber));
            cmd.Parameters.Add(new SqlParameter("accessTypeId", accessTypeId));
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        protected void Clear()
        {
            this.txtUserIdAdd.Text = this.txtRegAdd.Text = this.txtNameAdd.Text = this.txtEmailAdd.Text = this.txtAccessTypeIdAdd.Text = string.Empty;
        }

        private DataTable GetDataTable()
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            cmd.CommandText = "Select * from [User] where registrationNumber = @registrationNumber order by registrationNumber, UserId ";

            int regValue = 0;
            Int32.TryParse(this.txtSearch.Text.Trim(),out regValue);

            cmd.Parameters.Add(new SqlParameter("registrationNumber", string.Format("{0}", regValue)));
            //save the result in data table
            DataTable dt = new DataTable();
            ad.SelectCommand = cmd;
            ad.Fill(dt);
            return dt;
        }

        protected void BindItemsList()
        {
            this.pnlPager.Visible = true;

            DataTable dataTable = this.GetDataTable();
            PagedDataSource _PageDataSource = new PagedDataSource();
            _PageDataSource.DataSource = dataTable.DefaultView;
            _PageDataSource.AllowPaging = true;
            _PageDataSource.PageSize = 10;
            _PageDataSource.CurrentPageIndex = CurrentPage;
            ViewState["TotalPages"] = _PageDataSource.PageCount;

            this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + _PageDataSource.PageCount;
            this.lbtnPrevious.Enabled = !_PageDataSource.IsFirstPage;
            this.lbtnNext.Enabled = !_PageDataSource.IsLastPage;
            this.lbtnFirst.Enabled = !_PageDataSource.IsFirstPage;
            this.lbtnLast.Enabled = !_PageDataSource.IsLastPage;
            int num = _PageDataSource.Count;
            this.rptRegistrantMapping.DataSource = _PageDataSource;
            this.rptRegistrantMapping.DataBind();

            this.doPaging();

        }

        private void doPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");

            firstIndex = CurrentPage - 5;

            if (CurrentPage > 5)
            {
                lastIndex = CurrentPage + 5;
            }
            else
            {
                lastIndex = 10;
            }
            if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
            {
                lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
                firstIndex = lastIndex - 10;
            }

            if (firstIndex < 0)
            {
                firstIndex = 0;
            }

            for (int i = firstIndex; i < lastIndex; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i;
                dr[1] = i + 1;
                dt.Rows.Add(dr);
            }

            this.dlPaging.DataSource = dt;
            this.dlPaging.DataBind();
        }

        private DataTable GetRegistrant(int registrationNumber)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            cmd.CommandText = "Select * from [Registrant] where registrationNumber = @registrationNumber";
            int regValue = 0;
            Int32.TryParse(this.txtSearch.Text.Trim(),out regValue);
            cmd.Parameters.Add(new SqlParameter("registrationNumber", string.Format("{0}", regValue)));
            //save the result in data table
            DataTable dt = new DataTable();
            ad.SelectCommand = cmd;
            ad.Fill(dt);
            return dt;
        }

        #endregion



    }
}