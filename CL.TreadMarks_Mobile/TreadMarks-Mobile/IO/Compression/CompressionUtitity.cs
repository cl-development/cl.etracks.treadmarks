﻿using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace TreadMarks_Mobile.IO.Compression
{
    public class CompressionUtitity
    {
        internal static byte[] Compress(string text)
        {
            byte[] compressedBytes;

            using (MemoryStream compressedStream = new MemoryStream())
            {
                Stream deflate = new GZipStream(compressedStream, CompressionMode.Compress, CompressionLevel.BestCompression);
                byte[] bytes = Encoding.UTF8.GetBytes(text);
                deflate.Write(bytes, 0, bytes.Length);
                deflate.Dispose();

                compressedBytes = compressedStream.ToArray();
            }

            return compressedBytes;
        }

    }
}