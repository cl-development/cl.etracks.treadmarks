﻿using TreadMarks_Mobile.CommunicationData;
using TreadMarks_Mobile.ResponseMessages;
using TreadMarksContracts;
using TreadMarksContracts.CommunicationSrv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace TreadMarks_Mobile
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ITransactionCommunication
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        /// <summary>
        /// SaveTransaction
        /// </summary>
        /// 
        /// <example>
        /// URL:http://192.168.2.93/testTransactionNew/TransactionCommunication.svc/rest/SaveTransaction
        /// Verb: POST
        /// Header: Content-Type: application/json; charset=utf-8
        /// Body:
        ///   {"transactionInfo": {"TransactionID":"936DA01F-9ABD-4d9d-80C7-02AF85C822A8","TransactionStatusType":"123","TransactionSyncStatusType":"0","TransactionType":"0","CreatedDate":"\/Date(1317303882420+0500)\/", "CreatedUser":"","Date":"\/Date(1317303882420+0500)\/",
        ///   "FriendlyID":"", "IncomingGpsLog":"", "IncomingRegistrant":"123", "IncomingSignatureName":"","IncomingSignaturePhoto":"",   "IncomingUser":"","ModifiedDate":"\/Date(1317303882420+0500)\/",
        ///   "ModifiedUser":"", "OutgoingGpsLog":"", "OutgoingSignatureName":"","OutgoingSignaturePhoto":"",  "OutgoingUser":"", "SyncDate":"\/Date(1317303882420+0500)\/", "TrailerLocation":"",
        ///   "TrailerNumber":""   },
        /// "credentials": { "UserName":"ignat", "Password":"kurzalevski" } }
        /// </example>
        [OperationContract]
        [ServiceKnownType(typeof(TransactionResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ITransactionResponse SyncTransaction(TransactionModel transactionInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(Transaction_TireTypeResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ITransaction_TireTypeResponse SyncTransaction_TireType(Transaction_TireTypeModel[] tiresInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(Transaction_EligibilityResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ITransaction_EligibilityResponse SyncTransaction_Eligibility(Transaction_EligibilityModel[] transaction_EligibilityInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(ScaleTicketResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        IScaleTicketResponse SyncScaleTicket(ScaleTicketModel[] scaleTicketInfo, ServiceCredentials credentials);
        
        [OperationContract]
        [ServiceKnownType(typeof(CommentResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ICommentResponse SyncComment(CommentModel commentInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(CommentResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ICommentResponse SyncComments(CommentModel[] commentInfo, ServiceCredentials credentials);
        
        [OperationContract]
        [ServiceKnownType(typeof(GpsLogResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        IGpsLogResponse SyncGpsLog(GpsLogModel[] gpsLogInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(LocationResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ILocationResponse SyncLocation(LocationModel locationInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(PhotoResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        IPhotoResponse SyncPhoto(PhotoModel photoInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(STCAuthorizationResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ISTCAuthorizationResponse SyncSTCAuthorization(STCAuthorizationModel authorizationInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(Transaction_MaterialTypeResponse))]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ITransaction_MaterialTypeResponse SyncTransaction_MaterialType(Transaction_MaterialTypeModel materialsInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(PhotoResponse))]
        [WebInvoke(Method = "POST",
            UriTemplate = "FileUpload",
            ResponseFormat = WebMessageFormat.Json
            )]
        IPhotoResponse FileUpload(Stream fileStream);

        [OperationContract]
        [ServiceKnownType(typeof(SyncDatesResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        SyncDatesResponse GetSyncDatesList(ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionResponse))]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        ITransactionResponse GetTransactionById(Guid transactionId);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionResponse))]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        ITransactionResponse GetTransactionList();

        [OperationContract]
        [ServiceKnownType(typeof(UserResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        IUserResponse GetUserList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(RegistrantResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        IRegistrantResponse GetRegistrantList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(LocationResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        ILocationResponse GetLocationList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        
        [OperationContract]
        [ServiceKnownType(typeof(DocumentTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        DocumentTypeResponse GetDocumentTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(EligibilityResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        EligibilityResponse GetEligibilityList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        [OperationContract]
        [ServiceKnownType(typeof(MessageResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        MessageResponse GetMessageList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(PhotoPreselectCommentResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        PhotoPreselectCommentResponse GetPhotoPreselectCommentList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(PhotoTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        PhotoTypeResponse GetPhotoTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(RegistrantTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        RegistrantTypeResponse GetRegistrantTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        [OperationContract]
        [ServiceKnownType(typeof(ScaleTicketTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        ScaleTicketTypeResponse GetScaleTicketTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TireTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TireTypeResponse GetTireTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        [OperationContract]
        [ServiceKnownType(typeof(TransactionStatusTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionStatusTypeResponse GetTransactionStatusTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionTypeResponse GetTransactionTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionSyncStatusTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionSyncStatusTypeResponse GetTransactionSyncStatusTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        [OperationContract]
        [ServiceKnownType(typeof(TransactionType_RegistrantTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionType_RegistrantTypeResponse GetTransactionType_RegistrantTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionType_TireTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionType_TireTypeResponse GetTransactionType_TireTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);
        [OperationContract]
        [ServiceKnownType(typeof(UnitTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        UnitTypeResponse GetUnitTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionType_MaterialTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionType_MaterialTypeResponse GetTransactionType_MaterialTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(MaterialTypeResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        MaterialTypeResponse GetMaterialTypeList(IncomingDateModel dateInfo, ServiceCredentials credentials);

        [OperationContract]
        [ServiceKnownType(typeof(TransactionResponse))]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped
          )]
        TransactionResponse PushFailedListToTM(Guid tId, ServiceCredentials credentials);

    }
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

}

