<%@ Page Language="C#" %>
<%@ Import Namespace="System.Web.Security" %>

<script runat="server">
  void Logon_Click(object sender, EventArgs e)
  {
    if ((User.Text == "admin") && 
            (UserPass.Text == "TZA/N2ct'j-4wNpj"))
      {
          FormsAuthentication.RedirectFromLoginPage 
             (User.Text, Persist.Checked);
			 Response.Redirect("FindTransactions.aspx");
      }
      else
      {
          Msg.Text = "Invalid credentials. Please try again.";
      }
  }
</script>
<html>
<head id="Head1" runat="server">
  <title>Authentication - Login</title>
  <style>        

        .smart-green {
            margin-left: auto;
            margin-right: auto;
            background: rgba(232, 235, 235, 0.91);
            padding: 30px 30px 20px 30px;
            font: 13px Arial, Helvetica, sans-serif;
            color: #666;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
        }
       
table {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:12px;
	margin:20px;
	
}

    </style>
</head>
<body>
  <form id="form1" runat="server" class="smart-green">
    <h3>
      Logon Page</h3>
    <table>
      <tr>
        <td>
          User:</td>
        <td>
          <asp:TextBox ID="User" runat="server" MaxLength="50"/></td>
        <td>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
            ControlToValidate="User"
            Display="Dynamic" 
            ErrorMessage="Cannot be empty." 
            runat="server" />
        </td>
      </tr>
      <tr>
        <td>
          Password:</td>
        <td>
          <asp:TextBox ID="UserPass" TextMode="Password" MaxLength="50"
             runat="server" />
        </td>
        <td>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
            ControlToValidate="UserPass"
            ErrorMessage="Cannot be empty." 
            runat="server" />
        </td>
      </tr>
      <tr>
        <td>
          Remember me?</td>
        <td>
          <asp:CheckBox ID="Persist" runat="server" /></td>
      </tr>
    </table>
    <asp:Button ID="Submit1" OnClick="Logon_Click" Text="Log On" 
       runat="server" />
    <p>
      <asp:Label ID="Msg" ForeColor="red" runat="server" />
    </p>
  </form>
</body>
</html>