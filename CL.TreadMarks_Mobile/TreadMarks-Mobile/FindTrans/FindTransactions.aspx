﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindTransactions.aspx.cs" Inherits="TreadMarks_Mobile.FindTransactions" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .EU_TableScroll {
            max-height: 275px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .EU_DataTable {
            border-collapse: collapse;
            width: 100%;
        }

            .EU_DataTable tr th {
                background: #a9db80; /* Old browsers */
                background: -moz-linear-gradient(top, #a9db80 0%, #96c56f 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9db80), color-stop(100%,#96c56f)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* IE10+ */
                background: linear-gradient(to bottom, #a9db80 0%,#96c56f 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 ); /* IE6-9 */
                background-color: #9DC45F;
                color: #ffffff;
                padding: 10px 5px 10px 5px;
                border: 1px solid #cccccc;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13px;
                font-weight: normal;
                text-transform: capitalize;
            }

            .EU_DataTable tr:nth-child(2n+1) td {
                background: #e7f5db; /* Old browsers */
                background: -moz-linear-gradient(top, #e7f5db 0%, #e2efd7 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e7f5db), color-stop(100%,#e2efd7)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #e7f5db 0%,#e2efd7 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, #e7f5db 0%,#e2efd7 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, #e7f5db 0%,#e2efd7 100%); /* IE10+ */
                background: linear-gradient(to bottom, #e7f5db 0%,#e2efd7 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e7f5db', endColorstr='#e2efd7',GradientType=0 ); /* IE6-9 */
                background-color: #e9ffc6;
                color: #454545;
            }

            .EU_DataTable tr td {
                padding: 5px 10px 5px 10px;
                color: #454545;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13px;
                border: 1px solid #cccccc;
                vertical-align: middle;
            }

                .EU_DataTable tr td:first-child {
                    text-align: center;
                }


        /*######## Smart Green ########*/
        .smart-green {
            margin-left: auto;
            margin-right: auto;
            background: rgba(235, 235, 235, 0.91);
            padding: 30px 30px 20px 30px;
            font: 13px Arial, Helvetica, sans-serif;
            color: #666;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
        }

            .smart-green h1 {
                background: #a9db80; /* Old browsers */
                background: -moz-linear-gradient(top, #a9db80 0%, #96c56f 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9db80), color-stop(100%,#96c56f)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* IE10+ */
                background: linear-gradient(to bottom, #a9db80 0%,#96c56f 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 ); /* IE6-9 */
                font: 24px "Trebuchet MS", Arial, Helvetica, sans-serif;
                padding: 20px 0px 20px 40px;
                display: block;
                margin: -30px -30px 10px -30px;
                color: #FFF;
                text-shadow: 1px 1px 1px #949494;
                border-radius: 5px 5px 0px 0px;
                -webkit-border-radius: 5px 5px 0px 0px;
                -moz-border-radius: 5px 5px 0px 0px;
                border-bottom: 1px solid #89AF4C;
            }

                .smart-green h1 > span {
                    display: block;
                    font-size: 12px;
                    color: #FFF;
                }

            .smart-green label {
                display: block;
                margin: 0px 0px 5px;
            }

                .smart-green label > span {
                    float: left;
                    margin-top: 10px;
                    color: #5E5E5E;
                }

            .smart-green input[type="text"], .smart-green input[type="email"], .smart-green textarea, .smart-green select {
                color: #555;
                height: 30px;
                line-height: 15px;
                width: 20%;
                padding: 0px 0px 0px 10px;
                margin-top: 2px;
                border: 1px solid #E5E5E5;
                background: #FBFBFB;
                outline: 0;
                -webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
                box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
            }

            .smart-green textarea {
                height: 100px;
                padding-top: 10px;
            }

        .button {
            background: #a9db80; /* Old browsers */
            background: -moz-linear-gradient(top, #a9db80 0%, #96c56f 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9db80), color-stop(100%,#96c56f)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #a9db80 0%,#96c56f 100%); /* IE10+ */
            background: linear-gradient(to bottom, #a9db80 0%,#96c56f 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 ); /* IE6-9 */
            background-color: #9DC45F;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-border-radius: 5px;
            border: none;
            padding: 10px 25px 10px 25px;
            color: #FFF;
            text-shadow: 1px 1px 1px #949494;
        }

        .button:hover {
            background-color: #a9db80 !important;
            cursor: pointer;
        }

        #cblSyncStatus td {
            margin-right: 20px;
            display: inline;
        }

        #cblSyncStatus label {
            display: inline;
        }

        #cblSyncStatus input {
        }
    </style>
</head>
<script runat="server">
  
  //Welcome.Text = "Hello, " + Context.User.Identity.Name;
  
  void Signout_Click(object sender, EventArgs e)
  {
    FormsAuthentication.SignOut();
    Response.Redirect("Logon.aspx");
  }
</script>
<body>
    <form id="form1" runat="server" class="smart-green">
        <h1>Find Mobile Transactions
            <span>Please enter the filter values as required!</span>
			<asp:Button ID="Submit1" OnClick="Signout_Click" Text="Sign Out" runat="server" />
        </h1>		 
		
        <div>
            <table border="0" cellspacing="3" cellpadding="3" width="100%">
                <tr>
                    <td>
                        <asp:Label ID="lblDevice" Text="Device Name:" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDevice" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFriendlyId" Text="Transaction Id:" runat="server"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtFriendlyId" runat="server" MaxLength="13"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ForeColor="Red" runat="server" ControlToValidate="txtFriendlyId" ValidationExpression="^[0-9]*$" Text="Accepts only numbers."></asp:RegularExpressionValidator>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblIncomingReg" Text="Reg#:" runat="server"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtIncomingReg" runat="server" MaxLength="13"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ForeColor="Red" runat="server" ControlToValidate="txtIncomingReg" ValidationExpression="^[0-9]*$" Text="Accepts only numbers."></asp:RegularExpressionValidator>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Transaction Date:" runat="server"></asp:Label></td>
                    <td>
                        <BDP:BasicDatePicker ID="dpTDate" runat="server" DateFormat="MM/dd/yyyy" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Sync Date:" runat="server"></asp:Label></td>
                    <td>
                        <BDP:BasicDatePicker ID="dpSyncDate" runat="server" DateFormat="MM/dd/yyyy" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSync" Text="Sync Status:" runat="server"></asp:Label></td>
                    <td>
                        <asp:CheckBoxList RepeatLayout="Table" ID="cblSyncStatus" RepeatDirection="Horizontal" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <td>
                            <asp:Button ID="cmdFind" runat="server" Text="Find" CssClass="button" OnClick="cmdFind_Click" />
                            &nbsp;  &nbsp; 
                        <asp:Button ID="cmdExport" runat="server" Text="Export" Enabled="false" CssClass="button" OnClick="cmdExport_Click" />
                            <tr>
                                <td colspan="2">
                            </tr>
            </table>
            <br />
        </div>
        <asp:Label ID="lblTotal" Visible="false" Text="0" runat="server"></asp:Label></td>                                         
        <asp:GridView ID="gdTransactions" runat="server" AllowPaging="true" AllowSorting="true" CssClass="EU_DataTable" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="PageIndexChanging" OnSorting="Sorting">
            <Columns>
                <asp:BoundField DataField="friendlyId" HeaderText="Transaction Id" SortExpression="friendlyId" />
                <asp:BoundField DataField="transactionStartDate" HeaderText="Transaction Start Date" SortExpression="transactionStartDate" />
                <asp:BoundField DataField="transactionDate" HeaderText="Transaction Date" SortExpression="transactionDate" />
                <asp:BoundField DataField="syncDate" HeaderText="Sync Date" SortExpression="syncDate" />
                <asp:BoundField DataField="responseComments" HeaderText="Comments" SortExpression="responseComments" />
                <asp:BoundField DataField="deviceName" HeaderText="Device Name" SortExpression="deviceName" />
                <asp:BoundField DataField="_type" HeaderText="Transaction Type" SortExpression="transactionTypeId" />
                <asp:BoundField DataField="sync_status" HeaderText="Sync Status" SortExpression="transactionSyncStatusTypeId" ReadOnly="True" />
                <asp:BoundField DataField="_status" HeaderText="Transaction Status" SortExpression="transactionStatusTypeId" />
                <asp:BoundField DataField="incomingRegistrationNumber" HeaderText="Incoming Reg#" SortExpression="incomingRegistrationNumber" />
                <asp:BoundField DataField="outgoingRegistrationNumber" HeaderText="Outgoing Reg#" SortExpression="outgoingRegistrationNumber" />
                <asp:BoundField DataField="incomingUserId" HeaderText="Incoming UserId" SortExpression="incomingUserId" />
                <asp:BoundField DataField="outgoingUserId" HeaderText="Outgoing UserId" SortExpression="outgoingUserId" />
                <asp:BoundField DataField="versionBuild" HeaderText="version" SortExpression="versionBuild" />
            </Columns>
            <HeaderStyle Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#a9db80" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="white" ForeColor="#333333" />

        </asp:GridView>
    </form>
</body>
</html>
