﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionType_TireType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionType_TireTypeModel))]
    public class TransactionType_TireTypeResponse
    {
        #region TransactionType_TireTypeResponse Members
        public void GetTransactionType_TireTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionType_TireType> ttttList = pullStaticDataMgr.GetTransactionType_TireTypeList(lastUpdated);
                if (ttttList != null)
                    TransactionType_TireTypeList = (from transactionType_TireType in ttttList
                                                    select new TransactionType_TireTypeModel()
                                                    {
                                                        TransactionTypeTireTypeId = transactionType_TireType.transactionTypeTireTypeId,
                                                        TireTypeId = transactionType_TireType.TireTypeId,
                                                        TransactionTypeId = transactionType_TireType.transactionTypeId,
                                                        SyncDate = transactionType_TireType.syncDate
                                                    }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionType_TireTypeModel> responseContent;
        public IEnumerable<TransactionType_TireTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionType_TireTypeModel>();
                if (TransactionType_TireTypeList.Count > 0)
                    this.responseContent = TransactionType_TireTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionType_TireTypeModel> TransactionType_TireTypeList { get; set; }
                
        #endregion
    }
}