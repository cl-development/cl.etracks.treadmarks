﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(ScaleTicketModel))]
    public class ScaleTicketResponse : IScaleTicketResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncScaleTicket(ScaleTicketModel[] scaleTicketList)
        {
            try
            {
                if (scaleTicketList == null)
                    throw new Exception("Null object received from device!");
                ScaleTicketMgr ScaleTicketMgr = new ScaleTicketMgr();
                this.syncDate = ScaleTicketMgr.Save(scaleTicketList);
                this.responseMessages.Add("ScaleTicket saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("ScaleTicket not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        
        private IEnumerable<ScaleTicketModel> responseContent;
        public IEnumerable<ScaleTicketModel> ResponseContent
        {
            get
            {
                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
        
        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }
        
        #endregion
    }
}