﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarksContracts.Common.Enums;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionModel))]
    public class TransactionResponse : ITransactionResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }

        /// <summary>
        /// Saves the transaction information.
        /// </summary>
        /// <param name="model">Transaction Info.</param>
        public void SyncTransaction(TransactionModel transactionInfo)
        {
            try
            {
                lock (GetLocker(transactionInfo.TransactionID))
                {
                    TransactionMgr transactionMgr = new TransactionMgr();
                    Transaction transaction = transactionMgr.GetById(transactionInfo.TransactionID);
                    
                    //If it is already exisiting and Repository_Sync_TM don't push again to TM
                    if (transaction != null && transaction.transactionSyncStatusTypeId == (int)SyncStatuses.Repository_Sync_TM && transactionInfo.TransactionStatusTypeId == (int)TransactionStatuses.COMPLETE)
                    {
                        //Logger.Error("Tranaction already synced: " + transactionInfo.TransactionID + " Device Name: " + transactionInfo.DeviceName, "SyncTransaction");
                        this.TransactionSyncStatusTypeId = transaction.transactionSyncStatusTypeId;
                        this.syncDate = transaction.syncDate;
                    }
                    else
                    {
                        //ET-96 Don't allow to sync anything created before
                        //AppStartDate
                        bool isSynctoTMPermitted = CheckAppStartDate(transactionInfo);                                                

                        transactionInfo.TransactionSyncStatusTypeId = isSynctoTMPermitted ? (int)SyncStatuses.Device_Sync_Repository : (int)SyncStatuses.Repository_Sync_Fail_TM;                        
                        
                        ITransactionModel transactionModel = transactionMgr.Save(transactionInfo);

                        if (isSynctoTMPermitted)
                        {
                            //If PrepareTransactionToPush is successful then push else save
                            transactionModel = PrepareTransactionToPush(ref transactionInfo) ? transactionMgr.PushToTM(transactionInfo) : transactionMgr.Save(transactionInfo);
                            //transactionModel = transactionMgr.PushToTM(transactionInfo);
                        }

                        if (transactionModel.TransactionSyncStatusTypeId == (int)SyncStatuses.Repository_Sync_Fail_TM)
                            Logger.Error(" \nTranaction failed to Sync:  \nFriendly Id: " + transactionInfo.FriendlyId + "\nTransaction Id: " + transactionInfo.TransactionID + "\nDevice Name : " + transactionInfo.DeviceName + "\nMessage: " + transactionInfo.ResponseComments, "Method: SyncTransaction\n");
                        

                        //Send it back to device
                        this.TransactionSyncStatusTypeId = transactionModel.TransactionSyncStatusTypeId;
                        this.syncDate = transactionModel.SyncDate;
                    }

                    this.responseMessages.Add("saved successfully!");
                    this.responseStatus = MobileWSResponseStatus.OK;
                }
            }

            catch (ArgumentException aEx)
            {
                Logger.Error(aEx.Message, "Sync Transaction");
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                Logger.Error(ex.GetAllMessages(), "Sync Transaction");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        private static bool CheckAppStartDate(ITransactionModel transactionInfo)
        {
            bool isSynctoTMPermitted = true;

            //version check not required anymore
            //string strVersion =
            //transactionInfo.VersionBuild;
            ////Check version only if it is required 
            //if (ConfigurationManager.AppSettings["EnableVersionCheck"] == "1" &&   transactionInfo.TransactionTypeId == (int)TransactionTypes.PTR)              
            //{
            //    //Need to catch the exception just in case version format is wrong or corrupted
            //    try
            //    {
            //        Version minVerReqd = new Version(ConfigurationManager.AppSettings["MinVersionRequired"]);
            //        Version appVersion = new Version(strVersion.Remove(strVersion.IndexOf('(')).Substring(strVersion.IndexOf(' ')).Trim());
            //        DateTime dateForVersionCheck = DateTime.Parse(ConfigurationManager.AppSettings["DateForVersionCheck"]);
            //        if (appVersion < minVerReqd && transactionInfo.CreatedDate >= dateForVersionCheck)
            //        {
            //            isVersionAllowed = false;
            //            transactionInfo.ResponseComments = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + " Version Check Failed.\nBuild Version: " + strVersion + "\nTransaction Created Date: " + transactionInfo.CreatedDate.ToString();                        
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        transactionInfo.ResponseComments = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + "Exception: " + ex.Message + "\nBuild Version: " + strVersion + "\nTransaction Created Date: " + transactionInfo.CreatedDate.ToString();
            //        Logger.Error(transactionInfo.ResponseComments + ex.GetAllMessages(), "Version Check Exception:\n");
            //    }                
            //}

            //ET-96 Don't allow to sync anything created before AppStartDate
            DateTime appStartDate = DateTime.Parse(ConfigurationManager.AppSettings["AppStartDate"]);
            if (transactionInfo.CreatedDate.Date < appStartDate.Date)
            {
                isSynctoTMPermitted = false;
                transactionInfo.ResponseComments += DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]") + " Snap! Gates are closed! Transaction Created Date: " + transactionInfo.CreatedDate.ToString();
            }
            return isSynctoTMPermitted;
        }

        //Eventually we won't need this anymore coz we are sending only trtansactionId 
        private bool PrepareTransactionToPush(ref TransactionModel t)
        {
            try
            {                
                t.Transaction_EligibilityList = GetTransaction_Eligibility(t.TransactionID);

                //delete it after testing push code
                t.Transaction_Eligibility = t.Transaction_EligibilityList.Count > 0 ? t.Transaction_EligibilityList.First() : null;

                t.Comments = GetComment(t.TransactionID);

                t.Transaction_TireTypes = GetTire_Types(t.TransactionID);

                t.Photos = GetPhoto(t.TransactionID);

                if (t.TransactionTypeId == (int)TransactionTypes.STC && t.TransactionStatusTypeId == (int)TransactionStatuses.COMPLETE)
                {
                    STCAuthorizationMgr stcMgr = new STCAuthorizationMgr();
                    Authorization auth = stcMgr.GetAuthorizationByTransactionId(t.TransactionID);
                    t.Authorization = new AuthorizationModel
                    {
                        authorizationId = auth.authorizationId,
                        transactionId = auth.transactionId,
                        authorizationNumber = auth.authorizationNumber,
                        transactionTypeId = auth.transactionTypeId
                    };

                    STCAuthorization stcAuth = stcMgr.GetSTCAuthorizationById(auth.authorizationId);
                    LocationMgr locMgr = new LocationMgr();
                    Location loc = locMgr.GetById((Guid)stcAuth.locationId);
                    t.STCLocation = new LocationModel() { LocationId = loc.locationId, Name = loc.name, Latitude=loc.latitude, Longitude=loc.longitude, Phone = loc.phone };
                }

                if (t.TransactionTypeId == (int)TransactionTypes.UCR && t.OutgoingRegistrationNumber == 9999999)
                {
                    t.Company = new CompanyModel()
                    {
                        TransactionId = t.TransactionID,
                        Address1 = t.CompanyInfo.Location.Address1,
                        Address2 = t.CompanyInfo.Location.Address2,
                        Address3 = t.CompanyInfo.Location.Address3,
                        City = t.CompanyInfo.Location.City,
                        Country = t.CompanyInfo.Location.Country,
                        Name = t.CompanyInfo.BusinessName,
                        PostalCode = t.CompanyInfo.Location.PostalCode,
                        Phone = t.CompanyInfo.Location.Phone
                    };
                }

                //New TM change: Scale tickets can be there in DOT too
                if ((t.TransactionTypeId == (int)TransactionTypes.PTR || t.TransactionTypeId == (int)TransactionTypes.DOT) && t.TransactionStatusTypeId == (int)TransactionStatuses.COMPLETE)
                {
                    ScaleTicketMgr stMgr = new ScaleTicketMgr();

                    PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                    ICollection<UnitType> utList = pullStaticDataMgr.GetUnitTypeList(new DateTime(2000, 01, 01)).ToList();
                    
                    ScaleTicket bothST = stMgr.GetById(t.TransactionID);

                    //New TM change: Scale tickets can be there in DOT too
                    //For DOT, If no scaleticket
                    if (bothST != null)
                    {
                        if (bothST.scaleTicketTypeId != 3)
                        {
                            ScaleTicket inST = stMgr.GetInboundById(t.TransactionID);
                            ScaleTicket outST = stMgr.GetOutboundById(t.TransactionID);


                            t.InboundScaleTicket = new ScaleTicketModel()
                            {
                                InboundWeight = Convert.ToDecimal(utList.Where(ut => ut.unitTypeId == inST.unitTypeId).SingleOrDefault().kgMultiplier * Convert.ToDouble(inST.inboundWeight)),
                                TicketNumber = inST.ticketNumber,
                                ScaleTicketId = inST.scaleTicketId,
                                ScaleTicketTypeId = inST.scaleTicketTypeId,
                                Date = inST.date,
                                PhotoId = inST.photoId,
                                TransactionId = inST.transactionId,
                                UnitTypeId = inST.unitTypeId
                            };

                            t.OutboundScaleTicket = new ScaleTicketModel()
                            {
                                OutboundWeight = Convert.ToDecimal(utList.Where(ut => ut.unitTypeId == outST.unitTypeId).SingleOrDefault().kgMultiplier * Convert.ToDouble(outST.outboundWeight)),
                                TicketNumber = outST.ticketNumber,
                                ScaleTicketId = outST.scaleTicketId,
                                ScaleTicketTypeId = outST.scaleTicketTypeId,
                                Date = outST.date,
                                PhotoId = outST.photoId,
                                TransactionId = outST.transactionId,
                                UnitTypeId = outST.unitTypeId
                            };
                        }
                        else
                        {
                            t.ScaleTicketBoth = new ScaleTicketModel()
                            {
                                InboundWeight = Convert.ToDecimal(utList.Where(ut => ut.unitTypeId == bothST.unitTypeId).SingleOrDefault().kgMultiplier * Convert.ToDouble(bothST.inboundWeight)),
                                OutboundWeight = Convert.ToDecimal(utList.Where(ut => ut.unitTypeId == bothST.unitTypeId).SingleOrDefault().kgMultiplier * Convert.ToDouble(bothST.outboundWeight)),
                                TicketNumber = bothST.ticketNumber,
                                ScaleTicketId = bothST.scaleTicketId,
                                ScaleTicketTypeId = bothST.scaleTicketTypeId,
                                Date = bothST.date,
                                PhotoId = bothST.photoId,
                                TransactionId = bothST.transactionId,
                                UnitTypeId = bothST.unitTypeId
                            }; ;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //if failed to prepare transaction send back a 6
                t.TransactionSyncStatusTypeId = (int)SyncStatuses.Repository_Sync_Fail_TM;
                t.ResponseComments += "Failed to prepare transaction for TM: " + ex.Message;
                return false;
            }
        }

        private static List<IPhotoModel> GetPhoto(Guid transactionId)
        {
            PhotoMgr pMgr = new PhotoMgr();
            var photoList = pMgr.GetAllByTransactionId(transactionId);

            List<IPhotoModel> pList = new List<IPhotoModel>();

            foreach (var photo in photoList)
            {
                pList.Add(new PhotoModel { PhotoID = photo.photoId, Comments = photo.comments, PhotoTypeId = photo.photoTypeId, CreatedDate = photo.createdDate, Date = photo.date, FileName = photo.fileName, Latitude = photo.latitude, Longitude = photo.longitude, SyncDate = photo.syncDate, TransactionID = photo.transactionId, UniqueFileName = photo.uniqueFileName });
            }
            return pList;
        }

        private static List<ITransaction_TireTypeModel> GetTire_Types(Guid transactionId)
        {
            Transaction_TireTypeMgr tTireTypeMgr = new Transaction_TireTypeMgr();
            var tTireTypeList = tTireTypeMgr.GetAllByTransactionId(transactionId);

            List<ITransaction_TireTypeModel> ttmList = new List<ITransaction_TireTypeModel>();

            foreach (var transaction_TireType in tTireTypeList)
            {
                ttmList.Add(new Transaction_TireTypeModel { TireTypeId = transaction_TireType.tireTypeId, TransactionTireTypeId = transaction_TireType.transactionTireTypeId, Quantity = transaction_TireType.quantity });
            }
            return ttmList;
        }

        private static List<ICommentModel> GetComment(Guid transactionId)
        {
            CommentMgr comMgr = new CommentMgr();
            var commentList = comMgr.GetByTransactionId(transactionId);

            List<ICommentModel> cList = new List<ICommentModel>();

            foreach (var comment in commentList)
            {
                cList.Add(new CommentModel { commentId = comment.commentId, createdDate = comment.createdDate, text = comment.text ?? "", transactionId = comment.transactionId });
            }
            return cList;
        }

        private static List<ITransaction_EligibilityModel> GetTransaction_Eligibility(Guid transactionId)
        {
            Transaction_EligibilityMgr tEligibilityMgr = new Transaction_EligibilityMgr();
            var tEligibilityList = tEligibilityMgr.GetAllById(transactionId);

            List<ITransaction_EligibilityModel> tEligibilityMList = new List<ITransaction_EligibilityModel>();

            foreach (var transaction_Eligibility in tEligibilityList)
            {
                tEligibilityMList.Add(new Transaction_EligibilityModel { EligibilityId = transaction_Eligibility.eligibilityId, TransactionId = transaction_Eligibility.transactionId, Value = transaction_Eligibility.value });
            }
            return tEligibilityMList;
        }


        public void PushFailedListToTM(Guid tId)
        {
            try
            {               
                TransactionMgr transactionMgr = new TransactionMgr();
                Transaction t = transactionMgr.GetById(tId);
                
                TransactionModel model = new TransactionModel()
                {
                    CreatedUserId = t.createdUserId,
                    CreatedDate = t.createdDate,
                    ModifiedUserId = t.modifiedUserId,
                    ModifiedDate = t.modifiedDate,
                    TransactionID = t.transactionId,
                    TransactionStatusTypeId = t.transactionStatusTypeId,
                    TransactionSyncStatusTypeId = t.transactionSyncStatusTypeId,
                    TransactionTypeId = t.transactionTypeId,
                    TransactionDate = t.transactionDate.ToLocalTime(),
                    FriendlyId = t.friendlyId,
                    IncomingGpsLogId = t.incomingGpsLogId,
                    OutgoingGpsLogId = t.outgoingGpsLogId,
                    RegistrationNumber = t.incomingRegistrationNumber,
                    IncomingSignatureName = t.incomingSignatureName,
                    IncomingSignaturePhotoId = t.incomingSignaturePhotoId,
                    OutgoingSignaturePhotoId = t.outgoingSignaturePhotoId,
                    OutgoingSignatureName = t.outgoingSignatureName,
                    IncomingUserId = t.incomingUserId,
                    OutgoingUserId = t.outgoingUserId,
                    SyncDate = t.syncDate,
                    TrailerLocationId = t.trailerLocationId,
                    TrailerNumber = t.trailerNumber,
                    PostalCode1 = t.postalCode1,
                    PostalCode2 = t.postalCode2,
                    DeviceName = t.deviceName,
                    OutgoingRegistrationNumber = t.outgoingRegistrationNumber,
                    ResponseComments = t.responseComments,
                    VersionBuild = t.versionBuild
                };

                ITransactionModel iModel = model;
                
                lock (GetLocker(model.TransactionID))
                {
                    iModel = PrepareTransactionToPush(ref model) ? transactionMgr.PushToTM(iModel) : transactionMgr.Save(iModel);                    
                }

                Logger.Error(" \nTried manual push to TM: \nFriendly Id: " + model.FriendlyId + " \nTransaction Id: " + model.TransactionID + " \nMessage: " + model.ResponseComments, "Method: PushFailedListToTM\n");            
                this.responseMessages.Add("sent successfully!");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        

        /// <summary>
        /// Get Transaction By Id
        /// </summary>
        /// <param name="transactionId">TransactionId</param>
        public void GetTransactionById(Guid transactionId)
        {
            try
            {
                TransactionMgr transactionMgr = new TransactionMgr();
                Transaction transaction = transactionMgr.GetById(transactionId);
                SingleEntity = new TransactionModel()
                {
                    TransactionID = transaction.transactionId,
                    TransactionTypeId = transaction.transactionTypeId,
                    TransactionStatusTypeId = transaction.transactionStatusTypeId,
                    TransactionSyncStatusTypeId = transaction.transactionSyncStatusTypeId
                };

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("TransactionId: " + transactionId);
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        public void GetTransactionList()
        {
            try
            {
                TransactionMgr transactionMgr = new TransactionMgr();
                TransactionList = (from transaction in transactionMgr.GetList()
                                   select new TransactionModel()
                                   {                                       
                                       TransactionID = transaction.transactionId,
                                       TransactionTypeId = transaction.transactionTypeId,
                                       TransactionStatusTypeId = transaction.transactionStatusTypeId,
                                       TransactionSyncStatusTypeId = transaction.transactionSyncStatusTypeId
                                   }).ToList();
				
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        #endregion

        #region IResponseMessage<ITransactionModel> Members

        private IEnumerable<TransactionModel> responseContent;
        public IEnumerable<TransactionModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionModel>();
                if (TransactionList.Count > 0)
                    this.responseContent = TransactionList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public TransactionModel SingleEntity { get; set; }

        [DataMember]
        public long TransactionSyncStatusTypeId { get; set; }
       
        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }
     
        [DataMember]
        public List<TransactionModel> TransactionList { get; set; }

        private static readonly Dictionary<Guid, Object> _lockers = new Dictionary<Guid, object>();
        /// <summary>
        /// returns a lock key based on the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private object GetLocker(Guid id)
        {
            lock (_lockers)
            {
                if (!_lockers.ContainsKey(id))
                    _lockers.Add(id, new object());
                return _lockers[id];
            }
        }
        #endregion
    }
}