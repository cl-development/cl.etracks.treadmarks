﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TireType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TireTypeModel))]
    public class TireTypeResponse
    {
        #region TireTypeResponse Members
        public void GetTireTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TireType> ttList = pullStaticDataMgr.GetTireTypeList(lastUpdated);
                if (ttList != null)
                    TireTypeList = (from tireType in ttList
                                    select new TireTypeModel()
                                    {
                                        TireTypeId = tireType.tireTypeId,
                                        NameKey = tireType.nameKey,
                                        SortIndex = tireType.sortIndex,
                                        SyncDate = tireType.syncDate,
                                        EffectiveEndDate = tireType.effectiveEndDate,
                                        EffectiveStartDate = tireType.effectiveStartDate,
                                        ShortNameKey = tireType.shortNameKey,
                                        EstimatedWeight = tireType.estimatedWeight
                                    }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TireTypeModel> responseContent;
        public IEnumerable<TireTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TireTypeModel>();
                if (TireTypeList.Count > 0)
                    this.responseContent = TireTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TireTypeModel> TireTypeList { get; set; }
                
        #endregion
    }
}