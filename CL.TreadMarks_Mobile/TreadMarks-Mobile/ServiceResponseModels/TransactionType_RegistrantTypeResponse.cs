﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionType_RegistrantType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionType_RegistrantTypeModel))]
    public class TransactionType_RegistrantTypeResponse
    {
        #region TransactionType_RegistrantTypeResponse Members
        public void GetTransactionType_RegistrantTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionType_RegistrantType> ttrtList = pullStaticDataMgr.GetTransactionType_RegistrantTypeList(lastUpdated);
                if (ttrtList != null)
                    TransactionType_RegistrantTypeList = (from transactionType_RegistrantType in ttrtList
                                                          select new TransactionType_RegistrantTypeModel()
                                                          {
                                                              TransactionTypeRegistrantTypeId = transactionType_RegistrantType.transactionTypeRegistrantTypeId,
                                                              RegistrantTypeId = transactionType_RegistrantType.registrantTypeId,
                                                              TransactionTypeId = transactionType_RegistrantType.transactionTypeId,
                                                              SyncDate = transactionType_RegistrantType.syncDate
                                                          }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionType_RegistrantTypeModel> responseContent;
        public IEnumerable<TransactionType_RegistrantTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionType_RegistrantTypeModel>();
                if (TransactionType_RegistrantTypeList.Count > 0)
                    this.responseContent = TransactionType_RegistrantTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionType_RegistrantTypeModel> TransactionType_RegistrantTypeList { get; set; }
                
        #endregion
    }
}