﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(PhotoModel))]
    public class PhotoResponse : IPhotoResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncPhoto(PhotoModel photoInfo)
        {
            try
            {
                PhotoMgr photoMgr = new PhotoMgr();
                
                Photo photo = photoMgr.GetById(photoInfo.PhotoID);
                // Don't insert a new object for which the image is not still received
                if (photo == null)
                    throw new Exception("Photo object not found, please upload the image first!");
                //if photo is found keep the existing file name, don't update it to user's filename
                else
                    photoInfo.UniqueFileName = photo.uniqueFileName;

                this.syncDate = photoMgr.Save(photoInfo);
                this.responseMessages.Add("Photo saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Photo not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        public void FileUpload(Stream fileStream)
        {
            try
            {
                //We need to extract three keys: username, password, transactionId
                HttpMultipartParser parser = new HttpMultipartParser(fileStream, "image");

                if (parser.Success)
                {
                    ServiceCredentials credentials = new ServiceCredentials();
                    credentials.username = HttpUtility.UrlDecode(parser.Parameters["username"]);
                    credentials.password = HttpUtility.UrlDecode(parser.Parameters["password"]);

                    if (!credentials.Validate())
                    {
                        this.ResponseStatus = (int)MobileWSResponseStatus.ERROR;
                        this.ResponseMessages.Add("Passed credentials invalid. The method cannot be executed.");
                    }
                    else
                    {
                        PhotoModel photo = parser.GetPhotoObject();

                        string transactionId = photo.TransactionID.ToString();

                        string createPath = "\\" + transactionId.Substring(0, 2) + "\\" + transactionId.Substring(2).Replace("-", "\\") + "\\" + photo.PhotoID;

                        string fullPath = HttpContext.Current.Server.MapPath(".\\images") + createPath;

                        //Create directory
                        Directory.CreateDirectory(fullPath);

                        //Create unique image name
                        string strUniqueFileName = Guid.NewGuid() + photo.FileName.Substring(photo.FileName.LastIndexOf('.'));
                        
                        // Save the file
                        File.WriteAllBytes(fullPath + "\\" + strUniqueFileName, parser.FileContents);
                        
                        PhotoMgr photoMgr = new PhotoMgr();
                        photo.UniqueFileName = createPath + "\\" + strUniqueFileName;
                        this.syncDate = photoMgr.Save(photo);

                        this.responseMessages.Add("Uploaded successfully!");
                        this.responseStatus = MobileWSResponseStatus.OK;
                    }
                }
            }
            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                this.ResponseMessages.Add("Error parsing data: " + ex.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }      

        private IEnumerable<PhotoModel> responseContent;
        public IEnumerable<PhotoModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<PhotoModel>();
                if (TransactionList.Count > 0)
                    this.responseContent = TransactionList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public PhotoModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<PhotoModel> TransactionList { get; set; }
                
        #endregion
    }
}