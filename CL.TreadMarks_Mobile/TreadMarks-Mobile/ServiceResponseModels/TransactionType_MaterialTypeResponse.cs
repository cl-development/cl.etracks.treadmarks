﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionType_MaterialType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionType_MaterialTypeModel))]
    public class TransactionType_MaterialTypeResponse
    {
        #region TransactionType_MaterialTypeResponse Members
        public void GetTransactionType_MaterialTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionType_MaterialType> ttrtList = pullStaticDataMgr.GetTransactionType_MaterialTypeList(lastUpdated);
                if (ttrtList != null)
                    TransactionType_MaterialTypeList = (from transactionType_MaterialType in ttrtList
                                                          select new TransactionType_MaterialTypeModel()
                                                          {
                                                              TransactionTypeMaterialTypeId = transactionType_MaterialType.transactionTypeMaterialTypeId,
                                                              MaterialTypeId = transactionType_MaterialType.materialTypeId,
                                                              TransactionTypeId = transactionType_MaterialType.transactionTypeId,
                                                              SyncDate = transactionType_MaterialType.syncDate
                                                          }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionType_MaterialTypeModel> responseContent;
        public IEnumerable<TransactionType_MaterialTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionType_MaterialTypeModel>();
                if (TransactionType_MaterialTypeList.Count > 0)
                    this.responseContent = TransactionType_MaterialTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionType_MaterialTypeModel> TransactionType_MaterialTypeList { get; set; }
                
        #endregion
    }
}