﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// PhotoType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(PhotoTypeModel))]
    public class PhotoTypeResponse
    {
        #region PhotoTypeResponse Members
        public void GetPhotoTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<PhotoType> ptList = pullStaticDataMgr.GetPhotoTypeList(lastUpdated);
                if (ptList != null)
                    PhotoTypeList = (from photoType in ptList
                                     select new PhotoTypeModel()
                                     {
                                         PhotoTypeId = photoType.photoTypeId,
                                         Name = photoType.name,
                                         SyncDate = photoType.syncDate

                                     }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<PhotoTypeModel> responseContent;
        public IEnumerable<PhotoTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<PhotoTypeModel>();
                if (PhotoTypeList.Count > 0)
                    this.responseContent = PhotoTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<PhotoTypeModel> PhotoTypeList { get; set; }
                
        #endregion
    }
}