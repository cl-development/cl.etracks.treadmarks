﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(Transaction_EligibilityModel))]
    public class Transaction_EligibilityResponse : ITransaction_EligibilityResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncTransaction_Eligibility(Transaction_EligibilityModel[] transaction_EligibilityList)
        {
            try
            {
                if (transaction_EligibilityList == null)
                    throw new Exception("Null object received from device!");
                Transaction_EligibilityMgr transaction_EligibilityMgr = new Transaction_EligibilityMgr();
                this.syncDate = transaction_EligibilityMgr.Save(transaction_EligibilityList);
                this.responseMessages.Add("Transaction_Eligibility saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Transaction_Eligibility not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        
        private IEnumerable<Transaction_EligibilityModel> responseContent;
        public IEnumerable<Transaction_EligibilityModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<Transaction_EligibilityModel>();
                if (TransactionList.Count > 0)
                    this.responseContent = TransactionList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public Transaction_EligibilityModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<Transaction_EligibilityModel> TransactionList { get; set; }
                
        #endregion
    }
}