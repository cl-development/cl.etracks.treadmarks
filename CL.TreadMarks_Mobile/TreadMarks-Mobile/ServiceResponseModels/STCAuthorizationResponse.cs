﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(STCAuthorizationModel))]
    public class STCAuthorizationResponse : ISTCAuthorizationResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncSTCAuthorization(ISTCAuthorizationModel stcAuthorizationInfo)
        {
            try
            {
                STCAuthorizationMgr stcAuthorizationMgr = new STCAuthorizationMgr();
                this.syncDate = stcAuthorizationMgr.Save(stcAuthorizationInfo);
                this.responseMessages.Add("STCAuthorization saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("STCAuthorization not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }


        public void GetSTCAuthorizationList(string lastUpdated)
        {
            try
            {
                //STCAuthorizationMgr STCAuthorizationMgr = new STCAuthorizationMgr();
                //STCAuthorizationList = (from STCAuthorization in STCAuthorizationMgr.GetList(Helper.UnixTimeStampToDateTime(lastUpdated))
                //            select new STCAuthorizationModel()
                //            {
                //                STCAuthorizationId = STCAuthorization.STCAuthorizationId,
                //                RegistrationNumber = STCAuthorization.registrationNumber,
                //                AccessTypeId = STCAuthorization.accessTypeId,
                //                CreatedDate = STCAuthorization.createdDate,
                //                Email = STCAuthorization.email,
                //                LastAccessDate = STCAuthorization.lastAccessDate,
                //                Metadata = STCAuthorization.metadata,
                //                ModifiedDate = STCAuthorization.modifiedDate,
                //                Name = STCAuthorization.name,
                //                SyncDate = STCAuthorization.syncDate
                //            }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<STCAuthorizationModel> responseContent;
        public IEnumerable<STCAuthorizationModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<STCAuthorizationModel>();
                if (STCAuthorizationList.Count > 0)
                    this.responseContent = STCAuthorizationList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public STCAuthorizationModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<STCAuthorizationModel> STCAuthorizationList { get; set; }
                
        #endregion
    }
}