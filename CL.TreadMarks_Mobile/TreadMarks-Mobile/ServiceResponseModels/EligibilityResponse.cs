﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Eligibility response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(EligibilityModel))]
    public class EligibilityResponse
    {
        #region EligibilityResponse Members
        public void GetEligibilityList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<Eligibility> eligList = pullStaticDataMgr.GetEligibilityList(lastUpdated);
                if (eligList != null)
                    EligibilityList = (from eligibility in eligList
                                       select new EligibilityModel()
                                       {
                                           EligibilityId = eligibility.eligibilityId,
                                           NameKey = eligibility.nameKey,
                                           SortIndex = eligibility.sortIndex,
                                           SyncDate = eligibility.syncDate,
                                           TransactionTypeId = eligibility.transactionTypeId
                                       }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<EligibilityModel> responseContent;
        public IEnumerable<EligibilityModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<EligibilityModel>();
                if (EligibilityList.Count > 0)
                    this.responseContent = EligibilityList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<EligibilityModel> EligibilityList { get; set; }
                
        #endregion
    }
}