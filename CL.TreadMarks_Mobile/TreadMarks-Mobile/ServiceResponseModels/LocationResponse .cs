﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(LocationModel))]
    public class LocationResponse : ILocationResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncLocation(LocationModel locationInfo)
        {
            try
            {
                LocationMgr locationMgr = new LocationMgr();
                this.syncDate = locationMgr.Save(locationInfo).SyncDate;
                this.responseMessages.Add("Location saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Location not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        public void GetLocationList(DateTime lastUpdated)
        {
            try
            {
                LocationMgr locationMgr = new LocationMgr();
                IEnumerable<Location> locList = locationMgr.GetList(lastUpdated);
                if (locList != null)
                    LocationList = (from location in locList
                                    select new LocationModel()
                                    {
                                        LocationId = location.locationId,
                                        Address1 = location.address1,
                                        Address2 = location.address2,
                                        Address3 = location.address3,
                                        City = location.city,
                                        Country = location.country,
                                        Fax = location.fax,
                                        Latitude = location.latitude,
                                        Longitude = location.longitude,
                                        Name = location.name,
                                        Phone = location.phone,
                                        PostalCode = location.postalCode,
                                        Province = location.province,
                                        SyncDate = location.syncDate
                                    }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        private IEnumerable<LocationModel> responseContent;
        public IEnumerable<LocationModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<LocationModel>();
                if (LocationList.Count > 0)
                    this.responseContent = LocationList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public LocationModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<LocationModel> LocationList { get; set; }
                
        #endregion
    }
}