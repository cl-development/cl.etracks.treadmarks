﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Message response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(MessageModel))]
    public class MessageResponse
    {
        #region MessageResponse Members
        public void GetMessageList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<Message> mesgList = pullStaticDataMgr.GetMessageList(lastUpdated);
                if (mesgList != null)
                    MessageList = (from message in mesgList
                                   select new MessageModel()
                                   {
                                       MessageId = message.messageId,
                                       NameKey = message.nameKey,
                                       SyncDate = message.syncDate
                                   }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<MessageModel> responseContent;
        public IEnumerable<MessageModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<MessageModel>();
                if (MessageList.Count > 0)
                    this.responseContent = MessageList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<MessageModel> MessageList { get; set; }
                
        #endregion
    }
}