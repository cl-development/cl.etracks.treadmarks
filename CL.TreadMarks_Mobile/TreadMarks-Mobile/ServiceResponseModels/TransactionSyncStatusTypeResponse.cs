﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionSyncStatusType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionSyncStatusTypeModel))]
    public class TransactionSyncStatusTypeResponse
    {
        #region TransactionSyncStatusTypeResponse Members
        public void GetTransactionSyncStatusTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionSyncStatusType> tsstList = pullStaticDataMgr.GetTransactionSyncStatusTypeList(lastUpdated);
                if (tsstList != null)
                    TransactionSyncStatusTypeList = (from transactionSyncStatusType in tsstList
                                                     select new TransactionSyncStatusTypeModel()
                                                     {
                                                         TransactionSyncStatusTypeId = transactionSyncStatusType.transactionSyncStatusTypeId,
                                                         NameKey = transactionSyncStatusType.nameKey,
                                                         FileName = transactionSyncStatusType.fileName,
                                                         internalDescription = transactionSyncStatusType.internalDescription,
                                                         userMessage = transactionSyncStatusType.userMessage,
                                                         SyncDate = transactionSyncStatusType.syncDate
                                                     }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionSyncStatusTypeModel> responseContent;
        public IEnumerable<TransactionSyncStatusTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionSyncStatusTypeModel>();
                if (TransactionSyncStatusTypeList.Count > 0)
                    this.responseContent = TransactionSyncStatusTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionSyncStatusTypeModel> TransactionSyncStatusTypeList { get; set; }
                
        #endregion
    }
}