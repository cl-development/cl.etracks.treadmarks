﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(RegistrantModel))]
    public class RegistrantResponse : IRegistrantResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncRegistrant(RegistrantModel registrantInfo)
        {
            try
            {
                RegistrantMgr registrantMgr = new RegistrantMgr();
                this.syncDate = registrantMgr.Save(registrantInfo);
                this.responseMessages.Add("Registrant saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Registrant not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        public void GetRegistrantList(DateTime lastUpdated)
        {
            try
            {
                RegistrantMgr registrantMgr = new RegistrantMgr();
                IEnumerable<Registrant> regList = registrantMgr.GetList(lastUpdated);
                if (regList != null)
                    RegistrantList = (from registrant in regList
                                      select new RegistrantModel()
                                      {
                                          RegistrationNumber = registrant.registrationNumber,
                                          RegistrantTypeId = registrant.registrantTypeId,
                                          ActivationDate = registrant.activationDate,
                                          IsActive = registrant.isActive,
                                          ActiveStateChangeDate = registrant.activeStateChangeDate,
                                          BusinessName = registrant.businessName,
                                          CreatedDate = registrant.createdDate,
                                          LastUpdatedDate = registrant.lastUpdatedDate,
                                          LocationId = registrant.locationId,
                                          Metadata = registrant.metadata,
                                          ModifiedDate = registrant.modifiedDate,
                                          SyncDate = registrant.syncDate
                                      }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<RegistrantModel> responseContent;
        public IEnumerable<RegistrantModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<RegistrantModel>();
                if (RegistrantList.Count > 0)
                    this.responseContent = RegistrantList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public RegistrantModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<RegistrantModel> RegistrantList { get; set; }
                
        #endregion
    }
}