﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// UnitType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(UnitTypeModel))]
    public class UnitTypeResponse
    {
        #region UnitTypeResponse Members
        public void GetUnitTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<UnitType> utList = pullStaticDataMgr.GetUnitTypeList(lastUpdated);
                if (utList != null)
                    UnitTypeList = (from unitType in utList
                                    select new UnitTypeModel()
                                    {
                                        UnitTypeId = unitType.unitTypeId,
                                        NameKey = unitType.nameKey,
                                        KgMultiplier = unitType.kgMultiplier,
                                        SyncDate = unitType.syncDate
                                    }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        private ICollection<UnitTypeModel> responseContent;
        public ICollection<UnitTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new HashSet<UnitTypeModel>();
                if (UnitTypeList.Count > 0)
                    this.responseContent = UnitTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public ICollection<UnitTypeModel> UnitTypeList { get; set; }
                
        #endregion
    }
}