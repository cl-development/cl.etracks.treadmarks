﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// MaterialType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(MaterialTypeModel))]
    public class MaterialTypeResponse
    {
        #region MaterialTypeResponse Members
        public void GetMaterialTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<MaterialType> ttList = pullStaticDataMgr.GetMaterialTypeList(lastUpdated);
                if (ttList != null)
                    MaterialTypeList = (from materialType in ttList
                                           select new MaterialTypeModel()
                                           {
                                               MaterialTypeId = materialType.materialTypeId,
                                               NameKey = materialType.nameKey,
                                               SyncDate = materialType.syncDate,
                                               ShortNameKey = materialType.shortNameKey,
                                               SortIndex = materialType.sortIndex,
                                               EffectiveEndDate = materialType.effectiveEndDate,
                                               EffectiveStartDate = materialType.effectiveStartDate                                              
                                           }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<MaterialTypeModel> responseContent;
        public IEnumerable<MaterialTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<MaterialTypeModel>();
                if (MaterialTypeList.Count > 0)
                    this.responseContent = MaterialTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<MaterialTypeModel> MaterialTypeList { get; set; }
                
        #endregion
    }
}