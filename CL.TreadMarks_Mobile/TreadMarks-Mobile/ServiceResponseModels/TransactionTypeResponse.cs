﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionTypeModel))]
    public class TransactionTypeResponse
    {
        #region TransactionTypeResponse Members
        public void GetTransactionTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionType> ttList = pullStaticDataMgr.GetTransactionTypeList(lastUpdated);
                if (ttList != null)
                    TransactionTypeList = (from transactionType in ttList
                                           select new TransactionTypeModel()
                                           {
                                               TransactionTypeId = transactionType.transactionTypeId,
                                               NameKey = transactionType.nameKey,
                                               FileName = transactionType.fileName,
                                               SyncDate = transactionType.syncDate,
                                               TireCountMessageKey = transactionType.tireCountMessageKey,
                                               IncomingSignatureKey = transactionType.incomingSignatureKey,
                                               OutgoingSignatureKey = transactionType.outgoingSignatureKey,
                                               ShortNameKey = transactionType.shortNameKey,
                                               SortIndex = transactionType.sortIndex
                                           }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionTypeModel> responseContent;
        public IEnumerable<TransactionTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionTypeModel>();
                if (TransactionTypeList.Count > 0)
                    this.responseContent = TransactionTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionTypeModel> TransactionTypeList { get; set; }
                
        #endregion
    }
}