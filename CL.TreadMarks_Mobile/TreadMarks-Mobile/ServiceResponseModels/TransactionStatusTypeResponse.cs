﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// TransactionStatusType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionStatusTypeModel))]
    public class TransactionStatusTypeResponse
    {
        #region TransactionStatusTypeResponse Members
        public void GetTransactionStatusTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<TransactionStatusType> tstList = pullStaticDataMgr.GetTransactionStatusTypeList(lastUpdated);
                if (tstList != null)
                    TransactionStatusTypeList = (from transactionStatusType in tstList
                                                 select new TransactionStatusTypeModel()
                                                 {
                                                     TransactionStatusTypeID = transactionStatusType.transactionStatusTypeId,
                                                     NameKey = transactionStatusType.nameKey,
                                                     FileName = transactionStatusType.fileName,
                                                     SyncDate = transactionStatusType.syncDate
                                                 }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<TransactionStatusTypeModel> responseContent;
        public IEnumerable<TransactionStatusTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<TransactionStatusTypeModel>();
                if (TransactionStatusTypeList.Count > 0)
                    this.responseContent = TransactionStatusTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<TransactionStatusTypeModel> TransactionStatusTypeList { get; set; }
                
        #endregion
    }
}