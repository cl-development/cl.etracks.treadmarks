﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// PhotoPreSelectComment response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(PhotoPreselectCommentModel))]
    public class PhotoPreselectCommentResponse
    {
        #region PhotoPreSelectCommentResponse Members
        public void GetPhotoPreSelectCommentList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<PhotoPreSelectComment> ppsList = pullStaticDataMgr.GetPhotoPreSelectCommentList(lastUpdated);
                if (ppsList != null)
                    PhotoPreselectCommentList = (from photoPreSelectComment in ppsList
                                                 select new PhotoPreselectCommentModel()
                                                 {
                                                     PhotoPreSelectCommentId = photoPreSelectComment.photoPreSelectCommentId,
                                                     NameKey = photoPreSelectComment.nameKey,
                                                     SortIndex = photoPreSelectComment.sortIndex,
                                                     SyncDate = photoPreSelectComment.syncDate

                                                 }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<PhotoPreselectCommentModel> responseContent;
        public IEnumerable<PhotoPreselectCommentModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<PhotoPreselectCommentModel>();
                if (PhotoPreselectCommentList.Count > 0)
                    this.responseContent = PhotoPreselectCommentList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<PhotoPreselectCommentModel> PhotoPreselectCommentList { get; set; }
                
        #endregion
    }
}