﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public interface ITransactionModelExtended : ITransactionModel
    {
        #region Hauler Claim Summary Properties
        String Phone { get; set; }

        String PreparedBy { get; set; }

        Decimal? OpeningInventoryWeight { get; set; }

        Decimal? OpeningInventoryCredit { get; set; }

        #endregion


        #region Received From Collectors Properties

        bool? Revised { get; set; }

        String OtsComments { get; set; }

        #endregion

        #region Collector Properties

        String UID { get; set; }

        #endregion



    }
}
