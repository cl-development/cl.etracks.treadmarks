namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    
    public interface ITransactionType_ModuleModel
    {
        long TransactionTypeModuleId { get; set; }
        long Step { get; set; }
        long SortIndex { get; set; }
        long ModuleId { get; set; }
        long TransactionTypeId { get; set; }
        bool IsRequired { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
