﻿namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITireTypeModel
    {
        long TireTypeId { get; set; }
        string NameKey { get; set; }
        long SortIndex { get; set; }
        decimal EstimatedWeight { get; set; }
        System.DateTime EffectiveStartDate { get; set; }
        System.DateTime EffectiveEndDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        string ShortNameKey { get; set; }    
        ICollection<ITransaction_TireTypeModel> Transaction_TireType { get; set; }
        ICollection<ITransactionType_TireTypeModel> TransactionType_TireType { get; set; }

    }
}
