namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class MessageModel
    {
        public MessageModel()
        { }
        public long MessageId { get; set; }
        public string NameKey { get; set; }
        public System.DateTime SyncDate { get; set; }
    }
}
