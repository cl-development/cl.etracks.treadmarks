namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IUserModel 
    {
        long UserId { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        long AccessTypeId { get; set; }
        decimal RegistrationNumber { get; set; }
        string Metadata { get; set; }
        Nullable<System.DateTime> LastAccessDate { get; set; }
        Nullable<System.DateTime> CreatedDate { get; set; }
        Nullable<System.DateTime> ModifiedDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
