namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IRegistrantTypeModel
    {
        long RegistrantTypeID { get; set; }
        string DescriptionKey { get; set; }
        string ShortDescriptionKey { get; set; }
        string FileName { get; set; }
        System.DateTime? CreatedDate { get; set; }
        Nullable<System.DateTime> ModifiedDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }    
        ICollection<IRegistrantModel> Registrants { get; set; }
    }
}
