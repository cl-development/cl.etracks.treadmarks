﻿namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    
    public interface ICommentModel
    {
        System.Guid commentId { get; set; }
        System.DateTime createdDate { get; set; }
        Nullable<System.DateTime> syncDate { get; set; }
        string text { get; set; }
        System.Guid transactionId { get; set; }
        long UserId { get; set; }    


    }
}
