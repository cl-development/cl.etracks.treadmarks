namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IPhotoModel
    {
        Guid PhotoID { get; set; }
        System.DateTime? Date { get; set; }
        byte[] FileContent { get; set; }
        string FileName { get; set; }
        string Comments { get; set; }
        Nullable<decimal> Latitude { get; set; }
        Nullable<decimal> Longitude { get; set; }
        System.DateTime CreatedDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        Guid TransactionID { get; set; }
        int PhotoTypeId { get; set; }
        string UniqueFileName { get; set; }
    }
}
