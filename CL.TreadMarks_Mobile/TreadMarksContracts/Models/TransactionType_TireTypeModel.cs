﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    public class TransactionType_TireTypeModel
    {
        public TransactionType_TireTypeModel()
        { }

        public long TransactionTypeTireTypeId { get; set; }
        public long TireTypeId { get; set; }
        public long TransactionTypeId { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

    }
}
