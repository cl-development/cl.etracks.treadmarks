namespace TreadMarksContracts
{ 
    using System;
    using System.Collections.Generic;
    public interface ICompanyModel 
    {
        int CompanyId { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Name { get; set; }
        string Phone { get; set; }
        string PostalCode { get; set; }
        string Province { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        Guid TransactionId { get; set; }    
    }
}
