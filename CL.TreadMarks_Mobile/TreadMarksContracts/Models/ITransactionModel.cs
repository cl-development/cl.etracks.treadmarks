namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionModel
    {        
        System.Guid TransactionID { get; set; }
        long FriendlyId { get; set; }
        System.DateTime TransactionDate { get; set; }
        long TransactionTypeId { get; set; }
        long OutgoingUserId { get; set; }
        Guid OutgoingGpsLogId { get; set; }
        long IncomingUserId { get; set; }
        Guid? IncomingGpsLogId { get; set; }
        decimal RegistrationNumber { get; set; }
        long TransactionStatusTypeId { get; set; }
        long TransactionSyncStatusTypeId { get; set; }
        string TrailerNumber { get; set; }
        Nullable<long> TrailerLocationId { get; set; }
        Nullable<Guid> OutgoingSignaturePhotoId { get; set; }
        string OutgoingSignatureName { get; set; }
        Nullable<Guid> IncomingSignaturePhotoId { get; set; }
        string IncomingSignatureName { get; set; }
        long CreatedUserId { get; set; }
        System.DateTime CreatedDate { get; set; }
        Nullable<long> ModifiedUserId { get; set; }
        Nullable<System.DateTime> ModifiedDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        ICollection<IPhotoModel> Photos { get; set; }
        ICollection<ITransaction_TireTypeModel> Transaction_TireTypes { get; set; }
        ICollection<ITransaction_EligibilityModel> Transaction_EligibilityList { get; set; }
        ILocationModel TrailerLocation { get; set; }
        ICollection<ICommentModel> Comments { get; set; }
        ITransaction_EligibilityModel Transaction_Eligibility { get; set; }
        string ResponseComments { get; set; }
        string PostalCode1 { get; set; }
        string PostalCode2 { get; set; }
        decimal OutgoingRegistrationNumber { get; set; }
        string DeviceName { get; set; }
        ISTCAuthorizationModel STCAuthorization { get; set; }
        IAuthorizationModel Authorization { get; set; }
        ILocationModel STCLocation { get; set; }
        IScaleTicketModel InboundScaleTicket { get; set; }
        IScaleTicketModel OutboundScaleTicket { get; set; }
        IScaleTicketModel ScaleTicketBoth { get; set; }
        string VersionBuild { get; set; }
        string DeviceId { get; set; }
        //Dennis is sending registrant and location in companyInfo
        //IRegistrantModel CompanyInfo { get; set; }
        ICompanyModel Company { get; set; }
    
      
    }
}
