namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IEligibilityModel
    {
        long EligibilityId { get; set; }
        string NameKey { get; set; }
        long SortIndex { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        long TransactionTypeId { get; set; }
    }
}
