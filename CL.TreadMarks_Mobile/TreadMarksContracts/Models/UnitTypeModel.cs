namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class UnitTypeModel
    {
        public UnitTypeModel()
        { }
        public long UnitTypeId { get; set; }
        public decimal KgMultiplier { get; set; }
        public string NameKey { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
