namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionStatusTypeModel 
    {
        long TransactionStatusTypeID { get; set; }
        string NameKey { get; set; }
        string FileName { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }          
    }
}
