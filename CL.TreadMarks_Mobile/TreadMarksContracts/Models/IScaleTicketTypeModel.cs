namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
   
    public interface IScaleTicketTypeModel
    {
        long ScaleTicketTypeId { get; set; }
        string NameKey { get; set; }
        long SortIndex { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
