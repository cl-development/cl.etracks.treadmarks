namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionType_ModuleModel
    {
        public TransactionType_ModuleModel()
        { }
        public long TransactionTypeModuleId { get; set; }
        public long Step { get; set; }
        public long SortIndex { get; set; }
        public long ModuleId { get; set; }
        public long TransactionTypeId { get; set; }
        public bool IsRequired { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
