namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IMaterialTypeModel 
    {
        long MaterialTypeId { get; set; }
        long SortIndex { get; set; }
        string ShortNameKey { get; set; }
        string NameKey { get; set; }
        System.DateTime EffectiveStartDate { get; set; }
        System.DateTime EffectiveEndDate { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
