﻿namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class TireTypeModel
    {
        public TireTypeModel()
        {
            this.Transaction_TireType = new HashSet<Transaction_TireTypeModel>();
            this.TransactionType_TireType = new HashSet<TransactionType_TireTypeModel>();
        }
    
        public long TireTypeId { get; set; }
        public string NameKey { get; set; }
        public long SortIndex { get; set; }
        public decimal EstimatedWeight { get; set; }
        public System.DateTime EffectiveStartDate { get; set; }
        public System.DateTime EffectiveEndDate { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public string ShortNameKey { get; set; }
    
        public virtual ICollection<Transaction_TireTypeModel> Transaction_TireType { get; set; }
        public virtual ICollection<TransactionType_TireTypeModel> TransactionType_TireType { get; set; }

    }
}
