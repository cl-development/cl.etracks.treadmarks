namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;

    public interface IModuleModel
    {
        long ModuleId { get; set; }
        string NameKey { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
