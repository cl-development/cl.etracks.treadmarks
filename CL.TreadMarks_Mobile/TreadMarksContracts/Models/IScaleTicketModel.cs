namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
   
    public interface IScaleTicketModel
    {
        Guid ScaleTicketId { get; set; }
        System.DateTime Date { get; set; }
        Nullable<decimal> InboundWeight { get; set; }
        Nullable<decimal> OutboundWeight { get; set; }
        string TicketNumber { get; set; }
        long SortIndex { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        System.Guid PhotoId { get; set; }
        long ScaleTicketTypeId { get; set; }
        System.Guid TransactionId { get; set; }
        long UnitTypeId { get; set; }
    }
}
