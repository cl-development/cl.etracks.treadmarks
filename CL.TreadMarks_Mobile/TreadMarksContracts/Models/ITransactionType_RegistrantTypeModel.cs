namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionType_RegistrantTypeModel
    {
        long TransactionTypeRegistrantTypeId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        long RegistrantTypeId { get; set; }
        long TransactionTypeId { get; set; }
    }
}
