namespace TreadMarksContracts
{ 
    using System;
    using System.Collections.Generic;
  
    public interface IGpsLogModel
    {
        System.Guid GpsLogId { get; set; }
        decimal Latitude { get; set; }
        decimal Longitude { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        System.DateTime TimeStamp { get; set; }
    }
}
