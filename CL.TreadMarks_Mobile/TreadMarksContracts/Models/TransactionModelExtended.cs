﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionModelExtended : TransactionModel
    {

        public TransactionModel parent { get; set; }
        public TransactionModelExtended() : base()
        {

        }

        public TransactionModelExtended(TransactionModel transaction)
        {
            parent = transaction;

            foreach (PropertyInfo prop in parent.GetType().GetProperties())
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(parent, null), null);
            this.Phone = string.Empty;
            this.PreparedBy = string.Empty;
            this.OpeningInventoryCredit = 0;
            this.OpeningInventoryWeight = 0;
        }

        [DataMember(Name = "Phone")]
        public System.String Phone { get; set; }

        [DataMember(Name = "PreparedBy")]
        public System.String PreparedBy { get; set; }

        [DataMember(Name = "OpeningInventoryWeight")]
        public Decimal? OpeningInventoryWeight { get; set; }

        [DataMember(Name = "OpeningInventoryCredit")]
        public Decimal? OpeningInventoryCredit { get; set; }

    }
}
