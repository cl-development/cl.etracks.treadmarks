﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts.Common.Enums
{
    /// <summary>
    /// Enumeration which defines possible Web Service Response Status values and their labels
    /// </summary>
    public enum TransactionStatuses
    {
        [Description("ISSUES")]
        ISSUES = 1,

        [Description("INCOMPLETE")]
        INCOMPLETE = 2,

        [Description("COMPLETE")]
        COMPLETE = 3,

        [Description("DELETED")]
        DELETED = 4,
    }
}
