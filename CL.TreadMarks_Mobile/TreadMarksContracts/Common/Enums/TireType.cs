﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TreadMarksContracts.Common.Enums
{
    public enum TireTypes
    {
        NONE = 0,
        
        [Description("PLT")]
        PLT = 1, //'Passenger & Light Truck tires'

        [Description("MT")]
        MT = 2, //'Medium Truck Tires'

        [Description("AG/LS")]
        AGLS = 3, //'Agricultural Drive and Logger Skidder tires'

        [Description("IND")]
        IND = 4, //'Small and Large Industrial tires'

        [Description("SOTR")]
        SOTR = 5, //'Small OTR tires'

        [Description("MOTR")]
        MOTR = 6, //'Medium OTR tires'

        [Description("LOTR")]
        LOTR = 7, //'Large OTR tires'

        [Description("GOTR")]
        GOTR = 8 //'Giant OTR tires'

    }
}
