CREATE TABLE VendorService
(
	ID INT IDENTITY(1,1) NOT NULL,
	VendorTypeID INT NOT NULL,
	MaxNumber INT NOT NULL,
	[RowVersion] [timestamp] NULL,
	CONSTRAINT pk_VendorVendorService PRIMARY KEY(ID),
	CONSTRAINT fk_VendorVendorService FOREIGN KEY(VendorTypeID) REFERENCES VendorType(ID)
)