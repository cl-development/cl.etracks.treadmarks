﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Mobile;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class MobileTransactionBoundedContext : MDBaseContext<MobileTransactionBoundedContext>
    {
        public MobileTransactionBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ScaleTicket> ScaleTickets { get; set; }

        public DbSet<Photo> Photos { get; set; }
        public DbSet<TransactionEligibility> TransactionEligibilities { get; set; }
        public DbSet<Authorization> Authorizations { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<MobileRegistrant> MobileRegistrants { get; set; }
        public DbSet<GpsLog> GpsLogs { get; set; }
        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>()
                .Property(t => t.ID)
                .HasColumnName("transactionId");

            modelBuilder.Entity<Transaction>().Ignore(t => t.Rowversion);

            modelBuilder.Entity<TransactionType>()
              .Property(t => t.ID)
              .HasColumnName("transactionTypeId");

            modelBuilder.Entity<TransactionType>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<TransactionStatusType>()
            .Property(t => t.ID)
            .HasColumnName("transactionStatusTypeId");

            modelBuilder.Entity<TransactionStatusType>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<TransactionTireType>()
            .Property(t => t.ID)
            .HasColumnName("transactionTireTypeId");

            modelBuilder.Entity<TransactionTireType>().Ignore(t => t.Rowversion);

           
            modelBuilder.Entity<TransactionMaterialType>()
                       .Property(t => t.ID)
                       .HasColumnName("transactionMaterialTypeId");

            modelBuilder.Entity<TransactionMaterialType>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<Photo>()
            .Property(t => t.ID)
            .HasColumnName("photoId");

            modelBuilder.Entity<Photo>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<Comment>()
            .Property(t => t.ID)
            .HasColumnName("commentId");

            modelBuilder.Entity<Comment>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<TireType>()
            .Property(t => t.ID)
            .HasColumnName("tireTypeId");

            modelBuilder.Entity<TireType>().Ignore(t => t.Rowversion);

            modelBuilder.Entity<MaterialType>()
                      .Property(t => t.ID)
                      .HasColumnName("materialTypeId");

            modelBuilder.Entity<MaterialType>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<ScaleTicket>()
            .Property(t => t.ID)
            .HasColumnName("scaleTicketId");
            modelBuilder.Entity<ScaleTicket>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<ScaleTicketType>()
            .Property(t => t.ID)
            .HasColumnName("scaleTicketTypeId");

            modelBuilder.Entity<ScaleTicketType>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<MobileRegistrant>()
            .Property(t => t.ID)
            .HasColumnName("registrationNumber");
            modelBuilder.Entity<MobileRegistrant>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<Authorization>()
            .Property(t => t.ID)
            .HasColumnName("authorizationId");
            modelBuilder.Entity<Authorization>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<GpsLog>()
            .Property(t => t.ID)
            .HasColumnName("gpsLogId");
            modelBuilder.Entity<GpsLog>().Ignore(t => t.Rowversion);


            modelBuilder.Entity<TransactionEligibility>()
            .Property(t => t.ID)
            .HasColumnName("transactionEligibilityId");
            modelBuilder.Entity<TransactionEligibility>().Ignore(t => t.Rowversion);

            modelBuilder.Entity<Location>()
            .Property(t => t.ID)
            .HasColumnName("locationId");
            modelBuilder.Entity<Location>().Ignore(t => t.Rowversion);

            modelBuilder.Entity<Company>()
           .Property(t => t.ID)
           .HasColumnName("companyId");

            modelBuilder.Entity<ScaleTicket>().Property(c => c.InboundWeight).HasPrecision(13, 4);
            modelBuilder.Entity<ScaleTicket>().Property(c => c.OutboundWeight).HasPrecision(13, 4);

            modelBuilder.Entity<GpsLog>().Property(c => c.Latitude).HasPrecision(9, 6);
            modelBuilder.Entity<GpsLog>().Property(c => c.Longitude).HasPrecision(9, 6);

            modelBuilder.Entity<Photo>().Property(c => c.Latitude).HasPrecision(9, 6);
            modelBuilder.Entity<Photo>().Property(c => c.Longitude).HasPrecision(9, 6);

            modelBuilder.Entity<Company>().Ignore(t => t.Rowversion);

            modelBuilder.Entity<Authorization>().HasMany<Location>(r => r.Locations).WithMany(u => u.Authorizations).Map(m => { m.ToTable("STCAuthorization"); m.MapLeftKey("authorizationId"); m.MapRightKey("locationId"); });
           
            base.OnModelCreating(modelBuilder);
        }
    }
}
