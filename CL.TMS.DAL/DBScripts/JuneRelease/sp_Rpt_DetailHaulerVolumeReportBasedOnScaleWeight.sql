USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_DetailHaulerVolumeReportBasedOnScaleWeight]    Script Date: 6/28/2016 1:44:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_DetailHaulerVolumeReportBasedOnScaleWeight] 
	@startDate DateTime, @endDate DateTime
AS
BEGIN
	select 

	p.StartDate as 'ClaimPeriodStart',
	v.Number as 'RegistrationNumber',
	case when v.IsActive = 1
		then 'Active'
		else 'InActive'
	end as 'HaulerState',
	c.[Status]  as 'RecordState',
	'deliveries_to_ontario_processors' 'ReferenceTable',
	t.FriendlyId as 'PtrFormNumber',
	(select top 1 v.number from vendor v where v.id = t.IncomingId) as 'ProcessorNumber',
	(select 
		case when v.IsActive = 1
			then 'Active'
			else 'InActive'
		end 
	 from vendor v where v.id = t.IncomingId) as 'ProcessorState',
	(coalesce(transactionActualWeight.PLT,0) + coalesce(transactionActualWeight.MT,0) + coalesce(transactionActualWeight.AGLS,0) + coalesce(transactionActualWeight.IND,0) + coalesce(transactionActualWeight.SOTR,0) + coalesce(transactionActualWeight.MOTR,0) + coalesce(transactionActualWeight.LOTR,0) + coalesce(transactionActualWeight.GOTR,0)) as 'ScaleWeight',
	(coalesce(transactionActualWeight.AGLS,0) + coalesce(transactionActualWeight.IND,0) + coalesce(transactionActualWeight.SOTR,0) + coalesce(transactionActualWeight.MOTR,0) + coalesce(transactionActualWeight.LOTR,0) + coalesce(transactionActualWeight.GOTR,0)) as 'OtrTotalScaleWeight',
	coalesce(transactionActualWeight.PLT,0) 'PltScaleWeight',
	coalesce(transactionActualWeight.MT,0) 'MtScaleWeight',
	coalesce(transactionActualWeight.AGLS,0) 'AglsScaleWeight',
	coalesce(transactionActualWeight.IND,0) 'IndScaleWeight',
	coalesce(transactionActualWeight.SOTR,0) 'SotrScaleWeight',
	coalesce(transactionActualWeight.MOTR,0) 'MotrScaleWeight',
	coalesce(transactionActualWeight.LOTR,0) 'LotrScaleWeight',
	coalesce(transactionActualWeight.GOTR,0) 'GotrScaleWeight',
	coalesce(transactionAvgWeight.TotalTireWeight,0),
	(coalesce(transactionAvgWeight.AGLS,0)+ coalesce(transactionAvgWeight.IND,0)+ coalesce(transactionAvgWeight.SOTR,0)+ coalesce(transactionAvgWeight.MOTR,0)+ coalesce(transactionAvgWeight.LOTR,0)+ coalesce(transactionAvgWeight.GOTR,0)) 'OtrTotalEstweight',
	coalesce(transactionAvgWeight.PLT,0) 'PltEstWeight',
	coalesce(transactionAvgWeight.MT,0) 'MtEstWeight',
	coalesce(transactionAvgWeight.AGLS,0) 'AglsEstWeight',
	coalesce(transactionAvgWeight.IND,0) 'IndEstWeight',
	coalesce(transactionAvgWeight.SOTR,0) 'SotrEstWeight',
	coalesce(transactionAvgWeight.MOTR,0) 'MotrEstWeight',
	coalesce(transactionAvgWeight.LOTR,0) 'LotrEstWeight',
	coalesce(transactionAvgWeight.GOTR,0) 'GotrEstWeight'
	from claim c 
	inner join [Period] p on p.id = c.ClaimPeriodID
	inner join [Vendor] v on v.id = c.ParticipantID 
	inner join claimdetail cd on c.id = cd.claimid 
	inner join [transaction] t on t.id = cd.TransactionId 
	inner join
	(
		select piv.ClaimId, piv.TransactionId, piv.PeriodName, (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  TotalTireWeight, 
		sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
		from
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
			from
			(
				select cpd.* 
				from tmdb.dbo.ClaimPaymentDetail cpd 	
				where cpd.TransactionTypeCode = 'ptr'
			) src
			PIVOT
			(
				sum([AverageWeight])
				for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
			) piv	 	
		) piv
		group by piv.ClaimId, piv.TransactionId, piv.PeriodName
	) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID 

	inner join
	(
		select piv.ClaimId, piv.TransactionId, piv.PeriodName, (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  TotalTireWeight, 
		sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
		from
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
			from
			(
				select cpd.* 
				from tmdb.dbo.ClaimPaymentDetail cpd 	
				where cpd.TransactionTypeCode = 'ptr'
			) src
			PIVOT
			(
				sum([ActualWeight])
				for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
			) piv	 	
		) piv
		group by piv.ClaimId, piv.TransactionId, piv.PeriodName
	) as transactionActualWeight on transactionActualWeight.TransactionId = t.ID 

	where transactionTypecode = 'ptr' and v.VendorType = 3
	and (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate)
	order by p.StartDate, ReferenceTable, v.Number, ProcessorNumber;

END

