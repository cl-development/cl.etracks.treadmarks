IF NOT EXISTS(select * from AppSetting where [Key] = 'Settings.TSFMonthYearSelectionDate') 
begin 
	insert into AppSetting ([Key],Value) values('Settings.TSFMonthYearSelectionDate','2016-06-01');
end

IF NOT EXISTS(select * from AppSetting where [Key] = 'Settings.TSFNegAdjSwitchDate') 
begin 
	insert into AppSetting ([Key],Value) values('Settings.TSFNegAdjSwitchDate','2016-07-01');
end

alter table TSFClaimDetail add TSFRateDate datetime null;

alter table claimpayment drop column transactionId;


CREATE TABLE [dbo].[ClaimPaymentDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimId] [int] NOT NULL,
	[TransactionId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ItemType] [int] NOT NULL,
	[ItemName] [nvarchar](20) NOT NULL,
	[Rate] [decimal](18, 3) NOT NULL,
	[PaymentType] [int] NOT NULL,
	[UnitType] [int] NOT NULL,
	[TransactionTypeCode] [nvarchar](50) NOT NULL,
	[ClaimPeriodId] [int] NOT NULL,
	[PeriodName] [nvarchar](20) NOT NULL,
	[VendorId] [int] NOT NULL,
	[VendorType] [int] NOT NULL,
	[BusinessName] [nvarchar](65) NOT NULL,
	[RegistrationNumber] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
	[PeriodStartDate] [datetime] NOT NULL,
	[PeriodEndDate] [datetime] NOT NULL,
	[AverageWeight] [decimal](18, 4) NOT NULL,
	[Amount] [decimal](18, 3) NOT NULL,
	[Quantity] [int] NOT NULL,
	[ActualWeight] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_ClaimPaymentDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClaimPaymentDetail]  WITH CHECK ADD  CONSTRAINT [FK_ClaimPaymentDetail_Claim] FOREIGN KEY([ClaimId])
REFERENCES [dbo].[Claim] ([ID])
GO

ALTER TABLE [dbo].[ClaimPaymentDetail] CHECK CONSTRAINT [FK_ClaimPaymentDetail_Claim]
GO

ALTER TABLE [dbo].[ClaimPaymentDetail]  WITH CHECK ADD  CONSTRAINT [FK_ClaimPaymentDetail_Transaction] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[Transaction] ([ID])
GO

ALTER TABLE [dbo].[ClaimPaymentDetail] CHECK CONSTRAINT [FK_ClaimPaymentDetail_Transaction]
GO

--== OTSTM2-57 ==--
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'NegativeAdjDate' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD NegativeAdjDate Datetime
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CreditItemID' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD CreditItemID int not null default (0)
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CreditNegativeAdj' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD CreditNegativeAdj int not null default (0)
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CreditTSFRate' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD CreditTSFRate decimal(18,3) not null default (0)
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CreditTSFDue' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD CreditTSFDue decimal(18,3) not null default (0)
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'Credit' AND OBJECT_ID = OBJECT_ID(N'TSFClaim')) ALTER TABLE TSFClaim ADD Credit decimal(18,3)
GO
--== OTSTM2-57 ==--

--== OTSTM2-302 ==--
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'TSFRateDate' AND OBJECT_ID = OBJECT_ID(N'TSFClaimDetail')) ALTER TABLE TSFClaimDetail ADD TSFRateDate Datetime
GO
--== OTSTM2-302 ==--

Insert into [Role] (Name,[Description]) values ('AccountManager','AccountManager')


--== OTSTM2-131 ==--
alter table ClaimReuseTires
drop COLUMN ReuseTiresValue

-- OTSTM2-311 --
CREATE TABLE [dbo].[ClaimInventory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClaimId] [int] NOT NULL,
	[VendorId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[IsEligible] [bit] NOT NULL,
	[Qty] [int] NOT NULL,
	[Weight] [decimal](18, 4) NOT NULL,
	[ActualWeight] [decimal](18, 4) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_ClaimInventory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

UPDATE "rate" SET "EffectiveEndDate"='2015-04-30' WHERE  "ID"=27;
SELECT "ID", "ItemID", "ItemType", "PaymentType", "ClaimType", "PeriodID", "SourceZoneID", "DeliveryZoneID", "GreaterZone", "ItemRate", "RateDescription", "ModifiedDate", "ModifiedBy", "EffectiveStartDate", "EffectiveEndDate", "RowVersion" FROM "rate" WHERE  "ID"=27;
UPDATE "rate" SET "EffectiveStartDate"='2015-05-01' WHERE  "ID"=28;
SELECT "ID", "ItemID", "ItemType", "PaymentType", "ClaimType", "PeriodID", "SourceZoneID", "DeliveryZoneID", "GreaterZone", "ItemRate", "RateDescription", "ModifiedDate", "ModifiedBy", "EffectiveStartDate", "EffectiveEndDate", "RowVersion" FROM "rate" WHERE  "ID"=28;
UPDATE "rate" SET "EffectiveStartDate"='2015-05-01' WHERE  "ID"=46;
SELECT "ID", "ItemID", "ItemType", "PaymentType", "ClaimType", "PeriodID", "SourceZoneID", "DeliveryZoneID", "GreaterZone", "ItemRate", "RateDescription", "ModifiedDate", "ModifiedBy", "EffectiveStartDate", "EffectiveEndDate", "RowVersion" FROM "rate" WHERE  "ID"=46;

GO