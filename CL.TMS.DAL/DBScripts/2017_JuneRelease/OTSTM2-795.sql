﻿USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_Rpt_RegistrantWeeklyReport]    Script Date: 5/30/2017 10:02:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_Rpt_RegistrantWeeklyReport] 
AS
BEGIN

	select *
	from
	(
		select 	
		v.ID, v.VendorType as RegistrantType, v.Number as RegistrationNumber, v.BusinessName, v.FranchiseName, v.OperatingName, v.BusinessNumber, v.TerritoryCode, v.GlobalDimension1Code, v.GlobalDimension2Code, v.CreditLimit, v.TelexAnswerBack, v.TaxRegistrationNo, v.TaxRegistrationNo2, v.HomePage, v.BusinessGroupCode, v.CustomerGroupCode, v.DepartmentCode, v.ProjectCode, v.PurchaserCode, v.SalesCampaignCode, v.SalesPersonCode, v.AuthSigComplete, v.ReceivedDate, v.GracePeriodStartDate, v.GracePeriodEndDate, v.CommercialLiabInsurerName, v.CommercialLiabInsurerExpDate, v.WorkerHealthSafetyCertNo, v.PermitsCertDescription, v.AmendedAgreement, v.AmendedDate, v.IsTaxExempt, v.UpdateToFinancialSoftware, v.PrimaryBusinessActivity, v.MailingAddressSameAsBusiness, v.BusinessActivityID, v.GSTNumber, v.AuthorizedSigComplete, v.LocationEmailAddress, 
		coalesce(v.DateReceived, (select top 1 SubmittedDate from [application] a where a.id = v.ApplicationID)) as DateReceived, 
		v.ActivationDate, 
		coalesce(v.ConfirmationMailedDate, v.CreatedDate) as 'ConfirmationMailedDate', 	
		v.PreferredCommunication, v.RegistrantTypeId, v.RegistrantSubTypeId, v.LastUpdatedBy, v.LastUpdatedDate, v.CheckoutNum, v.CreatedDate, v.InBatch, v.IsActive, vHistory.maxChangeDate as ActiveStateChangeDate, case when v.IsActive = 0 then case when vHistory.Reason = 'Other' then vHistory.OtherReason else vHistory.Reason end else '' end as InactivationReason, vHistory.[Description] as NewRegNumber, 
		businessAddress.*,
		mailingAddress.*,
		primaryContact.*,
		coalesce([PLT], 0) as PLT, coalesce([MT], 0) as MT, coalesce([AGLS], 0) as AGLS, coalesce([IND], 0) as IND, coalesce([SOTR], 0) as SOTR, coalesce([MOTR], 0) as MOTR, coalesce([LOTR], 0) as LOTR, coalesce([GOTR], 0) as GOTR
		from vendor v left join 
		(
			select his.VendorID, maxChangeDate, ActiveState, Reason, OtherReason, Description, CreateDate
			from 
			(
				select VendorID, MAX(ActiveStateChangeDate)as [maxChangeDate], MAX(CreateDate)as [maxCreateDate]
				from VendorActiveHistory his 
				where his.ActiveState = 0 and his.IsValid = 1 and ActiveStateChangeDate < GETDATE() group by VendorID
			) as validInactiveActs left join VendorActiveHistory as his on his.VendorID = validInactiveActs.VendorID and his.ActiveStateChangeDate = validInactiveActs.maxChangeDate and validInactiveActs.maxCreateDate=his.CreateDate
			where IsValid = 1 and ActiveState = 0 
		) as vHistory on vHistory.VendorID = v.ID left join 
		(
			select va.VendorID as PrimaryContactVendorID, 
			c.[AddressID],case when c.[FirstName]=c.[LastName] then c.[FirstName] else CONCAT(c.[FirstName],' ',c.[LastName]) end as [Name],c.[Position],c.[Email],c.[IsPrimary],c.[PhoneNumber],c.[AlternatePhoneNumber],c.[FaxNumber],c.[PreferredContactMethod],c.[ContactAddressSameAsBusiness],c.[Ext],c.[FirstName],c.[LastName],
			a.ID as PrimaryContactID, a.AddressType as PrimaryContactAddressType, a.Address1 as PrimaryContactAddress1, a.Address2 as PrimaryContactAddress2,a.Address3 as PrimaryContactAddress3, a.City as PrimaryContactCity, a.Province as PrimaryContactProvince,a.PostalCode as PrimaryContactPostalCode, a.Country as PrimaryContactCountry, coalesce(c.PhoneNumber,a.Phone) as PrimaryContactPhone, a.Fax as PrimaryContactFax, a.Ext as PrimaryContactExt, a.Email as PrimaryContactEmail
			from VendorAddress va inner join [Contact] c on va.AddressID = c.AddressID left join 
			[Address] a on a.ID = va.AddressID
			where c.IsPrimary = 1
		) as primaryContact on v.ID = primaryContact.PrimaryContactVendorID left join 
		(
			select va.VendorID as BusinessVendorID, 
			a.ID as BusinessID, a.AddressType as BusinessAddressType, a.Address1 as BusinessAddress1, a.Address2 as BusinessAddress2,a.Address3 as BusinessAddress3, a.City as BusinessCity, a.Province as BusinessProvince,a.PostalCode as BusinessPostalCode, a.Country as BusinessCountry, a.Phone as BusinessPhone, a.Fax as BusinessFax, a.Ext as BusinessExt, a.Email as BusinessEmail
			from VendorAddress va inner join [Address] a on va.AddressID = a.ID
			where a.AddressType = 1
		) as businessAddress on v.ID = businessAddress.BusinessVendorID left join
		(
			select va.VendorID as MailingAddressID, 
			a.ID as MailingID, a.AddressType as MailingAddressType, a.Address1 as MailingAddress1, a.Address2 as MailingAddress2,a.Address3 as MailingAddress3, a.City as MailingCity, a.Province as MailingProvince,a.PostalCode as MailingPostalCode, a.Country as MailingCountry, a.Phone as MailingPhone, a.Fax as MailingFax, a.Ext as MailingExt, a.Email as MailingEmail,a.RowVersion as MailingRowVersion
			from VendorAddress va inner join [Address] a on va.AddressID = a.ID
			where a.AddressType = 2
		) as mailingAddress on v.ID = mailingAddress.MailingAddressID left join 
		(
			select itemVendorId, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from 
			(
				select piv.ItemVendorID, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select vi.VendorID as ItemVendorID, i.*
					from VendorItem vi left join Item i on i.id = vi.ItemID
				) src
				PIVOT
				(
					count(ShortName)
					for ShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv
			) as items
			group by itemVendorID
		) as items on items.ItemVendorID = v.ID

		union

		select 	c.ID, c.CustomerType as RegistrantType, c.RegistrationNumber, c.BusinessName, c.FranchiseName, c.OperatingName, c.BusinessNumber, c.TerritoryCode, c.GlobalDimension1Code, c.GlobalDimension2Code, c.CreditLimit, c.TelexAnswerBack, c.TaxRegistrationNo, c.TaxRegistrationNo2, c.HomePage, c.BusinessGroupCode, c.CustomerGroupCode, c.DepartmentCode, c.ProjectCode, c.PurchaserCode, c.SalesCampaignCode, c.SalesPersonCode, c.AuthSigComplete, c.ReceivedDate, c.GracePeriodStartDate, c.GracePeriodEndDate, c.CommercialLiabInsurerName, c.CommercialLiabInsurerExpDate, c.WorkerHealthSafetyCertNo, c.PermitsCertDescription, c.AmendedAgreement, c.AmendedDate, c.IsTaxExempt, c.UpdateToFinancialSoftware, c.PrimaryBusinessActivity, c.MailingAddressSameAsBusiness, c.BusinessActivityID, c.GSTNumber, c.AuthorizedSigComplete, c.LocationEmailAddress, 
		coalesce(c.DateReceived, (select top 1 SubmittedDate from [application] a where a.id = c.ApplicationID)) as 'DateReceived', 	
		c.ActivationDate, 
		coalesce(c.ConfirmationMailedDate, c.CreatedDate) as 'ConfirmationMailedDate', 		
		c.PreferredCommunication, c.RegistrantTypeId, c.RegistrantSubTypeId, c.LastUpdatedBy, c.LastUpdatedDate, c.CheckoutNum, c.CreatedDate, c.InBatch, c.IsActive, vHistory.maxChangeDate as ActiveStateChangeDate, case when c.IsActive = 0 then case when vHistory.Reason = 'Other' then vHistory.OtherReason else vHistory.Reason end else '' end as InactivationReason, vHistory.[Description] as NewRegNumber,
		businessAddress.*,
		mailingAddress.*,
		primaryContact.*,
		coalesce([PLT], 0) as PLT, coalesce([MT], 0) as MT, coalesce([AGLS], 0) as AGLS, coalesce([IND], 0) as IND, coalesce([SOTR], 0) as SOTR, coalesce([MOTR], 0) as MOTR, coalesce([LOTR], 0) as LOTR, coalesce([GOTR], 0) as GOTR
		from customer c left join 
		(
			select his.CustomerID, maxChangeDate, ActiveState, Reason, OtherReason, Description, CreateDate
			from 
			(
				select CustomerID, MAX(ActiveStateChangeDate)as [maxChangeDate], MAX(CreateDate)as [maxCreateDate]
				from CustomerActiveHistory his 
				where his.ActiveState = 0 and his.IsValid = 1 and ActiveStateChangeDate < GETDATE() group by CustomerID
			) as validInactiveActs left join CustomerActiveHistory as his on his.CustomerID = validInactiveActs.CustomerID and his.ActiveStateChangeDate = validInactiveActs.maxChangeDate and validInactiveActs.maxCreateDate=his.CreateDate
			where IsValid = 1 and ActiveState = 0 
		)  as vHistory on vHistory.CustomerID = c.ID left join 
		(
			select ca.CustomerID as PrimaryContactVendorID, 
			c.[AddressID],case when c.[FirstName]=c.[LastName] then c.[FirstName] else CONCAT(c.[FirstName],' ',c.[LastName]) end as [Name],c.[Position],c.[Email],c.[IsPrimary],c.[PhoneNumber],c.[AlternatePhoneNumber],c.[FaxNumber],c.[PreferredContactMethod],c.[ContactAddressSameAsBusiness],c.[Ext],c.[FirstName],c.[LastName],
			a.ID as PrimaryContactID, a.AddressType as PrimaryContactAddressType, a.Address1 as PrimaryContactAddress1, a.Address2 as PrimaryContactAddress2,a.Address3 as PrimaryContactAddress3, a.City as PrimaryContactCity, a.Province as PrimaryContactProvince,a.PostalCode as PrimaryContactPostalCode, a.Country as PrimaryContactCountry, coalesce(c.PhoneNumber,a.Phone) as PrimaryContactPhone, a.Fax as PrimaryContactFax, a.Ext as PrimaryContactExt, a.Email as PrimaryContactEmail
			from CustomerAddress ca inner join [Contact] c on ca.AddressID = c.AddressID left join 
			[Address] a on a.ID = ca.AddressID
			where c.IsPrimary = 1
		) as primaryContact on c.ID = primaryContact.PrimaryContactVendorID left join 
		(
			select ca.CustomerID as BusinessCustomerID,
			a.ID as BusinessID, a.AddressType as BusinessAddressType, a.Address1 as BusinessAddress1, a.Address2 as BusinessAddress2,a.Address3 as BusinessAddress3, a.City as BusinessCity, a.Province as BusinessProvince,a.PostalCode as BusinessPostalCode, a.Country as BusinessCountry, a.Phone as BusinessPhone, a.Fax as BusinessFax, a.Ext as BusinessExt, a.Email as BusinessEmail
			from CustomerAddress ca inner join [Address] a on ca.AddressID = a.ID
			where a.AddressType = 1
		) as businessAddress on c.ID = businessAddress.BusinessCustomerID left join
		(
			select ca.CustomerID as MailingCustomerID, 
			a.ID as MailingID, a.AddressType as MailingAddressType, a.Address1 as MailingAddress1, a.Address2 as MailingAddress2,a.Address3 as MailingAddress3, a.City as MailingCity, a.Province as MailingProvince,a.PostalCode as MailingPostalCode, a.Country as MailingCountry, a.Phone as MailingPhone, a.Fax as MailingFax, a.Ext as MailingExt, a.Email as MailingEmail,a.RowVersion as MailingRowVersion
			from CustomerAddress ca inner join [Address] a on ca.AddressID = a.ID
			where a.AddressType = 2
		) as mailingAddress on c.ID = mailingAddress.MailingCustomerID left join
		(
			select itemCustomerId, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from 
			(
				select piv.ItemCustomerID, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select ci.CustomerID as ItemCustomerID, i.*
					from CustomerItem ci left join Item i on i.id = ci.ItemID
				) src
				PIVOT
				(
					count(ShortName)
					for ShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv
			) as items
			group by itemCustomerID
		) as items on items.ItemCustomerID = c.ID
	) main 
	order by CreatedDate asc, RegistrationNumber;

END
GO