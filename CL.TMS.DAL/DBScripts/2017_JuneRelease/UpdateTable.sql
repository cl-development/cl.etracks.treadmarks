﻿USE [tmdb]
go

--== OTSTM2-838 begin ==--

IF COL_LENGTH('[AppResource]', 'ApplicableLevel') IS NULL
BEGIN
    ALTER TABLE AppResource ADD ApplicableLevel varchar(10) not null DEFAULT '111'
END
go


--===========================================================================================================================================================================--
--============================================ START REPORTS ================================================================================================================--
--===========================================================================================================================================================================--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 0 and Name like '%Admin/Reports%' and ResourceMenu = 'Admin/Reports' and ResourceScreen = 'Admin/Reports' --ApplicableLevel = '110


--===========================================================================================================================================================================--
--============================================ START REPORTS ================================================================================================================--
--===========================================================================================================================================================================--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 0 and Name like '%Reports%' and ResourceMenu = 'Reports' and ResourceScreen = 'Reports' --ApplicableLevel = '110

update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 1 and Name like '%All Reports%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110

--Steward--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Steward%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110

--Collector--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Collector%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110

--Hauler--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Hauler%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110

--Processor--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Processor%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110

--RPM--
update AppResource
set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%RPM%' and ResourceMenu = 'Reports' and ResourceScreen = 'All Reports' --ApplicableLevel = '110


--===========================================================================================================================================================================--
--============================================ START APPLICATION ============================================================================================================--
--===========================================================================================================================================================================--

--======================================= RPM Application ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Submission Information%' and ResourceMenu = 'Applications' and ResourceScreen = 'RPM Application' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Terms And Conditions%' and ResourceMenu = 'Applications' and ResourceScreen = 'RPM Application' --ApplicableLevel = '110


--======================================= Processor Application ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Submission Information%' and ResourceMenu = 'Applications' and ResourceScreen = 'Processor Application' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Terms And Conditions%' and ResourceMenu = 'Applications' and ResourceScreen = 'Processor Application' --ApplicableLevel = '110


--======================================= Hauler Application ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Submission Information%' and ResourceMenu = 'Applications' and ResourceScreen = 'Hauler Application' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Terms And Conditions%' and ResourceMenu = 'Applications' and ResourceScreen = 'Hauler Application' --ApplicableLevel = '110


--======================================= Collector Application ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Submission Information%' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Terms And Conditions%' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application' --ApplicableLevel = '110

--======================================= Steward Application ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Submission Information%' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Terms And Conditions%' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application' --ApplicableLevel = '110


--===========================================================================================================================================================================--
--============================================ START TSF REMITTANCE SUMMARY =================================================================================================--
--===========================================================================================================================================================================--

--======================================= TSF Remittance ================================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Status%' and ResourceMenu = 'Remittance' and ResourceScreen = 'TSF Remittance' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Payments%' and ResourceMenu = 'Remittance' and ResourceScreen = 'TSF Remittance' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Activities%' and ResourceMenu = 'Remittance' and ResourceScreen = 'TSF Remittance' --ApplicableLevel = '110


--===========================================================================================================================================================================--
--============================================ CLAIM ========================================================================================================================--
--===========================================================================================================================================================================--

--======================================= RPM Claim Summary ========================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Activities%' and ResourceMenu = 'Claims' and ResourceScreen = 'RPM Claim Summary' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Payments And Adjustments%' and ResourceMenu = 'Claims' and ResourceScreen = 'RPM Claim Summary' --ApplicableLevel = '110



--======================================= Processor Claim Summary ========================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Activities%' and ResourceMenu = 'Claims' and ResourceScreen = 'Processor Claim Summary' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Payments And Adjustments%' and ResourceMenu = 'Claims' and ResourceScreen = 'Processor Claim Summary' --ApplicableLevel = '110


--======================================= Hauler Claim Summary =============================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Activities%' and ResourceMenu = 'Claims' and ResourceScreen = 'Hauler Claim Summary' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Payments And Adjustments%' and ResourceMenu = 'Claims' and ResourceScreen = 'Hauler Claim Summary' --ApplicableLevel = '110


--======================================= Collector Claim Summary ============================================================================--
update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Activities%' and ResourceMenu = 'Claims' and ResourceScreen = 'Collector Claim Summary'  --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Reuse Tires%' and ResourceMenu = 'Claims' and ResourceScreen = 'Collector Claim Summary' --ApplicableLevel = '110

update AppResource set ApplicableLevel = '110'
where ResourceLevel = 2 and Name like '%Payments And Adjustments%' and ResourceMenu = 'Claims' and ResourceScreen = 'Collector Claim Summary' --ApplicableLevel = '110

--============================================================= END ==========================================================================================================--

go
--== OTSTM2-838 end ==--



--== OTSTM2-630 begin ==--
--if ((select COUNT(*) from [AppResource]) = 173) 
--begin
--declare @CreateBy nvarchar(1000);
--declare @dt datetime;
--set @CreateBy = 'System'
--set @dt = GETDATE()
--insert into [AppResource]	(Name,			ResourceMenu,			ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
--			VALUES			('Activities',	'Applications',			'Steward Application',		2,				131123,			@CreateBy,	@dt,		@dt),
--							('Activities',	'Applications',			'Collector Application',	2,				131222,			@CreateBy,	@dt,		@dt),
--							('Activities',	'Applications',			'Hauler Application',		2,				131322,			@CreateBy,	@dt,		@dt),
--							('Activities',	'Applications',			'Processor Application',	2,				131422,			@CreateBy,	@dt,		@dt),
--							('Activities',	'Applications',			'RPM Application',			2,				131522,			@CreateBy,	@dt,		@dt)

--end
--go

----add permission to role SuperAdmin NOTE: new [AppResource].Id may different in different DB
--DECLARE @RoleID INT, @AppResourceId INT, @AppPermissionId INT, @iCounter INT;
--DECLARE @getResIDs CURSOR
--SET @getResIDs = CURSOR FOR select Id from [AppResource] where Name = 'Activities' and ResourceMenu='Applications'
--set @iCounter = 0

--(select @AppPermissionId = (select Id from AppPermission where Name = 'EditSave'))
--(select @RoleID = (select Id from [Role] where Name = 'SuperAdmin'))

--OPEN @getResIDs
--FETCH NEXT
--FROM @getResIDs INTO @AppResourceId
--WHILE @@FETCH_STATUS = 0
--BEGIN
--	IF not EXISTS (SELECT * from RolePermission rp WHERE rp.RoleID = @RoleId and rp.AppResourceId = @AppResourceId and rp.AppPermissionId = @AppPermissionId)
--	BEGIN
--		insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, @AppResourceId, @AppPermissionId)
--		--select @iCounter , @RoleID, @AppResourceId, @AppPermissionId
--		set @iCounter = @iCounter + 1;
--	END
--	FETCH NEXT
--	FROM @getResIDs INTO @AppResourceId
--END
--CLOSE @getResIDs

--go
--== OTSTM2-630 end ==--
