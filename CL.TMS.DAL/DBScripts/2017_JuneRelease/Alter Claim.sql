ALTER TABLE "Claim"
	DROP COLUMN "AuidtOnhold";

ALTER TABLE "Claim"
	ADD "AuditOnhold" BIT NULL;
