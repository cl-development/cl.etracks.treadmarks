﻿--------------------- note: this is moved to Jun release BEGIN --------------------- 
--== OTSTM2-587 begin ==--
IF NOT EXISTS(select * from Report where [ReportName] = 'Hauler Tire Collection Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(32,'Hauler Tire Collection Report','3');
end
go
-- RptClaimInventoryDetail.TransactionProcessingStatus was out of sync
update RptClaimInventoryDetail set [TransactionProcessingStatus] = t.ProcessingStatus
from [Transaction] t join RptClaimInventoryDetail r on r.TransactionId=t.ID where r.TransactionProcessingStatus != t.ProcessingStatus
go
--== OTSTM2-587 end ==--
--------------------- note: this is moved to Jun release END --------------------- 
