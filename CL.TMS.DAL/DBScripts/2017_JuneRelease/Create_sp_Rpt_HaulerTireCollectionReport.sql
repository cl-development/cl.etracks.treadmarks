﻿USE [tmdb]
GO

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_Rpt_HaulerTireCollectionReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].sp_Rpt_HaulerTireCollectionReport
END

/****** Object:  StoredProcedure [dbo].[sp_Rpt_HaulerTireCollectionReport]    Script Date:  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Rpt_HaulerTireCollectionReport] 
	@startDate DateTime, @endDate DateTime, @regNumber nvarchar(256)
AS
BEGIN

	SELECT 
	convert(nvarchar(10), main.ReportPeriod, 101) as 'ReportingPeriod',
	1 as 'SubmissionNumber',
	main.HaulerRegistrationNumber,
	main.HaulerRegistrationName,
	main.HaulerStatus,
	main.RecordState,
	main.ClaimPeriod,
	main.TransactionStatus,
	main.TransactionDate,
	main.TransactionType,
	main.ClaimState,
	main.TransactionNbr,
	sum(main.PLT) as PLT,
	sum(main.MT) as MT,
	sum(main.AGLS) as AGLS,
	sum(main.IND) as IND,
	sum(main.SOTR) as SOTR,
	sum(main.MOTR) as MOTR,
	sum(main.LOTR) as LOTR,
	sum(main.GOTR) as GOTR,
	sum(main.PLTWeight) as PLTWeight,
	sum(main.MTWeight) as MTWeight,
	sum(main.AGLSWeight) as AGLSWeight,
	sum(main.INDWeight) as INDWeight,
	sum(main.SOTRWeight) as SOTRWeight,
	sum(main.MOTRWeight) as MOTRWeight,
	sum(main.LOTRWeight) as LOTRWeight,
	sum(main.GOTRWeight) as GOTRWeight
	FROM
	(
		select  
		p.ShortName as 'ClaimPeriod',
		p.startDate as 'ReportPeriod',
		v.Number as 'HaulerRegistrationNumber',
		v.BusinessName as 'HaulerRegistrationName',
		t.ProcessingStatus as 'TransactionStatus',
		c.[Status] as 'ClaimState',
		t.[TransactionTypeCode] as 'TransactionType',
		t.[TransactionDate] as 'TransactionDate',
		t.[FriendlyId] as 'TransactionNbr',
		case when v.IsActive = 1
			then 'Active'	
			else 'InActive'
		end as 'HaulerStatus',
		c.[Status] as 'RecordState',
		coalesce(transactionTireCount.PLT,0) as 'PLT',
		coalesce(transactionTireCount.MT,0) as 'MT',
		coalesce(transactionTireCount.AGLS,0) as 'AGLS',
		coalesce(transactionTireCount.IND,0) as 'IND',
		coalesce(transactionTireCount.SOTR,0) as 'SOTR',
		coalesce(transactionTireCount.MOTR,0) as 'MOTR',
		coalesce(transactionTireCount.LOTR,0) as 'LOTR',
		coalesce(transactionTireCount.GOTR,0) as 'GOTR',
		coalesce(transactionAvgWeight.PLT,0) 'PltWeight',
		coalesce(transactionAvgWeight.MT,0) 'MtWeight',
		coalesce(transactionAvgWeight.AGLS,0) 'AglsWeight',
		coalesce(transactionAvgWeight.IND,0) 'IndWeight',
		coalesce(transactionAvgWeight.SOTR,0) 'SotrWeight',
		coalesce(transactionAvgWeight.MOTR,0) 'MotrWeight',
		coalesce(transactionAvgWeight.LOTR,0) 'LotrWeight',
		coalesce(transactionAvgWeight.GOTR,0) 'GotrWeight'
		from claim c
		inner join [Period] p on p.id = c.ClaimPeriodID
		inner join [Vendor] v on v.id = c.ParticipantID 
		inner join claimdetail cd on c.id = cd.claimid 
		inner join [transaction] t on t.id = cd.TransactionId 
		inner join
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  TotalTireWeight, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName, 
					coalesce([PLT],0) as 'PLT', 
					coalesce([MT],0) as 'MT', 
					coalesce([AGLS],0) as 'AGLS', 
					coalesce([IND],0) as 'IND', 
					coalesce([SOTR],0) as 'SOTR', 
					coalesce([MOTR],0) as 'MOTR', 
					coalesce([LOTR],0) as 'LOTR', 
					coalesce([GOTR],0) as 'GOTR'
				from
				(
					select rcid.* 
					from tmdb.dbo.RptClaimInventoryDetail rcid 	
					where rcid.TransactionTypeCode in ('tcr', 'dot', 'stc', 'ucr', 'hit')
					and rcid.Direction = 1 --rcid.TransactionProcessingStatus != 'Invalidated'
				) src
				PIVOT
				(
					sum([AverageWeight])
					for ItemShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName
		) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID and transactionAvgWeight.ClaimId = c.id
		inner join
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  TotalTireWeight, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName, 
					coalesce([PLT],0) as 'PLT', 
					coalesce([MT],0) as 'MT', 
					coalesce([AGLS],0) as 'AGLS', 
					coalesce([IND],0) as 'IND', 
					coalesce([SOTR],0) as 'SOTR', 
					coalesce([MOTR],0) as 'MOTR', 
					coalesce([LOTR],0) as 'LOTR', 
					coalesce([GOTR],0) as 'GOTR'
				from
				(
					select rcid.* 
					from tmdb.dbo.RptClaimInventoryDetail rcid 	
					where rcid.TransactionTypeCode in ('TCR', 'DOT', 'STC', 'UCR', 'HIT')
					and rcid.Direction = 1 --rcid.TransactionProcessingStatus != 'Invalidated'
				) src
				PIVOT
				(
					sum([Quantity])
					for ItemShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName

		) as transactionTireCount on transactionTireCount.TransactionId = t.ID and transactionTireCount.ClaimId = c.id
		where v.VendorType = 3 
		and (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate) AND ((@regNumber IS NULL) OR (@regNumber IS NOT NULL AND (v.Number =  CAST( @regNumber AS nvarchar(max)))))
	) as main
	group by main.ReportPeriod, main.HaulerRegistrationNumber, main.HaulerRegistrationName, main.HaulerStatus, main.RecordState, main.ClaimPeriod, main.TransactionStatus, main.TransactionDate, main.TransactionType, main.ClaimState, main.TransactionNbr
	order by main.ReportPeriod, main.HaulerRegistrationNumber;
END

GO