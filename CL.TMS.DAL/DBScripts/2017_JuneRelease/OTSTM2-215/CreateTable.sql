﻿USE [tmdb]

--------------== OTSTM2-215 ==------------------------

CREATE TABLE [dbo].[Region](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[RegionDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FSA] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NOT NULL,
	[RegionID] [int] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_RegionDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RegionDetail]  WITH CHECK ADD  CONSTRAINT [FK_RegionDetail_Region] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Region] ([ID])
GO

ALTER TABLE [dbo].[RegionDetail] CHECK CONSTRAINT [FK_RegionDetail_Region]
GO

---------------== OTSTM2-215 ==--------------------

GO

