INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppDOT', '0');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppHIT', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppPIT', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppPTR', '0');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppSTC', '0');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppTCR', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBAutoAppUCR', '0');

