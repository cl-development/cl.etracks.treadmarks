﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_TopTenStewards]    Script Date: 5/17/18 2:27:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--  Name:        sp_Rpt_TopTenStewards
--  Author:		 Hefen Zhou
--  Create date: 05-15-2018
--  Description: Store procedure for loading Top Ten Stewards of Revenue (per Period)
-- =============================================
Create PROCEDURE [dbo].[sp_Rpt_TopTenStewards]
	@startDate DateTime='2000-01-01'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	declare @periodTotalAmount int, @thisYear int, @thisMonth int, 	@preYear int, @preMonth int
	set @thisYear=year(@startDate);
	set @thisMonth=month(@startDate);
	set @preYear=case 
					when @thisMonth=1
					then @thisYear-1
					else @thisYear
				 end
	set @preMonth=case 
					when @thisMonth=1
					then 12
					else @thisMonth-1
				 end	
	-- current period total revenue
	set @periodTotalAmount=(select coalesce((sum(tc.TSFDue)-sum(tc.CreditTSFDue)),0) 
							from TSFClaimDetail tc		
							join TSFClaim t on tc.TSFClaimID=t.ID	
							join [Period] p on t.PeriodID=p.ID	
							where p.PeriodType=1 
								and year(p.StartDate)=@thisYear
								and month(p.StartDate)=@thisMonth						
							)   

	-- current period top 10 stewards
	select top 10 
		 c.RegistrationNumber as [RNumber]
		,c.BusinessName as Steward
		,c.RegistrantSubTypeId as StewardSubTypeID	
		,coalesce((sum(tcd.TSFDue)-sum(tcd.CreditTSFDue)),0) as Amount
	into #top10
	from TSFClaimDetail tcd			
		join TSFClaim tc on tcd.TSFClaimID=tc.ID
		join Customer c on tc.CustomerID=c.id
		join [Period] pc on tc.PeriodID=pc.ID	
	where pc.PeriodType=1 
		and year(pc.StartDate)=@thisYear
		and month(pc.StartDate)=@thisMonth	
	group by c.RegistrationNumber,c.BusinessName,c.RegistrantSubTypeId
	order by Amount desc	

	-- pre period stewards base on current top 10 
	select c_p.RegistrationNumber as cuNumber
		,coalesce((sum(tcd_p.TSFDue)-sum(tcd_p.CreditTSFDue)),0) as Amount_p
	into #perTop10
	from TSFClaimDetail tcd_p			
		join TSFClaim tc_p on tcd_p.TSFClaimID=tc_p.ID
		join Customer c_p on tc_p.CustomerID=c_p.id
		join [Period] pp on tc_p.PeriodID=pp.ID	
	where pp.PeriodType=1 
		and year(pp.StartDate)=@preYear
		and month(pp.StartDate)=@preMonth
	    and c_p.RegistrationNumber in ( select RNumber from #top10	)
	group by c_p.RegistrationNumber 
		
	-- combine result
	select 
		 ct.Steward
		,ct.StewardSubTypeID
		,ct.Amount
		,case 
			when  @periodTotalAmount<>0
			then  ct.Amount/@periodTotalAmount 
			else  0
		 end as TSFPercentage
		,case
			when pt.Amount_p<>0
			then (ct.Amount- pt.Amount_p)/pt.Amount_p
			else 1
		 end as ChangePercentage
	from  #top10 ct
	left join #perTop10 pt on ct.RNumber=pt.cuNumber		
END
