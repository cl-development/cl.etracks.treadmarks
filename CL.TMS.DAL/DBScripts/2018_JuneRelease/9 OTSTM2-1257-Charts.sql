﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_TopTenStewards]    Script Date: 5/16/18 5:27:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--  Name:        [sp_Rpt_TopTenStewards_Charts]
--  Author:		 Hefen Zhou
--  Create date: 05-15-2018
--  Description: Store procedure for loading Top Ten Stewards of Revenue (per Period)
-- =============================================
Create PROCEDURE [dbo].[sp_Rpt_TopTenStewards_Charts]
(
	@startDate DateTime='2000-01-01'
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	declare @periodTotalAmount int, @thisYear int, @thisMonth int
	set @thisYear=year(@startDate);
	set @thisMonth=month(@startDate);

	-- current period top 10 stewards
	select top 10 
		 c.RegistrationNumber as [RNumber]		
		,coalesce((sum(tcd.TSFDue)-sum(tcd.CreditTSFDue)),0) as Amount_top10
	into #top10
	from TSFClaimDetail tcd			
		join TSFClaim tc on tcd.TSFClaimID=tc.ID
		join Customer c on tc.CustomerID=c.id
		join [Period] pc on tc.PeriodID=pc.ID	
	where pc.PeriodType=1 
		and year(pc.StartDate)=@thisYear
		and month(pc.StartDate)=@thisMonth	
	group by c.RegistrationNumber,pc.StartDate	
	order by Amount_top10 desc

	select Year(p.StartDate) as [Year]
		   ,coalesce((sum(tc.TSFDue)-sum(tc.CreditTSFDue)),0) as Total	
	from TSFClaimDetail tc	
		join TSFClaim t on tc.TSFClaimID=t.ID	
		join Customer c on t.CustomerID=c.id		
		join [Period] p on t.PeriodID=p.ID
	where p.PeriodType=1 
		  and c.RegistrationNumber in ( select RNumber from #top10)
		  and ((year(p.StartDate)=@thisYear and month(p.StartDate)=@thisMonth) 
			or (year(p.StartDate)=(@thisYear-1) and month(p.StartDate)=@thisMonth) 
			or (year(p.StartDate)=(@thisYear-2) and month(p.StartDate)=@thisMonth) 
		  )
	group by Year(p.StartDate)
	order by Year(p.StartDate) desc
END
