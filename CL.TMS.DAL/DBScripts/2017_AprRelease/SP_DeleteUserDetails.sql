﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUserDetails]    Script Date: 3/1/2017 12:25:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_DeleteUserDetails] 
	@userId as bigint
AS
BEGIN
	SET NOCOUNT ON;

    delete from userrole where userid=@userId;
	delete from UserClaimsWorkflow where UserId=@userId;
	delete from UserApplicationsWorkflow where UserId=@userId;
END
