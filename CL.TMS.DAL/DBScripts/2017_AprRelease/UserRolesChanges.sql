﻿use tmdb
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
if not exists (select * from sysobjects where name='AppResource' And xtype='U')
CREATE TABLE [dbo].[AppResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ResourceMenu] [nvarchar](50) NOT NULL,
	[ResourceScreen] [nvarchar](50) NOT NULL,
	[ResourceLevel] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL DEFAULT ('System'),
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppResource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

if not exists (select * from sysobjects where name='AppPermission' And xtype='U')
CREATE TABLE [dbo].[AppPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PermissionLevel] [int] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL DEFAULT ('System'),
	[CreateDate] [datetime] NOT NULL ,
	[UpdateDate] [datetime] NOT NULL ,
 CONSTRAINT [PK_ResourceAction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

if not exists (select * from sysobjects where name='RolePermission' And xtype='U')
begin
CREATE TABLE [dbo].[RolePermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[AppResourceId] [int] NOT NULL,
	[AppPermissionId] [int] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
CONSTRAINT [UQ_Ids] UNIQUE NONCLUSTERED
	(
		[RoleId], [AppResourceId], [AppPermissionId]
	)
) ON [PRIMARY]

ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_AppPermission] FOREIGN KEY([AppPermissionId])
REFERENCES [dbo].[AppPermission] ([Id])

ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_AppPermission]

ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_AppResource] FOREIGN KEY([AppResourceId])
REFERENCES [dbo].[AppResource] ([Id])

ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_AppResource]

ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ID])

ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_Role]
end
GO

if not exists (select * from sysobjects where name='InvitationClaimsWorkflow' And xtype='U')
begin
CREATE TABLE [dbo].[InvitationClaimsWorkflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvitationId] [bigint] NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Workflow] [nvarchar](50) NOT NULL,
	[ActionGear] [bit] NOT NULL,
 CONSTRAINT [PK_InvitationClaimsWorkflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[InvitationClaimsWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_InvitationClaimsWorkflow_Invitation] FOREIGN KEY([InvitationId])
REFERENCES [dbo].[Invitation] ([ID])

ALTER TABLE [dbo].[InvitationClaimsWorkflow] CHECK CONSTRAINT [FK_InvitationClaimsWorkflow_Invitation]

end
GO

if not exists (select * from sysobjects where name='InvitationApplicationsWorkflow' And xtype='U')
begin
CREATE TABLE [dbo].[InvitationApplicationsWorkflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvitationId] [bigint] NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Routing] [bit] NOT NULL,
	[ActionGear] [bit] NOT NULL,
 CONSTRAINT [PK_InvitationApplicationsWorkflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[InvitationApplicationsWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_InvitationApplicationsWorkflow_Invitation] FOREIGN KEY([InvitationId])
REFERENCES [dbo].[Invitation] ([ID])

ALTER TABLE [dbo].[InvitationApplicationsWorkflow] CHECK CONSTRAINT [FK_InvitationApplicationsWorkflow_Invitation]
end
GO

if not exists (select * from sysobjects where name='UserClaimsWorkflow' And xtype='U')
begin
CREATE TABLE [dbo].[UserClaimsWorkflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Workflow] [nvarchar](50) NOT NULL,
	[ActionGear] [bit] NOT NULL,
 CONSTRAINT [PK_UserClaimsWorkflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[UserClaimsWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_UserClaimsWorkflow_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([ID])

ALTER TABLE [dbo].[UserClaimsWorkflow] CHECK CONSTRAINT [FK_UserClaimsWorkflow_User]
end
GO

if not exists (select * from sysobjects where name='UserApplicationsWorkflow' And xtype='U')
begin
CREATE TABLE [dbo].[UserApplicationsWorkflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Routing] [bit] NOT NULL,
	[ActionGear] [bit] NOT NULL,
 CONSTRAINT [PK_UserApplicationsWorkflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[UserApplicationsWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_UserApplicationsWorkflow_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([ID])

ALTER TABLE [dbo].[UserApplicationsWorkflow] CHECK CONSTRAINT [FK_UserApplicationsWorkflow_User]
end
GO
SET ANSI_PADDING OFF

IF COL_LENGTH('[Transaction]', 'TransactionAdjustmentID') IS NULL
BEGIN
	ALTER TABLE [Transaction]
	ADD [TransactionAdjustmentID] int null  CONSTRAINT FK_Transaction_TransactionAdjustment FOREIGN KEY (TransactionAdjustmentID) REFERENCES TransactionAdjustment(ID)
END
GO

IF COL_LENGTH('[User]', 'CreateBy') IS NULL
BEGIN
	ALTER TABLE [User] ADD [CreateBy] [varchar](50) NOT NULL DEFAULT ('System')
END

IF COL_LENGTH('[Invitation]', 'UpdateDate') IS NULL
BEGIN
	ALTER TABLE [Invitation] ADD UpdateDate datetime NULL 
END
GO

declare @updateDate datetime;
set @updateDate =GETDATE()

if ((select COUNT(*) from [AppResource]) = 0) 
begin
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

insert into [AppResource]	(Name,						ResourceMenu,			ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES			('Claims',					'Claims',				'Claims',					0,				100000,			@CreateBy,	@dt,		@dt),
							('Claims Assignment',		'Claims',				'Claims Assignment',		1,				101000,			@CreateBy,	@dt,		@dt),
							('Claims All Claims',		'Claims',				'Claims All Claims',		1,				101010,			@CreateBy,	@dt,		@dt),
							('Collector Claim Summary',	'Claims',				'Collector Claim Summary',	1,				101110,			@CreateBy,	@dt,		@dt),
							('Status',					'Claims',				'Collector Claim Summary',	2,				101111,			@CreateBy,	@dt,		@dt),
							('Inbound',					'Claims',				'Collector Claim Summary',	2,				101112,			@CreateBy,	@dt,		@dt),
							('Outbound',				'Claims',				'Collector Claim Summary',	2,				101113,			@CreateBy,	@dt,		@dt),
							('Adjustments',				'Claims',				'Collector Claim Summary',	2,				101114,			@CreateBy,	@dt,		@dt),
							('Reuse Tires',				'Claims',				'Collector Claim Summary',	2,				101115,			@CreateBy,	@dt,		@dt),
							('Transaction Adjustments',	'Claims',				'Collector Claim Summary',	2,				101116,			@CreateBy,	@dt,		@dt),
							('Payments And Adjustments','Claims',				'Collector Claim Summary',	2,				101117,			@CreateBy,	@dt,		@dt),
							('Internal Adjustments',	'Claims',				'Collector Claim Summary',	2,				101118,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Claims',				'Collector Claim Summary',	2,				101119,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Claims',				'Collector Claim Summary',	2,				101120,			@CreateBy,	@dt,		@dt),
							('Activities',				'Claims',				'Collector Claim Summary',	2,				101121,			@CreateBy,	@dt,		@dt),
							('Hauler Claim Summary',	'Claims',				'Hauler Claim Summary',		1,				101210,			@CreateBy,	@dt,		@dt),
							('Status',					'Claims',				'Hauler Claim Summary',		2,				101211,			@CreateBy,	@dt,		@dt),
							('Inbound',					'Claims',				'Hauler Claim Summary',		2,				101212,			@CreateBy,	@dt,		@dt),
							('Outbound',				'Claims',				'Hauler Claim Summary',		2,				101213,			@CreateBy,	@dt,		@dt),
							('Transaction Adjustments',	'Claims',				'Hauler Claim Summary',		2,				101214,			@CreateBy,	@dt,		@dt),
							('Inventory And Adjustments','Claims',				'Hauler Claim Summary',		2,				101215,			@CreateBy,	@dt,		@dt),
							('Payments And Adjustments','Claims',				'Hauler Claim Summary',		2,				101216,			@CreateBy,	@dt,		@dt),
							('Internal Adjustments',	'Claims',				'Hauler Claim Summary',		2,				101217,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Claims',				'Hauler Claim Summary',		2,				101218,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Claims',				'Hauler Claim Summary',		2,				101219,			@CreateBy,	@dt,		@dt),
							('Activities',				'Claims',				'Hauler Claim Summary',		2,				101220,			@CreateBy,	@dt,		@dt),
							('Processor Claim Summary',	'Claims',				'Processor Claim Summary',	1,				101310,			@CreateBy,	@dt,		@dt),
							('Status',					'Claims',				'Processor Claim Summary',	2,				101311,			@CreateBy,	@dt,		@dt),
							('Inbound',					'Claims',				'Processor Claim Summary',	2,				101312,			@CreateBy,	@dt,		@dt),
							('Outbound',				'Claims',				'Processor Claim Summary',	2,				101313,			@CreateBy,	@dt,		@dt),
							('Transaction Adjustments',	'Claims',				'Processor Claim Summary',	2,				101314,			@CreateBy,	@dt,		@dt),
							('Inventory And Adjustments','Claims',				'Processor Claim Summary',	2,				101315,			@CreateBy,	@dt,		@dt),
							('Payments And Adjustments','Claims',				'Processor Claim Summary',	2,				101316,			@CreateBy,	@dt,		@dt),
							('Internal Adjustments',	'Claims',				'Processor Claim Summary',	2,				101317,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Claims',				'Processor Claim Summary',	2,				101318,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Claims',				'Processor Claim Summary',	2,				101319,			@CreateBy,	@dt,		@dt),
							('Activities',				'Claims',				'Processor Claim Summary',	2,				101320,			@CreateBy,	@dt,		@dt),
							('RPM Claim Summary',		'Claims',				'RPM Claim Summary',		1,				101410,			@CreateBy,	@dt,		@dt),
							('Status',					'Claims',				'RPM Claim Summary',		2,				101411,			@CreateBy,	@dt,		@dt),
							('Inbound',					'Claims',				'RPM Claim Summary',		2,				101412,			@CreateBy,	@dt,		@dt),
							('Outbound',				'Claims',				'RPM Claim Summary',		2,				101413,			@CreateBy,	@dt,		@dt),
							('Transaction Adjustments',	'Claims',				'RPM Claim Summary',		2,				101414,			@CreateBy,	@dt,		@dt),
							('Inventory And Adjustments','Claims',				'RPM Claim Summary',		2,				101415,			@CreateBy,	@dt,		@dt),
							('Payments And Adjustments','Claims',				'RPM Claim Summary',		2,				101416,			@CreateBy,	@dt,		@dt),
							('Internal Adjustments',	'Claims',				'RPM Claim Summary',		2,				101417,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Claims',				'RPM Claim Summary',		2,				101418,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Claims',				'RPM Claim Summary',		2,				101419,			@CreateBy,	@dt,		@dt),
							('Activities',				'Claims',				'RPM Claim Summary',		2,				101420,			@CreateBy,	@dt,		@dt),
							('Transactions',			'Transactions',			'Transactions',				0,				110000,			@CreateBy,	@dt,		@dt),
							('All Transactions',		'Transactions',			'All Transactions',			1,				111010,			@CreateBy,	@dt,		@dt),
							('Remittance',				'Remittance',			'Remittance',				0,				120000,			@CreateBy,	@dt,		@dt),
							('Remittances Assignment',	'Remittance',			'Remittances Assignment',	1,				121000,			@CreateBy,	@dt,		@dt),
							('TSF Remittances',			'Remittance',			'TSF Remittances',			1,				121100,			@CreateBy,	@dt,		@dt),
							('TSF Remittance',			'Remittance',			'TSF Remittance',			1,				121200,			@CreateBy,	@dt,		@dt),
							('Remittance Period',		'Remittance',			'TSF Remittance',			2,				121210,			@CreateBy,	@dt,		@dt),
							('Status',					'Remittance',			'TSF Remittance',			2,				121211,			@CreateBy,	@dt,		@dt),
							('Tire Counts',				'Remittance',			'TSF Remittance',			2,				121212,			@CreateBy,	@dt,		@dt),
							('Credit',					'Remittance',			'TSF Remittance',			2,				121213,			@CreateBy,	@dt,		@dt),
							('Payments',				'Remittance',			'TSF Remittance',			2,				121214,			@CreateBy,	@dt,		@dt),
							('Internal Adjustments',	'Remittance',			'TSF Remittance',			2,				121215,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Remittance',			'TSF Remittance',			2,				121216,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Remittance',			'TSF Remittance',			2,				121217,			@CreateBy,	@dt,		@dt),
							('Activities',				'Remittance',			'TSF Remittance',			2,				121218,			@CreateBy,	@dt,		@dt),
							('Applications',			'Applications',			'Applications',				0,				130000,			@CreateBy,	@dt,		@dt),
							('All Applications',		'Applications',			'All Applications',			1,				131000,			@CreateBy,	@dt,		@dt),
							('Assignment',				'Applications',			'All Applications',			2,				131010,			@CreateBy,	@dt,		@dt),
							('Registrants',				'Applications',			'All Applications',			2,				131011,			@CreateBy,	@dt,		@dt),
							('Banking Actions',			'Applications',			'All Applications',			2,				131012,			@CreateBy,	@dt,		@dt),
							('Steward Application',		'Applications',			'Steward Application',		1,				131100,			@CreateBy,	@dt,		@dt),
							('Active/Inactive',			'Applications',			'Steward Application',		2,				131110,			@CreateBy,	@dt,		@dt),
							('Remit Frequency',			'Applications',			'Steward Application',		2,				131111,			@CreateBy,	@dt,		@dt),
							('Audit Toggle',			'Applications',			'Steward Application',		2,				131112,			@CreateBy,	@dt,		@dt),
							('MOE Toggle',				'Applications',			'Steward Application',		2,				131113,			@CreateBy,	@dt,		@dt),
							('Submission Information',	'Applications',			'Steward Application',		2,				131114,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Applications',			'Steward Application',		2,				131115,			@CreateBy,	@dt,		@dt),
							('Business Location',		'Applications',			'Steward Application',		2,				131116,			@CreateBy,	@dt,		@dt),
							('Contact Information',		'Applications',			'Steward Application',		2,				131117,			@CreateBy,	@dt,		@dt),
							('Tire Details',			'Applications',			'Steward Application',		2,				131118,			@CreateBy,	@dt,		@dt),
							('Steward Details',			'Applications',			'Steward Application',		2,				131119,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Applications',			'Steward Application',		2,				131120,			@CreateBy,	@dt,		@dt),
							('Terms And Conditions',	'Applications',			'Steward Application',		2,				131121,			@CreateBy,	@dt,		@dt),
							('Re-send Welcome Letter',	'Applications',			'Steward Application',		2,				131122,			@CreateBy,	@dt,		@dt),
							('Collector Application',	'Applications',			'Collector Application',	1,				131200,			@CreateBy,	@dt,		@dt),
							('Active/Inactive',			'Applications',			'Collector Application',	2,				131210,			@CreateBy,	@dt,		@dt),
							('Generator Toggle',		'Applications',			'Collector Application',	2,				131211,			@CreateBy,	@dt,		@dt),
							('Submission Information',	'Applications',			'Collector Application',	2,				131212,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Applications',			'Collector Application',	2,				131213,			@CreateBy,	@dt,		@dt),
							('Business Location',		'Applications',			'Collector Application',	2,				131214,			@CreateBy,	@dt,		@dt),
							('Contact Information',		'Applications',			'Collector Application',	2,				131215,			@CreateBy,	@dt,		@dt),
							('Tire Details',			'Applications',			'Collector Application',	2,				131216,			@CreateBy,	@dt,		@dt),
							('Collector Details',		'Applications',			'Collector Application',	2,				131217,			@CreateBy,	@dt,		@dt),
							('Banking Information',		'Applications',			'Collector Application',	2,				131218,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Applications',			'Collector Application',	2,				131219,			@CreateBy,	@dt,		@dt),
							('Terms And Conditions',	'Applications',			'Collector Application',	2,				131220,			@CreateBy,	@dt,		@dt),
							('Re-send Welcome Letter',	'Applications',			'Collector Application',	2,				131221,			@CreateBy,	@dt,		@dt),
							('Hauler Application',		'Applications',			'Hauler Application',		1,				131300,			@CreateBy,	@dt,		@dt),
							('Active/Inactive',			'Applications',			'Hauler Application',		2,				131310,			@CreateBy,	@dt,		@dt),
							('Submission Information',	'Applications',			'Hauler Application',		2,				131311,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Applications',			'Hauler Application',		2,				131312,			@CreateBy,	@dt,		@dt),
							('Business Location',		'Applications',			'Hauler Application',		2,				131313,			@CreateBy,	@dt,		@dt),
							('Contact Information',		'Applications',			'Hauler Application',		2,				131314,			@CreateBy,	@dt,		@dt),
							('Sort Yard Details',		'Applications',			'Hauler Application',		2,				131315,			@CreateBy,	@dt,		@dt),
							('Tire Details',			'Applications',			'Hauler Application',		2,				131316,			@CreateBy,	@dt,		@dt),
							('Hauler Details',			'Applications',			'Hauler Application',		2,				131317,			@CreateBy,	@dt,		@dt),
							('Banking Information',		'Applications',			'Hauler Application',		2,				131318,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Applications',			'Hauler Application',		2,				131319,			@CreateBy,	@dt,		@dt),
							('Terms And Conditions',	'Applications',			'Hauler Application',		2,				131320,			@CreateBy,	@dt,		@dt),
							('Re-send Welcome Letter',	'Applications',			'Hauler Application',		2,				131321,			@CreateBy,	@dt,		@dt),
							('Processor Application',	'Applications',			'Processor Application',	1,				131400,			@CreateBy,	@dt,		@dt),
							('Active/Inactive',			'Applications',			'Processor Application',	2,				131410,			@CreateBy,	@dt,		@dt),
							('Submission Information',	'Applications',			'Processor Application',	2,				131411,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Applications',			'Processor Application',	2,				131412,			@CreateBy,	@dt,		@dt),
							('Business Location',		'Applications',			'Processor Application',	2,				131413,			@CreateBy,	@dt,		@dt),
							('Contact Information',		'Applications',			'Processor Application',	2,				131414,			@CreateBy,	@dt,		@dt),
							('Processing Locations',	'Applications',			'Processor Application',	2,				131415,			@CreateBy,	@dt,		@dt),
							('Tires And Processing Details','Applications',		'Processor Application',	2,				131416,			@CreateBy,	@dt,		@dt),
							('Processor Details',		'Applications',			'Processor Application',	2,				131417,			@CreateBy,	@dt,		@dt),
							('Banking Information',		'Applications',			'Processor Application',	2,				131418,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Applications',			'Processor Application',	2,				131419,			@CreateBy,	@dt,		@dt),
							('Terms And Conditions',	'Applications',			'Processor Application',	2,				131420,			@CreateBy,	@dt,		@dt),
							('Re-send Welcome Letter',	'Applications',			'Processor Application',	2,				131421,			@CreateBy,	@dt,		@dt),
							('RPM Application',			'Applications',			'RPM Application',			1,				131500,			@CreateBy,	@dt,		@dt),
							('Active/Inactive',			'Applications',			'RPM Application',			2,				131510,			@CreateBy,	@dt,		@dt),
							('Submission Information',	'Applications',			'RPM Application',			2,				131511,			@CreateBy,	@dt,		@dt),
							('Internal Notes',			'Applications',			'RPM Application',			2,				131512,			@CreateBy,	@dt,		@dt),
							('Business Location',		'Applications',			'RPM Application',			2,				131513,			@CreateBy,	@dt,		@dt),
							('Contact Information',		'Applications',			'RPM Application',			2,				131514,			@CreateBy,	@dt,		@dt),
							('Manufacturing Location',	'Applications',			'RPM Application',			2,				131515,			@CreateBy,	@dt,		@dt),
							('Manufacturing Details',	'Applications',			'RPM Application',			2,				131516,			@CreateBy,	@dt,		@dt),
							('RPM Details',				'Applications',			'RPM Application',			2,				131517,			@CreateBy,	@dt,		@dt),
							('Banking Information',		'Applications',			'RPM Application',			2,				131518,			@CreateBy,	@dt,		@dt),
							('Supporting Documents',	'Applications',			'RPM Application',			2,				131519,			@CreateBy,	@dt,		@dt),
							('Terms And Conditions',	'Applications',			'RPM Application',			2,				131520,			@CreateBy,	@dt,		@dt),
							('Re-send Welcome Letter',	'Applications',			'RPM Application',			2,				131521,			@CreateBy,	@dt,		@dt),
							('Users',					'Users',				'Users',					0,				140000,			@CreateBy,	@dt,		@dt),
							('Registrant Users',		'Users',				'Registrant Users',			1,				141000,			@CreateBy,	@dt,		@dt),
							('iPads + QR Codes',		'iPads + QR Codes',		'iPads + QR Codes',			0,				150000,			@CreateBy,	@dt,		@dt),
							('All iPads + QR Codes',	'iPads + QR Codes',		'All iPads + QR Codes',		1,				151000,			@CreateBy,	@dt,		@dt),
							('iPad Fleet',				'iPads + QR Codes',		'All iPads + QR Codes',		2,				151010,			@CreateBy,	@dt,		@dt),
							('QR Codes',				'iPads + QR Codes',		'All iPads + QR Codes',		2,				151011,			@CreateBy,	@dt,		@dt),
							('Retail Connection',		'Retail Connection',	'Retail Connection',		0,				160000,			@CreateBy,	@dt,		@dt),
							('All Retail Connection',	'Retail Connection',	'All Retail Connection',	1,				161000,			@CreateBy,	@dt,		@dt),
							('All Retailers',			'Retail Connection',	'All Retail Connection',	2,				161010,			@CreateBy,	@dt,		@dt),
							('All Products',			'Retail Connection',	'All Retail Connection',	2,				161011,			@CreateBy,	@dt,		@dt),
							('Product Categories',		'Retail Connection',	'All Retail Connection',	2,				161012,			@CreateBy,	@dt,		@dt),
							('Reports',					'Reports',				'Reports',					0,				170000,			@CreateBy,	@dt,		@dt),
							('All Reports',				'Reports',				'All Reports',				1,				171000,			@CreateBy,	@dt,		@dt),
							('Steward',					'Reports',				'All Reports',				2,				171010,			@CreateBy,	@dt,		@dt),
							('Collector',				'Reports',				'All Reports',				2,				171011,			@CreateBy,	@dt,		@dt),
							('Hauler',					'Reports',				'All Reports',				2,				171012,			@CreateBy,	@dt,		@dt),
							('Processor',				'Reports',				'All Reports',				2,				171013,			@CreateBy,	@dt,		@dt),
							('RPM',						'Reports',				'All Reports',				2,				171014,			@CreateBy,	@dt,		@dt),
							('Registrant',				'Reports',				'All Reports',				2,				171015,			@CreateBy,	@dt,		@dt),
							('Financial Integration',	'Financial Integration','Financial Integration',	0,				180000,			@CreateBy,	@dt,		@dt),
							('GP Financial Integration','Financial Integration','GP Financial Integration',	1,				181000,			@CreateBy,	@dt,		@dt),
							('GP Participant Manager',	'Financial Integration','GP Financial Integration',	2,				181010,			@CreateBy,	@dt,		@dt),
							('GP Steward Manager',		'Financial Integration','GP Financial Integration',	2,				181011,			@CreateBy,	@dt,		@dt),
							('GP Collector Manager',	'Financial Integration','GP Financial Integration',	2,				181012,			@CreateBy,	@dt,		@dt),
							('GP Hauler Manager',		'Financial Integration','GP Financial Integration',	2,				181013,			@CreateBy,	@dt,		@dt),
							('GP Processor Manager',	'Financial Integration','GP Financial Integration',	2,				181014,			@CreateBy,	@dt,		@dt),
							('GP RPM Manager',			'Financial Integration','GP Financial Integration',	2,				181015,			@CreateBy,	@dt,		@dt),
							('Admin Access',			'Admin Access',			'Admin Access',				0,				190000,			@CreateBy,	@dt,		@dt),
							('Admin/Users',				'Admin/Users',			'Admin/Users',				0,				200000,			@CreateBy,	@dt,		@dt),
							('Staff Users',				'Admin/Users',			'Staff Users',				1,				201000,			@CreateBy,	@dt,		@dt),
							('Add/Edit New User',		'Admin/Users',			'Add/Edit New User',		1,				201100,			@CreateBy,	@dt,		@dt),
							('Roles',					'Admin/Users',			'Add/Edit New User',		2,				201110,			@CreateBy,	@dt,		@dt),
				('Routing And Workflow - CLAIMS',		'Admin/Users',			'Add/Edit New User',		2,				201111,			@CreateBy,	@dt,		@dt),
				('Routing And Workflow - REMITTANCES',	'Admin/Users',			'Add/Edit New User',		2,				201112,			@CreateBy,	@dt,		@dt),
				('Routing And Workflow - APPLICATIONS',	'Admin/Users',			'Add/Edit New User',		2,				201113,			@CreateBy,	@dt,		@dt),
				('Admin/Roles And Permissions',			'Admin/Roles And Permissions','Admin/Roles And Permissions',0,		210000,			@CreateBy,	@dt,		@dt),
				('Admin/Reports',						'Admin/Reports',		'Admin/Reports',			0,				220000,			@CreateBy,	@dt,		@dt),
				('Admin/Rates',							'Admin/Rates',			'Admin/Rates',				0,				230000,			@CreateBy,	@dt,		@dt),
				('Admin/Announcements',					'Admin/Announcements',	'Admin/Announcements',		0,				240000,			@CreateBy,	@dt,		@dt)
end
go

if ((select COUNT(*) from [AppPermission]) = 0) 
begin
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt =GETDATE()
insert into [AppPermission]	(Name,						PermissionLevel,	CreateBy,	CreateDate,	UpdateDate)
			VALUES			('NoAccess',				'0',				@CreateBy,	@dt,		@dt),
							('ReadOnly',				'1',				@CreateBy,	@dt,		@dt),
							('Custom',					'2',				@CreateBy,	@dt,		@dt),
							('EditSave',				'3',				@CreateBy,	@dt,		@dt)
end
go
