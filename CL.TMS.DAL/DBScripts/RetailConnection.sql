-- Execute the following stored procedure to add permission
-- sp_AddSecurityDataForRetailConnection

CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[BrandName] [nvarchar](500) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[AttachmentId] [int] NULL,
	[ApprovedDate] [datetime] NULL,
	[VendorId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO

ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductCategory]
GO

ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_User] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_User]
GO


CREATE TABLE [dbo].[Retailer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[IsOnline] [bit] NOT NULL,
	[Url] [nvarchar](250) NOT NULL,
	[ApprovedDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[AddressId] [int] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[VendorId] [int] NULL,
	[AddedBy] [bigint] NOT NULL,
	[IsAddedByParticipant] [bit] NOT NULL,
 CONSTRAINT [PK_Retailer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Retailer]  WITH CHECK ADD  CONSTRAINT [FK_Retailer_User] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[Retailer] CHECK CONSTRAINT [FK_Retailer_User]
GO

CREATE TABLE [dbo].[RetailerNote](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RetailerId] [int] NOT NULL,
	[Note] [nvarchar](4000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_RetailerNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RetailerNote]  WITH CHECK ADD  CONSTRAINT [FK_RetailerNote_Retailer] FOREIGN KEY([RetailerId])
REFERENCES [dbo].[Retailer] ([Id])
GO

ALTER TABLE [dbo].[RetailerNote] CHECK CONSTRAINT [FK_RetailerNote_Retailer]
GO

CREATE TABLE [dbo].[RetailerProduct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RetailerId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ProductUrl] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_RetailerProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RetailerProduct]  WITH CHECK ADD  CONSTRAINT [FK_RetailerProduct_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO

ALTER TABLE [dbo].[RetailerProduct] CHECK CONSTRAINT [FK_RetailerProduct_Product]
GO

ALTER TABLE [dbo].[RetailerProduct]  WITH CHECK ADD  CONSTRAINT [FK_RetailerProduct_Retailer] FOREIGN KEY([RetailerId])
REFERENCES [dbo].[Retailer] ([Id])
GO

ALTER TABLE [dbo].[RetailerProduct] CHECK CONSTRAINT [FK_RetailerProduct_Retailer]
GO

CREATE TABLE [dbo].[CategoryNote](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Note] [nvarchar](1000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_CategoryNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CategoryNote]  WITH CHECK ADD  CONSTRAINT [FK_CategoryNote_ProductCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO

ALTER TABLE [dbo].[CategoryNote] CHECK CONSTRAINT [FK_CategoryNote_ProductCategory]
GO



alter table product
alter column BrandName NVARCHAR(500) NULL


alter table retailer
alter column Url NVARCHAR(250) NULL

alter table Product 
alter column Description nvarchar(max) not null

insert into AppSetting ([Key], Value)
values ('Email.RCProductApproveEmailTemplate','Templates\RCProductApproveEmailTemplate.html')

insert into AppSetting ([Key], Value)
values ('Email.RCRetailerApproveEmailTemplate','Templates\RCRetailerApproveEmailTemplate.html')