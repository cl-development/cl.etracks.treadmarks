--select v.Number reg#, v.IsActive, v.ActiveStateChangeDate NewTM_ActiveStateChangeDate, l.[active_state_change_date] legacy_active_state_change_date from Vendor v 
--inner join Legacy_Registrant$ l
--on l.registration_number=v.Number
--where 
--v.LastUpdatedDate < '2016-04-01' 
--and 
--v.ActiveStateChangeDate!=l.[active_state_change_date]



select * from VendorActiveHistory va where va.VendorID
in(
select v.id from Vendor v 
inner join Legacy_Registrant$ l
on l.registration_number=v.Number
left join VendorActiveHistory va
on va.VendorID=v.ID
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]
group by v.id 
having count(va.id) > 1
   )
   order by va.VendorID
   

--update v 
--set v.ActiveStateChangeDate = l.active_state_change_date
--from Vendor v
--    inner join Legacy_Registrant$ l  
--	on l.registration_number=v.Number
--where 
--v.LastUpdatedDate < '2016-04-01' 
--and 
--v.ActiveStateChangeDate!=l.[active_state_change_date]


//customer


select * from CustomerActiveHistory va where va.CustomerID
in(
select v.id from Customer v 
inner join Legacy_Registrant$ l
on l.registration_number=v.RegistrationNumber
left join CustomerActiveHistory va
on va.CustomerID=v.ID
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]
group by v.id 
having count(va.id) > 1
   )
   order by va.CustomerID
   
   
   
select v.RegistrationNumber reg#, v.IsActive, v.ActiveStateChangeDate NewTM_ActiveStateChangeDate, l.[active_state_change_date] legacy_active_state_change_date from Customer v 
inner join Legacy_Registrant$ l
on l.registration_number=v.RegistrationNumber
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]



   update v 
set v.ActiveStateChangeDate = l.active_state_change_date
from Customer v
    inner join Legacy_Registrant$ l  
	on l.registration_number=v.RegistrationNumber
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]



update ca 
set ca.ActiveStateChangeDate = c.ActiveStateChangeDate
from Customer c
    inner join CustomerActiveHistory ca
	on c.id=ca.CustomerID
where 
c.LastUpdatedDate < '2016-04-01' 
and 
c.ActiveStateChangeDate!=ca.ActiveStateChangeDate



//find more than 1 occurance in history

select ca.VendorID, COUNT(*) from VendorActiveHistory ca
    inner join Vendor c
	on c.id=ca.VendorID
where 
c.LastUpdatedDate < '2016-04-01' 
and 
c.ActiveStateChangeDate!=ca.ActiveStateChangeDate
group by ca.VendorID
order by 2 desc


//update all except these

select ca.* from VendorActiveHistory ca
    inner join Vendor c
	on c.id=ca.VendorID
where 
ca.VendorID in (20670,
22858,
20749,
27851,
22125)
order by ca.VendorID

//then check the history records
select ca.* from VendorActiveHistory ca
    inner join Vendor c
	on c.id=ca.VendorID
where 
ca.VendorID in (20670,
22858,
20749,
27851,
22125)
order by ca.VendorID;


select c.ActiveStateChangeDate, c.* from Vendor c
where 
c.ID in (20670,
22858,
20749,
27851,
22125)

//and update the mismatching


UPDATE "VendorActiveHistory" SET "ActiveStateChangeDate"='2010-10-01' WHERE  "ID"=18622;
UPDATE "VendorActiveHistory" SET "ActiveStateChangeDate"='2015-12-14' WHERE  "ID"=23661;



//UAT Server

select v.RegistrationNumber reg#, v.IsActive, v.ActiveStateChangeDate NewTM_ActiveStateChangeDate, l.[active_state_change_date] legacy_active_state_change_date from Customer v 
inner join  [mxotsuso11.ontariots.ad].tmdb.dbo.Legacy_Registrant$ l
on l.registration_number=v.RegistrationNumber
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]

   update v 
set v.ActiveStateChangeDate = l.active_state_change_date
from Customer v
    inner join  [mxotsuso11.ontariots.ad].tmdb.dbo.Legacy_Registrant$ l  
	on l.registration_number=v.RegistrationNumber
where 
v.LastUpdatedDate < '2016-04-01' 
and 
v.ActiveStateChangeDate!=l.[active_state_change_date]


//find more than 1 occurance in history

select ca.CustomerID, COUNT(*) from CustomerActiveHistory ca
    inner join Customer c
	on c.id=ca.CustomerID
where 
c.LastUpdatedDate < '2016-04-01' 
and 
c.ActiveStateChangeDate!=ca.ActiveStateChangeDate
group by ca.CustomerID
order by 2 desc


select ca.* from CustomerActiveHistory ca
    inner join Customer c
	on c.id=ca.CustomerID
where 
c.LastUpdatedDate < '2016-04-01' 
and 
c.ActiveStateChangeDate!=ca.ActiveStateChangeDate
and 
ca.CustomerID in (2865);


update ca 
set ca.ActiveStateChangeDate = c.ActiveStateChangeDate
from Customer c
    inner join CustomerActiveHistory ca
	on c.id=ca.CustomerID
where 
c.LastUpdatedDate < '2016-04-01' 
and 
c.ActiveStateChangeDate!=ca.ActiveStateChangeDate
and 
ca.CustomerID in (2865)


//then check the history records

select ca.* from CustomerActiveHistory ca
    inner join Customer c
	on c.id=ca.CustomerID
where 
ca.CustomerID in (2865)
order by ca.CustomerID


select c.ActiveStateChangeDate, c.* from Customer c
where 
c.ID in (2865)

//and update the mismatching


--UPDATE "CustomerActiveHistory" SET "ActiveStateChangeDate"='' WHERE  "ID"=18622;

