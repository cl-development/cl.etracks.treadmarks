﻿USE [tmdb]

--------------== OTSTM2-270 ==------------------------

CREATE TABLE ClaimInternalAdjustmentNote (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimInternalAdjustmentId] [int] NULL,
	[ClaimInternalPaymentAdjustId] [int] NULL,
	[Note] [nvarchar](4000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_ClaimInternalAdjustmentNote] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

---------------== OTSTM2-270 ==--------------------

GO

--------------== OTSTM2-485 ==------------------------

/****** Object:  Table [dbo].[TSFClaimInternalPaymentAdjustment]    Script Date: 09/01/2017 10:40:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TSFClaimInternalPaymentAdjustment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClaimId] [int] NOT NULL,
	[PaymentType] [int] NOT NULL,
	[AdjustmentAmount] [decimal](18, 3) NOT NULL,
	[AdjustBy] [bigint] NOT NULL,
	[AdjustDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TSFClaimInternalPaymentAdjustment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TSFClaimInternalPaymentAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_TSFClaimInternalPaymentAdjustment_TSFClaim] FOREIGN KEY([ClaimId])
REFERENCES [dbo].[TSFClaim] ([ID])
GO

ALTER TABLE [dbo].[TSFClaimInternalPaymentAdjustment] CHECK CONSTRAINT [FK_TSFClaimInternalPaymentAdjustment_TSFClaim]
GO

ALTER TABLE [dbo].[TSFClaimInternalPaymentAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_TSFClaimInternalPaymentAdjustment_User] FOREIGN KEY([AdjustBy])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[TSFClaimInternalPaymentAdjustment] CHECK CONSTRAINT [FK_TSFClaimInternalPaymentAdjustment_User]
GO

/****** Object:  Table [dbo].[TSFClaimInternalAdjustmentNote]    Script Date: 09/01/2017 10:40:01 ******/

CREATE TABLE [dbo].[TSFClaimInternalAdjustmentNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TSFInternalAdjustmentId] [int] NOT NULL,
	[Note] [nvarchar](4000) NULL,
	[UserId] [bigint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[NoteType] [nvarchar](20) NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TSFClaimInternalAdjustmentNote] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TSFClaimInternalAdjustmentNote]  WITH CHECK ADD  CONSTRAINT [FK_TSFClaimInternalAdjustmentNote_TSFClaimInternalPaymentAdjustment] FOREIGN KEY([TSFInternalAdjustmentId])
REFERENCES [dbo].[TSFClaimInternalPaymentAdjustment] ([ID])
GO

ALTER TABLE [dbo].[TSFClaimInternalAdjustmentNote] CHECK CONSTRAINT [FK_TSFClaimInternalAdjustmentNote_TSFClaimInternalPaymentAdjustment]
GO

ALTER TABLE [dbo].[TSFClaimInternalAdjustmentNote]  WITH CHECK ADD  CONSTRAINT [FK_TSFClaimInternalAdjustmentNote_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[TSFClaimInternalAdjustmentNote] CHECK CONSTRAINT [FK_TSFClaimInternalAdjustmentNote_User]
GO

--------------== OTSTM2-485 ==------------------------