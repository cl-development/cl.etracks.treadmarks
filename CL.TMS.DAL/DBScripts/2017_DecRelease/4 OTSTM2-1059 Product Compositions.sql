﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

--user access permission control for Admin/Product Compositions --display order:29xxxx
if NOT EXISTS (select * from AppResource where Name = 'Admin/Product Compositions' and ResourceMenu = 'Admin/Product Compositions' and ResourceScreen = 'Admin/Product Compositions')
BEGIN

insert into [AppResource]	
	(Name,						ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Product Compositions',	'Admin/Product Compositions',		    'Admin/Product Compositions',	    0,				290000,			@CreateBy,	@dt,		@dt)
END
go

--create permission for new appResource(Product Compositions)

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int, @iEditSave int
Declare @getNewSourceIDs cursor
Declare @getRoleIds cursor
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]
set @getNewSourceIDs=cursor for select id from AppResource where ResourceMenu = 'Admin/Product Compositions'
select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	OPEN @getNewSourceIDs FETCH NEXT FROM @getNewSourceIDs INTO @newResourceID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
		BEGIN
			if (@RoleId = @superAdminID) 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)					
				end 
			else 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iNoAccess)					
				end
		END
		FETCH NEXT
		FROM @getNewSourceIDs INTO @newResourceID
		set @iCounter = @iCounter + 1;
	END
	CLOSE @getNewSourceIDs
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getNewSourceIDs
DEALLOCATE @getRoleIds
go

/***************
* Create tables
****************/
--create recoverable-material  
if not exists (select * from sysobjects where name='RecoverableMaterial' And xtype='U')	
begin
CREATE TABLE [dbo].[RecoverableMaterial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[MaterialName] [nvarchar](50) null,
	[MaterialDescription] [nvarchar](max) null,
	[Color] [nvarchar](20) null, 
	[RowVersion] [timestamp] NOT NULL,
CONSTRAINT [PK_RecoverableMaterial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

	ALTER TABLE [dbo].[RecoverableMaterial]  WITH CHECK ADD  CONSTRAINT [FK_RecoverableMaterial_CreatedByUser] FOREIGN KEY([CreatedByID])
	REFERENCES [dbo].[User] ([ID])
	ALTER TABLE [dbo].[RecoverableMaterial]  WITH CHECK ADD  CONSTRAINT [FK_RecoverableMaterial_ModifiedUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])
end
GO

--create item-recoverablematerial
if not exists (select * from sysobjects where name='ItemRecoverableMaterial' And xtype='U')	
begin
CREATE TABLE [dbo].[ItemRecoverableMaterial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MaterialID] [int] null,
	[ItemID] [int] null,
	[Quantity] [decimal](18,4)null,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[RowVersion] [timestamp] NOT NULL,

CONSTRAINT [PK_ItemRecoverableMaterial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]  
	
	SET ANSI_PADDING OFF
	ALTER TABLE [dbo].[ItemRecoverableMaterial]  WITH CHECK ADD  CONSTRAINT [FK_ItemRecoverableMaterial_Material] FOREIGN KEY([MaterialID])
	REFERENCES [dbo].[RecoverableMaterial] ([ID])
    
	ALTER TABLE [dbo].[ItemRecoverableMaterial] CHECK CONSTRAINT [FK_ItemRecoverableMaterial_Material]
	
	ALTER TABLE [dbo].[ItemRecoverableMaterial]  WITH CHECK ADD  CONSTRAINT [FK_ItemRecoverableMaterial_Item] FOREIGN KEY([ItemID])	
	REFERENCES [dbo].[Item] ([ID])
	
	ALTER TABLE [dbo].[ItemRecoverableMaterial] CHECK CONSTRAINT [FK_ItemRecoverableMaterial_Item]

	ALTER TABLE [dbo].[ItemRecoverableMaterial]  WITH CHECK ADD  CONSTRAINT [FK_ItemRecoverableMaterial_ModifiedUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])


--insert items into item-recoverablematerial 
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(65)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(66)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(67)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(68)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(69)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(70)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(71)
insert into [dbo].[ItemRecoverableMaterial]	
	(ItemID)VALUES(72)

end
GO