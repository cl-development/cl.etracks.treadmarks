﻿USE [tmdb]
GO

--update the 'TransactionAdjustmentID' column from null to the corresponding adjustmentId for all existing spsr transactions with wrong data. (The transaction has adjustment, but the 'TransactionAdjustmentID' column is null)
DECLARE @transactionID int, @adjustmentID int, @iCounter int
DECLARE @getTransactionIds CURSOR
SET @getTransactionIds = CURSOR FOR SELECT t.id, ta.ID FROM [Transaction] t JOIN [TransactionAdjustment] ta ON t.ID=ta.OriginalTransactionId WHERE ta.Status = 'Accepted' AND t.TransactionAdjustmentID IS NULL AND t.TransactionTypeCode = 'SPSR' ORDER BY t.ID

SET @iCounter = 1

OPEN @getTransactionIds FETCH NEXT FROM @getTransactionIds INTO @transactionID, @adjustmentID

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE [Transaction] SET TransactionAdjustmentID = @adjustmentID WHERE ID = @transactionID;
	--SELECT @transactionID[TransactionID], @adjustmentID[AdjustmentID], @iCounter[Counter];
	SET @iCounter = @iCounter + 1;
	FETCH NEXT FROM @getTransactionIds INTO @transactionID, @adjustmentID
END

CLOSE @getTransactionIds

DEALLOCATE @getTransactionIds

GO
