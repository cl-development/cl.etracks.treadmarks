﻿USE [tmdb]
GO

/****** Object:  Table [dbo].[Country]    Script Date: 11/29/2017 11:33:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[Country]') AND type in (N'U'))

BEGIN

CREATE TABLE [dbo].[Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Name] [nvarchar](256) NOT NULL,
	[Alpha2Code] [nvarchar](50) NOT NULL,
	[NumericCode] [nvarchar](50) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


END

GO

--insert counties
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Afghanistan', 'AF', '4'); --1
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Åland Islands', 'AX', '248'); --2
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Albania', 'AL', '8'); --3
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Algeria','DZ','12'); --4
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('American Samoa', 'AS', '16'); --5
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Andorra', 'AD', '20'); --6
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Angola', 'AO', '24'); --7
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Anguilla', 'AI', '660'); --8
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Antarctica', 'AQ', '10'); --9
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Antigua and Barbuda', 'AG', '28'); --10
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Argentina', 'AR', '32'); --11
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Armenia', 'AM', '51'); --12
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Aruba', 'AW', '533'); --13
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Australia', 'AU', '36'); --14
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Austria', 'AT', '40'); --15
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Azerbaijan', 'AZ', '31'); --16
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bahamas (the)', 'BS', '44'); --17
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bahrain', 'BH', '48'); --18
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bangladesh', 'BD', '50'); --19
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Barbados', 'BB', '52'); --20
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Belarus', 'BY', '112'); --21
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Belgium', 'BE', '56'); --22
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Belize', 'BZ', '84'); --23
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Benin', 'BJ', '204'); --24
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bermuda', 'BM', '60'); --25
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bhutan', 'BT', '64'); --26
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bolivia (Plurinational State of)', 'BO', '68'); --27
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bonaire, Sint Eustatius and Saba', 'BQ', '535'); --28
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bosnia and Herzegovina', 'BA', '70'); --29
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Botswana', 'BW', '72'); --30
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bouvet Island', 'BV', '74'); --31
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Brazil', 'BR', '76'); --32
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('British Indian Ocean Territory (the)', 'IO', '86'); --33
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Brunei Darussalam', 'BN', '96'); --34
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Bulgaria', 'BG', '100'); --35
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Burkina Faso', 'BF', '854'); --36
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Burundi', 'BI', '108'); --37
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cabo Verde', 'CV', '132'); --38
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cambodia', 'KH', '116'); --39
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cameroon', 'CM', '120'); --40
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Canada', 'CA', '124'); --41
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cayman Islands (the)', 'KY', '136'); --42
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Central African Republic (the)', 'CF', '140'); --43
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Chad', 'TD', '148'); --44
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Chile', 'CL', '152'); --45
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('China', 'CN', '156'); --46
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Christmas Island', 'CX', '162'); --47
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cocos (Keeling) Islands (the)', 'CC', '166'); --48
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Colombia', 'CO', '170'); --49
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Comoros (the)', 'KM', '174'); --50
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Congo (the Democratic Republic of the)', 'CD', '180'); --51
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Congo (the)', 'CG', '178'); --52
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cook Islands (the)', 'CK', '184'); --53
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Costa Rica', 'CR', '188'); --54
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Côte d''Ivoire', 'CI', '384'); --55
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Croatia', 'HR', '191'); --56
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cuba', 'CU', '192'); --57
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Curaçao', 'CW', '531'); --58
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Cyprus', 'CY', '196'); --59
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Czechia', 'CZ', '203'); --60
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Denmark', 'DK', '208'); --61
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Djibouti', 'DJ', '262'); --62
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Dominica', 'DM', '212'); --63
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Dominican Republic (the)', 'DO', '214'); --64
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Ecuador', 'EC', '218'); --65
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Egypt', 'EG', '818'); --66
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('El Salvador', 'SV', '222'); --67
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Equatorial Guinea', 'GQ', '226'); --68
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Eritrea', 'ER', '232'); --69
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Estonia', 'EE', '233'); --70
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Ethiopia', 'ET', '231'); --71
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Falkland Islands (the) [Malvinas]', 'FK', '238'); --72
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Faroe Islands (the)', 'FO', '234'); --73
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Fiji', 'FJ', '242'); --74
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Finland', 'FI', '246'); --75
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('France', 'FR', '250'); --76
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('French Guiana', 'GF', '254'); --77
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('French Polynesia', 'PF', '258'); --78
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('French Southern Territories (the)', 'TF', '260'); --79
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Gabon', 'GA', '266'); --80
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Gambia (the)', 'GM', '270'); --81
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Georgia', 'GE', '268'); --82
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Germany', 'DE', '276'); --83
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Ghana', 'GH', '288'); --84
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Gibraltar', 'GI', '292'); --85
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Greece', 'GR', '300'); --86
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Greenland', 'GL', '304'); --87
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Grenada', 'GD', '308'); --88
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guadeloupe', 'GP', '312'); --89
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guam', 'GU', '316'); --90
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guatemala', 'GT', '320'); --91
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guernsey', 'GG', '831'); --92
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guinea', 'GN', '324'); --93
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guinea-Bissau', 'GW', '624'); --94
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Guyana', 'GY', '328'); --95
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Haiti', 'HT', '332'); --96
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Heard Island and McDonald Islands', 'HM', '334'); --97
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Holy See (the)', 'VA', '336'); --98
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Honduras', 'HN', '340'); --99
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Hong Kong', 'HK', '344'); --100
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Hungary', 'HU', '348'); --101
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Iceland', 'IS', '352'); --102
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('India', 'IN', '356'); --103
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Indonesia', 'ID', '360'); --104
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Iran (Islamic Republic of)', 'IR', '364'); --105
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Iraq', 'IQ', '368'); --106
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Ireland', 'IE', '372'); --107
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Isle of Man', 'IM', '833'); --108
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Israel', 'IL', '376'); --109
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Italy', 'IT', '380'); --110
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Jamaica', 'JM', '388'); --111
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Japan', 'JP', '392'); --112
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Jersey', 'JE', '832'); --113
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Jordan', 'JO', '400'); --114
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Kazakhstan', 'KZ', '398'); --115
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Kenya', 'KE', '404'); --116
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Kiribati', 'KI', '296'); --117
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Korea (the Democratic People''s Republic of)', 'KP', '408'); --118
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Korea (the Republic of)', 'KR', '410'); --119
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Kuwait', 'KW', '414'); --120
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Kyrgyzstan', 'KG', '417'); --121
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Lao People''s Democratic Republic (the)', 'LA', '418'); --122
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Latvia', 'LV', '428'); --123
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Lebanon', 'LB', '422'); --124
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Lesotho', 'LS', '426'); --125
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Liberia', 'LR', '430'); --126
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Libya', 'LY', '434'); --127
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Liechtenstein', 'LI', '438'); --128
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Lithuania', 'LT', '440'); --129
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Luxembourg', 'LU', '442'); --130
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Macao', 'MO', '446'); --131
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Macedonia (the former Yugoslav Republic of)', 'MK', '807'); --132
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Madagascar', 'MG', '450'); --133
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Malawi', 'MW', '454'); --134
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Malaysia', 'MY', '458'); --135
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Maldives', 'MV', '462'); --136
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mali', 'ML', '466'); --137
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Malta', 'MT', '470'); --138
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Marshall Islands (the)', 'MH', '584'); --139
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Martinique', 'MQ', '474'); --140
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mauritania', 'MR', '478'); --141
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mauritius', 'MU', '480'); --142
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mayotte', 'YT', '175'); --143
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mexico', 'MX', '484'); --144
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Micronesia (Federated States of)', 'FM', '583'); --145
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Moldova (the Republic of)', 'MD', '498'); --146
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Monaco', 'MC', '492'); --147
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mongolia', 'MN', '496'); --148
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Montenegro', 'ME', '499'); --149
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Montserrat', 'MS', '500'); --150
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Morocco', 'MA', '504'); --151
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Mozambique', 'MZ', '508'); --152
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Myanmar', 'MM', '104'); --153
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Namibia', 'NA', '516'); --154
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Nauru', 'NR', '520'); --155
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Nepal', 'NP', '524'); --156
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Netherlands (the)', 'NL', '528'); --157
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('New Caledonia', 'NC', '540'); --158
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('New Zealand', 'NZ', '554'); --159
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Nicaragua', 'NI', '558'); --160
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Niger (the)', 'NE', '562'); --161
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Nigeria', 'NG', '566'); --162
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Niue', 'NU', '570'); --163
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Norfolk Island', 'NF', '574'); --164
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Northern Mariana Islands (the)', 'MP', '580'); --165
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Norway', 'NO', '578'); --166
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Oman', 'OM', '512'); --167
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Pakistan', 'PK', '586'); --168
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Palau', 'PW', '585'); --169
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Palestine, State of', 'PS', '275'); --170
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Panama', 'PA', '591'); --171
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Papua New Guinea', 'PG', '598'); --172
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Paraguay', 'PY', '600'); --173
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Peru', 'PE', '604'); --174
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Philippines (the)', 'PH', '608'); --175
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Pitcairn', 'PN', '612'); --176
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Poland', 'PL', '616'); --177
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Portugal', 'PT', '620'); --178
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Puerto Rico', 'PR', '630'); --179
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Qatar', 'QA', '634'); --180
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Réunion', 'RE', '638'); --181
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Romania', 'RO', '642'); --182
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Russian Federation (the)', 'RU', '643'); --183
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Rwanda', 'RW', '646'); --184
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Barthélemy', 'BL', '652'); --185
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Helena, Ascension and Tristan da Cunha', 'SH', '654'); --186
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Kitts and Nevis', 'KN', '659'); --187
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Lucia', 'LC', '662'); --188
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Martin (French part)', 'MF', '663'); --189
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Pierre and Miquelon', 'PM', '666'); --190
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saint Vincent and the Grenadines', 'VC', '670'); --191
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Samoa', 'WS', '882'); --192
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('San Marino', 'SM', '674'); --193
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sao Tome and Principe', 'ST', '678'); --194
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Saudi Arabia', 'SA', '682'); --195
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Senegal', 'SN', '686'); --196
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Serbia', 'RS', '688'); --197
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Seychelles', 'SC', '690'); --198
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sierra Leone', 'SL', '694'); --199
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Singapore', 'SG', '702'); --200
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sint Maarten (Dutch part)', 'SX', '534'); --201
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Slovakia', 'SK', '703'); --202
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Slovenia', 'SI', '705'); --203
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Solomon Islands', 'SB', '90'); --204
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Somalia', 'SO', '706'); --205
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('South Africa', 'ZA', '710'); --206
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('South Georgia and the South Sandwich Islands', 'GS', '239'); --207
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('South Sudan', 'SS', '728'); --208
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Spain', 'ES', '724'); --209
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sri Lanka', 'LK', '144'); --210
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sudan (the)', 'SD', '729'); --211
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Suriname', 'SR', '740'); --212
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Svalbard and Jan Mayen', 'SJ', '744'); --213
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Swaziland', 'SZ', '748'); --214
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Sweden', 'SE', '752'); --215
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Switzerland', 'CH', '756'); --216
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Syrian Arab Republic', 'SY', '760'); --217
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Taiwan (Province of China)', 'TW', '158'); --218
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tajikistan', 'TJ', '762'); --219
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tanzania, United Republic of', 'TZ', '834'); --220
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Thailand', 'TH', '764'); --221
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Timor-Leste', 'TL', '624'); --222
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Togo', 'TG', '768'); --223
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tokelau', 'TK', '772'); --224
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tonga', 'TO', '776'); --225
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Trinidad and Tobago', 'TT', '780'); --226
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tunisia', 'TN', '788'); --227
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Turkey', 'TR', '792'); --228
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Turkmenistan', 'TM', '795'); --229
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Turks and Caicos Islands (the)', 'TC', '796'); --230
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Tuvalu', 'TV', '798'); --231
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Uganda', 'UG', '800'); --232
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Ukraine', 'UA', '804'); --233
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('United Arab Emirates (the)', 'AE', '784'); --234
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('United Kingdom of Great Britain and Northern Ireland (the)', 'GB', '826'); --235
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('United States Minor Outlying Islands (the)', 'UM', '581'); --236
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('United States of America (the)', 'US', '840'); --237
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Uruguay', 'UY', '858'); --238
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Uzbekistan', 'UZ', '860'); --239
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Vanuatu', 'VU', '548'); --240
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Venezuela (Bolivarian Republic of)', 'VE', '862'); --241
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Viet Nam', 'VN', '704'); --242
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Virgin Islands (British)', 'VG', '92'); --243
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Virgin Islands (U.S.)', 'VI', '850'); --244
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Wallis and Futuna', 'WF', '876'); --245
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Western Sahara*', 'EH', '732'); --246
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Yemen', 'YE', '887'); --247
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Zambia', 'ZM', '894'); --248
INSERT INTO "Country" ("Name", "Alpha2Code", "NumericCode") VALUES ('Zimbabwe', 'ZW', '716'); --249

go

--make sure the size of Country column is big enough for new country name, for example 'United Kingdom of Great Britain and Northern Ireland (the)'
ALTER TABLE [Address] ALTER COLUMN [Country] [nvarchar](100) not null

go

--change the country name of existing transactions
--missing name, leave them with no change
select * from Address where Country = 'Caribbean' --#1, 6 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'Caribbean' --6 x record on QA
select * from Address where Country = 'Serbia and Montenegro (Former)' --#2, 0 record on QA

go 

--name changed
select * from Address where Country = 'Bolivarian Republic of Venezuela' --#1, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Bolivarian Republic of Venezuela')
BEGIN
	UPDATE [Address] SET Country = 'Venezuela (Bolivarian Republic of)' where Country = 'Bolivarian Republic of Venezuela'
END

go 

select * from Address where Country = 'Bolivia' --#2, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Bolivia')
BEGIN
	UPDATE [Address] SET Country = 'Bolivia (Plurinational State of)' where Country = 'Bolivia'
END

go 

select * from Address where Country = 'Czech Republic'  --#3 ,1 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'Czech Republic' --1 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Czech Republic')
BEGIN
	UPDATE [Address] SET Country = 'Czechia' where Country = 'Czech Republic'
END

go 

select * from Address where Country = 'Dominican Republic' --#4, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Dominican Republic')
BEGIN
	UPDATE [Address] SET Country = 'Dominican Republic (the)' where Country = 'Dominican Republic'
END

go 

select * from Address where Country = 'Faroe Islands' --#5, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Faroe Islands')
BEGIN
	UPDATE [Address] SET Country = 'Faroe Islands (the)' where Country = 'Faroe Islands'
END

go 

select * from Address where Country = 'Hong Kong S.A.R.' --#6, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Hong Kong S.A.R.')
BEGIN
	UPDATE [Address] SET Country = 'Hong Kong' where Country = 'Hong Kong S.A.R.'
END

go 

select * from Address where Country = 'Iran' --#7, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Iran')
BEGIN
	UPDATE [Address] SET Country = 'Iran (Islamic Republic of)' where Country = 'Iran'
END

go 

select * from Address where Country = 'Islamic Republic of Pakistan' --#8, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Islamic Republic of Pakistan')
BEGIN
	UPDATE [Address] SET Country = 'Pakistan' where Country = 'Islamic Republic of Pakistan'
END

go 

select * from Address where Country = 'Korea' --#9, 12 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'Korea' --12 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Korea')
BEGIN
	UPDATE [Address] SET Country = 'Korea (the Republic of)' where Country = 'Korea'
END

go 

select * from Address where Country = 'Lao P.D.R.' --#10, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Lao P.D.R.')
BEGIN
	UPDATE [Address] SET Country = 'Lao People''s Democratic Republic (the)' where Country = 'Lao P.D.R.'
END

go 

select * from Address where Country = 'Macao S.A.R.' --#11, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Macao S.A.R.')
BEGIN
	UPDATE [Address] SET Country = 'Macao' where Country = 'Macao S.A.R.'
END

go 

select * from Address where Country = 'Macedonia (FYROM)' --#12, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Macedonia (FYROM)')
BEGIN
	UPDATE [Address] SET Country = 'Macedonia (the former Yugoslav Republic of)' where Country = 'Macedonia (FYROM)'
END

go 

select * from Address where Country = 'Netherlands'  --#13, 180 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'Netherlands' --177 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Netherlands')
BEGIN
	UPDATE [Address] SET Country = 'Netherlands (the)' where Country = 'Netherlands'
END

go 

select * from Address where Country = 'People''s Republic of China'  --#14, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'People''s Republic of China')
BEGIN
	UPDATE [Address] SET Country = 'China' where Country = 'People''s Republic of China'
END

go 

select * from Address where Country = 'Philippines' --#15, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Philippines')
BEGIN
	UPDATE [Address] SET Country = 'Philippines (the)' where Country = 'Philippines'
END

go 

select * from Address where Country = 'Principality of Monaco' --#16, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Principality of Monaco')
BEGIN
	UPDATE [Address] SET Country = 'Monaco' where Country = 'Principality of Monaco'
END

go 

select * from Address where Country = 'Russia' --#17, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Russia')
BEGIN
	UPDATE [Address] SET Country = 'Russian Federation (the)' where Country = 'Russia'
END

go 

select * from Address where Country = 'Syria' --#18, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Syria')
BEGIN
	UPDATE [Address] SET Country = 'Syrian Arab Republic' where Country = 'Syria'
END

go 

select * from Address where Country = 'Taiwan' --#19, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Taiwan')
BEGIN
	UPDATE [Address] SET Country = 'Taiwan (Province of China)' where Country = 'Taiwan'
END

go 

select * from Address where Country = 'U.A.E.' --#20, 27 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'U.A.E.' --25 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'U.A.E.')
BEGIN
	UPDATE [Address] SET Country = 'United Arab Emirates (the)' where Country = 'U.A.E.'
END

go 

select * from Address where Country = 'United Kingdom' --#21, 177 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'United Kingdom' --166 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'United Kingdom')
BEGIN
	UPDATE [Address] SET Country = 'United Kingdom of Great Britain and Northern Ireland (the)' where Country = 'United Kingdom'
END

go 

select * from Address where Country = 'United States' --#22, 9796 x record on QA
select t.TransactionTypeCode, t.FriendlyId, a.* from [Transaction] t join CompanyInfo c on t.CompanyInfoId=c.ID join Address a on a.ID=c.AddressId where a.Country = 'United States' --9108 x record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'United States')
BEGIN
	UPDATE [Address] SET Country = 'United States of America (the)' where Country = 'United States'
END

go 

select * from Address where Country = 'Vietnam' --#23, 0 record on QA
IF EXISTS (SELECT * FROM [ADDRESS] WHERE Country = 'Vietnam')
BEGIN
	UPDATE [Address] SET Country = 'Viet Nam' where Country = 'Vietnam'
END

go 