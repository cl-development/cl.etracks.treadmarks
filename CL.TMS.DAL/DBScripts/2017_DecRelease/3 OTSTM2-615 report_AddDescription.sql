﻿USE [tmdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- Author:		Hefen Zhou
-- Create date: 10/30/2017
-- Description:	Report add column description for Tool Tip text
-- ============================================================
IF COL_LENGTH('[Report]', 'Description') IS NULL
BEGIN
    ALTER TABLE Report
    ADD [Description] nvarchar(max) null
END
go

update Report set [Description]='
This report produces two tabs in Excel. The first tab is called Tire Counts and the second tab is called Credit.<br/>
The Tire Count tab pulls steward details regarding:<br/>
· Submission information<br/>
· Payment Information<br/>
· Tire Class and Adjustment Details<br/>
The Credit Panel tab pulls steward detail regarding<br/>
· Submission information for negative adjustments<br/>
· Payment information<br/>
· Tire Class and Adjustment details<br/>
' where ReportName like 'Steward Revenue & Supply - New Tire Types%'

update Report set [Description]='
This report produces two tabs in Excel. The first tab is called Tire Counts and the second tab is called Credit.<br/>
The Tire Count tab This Report pulls steward details regarding:<br/>
· Submission information<br/>
· Payment Information<br/>
· Tire Class and Negative Adjustment Details<br/>
The Credit Panel tab pulls steward detail regarding:<br/>
· Submission information for negative adjustments<br/>
· Payment information<br/>
· Tire Class and Adjustment detail<br/>
' where ReportName like 'Steward Revenue & Supply - Old Tire Types%'

update Report set [Description]='
This report pulls a summary of all Steward Financial Integration batches.
' where ReportName like 'TSF Extract - In Batch Only Report (GP)%'

update Report set [Description]='
This reports pulls a list of all Stewards and the Remittance Periods that have not been submitted.
' where ReportName like 'Steward Non Filers%'

update Report set [Description]='
Stewards who submitted a Remittance but no payment has been received.
' where ReportName like 'TSF Online Payments Outstanding%'

update Report set [Description]='
This report pulls a summary of Collector''s business and claim status, payment amount, and submission details.
' where ReportName like 'Collector Cumulative Report%'

update Report set [Description]='
This report pulls completed Hauler transactions and compares them against the Collectors transactions. Mobile transactions will automatically create/update the shared transaction for both participants. This report allows you to search all account numbers or you can enter a specific account number to narrow down your results.
' where ReportName like 'Hauler Collector Comparison Report%'

update Report set [Description]='
This report pulls summary of all PTR deliveries by Haulers to Processors. The report includes total actual weight, estimate weights by tire type and actual payment by tire type.
' where ReportName like 'Hauler Volume Report%'

update Report set [Description]='
This report pulls transaction details of PTR delivered by Haulers to Processors. The report includes actual scale weights by tire type and estimate weight by tire type.
' where ReportName like 'Detail Hauler Volume Report Based on Scale Weight%'

update Report set [Description]='
This reports can be pulled by specific Hauler or by all Haulers. The details of this report pulls all transactions except for invalidated and includes the following details by tire count:<br/>
· all tires collected by the Hauler (TCR,DOT,STC,UCR, HIT),<br/>
· all tires delivered by the Hauler (PTR,HIT, RTR)<br/>
· all tire count/yard count adjustments created by OTS.
' where ReportName like 'Hauler Tire Movement Report%'

update Report set [Description]='
This report pulls details relating to RPM SPS transactions. This report includes transaction and review status details, product code numbers, product type, sales invoice number, purchaser''s name, weight of rubber claimed, incentive rate and total payment per transaction.
' where ReportName like 'RPM Data Report%'

update Report set [Description]='
This report pulls a summary of RPMs claims status details, payment amount, and open and closing inventory values.
' where ReportName like 'RPM Cumulative Report%'

--another collector cumulative report in document.

update Report set [Description]='
This report pulls a list of Processors, claim periods, claim status, TI-PI payment amount, PI payment amount, adjustments and total Processor payment.
' where ReportName like 'Processor TIPI Report%'

update Report set [Description]='
This report pulls a summary of TDP material types sold by the Processors. The report provides the claim period, processors details, claims status, TDP material type, destination sold, total weight sold and PI payment total by TDP type.
' where ReportName like 'Processor Volume Report%'

update Report set [Description]='
This report runs for all claims. The report provide transaction line details of claimed Disposition of Residuals entries by disposition reason, material type, scale and estimate weight (if applicable), and tire counts by type for culled tires sales.
' where ReportName like 'Processor Disposition of Residual%'

update Report set [Description]='
This report provides registration, status, business and contact details relating to all active and inactive participants (Stewards, Collectors, Haulers, Processors, RPMs).
' where ReportName like 'Registrant Cumulative Weekly Report%'     

update Report set [Description]='
This report pulls active Collector business details for Collectors that have a postal code starting with “K”. Note: This report does not pull Collectors with a primary business type of “Generator”.
' where ReportName like 'Web Listing Collector Report Postal K%'

update Report set [Description]='
This report pulls active Collector business details for Collectors that have a postal code starting with “L”. Note: This report does not pull Collectors with a primary business type of “Generator”.
' where ReportName like 'Web Listing Collector Report Postal L%'

update Report set [Description]='
This report pulls active Collector business details for Collectors that have a postal code starting with “M”. Note: This report does not pull Collectors with a primary business type of “Generator”.
' where ReportName like 'Web Listing Collector Report Postal M%'

update Report set [Description]='
This report pulls active Collector business details for Collectors that have a postal code starting with “N”. Note: This report does not pull Collectors with a primary business type of “Generator”.
' where ReportName like 'Web Listing Collector Report Postal N%'

update Report set [Description]='
This report pulls active Collector business details for Collectors that have a postal code starting with “P”. Note: This report does not pull Collectors with a primary business type of “Generator”.
' where ReportName like 'Web Listing Collector Report Postal P%'

update Report set [Description]='
This report pulls active Haulers business details.
' where ReportName like 'Web Listing Hauler Report%'

update Report set [Description]='
This report pulls active Stewards business details.
' where ReportName like 'Web Listing Steward Report%'

update Report set [Description]='
This report pulls active Processor business details.
' where ReportName like 'Web Listing Processor Report%'

update Report set [Description]='
This report pulls active RPM business details.
' where ReportName like 'Web Listing Recycle Product Manufacturers Report%'

update Report set [Description]='
This report displays Collector tire origins along with their respective tire counts, by claim period, for the date range and registration number specified in the query.
' where ReportName like 'Collector Tire Origin Report%'

update Report set [Description]='
This report provides users with the number of tires collected, by type, within a postal code FSA and within a specified date range. Clicking on a FSA in the results panel will display all the tire collection transactions in that FSA within the specified date range.
' where ReportName like 'Collection/Generation of tires based on FSA Report%'

update Report set [Description]='
This report displays the tire collection transactions for a specific Hauler within a specified date range and includes the estimated weights per tire type.
' where ReportName like 'Hauler Tire Collection Report%'