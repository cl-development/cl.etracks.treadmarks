USE [tmdb]
GO

/****** Object:  Table [dbo].[Claim]    Script Date: 24/09/2015 2:01:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Claim](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParticipantUnifier] [nvarchar](32) NOT NULL,
	[ClaimPeriodID] [int] NOT NULL,
	[ParticipantID] [int] NOT NULL,
	[ParticipantNumber] [nvarchar](50) NOT NULL,
	[ParticipantAddressID] [int] NOT NULL,
	[participantName] [varchar](50) NULL,
	[TotalTax] [float] NULL,
	[AdjustmentTotal] [float] NULL,
	[ClaimsAmountTotal] [float] NULL,
	[ClaimsAmountCredit] [float] NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Notes] [nvarchar](100) NULL,
	[SubmissionDate] [datetime] NULL,
	[ApproveDate] [datetime] NULL,
	[ApproveDate2] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ReviewByDate] [datetime] NULL,
	[ChequeDueDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__Claim] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Claim]  WITH CHECK ADD  CONSTRAINT [FK_Claim_Address] FOREIGN KEY([ParticipantAddressID])
REFERENCES [dbo].[Address] ([ID])
GO

ALTER TABLE [dbo].[Claim] CHECK CONSTRAINT [FK_Claim_Address]
GO

ALTER TABLE [dbo].[Claim]  WITH CHECK ADD  CONSTRAINT [FK_Claim_Period] FOREIGN KEY([ClaimPeriodID])
REFERENCES [dbo].[Period] ([ID])
GO

ALTER TABLE [dbo].[Claim] CHECK CONSTRAINT [FK_Claim_Period]
GO

ALTER TABLE [dbo].[Claim]  WITH CHECK ADD  CONSTRAINT [FK_Claim_Vendor] FOREIGN KEY([ParticipantID])
REFERENCES [dbo].[Vendor] ([ID])
GO

ALTER TABLE [dbo].[Claim] CHECK CONSTRAINT [FK_Claim_Vendor]
GO


