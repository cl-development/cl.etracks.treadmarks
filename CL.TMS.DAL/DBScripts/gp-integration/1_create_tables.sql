/*

** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** SET INCREMENT SEED TO 10000 FOR GpiBatch TABLE ID COLLUMN ONCE GpiBatch CREATED
** MAKE SURE IN TABLE appSetting has entry 'GP:INTERID','OTS'
** MAKE SURE IN TABLE appSetting has entry 'GP:INTERID','OTS'
** MAKE SURE IN TABLE appSetting has entry 'GP:INTERID','OTS'
** MAKE SURE IN TABLE appSetting has entry 'GP:INTERID','OTS'
** MAKE SURE IN TABLE appSetting has entry 'GP:INTERID','OTS'

*/

GO
/****** Object:  Table [dbo].[GpChequeBook]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpChequeBook]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[GpChequeBook](
	[account_number] [varchar](13) NOT NULL,
	[name] [varchar](45) NOT NULL,
	[is_default] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_gp_cheque_book_account_number] PRIMARY KEY CLUSTERED 
(
	[account_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GpFiscalYear]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpFiscalYear]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[GpFiscalYear](
	[ID] [int] IDENTITY(6,1) NOT NULL,
	[MinimumDocDate] [date] NOT NULL,
	[EffectiveStartDate] [date] NOT NULL,
	[EffectiveEndDate] [date] NOT NULL,
 CONSTRAINT [PK_gp_fiscal_year_id] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[GpiAccount]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpiAccount]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[GpiAccount](
	[id] [bigint] NOT NULL,
	[account_number] [varchar](13) NOT NULL,
	[description] [varchar](255) NOT NULL,
	[distribution_type_id] [bigint] NOT NULL,
	[tire_type_id] [bigint] NULL CONSTRAINT [DF__gpiaccoun__tire___48A5B54C]  DEFAULT (NULL),
	[registrant_type_ind] [char](2) NOT NULL,
	[ShortName] [nvarchar](20) NULL,	
 CONSTRAINT [PK_gpiaccount_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GpiBatch]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpiBatch]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[GpiBatch](
	[ID] [int] IDENTITY(10000,1) NOT NULL,
	[GpiTypeID] [varchar](3) NOT NULL,
	[GpiBatchCount] [int] NOT NULL,
	[GpiStatusId] [varchar](2) NOT NULL,
	[CreatedDate] [datetime2](0) NOT NULL,
	[CreatedBy] [varchar](45) NOT NULL,
	[LastUpdatedDate] [datetime2](0) NOT NULL,
	[LastUpdatedBy] [varchar](45) NOT NULL,
	[Message] [varchar](max) NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_gpibatch_gpibatch_id] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GpiBatchEntry]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpiBatchEntry]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[GpiBatchEntry](
	[ID] [int] IDENTITY(564095,1) NOT NULL,
	[GpiBatchID] [int] NOT NULL,
	[GpiTxnNumber] [varchar](45) NOT NULL,
	[GpiBatchEntryDataKey] [varchar](45) NOT NULL,
	[GpiStatusID] [varchar](3) NOT NULL,
	[CreatedDate] [datetime2](0) NOT NULL,
	[CreatedBy] [varchar](45) NOT NULL,
	[LastUpdatedDate] [datetime2](0) NOT NULL,
	[LastUpdatedBy] [varchar](45) NOT NULL,
	[Message] [varchar](max) NULL,
	[IntStatus] [bigint] NOT NULL CONSTRAINT [DF__GpiBatchE__int_s__33208881]  DEFAULT ((0)),
	[IntDate] [datetime2](0) NULL CONSTRAINT [DF__GpiBatchE__int_d__3414ACBA]  DEFAULT (NULL),
	[RowVersion] [timestamp] NOT NULL,
	[ClaimId] [int] NOT NULL,	
 CONSTRAINT [PK_gpibatch_entry_gpibatchentry_id] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsPmTransaction]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsPmTransaction]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsPmTransaction](
	[BACHNUMB] [char](15) NOT NULL,
	[VCHNUMWK] [char](17) NOT NULL,
	[VENDORID] [char](15) NOT NULL,
	[DOCNUMBR] [char](20) NOT NULL,
	[DOCTYPE] [int] NOT NULL,
	[DOCAMNT] [decimal](19, 2) NOT NULL,
	[DOCDATE] [datetime2](0) NOT NULL,
	[PSTGDATE] [datetime2](0) NULL,
	[VADDCDPR] [char](15) NULL,
	[PYMTRMID] [char](20) NULL,
	[TAXSCHID] [char](15) NULL,
	[DUEDATE] [datetime2](0) NULL,
	[PRCHAMNT] [decimal](19, 2) NOT NULL,
	[TAXAMNT] [decimal](19, 2) NULL,
	[PORDNMBR] [char](20) NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(80540,1) NOT NULL,
	[GpiBatchEntryId] [int] NOT NULL,
 CONSTRAINT [PK_rr_ots_pm_transactions_VCHNUMWK] PRIMARY KEY CLUSTERED 
(
	[VCHNUMWK] ASC,
	[DOCTYPE] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsPmTransDistribution]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsPmTransDistribution]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsPmTransDistribution](
	[VCHNUMWK] [char](17) NOT NULL,
	[DOCTYPE] [int] NOT NULL,
	[VENDORID] [char](15) NOT NULL,
	[DSTSQNUM] [int] NOT NULL,
	[DISTTYPE] [int] NOT NULL,
	[DistRef] [char](30) NULL,
	[ACTNUMST] [char](75) NOT NULL,
	[DEBITAMT] [decimal](19, 2) NOT NULL,
	[CRDTAMNT] [decimal](19, 2) NOT NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(266560,1) NOT NULL,
CONSTRAINT [PK_rr_ots_pm_trans_distributions_VCHNUMWK] PRIMARY KEY CLUSTERED 
(
	[VCHNUMWK] ASC,
	[DOCTYPE] ASC,
	[DSTSQNUM] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsPmVendor]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsPmVendor]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsPmVendor](
	[VENDORID] [char](15) NOT NULL,
	[VENDNAME] [char](65) NOT NULL,
	[VADDCDPR] [char](15) NOT NULL,
	[VNDCLSID] [char](11) NULL DEFAULT (NULL),
	[VNDCNTCT] [char](61) NULL DEFAULT (NULL),
	[ADDRESS1] [char](61) NOT NULL,
	[ADDRESS2] [char](61) NULL DEFAULT (NULL),
	[ADDRESS3] [char](61) NULL DEFAULT (NULL),
	[CITY] [char](35) NOT NULL,
	[STATE] [char](29) NOT NULL,
	[ZIPCODE] [char](11) NOT NULL,
	[COUNTRY] [char](61) NULL DEFAULT (NULL),
	[PHNUMBR1] [char](21) NULL DEFAULT (NULL),
	[FAXNUMBR] [char](21) NULL DEFAULT (NULL),
	[CURNCYID] [char](15) NULL DEFAULT (NULL),
	[TAXSCHID] [char](15) NULL DEFAULT (NULL),
	[PYMTRMID] [char](21) NULL DEFAULT (NULL),
	[USERDEF1] [char](50) NOT NULL DEFAULT (N''),
	[USERDEF2] [char](50) NULL DEFAULT (NULL),
	[VENDSTTS] [int] NULL DEFAULT (NULL),
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL DEFAULT (NULL),
	[INTDATE] [datetime2](0) NULL DEFAULT (NULL),
	[ERRORCODE] [varchar](200) NULL DEFAULT (NULL),
	[DEX_ROW_ID] [int] IDENTITY(23999,1) NOT NULL,
	[gpibatchentry_id] [bigint] NOT NULL,	
	[EmailToAddress] [char](61) NULL,
	[EmailCcAddress] [char](50) NULL,
	[BANKNAME] [varchar](61) NULL,
	[EFTTransitNumber] [nvarchar](64) NULL,
	[EFTBankNumber] [nvarchar](64) NULL,
	[EFTBankAcct] [nvarchar](64) NULL,
	[Key] [nvarchar](64) NULL,			



	
/*	
	[BankName] [nvarchar](50) NULL,
	[EftTransitRoutingNo] [nvarchar](128) NULL,
	[EftBankAcct] [nvarchar](64) NULL,
	[EmailToAddress] [nvarchar](50) NULL,
	[EmailCcAddress] [nvarchar](50) NULL,	
	[Key] [nvarchar](64) NULL,		
*/
 CONSTRAINT [PK_rr_ots_pm_vendors_VENDORID] PRIMARY KEY CLUSTERED 
(
	[VENDORID] ASC,
	[VADDCDPR] ASC,
	[INTERID] ASC,
	[USERDEF1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsRmApply]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmApply]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsRmApply](
	[APTODCNM] [char](21) NOT NULL,
	[APFRDCNM] [char](21) NOT NULL,
	[APPTOAMT] [decimal](19, 2) NOT NULL,
	[APFRDCTY] [smallint] NOT NULL,
	[APTODCTY] [smallint] NOT NULL,
	[APPLYDATE] [datetime2](0) NULL,
	[GLPOSTDT] [datetime2](0) NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(21557,1) NOT NULL,
 CONSTRAINT [PK_rr_ots_rm_apply_APTODCNM] PRIMARY KEY CLUSTERED 
(
	[APTODCNM] ASC,
	[APFRDCNM] ASC,
	[APFRDCTY] ASC,
	[APTODCTY] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsRmCashReceipt]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmCashReceipt]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsRmCashReceipt](
	[BACHNUMB] [char](15) NOT NULL,
	[DOCNUMBR] [char](21) NOT NULL,
	[CUSTNMBR] [char](15) NOT NULL,
	[RMDTYPAL] [smallint] NOT NULL,
	[DOCDATE] [datetime2](0) NULL,
	[ORTRXAMT] [decimal](19, 2) NOT NULL,
	[CSHRCTYP] [int] NOT NULL,
	[CHEKNMBR] [char](21) NOT NULL,
	[CHEKBKID] [char](15) NOT NULL,
	[CRCARDID] [char](15) NULL,
	[CURNCYID] [char](15) NULL,
	[TRXDSCRN] [char](31) NULL,
	[GLPOSTDT] [datetime2](0) NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(21564,1) NOT NULL,
	[gpibatchentry_id] [bigint] NOT NULL,
 CONSTRAINT [PK_rr_ots_rm_cash_receipt_DOCNUMBR] PRIMARY KEY CLUSTERED 
(
	[DOCNUMBR] ASC,
	[RMDTYPAL] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RrOtsRmCustomer]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmCustomer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsRmCustomer](
	[CUSTNMBR] [char](15) NOT NULL,
	[CUSTNAME] [char](65) NOT NULL,
	[CUSTCLAS] [char](15) NULL DEFAULT (NULL),
	[CNTCPRSN] [char](61) NULL DEFAULT (NULL),
	[ADRSCODE] [char](15) NOT NULL,
	[TAXSCHID] [char](15) NULL DEFAULT (NULL),
	[ADDRESS1] [char](61) NOT NULL,
	[ADDRESS2] [char](61) NULL DEFAULT (NULL),
	[ADDRESS3] [char](61) NULL DEFAULT (NULL),
	[COUNTRY] [char](61) NULL DEFAULT (NULL),
	[CITY] [char](35) NOT NULL,
	[STATE] [char](29) NOT NULL,
	[ZIP] [char](11) NOT NULL,
	[PHNUMBR1] [char](21) NULL DEFAULT (NULL),
	[FAX] [char](21) NULL DEFAULT (NULL),
	[CURNCYID] [char](15) NULL DEFAULT (NULL),
	[PYMTRMID] [char](21) NULL DEFAULT (NULL),
	[USERDEF1] [char](50) NOT NULL DEFAULT (N''),
	[USERDEF2] [char](50) NULL DEFAULT (NULL),
	[INACTIVE] [int] NULL DEFAULT (NULL),
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL DEFAULT (NULL),
	[INTDATE] [datetime2](0) NULL DEFAULT (NULL),
	[ERRORCODE] [varchar](200) NULL DEFAULT (NULL),
	[DEX_ROW_ID] [int] IDENTITY(7824,1) NOT NULL,
	[gpibatchentry_id] [bigint] NOT NULL,
 CONSTRAINT [PK_rr_ots_rm_customers_CUSTNMBR] PRIMARY KEY CLUSTERED 
(
	[CUSTNMBR] ASC,
	[ADRSCODE] ASC,
	[INTERID] ASC,
	[USERDEF1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RrOtsRmTransaction]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmTransaction]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsRmTransaction](
	[RMDTYPAL] [smallint] NOT NULL,
	[RMDNUMWK] [char](17) NOT NULL,
	[DOCNUMBR] [char](21) NOT NULL,
	[DOCDESCR] [char](31) NULL,
	[DOCDATE] [datetime2](0) NULL,
	[BACHNUMB] [char](15) NOT NULL,
	[CUSTNMBR] [char](15) NOT NULL,
	[ADRSCODE] [char](15) NULL,
	[CSTPONBR] [char](21) NULL,
	[TAXSCHID] [char](15) NULL,
	[SLSAMNT] [decimal](19, 2) NOT NULL,
	[TAXAMNT] [decimal](19, 2) NULL,
	[DOCAMNT] [decimal](19, 2) NOT NULL,
	[PYMTRMID] [char](20) NULL,
	[DUEDATE] [datetime2](0) NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(24431,1) NOT NULL,
	[gpibatchentry_id] [bigint] NOT NULL,
 CONSTRAINT [PK_rr_ots_rm_transactions_RMDTYPAL] PRIMARY KEY CLUSTERED 
(
	[RMDTYPAL] ASC,
	[DOCNUMBR] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RrOtsRmTransDistribution]    Script Date: 4/11/2016 1:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmTransDistribution]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].[RrOtsRmTransDistribution](
	[RMDTYPAL] [smallint] NOT NULL,
	[DOCNUMBR] [char](20) NOT NULL,
	[CUSTNMBR] [char](15) NOT NULL,
	[SEQNUMBR] [int] NOT NULL,
	[DISTTYPE] [int] NOT NULL,
	[DistRef] [char](30) NULL,
	[ACTNUMST] [char](75) NOT NULL,
	[DEBITAMT] [decimal](19, 2) NOT NULL,
	[CRDTAMNT] [decimal](19, 2) NOT NULL,
	[USERDEF1] [char](50) NULL,
	[USERDEF2] [char](50) NULL,
	[INTERID] [char](5) NOT NULL,
	[INTSTATUS] [int] NULL,
	[INTDATE] [datetime2](0) NULL,
	[ERRORCODE] [varchar](200) NULL,
	[DEX_ROW_ID] [int] IDENTITY(56206,1) NOT NULL,
 CONSTRAINT [PK_rr_ots_rm_trans_distributions_RMDTYPAL] PRIMARY KEY CLUSTERED 
(
	[RMDTYPAL] ASC,
	[DOCNUMBR] ASC,
	[SEQNUMBR] ASC,
	[INTERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[GpChequeBook] ([account_number], [name], [is_default]) VALUES (N'1010-00-00-00', N'RBC OPERATING', 1)
INSERT [dbo].[GpChequeBook] ([account_number], [name], [is_default]) VALUES (N'1020-00-00-00', N'RBC US', 0)
INSERT [dbo].[GpChequeBook] ([account_number], [name], [is_default]) VALUES (N'1030-00-00-00', N'CIBC OPERATING', 0)
SET IDENTITY_INSERT [dbo].[GpFiscalYear] ON 

INSERT [dbo].[GpFiscalYear] ([ID], [MinimumDocDate], [EffectiveStartDate], [EffectiveEndDate]) VALUES (7, CAST(N'2012-01-01' AS Date), CAST(N'2009-09-01' AS Date), CAST(N'2013-02-04' AS Date))
INSERT [dbo].[GpFiscalYear] ([ID], [MinimumDocDate], [EffectiveStartDate], [EffectiveEndDate]) VALUES (8, CAST(N'2013-01-01' AS Date), CAST(N'2013-02-05' AS Date), CAST(N'2014-02-02' AS Date))
INSERT [dbo].[GpFiscalYear] ([ID], [MinimumDocDate], [EffectiveStartDate], [EffectiveEndDate]) VALUES (9, CAST(N'2014-01-01' AS Date), CAST(N'2014-02-03' AS Date), CAST(N'2015-01-30' AS Date))
INSERT [dbo].[GpFiscalYear] ([ID], [MinimumDocDate], [EffectiveStartDate], [EffectiveEndDate]) VALUES (10, CAST(N'2015-01-01' AS Date), CAST(N'2015-01-31' AS Date), CAST(N'2016-01-29' AS Date))
INSERT [dbo].[GpFiscalYear] ([ID], [MinimumDocDate], [EffectiveStartDate], [EffectiveEndDate]) VALUES (11, CAST(N'2016-01-01' AS Date), CAST(N'2016-01-30' AS Date), CAST(N'2999-12-31' AS Date))

SET IDENTITY_INSERT [dbo].[GpFiscalYear] OFF
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (1, N'1220-00-00-00', N'Accounts (Stewards) Receivable', 3, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (2, N'2000-00-00-00', N'Accounts (Collector) Payable', 2, NULL, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (3, N'2170-00-00-00', N'TSF Tax Payable', 13, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (4, N'2200-00-00-00', N'Tax Payable (HST)', 13, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (5, N'4000-90-00-99', N'Negative Adjustments', 3, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (6, N'4010-10-00-00', N'Tire Stewardship Fee - PLT', 9, 1, N'S ', N'PLT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (7, N'4015-20-00-00', N'Tire Stewardship Fee - MT', 9, 2, N'S ', N'MT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (8, N'4020-30-00-00', N'Tire Stewardship Fee - AG & LS', 9, 3, N'S ', N'AGLS')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (9, N'4025-40-00-00', N'Tire Stewardship Fee - IND', 9, 4, N'S ', N'IND')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (10, N'4030-50-00-00', N'Tire Stewardship Fee SOTR', 9, 5, N'S ', N'SOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (11, N'4035-60-00-00', N'Tire Stewardship Fee - MOTR', 9, 6, N'S ', N'MOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (12, N'4040-70-00-00', N'Tire Stewardship Fee - LOTR', 9, 7, N'S ', N'LOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (13, N'4045-80-00-00', N'Tire Stewardship Fee - GOTR', 9, 8, N'S ', N'GOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (14, N'4065-90-00-00', N'TSF Penalty', 9, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (15, N'4070-90-00-00', N'TSF Interest   ', 9, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (16, N'4110-10-20-40', N'CA - PLT', 6, 1, N'C ', N'')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (17, N'4115-10-20-40', N'CA - PLT HST', 10, 1, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (18, N'4120-20-20-40', N'CA - MT', 6, 2, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (19, N'4125-20-20-40', N'CA - MT HST', 10, 2, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (20, N'4130-30-20-40', N'CA - AG & LS', 6, 3, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (21, N'4135-30-20-40', N'CA - AG/LS HST', 10, 3, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (22, N'4140-40-20-40', N'CA - IND', 6, 4, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (23, N'4145-40-20-40', N'CA - IND HST', 10, 4, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (24, N'4150-50-20-40', N'CA - SOTR', 6, 5, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (25, N'4155-50-20-40', N'CA - SOTR HST', 10, 5, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (26, N'4160-60-20-40', N'CA - MOTR', 6, 6, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (27, N'4165-60-20-40', N'CA - MOTR HST', 10, 6, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (28, N'4170-70-20-40', N'CA - LOTR', 6, 7, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (29, N'4175-70-20-40', N'CA - LOTR HST', 10, 7, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (30, N'4180-80-20-40', N'CA - GOTR', 6, 8, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (31, N'4185-80-20-40', N'CA - GOTR HST', 10, 8, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (32, N'4201-10-50-40', N'MI - PLT', 6, 1, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (33, N'4203-20-50-40', N'MI - MT', 6, 2, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (34, N'4206-30-50-40', N'MI - AG & LS', 6, 3, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (35, N'4208-40-50-40', N'MI - IND', 6, 4, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (36, N'4210-90-50-40', N'MI TM Adjustments', 6, NULL, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (37, N'4211-50-50-40', N'MI - SOTR', 6, 5, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (38, N'4213-60-50-40', N'MI - MOTR', 6, 6, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (39, N'4216-70-50-40', N'MI - LOTR', 6, 7, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (40, N'4218-80-50-40', N'MI - GOTR', 6, 8, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (41, N'4245-90-40-40', N'PI - Out of Province TM Adjustments', 6, NULL, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (42, N'4246-10-40-40', N'PI - PLT', 6, 1, N'P ', N'PLT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (43, N'4248-20-40-40', N'PI - MT', 6, 2, N'P ', N'MT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (44, N'4251-30-40-40', N'PI - AG & LS', 6, 3, N'P ', N'AGLS')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (45, N'4253-40-40-40', N'PI - IND', 6, 4, N'P ', N'IND')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (46, N'4256-50-40-40', N'PI - SOTR', 6, 5, N'P ', N'SOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (47, N'4258-60-40-40', N'PI - MOTR', 6, 6, N'P ', N'MOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (48, N'4261-70-40-40', N'PI - LOTR', 6, 7, N'P ', N'LOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (49, N'4263-80-40-40', N'PI - GOTR', 6, 8, N'P ', N'GOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (50, N'4266-10-40-40', N'PI - Surplus PLT', 6, 1, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (51, N'4268-20-40-40', N'PI - Surplus MT', 6, 2, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (52, N'4271-30-40-40', N'PI - Surplus AG & LS', 6, 3, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (53, N'4273-40-40-40', N'PI - Surplus IND', 6, 4, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (54, N'4276-50-40-40', N'PI Surplus SOTR', 6, 5, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (55, N'4278-60-40-40', N'PI - Surplus MOTR', 6, 6, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (56, N'4281-70-40-40', N'PI - Surplus LOTR', 6, 7, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (57, N'4283-80-40-40', N'PI - Surplus GOTR', 6, 8, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (58, N'4365-90-40-40', N'PI TM Adjustments', 6, NULL, N'P ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (59, N'4381-10-30-40', N'TI - PLT', 6, 1, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (60, N'4382-10-30-40', N'TI - PLT HST', 10, 1, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (61, N'4383-20-30-40', N'TI - MT', 6, 2, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (62, N'4384-20-30-40', N'TI - MT HST', 10, 2, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (63, N'4386-30-30-40', N'TI - AG & LS', 6, 3, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (64, N'4387-30-30-40', N'TI - AG & LS HST', 10, 3, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (65, N'4388-40-30-40', N'TI - IND', 6, 4, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (66, N'4389-40-30-40', N'TI - IND HST', 10, 4, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (67, N'4397-50-30-40', N'TI - SOTR', 6, 5, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (68, N'4398-50-30-40', N'TI- SOTR HST', 10, 5, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (69, N'4399-60-30-40', N'TI - MOTR', 6, 6, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (70, N'4401-60-30-40', N'TI - MOTR HST', 10, 6, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (71, N'4402-70-30-40', N'TI - LOTR', 6, 7, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (72, N'4403-70-30-40', N'TI - LOTR HST', 10, 7, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (73, N'4404-80-30-40', N'TI - GOTR', 6, 8, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (74, N'4406-80-30-40', N'TI - GOTR HST', 10, 8, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (75, N'4426-10-30-40', N'Surplus TI Adhoc - PLT', 6, 1, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (76, N'4427-10-30-40', N'Surplus TI Adhoc - PLT HST', 10, 1, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (77, N'4428-20-30-40', N'Surplus TI Adhoc - MT', 6, 2, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (78, N'4429-20-30-40', N'Surplus TI Adhoc - MT HST', 10, 2, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (79, N'4431-30-30-40', N'Surplus TI - Adhoc - AG & LS', 6, 3, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (80, N'4432-30-30-40', N'Surplus TI Adhoc - AG & LS HST', 10, 3, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (81, N'4433-40-30-40', N'Surplus TI Adhoc - IND', 6, 4, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (82, N'4434-40-30-40', N'Surplus TI Adhoc - IND HST', 10, 4, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (83, N'4441-50-30-40', N'Surplus TI Adhoc - SOTR', 6, 5, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (84, N'4442-50-30-40', N'Surplus TI Adhoc - SOTR HST', 10, 5, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (85, N'4443-60-30-40', N'Surplus TI Adhoc - MOTR', 6, 6, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (86, N'4444-60-30-40', N'Surplus TI Adhoc - MOTR HST', 10, 6, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (87, N'4446-70-30-40', N'Surplus TI Adhoc - LOTR', 6, 7, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (88, N'4447-70-30-40', N'Surplus TI Adhoc - LOTR HST', 10, 7, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (89, N'4448-80-30-40', N'Surplus TI Adhoc - GOTR', 6, 8, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (90, N'4449-10-30-40', N'Surplus TI Redirect - PLT', 6, 1, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (91, N'4451-10-30-40', N'Surplus TI Redirect - PLT HST', 10, 1, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (92, N'4452-20-30-40', N'Surplus TI Redirect - MT', 6, 2, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (93, N'4453-20-30-40', N'Surplus TI Redirect - MT HST', 10, 2, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (94, N'4454-30-30-40', N'Surplus TI Redirect - AG & LS', 6, 3, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (95, N'4461-30-30-40', N'Surplus TI Redirect - AG & LS HST', 10, 3, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (96, N'4462-40-30-40', N'Surplus TI Redirect - IND', 6, 4, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (97, N'4463-40-30-40', N'Surplus TI Redirect - IND HST', 10, 4, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (98, N'4464-50-30-40', N'Surplus TI Redirect - SOTR', 6, 5, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (99, N'4470-90-30-40', N'TI - TM Adjustment HST', 10, NULL, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (100, N'4471-50-30-40', N'Surplus TI Redirect - SOTR HST', 10, 5, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (101, N'4472-60-30-40', N'Surplus TI Redirect - MOTR', 6, 6, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (102, N'4473-60-30-40', N'Surplus TI Redirect - MOTR HST', 10, 6, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (103, N'4474-70-30-40', N'Surplus TI Redirect - LOTR', 6, 7, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (104, N'4475-90-30-40', N'TI - TM Adjustments', 6, NULL, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (105, N'4476-70-30-40', N'Surplus TI Redirect - LOTR HST', 10, 7, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (106, N'4477-80-30-40', N'Surplus TI Redirect - GOTR', 6, 8, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (107, N'4478-80-30-40', N'Surplus TI Redirect - GOTR HST', 10, 8, N'H2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (108, N'4479-80-30-40', N'Surplus TI Adhoc - GOTR HST', 10, 8, N'H1', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (109, N'2000-00-00-00', N'Accounts (Hauler) Payable', 2, NULL, N'H ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (110, N'2000-00-00-00', N'Accounts (Processor) Payable', 2, NULL, N'P ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (111, N'2000-00-00-00', N'Accounts (Surplus Processor) Payable', 2, NULL, N'P2', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (112, N'2000-00-00-00', N'Accounts (Manufacturer) Payable', 2, NULL, N'M ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (113, N'4011-10-00-00', N'On-Road Tires: Passenger & Light Truck', 9, 9, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (114, N'4016-20-00-00', N'On-Road Tires: Medium Truck', 9, 10, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (115, N'4071-30-00-00', N'Off-Road Pneumatic Tires: 1 to <= 15 Kgs', 9, 11, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (116, N'4072-30-00-00', N'Off-Road Pneumatic Tires: > 15 to <= 30 Kgs', 9, 12, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (117, N'4073-30-00-00', N'Off-Road Pneumatic Tires: > 30 to <= 70 Kgs', 9, 13, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (118, N'4074-30-00-00', N'Off-Road Pneumatic Tires: > 70 to <= 120 Kgs', 9, 14, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (119, N'4075-30-00-00', N'Off-Road Pneumatic Tires: > 120 to <= 250 Kgs', 9, 15, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (120, N'4076-30-00-00', N'Off-Road Pneumatic Tires: > 250 to <= 375 Kgs', 9, 16, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (121, N'4077-30-00-00', N'Off-Road Pneumatic Tires: > 375 to <= 700 Kgs', 9, 17, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (122, N'4078-30-00-00', N'Off-Road Pneumatic Tires: > 700 to <= 1200 Kgs', 9, 18, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (123, N'4079-30-00-00', N'Off-Road Pneumatic Tires: > 1200 Kgs', 9, 19, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (124, N'4080-30-00-00', N'Solid & Resilient Tires: 1 to <= 30 Kgs', 9, 20, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (125, N'4081-30-00-00', N'Solid & Resilient Tires: > 30 to <= 60 Kgs', 9, 21, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (126, N'4082-30-00-00', N'Solid & Resilient Tires: > 60 to <= 250 Kgs', 9, 22, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (127, N'4083-30-00-00', N'Solid & Resilient Tires: > 250 to <= 375 Kgs', 9, 23, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (128, N'4084-30-00-00', N'Solid & Resilient Tires: > 375 to <= 700 Kgs', 9, 24, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (129, N'4085-30-00-00', N'Solid & Resilient Tires: > 700 to <= 1200 Kgs', 9, 25, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (130, N'4086-30-00-00', N'Solid & Resilient Tires: > 1200 Kgs', 9, 26, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (131, N'4011-10-00-00', N'On-Road Tires: Passenger & Light Truck', 9, 27, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (132, N'4241-10-40-40', N'TI for PLT tires ', 6, 1, N'PT', N'PLT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (133, N'4242-20-40-40', N'TI for MT tires	', 6, 2, N'PT', N'MT')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (134, N'4243-30-40-40', N'TI for AG/LS tire', 6, 3, N'PT', N'AGLS')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (135, N'4244-40-40-40', N'TI for IND tires ', 6, 4, N'PT', N'IND')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (136, N'4361-50-40-40', N'TI for SOTR tires', 6, 5, N'PT', N'SOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (137, N'4362-60-40-40', N'TI for MOTR tires', 6, 6, N'PT', N'MOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (138, N'4363-70-40-40', N'TI for LOTR tires', 6, 7, N'PT', N'LOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (139, N'4364-80-40-40', N'TI for GOTR tires', 6, 8, N'PT', N'GOTR')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (140, N'4366-10-30-40', N'TI- Premium PLT', 6, 1, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (141, N'4367-20-30-40', N'TI- Premium MT', 6, 2, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (142, N'4368-30-30-40', N'TI- Premium AG/LS', 6, 3, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (143, N'4369-40-30-40', N'TI- Premium IND', 6, 4, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (144, N'4371-50-30-40', N'TI- Premium SOTR', 6, 5, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (145, N'4372-60-30-40', N'TI- Premium MOTR', 6, 6, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (146, N'4373-70-30-40', N'TI- Premium LOTR', 6, 7, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (147, N'4374-80-30-40', N'TI- Premium GOTR', 6, 8, N'H3', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (148, N'4376-60-30-40', N'TI-DOT MOTR', 6, 6, N'H4', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (149, N'4377-70-30-40', N'TI-DOT LOTR', 6, 7, N'H4', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (150, N'4378-80-30-40', N'TI-DOT GOTR', 6, 8, N'H4', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (151, N'4390-90-30-40', N'TI-DOT OTR HST', 10, NULL, N'H4', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (152, N'4000-00-00-00', N'Accounts (Stewards,EFT) Receivable', 3, NULL, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (153, N'4011-10-00-00', N'On-Road Tires: Passenger & Light Truck', 9, 28, N'S ', N'C1')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (154, N'4016-20-00-00', N'On-Road Tires: Medium Truck', 9, 29, N'S ', N'C2')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (155, N'4071-30-00-00', N'Off-Road Pneumatic Tires: 1 to <= 15 Kgs', 9, 30, N'S ', N'C3')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (156, N'4072-30-00-00', N'Off-Road Pneumatic Tires: > 15 to <= 30 Kgs', 9, 31, N'S ', N'C4')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (157, N'4073-30-00-00', N'Off-Road Pneumatic Tires: > 30 to <= 70 Kgs', 9, 32, N'S ', N'C5')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (158, N'4074-30-00-00', N'Off-Road Pneumatic Tires: > 70 to <= 120 Kgs', 9, 33, N'S ', N'C6')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (159, N'4075-30-00-00', N'Off-Road Pneumatic Tires: > 120 to <= 250 Kgs', 9, 34, N'S ', N'C7')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (160, N'4076-30-00-00', N'Off-Road Pneumatic Tires: > 250 to <= 375 Kgs', 9, 35, N'S ', N'C8')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (161, N'4077-30-00-00', N'Off-Road Pneumatic Tires: > 375 to <= 700 Kgs', 9, 36, N'S ', N'C9')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (162, N'4078-30-00-00', N'Off-Road Pneumatic Tires: > 700 to <= 1200 Kgs', 9, 37, N'S ', N'C10')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (163, N'4079-30-00-00', N'Off-Road Pneumatic Tires: > 1200 Kgs', 9, 38, N'S ', N'C11')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (164, N'4080-30-00-00', N'Solid & Resilient Tires: 1 to <= 30 Kgs', 9, 39, N'S ', N'C12')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (165, N'4081-30-00-00', N'Solid & Resilient Tires: > 30 to <= 60 Kgs', 9, 40, N'S ', N'C13')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (166, N'4082-30-00-00', N'Solid & Resilient Tires: > 60 to <= 250 Kgs', 9, 41, N'S ', N'C14')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (167, N'4083-30-00-00', N'Solid & Resilient Tires: > 250 to <= 375 Kgs', 9, 42, N'S ', N'C15')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (168, N'4084-30-00-00', N'Solid & Resilient Tires: > 375 to <= 700 Kgs', 9, 43, N'S ', N'C16')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (169, N'4085-30-00-00', N'Solid & Resilient Tires: > 700 to <= 1200 Kgs', 9, 44, N'S ', N'C17')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (170, N'4086-30-00-00', N'Solid & Resilient Tires: > 1200 Kgs', 9, 45, N'S ', N'C18')
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (171, N'4011-10-00-00', N'On-Road Tires: Passenger & Light Truck', 9, 46, N'S ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (172, N'4100-90-20-99', N'Collector Adjustments', 6, null, N'C ', NULL)
GO
INSERT [dbo].[GpiAccount] ([id], [account_number], [description], [distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName]) VALUES (173, N'4105-90-20-40', N'Collector Adjustments Tax', 10, null, N'C ', NULL)
GO


GO
SET IDENTITY_INSERT [dbo].[RrOtsRmCustomer] ON 
SET IDENTITY_INSERT [dbo].[RrOtsRmCustomer] OFF
SET ANSI_PADDING ON

GO

/****** Object:  Index [gp_cheque_book$account_number_UNIQUE]    Script Date: 4/11/2016 1:47:24 PM ******/
ALTER TABLE [dbo].[GpChequeBook] ADD  CONSTRAINT [gp_cheque_book$account_number_UNIQUE] UNIQUE NONCLUSTERED 
(
	[account_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__PSTGD__37E53D9E]  DEFAULT (NULL) FOR [PSTGDATE]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__VADDC__38D961D7]  DEFAULT (NULL) FOR [VADDCDPR]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__PYMTR__39CD8610]  DEFAULT (NULL) FOR [PYMTRMID]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__TAXSC__3AC1AA49]  DEFAULT (NULL) FOR [TAXSCHID]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__DUEDA__3BB5CE82]  DEFAULT (NULL) FOR [DUEDATE]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__TAXAM__3CA9F2BB]  DEFAULT (NULL) FOR [TAXAMNT]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__PORDN__3D9E16F4]  DEFAULT (NULL) FOR [PORDNMBR]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__USERD__3E923B2D]  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__USERD__3F865F66]  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__INTST__407A839F]  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__INTDA__416EA7D8]  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsPmTransaction] ADD  CONSTRAINT [DF__RrOtsPmTr__ERROR__4262CC11]  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__DistR__46335CF5]  DEFAULT (NULL) FOR [DistRef]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__USERD__4727812E]  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__USERD__481BA567]  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__INTST__490FC9A0]  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__INTDA__4A03EDD9]  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsPmTransDistribution] ADD  CONSTRAINT [DF__RrOtsPmTr__ERROR__4AF81212]  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [APPLYDATE]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [GLPOSTDT]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsRmApply] ADD  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [DOCDATE]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [CRCARDID]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [CURNCYID]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [TRXDSCRN]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [GLPOSTDT]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsRmCashReceipt] ADD  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [DOCDESCR]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [DOCDATE]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [ADRSCODE]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [CSTPONBR]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [TAXSCHID]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [TAXAMNT]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [PYMTRMID]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [DUEDATE]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsRmTransaction] ADD  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [DistRef]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [USERDEF1]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [USERDEF2]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [INTSTATUS]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [INTDATE]
GO
ALTER TABLE [dbo].[RrOtsRmTransDistribution] ADD  DEFAULT (NULL) FOR [ERRORCODE]
GO
ALTER TABLE [dbo].[GpiBatch]  WITH CHECK ADD  CONSTRAINT [FK_GpiBatch_GpiBatch] FOREIGN KEY([ID])
REFERENCES [dbo].[GpiBatch] ([ID])
GO
ALTER TABLE [dbo].[GpiBatch] CHECK CONSTRAINT [FK_GpiBatch_GpiBatch]
GO
ALTER TABLE [dbo].[GpiBatchEntry]  WITH CHECK ADD  CONSTRAINT [FK_GpiBatchEntry_GpiBatch] FOREIGN KEY([GpiBatchID])
REFERENCES [dbo].[GpiBatch] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GpiBatchEntry] CHECK CONSTRAINT [FK_GpiBatchEntry_GpiBatch]
GO
INSERT INTO [dbo].[AppSetting]([ID], [Key], Value) values ((select TOP 1 max(id)+1 from [dbo].[appsetting]), 'GP:INTERID','OTS'); 
GO
ALTER TABLE dbo.Vendor ALTER COLUMN InBatch bit NULL
GO
ALTER TABLE dbo.Vendor ADD GpUpdate bit NULL
GO
ALTER TABLE dbo.Customer ALTER COLUMN InBatch bit NULL
GO
ALTER TABLE dbo.Customer ADD GpUpdate bit NULL
GO
ALTER TABLE dbo.TSFClaim ALTER COLUMN InBatch bit NULL
GO
ALTER TABLE dbo.Claim ADD InBatch bit NULL
GO
ALTER TABLE dbo.ClaimPaymentSummary ALTER COLUMN [ChequeNumber] [nvarchar](500) NULL
GO
/*
ALTER TABLE dbo.Claim ALTER COLUMN [IsBatchPosted] [bit] NOT NULL default(0);
GO
*/

