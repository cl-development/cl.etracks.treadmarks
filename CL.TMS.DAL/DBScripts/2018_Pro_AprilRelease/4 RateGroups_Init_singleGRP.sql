﻿USE [tmdb]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
delete from RateTransactionNote where RateTransactionID in (select ID from dbo.[RateTransaction] where PickupRateGroupId is not null and DeliveryRateGroupId is not null)
delete from dbo.[RateGroupRate]
delete from dbo.[RateTransaction] where PickupRateGroupId is not null and DeliveryRateGroupId is not null
delete from dbo.[VendorRateGroup]
delete from dbo.[Group]
delete from dbo.[RateGroup]

DBCC CHECKIDENT ('[VendorRateGroup]', RESEED, 1);
DBCC CHECKIDENT ('[Group]', RESEED, 1);
DBCC CHECKIDENT ('[RateGroup]', RESEED, 1);
GO
go

--add vendor group category
IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='VendorGroup')--if first line not exists, then insert whole batch
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,			[Code] ,			[Description] ,			[Category] ,	[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Collector Group',	'CollectorGroup',	'Processor Group',		'VendorGroup',	2,					1),
		('Processor Group',	'ProcessorGroup',	'Processor Group',		'VendorGroup',	4,					2)		
END

GO
--add rate group category
IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='RateGroup')--if first line not exists, then insert whole batch
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,			[Code] ,			[Description] ,			[Category] ,	[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Pickup Group',	'PickupGroup',		'Pickup Group',			'RateGroup',	1,					1),
		('Delivery Group',	'DeliveryGroup',	'Delivery Group',		'RateGroup',	0,					2)		
END
GO

--initial vendor group 
IF NOT EXISTS (SELECT 1 FROM [Group])--if first line not exists, then insert initial records
BEGIN
	declare @CreateByID int;
	declare @dt datetime;
	declare @categoryCollectorGroup [nvarchar](150);
	declare @categoryProcessorGroup [nvarchar](150);
	set @categoryCollectorGroup = (select Top 1 [Code] from TypeDefinition where Category='VendorGroup' and Code='CollectorGroup');
	set @categoryProcessorGroup = (select Top 1 [Code] from TypeDefinition where Category='VendorGroup' and Code='ProcessorGroup');

	set @CreateByID=(select Top 1 ID from [User] where FirstName='System' and LastName='Admin');

	set @dt = GETDATE()
		INSERT INTO [Group] 
			([GroupName] ,	[GroupDescription] ,		[GroupType],				[CreatedDate]      ,[CreatedBy])
			VALUES 
			('CG1',			'Collector Group 1',		@categoryCollectorGroup,	@dt,				@CreateByID),
			('PG1',			'Processor Group 1',		@categoryProcessorGroup,	@dt,				@CreateByID);
END
GO

--initial rate group 
IF NOT EXISTS (SELECT 1 FROM RateGroup)--if first line not exists, then insert initial records
BEGIN
	declare @CreateByID int, @RateGroupId int, @VendorGroupId int, @CollectorId int, @ProcessorId int;
	declare @dt datetime;
	declare @rateGroupCategoryPickup [nvarchar](150);
	declare @rateGroupCategoryDelivery [nvarchar](150);
	Declare @getNewRateGroupIDs cursor, @getNewVendorGroupIDs cursor, @getNewCollectorIDs cursor, @getNewProcessorIDs cursor
	set @rateGroupCategoryPickup = (select Top 1 [Code] from TypeDefinition where Category='RateGroup' and Code='PickupGroup');
	set @rateGroupCategoryDelivery = (select Top 1 [Code] from TypeDefinition where Category='RateGroup' and Code='DeliveryGroup');

	set @CreateByID=(select Top 1 ID from [User] where FirstName='System' and LastName='Admin');
	set @dt = GETDATE()
	IF NOT EXISTS (SELECT 1 FROM RateGroup)--if first line not exists, then insert initial records
	INSERT INTO [RateGroup]
		([RateGroupName] ,	[Category],						[CreatedDate],	[CreatedBy])
		VALUES 
		('PRG1',			@rateGroupCategoryPickup,		@dt,			@CreateByID),
		('DRG1',			@rateGroupCategoryDelivery,		@dt,			@CreateByID);

	set @getNewCollectorIDs = cursor for select Id from [Vendor] where [VendorType] = 2;
	set @getNewProcessorIDs = cursor for select Id from [Vendor] where [VendorType] = 4;

	--starting insert PRG1 and CG1
	set @getNewRateGroupIDs = cursor for select Id from [RateGroup] where [RateGroupName] in ('PRG1');
	set @getNewVendorGroupIDs = cursor for select Id from [Group] where [GroupName] in ('CG1');
	OPEN @getNewRateGroupIDs FETCH NEXT FROM @getNewRateGroupIDs INTO @RateGroupId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		OPEN @getNewVendorGroupIDs FETCH NEXT FROM @getNewVendorGroupIDs INTO @VendorGroupId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			OPEN @getNewCollectorIDs FETCH NEXT FROM @getNewCollectorIDs INTO @CollectorId
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF not EXISTS 
				(SELECT Id from VendorRateGroup where RateGroupId = @RateGroupId and GroupId = @VendorGroupId and VendorId = @CollectorId )
				BEGIN
					begin 
						insert into VendorRateGroup (RateGroupId,	GroupId,	VendorId,	CreatedDate,	CreatedBy) VALUES	(@RateGroupId, @VendorGroupId, @CollectorId, @dt,	@CreateByID)
						--select  @RateGroupId, @VendorGroupId, @CollectorId
					end 
				END
				FETCH NEXT
				FROM @getNewCollectorIDs INTO @CollectorId
			END
			CLOSE @getNewCollectorIDs
			FETCH NEXT
			FROM @getNewVendorGroupIDs INTO @VendorGroupId
		END
		CLOSE @getNewVendorGroupIDs
		FETCH NEXT
		FROM @getNewRateGroupIDs INTO @RateGroupId
	END
	CLOSE @getNewRateGroupIDs

	--starting insert DRG1 and PG1
	set @getNewRateGroupIDs = cursor for select Id from [RateGroup] where [RateGroupName] in ('DRG1');
	set @getNewVendorGroupIDs = cursor for select Id from [Group] where [GroupName] in ('PG1');
	OPEN @getNewRateGroupIDs FETCH NEXT FROM @getNewRateGroupIDs INTO @RateGroupId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		OPEN @getNewVendorGroupIDs FETCH NEXT FROM @getNewVendorGroupIDs INTO @VendorGroupId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			OPEN @getNewProcessorIDs FETCH NEXT FROM @getNewProcessorIDs INTO @ProcessorId
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF not EXISTS 
				(SELECT Id from VendorRateGroup where RateGroupId = @RateGroupId and GroupId = @VendorGroupId and VendorId = @ProcessorId )
				BEGIN
					begin 
						insert into VendorRateGroup (RateGroupId,	GroupId,	VendorId,	CreatedDate,	CreatedBy) VALUES	(@RateGroupId, @VendorGroupId, @ProcessorId, @dt,	@CreateByID)
						select  @RateGroupId, @VendorGroupId, @ProcessorId
					end 
				END
				FETCH NEXT
				FROM @getNewProcessorIDs INTO @ProcessorId
			END
			CLOSE @getNewProcessorIDs
			FETCH NEXT
			FROM @getNewVendorGroupIDs INTO @VendorGroupId
		END
		CLOSE @getNewVendorGroupIDs
		FETCH NEXT
		FROM @getNewRateGroupIDs INTO @RateGroupId
	END
	CLOSE @getNewRateGroupIDs

	DEALLOCATE @getNewCollectorIDs
	DEALLOCATE @getNewProcessorIDs
	DEALLOCATE @getNewVendorGroupIDs
	DEALLOCATE @getNewRateGroupIDs
END
GO

--SET IDENTITY_INSERT [dbo].[RateGroupRate] ON 
--select* from VendorRateGroup
--select* from RateGroup
--select* from RateTransaction
--select* from [Group]

declare @PickupRateGroupId int, @DeliveryRateGroupId int
select @PickupRateGroupId = (select Id from RateGroup where RateGroupName='PRG1');
select @DeliveryRateGroupId  = (select Id from RateGroup where RateGroupName='DRG1');
INSERT into [dbo].[RateTransaction] 
(CreatedDate,	CreatedByID,	ModifiedDate,	ModifiedByID,	Category,	EffectiveStartDate,	EffectiveEndDate,	IsSpecificRate,	PickupRateGroupId,	DeliveryRateGroupId) values
('2018-03-01',	1,				'2018-03-01',	null,			3,			'2018-02-01',		'2018-06-01',		NULL,			@PickupRateGroupId,	@DeliveryRateGroupId);


declare @rateTraid int, @CollectorGroupId int, @ProcessorGroupId int
select @rateTraid = (select ID from RateTransaction where PickupRateGroupId is not null and DeliveryRateGroupId is not null);
select @CollectorGroupId= (select Id from [group] where GroupName = 'CG1');
select @ProcessorGroupId= (select Id from [group] where GroupName = 'PG1');


INSERT [dbo].[RateGroupRate] 
([CollectorGroupId], [ProcessorGroupId], [PaymentType], [Rate],							[ItemType], [EffectiveStartDate],					[EffectiveEndDate],					[RateTransactionId]) 
VALUES 
(@CollectorGroupId,	@ProcessorGroupId,	 1,				CAST(0.069 AS Decimal(18, 3)),	1,			CAST(0x0000A85B00000000 AS DateTime),	CAST(0x002D247F00000000 AS DateTime), @rateTraid),
(@CollectorGroupId,	@ProcessorGroupId,	 1,				CAST(0.081 AS Decimal(18, 3)),	2,			CAST(0x0000A85B00000000 AS DateTime),	CAST(0x002D247F00000000 AS DateTime), @rateTraid),
(@CollectorGroupId,	@ProcessorGroupId,	 2,				CAST(0.024 AS Decimal(18, 3)),	2,			CAST(0x0000A85B00000000 AS DateTime),	CAST(0x002D247F00000000 AS DateTime), @rateTraid),
(NULL,				@ProcessorGroupId,	 3,				CAST(0.135 AS Decimal(18, 3)),	1,			CAST(0x0000A85B00000000 AS DateTime),	CAST(0x002D247F00000000 AS DateTime), @rateTraid),
(NULL,				@ProcessorGroupId,	 3,				CAST(0.163 AS Decimal(18, 3)),	2,			CAST(0x0000A85B00000000 AS DateTime),	CAST(0x002D247F00000000 AS DateTime), @rateTraid);

--SET IDENTITY_INSERT [dbo].[RateGroupRate] OFF
GO
