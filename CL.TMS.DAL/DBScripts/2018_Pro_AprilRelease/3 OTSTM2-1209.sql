﻿USE [tmdb]
GO
	ALTER TABLE RateTransaction DROP CONSTRAINT [FK_RateTransaction_RateGroup_Pickup];
	ALTER TABLE RateTransaction DROP CONSTRAINT [FK_RateTransaction_RateGroup_Delivery];
	ALTER TABLE RateTransaction DROP COLUMN PickupRateGroupId,DeliveryRateGroupId; 
if exists (select * from sysobjects where name='RateGroup' And xtype='U')
begin
	drop table [dbo].[VendorRateGroup]
	drop table [dbo].[RateGroupRate]
	drop table [dbo].[Group]
	drop table [dbo].[RateGroup]
end
go
/****** Object:  Table [dbo].[Group]    Script Date: 2018-04-02 10:12:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](150) NOT NULL,
	[GroupDescription] [nvarchar](150) NOT NULL,
	[GroupType] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_Group_CreatedByUser] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_CreatedByUser]
GO

ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_Group_UpdatedByUser] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_UpdatedByUser]
GO

/****** Object:  Table [dbo].[RateGroup]    Script Date: 2018-04-02 10:12:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RateGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RateGroupName] [nvarchar](150) NOT NULL,
	[Category] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_RateGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RateGroup]  WITH CHECK ADD  CONSTRAINT [FK_RateGroup_CreatedByUser] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[RateGroup] CHECK CONSTRAINT [FK_RateGroup_CreatedByUser]
GO

ALTER TABLE [dbo].[RateGroup]  WITH CHECK ADD  CONSTRAINT [FK_RateGroup_UpdatedByUser] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[RateGroup] CHECK CONSTRAINT [FK_RateGroup_UpdatedByUser]
GO



/****** Object:  Table [dbo].[VendorRateGroup]    Script Date: 2018-04-02 10:53:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VendorRateGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RateGroupId] [int] NULL,
	[GroupId] [int] NOT NULL,
	[VendorId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_VendorGroupRateGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	CONSTRAINT [UNIQUE_Ids] UNIQUE NONCLUSTERED
	(
		[RateGroupId], [GroupId], [VendorId]
	)
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[VendorRateGroup]  WITH CHECK ADD  CONSTRAINT [FK_VendorGroupRateGroup_Vendor] FOREIGN KEY([VendorId])
REFERENCES [dbo].[Vendor] ([ID])
GO

ALTER TABLE [dbo].[VendorRateGroup] CHECK CONSTRAINT [FK_VendorGroupRateGroup_Vendor]
GO

ALTER TABLE [dbo].[VendorRateGroup]  WITH CHECK ADD  CONSTRAINT [FK_VendorGroupRateGroup_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO

ALTER TABLE [dbo].[VendorRateGroup] CHECK CONSTRAINT [FK_VendorGroupRateGroup_Group]
GO

ALTER TABLE [dbo].[VendorRateGroup]  WITH CHECK ADD  CONSTRAINT [FK_VendorGroupRateGroup_RateGroup] FOREIGN KEY([RateGroupId])
REFERENCES [dbo].[RateGroup] ([Id])
GO

ALTER TABLE [dbo].[VendorRateGroup] CHECK CONSTRAINT [FK_VendorGroupRateGroup_RateGroup]
GO

ALTER TABLE [dbo].[VendorRateGroup]  WITH CHECK ADD  CONSTRAINT [FK_VendorGroupRateGroup_CreatedByUser] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[VendorRateGroup] CHECK CONSTRAINT [FK_VendorGroupRateGroup_CreatedByUser]
GO


/****** Object:  Table [dbo].[RateGroupRate]    Script Date: 2018-04-02 10:13:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RateGroupRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CollectorGroupId] [int] NULL,
	[ProcessorGroupId] [int] NULL,
	[PaymentType] [int] NOT NULL,
	[Rate] [decimal](18, 3) NOT NULL,
	[ItemType] [int] NOT NULL,
	[EffectiveStartDate] [datetime] NOT NULL,
	[EffectiveEndDate] [datetime] NOT NULL,
	[RateTransactionId] [int] NOT NULL,
 CONSTRAINT [PK_GroupRate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RateGroupRate]  WITH CHECK ADD  CONSTRAINT [FK_GroupRate_RateTransaction] FOREIGN KEY([RateTransactionId])
REFERENCES [dbo].[RateTransaction] ([ID])
GO

ALTER TABLE [dbo].[RateGroupRate] CHECK CONSTRAINT [FK_GroupRate_RateTransaction]
GO

ALTER TABLE [dbo].[RateGroupRate] WITH CHECK ADD  CONSTRAINT [FK_RateGroupRate_Group_Collector] FOREIGN KEY([CollectorGroupId])
REFERENCES [dbo].[Group] ([Id])
ALTER TABLE [dbo].[RateGroupRate] WITH CHECK ADD  CONSTRAINT [FK_RateGroupRate_Group_Processor] FOREIGN KEY([ProcessorGroupId])
REFERENCES [dbo].[Group] ([Id])
GO
IF COL_LENGTH('RateTransaction', 'PickupRateGroupId') IS NULL
BEGIN
	ALTER TABLE [RateTransaction] add [PickupRateGroupId] int null;
	ALTER TABLE [RateTransaction] add [DeliveryRateGroupId] int null;

	ALTER TABLE [dbo].[RateTransaction] WITH CHECK ADD  CONSTRAINT [FK_RateTransaction_RateGroup_Pickup] FOREIGN KEY([PickupRateGroupId])
	REFERENCES [dbo].[RateGroup] ([Id])
	ALTER TABLE [dbo].[RateTransaction] WITH CHECK ADD  CONSTRAINT [FK_RateTransaction_RateGroup_Delivery] FOREIGN KEY([DeliveryRateGroupId])
	REFERENCES [dbo].[RateGroup] ([Id])

END
go