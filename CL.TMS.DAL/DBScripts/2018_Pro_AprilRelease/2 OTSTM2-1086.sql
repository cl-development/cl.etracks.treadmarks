﻿USE [tmdb]
GO

if exists (select * from sysobjects where name='VendorRate' And xtype='U')
begin
	drop table [dbo].[VendorRate]
end
go

/****** Object:  Table [dbo].[VendorRate]    Script Date: 2018-03-22 7:37:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if not exists (select * from sysobjects where name='VendorRate' And xtype='U')
begin
	CREATE TABLE [dbo].[VendorRate](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[RateTransactionId] [int] NOT NULL,
		[VendorId] [int] NOT NULL,
	 CONSTRAINT [PK_VendorRate] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[VendorRate]  WITH CHECK ADD  CONSTRAINT [FK_VendorRate_Vendor] FOREIGN KEY([VendorId])
	REFERENCES [dbo].[Vendor] ([ID])

	ALTER TABLE [dbo].[VendorRate]  WITH CHECK ADD  CONSTRAINT [FK_VendorRate_RateTransaction] FOREIGN KEY([RateTransactionId])
	REFERENCES [dbo].[RateTransaction] ([Id])
end
GO

IF COL_LENGTH('Rate', 'IsSpecificRate') IS NULL alter table [Rate] add IsSpecificRate bit null default(0);
GO
IF COL_LENGTH('RateTransaction', 'IsSpecificRate') IS NULL alter table [RateTransaction] add IsSpecificRate bit null default(0);
GO

--Manufacturing Incentives Rates display order update
update TypeDefinition set DisplayOrder=7 WHERE Category='RateCategory' and DefinitionValue=5
