﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

--user access permission control for Admin/Rate Groups --display order:24xxxx
if NOT EXISTS (select * from AppResource where Name = 'Admin/Rate Groups' and ResourceMenu = 'Admin/Rate Groups ' and ResourceScreen = 'Admin/Rate Groups')
BEGIN
insert into [AppResource]	
	(Name,						ResourceMenu,			ResourceScreen,			ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Rate Groups',	'Admin/Rate Groups',		'Admin/Rate Groups',	0,				240000,			@CreateBy,	@dt,		@dt)
	--('Pickup Groups',		'Admin/Rate Groups',		'Pickup Groups',		1,				241000,			@CreateBy,	@dt,		@dt),
	--('Delivery Groups',		'Admin/Rate Groups',		'Delivery Groups',		1,				242000,			@CreateBy,	@dt,		@dt)	
END
go

--create permission for new appResource new Admin menu items

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int, @iEditSave int
Declare @getNewSourceIDs cursor
DECLARE @getRoleIds CURSOR
set @superAdminID=(select ID from role where name='Super Admin')
SET @getRoleIds = CURSOR FOR select ID from [Role]
set @getNewSourceIDs=cursor for select id from appresource where ResourceMenu ='Admin/Rate Groups'
select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	OPEN @getNewSourceIDs FETCH NEXT FROM @getNewSourceIDs INTO @newResourceID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
		BEGIN
			if (@RoleId = @superAdminID) 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
					--select  @RoleId, @newResourceID,  @iCounter[counter],'admin' 
				end 
			else 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iNoAccess)
					--select  @RoleId, @newResourceID,  @iCounter[counter] 
				end
		END
		FETCH NEXT
		FROM @getNewSourceIDs INTO @newResourceID
		set @iCounter = @iCounter + 1;
	END
	CLOSE @getNewSourceIDs
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getNewSourceIDs
DEALLOCATE @getRoleIds
go