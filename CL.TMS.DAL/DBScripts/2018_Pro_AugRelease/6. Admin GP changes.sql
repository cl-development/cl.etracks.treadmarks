﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


USE [tmdb]
GO

IF OBJECT_ID('dbo.[UQ_FIAccount_CID_Number_Label]') IS NULL 
begin
	ALTER TABLE dbo.FIAccount ADD CONSTRAINT UQ_FIAccount_CID_Number_Label UNIQUE (AdminFICategoryID, AccountNumber, AccountLabel); 
end


DECLARE @maxDisplayOrder INT;
set @maxDisplayOrder = (select max(DisplayOrder) from FIAccount where AdminFICategoryID = 4) 

-- insert FIAccount initial data
IF NOT EXISTS (select * from FIAccount where AccountNumber in ( '4247-10-40-40', '4249-20-40-40', '4252-30-40-40', '4254-40-40-40', '4257-50-40-40', '4259-60-40-40', '4262-70-40-40', '4264-80-40-40', '4359-90-40-40') and AdminFICategoryID = 4)
BEGIN
	INSERT INTO FIAccount 
		([AdminFICategoryID],	[AccountLabel],					[AccountNumber],	[DisplayOrder],			[AccountDescription],		[MappingNote],					[distribution_type_id], [Name])
		VALUES 
        (4,						'PLT',							'4247-10-40-40',	@maxDisplayOrder + 1,	'PI- PLT HST',				'HST On PI for PLT tires',		10,						'PLT HST'),
        (4,						'MT',							'4249-20-40-40',	@maxDisplayOrder + 2,	'PI- MT HST',				'HST On PI for MT tires',		10,						'MT HST'),
        (4,						'AG/LS',						'4252-30-40-40',	@maxDisplayOrder + 3,	'PI- AG/LS HST',			'HST On PI for AG/LS tires',	10,						'AG/LS HST'),
        (4,						'IND',							'4254-40-40-40',	@maxDisplayOrder + 4,	'PI- IND HST',				'HST On PI for IND tires',		10,						'IND HST'),
        (4,						'SOTR',							'4257-50-40-40',	@maxDisplayOrder + 5,	'PI- SOTR HST',				'HST On PI for SOTR tires',		10,						'SOTR HST'),
        (4,						'MOTR',							'4259-60-40-40',	@maxDisplayOrder + 6,	'PI- MOTR HST',				'HST On PI for MOTR tires',		10,						'MOTR HST'),
        (4,						'LOTR',							'4262-70-40-40',	@maxDisplayOrder + 7,	'PI- LOTR HST',				'HST On PI for LOTR tires',		10,						'LOTR HST'),
        (4,						'GOTR',							'4264-80-40-40',	@maxDisplayOrder + 8,	'PI- GOTR HST',				'HST On PI for GOTR tires',		10,						'GOTR HST'),
        (4,						'Internal Payment Adjustments',	'4359-90-40-40',	@maxDisplayOrder + 9,	'PI- TM Adjustment HST',	'+ve adjustment: tax amount output as Debit. -ve adjustment: abs(tax amount) output as Credit',	10,	'Internal Payment Adjustments HST')
End
go

DECLARE @maxDisplayOrder INT;
set @maxDisplayOrder = (select max(DisplayOrder) from FIAccount where AdminFICategoryID = 5) 

IF NOT EXISTS (select * from FIAccount where AccountNumber in ( '4202-10-50-40', '4204-20-50-40', '4207-30-50-40', '4209-40-50-40', '4212-50-50-40', '4214-60-50-40', '4217-70-50-40', '4219-80-50-40', '4221-90-50-40') and AdminFICategoryID = 5)
BEGIN
	INSERT INTO FIAccount 
		([AdminFICategoryID],	[AccountLabel],					[AccountNumber],	[DisplayOrder],			[AccountDescription],		[MappingNote],					[distribution_type_id], [Name])
		VALUES 
        (5,						'PLT',							'4202-10-50-40',	@maxDisplayOrder + 1,	'MI- PLT HST',				'HST On MI for PLT tires',		10,						'PLT HST'),
        (5,						'MT',							'4204-20-50-40',	@maxDisplayOrder + 2,	'MI- MT HST',				'HST On MI for MT tires',		10,						'MT HST'),
        (5,						'AG/LS',						'4207-30-50-40',	@maxDisplayOrder + 3,	'MI- AG/LS HST',			'HST On MI for AG/LS tires',	10,						'AG/LS HST'),
        (5,						'IND',							'4209-40-50-40',	@maxDisplayOrder + 4,	'MI- IND HST',				'HST On MI for IND tires',		10,						'IND HST'),
        (5,						'SOTR',							'4212-50-50-40',	@maxDisplayOrder + 5,	'MI- SOTR HST',				'HST On MI for SOTR tires',		10,						'SOTR HST'),
        (5,						'MOTR',							'4214-60-50-40',	@maxDisplayOrder + 6,	'MI- MOTR HST',				'HST On MI for MOTR tires',		10,						'MOTR HST'),
        (5,						'LOTR',							'4217-70-50-40',	@maxDisplayOrder + 7,	'MI- LOTR HST',				'HST On MI for LOTR tires',		10,						'LOTR HST'),
        (5,						'GOTR',							'4219-80-50-40',	@maxDisplayOrder + 8,	'MI- GOTR HST',				'HST On MI for GOTR tires',		10,						'GOTR HST'),
        (5,						'Internal Payment Adjustments',	'4221-90-50-40',	@maxDisplayOrder + 9,	'MI- TM Adjustment HST',	'+ve adjustment: tax amount output as Debit. -ve adjustment: abs(tax amount) output as Credit',	10,	'Internal Payment Adjustments HST')
End
go