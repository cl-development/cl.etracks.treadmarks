USE [tmdb]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VendorServiceThreshold](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VendorID] [int] not null,
	[ThresholdDays] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[IsDeleted] [Bit]  NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_VendorServiceThreshold] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[VendorServiceThreshold]  WITH CHECK ADD  CONSTRAINT [FK_VendorServiceThreshold_CreatedByUser] FOREIGN KEY([CreatedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[VendorServiceThreshold] CHECK CONSTRAINT [FK_VendorServiceThreshold_CreatedByUser]
GO

ALTER TABLE [dbo].[VendorServiceThreshold]  WITH CHECK ADD  CONSTRAINT [FK_VendorServiceThreshold_ModifiedUser] FOREIGN KEY([ModifiedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[VendorServiceThreshold] CHECK CONSTRAINT [FK_VendorServiceThreshold_ModifiedUser]
GO

ALTER TABLE [dbo].[VendorServiceThreshold]  WITH CHECK ADD  CONSTRAINT [FK_VendorServiceThreshold_Vendor] FOREIGN KEY([VendorID])
REFERENCES [dbo].[Vendor] ([ID])
GO

ALTER TABLE [dbo].[VendorServiceThreshold] CHECK CONSTRAINT [FK_VendorServiceThreshold_Vendor]
GO

