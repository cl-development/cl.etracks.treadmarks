USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllServiceThresholds]    Script Date: 5/1/2018 10:00:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_GetAllServiceThresholds]
as 
begin
select	
	vt.ID,
	vt.VendorID, 
	v.Number RegNumber,
	v.BusinessName, 
	vt.[Description], 
	vt.CreatedDate,
	vt.ThresholdDays,
	vt.ModifiedDate,
	DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate())) DaysSinceLastTCR,
	case when (vt.ThresholdDays - DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate()))) > 0
	then (vt.ThresholdDays - DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate())))
	else 0 end DaysLeft,
	convert(date, max(t.TransactionDate)) LastTCRDate,
	vt.IsDeleted
from VendorServiceThreshold vt 
	inner join Vendor v on v.ID=vt.VendorID
	left  join Claim c  on c.ParticipantID=v.ID
	left  join ClaimDetail cd on c.ID=cd.ClaimId
	left join [Transaction] t on (t.ID=cd.TransactionId) and (t.TransactionTypeCode='TCR')
where vt.IsDeleted is null or vt.IsDeleted=0
group by
	vt.ID, 
	vt.VendorID,
	v.Number, 
	v.BusinessName, 
	vt.[Description], 
	vt.ThresholdDays,
	vt.CreatedDate,
	vt.ModifiedDate,
	vt.IsDeleted
end