create table ApplicationSupportingDocument
(
  ID int identity,
  ApplicationID int not null,
  TypeID int not null,
  [Option] nvarchar(16) null,
  RowVersion timestamp null
  constraint pk_ApplicationSupportingDocument primary key(ID),
  constraint fk_Application_SupportingDocuments foreign key(ApplicationID) references Application(ID)
)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('Master Business Licence', 'MBL', 'Master Business Licence Document', 'SupportingDocument', 1)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('Commercial Liability Insurance', 'CLI', 'Commercial Liability Insurance Document', 'SupportingDocument', 2)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('Certificate of Approval for Processing', 'CFP', 'Certificate of Approval for Processing Document', 'SupportingDocument', 3)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('HST Certificate', 'HST', 'HST Certificate', 'SupportingDocument', 4)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('WSIB Certificate', 'WSIB', 'WSIB Certificate', 'SupportingDocument', 5)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('Void Cheque', 'CHQ', 'Void Cheque', 'SupportingDocument', 6)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('CVOR Abstract Level II', 'CVOR', 'CVOR Abstract Level II', 'SupportingDocument', 7)

insert into TypeDefinition
(Name, Code, Description, Category, DefinitionValue)
VALUES('Processor Relationship Letter', 'PRL', 'Processor Relationship Letter', 'SupportingDocument', 8)