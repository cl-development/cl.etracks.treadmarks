﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

--remove "Audit Toggle" from existing RolePermission Table
Declare @existingResourceID int, @iCounter int, @RoleId int
DECLARE @getRoleIds CURSOR
SET @getRoleIds = CURSOR FOR select ID from [Role]
select @existingResourceID = Id from AppResource where Name = 'Audit Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
    IF EXISTS (SELECT rp.Id FROM [RolePermission] rp WHERE rp.AppResourceId = @existingResourceID and rp.RoleId = @RoleId)
	BEGIN
		delete from [RolePermission] where AppResourceId = @existingResourceID and RoleId = @RoleId;
		--select  @RoleId, @existingResourceID,  @iCounter[counter];
		set @iCounter = @iCounter + 1;
	END
 FETCH NEXT
 FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go


--remove existing appResource, "Audit Toggle"
IF EXISTS (select * from AppResource where Name = 'Audit Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application')
BEGIN

	delete from AppResource where Name = 'Audit Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application';

	update [AppResource] set DisplayOrder = 131112 where Name = 'MOE Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131113 where Name = 'Submission Information' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131114 where Name = 'Internal Notes' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131115 where Name = 'Business Location' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131116 where Name = 'Contact Information' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131117 where Name = 'Tire Details' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131118 where Name = 'Steward Details' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131119 where Name = 'Supporting Documents' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131120 where Name = 'Terms And Conditions' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'
	update [AppResource] set DisplayOrder = 131121 where Name = 'Re-send Welcome Letter' and ResourceMenu = 'Applications' and ResourceScreen = 'Steward Application'

END
go

--remove 'Audit' column from 'Customer' Table
IF COL_LENGTH('[Customer]', 'Audit') IS NOT NULL
BEGIN
    ALTER TABLE [Customer]
    DROP COLUMN [Audit]
END
go

--remove 'AuditSwitchDate' column from 'Customer' Table
IF COL_LENGTH('[Customer]', 'AuditSwitchDate') IS NOT NULL
BEGIN
    ALTER TABLE [Customer]
    DROP COLUMN [AuditSwitchDate]
END
go