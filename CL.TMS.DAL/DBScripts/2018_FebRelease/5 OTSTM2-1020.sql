﻿use tmdb

IF EXISTS (select * from TypeDefinition where Code = 'OEM' and Category = 'StewardType' and DefinitionValue = 1)
BEGIN
	update TypeDefinition set Description = 'Original Equipment Manufacturer (OEM)', Name = 'Original Equipment Manufacturer (OEM)' where Code = 'OEM' and Category = 'StewardType' and DefinitionValue = 1
END

IF EXISTS (select * from TypeDefinition where Code = 'Owner' and Category = 'StewardType' and DefinitionValue = 2)
BEGIN
	update TypeDefinition set Description = 'Tire Manufacturer/Brand Owner', Name = 'Tire Manufacturer/Brand Owner' where Code = 'Owner' and Category = 'StewardType' and DefinitionValue = 2
END