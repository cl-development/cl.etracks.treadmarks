﻿IF OBJECT_ID('dbo.[UQ_Name_Menu_Screen]') IS NULL 
begin
	ALTER TABLE dbo.AppResource ADD CONSTRAINT UQ_Name_Menu_Screen UNIQUE (Name,ResourceMenu,ResourceScreen); 
end

IF OBJECT_ID('dbo.[UQ_UserClaimsWorkflow_UId_AccountName]') IS NULL 
begin
	ALTER TABLE dbo.UserClaimsWorkflow ADD CONSTRAINT UQ_UserClaimsWorkflow_UId_AccountName UNIQUE (UserId,AccountName); 
end
