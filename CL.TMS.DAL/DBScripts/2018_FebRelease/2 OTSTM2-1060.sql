﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1042 for add weight categories to TypeDefinition
Purpose     :   add/update weight categories to TypeDefinition			
Created By  :   Frank Jan 10th, 2018
*****************************************************************/

USE [tmdb]
GO

IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='WeightCategory' and Name='Supply Estimated Weights')--if first line not exists, then insert whole batch
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,								[Code] ,							[Description] ,								[Category] ,[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Supply Estimated Weights',			'SupplyEstimatedWeights',			'Supply Estimated Weights',					'WeightCategory',			1 ,				1),
		('Recovery Estimated Weights',			'RecoveryEstimatedWeights',			'Recovery Estimated Weights',				'WeightCategory',			2 ,				2)
END
GO

--Create Table WeightTransaction
/****** Object:  Table [dbo].[WeightTransaction]    Script Date: 1/3/2018 2:42:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[WeightTransaction]',N'U') IS NULL
BEGIN

	CREATE TABLE [dbo].[WeightTransaction](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[CreatedByID] [bigint] NOT NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedByID] [bigint] NULL,
		[Category] [int] NOT NULL,
		[EffectiveStartDate] [datetime] NOT NULL,
		[EffectiveEndDate] [datetime] NOT NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_WeightTransaction] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[WeightTransaction]  WITH CHECK ADD  CONSTRAINT [FK_WeightTransaction_CreatedByUser] FOREIGN KEY([CreatedByID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[WeightTransaction] CHECK CONSTRAINT [FK_WeightTransaction_CreatedByUser]

	ALTER TABLE [dbo].[WeightTransaction]  WITH CHECK ADD  CONSTRAINT [FK_WeightTransaction_ModifiedUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[WeightTransaction] CHECK CONSTRAINT [FK_WeightTransaction_ModifiedUser]

END

GO

--Create Table WeightTransactionNote
/****** Object:  Table [dbo].[WeightTransactionNote]    Script Date: 1/3/2018 2:45:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF OBJECT_ID(N'[dbo].[WeightTransactionNote]',N'U') IS NULL
BEGIN

	CREATE TABLE [dbo].[WeightTransactionNote](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[WeightTransactionID] [int] NOT NULL,
		[Note] [varchar](max) NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserID] [bigint] NOT NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_WeightNote] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[WeightTransactionNote]  WITH CHECK ADD  CONSTRAINT [FK_WeightTransactionNote_WeightTransaction] FOREIGN KEY([WeightTransactionID])
	REFERENCES [dbo].[WeightTransaction] ([ID])

	ALTER TABLE [dbo].[WeightTransactionNote] CHECK CONSTRAINT [FK_WeightTransactionNote_WeightTransaction]

	ALTER TABLE [dbo].[WeightTransactionNote]  WITH CHECK ADD  CONSTRAINT [FK_WeightTransactionNote_User] FOREIGN KEY([UserID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[WeightTransactionNote] CHECK CONSTRAINT [FK_WeightTransactionNote_User]

END

go

--Add 2 new columns into Item TABLE
IF COL_LENGTH('[Item]', 'AvailableRangeMin') IS NULL
BEGIN
    ALTER TABLE [Item]
    ADD [AvailableRangeMin] decimal(18,4) null DEFAULT null;    
END
go

IF COL_LENGTH('[Item]', 'AvailableRangeMax') IS NULL
BEGIN
    ALTER TABLE [Item]
    ADD [AvailableRangeMax] decimal(18,4) null DEFAULT null;    
END
go

--Add 2 new columns into ItemWeight Table
IF COL_LENGTH('[ItemWeight]', 'SetRangeMin') IS NULL
BEGIN
    ALTER TABLE [ItemWeight]
    ADD [SetRangeMin] decimal(18,4) null DEFAULT null;    
END
go

IF COL_LENGTH('[ItemWeight]', 'SetRangeMax') IS NULL
BEGIN
    ALTER TABLE [ItemWeight]
    ADD [SetRangeMax] decimal(18,4) null DEFAULT null;    
END
go

--Add 1 new column "WeightTransactionID" into ItemWeight Table
IF COL_LENGTH('[ItemWeight]', 'WeightTransactionID') IS NULL
BEGIN
    ALTER TABLE [ItemWeight]
    ADD [WeightTransactionID] int null DEFAULT null;
    ALTER TABLE [ItemWeight] WITH CHECK ADD  CONSTRAINT [FK_ItemWeight_WeightTransaction] FOREIGN KEY([WeightTransactionID])
	REFERENCES [dbo].[WeightTransaction] ([ID])
END
go

SET IDENTITY_INSERT weighttransaction ON 

insert into weighttransaction (ID, CreatedDate,CreatedByID, ModifiedDate,ModifiedByID,Category, EffectiveStartDate,EffectiveEndDate)
select ID, CreatedDate,CreatedByID, ModifiedDate,ModifiedByID, 2, EffectiveStartDate,EffectiveEndDate from RateTransaction  where Category=6;

SET IDENTITY_INSERT weighttransaction Off


Insert into weightTransactionNote (WeightTransactionID, Note, CreatedDate, UserID)
select RateTransactionID, Note, CreatedDate, UserID from RateTransactionNote where RateTransactionID in
(select ID from RateTransaction  where Category=6);


--make WeightTransactionID equal to RateTransactionID if there are records with RateTransactionID
IF EXISTS (SELECT * FROM [ItemWeight] WHERE RateTransactionID IS NOT NULL)
BEGIN
	UPDATE [ItemWeight] SET WeightTransactionID = RateTransactionID 
END
go

--remove 'RateTransactionID' column from 'ItemWeight' Table
ALTER TABLE [ItemWeight]
DROP CONSTRAINT FK_ItemWeight_RateTransaction;
ALTER TABLE [ItemWeight]
DROP CONSTRAINT DF__ItemWeigh__RateT__318D45CA;  --please note this Constraint is typically created automatically by the DBMS (SQL Server), so the name may be different in different DB, such as QA/UAT/Prod DB

IF COL_LENGTH('[ItemWeight]', 'RateTransactionID') IS NOT NULL
BEGIN
    ALTER TABLE [ItemWeight]
    DROP COLUMN [RateTransactionID]
END
go

--Insert default values into ItemWeight TABLE
Declare @ItemID int, @ItemShortName varchar(5), @iCounter int
DECLARE @getItemIds CURSOR
SET @getItemIds = CURSOR FOR select ID, ShortName from [Item] where ShortName like 'C%' and ShortName <> 'Calendared' and ShortName <> 'Calendered'

set @iCounter = 1

OPEN @getItemIds FETCH NEXT FROM @getItemIds INTO @ItemId, @ItemShortName
WHILE @@FETCH_STATUS = 0
BEGIN
    IF NOT EXISTS (SELECT Id FROM [ItemWeight] WHERE ItemId = @ItemId)
	BEGIN		
		Insert into [ItemWeight] (ItemID, StandardWeight, EffectiveStartDate, EffectiveEndDate, CreatedBy, CreatedDate, WeightTransactionID, SetRangeMin, SetRangeMax)
								values (@ItemId, 0, '20090101', '99991231', 'System',  CURRENT_TIMESTAMP, null,
								case @ItemShortName 
									when 'C1' then 10
									when 'C2' then 10
									when 'C3' then 1
									when 'C4' then 16
									when 'C5' then 31
									when 'C6' then 71
									when 'C7' then 121
									when 'C8' then 251
									when 'C9' then 376
									when 'C10' then 701
									when 'C11' then 1201
									when 'C12' then 1
									when 'C13' then 31
									when 'C14' then 61
									when 'C15' then 251
									when 'C16' then 376
									when 'C17' then 701
									when 'C18' then 1201
								end,
								case @ItemShortName 
									when 'C1' then 10
									when 'C2' then 50
									when 'C3' then 15
									when 'C4' then 30
									when 'C5' then 70
									when 'C6' then 250
									when 'C7' then 250
									when 'C8' then 9999
									when 'C9' then 700
									when 'C10' then 1200
									when 'C11' then 9999
									when 'C12' then 30
									when 'C13' then 60
									when 'C14' then 250
									when 'C15' then 375
									when 'C16' then 700
									when 'C17' then 1200
									when 'C18' then 9999
								end)
		--select  @ItemId, @ItemShortName,  @iCounter[counter];
		set @iCounter = @iCounter + 1;
	END
 FETCH NEXT
 FROM @getItemIds INTO @ItemId, @ItemShortName
END

CLOSE @getItemIds

DEALLOCATE @getItemIds
go

--update Item TABLE
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 100 where ShortName = 'C1'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 100 where ShortName = 'C2'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 100 where ShortName = 'C3'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 100 where ShortName = 'C4'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 100 where ShortName = 'C5'
update [Item] set AvailableRangeMin = 20, AvailableRangeMax = 300 where ShortName = 'C6'
update [Item] set AvailableRangeMin = 70, AvailableRangeMax = 300 where ShortName = 'C7'
update [Item] set AvailableRangeMin = 200, AvailableRangeMax = 9999 where ShortName = 'C8'
update [Item] set AvailableRangeMin = 330, AvailableRangeMax = 750 where ShortName = 'C9'
update [Item] set AvailableRangeMin = 650, AvailableRangeMax = 1250 where ShortName = 'C10'
update [Item] set AvailableRangeMin = 1150, AvailableRangeMax = 9999 where ShortName = 'C11'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 80 where ShortName = 'C12'
update [Item] set AvailableRangeMin = 1, AvailableRangeMax = 110 where ShortName = 'C13'
update [Item] set AvailableRangeMin = 10, AvailableRangeMax = 300 where ShortName = 'C14'
update [Item] set AvailableRangeMin = 200, AvailableRangeMax = 430 where ShortName = 'C15'
update [Item] set AvailableRangeMin = 330, AvailableRangeMax = 750 where ShortName = 'C16'
update [Item] set AvailableRangeMin = 650, AvailableRangeMax = 1250 where ShortName = 'C17'
update [Item] set AvailableRangeMin = 1150, AvailableRangeMax = 9999 where ShortName = 'C18'


--remove "Estimated Weights" from existing RolePermission Table
Declare @existingResourceID int, @iCounter int, @RoleId int
DECLARE @getRoleIds CURSOR
SET @getRoleIds = CURSOR FOR select ID from [Role]
select @existingResourceID = Id from AppResource where Name = 'Estimated Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
    IF EXISTS (SELECT rp.Id FROM [RolePermission] rp WHERE rp.AppResourceId = @existingResourceID and rp.RoleId = @RoleId)
	BEGIN
		delete from [RolePermission] where AppResourceId = @existingResourceID and RoleId = @RoleId;
		--select  @RoleId, @existingResourceID,  @iCounter[counter];
		set @iCounter = @iCounter + 1;
	END
 FETCH NEXT
 FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go


--remove existing appResource, "Estimated Weights"
IF EXISTS (select * from AppResource where Name = 'Estimated Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	delete from AppResource where Name = 'Estimated Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

--rename "Rates and Weights" to "Fees and Incentives" in AppResource
IF EXISTS (select * from AppResource where Name = 'Admin/Rates And Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Admin/Rates And Weights')
BEGIN
	update [AppResource] set Name = 'Admin/Fees and Incentives', ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Admin/Fees and Incentives' where Name = 'Admin/Rates And Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Admin/Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Rates And Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set Name = 'Fees and Incentives', ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Rates And Weights' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Tire Stewardship Fees' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Tire Stewardship Fees' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Remittance Penalty' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Remittance Penalty' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Collection Allowances' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Collection Allowances' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Transportation Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Transportation Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Processing Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Processing Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

IF EXISTS (select * from AppResource where Name = 'Manufacturing Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights')
BEGIN
	update [AppResource] set ResourceMenu = 'Admin/Fees and Incentives', ResourceScreen = 'Fees and Incentives' where Name = 'Manufacturing Incentives' and ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Rates And Weights';
END
go

--Delete Type "Estimated Weights Rates" from TypeDefinition Table
IF EXISTS (SELECT * FROM TypeDefinition WHERE Category='RateCategory' and Name='Estimated Weights Rates')
BEGIN
	delete from [TypeDefinition] where Category='RateCategory' and Name='Estimated Weights Rates';
END
go