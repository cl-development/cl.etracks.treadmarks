﻿IF NOT EXISTS (SELECT * FROM AppSetting WHERE [Key] = 'Threshold.HaulerApprover1ReqdAmount') --use first entry as flag
BEGIN
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBCollectorNegativeClaimReqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CollectorNegativeClaimApprover', 'Approver1');

	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBHaulerApprover1Reqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.HaulerApprover1ReqdAmount', '0');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBHaulerNegativeClaimReqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.HaulerNegativeClaimApprover', 'Approver1');

	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBProcessorApprover1Reqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.ProcessorApprover1ReqdAmount', '0');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBProcessorNegativeClaimReqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.ProcessorNegativeClaimApprover', 'Approver1');

	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBRPMApprover1Reqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.RPMApprover1ReqdAmount', '0');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBRPMNegativeClaimReqd', '1');
	INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.RPMNegativeClaimApprover', 'Approver1');
END
go