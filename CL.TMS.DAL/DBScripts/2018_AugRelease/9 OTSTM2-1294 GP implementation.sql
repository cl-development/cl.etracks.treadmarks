﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


USE [tmdb]
GO

DECLARE @maxId INT;
set @maxId = (select max(id) from GpiAccount)

-- insert GpiAccount initial data
IF NOT EXISTS (select * from GpiAccount where account_number in ( '4382-10-30-40', '4384-20-30-40', '4387-30-30-40', '4389-40-30-40', '4398-50-30-40', '4401-60-30-40', '4403-70-30-40', '4406-80-30-40', '4247-10-40-40', '4249-20-40-40', '4252-30-40-40', '4254-40-40-40', '4257-50-40-40', '4259-60-40-40', '4262-70-40-40', '4264-80-40-40', '4359-90-40-40') and registrant_type_ind = 'P')
BEGIN
	INSERT INTO GpiAccount 
		([id],				[account_number],		[description],				[distribution_type_id], [tire_type_id], [registrant_type_ind], [ShortName])
		VALUES 
		(@maxId + 1,		'4382-10-30-40',		'TI- PLT HST',				10,						1,				'P',					'PLT'),
		(@maxId + 2,		'4384-20-30-40',		'TI- MT HST',				10,						2,				'P', 					'MT'),
		(@maxId + 3,		'4387-30-30-40',		'TI- AG/LS HST',			10,						3,				'P',					'AGLS'),
		(@maxId + 4,		'4389-40-30-40',		'TI- IND HST',				10,						4,				'P',					'IND'),
		(@maxId + 5,		'4398-50-30-40',		'TI- SOTR HST',				10,						5,				'P',					'SOTR'),
		(@maxId + 6,		'4401-60-30-40',		'TI- MOTR HST',				10,						6,				'P',					'MOTR'),
		(@maxId + 7,		'4403-70-30-40',		'TI- LOTR HST',				10,						7,				'P',					'LOTR'),
		(@maxId + 8,		'4406-80-30-40',		'TI- GOTR HST',				10,						8,				'P',					'GOTR'),
        (@maxId + 9,		'4247-10-40-40',		'PI- PLT HST',				10,						1,				'P',					'PLT'),
		(@maxId + 10,		'4249-20-40-40',		'PI- MT HST',				10,						2,				'P',					'MT'),
		(@maxId + 11,		'4252-30-40-40',		'PI- AG/LS HST',			10,						3,				'P',					'AGLS'),
		(@maxId + 12,		'4254-40-40-40',		'PI- IND HST',				10,						4,				'P',					'IND'),
		(@maxId + 13,		'4257-50-40-40',		'PI- SOTR HST',				10,						5,				'P',					'SOTR'),
		(@maxId + 14,		'4259-60-40-40',		'PI- MOTR HST',				10,						6,				'P',					'MOTR'),
		(@maxId + 15,		'4262-70-40-40',		'PI- LOTR HST',				10,						7,				'P',					'LOTR'),
		(@maxId + 16,		'4264-80-40-40',		'PI- GOTR HST',				10,						8,				'P',					'GOTR'),
		(@maxId + 17,		'4359-90-40-40',		'PI- TM Adjustment HST',	10,						NULL,			'P',					NULL)
        
End
go