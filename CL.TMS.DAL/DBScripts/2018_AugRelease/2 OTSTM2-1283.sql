﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_AddClaimForVendors]    Script Date: 7/10/18 9:47:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Update : Set IsTaxApplicable=true as default OTSTM2-1283
--        : July 10, 2018
-- =============================================
ALTER PROCEDURE [dbo].[sp_AddClaimForVendors]
	@periodDate as datetime
AS
BEGIN
	SET NOCOUNT ON;
	declare @vendorId as int;
	declare @addressId as int;
    declare @vendorType as int;

	declare @collectorPeriodId as int;
	set @collectorPeriodId=(select top 1 id from period a where a.StartDate<=@periodDate and a.EndDate>=@periodDate and a.PeriodType=2);

	declare @haulerPeriodId as int;
	set @haulerPeriodId=(select top 1 id from period a where a.StartDate<=@periodDate and a.EndDate>=@periodDate and a.PeriodType=3);

	declare @processorPeriodId as int;
	set @processorPeriodId=(select top 1 id from period a where a.StartDate<=@periodDate and a.EndDate>=@periodDate and a.PeriodType=4);

	declare @rpmPeriodId as int;
	set @rpmPeriodId=(select top 1 id from period a where a.StartDate<=@periodDate and a.EndDate>=@periodDate and a.PeriodType=5);

	declare @vendorCursor as Cursor;
	set @vendorCursor=cursor for
	select a.VendorType, a.ID as vendorId, c.id as addressid from vendor a, VendorAddress b, address c where a.ID=b.VendorID and b.AddressID=c.ID and a.IsActive=1 and c.AddressType=1;

	open @vendorCursor;
	fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		if NOT EXISTS(select top 1 * from claim a, Period b where a.ClaimPeriodID=b.ID and b.StartDate<=@periodDate and b.EndDate>=@periodDate and a.ParticipantID=@vendorId)
		begin
		   if (@vendorType=2)
				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType, IsTaxApplicable) values(@collectorPeriodId, @vendorId,@addressId,'Open', @vendorType,1);
			if (@vendorType=3)
				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@haulerPeriodId, @vendorId,@addressId,'Open', @vendorType,1);
			if (@vendorType=4)
				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@processorPeriodId, @vendorId,@addressId,'Open', @vendorType,1);
			if (@vendorType=5)
				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@rpmPeriodId, @vendorId,@addressId,'Open', @vendorType,1);
		end
		fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;
	END
 	CLOSE @vendorCursor;
	DEALLOCATE @vendorCursor;	
END
