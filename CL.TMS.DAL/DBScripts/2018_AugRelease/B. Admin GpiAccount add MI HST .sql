﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


USE [tmdb]
GO

DECLARE @maxID INT;
set @maxID = (select max(id) from GpiAccount) 

-- insert GpiAccount initial data
IF NOT EXISTS (select * from GpiAccount where account_number in (
 '4202-10-50-40', 
 '4204-20-50-40', 
 '4207-30-50-40', 
 '4209-40-50-40', 
 '4212-50-50-40', 
 '4214-60-50-40', 
 '4217-70-50-40', 
 '4219-80-50-40', 
 '4221-90-50-40') and registrant_type_ind = 'M')
BEGIN
	INSERT INTO GpiAccount 
		([id],		[account_number],	[description],				[distribution_type_id],[tire_type_id],[registrant_type_ind])
		VALUES 
        (@maxID+1,	'4202-10-50-40',	'MI-PLT HST',				10,						1,				'M'),
        (@maxID+2,	'4204-20-50-40',	'MI-MT HST',				10,						2,				'M'),
        (@maxID+3,	'4207-30-50-40',	'MI-AG/LS HST',				10,						3,				'M'),
        (@maxID+4,	'4209-40-50-40',	'MI-IND HST',				10,						4,				'M'),
        (@maxID+5,	'4212-50-50-40',	'MI-SOTR HST',				10,						5,				'M'),
        (@maxID+6,	'4214-60-50-40',	'MI-MOTR HST',				10,						6,				'M'),
        (@maxID+7,	'4217-70-50-40',	'MI-LOTR HST',				10,						7,				'M'),
        (@maxID+8,	'4219-80-50-40',	'MI-GOTR HST',				10,						8,				'M'),
        (@maxID+9,	'4221-90-50-40',	'MI-TM Adjustment HST',		10,						null,			'M')
End
go

