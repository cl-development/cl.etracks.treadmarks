select p.StartDate Period, v.Number reg#, c.* from claim c
inner join Period p
on p.id=c.ClaimPeriodID	 
inner join vendor v
on v.id=c.ParticipantID	 
where c.ClaimType in (4,5)
and p.StartDate>='2018-08-01'
and (c.IsTaxApplicable=0 or c.IsTaxApplicable is null)


update c set c.IsTaxApplicable=1 
from claim c
inner join Period p
on p.id=c.ClaimPeriodID
where c.ClaimType in (4,5)
and p.StartDate>='2018-08-01'
and (c.IsTaxApplicable=0 or c.IsTaxApplicable is null)

SELECT DISTINCT CONCAT('START /WAIT HTTPS://ots.TREADMARKS.CA/DEFAULT/RECALCULATE?CLAIMID=', C.ID, CHAR(13) + CHAR(10), 'TIMEOUT /T 3') 
from claim c
inner join Period p
on p.id=c.ClaimPeriodID
where c.ClaimType in (4,5)
and p.StartDate>='2018-08-01'
and (c.IsTaxApplicable=0 or c.IsTaxApplicable is null)