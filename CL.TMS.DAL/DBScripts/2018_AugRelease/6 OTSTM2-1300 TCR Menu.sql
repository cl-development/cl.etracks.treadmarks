﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1300 TCR menu of Dashboard
Purpose	    :   1. add Dashboard menu to appresource
            :   2. create permission for new AppResource Menu Dashboard   		
Created By  :   Joe Zhou July 11th, 2018
*****************************************************************/
USE [tmdb]
GO

if NOT EXISTS (select * from AppResource where ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard' and Name = 'TCR Service Threshold')

BEGIN
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

insert into [AppResource]	(Name,		ResourceMenu,		ResourceScreen,		ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES	
			('TCR Service Threshold',	'Dashboard',		'Dashboard',		    2,				88100,		@CreateBy,	@dt,		@dt)
		
END
go

--create permission for new appResource Dashboard/TCR Service Threshold

Declare @superAdminID int, @newTCR int, @iCounter int, @RoleId int, @iNoAccess int,  @iReadOnly int, @iEditSave int
Declare @getRoleIds cursor
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]

set @newTCR=(select id from AppResource	where Name = 'TCR Service Threshold' and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')

select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iReadOnly = Id from AppPermission where Name = 'ReadOnly'
select @iEditSave = Id from AppPermission where Name = 'EditSave'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
	(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
	WHERE rp.AppResourceId = @newTCR and rp.RoleId = @RoleId)
	BEGIN
		if (@RoleId = @superAdminID) 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newTCR, @iEditSave)
			end 
		else 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newTCR, @iEditSave)
			end
	END

	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go