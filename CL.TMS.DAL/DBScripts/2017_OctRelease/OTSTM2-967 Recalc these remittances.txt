select p.ShortName, * from TSFClaim t 
inner join Period p
on p.id=t.PeriodID
where t.PeriodID in
(select id from Period p where p.PeriodType=1 and p.StartDate>='2016-07-01')
and RecordState not in ('Approved', 'Open')
and TotalRemittancePayable>=Credit and Penalties>0 order by t.RegistrationNumber