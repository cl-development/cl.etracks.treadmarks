USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRegistrantSpotlightsPaginate]    Script Date: 10/23/2017 5:04:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_GetRegistrantSpotlightsPaginate] 
	@keyWords nvarchar(255),@section nvarchar(50), @start int, @pageLimit int
AS
BEGIN
	DECLARE @wordCount int = (SELECT count(*) FROM [dbo].[fnSplitSpotlight](@keyWords, ' '));
	DECLARE @moduleType nvarchar(50);

	DECLARE @topCount bigint =  0x7fffffffffffffff;
	
	DECLARE @tmp TABLE (
		ID int,
		vendorID int,        
		PeriodStartDate datetime,        
		PlaceHolder1 nvarchar(100),
		PlaceHolder2 nvarchar(65),
		PlaceHolder3 nvarchar(65),
		PlaceHolder4 nvarchar(65),
		PlaceHolder5 nvarchar(65),
		ModuleType int,
		[Status] nvarchar(50),
		Section nvarchar(50)
	);

	DECLARE @result TABLE (
		ID int,
		vendorID int,        		
		PeriodStartDate datetime,        
		PlaceHolder1 nvarchar(100),
		PlaceHolder2 nvarchar(65),
		PlaceHolder3 nvarchar(65),
		PlaceHolder4 nvarchar(65),
		PlaceHolder5 nvarchar(65),
		ModuleType int,
		[Status] nvarchar(50),
		Section nvarchar(50),
		TotalCount int
	);

	INSERT INTO @result ([ID], vendorID, PeriodStartDate,
		 PlaceHolder1, PlaceHolder2, PlaceHolder3, PlaceHolder4, PlaceHolder5,
		 ModuleType, [Status], Section)
	SELECT *
	FROM
	(
		SELECT top (@topCount)
			v.ID 'ID',
			v.ID 'vendorID',
			v.DateReceived PeriodStartDate,
			v.Number 'PlaceHolder1',
			v.BusinessName 'PlaceHolder2',
			v.OperatingName	'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			v.VendorType 'ModuleType',
			CONVERT(VARCHAR(1), v.IsActive) 'Status',
			'Applications' 'Section'			
		FROM [dbo].[Vendor] v 
		WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Applications' or tblWords.splitdata='All')>0)
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount and @wordCount > 0) OR
		v.Keywords like '%'+@keyWords+'%') order by v.IsActive desc, v.number

		UNION All  

		SELECT top (@topCount)
			c.ID 'ID',
			c.ID 'vendorID',
			c.DateReceived PeriodStartDate,			
			c.RegistrationNumber 'PlaceHolder1',
			c.BusinessName 'PlaceHolder2',
			c.OperatingName	'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			c.CustomerType 'ModuleType',
			CONVERT(VARCHAR(1), c.IsActive) 'Status',
			'Applications' 'Section'
		FROM [dbo].[Customer] c
		WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Applications' or tblWords.splitdata='All')>0)
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount AND @wordCount > 0) OR
		c.Keywords like '%'+@keyWords+'%') order by c.IsActive desc, c.RegistrationNumber

		UNION All

			SELECT top (@topCount)
			c.ID 'ID',
			c.ParticipantID 'vendorID',	
			p.StartDate PeriodStartDate,					
			v.number 'PlaceHolder1',
			p.shortname 'PlaceHolder2',
			FORMAT(coalesce(c.ClaimsAmountTotal, 0), N'C', N'en-ca') 'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			c.Claimtype 'ModuleType',
			c.Status 'Status',
			'Claims' 'Section'
			FROM [dbo].Claim c
			inner join Vendor v
			on v.id=c.participantid
			inner join Period p
			on p.id=c.claimperiodid

			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Claims' or tblWords.splitdata='All')>0) 
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.number like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			v.number like '%'+@keyWords+'%') order by PeriodStartDate desc
		
		UNION All

			SELECT top (@topCount)
			c.ID 'ID',
			c.registrationnumber 'vendorID',	
			p.StartDate PeriodStartDate,										
			c.registrationnumber 'PlaceHolder1',
			p.ShortName 'PlaceHolder2',
			FORMAT(coalesce(c.totaltsfdue - coalesce(c.credit, 0) + coalesce(c.adjustmenttotal, 0), 0), N'C', N'en-ca') 'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			7 'ModuleType',
			c.RecordState 'Status',
			'Remittances' 'Section'
			FROM [dbo].TSFClaim c
			inner join Period p
			on p.id=c.periodid
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Remittances' or tblWords.splitdata='All')>0)
			AND ((( SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.registrationnumber like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			c.registrationnumber like '%'+@keyWords+'%') order by p.startdate desc, c.registrationnumber			
	
		UNION ALL

		SELECT distinct --top 1000
			u.ID 'ID',
			v.ID 'vendorID',		
			u.CreateDate 'PeriodStartDate',									
			concat(u.firstname, ' ', u.lastname) 'PlaceHolder1',
			u.email 'PlaceHolder2',			
			v.number 'PlaceHolder3',
			v.operatingname 'PlaceHolder4',
			'' 'PlaceHolder5',
			6 'ModuleType',
			CONVERT(VARCHAR(1), vu.IsActive) 'Status',
			'Users' 'Section'
			from [User] u 
			
			inner join vendoruser vu
			on vu.userid=u.id
			
			inner join vendor v
			on vu.vendorid=v.id			
			
			
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Users' or tblWords.splitdata='All')>0)
			AND (((SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.number like '%' + tblWords.splitData + '%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			v.number like '%'+@keyWords+'%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%'+@keyWords+'%')

	UNION ALL

	SELECT distinct --top 1000
			u.ID 'ID',
			c.ID 'vendorID',		
			u.CreateDate 'PeriodStartDate',									
			concat(u.firstname, ' ', u.lastname) 'PlaceHolder1',
			u.email 'PlaceHolder2',			
			c.registrationnumber 'PlaceHolder3',
			c.operatingname 'PlaceHolder4',
			'' 'PlaceHolder5',
			6 'ModuleType',
			CONVERT(VARCHAR(1), cu.IsActive) 'Status',
			'Users' 'Section'
			from [User] u 
			
			inner join CustomerUser cu
			on cu.userid=u.id			
			
			inner join Customer c
			on cu.customerid=c.id			
			
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Users' or tblWords.splitdata='All')>0)
			AND (((SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.registrationnumber like '%' + tblWords.splitData + '%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			c.registrationnumber like '%'+@keyWords+'%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%'+@keyWords+'%')
		
		UNION ALL

		SELECT top 2000
			t.ID 'ID',
			isnull(isnull(vi.ID,vo.id), 0) 'vendorID',
			t.transactiondate 'PeriodStartDate',											
			cast(t.friendlyid as nvarchar(10)) 'PlaceHolder1',
			t.transactiontypecode 'PlaceHolder2',			
			FORMAT (t.transactiondate, 'yyyy-MM-dd') 'PlaceHolder3',
			isnull(isnull(t.badge, vi.number),'') 'PlaceHolder4',
			isnull(vo.number,'') 'PlaceHolder5',
			7 'ModuleType',
			t.status 'Status',
			'Transactions' 'Section'
			from [Transaction] t 
			left outer join vendor vi
			on t.incomingid=vi.id
			left outer join vendor vo
			on t.outgoingid=vo.id
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Transactions' or tblWords.splitdata='All')>0) 
			AND ((( SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE concat(t.friendlyid, ' ', t.badge) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			concat(t.friendlyid,' ', t.badge) like '%'+@keyWords+'%') order by t.TransactionDate desc, t.FriendlyId
	) main
	
	--ORDER BY main.Status desc, main.PlaceHolder1;

	UPDATE @result
	SET TotalCount = (SELECT count(*) FROM @result);
	
	SET @pageLimit = coalesce(@pageLimit, (SELECT count(*) FROM @result));
	SET @start = coalesce(@start, 0);

	SELECT *
	FROM @result r
	ORDER BY r.Status DESC, r.PlaceHolder1
	OFFSET @start ROW
	FETCH NEXT @pageLimit ROWS ONLY;

END
