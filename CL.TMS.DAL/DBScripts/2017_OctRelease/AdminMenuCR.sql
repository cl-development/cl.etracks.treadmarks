﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO


-- Create Company Branding - Terms & Conditions table #973
-- Add Application Type
IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='ApplicationType')
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,					[Code] ,				[Description] ,					[Category] ,		[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Steward Application',	   'StewardApplication',	'Steward Application',	  		'ApplicationType',	1 ,					1),
		('Collector Application',  'CollectorApplication',	'Collector Application', 		'ApplicationType',	2 ,					2),
		('Hauler Application',     'HaulerApplication',		'Hauler Application',    		'ApplicationType',	3 ,					3),
		('Processor Application',  'ProcessorApplication',	'Processor Application', 		'ApplicationType',	4 ,					4),		
		('RPM Application',	       'RPMApplication',		'RPM Application',	      		'ApplicationType',	5 ,					5)	
END
GO

-- Create Company Branding - Terms & Conditions table #973
if not exists (select * from sysobjects where name='TermsAndConditions' And xtype='U')
begin
CREATE TABLE [dbo].[TermsAndConditions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationTypeID] [int] not null,
	[Content] [nvarchar] (MAX) null,

	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NULL,

	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,

	[EffectiveStartDate] [datetime] NOT NULL,
	[EffectiveEndDate] [datetime] NOT NULL,

	[RowVersion] [timestamp] NOT NULL,

	
CONSTRAINT [PK_TermsAndConditions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
	ALTER TABLE [dbo].[TermsAndConditions]  WITH CHECK ADD  CONSTRAINT [FK_TermsAndConditions_CreatedByUser] FOREIGN KEY([CreatedByID])
	REFERENCES [dbo].[User] ([ID])
end
GO

-- Create TermsAndConditionsConditionsNote table
if not exists (select * from sysobjects where name='TermsAndConditionsNote' And xtype='U')
begin
	CREATE TABLE [dbo].[TermsAndConditionsNote](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[ApplicationTypeID] [int] NOT NULL,
		[Note] [varchar](max) NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserID] [bigint] NOT NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_TermsAndConditionsNote] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	SET ANSI_PADDING OFF	
	ALTER TABLE [dbo].[TermsAndConditionsNote]  WITH CHECK ADD  CONSTRAINT [FK_TermsAndConditionsNote_User] FOREIGN KEY([UserID])
	REFERENCES [dbo].[User] ([ID])
	ALTER TABLE [dbo].[TermsAndConditionsNote] CHECK CONSTRAINT [FK_TermsAndConditionsNote_User]
end
go

/***
* Application add column TermsAndConditionsID
* Note TODO: update Application TermsAndConditionsID with relative value for exist application records.
* Sep 7, 2017 Joe Zhou
*****/
IF COL_LENGTH('Application', 'TermsAndConditionsID') IS NULL ALTER TABLE Application ADD [TermsAndConditionsID] int NULL
GO
IF (OBJECT_ID('Application_TermsAndConditions', 'F') IS NULL)
ALTER TABLE [dbo].[Application]  WITH CHECK ADD  CONSTRAINT [Application_TermsAndConditions] FOREIGN KEY([TermsAndConditionsID])
REFERENCES [dbo].[TermsAndConditions] ([ID])
GO

-- initial Terms & Conditions 
declare @userID int
set @userID=(select top 1 ID from [User])
if not exists (select * from TermsAndConditions where ApplicationTypeID=1)
begin
	insert into [TermsAndConditions]
	(ApplicationTypeID, CreatedDate,  CreatedByID, EffectiveStartDate,   EffectiveEndDate,Content)
	values
	(1,	'2017-09-19',@userID,	'2017-09-19','9999-12-31','
		<p>
                <strong>RULES FOR STEWARDS WITH RESPECT TO PAYMENT OF TIRE STEWARDSHIP FEES</strong><br>
                1.  <strong><u>Interpretation</u></strong><br>
                In these Rules, the following terms shall have the following meanings. A reference to a statute or act of any legislature, or to a regulation made under the authority of any such statute or act, shall, unless otherwise expressly provided, be deemed to refer to such statute or act as it existed at the date of these Rules and as it may be amended or replaced from time to time. The headings used throughout these Rules are solely for convenience and are not to be used as an aid in the interpretation of these Rules. The singular or masculine or neuter, as used in these Rules, shall be construed to mean the plural or feminine or body corporate where the context of these Rules may so require. Capitalized terms which are not otherwise defined will have the meaning given to them in the Waste Diversion Act, 2002:<br>
                <strong>Affiliate</strong> means an affiliated body corporate within the meaning of subsection 1(4) of the Business <i>Corporations Act</i> (Ontario).<br>
                <strong>Base Interest Rate</strong> means the interest rate established from time to time under the Rules of Civil Procedure of the <i>Courts of Justice Act</i> (Ontario), as amended from time to time, for prejudgment interest;<br>
                <strong>Brand</strong> means a trademark within the meaning of the <i>Trade-marks Act</i> (Canada), whether or not registered pursuant thereto;<br>
                <strong>Brand Owner</strong> means, with respect to Branded Tires, during any time in the Data Period:<br>
                (a) a Person Resident in Ontario who is the registered owner of the Brand, or<br>
                (b) a Person Resident in Ontario who is a licensee of the registered owner of the Brand, or<br>
                (c) a Person Resident in Ontario, who owns the intellectual property rights to the Brand, or<br>
                (d) a Person Resident in Ontario, who is a licensee, in respect of the intellectual property rights to the Brand;<br>
                <strong>Branded</strong> means that a Brand is attached to or otherwise associated with Tires; <br>
                <strong>Commencement Date</strong> means September 1, 2009;<br>
                <strong>Commercial Connection</strong>, for the purposes of these Rules, means that a Person has or will derive a direct economic benefit when particular New Tires are Supplied in Ontario, and which includes, for greater clarity, the promotional benefit arising when New Tires are Supplied gratis or at a loss;<br>
                <strong>Data Period</strong> means each calendar month starting from the Commencement Date;<br>
                <strong>Exempted Tires</strong> as defined in Appendix A means New Tires which are not required to be reported as Supplied or remitted on by a Steward;<br>
                <strong>Filed</strong> means electronically submitted or mailed to OTS at an address identified to the Stewards by mail or electronically, with confirmation of transmission in the case of sending by electronic means;<br>
                <strong>First Importer</strong> means a Person Resident in Ontario who imports New Tires into Ontario but is not a Brand Owner of such New Tires, and includes a Person Resident in Ontario who is the first to take title to or delivery or possession of such Tires, upon or after arrival in Ontario from elsewhere during the Data Period. A Person who takes delivery/possession of New Tires only for the purpose of transporting them to another Person in Ontario is not the First Importer of the Tires;<br>
                <strong>New Tires</strong> means any tires which result in the generation of Used Tires, including Tires Supplied with a new vehicle, or equipment or new replacement Tires or new additional Tires Supplied separately or with used vehicles, but does not include Retreaded Tires.<br>
                <strong>OEM</strong> means a manufacturer or First Importer of new vehicles for Supply in Ontario; <br>
                <strong>OTS</strong> means Ontario Tire Stewardship;<br>
                <strong>OTS website</strong> means the website located at <a href="http://www.RethinkTires.ca" target="_blank">ww.RethinkTires.ca</a>;<br>
                <strong>Person</strong> includes an individual, partnership, joint venture, sole proprietorship, company or corporation, government (whether national, federal, provincial, state, municipal, city, county or otherwise and including any instrumentality, division, body, department, board or agency of any of them), trust, trustee, executor, administrator or any other kind of legal personal representative, unincorporated organization, association, institution, entity, however designated;<br>
                <strong>Published Address</strong> means an address in Ontario appearing in a current telephone directory or a recognized current published business directory;<br>
                <strong>Regulation</strong> means Ontario Regulation 84/03 made under the Waste Diversion Act, 2002;<br>
                <strong>Resident in Ontario</strong> means either of the following:<br>
                (a) having a Published Address in Ontario; or<br>
                (b) having a "permanent establishment" in Ontario within the meaning given to that term in the <i>Corporations Tax Act</i> (Ontario);<br>
                <strong>Retreaded Tires</strong> means Tires which have been reconditioned for the purpose of extending the useful life of the Tires, including replacement of the tread rubber only or replacement of tread and sidewall rubbers;<br>
                <strong>Rules</strong> means these rules, and includes additional rules or amendments to these Rules from time to time, as published by OTS on the OTS website;<br>
                <strong>Steward</strong> means a Person designated as such under Rule 2; and "Stewards" means more than one Steward;<br>
                <strong>Steward''s Report</strong> means a report in the form set out on the OTS website from time to time prepared by a Steward and filed with OTS describing the aggregate amount of New Tires Supplied in the Data Period by the Steward and its Affiliates;<br>
                <strong>Supplied</strong> means:<br>
                (a) sold or otherwise transferred (whether by transfer of possession or title);<br>
                (b) leased;<br>
                (c) donated;<br>
                (d) disposed of; or<br>
                (e) otherwise made available or distributed in the Province of Ontario or for use in the Province of Ontario, and includes an import of New Tires for a purpose set out in Rule 2(3)(b); <br>
                <strong>Supply</strong> and <strong>Supplied</strong> have similar meanings;<br>
                <strong>Tire Stewardship Fees</strong> means the fees payable to OTS pursuant to the Regulation;<br>
                <strong>Tires</strong> in these Rules includes products comprised primarily of rubber for mounting on passenger vehicles, motorcycles, trucks, buses, mobile homes, trailers, aircraft, earthmoving, road building, mining, logging, agricultural, industrial and other vehicles to provide mobility, but does not include tires on or for toys, bicycles, personal mobility devices and commercial aircraft, or Exempted Tires;<br>
                <strong>Unbranded</strong> means a Brand is not attached to or otherwise associated with New Tires; <br>
                <strong>Used Tires</strong> has the same meaning as in the Regulation.<br>
                2.  <strong><u>Designation of Stewards</u></strong> <br>
                For the purposes of determining which Person shall be designated as a Steward for New Tires, the following provisions shall apply, in the order in which they are set out. If two or more Persons are designated as a Steward pursuant to the following, the earlier provision shall prevail.<br>
                (1) An OEM is designated as the Steward with respect to all New Tires on vehicles<br>
                Supplied by the OEM;<br>
                (2) A Brand Owner is designated as a Steward with respect to all New Tires:<br>
                (a) Supplied in the Data Period in respect of which it is the Brand Owner; and<br>
                (b) to which it has a Commercial Connection;<br>
                (3) A First Importer is designated as a Steward with respect to all New Tires:<br>
                (a) Supplied in the Data Period of which it is the First Importer; or<br>
                (b) of which it is the First Importer in the Data Period for use by it or its Affiliate in the Province of Ontario, other than New Tires purchased by an individual for personal use;<br>
                (4) If there are Unbranded New Tires Supplied in the Data Period, and if the<br>
                manufacturer is Resident in Ontario, the manufacturer of such New Tires shall be designated as the Steward for such New Tires; otherwise the First Importer shall be designated as the Steward for such New Tires;<br>
                (5) If there are two or more Brand Owners for the same New Tires Supplied in the<br>
                same Data Period, the Brand Owner most directly connected to the manufacture of the New Tires shall be designated as the Steward;<br>
                (6) Where there is no Steward for a particular Supply of New Tires in Ontario, any<br>
                Person who elects to become a Steward for such New Tires shall, upon obtaining the consent of OTS and executing an agreement to be bound by these Rules, be designated a Steward for such Supply;<br>
                (7) If a Person who has been previously designated as a Steward subsequently finds<br>
                that none of the circumstances set out in Rules 2(1) through 2(6) inclusive apply to it and that it is no longer obligated as a Steward with respect to New Tires, and if such Person wishes to be deregistered as a Steward:<br>
                (a) Using the form designated by OTS from time to time, the Steward shall notify OTS of its belief that it is no longer obligated as a Steward with respect to New Tires in Ontario, and that it desires to be deregistered as a Steward;<br>
                (b) The Steward shall file such reports with respect to its final Supply of New Tires, and the cessation of its obligations as a Steward, as are required by these Rules and/or OTS, in such form as OTS may designate from time to time; and<br>
                (c) OTS shall review the Steward''s final reports, and if OTS is satisfied that all the Steward''s obligations have been discharged, provide confirmation in writing of deregistration as a Steward.<br>
                (8) A Person designated as a Steward pursuant to these Rules shall remain a<br>
                Steward and shall be required to fulfil all requirements hereunder at all times until the Steward receives a confirmation of deregistration as described in Rule 2(7)(c).<br>
                3.  <strong><u>Steward''s Report</u></strong><br>
                (1)   Every Steward shall file its first Steward''s Report with OTS on or before the later of:<br>
                (a) the last day of the first calendar month after the end of the first Data<br>
                Period after the Commencement Date; and<br>
                (b) 90 days after such Steward is notified of the existence of these Rules and<br>
                how to obtain a copy of them.<br>
                (2) Stewards shall file their reports in the form and using the technology prescribed by OTS and published on the OTS website from time to time.<br>
                (3) Stewards may amend a Steward''s Report within [15] days of filing and with the consent of OTS to correct information in the Steward''s Report that is in error or to replace data previously reported.<br>
                (4) Once it has filed its first Steward''s Report, a Steward shall file subsequent Steward''s Reports for each Data Period on or before the last day of the calendar month following the end of the Data Period, or on such other schedule as may be determined by OTS from time to time and posted in these Rules in Appendix B.<br>
                (5) Notwithstanding the above OTS may require a Steward to file a Steward''s Report for a specified time period within 30 days after OTS sends a written request to the Steward.<br>
                (6) The first Steward''s Report shall cover the period from the later of the Commencement Date and the date on which the Person first became a Steward to the last day of the calendar month preceding the month of filing.<br>
                4.  <strong><u>Fees Payable</u></strong><br>
                (1) Stewards shall pay the Tire Stewardship Fees payable under the Regulation to OTS on New Tires, other than Exempted Tires, Supplied in the relevant Data Period on or before the last day of the calendar month immediately following the end of such Data Period. The amount of Tire Stewardship Fees shall be calculated in accordance with the Regulation and the applicable Table of Stewardship Fees shall be posted on the OTS website. The amount of Steward''s Fees payable by a Steward shall be determined by multiplying the number of units of each type of New Tires included in the Steward''s Report by the Fee Rate set out opposite such type.<br>
                (2) Notwithstanding that a Steward has not received the notice in Rule 3(1), it is responsible for payment of Tire Stewardship Fees for all New Tires for which it is a Steward from and after the Commencement Date to the date on which it receives such notice.<br>
                (3) Except for money due to a Steward under subsection 9(4) of the Regulation, OTS may provide a credit for Tire Stewardship Fees which are overpaid, or which are paid in respect of the same New Tires for which another Person has paid Tire Stewardship Fees.<br>
                (4) In addition to the Tire Stewardship Fees payable under Rule 4(1), Stewards shall pay the Tire Stewardship Fees determined under Section 9 of the Regulation no later than June 30 in the calendar year following the calendar year in respect of which such Fees are required to be paid.<br>
                (5) A Person described in any provision of Rule 2 who receives a Supply of New Tires from a Person who may be a Brand Owner, First Importer or OEM (the "provider") must ensure that the provider is a Steward with an OTS identification number. The OTS identification number will be posted on the OTS website.<br>
                (6) Tire Stewardship Fees do not include HST. If HST becomes eligible on Tire Stewardship Fees, OTS will indicate this on the OTS website and/or in the form of Steward''s Report published by OTS, and Stewards shall remit HST applicable to the Tire Stewardship Fee to OTS with each remittance of the Tire Stewardship Fee payable under Rules 4(1) and 4(4).<br>
                5.  <strong><u>Penalties, Interest, Enforcement Costs and Back Fees</u></strong><br>
                (1) Stewards who fail to pay Tire Stewardship Fees by the dates set out in Rule 4 or the Regulation, as applicable, will be liable to pay a penalty calculated at 10% of Tire Stewardship Fees due and payable.<br>
                (2) If the amounts reported in a Steward''s Report are inaccurate, any deficiency in Tire Stewardship Fees resulting from such inaccuracies shall be immediately due and payable from the date of the filing of the correcting Steward''s Report or the date the Steward first knew of such inaccuracy, whichever is earlier, and, if not paid within 30 days, will be liable to pay a penalty equal to 10% of such Tire Stewardship Fee deficiency.<br>
                (3) Interest on the amounts payable under Rules 5(1) and 5(2) shall accrue from their respective due dates at the Base Interest Rate plus 3% per annum.<br>
                (4) A Steward shall pay all the internal and external collection costs of OTS with respect to unpaid Tire Stewardship Fees or other amounts owed to OTS, including all proper and reasonable legal and professional fees incurred by OTS, whether or not an action or any other legal remedy has been commenced. In addition, if any review by OTS or its designee of a Steward''s records reveals that the Steward has failed to properly discharge all its obligations under these Rules, including the obligation to pay Tire Stewardship Fees, such Steward shall pay all OTS''s costs with respect to the review, whether OTS''s internal costs or amounts paid by OTS to external auditors, including for any follow-up review or inspection undertaken as a result of a finding of non-compliance. Such costs must be paid within 30 days of OTS giving notice that payment is required.<br>
                (5) OTS may waive all or part of any penalty, interest or charges otherwise payable under this Rule 5.<br>
                6.  <strong><u>Record Provision and Retention</u></strong><br>
                (1) Upon request from OTS, Stewards shall promptly provide data necessary for the<br>
                preparation of the Steward''s Report, including calculation methodology, product data, internal audit reports, list of Brands reported and list of Brands excluded from report and such other information or data in the Stewards possession or control as may be reasonably requested by OTS to substantiate the accuracy of the Steward''s Report.<br>
                (2) A Steward shall retain records or, on receipt of written request, provide records at<br>
                an address in the Province of Ontario to substantiate and verify the amounts set out in its Steward''s Reports for a period of not less than five years from the date of the Steward''s Report to which they relate. A Steward shall grant access to OTS or its designee upon OTS''s request to examine its books and records to enable OTS to audit and inspect such records respecting a Steward''s Report up to five years after the date of receipt of such Steward''s Report by OTS. A Steward shall provide OTS or its designee with any and all records requested related to the supply or sale of new tires and shall cooperate with the review of such records at the Steward''s own cost.<br>
                7.  <strong><u>Dispute Resolution</u></strong> <br>
                If any dispute arises between a Steward and OTS as to the amount of New Tires that is required to be included in a Steward''s Report:<br>
                (1) The parties shall attempt to resolve the dispute through designated representatives from each of OTS and the Steward within 30 days after written notice of the dispute was first given, or as otherwise agreed upon.<br>
                (2) If the parties are unable to resolve the dispute within the above period, the Steward and OTS shall, within 30 days thereafter, jointly select an arbitrator to arbitrate the dispute. If the Steward does not nominate an arbitrator within the 30 day period, OTS shall nominate the arbitrator. The arbitration shall be conducted in accordance with the Arbitration Act, 1991.<br>
                (3) The arbitrator shall render a written decision on the dispute within 14 days after the arbitration hearing or submission. The decision of the arbitrator shall be final and binding on the parties and shall be subject to appeal only on questions of law and not on questions of fact, in accordance with Section 45 of the Arbitration Act, 1991, and shall be enforceable against OTS and the Steward, as the case may be, immediately on the issue of such decision to the parties to the dispute.<br>
                8.  <strong><u>Interpretive Memoranda</u></strong><br>
                OTS may publish on the OTS website interpretive memoranda on these Rules and how it proposes to administer them.<br>
                9.  <strong><u>Publishing of Names</u></strong> <br>
                (1) OTS will provide all Stewards with an identification number.<br>
                (2) The names and identification numbers of Stewards filing Steward''s Reports will be posted on the OTS website.<br>
                (3) OTS may post a list on the OTS website of all Brands reported in Steward''s Reports from time to time, and all OEM''s, Brand Owners, First Importers and other Stewards associated with such Brands.<br>
                10. <strong><u>Notice</u></strong><br>
                Any notice, request or other communication from OTS to a Steward which is required or may be given under these Rules may be delivered or transmitted by means of electronic communication, personal service or by prepaid first class postage to the Steward at a Published Address in Ontario and shall be deemed to have been received on the third day after posting and on the first day after the date of electronic transmission, in each case which is not a Saturday, Sunday or public holiday in Ontario.<br>
                11. <strong><u>Effective Date</u></strong> <br>
                This version of the Rules as amended shall be effective July 1, 2015.<br>
                12. <strong><u>Amendments to Rules</u></strong> <br>
                These Rules and any forms, fee schedules or other matters provided for or referred to in them may be amended, removed or replaced by OTS from time to time, subject to any required contractual or regulatory approvals. The version of these Rules and the Stewards Fees in effect at the time of a particular Supply shall continue to apply to all New Tires which are the subject of such Supply, notwithstanding any subsequent amendments to these Rules or any document posted on the OTS website pursuant to them.
            </p>
            <p align="center"><strong>Appendix A: OTS Definition of Program Tires</strong><br></p>
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th width="25%"><strong>Product Category</strong></th>
                        <th><strong>Definition</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1)</td>
                        <td>Exempted Tires</td>
                        <td>
                            All tires with an overall tire diameter of less than 7"
                            regardless of weight<br>
                            All tires in which the rubber content is greater than 50% (by weight) derived from Used Tires
                        </td>
                    </tr>
                    <tr>
                        <td>2)</td>
                        <td>On-Road Tires</td>
                        <td>
                            <strong>
                                Passenger Tires, Small RV Tires, ST Trailer tires and
                                Light Truck Tires and Temporary Spares
                            </strong><br>
                            Passenger tires are designed for use on passenger
                            cars, light trucks, small RVs and multipurpose
                            passenger vehicles (MPVs), including sport utility
                            vehicles (SUVs) and crossover utility vehicles (CUV''s), and
                            to comply with Canadian Motor Vehicle Safety Standard
                            (CMVSS No. 109).<br>
                            The light truck tire category is tires designed for use on
                            consumer or commercial light trucks, under 10,000 lbs.
                            Gross Vehicle Weight, and comply with Canadian
                            Motor Vehicle Safety Standard (CMVSS No. 119).
                            Codes found on the sidewall of passenger and
                            light truck tires are P (Passenger) and LT (Light Truck).
                            Temporary spare tires are marked T (Temporary).<br>
                            <strong>Motorcycle, Golf Cart and All Terrain Vehicle Tires</strong><br>
                            Includes all tires specifically designed for on/off highway motorcycles, motorcycle sidecars, motor bikes, mopeds, mini-cycles, golf carts and all terrain vehicles.<br>
                            <strong>Forklift, Small utility and Skid steer Tires</strong><br>
                            Includes pneumatic and solid forklift and Skid Steer tires measuring 16" rim size and smaller.<br>
                            <strong>Free Rolling Farm Tires</strong><br>
                            Includes free rolling farm and implement tires up to 16" rim size used on farm equipment.<br>
                            <strong>Medium Truck Tires</strong><br>
                            Also commonly known as Commercial Truck Tires —Truck and Bus tires including Wide Base or Heavy Truck tires designed for truck/bus applications and Larger RV tires not marked "P or LT". All of which comply with Canadian Motor Vehicle Safety Standard (CMVSS No. 119).
                        </td>
                    </tr>
                    <tr>
                        <td>3)</td>
                        <td>Off-Road Tires</td>
                        <td>
                            <strong>All Terrain Vehicle Tires</strong><br>
                            <strong>Agricultural Drive Tires</strong><br>
                            Includes drive wheel tires used on tractors and combines and tree harvesting equipment. These tires are normally identified with a sidewall marking with suffix letters (R), or (HF) or (LS) and are 16.5” and up rim size. These tires are listed in The Tire and Rim Association Inc. annual yearbook Section 5 Agricultural.<br>
                            <strong>Industrial Forklift, Skid Steer Tires</strong><br>
                            Includes pneumatic and solid forklift and Skid Steer tires with a rim diameter greater than 16”.<br>
                            <strong>Small Off The Road Tires</strong><br>
                            Sizes 1300Tires &lt; and = to 23.5R25 Rim Size<br>
                            <strong>Medium Off The Road Tires</strong><br>
                            Above 23.5R25 to 33 inch Rim Size<br>
                            <strong>Large Off The Road Tires</strong><br>
                            Above 33 inch to and including 39 inch Rim Size<br>
                            <strong>Giant Off The Road Tires</strong><br>
                            Over 39 inch Rim Size<br>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p align="center">
                <strong>Appendix B</strong><br>
                <strong>Payment and Reporting Schedule</strong>
            </p>
            <p>Payment and reporting schedule will be monthly. Stewards must submit their Remittance Report and TSF Remittance within 30 days of the end of the month to which the Report and Remittance amount apply.<br></p>
            <table class="table">
                <thead>
                    <tr>
                        <th width="33%">Monthly Data period</th>
                        <th width="33%">Data period end date</th>
                        <th>Payment and Reporting deadline</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>January 1 2015</td>
                        <td>Last day of January 2015</td>
                        <td>30 days from last day January 2015</td>
                    </tr>
                    <tr>
                        <td>February 1 2015</td>
                        <td>Last day of February 2015</td>
                        <td>30 days from last day of February 2015</td>
                    </tr>
                    <tr>
                        <td>March 1 2015</td>
                        <td>Last day of March 2015</td>
                        <td>30 days from last day of March 2015</td>
                    </tr>
                    <tr>
                        <td>Etc.</td>
                        <td>Etc.</td>
                        <td>Etc.</td>
                    </tr>
                </tbody>
            </table>

            <p>
                <strong>Semi-Annual Reporting Schedule</strong><br>
                If in the prior calendar year a Steward remitted less than $10,000 in TSFs the Steward may elect to submit Remittance Reports and TSF Remittances on a semi-annual schedule.<br>
            </p>

            <table class="table">
                <thead>
                    <tr>
                        <th width="33%">Semi-Annual Data period</th>
                        <th width="33%">Data period end date</th>
                        <th>Payment and Reporting deadline</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>July 1 2015 — December 31 2015</td>
                        <td>Last day of December 2015</td>
                        <td>30 days from last day of December 2015</td>
                    </tr>
                    <tr>
                        <td>January 1 2016 — June 30 2016</td>
                        <td>Last day of June 2016</td>
                        <td>30 days from last day of June 2016</td>
                    </tr>
                    <tr>
                        <td>July 1 2016 — December 31 2016</td>
                        <td>Last day of December 2016</td>
                        <td>30 days from last day of December 2016</td>
                    </tr>
                    <tr>
                        <td>Etc.</td>
                        <td>Etc.</td>
                        <td>Etc.</td>
                    </tr>
                </tbody>
            </table>
	'),
	(2, '2017-09-19',@userID,	'2017-09-19','9999-12-31','
<p>                 <strong>RECITALS:</strong><br />                 
<strong>A.</strong>Collector OTS has been designated as the industry funding organization under the Act to be responsible for                
 the collection and environmentally responsible recycling of Used Tires;<br />                 
 <strong>B.</strong> The Collector wishes to operate a collection site for Used Tires and become entitled to Used Tire                 
 Pickup at no charge in accordance with the Plan;<br />                 
 <strong>C.</strong> The Collector has been approved by OTS as an approved collector in accordance with the Plan;<br />                 
 and<br />                 
 <strong>D.</strong> The purpose of this Agreement is to set out the terms and conditions under which the Collector                
  will operate a collection site under the Plan.<br />                 
  <strong>NOW THEREFORE</strong> the parties agree as follows, as of the date set out above:<br />                
   <strong>                     ARTICLE 1<br />                     
   DEFINITIONS                 </strong><br />                 
   1.1 <strong><u>Definitions</u></strong>. In addition to the words and phrases defined in the recitals or elsewhere in this                 
   Agreement, as used in this Agreement, in any schedule hereto, in any amendment hereof, and in                 
   any documents to be executed and delivered pursuant to this Agreement, the following words and                 
   phrases have the following meanings, respectively:<br />                 
   (a) “<strong>Act</strong>” means the Waste Diversion Act, 2002, S.O. 2002 c.6 as amended from time to                 
   time;<br />                 (b) “<strong>Act of Default</strong>” has the meaning given in Section 7.1;<br />                
    (c) “<strong>Agreement</strong>” means this Agreement, including the schedules to this Agreement, as it or                
	 they may be amended or supplemented from time to time, and the expressions “hereof”,                 
	 “herein”, “hereto”, “hereunder”, “hereby” and similar expressions refer to this Agreement                 
	 and not to any particular section of other portion of this Agreement;<br />                 
	 (d) “<strong>Applicable Laws</strong>” has the meaning given in Section 2.1(q);<br />                 
	 (e) “<strong>Approved Purpose</strong>” means a purpose for the use of recycled tires which is found on the                 
	 list of Approved Purposes for Recycled Tires maintained by OTS and published on                
	  OTS’s web site from time to time;<br />                 
	  (f) “<strong>Arbitration Guidelines</strong>” has the meaning given in Section 13.3;<br />                 
	  (g) “<strong>Audit</strong>” has the meaning given in Section 4.2<br />                
	   (h) “<strong>Change Notice</strong>” has the meaning given in Section 13.6;<br />                 
	   (i) “<strong>Collection Incentives</strong>” means those certain financial incentives from time to time                 
	   determined and payable by OTS to a person designated by OTS from time to time;<br />                 
	   (j) “<strong>Collector</strong>” has the meaning given to that term in the listing of parties to this Agreement;<br />                
	    (k) “<strong>Culled Used Tires</strong>” has the meaning given in Section 2.4;<br />                
		 (l) “<strong>Effective Date</strong>” ” has the meaning given to that term in the listing of parties to this                
		  Agreement;<br />                 
		  (m) “<strong>Environmental Laws</strong>” means any and all applicable laws, statutes, regulations, treatise,                 
		  orders, judgments, decrees, official directives and all authorizations of any department or                
		   body of any federal, provincial, regional or municipal government of any agency thereof                 
		   relating to the protection of the environment, including in particular, but without limiting                 
		   the generality of the foregoing, the manufacture, use, storage, disposal and transportation                 
		   of any Hazardous Substance;<br />                 
		   (n) “<strong>Exemption Order</strong>” means a written confirmation issued by OTS that certain Used                 
		   Tires, tire parts or processed rubber which would otherwise be Non-Eligible Material, are                
		    eligible for the claiming of certain financial incentives, on the terms set out therein;<br />                 
			(o) “<strong>False Statement</strong>” has the meaning given in Section 7.1(e);<br />                
			 (p) “<strong>Guidelines</strong>” means any directives, procedure manuals, administrative guidance, or other                
			  document regarding the implementation of the Plan published by OTS from time to time                 
			  on its web site;<br />                
			  (q) “<strong>Hazardous Substance</strong>” includes any contaminant, pollutant, dangerous substance,                
			   liquid waste, industrial waste, hauled liquid waste, toxic substance, hazardous waste,                
			    hazardous material, or hazardous substance as defined in or pursuant to any law,                 
				judgement, decree, order, injunction, rule, statute or regulation of any court, arbitrator or                 
				federal, provincial, state, municipal, county or regional government or governmental                 
				authority, domestic or foreign, or any department, commission, bureau, board,                 
				administrative agency or regulatory body of any of the foregoing to which the Collector if                 
				subject;<br />                 
				(r) “<strong>Inventory Statement</strong>” means any report submitted by the Collector with regard to the                 
				inventory of Used Tires and/or parts thereof held on the Collector’s premises and/or in                 
				the Collector’s inventory, as at a certain date;<br />                 
				(s) “<strong>Mediation Guidelines</strong>” has the meaning given in section 13.3;<br />                 
				(t) “<strong>Minister</strong>” means the Minister of the Environment for Ontario;<br />                 
				(u) “<strong>Non-Eligible Material</strong>” means:<br />                 
				(i) Used Tires, tire parts or processed rubber held in Collector’s inventory prior to                 
				September 1st, 2009, unless the subject of an Exemption Order issued by OTS;<br />                 
				and/or<br />                 
				(ii) Used Tires or parts thereof originating outside Ontario or obtained by the                
				 Collector directly or indirectly from any source outside Ontario; and/or<br />                 
				(iii) any material, article or item that is not a Used Tire or part thereof;<br />  
			    (v) “<strong>OTS</strong>” has the meaning given to that term in the listing of parties to this Agreement;<br />				 
				(w) “<strong>Party</strong>” means a party to this Agreement and any reference to a Party includes its 
				successors and permitted assigns; and Parties means every Party;<br />                 
				(x) “<strong>Plan</strong>” means the Used Tires Program Plan submitted by OTS on 27 February 2009 and
				approved by Waste Diversion Ontario and the Minister of Environment, as amended;<br /> 
				(y) “<strong>Program Participant</strong>” means any collector, hauler, or processor which has registered   
				and entered into an agreement with OTS to provide services under the Plan;<br />  
				(z) “<strong>Registered Hauler</strong>” means an entity engaged in the business of collecting and				 
	            transporting Used Tires to Registered Processors, and which has registered and remains
				in good standing with OTS and entered into a Hauler Agreement with OTS;<br />
				(aa) “<strong>Registered Processor</strong>” means a business that processes Used Tires into material that                 
				can be further processed in order to recover specific components within the same                 
				organization or sent to downstream processors for use as a raw material in another                 
				process, and which has registered and remains in good standing with OTS and entered                 
				into a Processor Agreement with OTS;<br />                 
				(bb) “<strong>Registration System</strong>” means the electronic database maintained by OTS in which                 
				registration and other information regarding Collectors is kept;<br />                 
				(cc) “<strong>Rejection Notice</strong>” has the meaning given in Section 13.6;<br />                 
				(dd) “<strong>Repayment Amounts</strong>” has the meaning given in Section 7.2;<br />                 
				(ee) “<strong>Report</strong>” means any report or submission made by the Collector from time to time                 
				regarding Used Tires;<br />                 
				(ff) “<strong>Subcontractor</strong>” has the meaning given in Section 2.3;<br />                 
				(gg) “<strong>Subcontracted Obligations</strong>” has the meaning given in Section 2.3;<br />                 
				(hh) “<strong>Used Tires</strong>” means used tires or parts of tires that that have not been refurbished for                 
				road use, or that, for any reason, are not suitable for their intended purpose; for greater                 
				clarity, “Used Tires” refers only to the tire body (or parts thereof), and does not include                 
				rims or any other component which is not an integral part of the tire body;<br />                 
				(ii) “<strong>Used Tire Pickup</strong>” means the retrieval of Used Tires from Collector by Registered                 
				Haulers and subsequent transport to a Registered Processor in accordance with this                 
				Agreement and the Plan; and<br />                 
				(jj) “<strong>WDO</strong>” means Waste Diversion Ontario.<br />                 
				<strong>                     ARTICLE 2<br />                     
				RESPONSIBILITIES OF COLLECTOR                 </strong><br />                 
				2.1 <u><strong>Responsibilities of Collector</strong></u>. In addition to the other obligations of the Collector set out in this                 
				Agreement, the Collector hereby agrees to:<br />                 
				(a) abide by the requirements set out in this Agreement, the Plan, and all Guidelines                 
				applicable to Collector;<br />                 
				(b) use the system of Guidelines and paper-based or electronic manifests and documents                 
				created by OTS to accurately, correctly and completely record and report all transactions                 
				involving Used Tires, as the system is modified by OTS from time to time in its sole                 
				discretion. Without limiting the generality of the foregoing, the Collector agrees and                 
				understands that it is required to maintain records of the Plan eligibility of all Used Tires.                 
				If such eligibility cannot be substantiated by other business records maintained by the                 
				Collector, the Collector may be required to obtain and record the names and contact                 
				information (addresses and telephone numbers) of any and all persons or companies from                 
				whom Used Tires are collected;<br />                 
				(c) retain in good order and legible or accessible condition all records required under any                 
				Guideline or which relate to Collector’s transfer or use of Used Tires, any Report made                 
				by Collector hereunder, and/or Collector’s activities to comply with the terms of this                 
				Agreement and the Plan, for a minimum of three years after such transfer, Report or                 
				activity;<br />                 
				(d) store all Used Tires in accordance with all requirements made from time to time by the                 
				Ontario Ministry of the Environment and the Ontario Office of the Fire Marshal;<br />                 
				(e) store all Used Tires in a manner that ensures they are free of foreign material and                 
				excessive moisture, secure, and accessible for efficient pickup;<br />                 
				(f) with the exception of Culled Used Tires, ensure that all eligible Used Tires in the                 
				possession of the Collector from time to time are released only to Registered Haulers;<br />                 
				(g) update any registration information provided to OTS in the Registration System as soon                 
				as possible after the information is changed;<br />                 
				(h) within five days after receiving a request from OTS, provide an Inventory Statement                 
				regarding Used Tires on the Collector’s premises and/or in the Collector’s inventory at                 
				the time of the request, in such format as OTS shall direct;<br />                 
				(i) submit to a mandatory yard count of all Used Tires in the Collector’s possession or on the                 
				Collector’s premises as and when directed by OTS or OTS’s designated representative(s);                 
				it is expressly agreed by the Collector that OTS is not required to provide the Collector                 
				with any advance notice of any such yard count;<br />                 
				(j) fully and properly complete required documents in preparation for transportation by                 
				Registered Hauler of Used Tires, in the manner directed by OTS from time to time;<br />                 
				(k) use equipment, supplies and service provided by OTS only for their intended purposes                 
				and in an efficient manner;<br />                 
				(l) file all required documents and reports in the manner directed by OTS from time to time;<br />                 
				(m) respond in a timely manner to all requests by OTS for information relating to Used Tires;<br />                 
				(n) comply, abide by and carry into effect, as may be required, the objectives of, and the                 
				obligations imposed upon the Collector contained in and set out in this Agreement, all                 
				applicable Guidelines, and the Plan;<br />                 
				(o) conduct itself in a professional and business-like manner in dealings with Registered                 
				Haulers, Registered Processors, members of the public and OTS;<br />                 
				(p) not use any trade-mark, trade name, or logo owned by OTS in any way not specifically                 
				authorised by OTS in writing, and to comply in all respects with any Guideline in effect                 
				from time to time regarding the use of OTS’s trade-marks;<br />                 
				(q) comply at all times with all laws issued by any government or governmental authority of                 
				Canada or any province of Canada, or any municipal, regional or other authority,                 
				including, without limitation, any governmental department, commission, bureau, board                 
				or administrative agency (“<strong>Applicable Laws</strong>”), which affect or govern the conduct and                 
				operation of the Collector, its business, and its performance under this Agreement; for                 
				greater clarity, the Applicable Laws include, without limitation, all laws relating to                 
				taxation, employment standards and compensation of workers, and the Environmental                 
				Laws;<br />                 
				(r) obtain and maintain all permits, certificates, licences and other qualifications required                 
				under any Applicable Law;<br />                 
				(s) provide notice to OTS of any fine or regulatory order made against Collector in the past                 
				five years relating to the substance of this Agreement or any aspect of the Plan; and<br />                 
				(t) provide notice to OTS within 60 days after any fine or regulatory order relating to the                 
				substance of this Agreement made against it after the date hereof.<br />                 
				2.2 <u><strong>Conflict Between Plan, Agreement, and Guidelines</strong></u>. To the extent any provision of the Plan                 
				may conflict with a term or terms of this Agreement or any Guideline, other than those dealing                 
				with the amount or payment of any incentive, then the Plan shall prevail. For greater clarity, in                 
				the event of any conflict between the Plan and any Guideline dealing with the amount or payment                 
				of any incentive, such Guideline shall prevail.<br />                 
				2.3 <u><strong>Subcontractors</strong></u>. The Collector agrees that if any third party (a “<strong>Subcontractor</strong>”) performs all or                 
				part of any of Collector’s obligations hereunder (“<strong>Subcontracted Obligations</strong>”) at any time,                 
				while this Agreement is in effect:<br />                 
				(a) Collector shall be solely responsible for ensuring that a Subcontractor complies fully with<br />                 
				all requirements set out in this Agreement, the Plan, and any Guidelines which are                 
				applicable to Subcontractor or any Subcontracted Obligations;<br />                 
				(b) Any act or omission by Subcontractor which would constitute a default if performed by                 
				the Collector shall constitute an actual default by the Collector; and<br />                 
				(c) Collector may not engage as a Subcontractor any person having any unresolved default(s)                 
				of which Collector is aware under any agreement with OTS; if Collector does engage                 
				such a person as a Subcontractor, the Collector shall become jointly and severally liable                 
				with such person for all amounts owed to OTS with respect to the default(s). For greater                 
				clarity, in addition to the actual knowledge of Collector from time to time, Collector shall                 
				be deemed to have awareness of any unresolved default included in any list published by                 
				OTS from time to time of persons in default of obligations to OTS.<br />                 
				2.4 <u><strong>Culled Tires</strong></u>.<br />                 
				(a) Collector may transfer, divert or use Used Tires for lawful purposes other than releasing                 
				them to Registered Haulers (such Used Tires referred to as “<strong>Culled Used Tires</strong>”), subject                 
				to the requirements set out in this Section 2.4.<br />                 
				(b) Collector agrees that all dealings in connection with Culled Used Tires, including without                 
				limitation the selection, use, or sale of Culled Used Tires, shall be at Collector’s own risk                 
				absolutely.<br />                 
				(c) Collector shall provide detailed information to OTS regarding the final destinations and                 
				end-uses of Culled Used Tires transferred, diverted, used, or otherwise dealt by the                 
				Collector, in the form required by OTS from time to time.<br />                 
				(d) No Collection Incentives shall be paid in respect of any Culled Used Tires, and Collector                 
				agrees that it shall not misrepresent any Culled Used Tires in any Report as being eligible                 
				for or actually having been retrieved by a Registered Hauler, and that submission of such                 
				a Report with respect to Culled Used Tires shall constitute a False Statement.<br />                 
				(e) Collector shall not sell, transfer or dispose of Culled Used Tires for any final purpose                 
				which is not an Approved Purpose, and which the Collector can demonstrate as such to                 
				OTS’s satisfaction, acting reasonably.<br />                 
				2.5 <strong><u>Non-Eligible Material</u></strong>.<br />                 
				(a) Except where OTS has issued an Exemption Order with respect thereto, no Collection                 
				Incentives shall be paid in respect of any Non-Eligible Material, and Collector agrees that                 
				it shall not request Used Tire Pickup with respect to any Non-Eligible Material, or                 
				misrepresent any Non-Eligible Material in any Report as being eligible for or actually                 
				having been subject to Used Tire Pickup, and that submission of such a Report with                 
				respect to Non-Eligible Material shall constitute a False Statement.<br />                 
				(b) Collector agrees that all its dealings in connection with Non-Eligible Material, including                 
				without limitation the selection, use, or sale of Non-Eligible Material, shall be at                 
				Collector’s own risk absolutely.<br />                 
				(c) Upon request by OTS, Collector shall provide detailed information to OTS regarding the                 
				final destinations and end-uses of Non-Eligible Material transferred, diverted, used, or                 
				otherwise dealt by the Collector, in the form required by OTS from time to time.<br />                 
				<strong>                     ARTICLE 3<br />                     
				REPRESENTATIONS AND WARRANTIES OF COLLECTOR                 </strong><br />                 
				3.1 <u><strong>Representations and Warranties</strong></u>. The Collector represents and warrants that:<br />                 
				(a) it is duly constituted and is validly existing and in good standing under the laws of its                 
				home jurisdiction, and has the necessary corporate or other powers, authority and                 
				capacity to own its property and assets and to carry on the business as presently                 
				conducted and contemplated by the terms of this Agreement;<br />                 
				(b) all information provided by it to OTS pursuant to this Agreement, including in all                 
				documents required by virtue of the Collector''s registration with OTS or by virtue of the                 
				requirements of law, are true and accurate;<br />                 
				(c) the registration of the Collector with OTS as an approved Collector, the provision of all                 
				required information to OTS, and the entering into of this Agreement by Collector and                 
				the performance of its obligation hereunder have been duly authorized by all necessary                 
				corporate action.<br />                 
				(d) it is not a non-resident of Canada within the meaning of Section 116 of the Income Tax                 
				Act;<br />                 
				(e) it holds all permits, licences, consents and authorities issued by all government or                 
				governmental authorities of Canada or any province of Canada, or any municipal,                 
				regional or other authority, including, without limitation, any governmental department,                 
				commission, bureau, board or administrative agency which are necessary and/or desirable                 
				in connection with the conduct and operation of the Collector’s business as it relates to                 
				any aspect of the Plan or this Agreement and is not in breach of or in default of any term                 
				or condition thereof;<br />                 
				(f) all Inventory Statements provided to OTS are true and accurate as of the date of the                 
				inventory; and<br />                 
				(g) all Reports, documentation and other instruments provided to OTS by the Collector are                 
				complete and correct.<br />                 
				3.2 <strong><u>Statements</u></strong>. All statements contained in any Reports, documents or other instruments delivered                 
				by or on behalf of the Collector to OTS shall be deemed to be representations and warranties of                 
				the Collector of the facts therein contained.<br />                 
				3.3 <u><strong>Reliance</strong></u>. The Collector acknowledges and agrees that OTS has entered into this Agreement                 
				relying on the warranties, representations and other terms and conditions set out in this                 
				Agreement notwithstanding independent searches or investigations that may have been                 
				undertaken by or on behalf of OTS and that no information which is now known or should be                 
				known or which may hereinafter become known to OTS or its officers, directors or professional                 
				advisors shall limit or extinguish any right of indemnification contained herein or otherwise limit,                 
				restrict, negate or constitute a waiver of any of the rights or remedies of OTS hereunder.<br />                 
				<strong>                     ARTICLE 4<br />                     AUDITS AND INSPECTION                 </strong><br />                 
				4.1 <u><strong>Inspection</strong></u>. The Collector agrees to permit OTS or its agents to inspect the Collector’s business                 
				site upon reasonable notice, during normal business hours, from time to time.<br />                 
				4.2 <u><strong>Audit</strong></u>. The Collector agrees that OTS may, from time to time, audit any records of the Collector                 
				maintained in support of the Collector’s claims, and further, may examine and review, and audit                
				 records relating to the Collector’s compliance with the terms of this Agreement, the Plan, and all                 
				 Applicable Laws, and in the course of doing so may review or inspect the Collector’s operations                 
				 to determine the Collector’s compliance (hereinafter referred to as the “Audit”).<br />                 
				 4.3 <u><strong>Yard Count</strong></u>. The Collector agrees that OTS or its OTS’s designated representative(s) may, from                 
				 time to time and without advance notice to the Collector, attend at the Collector’s premises to                 
				 perform a spot audit yard count of all Used Tires in the Collector’s possession or located on                 
				 Collector’s Premises, including Used Tires and other materials located in any vehicles, including                 
				 without limitation vehicles of a Registered Hauler or Registered Processor or any other person,                 
				 which are at Collector’s premises, and may also at this time review any records which OTS may                 
				 review in the course of an Audit as described in Section 4.2.<br />                 
				 4.4 <u><strong>Provision of Records</strong></u>. The Collector shall provide OTS’s auditor or designated representative                 
				 with any and all records requested and shall cooperate with the Audit at no expense to OTS. It is                 
				 agreed that OTS shall bear the cost of performing the Audit except in circumstances where the                 
				 auditor determines that the Collector has not complied with the terms of this Agreement and the                 
				 Plan in which case OTS’ reasonable costs of the Audit, including any follow-up review or                 
				 inspection undertaken as a result of a finding of non-compliance, shall be paid by the Collector                 
				 within 30 days of demand therefor being transmitted from OTS to the Collector. In the event the                 
				 Collector fails to pay OTS’ reasonable costs of the Audit as aforesaid, such costs of the Audit                 
				 shall be treated as Repayment Amounts and subject to set-off in accordance with Section 7.3.<br />                 
				 <strong>                     ARTICLE 5<br />                     OBLIGATIONS OF OTS                 </strong><br />                 
				 5.1 <u><strong>Used Tire Pickup</strong></u>. OTS shall allow and/or facilitate the retrieval of Used Tires from Collector                 
				 by Registered Haulers, at no charge for volumes of used tires greater than 50 PTE in for                 
				 Collectors in Southern Ontario and 75 PTE in Northern Ontario, under the Plan.<br />                 
				 5.2 <u><strong>Information and Support</strong></u>. OTS shall provide promotional and informational material and                 
				 telephone support to Collector, as OTS deems necessary.<br />                 
				 5.3 <u><strong>Collection Incentive</strong></u>. With respect to Used Tires retrieved from Collector by a Registered                 
				 Hauler and subsequently delivered to a Registered Processor, OTS shall provide a Collection                 
				 Incentive at such rate, to such person, and upon such schedule determined by OTS in its sole                 
				 discretion from time to time, all as may be published by OTS in one or more Guidelines from                 
				 time to time.<br />                 <strong>                     ARTICLE 6<br />                     
				 COLLECTION; PAYMENT OF INCENTIVES<br />                 
				 </strong>6.1 <u><strong>Pickup and Haulage</strong></u>.<br />                 
				 (a) In arranging any Used Tire Pickup, the Collector must attempt to make its own                
				  arrangements with a Registered Hauler for Used Tire Pickup; Collector must contact at                 
				  least three Registered Haulers before contacting OTS to facilitate Used Tire Pickup.<br />                 
				  (b) If the Collector is unable to arrange Used Tire Pickup by a Registered Hauler, the                 
				  Collector may request the assistance of OTS, and OTS shall facilitate retrieval of Used                 
				  Tires by a Registered Hauler to occur within five days of receiving a request for Used                 
				  Tire Pickup from Collector.<br />                 
				  (c) In Southern Ontario, Collector may request OTS facilitate the pick-up of Used Tire                 
				  Pickup only for lots of 50 Used Tires or more; in Northern Ontario, Collector may                 
				  request OTS facilitation of Used Tire Pickup only for lots of 75 Used Tires or more.<br />                 
				  (d) Notwithstanding any other provision of this Agreement, if the Collector commits an Act                 
				  of Default, then until the resolution of such Act of Default and for a period of [two years]                 
				  thereafter, OTS may in its sole discretion require that all Used Tire Pickup from Collector                 
				  be arranged through OTS.<br />                 
				  6.2 <strong>No Additional Fees</strong>. Collector may not charge additional fees, such as “tire recycling” or                 
				  “environmental” fees, to motor vehicle owners after Used Tires are removed from the motor                 
				  vehicle, or upon receipt of used tires from a consumer, provided the quantity of tires delivered by                 
				  the consumer does not exceed four (4) tires. Collector acknowledges and agrees that OTS will                 
				  advise members of the public of the requirement of this section, and that OTS will establish                 
				  mechanisms to enable members of the public to advise OTS of any breach of the requirements of                 
				  this section.<br />                 
				  6.3 <u><strong>Agreements between Collectors, Registered Haulers and/or Registered Processors</strong></u>.                 
				  Notwithstanding any other provision of this agreement, Collector may in its own right enter into                 
				  one or more contract(s) with Registered Haulers and/or Registered Processors:<br />                 
				  (a) regarding the retrieval by such Registered Hauler(s) and/or delivery to such Registered                 
				  Processor(s) of tires which are not Used Tires as defined in the Plan, or delivery                 
				  schedules which are different from those contemplated hereunder; such contracts may                 
				  provide for fees charged by or to Collector with respect to the services described in such                 
				  contracts; and/or<br />                 (b) containing additional terms as between Collector and such Registered Hauler(s) and/or                 
				  Registered Processor(s) regarding Used Tire Pickup; such contracts may provide for                 payments made by or to the Collector.<br />                
				   6.4 <u><strong>Third Party Monies</strong></u>. The Collector shall not collect monies on behalf of OTS from any other                 
				   person but in the event that such does occur notwithstanding the requirements of this section,                 
				   those monies shall be held for the benefit of, and remitted (without deduction or set-off),                
				    forthwith to OTS.<br />                 <strong>                     ARTICLE 7<br />                     COLLECTOR DEFAULT                 </strong><br />                 
					7.1 <u><strong>Events of Default</strong></u>. The occurrence of any of the following while this Agreement is in effect shall                
					 constitute an “<strong>Act of Default</strong>” by the Collector under this Agreement:<br />                
					  (a) If the Collector fails to make payment of any amount required in this Agreement when                 
					  such payment becomes due and payable, and fails to pay such amount in full within five                 
					  days of written demand therefor being sent by OTS;<br />                 
					  (b) If Collector breaches or fails to perform, observe or comply with any provision of this                 
					  Agreement, the Plan, or any Guideline, and does not rectify such breach or failure to                
					   OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure                
					    being sent by OTS;<br />                 
					   (c) If Collector defaults in the due observance or performance of any covenant, undertaking,                 
					   obligation or agreement given to OTS at any time, whether contained in this Agreement,                 
					   the Plan, or any Guideline, or not, and Collector does not rectify such default to OTS’s                
					    reasonable satisfaction within 15 days of written notice of the breach or failure being sent                 
						by OTS;<br />                 
						(d) If Collector fails to submit any document or Report required under this Agreement or any                 
						Guideline, or to maintain records as required under this Agreement or any Guideline;<br />                
						 (e) If any Report, representation, warranty, claim, certificate, submission, or statement made                
						  by Collector to OTS is in any respect untrue, erroneous, incomplete, inaccurate,                 
						  misleading, or not able to be supported by Collector’s records in an Audit or the results of                
						   a yard count pursuant to Section 4.3, all in OTS’s sole determination (each a “False                 
						   Statement”);<br />                 
						   (f) If Collector commits any default or breach under any other agreement between Collector                 
						   and OTS;<br />                 
						   (g) If Collector engages any Subcontractor who has an unresolved default contrary to Section<br />                 
						   2.3(c);<br />                 
						   (h) If Collector conspires or colludes with or assists any other person in making any False                 
						   Statement to OTS or obtain under false pretenses the payment of any amount from OTS;<br />                 
						   (i) If Collector fails to comply with any applicable law affecting the Collector’s operation;<br />                 
						   (j) If Collector is convicted of an offense under the Environmental Protection Act (Ontario);<br />                 
						   (k) If Collector becomes insolvent or bankrupt or subject to the Bankruptcy and Insolvency                 
						   Act (Canada) or the Companies Creditors Arrangement Act (Canada), or goes into                 
						   winding-up or liquidation, either voluntarily or under an order of a court of competent                 
						   jurisdiction, or makes a general assignment for the benefit of its creditors or otherwise                 
						   acknowledges itself insolvent;<br />                 
						   (l) If any execution, sequestration, extent, or any other process of any court becomes                 
						   enforceable against Collector or if a distress or analogous process is levied on the                 
						   property and assets of the Collector; or<br />                 
						   (m) any proceedings shall be commenced for the winding-up, dissolution or liquidation of the                 
						   Collector or under which the Collector could lose its corporate status, such proceedings                
						    not being bona fide opposed by the Collector within five days of the date of                 
							commencement or service on the Collector.<br />                 
							7.2 <u><strong>Repayment of Payments Based on False Statements</strong></u>. Collector acknowledges and agrees that :<br />                 
							(a) if any Collection Incentives are paid to Collector as a result of or in connection with any                 
							False Statement of the Collector or any other person, in OTS’s sole determination,                 
							Collector shall forthwith upon demand repay the full amount of such payments to OTS,                 
							together with an amount equal to 10 per cent thereof as a reasonable pre-estimate of                 
							OTS’s liquidated damages and administrative expenses arising therefrom; and<br />                 
							(b) if any payment or incentive of any kind, including without limitation any Collection                 
							Incentive, is paid by OTS to any third party as a result of or in connection with any False                 
							Statement of the Collector, in OTS’s sole determination, Collector shall forthwith upon                 
							demand reimburse OTS for the full amount of such payments, together with an amount                 
							equal to 10 per cent thereof as a reasonable pre-estimate of OTS’s liquidated damages                 
							and administrative expenses arising therefrom.<br />                 
							Amounts payable by Collector in accordance with this Article are collectively such amounts                 
							“<strong>Repayment Amounts</strong>”. OTS may, its sole discretion, waive all or a portion of any Repayment                 
							Amount.<br />                 
							7.3 <u><strong>Set-off</strong></u>. Collector agrees that OTS may set off Repayment Amounts owing by Collector to OTS                 
							against any future payments of Collection Incentives with respect to Used Tire Pickup from                 
							Collector, or any other amount of any kind to Collector under this Agreement or any other                 
							agreement between Collector and OTS. If such future payment amounts are insufficient to                 
							recoup Repayment Amounts owed by Collector to OTS, Collector agrees that such Repayment                 
							Amounts are recoverable from Collector as liquidated damages.<br />                 
							7.4 <u><strong>Consequences of Default</strong></u>. Immediately following any Act of Default, OTS may, in its sole and                 
							absolute discretion, do any, some or all of the following by written notice to the Collector:<br />                 
							(a) suspend payment of all incentives and other amounts hereunder, with immediate effect,                 
							until the Act of Default is resolved to OTS’s satisfaction;<br />                 
							(b) require Collector to pay any Repayment Amount arising in connection with an Act of                 
							Default;<br />                 
							(c) terminate this Agreement, with immediate effect;<br />                 
							(d) include the Collector on a published list of persons having unresolved defaults under                 
							agreements with OTS, until the Act of Default is resolved to OTS’s satisfaction; or<br />                 
							(e) exclude Collector from future participation in the Used Tires Program on a temporary or<br />                 
							permanent basis.<br />                 
							<strong>                     ARTICLE 8<br />                     
							TERM &amp; TERMINATION                 </strong><br />                 
							8.1 <u><strong>Term</strong></u>. This Agreement shall commence on the Effective Date and continue thereafter until                 
							terminated as provided for herein.<br />                
							 8.2 <u><strong>Termination by OTS</strong></u>. OTS may immediately terminate this Agreement by written notice to the                 
							 Collector, in addition to any other remedies available at law or in equity, in any of the following                 
							 events:<br />                 
							 (a) if the collector commits an Act of Default;<br />                 
							 (b) if the Plan is terminated by the Minister or any other governmental authority, or the                 
							 program agreement between OTS and WDO is terminated, provided that prior notice of                 
							 such termination is communicated to the Collector as soon as it is available;<br />                 
							 (c) if the Collector transfers by sale, assignment, bequest, inheritance, by operation of law or                 
							 other disposition, or shares issued by subscription or allotment, or shares cancelled or                 
							 redeemed, so as to result in a change in the effective voting or other control of the                 
							 Collector from the person or persons holding control on the date of execution of this                 
							 Agreement without the written consent of OTS, such consent not to be unreasonably                 
							 withheld; or<br />                 
							 (d) in the event any other legal proceeding involving the Collector is instituted that in the                 
							 reasonable opinion of OTS materially impairs the ability of the Collector to discharge its                 
							 obligations hereunder.<br />                 
							 8.3 <u><strong>Termination for Convenience</strong></u>. Either Party may terminate this Agreement for convenience                 
							 upon 90 days’ written notice to the other Party.<br />                 
							 8.4 <strong><u>Reports Following Termination</u></strong>. Following Termination, Collector agrees that it will submit                 
							 any Reports required hereunder with respect to any Used Tire Pickup occurring before the                 
							 Termination of this Agreement.<br />                 
							 8.5 <strong>Incentive Payment Following Termination</strong>.<br />                 
							 (a) In the event of termination by OTS where Collector has committed an Act of Default,                 
							 OTS may in its sole and absolute discretion cancel all payments of Collection Incentives                 
							 which are pending as of the date on which notice of termination is given or which may                 
							 arise at any time thereafter.<br />                 
							 (b) Upon termination of this Agreement, provided that payment has not been suspended by                
							  OTS in accordance with Section 7.4, OTS shall continue to pay Collection Incentives                 
							  with respect to services performed before the termination of this Agreement                 
							  (notwithstanding that claims for such services may be submitted to OTS after or                 
							  termination of this Agreement).<br />                 <strong>                     ARTICLE 9<br />                     
							  INDEMNITY &amp; INSURANCE                 </strong><br />                 
							  9.1 <u><strong>Indemnity</strong></u>. The Collector covenants and agrees with OTS to indemnify and hold harmless OTS,                 
							  its directors, officers, employees and agents against all costs, charges, expenses, legal fees and                 
							  any other losses or claims which OTS may hereinafter suffer, sustain or may incur or be                 
							  compelled to pay as a result of any performance or non-performance by Collector of its                 
							  obligations hereunder, or any claim, action or proceeding which is brought, prosecuted or                 
							  threatened against OTS, its directors, officers, employees and agents for any act, deed or omission                 
							  of the Collector related in any way to Culled Used Tires or arising from the breach of this                 
							  Agreement, the Plan, or any applicable law.<br />                 
							  9.2 <u><strong>Release</strong></u>. The Collector, for itself, its successors and assigns, agrees to release OTS and its                 
							  officers, directors, employees and agents from all manners of action, causes of action, claims,                 
							  demands, losses, damages, charges, expenses and the like, of any nature whatsoever which the                 
							  Collector ever had, now has or hereafter can, shall or may have against OTS and its officers,                 
							  directors, employees and agents related in any way to Culled Used Tires or arising out of or in                 
							  connection with this Agreement provided that all acts, deeds or omissions or the alleged acts,                 
							  deeds or omissions in respect of which any action, cause of action, claim, demand, loss, damage,                 
							  charge, expense and the like is based or performed in good faith, and when not performed or                 
							  omitted to be performed fraudulently or in bad faith by OTS, its directors, officers, employees or                 agents.<br />                 
							  9.3 <u><strong>Insurance</strong></u>. Collector shall maintain comprehensive “occurrence” general liability insurance,                 
							  including personal injury liability, property damage, contractual liability insurance and                 
							  employer’s liability coverage, with minimum limits of liability of $1,000,000, containing a                 
							  severability of interests and cross-liability clause, and deliver to OTS on request a certificate of                 
							  insurance thereof.<br />                 <strong>                     ARTICLE 10<br />                     
							  NO OTS LIABILITY FOR USED TIRES                 </strong><br />                 
							  10.1 <u><strong>Exclusion of Liability</strong></u>. Collector acknowledges and agrees that at no time shall OTS take                 
							  possession of any Used Tires and that OTS shall not, in any event, be liable under any theory of                 liability to Collector, the previous owner(s) or user(s) of any Used Tires or any other party or                 parties for any damages, losses, expenses, liabilities and/or other amounts of any nature or kind                 whatsoever, including without limitation, any direct, indirect, incidental, special, consequential,                 exemplary and/or punitive damages, arising out of or related to any loss, improper use, improper                 culling, improper transfer or sale, improper disposal or environmental degradation resulting,                 proceeding or connected in any way to Used Tires.<br />                 <strong>                     ARTICLE 11<br />                     PUBLICATION OF INFORMATION                 </strong><br />                 11.1 <u><strong>Publication of Information</strong></u>. The Collector understands that its name, main contact information,                 and the registration number assigned to it by OTS, as well as information regarding the                 Collector’s operation, may be published by OTS on OTS''s website or other publically-accessible                 websites. OTS will take commercially reasonable and appropriate precautions to maintain the                 confidentiality of information in its database, but will not be liable to the Collector, or anyone                 claiming by, through or under it for any losses, claims and damages arising out of negligent                 disclosure of any confidential information.<br />                 11.2 <u><strong>Release of Information Following Act of Default</strong></u>. The Collector agrees that, in the event the                 Collector commits an Act of Default, OTS may publish its name and registration number on a list                 of persons with unresolved defaults, as described in Section 7.4(d), and may release details of the                 Act of Default to any Program Participant who may be affected thereby.<br />                 <strong>                     ARTICLE 12<br />                     MODIFICATIONS TO PLAN                 </strong><br />                 12.1 <u><strong>Modifications to Plan</strong></u>. The parties agree and understand that the Plan may be revised from time                 to time without the input or consent of the Collector, and the Collector shall be bound by each                 revised version of the same as each revision may be issued, as though each was set out herein and                 formed a contractual obligation upon the Collector and the Collector covenants and agrees to                 abide by, comply with and satisfy such revised Plan.<br>                 12.2 <u><strong>Notice</strong></u>. In the event of the Plan or any part of it being cancelled or altered, then OTS shall issue                 notice to that effect.<br />                 12.3 <u><strong>Modification of Incentives</strong></u>. The incentives payable and the payment schedule implemented by                 OTS may be modified from time to time. All changes will be posed on OTS’s internet web site                 no less than 60 days before the effective date of such change.<br />                 <strong>                     ARTICLE 13<br />                     GENERAL                 </strong><br />                 13.1 <u><strong>Assignment</strong></u>. The parties hereby agree that the Collector’s rights under this Agreement are not                 assignable or transferable, in any manner, without the prior written consent of OTS, which                 consent may not be unreasonably withheld.<br />                 13.2<u><strong> Agreement Binding</strong></u>. This Agreement shall enure to the benefit of and be binding on the parties,                 their heirs, legal personal representatives, successors and permitted assigns.<br />                 13.3 <u><strong>Dispute Resolution</strong></u>. The parties agree that in the event of a dispute between them with respect to                 the terms or performance of this Agreement then such dispute shall first be subject to Mediation                 under Appendix 12 in the Plan, “<strong>Mediation Guidelines</strong>”, and if such dispute is not able to be                 resolved through mediation, then it shall be subject to arbitration under Appendix 13 in the Plan,                 “<strong>Arbitration Guidelines</strong>”.<br />                 13.4 <u><strong>Notices</strong></u>. Any notice, determination, consent, request or other communication from one party to                 the other or others or other documents required or which may be given under this Agreement may                 be delivered or transmitted by means of electronic communication with confirmation of                 transmission, personal service, facsimile with confirmation of transmission or by prepaid first                 class postage to the party at the addresses, in the case of the Collector at the address on the                 registration form completed by the Collector and in the case of OTS at the address noted at the                 top of page 1 of this Agreement, to the attention of the “Executive Director”. Such notifications                 shall be deemed to have been received on the third day after posting and on the first day after the                 date of electronic or facsimile transmission, in each case which is not a Saturday, Sunday or                 public holiday in Ontario. In the event of a postal disruption, notices must be given by personal                 delivery, e-mail or by a signed back facsimile and all notices delivered by post within one week                 prior to the postal disruption must be confirmed by a signed back facsimile to be effective.<br />                 13.5 <u><strong>Independent Contractors</strong></u>. The Parties hereto are and shall at all times in the performance of                 this Agreement be independent contractors and neither Party shall have the authority to assume or                 create any obligations expressed or implied, in the name of the other Party, nor to contractually                 bind the other Party in any manner whatsoever.<br />                 13.6 <u><strong>Amendment</strong></u>. OTS retains the right to revise or amend this Agreement. OTS will give notice to                 the Collector of such change (the “<strong>Change Notice</strong>”). Unless the Collector gives notice to OTS                 (the “<strong>Rejection Notice</strong>”) within 45 days of receipt of the Change Notice that the Collector does                 not accept the revisions or amendments in the Change Notice, this Agreement, as amended,                 remains in effect and is binding. If the Collector gives a Rejection Notice to OTS, this Agreement                 shall be terminated 30 days after the delivery by the Collector of the Rejection Notice and the                 Collector will forgo its approval status and not be compensated under the OTS program.<br />                 13.7 <u><strong>Waiver</strong></u>. No failure by any of the parties to insist on strict performance of any covenant,                 agreement, term or condition (the “<strong>provision</strong>”) of this Agreement, or to exercise any right or                 remedy consequent on the breach of any provision, and no acceptance of partial payment during                 the continuance of any such breach, shall constitute a waiver of any such breach or provision. No                 waiver of any breach shall affect or alter this Agreement, but each and every provision of this                 Agreement shall continue in full force and effect with respect to any other then existing or                 subsequent breach of such provision.<br />                 13.8 <u><strong>Severability</strong></u>. If any provision of this Agreement or the application of the provision to any                 circumstances shall be held to be invalid or unenforceable, then the remaining provisions of this                 Agreement or the application of them to other circumstances shall not be affected by the                 invalidity or unenforceability and shall be valid and enforceable to the fullest extent permitted by                 law.<br />                 13.9 <u><strong>Entire Agreement</strong></u>. This Agreement constitutes the entire agreement among the parties with                 respect to its subject matter and supersedes all prior and contemporaneous agreements,                 understandings, negotiations and discussions, whether oral or written, of the parties. There are no                 warranties, representations or other agreements among the parties in connection with the subject                 matter of this Agreement, except as specifically set forth in it. Except as specifically provided                 herein, no supplement, modification, waiver or termination of this Agreement shall be binding                 unless executed in writing by the parties to be bound by it.<br />                 13.10 <u><strong>Remedies</strong></u>. No remedy herein conferred upon or reserved in favour of any party hereto shall                 exclude any other remedy herein or existing at law or in equity or by statute, but each shall be                 cumulative and in addition to every other remedy given hereunder or now or hereinafter existing.<br />                 13.11 <u><strong>Force Majeure</strong></u>. Neither party shall be liable for delay or failure in performance resulting from                 acts beyond the control of such party, including but not limited to Acts of God, acts of war, riot,                 fire, flood or other disaster, acts of government, strike, lockout or communication line or power                 failure.<br />                 13.12 <u><strong>Governing Law</strong></u>. This Agreement shall be construed and enforced in accordance with, and the                 rights of the parties shall be governed by, the laws in force in the Province of Ontario.<br />                 13.13 <u><strong>Headings</strong></u>. The headings used throughout this Agreement are solely for convenience of the parties                 and are not to be used as an aid in the interpretation of this Agreement.<br />                 13.14 <u><strong>Time of Essence</strong></u>. Time shall be of the essence of this Agreement and every part of it.<br />                 13.15 <u><strong>Survival</strong></u>. All provisions of this Agreement which are expressed or which by their nature are                 intended to survive termination of this Agreement shall survive termination, and continue to bind                 the parties.<br />                 13.16 <u><strong>Electronic Commerce</strong></u>. Any execution or amendment of this agreement which is conducted                 electronically by any of the parties is made in accordance with and governed by the Electronic                 Commerce Act, 2000, (Ontario). If this Agreement is executed on behalf of any party                 electronically, the natural person who selects the “Agree” button at the bottom of the “Agreement                 Ratification” page on OTS''s web site on behalf of the executing party certifies that by selecting                 the “Agree” button, the natural person represents and warrants that he or she is at least eighteen                 years of age, and has been duly appointed with the authority to bind the executing party.<br />             </p>
	'),

	(3,  '2017-09-19',@userID,	'2017-09-19','9999-12-31','
<p>RECITALS:<br /> 
A.  OTS has been designated as the industry funding organization under the Act to be responsible for the collection and environmentally responsible recycling of Used Tires;<br /> 
B.  The Hauler wishes to operate as a hauler of Used Tires and become entitled to retrieve Used Tires from Registered Collectors at no charge, and to deliver such Used Tires to 
Registered Processor at no charge, in accordance with the Plan;<br /> 
C.  The Hauler has been approved by OTS as an approved hauler in accordance with the Plan; and<br /> 
D.  The purpose of this Agreement is to set out the terms and conditions under which the Hauler will operate as an approved hauler under the Plan.<br /> 
NOW THEREFORE the parties agree as follows, as of the date set out above:<br /><br /> ARTICLE 1<br /> DEFINITIONS<br /> 
1.1  Definitions. In addition to the words and phrases defined in the recitals or elsewhere in this Agreement, as used in this Agreement, in any schedule hereto, 
in any amendment hereof, and in any documents to be executed and delivered pursuant to this Agreement, the following words and phrases have the following meanings, respectively:<br /> 
(a)  “Act” means the Waste Diversion Act, 2002, S.O. 2002 c.6 as amended from time to time;<br /> 
(b)  “Act of Default” has the meaning given in Section 7.1;<br /> 
(c)  “Agreement” means this Agreement, including the schedules to this Agreement, as it or they may be amended or supplemented from time to time, and the expressions “hereof”, “herein”,
 “hereto”, “hereunder”, “hereby” and similar expressions refer to this Agreement and not to any particular section of other portion of this Agreement;<br /> 
 (d)  “Applicable Laws” has the meaning given in Section 2.1(r);<br /> (e)  “Approved Purpose” means a purpose for the use of recycled tires which is found on the list of Approved Purposes
  for Recycled Tires maintained by OTS and published on OTS’s web site from time to time;<br /> (f)  “Arbitration Guidelines” has the meaning given in Section 13.3;<br /> 
  (g)  “Audit” has the meaning given in Section 4.2;<br /> (h)  “Change Notice” has the meaning given in Section 13.6;<br /> (i)  “Culled Used Tires” has the meaning given in Section 2.4;<br /> 
  (j)  “Effective Date” “ has the meaning given to that term in the listing of parties to this Agreement;<br /> (k)  “Environmental Laws” means any and all applicable laws, statutes, regulations, 
  treatise, orders, judgements, decrees, official directives and all authorizations of any department or body of any federal, provincial, regional or municipal government of any agency thereof 
  relating to the protection of the environment, including in particular, but without limiting the generality of the foregoing, the manufacture, use, storage, disposal and transportation of any
   Hazardous Substance;<br /> (l)  “Exemption Order” means a written confirmation issued by OTS that certain Used Tires, tire parts or processed rubber which would otherwise be Non-Eligible Material, 
   are eligible for the claiming of certain financial incentives, on the terms set out therein;<br /> (m)  “False Statement” has the meaning given in Section 7.1(d);<br /> 
   (n)  “Guidelines” means any directives, forms, procedure manuals, administrative guidance, or other document regarding the implementation of the Plan published by OTS from time to time on its web site;<br />
    (o)  “Hauler” has the meaning given to that term in the listing of parties to this Agreement;<br /> 
	(p)  “Hazardous Substance” includes any contaminant, pollutant, dangerous substance, liquid waste, industrial waste, hauled liquid waste, toxic substance, hazardous waste, hazardous material, 
	or hazardous substance as defined in or pursuant to any law, judgement, decree, order, injunction, rule, statute or regulation of any court, arbitrator or federal, provincial, state, municipal, 
	county or regional government or governmental authority, domestic or foreign, or any department, commission, bureau, board, administrative agency or regulatory body of any of the foregoing to which the Hauler is subject;<br /> 
	(q) “Inventory Statement” means any report submitted by the Hauler with regard to the inventory of Used Tires and/or parts thereof held on the Hauler’s premises and/or in the Haulers’s inventory, as at a certain date;<br /> 
	(r)  “Mediation Guidelines” has the meaning given in Section 13.3;<br /> (s)  “Minister” means the Minister of the Environment for Ontario;<br /> (t)  “Non-Eligible Material” means:<br /> 
	(i)  Used Tires, tire parts or processed rubber held in Hauler’s inventory prior to September 1st, 2009, unless the subject of an Exemption Order issued by OTS; and/or<br /> 
	(ii)  Used Tires or parts thereof originating outside Ontario or obtained by the Hauler directly or indirectly from any source outside Ontario; and/or<br /> 
	(iii)  any material, article or item that is not a Used Tire or part thereof;<br /> (u)  “OTS” has the meaning given to that term in the listing of parties to this Agreement;;<br /> 
	(v)  “Party” means a party to this Agreement and any reference to a Party includes its successors and permitted assigns; and Parties means every Party;<br /> 
	(w)  “Plan” means the Used Tires Program Plan submitted by OTS on 27 February 2009 and approved by Waste Diversion Ontario and the Minister of Environment, as amended;<br />
	(x)  “Processor Delivery” means delivery by the Hauler of eligible Used Tires to a Registered Processor, in accordance with the Plan and this Agreement;<br /> 
	(y)  “Program Participant” means any collector, hauler, or processor which has registered and entered into an agreement with OTS to provide services under the Plan;<br />
	(z)  “Registered Collector” means an entity that has registered and remained in good standing with OTS and entered into a Collector Agreement with OTS;<br /> 
	(aa)  “Registered Processor” means a business that processes Used Tires into material that can be further processed in order to recover specific components within the same organization
	or sent to downstream processors for use as a raw material in another process, and which has registered and remains in good standing with OTS and entered into a Processor Agreement with OTS;<br /> 
	(bb)  “Registration System” means the electronic database maintained by OTS in which registration and other information regarding Haulers is kept;<br /> 
	(cc)  “Rejection Notice” has the meaning given in Section 13.6;<br /> (dd)  “Repayment Amounts” has the meaning given in Section 7.2;<br /> 
	(ee)  “Report” means any report or submission made by the Hauler from time to time regarding Used Tires;<br /> (ff)  “Subcontractor” has the meaning given in Section 2.3;<br /> 
	(gg)  “Subcontracted Obligations” has the meaning given in Section 2.3;<br /> 
	(hh)  “Transportation Incentives” means those certain financial incentives from time to time determined and payable by OTS, to a person designated by OTS from time to time;<br />
	(ii)  “Used Tires” means used tires or parts of tires that that have not been refurbished for road use, or that, for any reason, are not suitable for their intended purpose;
	for greater clarity, “Used Tires” refers only to the tire body (or parts thereof), and does not include rims or any other component which is not an integral part of the tire body;<br /> 
	(jj)  “Used Tire Pickup” means the retrieval of eligible Used Tires from Registered Collectors by the Hauler, in accordance with the Plan and this Agreement; and<br /> 
	(kk)  “WDO” means Waste Diversion Ontario.<br /> 1.2  Interpretation. Unless otherwise specified, all references to currency herein shall be to lawful money of Canada. Headings,
	table of contents, and Article and Section names and divisions are for convenience of reference only and shall not affect the meaning or interpretation of the Agreement.
	Any accounting terms not specifically defined shall have the meanings ascribed to them in accordance with Canadian generally accepted accounting principles. Unless the context 
	requires otherwise, words importing the singular include the plural and vice versa and words importing gender include all genders. The words “hereto”, “herein”, “hereof”, “hereunder”, 
	“this Agreement” and similar expressions mean and refer to this Agreement. All references to laws or statutes include all related rules, regulations, interpretations, amendments and successor
	provisions. All references to any document, instrument or agreement include any amendments, waivers and other modifications, extensions or renewals. The words “includes” or “including” shall 
	mean “includes without limitation” or “including without limitation”, respectively.<br /> 1.3  Agreements as Covenants. Each agreement and obligation of any of the Parties hereto in this 
	Agreement even though not expressed as a covenant, is considered for all purposes to be a covenant.<br /><br /> ARTICLE 2<br /> RESPONSIBILITIES OF HAULER<br /> 
	2.1  Responsibilities of Hauler. In addition to the other obligations of the Hauler set out in this Agreement, the Hauler hereby agrees to:<br /> 
	(a)  abide by the requirements set out in this Agreement and its schedules, the Plan, and all Guidelines applicable to Hauler;<br /> 
	(b)  use the system of Guidelines and paper-based or electronic manifests and documents created by OTS to accurately, correctly, and completely record and report all transactions involving Used Tires, as the system is modified by OTS from time to time in its sole discretion;<br /> (c)  retain in good order and legible or accessible condition all records required under any Guideline or which relate to Haulers’s transfer or use of Used Tires, any Report made by Hauler hereunder, and/or Hauler’s activities to comply with the terms of this Agreement and the Plan, for a minimum of three years after such transfer, Report or activity;<br /> 
	(d)  retrieve Used Tires from registered Collectors, and deliver all Used Tires which are not culled by the Hauler to registered Processors, as directed by OTS;<br />
	(e)  store all Used Tires with all requirements of Applicable Law, including without limitation all requirements of the Ontario Ministry of the Environment and the Ontario Office of the Fire Marshal;<br />
	(f)  update any registration information provided to OTS in the Registration System within 10 business days after the information is changed;<br /> 
	(g)  fully and properly complete required manifests and other documents in preparation for transportation of Used Tires, in the manner directed by OTS from time to time;<br /> 
	(h)  within five days after receiving the request from OTS, provide an Inventory Statement regarding Used Tires on the Hauler’s premises and/or in the Hauler’s inventory at the time of the request, in such format as 
	OTS shall direct;<br /> (i)  submit to periodic inspections of the Hauler’s premises and equipment by OTS or OTS’s designated representative, at intervals which are reasonable in the sole judgement of OTS;<br />
	(j)  submit to a mandatory yard count of all Used Tires in the Hauler’s possession or on the Hauler’s premises or trucks or other vehicles as and when directed by OTS or OTS’s designated representative(s); 
	it is expressly agreed by the Hauler that OTS is not required to provide the Hauler with any advance notice of any such yard count;<br /> 
	(k)  submit to an inspection and inventory count of the Used Tires in any of its trucks or other vehicles being used to transport Used Tires under this Agreement by OTS or OTS’s designated representative(s)
	including field agents, whether such truck or other vehicle is stationary or in the process of transporting Used Tires; it is expressly agreed by the Hauler that OTS is not required to provide the 
	Hauler with any advance notice of any such inspection or inventory count;<br /> (l)  use equipment, supplies and service provided by OTS only for their intended purposes and in an efficient manner;<br /> 
	(m)  file all required documents and reports in the manner directed by OTS from time to time;<br /> (n)  respond in a timely manner to all requests by OTS for information relating to Used Tires;<br /> 
	(o)  comply, abide by and carry into effect, as may be required, the objectives of, and the obligations imposed upon the Hauler contained in and set out in this Agreement, all applicable Guidelines, and the Plan;<br /> 
	(p)  conduct itself in a professional and business-like manner in dealings with Registered Collectors, Registered Processors, members of the public and OTS;<br /> 
	(q)  not use any trade-mark, trade name, or logo owned by OTS in any way not specifically authorized by OTS in writing, to provide samples of any proposed use to OTS for written approval which must be
	received before use begins, and to comply in all respects with any Guideline in effect from time to time regarding the use of OTS’s trade-marks;<br /> (r)  comply at all times with all laws issued by
	any government or governmental authority of Canada or any province of Canada, or any municipal, regional or other authority, including, without limitation, any governmental department, commission, bureau,
	 board or administrative agency (“Applicable Laws”), which affect or govern the conduct and operation of the Hauler, its business, and its performance under this Agreement; for greater clarity, the Applicable Laws 
	 include, without limitation, all laws relating to taxation, transportation and motor vehicles, employment standards and compensation of workers, and the Environmental Laws;<br /> 
	 (s)  obtain and maintain all permits, certificates, licences and other qualifications required under any Applicable Law;<br /> 
	 (t)  provide notice to OTS of any fines or regulatory orders relating to the Hauler’s business made against it in the previous five years; and<br /> 
	 (u)  provide notice to OTS within 24 hours after any fine or regulatory order relating to the substance of this Agreement made against it after the date hereof.<br /> 
	 2.2  Conflict Between Plan, Agreement, and Guidelines. To the extent any provision of the Plan may conflict with a term or terms of this Agreement or any Guideline, other than those dealing with the amount or payment 
	 of any incentive, then the Plan shall prevail. For greater clarity, in the event of any conflict between the Plan and any Guideline dealing with the amount or payment of any incentive, such Guideline shall prevail.<br /> 
	 2.3  Subcontractors. The Hauler agrees that if any third party (a “Subcontractor”) performs all or part of any of Hauler’s obligations hereunder (“Subcontracted Obligations”) at any time, while this Agreement is in effect:<br /> 
	 (a)  Hauler shall be solely responsible for ensuring that a Subcontractor complies fully with all requirements set out in this Agreement, the Plan, and any Guidelines which are applicable to Subcontractor or 
	 any Subcontracted Obligations;<br /> (b)  Any act or omission by Subcontractor which would constitute a default if performed by the Hauler shall constitute an actual default by the Hauler; and<br /> 
	 (c)  Hauler may not engage as a Subcontractor any person having any unresolved default(s) of which Hauler is aware under any agreement with OTS; if Collector does engage such a person as a Subcontractor, 
	 the Collector shall become jointly and severally liable with such person for all amounts owed to OTS with respect to the default(s). For greater clarity, in addition to the actual knowledge of Hauler from time to time, 
	 Hauler shall be deemed to have awareness of any unresolved default included in any list published by OTS from time to time of persons in default of obligations to OTS.<br /> 2.4  Culled Tires.<br /> 
	 (a)  Hauler may transfer, divert or use Used Tires for lawful purposes other than transporting them to Registered Processors (such Used Tires referred to as “Culled Used Tires”), subject to the requirements 
	 set out in this Section 2.4.<br /> (b)  Hauler agrees that all dealings in connection with Culled Used Tires, including without limitation the selection, use, or sale of Culled Used Tires, shall be at Hauler’s 
	 own risk absolutely.<br /> (c)  Hauler shall provide detailed information to OTS regarding the final destinations and end-uses of Culled Used Tires transferred, diverted, used, or otherwise dealt by the Hauler, 
	 in the form required by OTS from time to time.<br /> (d)  No Transportation Incentives shall be paid in respect of any Culled Used Tires, and Hauler agrees that it shall not misrepresent any Culled Used Tires in 
	 any Report as being eligible for or having actually been subject to Processor Delivery under the Plan, and that submission of such a Report with respect to Culled Used Tires shall constitute a False Statement.<br /> 
	 (e)  Hauler shall not sell, transfer or dispose of Culled Used Tires for any final purpose which is not an Approved Purpose, and which the Hauler can demonstrate as such to OTS’s satisfaction, acting reasonably.<br /> 
	 2.5  Non-Eligible Material.<br /> (a)  Except where OTS has issued an Exemption Order with respect thereto, no Transportation Incentives shall be paid in respect of any Non-Eligible Material, and Hauler agrees that it shall not provide Used Tire Pickup or Processor Delivery to any Non-Eligible Material, or represent to any person including any Registered Collector or Registered Processor that Used Tire Pickup or Processor Delivery is available with respect to any Non-eligible Material, or misrepresent any Non-Eligible Material in any Report as being eligible for or actually having been subject to Used Tire Pickup or Processor Delivery. Hauler agrees that submission of such a Report with respect to Non-Eligible Material shall constitute a False Statement.<br /> (b)  Hauler agrees that all its dealings in connection with Non-Eligible Material, including without limitation the selection, use, or sale of Non-Eligible Material, shall be at Hauler’s own risk absolutely.<br /> (c)  Upon request by OTS, Hauler shall provide detailed information to OTS regarding the final destinations and end-uses of Non-Eligible Material transferred, diverted, used, or otherwise dealt by the Hauler, in the form required by OTS from time to time.<br /><br /> ARTICLE 3<br /> REPRESENTATIONS AND WARRANTIES OF HAULER<br /> 3.1  Representations and Warranties. The Hauler represents and warrants that:<br /> (a)  it is duly constituted and is validly existing and in good standing under the laws of its home jurisdiction, and has the necessary corporate or other powers, authority and capacity to own its property and assets and to carry on the business as presently conducted and contemplated by the terms of this Agreement;<br /> (b)  all information provided by it to OTS pursuant to this Agreement, including in all documents required by virtue of the Hauler’s registration with OTS or by virtue of the requirements of law, are true and accurate;<br /> (c)  the registration of the Hauler with OTS as an approved Hauler, the provision of all required information to OTS, and the entering into of this Agreement by Hauler and the performance of its obligation hereunder have been duly authorized by all necessary corporate action.<br /> (d)  it is not a non-resident of Canada within the meaning of Section 116 of the Income Tax Act;<br /> (e)  it holds all permits, licences, consents and authorities issued by all government or governmental authorities of Canada or any province of Canada, or any municipal, regional or other authority, including, without limitation, any governmental department, commission, bureau, board or administrative agency which are necessary and/or desirable in connection with the conduct and operation of the Hauler’s business and is not in breach of or in default of any term or condition thereof;<br /> (f)  all Inventory Statements provided to OTS are true and accurate as of the date of the inventory; and<br /> (g)  all Reports, documentation and other instruments provided to OTS by the Hauler are complete and correct.<br /> 3.2  Statements. All statements contained in any Reports, documents or other instruments delivered by or on behalf of the Hauler to OTS <br />shall be deemed to be representations and warranties of the Hauler of the facts therein contained.<br /> 3.3  Reliance. The Hauler acknowledges and agrees that OTS has entered into this Agreement relying on the warranties, representations and other terms and conditions set out in this Agreement notwithstanding independent searches or investigations that may have been undertaken by or on behalf of OTS and that no information which is now known or should be known or which may hereinafter become known to OTS or its officers, directors or professional advisors shall limit or extinguish any right of indemnification contained herein or otherwise limit, restrict, negate or constitute a waiver of any of the rights or remedies of OTS hereunder.<br /><br /> ARTICLE 4<br /> AUDITS AND INSPECTION<br /> 4.1  Inspection. The Hauler agrees to permit OTS or its agents to inspect the Hauler’s business site upon reasonable notice, during normal business hours, from time to time.<br /> 4.2  Audit. The Hauler agrees that OTS may, from time to time, audit any records of the Hauler maintained in support of the Hauler’s claims, and further, may examine and review, and audit records relating to the Hauler’s compliance with the terms of this Agreement, the Plan and all Applicable Laws, and in the course of doing so may review or inspect the Hauler’s operations to determine the Hauler’s compliance (hereinafter referred to as the “Audit”).<br /> 4.3  Yard Count. The Hauler agrees that OTS or its OTS’s designated representative(s) may, from time to time and without advance notice to the Hauler, attend at the Hauler’s premises, to perform a spot audit yard count of all Used Tires in the Hauler’s possession or located on the Hauler’s premises, including Used Tires and other materials located in any vehicles, including without limitation vehicles of a Registered Collector or Registered Processor or any other person, which are at Hauler’s premises, and may also at this time review any records which OTS may review in the course of an Audit as described in Section 4.2.<br /> 4.4  Provision of Records. The Hauler shall provide OTS’s auditor with any and all records requested and shall cooperate with the Audit at no expense to OTS. It is agreed that OTS shall bear the cost of performing the Audit except in circumstances where the auditor determines that the Hauler has not complied with the terms of this Agreement and the Plan in which case OTS’ reasonable costs of the Audit, including any follow-up review or inspection undertaken as a result of a finding of non-compliance, shall be paid by the Hauler within 30 days of demand therefor being transmitted from OTS to the Hauler. In the event the Hauler fails to pay OTS’ reasonable costs of the Audit as aforesaid, such costs of the Audit shall be treated as Repayment Amounts and subject to set-off in accordance with Section 7.3.<br /><br /> ARTICLE 5<br /> OBLIGATIONS OF OTS<br /> 5.1  Used Tire Pickup and Processor Delivery. OTS shall allow and/or facilitate Used Tire Pickup and Processor Delivery by Hauler.<br /> 5.2  Information and Support. OTS shall provide promotional and informational material and telephone support to Hauler, as OTS deems necessary.<br /> 5.3  Transportation Incentive. With respect to Used Tires retrieved from a Registered Collector by Hauler and subsequently delivered to a Registered Processor, OTS shall pay a Transportation Incentive, at such rate, to such person, and upon such schedule determined by OTS in its sole discretion from time to time, all as may be published by OTS in one or more Guidelines from time to time.<br /><br /> ARTICLE 6<br /> OPERATIONAL MATTERS<br /> 6.1  Observation Following Default. Notwithstanding any other provision of this Agreement, if the Hauler commits an Act of Default, then until the resolution of such Act of Default and for a period of [two years] thereafter, OTS may in its sole discretion require that Hauler advise it, no less than [48 hours] in advance, of the time and location all deliveries of Used Tires to Registered Processors, and permit an OTS representative to observe any aspect of such delivery.<br /> 6.2  No Additional Fees. Hauler shall not charge any additional fee to Collectors for the retrieval of Used Tires in accordance with the Plan. Hauler acknowledges and agrees that OTS will advise Collectors of the requirement of this section, and that OTS will establish mechanisms to enable Collectors to advise OTS of any breach of the requirements of this section.<br /> 6.3  Load Size. The minimum standard load size for retrieval of Used Tires in Southern Ontario is 50 Used Tires; the minimum standard load size for retrieval of Used Tires in Northern Ontario is 75 Used Tires.<br /> 6.4  Agreements Between Hauler, Registered Collectors, and/or Registered Processors. Notwithstanding any other provision of this agreement, Hauler may in its own right enter into any number of contracts with Registered Collectors and/or Registered Processors:<br /> (a)  regarding the pickup from such Registered Collector(s) and/or delivery to such Registered Processor(s) of tires which are not Used Tires as defined in the Plan, or delivery schedules which are different from those contemplated hereunder; such contracts may provide for fees charged by or to Hauler with respect to the services described in such contracts; and/or<br /> (b)  containing additional terms as between Hauler and such Registered Collector(s) and/or Registered Processor(s) regarding Used Tire Pickup and/or Processor Delivery; such contracts may provide for payments made by or to the Hauler.<br /> 6.5  Third Party Monies. The Hauler shall not collect monies on behalf of OTS from any other person but in the event that such does occur notwithstanding the requirements of this section, those monies shall be held for the benefit of, and remitted (without deduction or set-off), forthwith to OTS.<br /><br /> ARTICLE 7<br /> HAULER DEFAULT<br /> 7.1  Events of Default. The occurrence of any of the following while this Agreement is in effect shall constitute an “Act of Default” by the Hauler under this Agreement:<br /> (a)  If the Hauler fails to make payment of any amount required in this Agreement, including any Repayment Amount, when such payment becomes due and payable, and fails to pay such amount in full within five days of written demand therefor being sent by OTS;<br /> (b)  If Hauler breaches or fails to perform, observe or comply with any provision of this Agreement, the Plan, or any Guideline, and does not rectify such breach or failure to OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure being sent by OTS;<br /> (c)  If Hauler defaults in the due observance or performance of any covenant, undertaking, obligation or agreement given to OTS at any time, whether contained in this Agreement, the Plan, or any Guideline, or not, and Hauler does not rectify such default to OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure being sent by OTS;<br /> (d)  If Hauler fails to submit any document or Report required under this Agreement or any Guideline, or to maintain records as required under this Agreement or any Guideline;<br /> (e)  If any Report, representation, warranty, certificate, submission, or statement made by Hauler to OTS is in any respect untrue, erroneous, incomplete, inaccurate, misleading, or not able to be supported by Hauler’s records in an Audit or the results of a yard count pursuant to Section 4.3, all in OTS’s sole determination (each a “False Statement”);<br /> (f)  If Hauler commits any default or breach under any other agreement between Hauler and OTS;<br /> (g)  If Hauler engages a Subcontractor who has an unresolved default contrary to Section 2.3(c);<br /> (h)  If Hauler conspires or colludes with or assists any other person in making any False Statement to OTS or obtain under false pretenses the payment of any amount from OTS;<br /> (i)  If Hauler fails to comply with any applicable law affecting the Hauler’s operation;<br /> (j)  If Hauler is convicted of an offense under the Environmental Protection Act (Ontario);<br /> (k)  If Hauler becomes insolvent or bankrupt or subject to the Bankruptcy and Insolvency Act (Canada) or the Companies Creditors Arrangement Act (Canada), or goes into winding-up or liquidation, either voluntarily or under an order of a court of competent jurisdiction, or makes a general assignment for the benefit of its creditors or otherwise acknowledges itself insolvent;<br /> (l)  If any execution, sequestration, extent, or any other process of any court becomes enforceable against Hauler or if a distress or analogous process is levied on the property and assets of the Hauler; or<br /> (m)  any proceedings shall be commenced for the winding-up, dissolution or liquidation of the Hauler or under which the Hauler could lose its corporate status, such proceedings not being bona fide opposed by the Hauler within five days of the date of commencement or service on the Hauler.<br /> 7.2  Repayment of Payments Based on False Statements. Hauler acknowledges and agrees that:<br /> (a)  if any Transportation Incentives are paid to Hauler as a result of or in connection with any False Statement of the Hauler or any other person, in OTS’s sole determination, Hauler shall forthwith upon demand repay the full amount of such payments to OTS, together with an amount equal to 10 per cent thereof as a reasonable pre-estimate of OTS’s liquidated damages and administrative expenses arising therefrom; and<br /> (b)  if any payment or incentive of any kind, including without limitation any Transportation Incentive, is paid by OTS to any third party as a result of or in connection with any False Statement of the Hauler, in OTS’s sole determination, Hauler shall forthwith upon demand reimburse OTS for the full amount of such payments, together with an amount equal to 10 per cent thereof as a reasonable pre-estimate of OTS’s liquidated damages and administrative expenses arising therefrom.<br /> Amounts payable by Hauler in accordance with this Article are collectively “Repayment Amounts”. OTS may, its sole discretion, waive all or a portion of any Repayment Amount.<br /> 7.3  Set-off. Hauler agrees that OTS may set off Repayment Amounts owing by Hauler to OTS against any future payments of Transportation Incentives with respect to Used Tire Pickup or Processor Delivery by Hauler, or any other amount of any kind to Hauler under this Agreement or any other agreement between Hauler and OTS. If such future payment amounts are insufficient to recoup Repayment Amounts owed by Hauler to OTS, Hauler agrees that such Repayment Amounts are recoverable from Hauler as liquidated damages.<br /> 7.4  Consequences of Default. Immediately following any Act of Default, OTS may, in its sole and absolute discretion, do any, some or all of the following by written notice to the Hauler:<br /> (a)  suspend payment of all incentives and other amounts hereunder, with immediate effect, until the Act of Default is resolved to OTS’s satisfaction;<br /> (b)  require Hauler to pay any Repayment Amount arising in connection with an Act of Default;<br /> (c)  terminate this Agreement, with immediate effect;<br /> (c)(d)  include the Hauler on a published list of persons having unresolved defaults under agreements with OTS, until the Act of Default is resolved to OTS’s satisfaction; or<br /> (d)(e)  exclude Hauler from future participation in the Used Tires Program in any capacity, on a temporary or permanent basis.<br /><br /> ARTICLE 8<br /> TERM & TERMINATION<br /> 8.1  Term. This Agreement shall commence on the Effective Date and continue thereafter until terminated as provided for herein. 8.2  Termination by OTS. OTS may immediately terminate this Agreement by written notice to the Hauler, in addition to any other remedies available at law or in equity, in any of the following events:<br /> (a)  if the Hauler commits an Act of Default;<br /> (b)  if the Plan is terminated by the Minister or any other governmental authority, or the program agreement between OTS and WDO is terminated, provided that prior notice of such termination is communicated to the Hauler as soon as it is available;<br /> (c)  if the Hauler transfers by sale, assignment, bequest, inheritance, by operation of law or other disposition, or shares issued by subscription or allotment, or shares cancelled or redeemed, so as to result in a change in the effective voting or other control of the Hauler from the person or persons holding control on the date of execution of this Agreement without the written consent of OTS, such consent not to be unreasonably withheld; or<br /> (d)  in the event any other legal proceeding involving the Hauler is instituted that in the reasonable opinion of OTS materially impairs the ability of the Hauler to discharge its obligations hereunder.<br /> 8.3  Termination for Convenience. Either Party may terminate this Agreement for convenience upon 90 days’ written notice to the other Party.<br /> 8.4  Reports Following Termination. Following Termination, Hauler agrees that it will submit any Reports required hereunder with respect to any Used Tire Pickup or Processor Delivery occurring before the Termination of this Agreement.<br /> 8.5  Incentive Payment Following Termination.<br /> (a)  In the event of termination by OTS where Hauler has committed an Act of Default, OTS may in its sole and absolute discretion cancel all payments of Transportation Incentives which are pending as of the date on which notice of termination is given or which may arise at any time thereafter.<br /> (b)  Upon termination of this Agreement, provided that payment has not been cancelled by OTS in accordance with Section 8.5(a), OTS shall continue to pay Transportation Incentives with respect to services performed before the termination of this Agreement (notwithstanding that claims for such services may be submitted to OTS after or termination of this Agreement).<br /><br /> ARTICLE 9<br /> INDEMNITY & INSURANCE<br /> 9.1  Indemnity. The Hauler covenants and agrees with OTS to indemnify and hold harmless OTS, its directors, officers, employees and agents against all costs, charges, expenses, legal fees and any other losses or claims which OTS may hereinafter suffer, sustain or may incur or be compelled to pay as a result of any performance or non-performance by Hauler of its obligations hereunder, or any claim, action or proceeding which is brought, prosecuted or threatened against OTS, its directors, officers, employees and agents for any act, deed or omission of the Hauler related in any way to Culled Used Tires or arising from the breach of this Agreement, the Plan, or any applicable law.<br /> 9.2  Release. The Hauler, for itself, its successors and assigns, agrees to release OTS and its officers, directors, employees and agents from all manners of action, causes of action, claims, demands, losses, damages, charges, expenses and the like, of any nature whatsoever which the Hauler ever had, now has or hereafter can, shall or may have against OTS and its officers, directors, employees and agents related in any way to Culled Used Tires or arising out of or in connection with this Agreement provided that all acts, deeds or omissions or the alleged acts, deeds or omissions in respect of which any action, cause of action, claim, demand, loss, damage, charge, expense and the like is based or performed in good faith, and when not performed or omitted to be performed fraudulently or in bad faith by OTS, its directors, officers, employees or agents.<br /> 9.3  Insurance. Hauler shall maintain comprehensive “occurrence” general liability insurance, including personal injury liability, property damage, contractual liability insurance and employer’s liability coverage, with minimum limits of liability of $2,000,000, containing a severability of interests and cross-liability clause, and deliver to OTS on request a certificate thereof with OTS named as an additional insured thereon.<br /><br /> ARTICLE 10<br /> NO OTS LIABILITY FOR USED TIRES<br /> 10.1  Exclusion of Liability. Hauler acknowledges and agrees that at no time shall OTS take possession of any Used Tires and that OTS shall not, in any event, be liable under any theory of liability to Hauler, the previous owner(s) or user(s) of any Used Tires or any other party or parties for any damages, losses, expenses, liabilities and/or other amounts of any nature or kind whatsoever, including without limitation, any direct, indirect, incidental, special, consequential, exemplary and/or punitive damages, arising out of or related to any loss, improper use, improper culling, improper disposal or environmental degradation resulting, proceeding or connected in any way to Used Tires.<br /><br /> ARTICLE 11<br /> PUBLICATION OF INFORMATION<br /> 11.1  Publication of Information. The Hauler understands that its name, main contact information, and the registration number assigned to it by OTS, as well as information regarding the Hauler’s operation, may be published by OTS on OTS’s website or other publically-accessible websites. OTS will take commercially reasonable and appropriate precautions to maintain the confidentiality of information in its database, but will not be liable to the Hauler, or anyone claiming by, through or under it, for any losses, claims and damages arising out of negligent disclosure of any confidential information.<br /> 11.111.2  Release of Information Following Act of Default. The Hauler agrees that, in the event the Hauler commits an Act of Default, OTS may publish its name and registration number on a list of persons with unresolved defaults, as described in Section 7.4(d), and may release details of the Act of Default to any Program Participant who may be affected thereby.<br /><br /> ARTICLE 12<br /> MODIFICATIONS TO PLAN & INCENTIVE STRUCTURE<br /> 12.1  Modifications to Plan. The parties agree and understand that the Plan may be revised from time to time without the input or consent of the Hauler, and the Hauler shall be bound by each revised version of the same as each revision may be issued, as though each was set out herein and formed a contractual obligation upon the Hauler and the Hauler covenants and agrees to abide by, comply with and satisfy such revised Plan.<br /> 12.2  Notice. In the event of the Plan or any part of it being cancelled or altered, then OTS shall issue notice to that effect.<br /> 12.3  Modification of Incentives. The incentives payable and the payment schedule implemented by OTS may be modified from time to time. All changes will be posed on OTS’s internet web site no less than 60 days before the effective date of such change.<br /><br /> ARTICLE 13<br /> GENERAL<br /> 13.1  Assignment. The parties hereby agree that the Hauler’s rights under this Agreement are not assignable or transferable, in any manner, without the prior written consent of OTS, which consent may not be unreasonably withheld.<br /> 13.2  Agreement Binding. This Agreement shall enure to the benefit of and be binding on the parties, their heirs, legal personal representatives, successors and permitted assigns.<br /> 13.3  Dispute Resolution. The parties agree that in the event of a dispute between them with respect to the terms or performance of this Agreement then such dispute shall first be subject to Mediation under Appendix 12 in the Plan, “Mediation Guidelines”, and if such dispute is not able to be resolved through mediation, then it shall be subject to arbitration under Appendix 13 in the Plan, “Arbitration Guidelines”.<br /> 13.4  Notices. Any notice, determination, consent, request or other communication from one party to the other or others or other documents required or which may be given under this Agreement may be delivered or transmitted by means of electronic communication with confirmation of transmission, personal service, facsimile with confirmation of transmission or by prepaid first class postage to the party at the addresses, in the case of the Hauler at the address on the registration form completed by the Hauler and in the case of OTS at the address noted at the top of page 1 of this Agreement, to the attention of the “Executive Director”. Such notifications shall be deemed to have been received on the third day after posting and on the first day after the date of electronic or facsimile transmission, in each case which is not a Saturday, Sunday or public holiday in Ontario. In the event of a postal disruption, notices must be given by personal delivery, e-mail or by a signed back facsimile and all notices delivered by post within one week prior to the postal disruption must be confirmed by a signed back facsimile to be effective.<br /> 13.5  Independent Contractors. The Parties hereto are and shall at all times in the performance of this Agreement be independent contractors and neither Party shall have the authority to assume or create any obligations expressed or implied, in the name of the other Party, nor to contractually bind the other Party in any manner whatsoever.<br /> 13.6  Amendment. OTS retains the right to revise or amend this Agreement. OTS will give notice to the Hauler of such change (the “Change Notice”). Unless the Hauler gives notice to OTS (the “Rejection Notice”) within 45 days of receipt of the Change Notice that the Hauler does not accept the revisions or amendments in the Change Notice, this Agreement, as amended, remains in effect and is binding. If the Hauler gives a Rejection Notice to OTS, this Agreement shall be terminated 30 days after the delivery by the Hauler of the Rejection Notice and the Approved Collection Site will forgo its approval status and not be compensated under the OTS program.<br /> 13.7  Waiver. No failure by any of the parties to insist on strict performance of any covenant, agreement, term or condition (the “provision”) of this Agreement, or to exercise any right or remedy consequent on the breach of any provision, and no acceptance of partial payment during the continuance of any such breach, shall constitute a waiver of any such breach or provision. No waiver of any breach shall affect or alter this Agreement, but each and every provision of this Agreement shall continue in full force and effect with respect to any other then existing or subsequent breach of such provision.<br /> 13.8  Severability. If any provision of this Agreement or the application of the provision to any circumstances shall be held to be invalid or unenforceable, then the remaining provisions of this Agreement or the application of them to other circumstances shall not be affected by the invalidity or unenforceability and shall be valid and enforceable to the fullest extent permitted by law.<br /> 13.9  Entire Agreement. This Agreement constitutes the entire agreement among the parties with respect to its subject matter and supersedes all prior and contemporaneous agreements, understandings, negotiations and discussions, whether oral or written, of the parties. There are no warranties, representations or other agreements among the parties in connection with the subject matter of this Agreement, except as specifically set forth in it. Except as specifically provided herein, no supplement, modification, waiver or termination of this Agreement shall be binding unless executed in writing by the parties to be bound by it.<br /> 13.10  Remedies. No remedy herein conferred upon or reserved in favour of any party hereto shall exclude any other remedy herein or existing at law or in equity or by statute, but each shall be cumulative and in addition to every other remedy given hereunder or now or hereinafter existing.<br /> 13.11  Force Majeure. Neither party shall be liable for delay or failure in performance resulting from acts beyond the control of such party, including but not limited to Acts of God, acts of war, riot, fire, flood or other disaster, acts of government, strike, lockout or communication line or power failure.<br /> 13.12  Governing Law. This Agreement shall be construed and enforced in accordance with, and the rights of the parties shall be governed by, the laws in force in the Province of Ontario.<br /> 13.13  Headings. The headings used throughout this Agreement are solely for convenience of the parties and are not to be used as an aid in the interpretation of this Agreement.<br /> 13.14  Time of Essence. Time shall be of the essence of this Agreement and every part of it.<br /> 13.15  Survival. All provisions of this Agreement which are expressed or which by their nature are intended to survive termination of this Agreement shall survive termination, and continue to bind the parties.<br /> 13.16  Electronic Commerce. Any execution or amendment of this agreement which is conducted electronically by any of the parties is made in accordance with and governed by the Electronic Commerce Act, 2000, (Ontario). If this Agreement is executed on behalf of any party electronically, the natural person who selects the “Agree” button at the bottom of the “Agreement Ratification” page on OTS’s web site on behalf of the executing party certifies that by selecting the “Agree” button, the natural person represents and warrants that he or she is at least eighteen years of age, and has been duly appointed with the authority to bind the executing party.<br /></p>
	'),
	(4, '2017-09-19',@userID,	'2017-09-19','9999-12-31', '
<p>
<strong>WHEREAS:</strong><br>
<strong>A.</strong> OTS has been designated as the industry funding organization under the Act to be responsible for<br>
the collection and environmentally responsible recycling of Used Tires;<br>
<strong>B.</strong> The Processor wishes to operate as a processor and/or recycler of Used Tires, to receive delivery of Used Tires from Registered Haulers, and to receive Incentives in accordance with the Plan;<br>
<strong>C.</strong> The Processor has been approved by OTS as an approved processor in accordance with the Plan; and<br>
<strong>D.</strong> The purpose of this Agreement is to set out the terms and conditions under which the Processor will operate as an approved processor under the Plan.<br>
<strong>NOW THEREFORE</strong> the parties hereto agree as follows as of the Effective Date:<br>
<strong>ARTICLE 1 DEFINITIONS</strong><br>
1.1 <strong><u>Definitions</u></strong>. In addition to the words and phrases defined in the recitals or elsewhere in this Agreement, as used in this Agreement, in any schedule hereto, in any amendment hereof, and in any documents to be executed and delivered pursuant to this Agreement, the following words and phrases have the following meanings, respectively:<br>
(a) <strong>“Act”</strong> means the <i>Waste Diversion Act, 2002</i>, S.O. 2002 c.6 as may be amended from time to time;<br>
(b) <strong>“Act of Default”</strong> has the meaning given in Section 7.1;<br>
(c) <strong>“Agreement”</strong> means this Agreement, including the schedules to this Agreement, as it or they may be amended or supplemented from time to time, and the expressions “hereof”, “herein”, “hereto”, “hereunder”, “hereby” and similar expressions refer to this Agreement and not to any particular section or other portion of this Agreement;<br>
(d) <strong>“Applicable Laws”</strong> has the meaning given in Section 2.1(q);<br>
(e) <strong>“Approved Purpose”</strong> means a purpose for the use of Recycled Tires which is found on the list of Approved Purposes for Recycled Tires maintained by OTS and published on OTS’s website;<br>
(f) <strong>“Arbitration Guidelines”</strong> has the meaning given in Section 13.3;<br>
(g) <strong>“Audit”</strong> has the meaning given in Section 4.2;<br>
(h) <strong>“Change Notice”</strong> has the meaning given in Section 13.6;<br>
(i) <strong>“Collection Incentives”</strong> means the financial incentives, from time to time determined and payable by OTS to support the participation of Registered Collectors in the Plan;<br>
(j) <strong>“Culled Used Tires”</strong> has the meaning given in Section 2.5;<br>
(k) <strong>“Effective Date”</strong> has the meaning given to that term in the listing of parties to this<br>
Agreement;<br>
(l) <strong>“Environmental Laws”</strong> means any and all applicable laws, statutes, regulations, treatise, orders, judgments, decrees, official directives and all authorizations of any department or body of any federal, provincial, regional or municipal government of any agency thereof relating to the protection of the environment, including in particular, but without limiting the generality of the foregoing, the manufacture, use, storage, disposal and transportation of any Hazardous Substance;<br>
(m) <strong>“Exemption Order”</strong> means a written confirmation issued by OTS that certain Used Tires, tire parts or processed rubber which would otherwise be Non-Eligible Material, are eligible for the claiming of certain financial incentives, on the terms set out therein;<br>
(n) <strong>“False Statement”</strong> has the meaning given in Section 7.1(f);<br>
(o) <strong>“Guidelines”</strong> means any directives, forms, procedure manuals, administrative guidance, or other document regarding the implementation of the Plan published by OTS from time to time on its web site;<br>
(p) <strong>“Hazardous Substance”</strong> includes any contaminant, pollutant, dangerous substance, liquid waste, industrial waste, hauled liquid waste, toxic substance, hazardous waste, hazardous material, or hazardous substance as defined in or pursuant to any law, judgement, decree, order, injunction, rule, statute or regulation of any court, arbitrator or federal, provincial, state, municipal, county or regional government or governmental authority, domestic or foreign, or any department, commission, bureau, board, administrative agency or regulatory body of any of the foregoing to which the Processor is subject;<br>
(q) <strong>“Incentives”</strong> means any or all of the Collection Incentives, Transportation Incentives, and Processing Incentives;<br>
(r) <strong>“Inventory Statement”</strong> means any report submitted by the Processor with regard to the inventory of Used Tires and/or parts thereof held on the Processor’s premises and/or in the Processor’s inventory, as at a certain date;<br>
(s) <strong>“Mediation Guidelines”</strong> has the meaning given in Section 13.3;<br>
(t) <strong>“Minister”</strong> means the Minister of the Environment for Ontario;<br>
(u) <strong>“Non-approved Sale”</strong> has the meaning given in Section 2.7;<br>
(v) <strong>“Non-Eligible Material”</strong> means:<br>
(i) Used Tires, tire parts or processed rubber held in Processor’s inventory prior to September 1st, 2009, unless the subject of an Exemption Order issued by OTS; and/or<br>
(ii) Used Tires or parts thereof originating outside Ontario or obtained by the Processor directly or indirectly from any source outside Ontario;<br>
(iii) any material, article or item that is not a Used Tire;<br>
(w) <strong>“OTS”</strong> has the meaning given to that term in the listing of parties to this Agreement;<br>
(x) <strong>“Party”</strong> means a party to this Agreement and any reference to a Party includes its successors and permitted assigns; and Parties means every Party;<br>
(y) <strong>“Performance Bond”</strong> has the meaning given in Section 2.4;<br>
(z) <strong>“Plan”</strong> means the Used Tires Program Plan submitted by OTS on 27 February 2009 and<br>
approved by Waste Diversion Ontario and the Minister of Environment, as amended;<br>
(aa) <strong>“Processing Incentives”</strong> means the financial incentive, as from time to time determined and payable by OTS to the Processor for undertaking the Used Tires recycling initiative contemplated under the terms of this Agreement;<br>
(bb) <strong>“Processor”</strong> has the meaning given to that term in the listing of parties to this Agreement;<br>
(cc) <strong>“Processor Delivery”</strong> means delivery by a Registered Hauler of eligible Used Tires to the Processor, in accordance with the Plan and this Agreement;<br>
(dd) <strong>“Program Participant”</strong> means any collector, hauler, or processor which has registered and entered into an agreement with OTS to provide services under the Plan;<br>
(ee) <strong>“Registered Collector”</strong> means an entity that has registered and remains in good standing with OTS and has entered into a Collector Agreement with OTS for the collection of Used Tires;<br>
(ff) <strong>“Registered Hauler”</strong> means an entity engaged in the business of Used Tire Pickup and Processor Delivery, which has registered and remains in good standing with OTS and entered into a Hauler Agreement with OTS;<br>
(gg) <strong>“Registration System”</strong> means the electronic database maintained by OTS in which registration and other information regarding Processors is kept;<br>
(hh) <strong>“Regulations”</strong> means the regulations passed pursuant to the Act; (ii) “Rejection Notice” has the meaning given in Section 13.6;<br>
(jj) <strong>“Repayment Amounts”</strong> has the meaning given in Section 7.2;<br>
(kk) <strong>“Report”</strong> means any report or submission made by the Processor from time to time regarding Used Tires, tire parts, processed rubber, or TDPs;<br>
(ll) <strong>“Subcontractor”</strong> has the meaning given in Section 2.3;<br>
(mm) <strong>“Subcontracted Obligations”</strong> has the meaning given in Section 2.3;<br>
(nn) <strong>“TDP”</strong> means marketable tire-derived products, made or derived from recycled or processed Used Tires or parts thereof, and which are included on the list of approved TDPs published on the OTS web site from time to time, and which conform to the specifications or definitions set out in such list; for greater clarity, TDP may be classified by grades;<br>
(oo) <strong>“Transportation Incentives”</strong> means those certain financial incentives from time to time determined and payable by OTS, to a person designated by OES from time to time, to support the participation of Registered Haulers in the Plan;<br>
(pp) <strong>“Used Tire Pickup”</strong> means the retrieval of eligible Used Tires from Registered Collectors by Registered Haulers, in accordance with the Plan and this Agreement;<br>
(qq) <strong>“Used Tires”</strong> means used tires or parts of tires that that have not been refurbished for road use, or that, for any reason, are not suitable for their intended purpose; for greater clarity, “Used Tires” refers only to the tire body (or parts thereof), and does not include rims or any other component which is not an integral part of the tire body;<br>
(rr) <strong>“WDO”</strong> means Waste Diversion Ontario; and<br>
(ss) <strong>“Yard Count”</strong> has the meaning given in Section 2.1(f).<br>
1.2 <strong><u>Interpretation</u></strong>. Unless otherwise specified, all references to currency herein shall be to lawful money of Canada. Headings, table of contents, and Article and Section names and divisions are for convenience of reference only and shall not affect the meaning or interpretation of the Agreement. Any accounting terms not specifically defined shall have the meanings ascribed to them in accordance with Canadian generally accepted accounting principles. Unless the context requires otherwise, words importing the singular include the plural and vice versa and words importing gender include all genders. The words “hereto”, “herein”, “hereof”, “hereunder”, “this Agreement” and similar expressions mean and refer to this Agreement. All references to laws or statutes include all related rules, regulations, interpretations, amendments and successor provisions. All references to any document, instrument or agreement include any amendments, waivers and other modifications, extensions or renewals. The words “includes” or “including” shall mean “includes without limitation” or “including without limitation”, respectively.<br>
1.3 <strong><u>Agreements as Covenants</u></strong>. Each agreement and obligation of any of the Parties hereto in this Agreement even though not expressed as a covenant, is considered for all purposes to be a covenant.<br>
<strong>ARTICLE 2 OBLIGATIONS OF THE PROCESSOR</strong><br>
2.1 <strong><u>Processor Obligations</u></strong>. In addition to the other obligations of the Processor set out in this Agreement, the Processor shall:<br>
(a) abide by the requirements set out in this Agreement and its schedules, the Plan, and all Guidelines applicable to Processor;<br>
(b) use the system of Guidelines and paper-based or electronic manifests and documents created by OTS to accurately, correctly, and completely record and report all transactions involving Used Tires, as such system is modified by OTS from time to time in its sole discretion;<br>
(c) post and maintain the Performance Bond;<br>
(d) recycle Used Tires received by it, specifically by:<br>
(i) accepting delivery of, at no cost to Registered Haulers, Used Tires delivered by Registered Haulers;<br>
(ii) processing all Used Tires received from Registered Haulers and not Culled by the Processor into TDP; and<br>
(iii) selling processed TDP on to end users;<br>
all in accordance with the Plan.<br>
(e) promptly complete and submit to OTS each month, or as otherwise directed by OTS from time to time, all documentation required under the Plan, including without limitation:<br>
(i) delivery receipt documentation for Used Tires received by Processor;<br>
(ii) reports and other documents as directed by OTS from time to time regarding sale of processed TDP to end users; and<br>
(iii) such other documentation as may be required by OTS from time to time regarding the receipt of Used Tires, the disposition of Used Tires or processing material or any other residue of processing, or inventories of Used Tires or partially processed materials of final processed TDP in the possession of the Processor.<br>
(f) once per calendar quarter, or more or less often as OTS may in its sole direction require, complete an inventory of all Used Tires in the Processor’s possession or on the Processor’s premises (a “Yard Count”) and provide OTS with the results of each such Yard Count, using such Guidelines and forms as OTS may create for the purpose from time to time and post on its web site. For greater clarity, each Yard Count must specify the total quantity/weight of Used Tires in the Processor’s possession or on the Processor’s premises, the weights of all materials in process, TDPs not yet claimed for Processing Incentives with OTS and any residual materials including but not limited to steel and fibre which have not yet been reported to OTS, as well as a breakdown of the quantity/weight of all Used Tires received from each specific Registered Hauler, and the Registered Collectors from which the Used Tires were retrieved, since the previous Yard Count.<br>
(g) provide advance notice of, or advise OTS immediately upon, any material change in the operating status of the Processor, including notice of all operating shutdowns or slowdowns that are expected to exceed five working days;<br>
(h) conduct itself in a professional and business-like manner in dealings with Registered Collectors, Registered Haulers, members of the public and OTS;<br>
(i) not use any trade-mark, trade name, or logo owned by OTS in any way not specifically authorised by OTS in writing, to provide samples of any proposed use to OTS for written approval which must be received before use begins, and to comply in all respects with any Guideline in effect from time to time regarding the use of OTS’s trade-marks;<br>
(j) store all Used Tires with all requirements of Applicable Law, including without limitation all requirements of the Ontario Ministry of the Environment and the Ontario Office of the Fire Marshal;<br>
(k) at all times maintain clean and tidy premises and where it is necessary to store or stockpile Used Tires and/or processed TDP, such is to be undertaken in an organized and safe fashion. In particular, but not limited to the generality of the foregoing, the Processor shall take reasonable steps to protect any Used Tires and processed TDP safe from fire and leaching;<br>
(l) update any registration information provided to OTS in the Registration System as soon as possible after the information is changed;<br>
(m) sell, transfer or dispose of all Culled Used Tires and TDP in accordance with the letter and spirit of all protocols, treaties or agreements affecting international or interprovincial trade;<br>
(n) within five days after receiving the request from OTS, provide an Inventory Statement regarding Used Tires on the Processor’s premises and/or in the Processor’s inventory at the time of the request, in such format as OTS shall direct;<br>
(o) provide detailed information to OTS regarding the final destinations and end-uses of TDP sold by the Processor for reuse within or outside Canada, in the form required by OTS from time to time;<br>
(p) submit to periodic inspections of the Processor’s premises and equipment by OTS or OTS’s designated representative, at intervals which are reasonable in the sole judgement of OTS;<br>
(q) comply at all times with all laws issued by any government or governmental authority of Canada or any province of Canada, or any municipal, regional or other authority, including, without limitation, any governmental department, commission, bureau, board or administrative agency (“Applicable Laws”), which affect or govern the conduct and operation of the Processor, its business, and its performance under this Agreement; for greater clarity, the Applicable Laws include, without limitation, all laws relating to taxation, employment standards and compensation of workers, and the Environmental Laws;<br>
(r) obtain and maintain all permits, certificates, licences and other qualifications required under any Applicable Law;<br>
(s) use equipment, supplies and service provided by OTS only for their intended purposes and in an efficient manner;<br>
(t) file all required documents and reports in the manner directed by OTS from time to time;<br>
(u) respond in a timely manner to all requests by OTS for information relating to Used Tires;<br>
(v) comply, abide by and carry into effect, as may be required, the objectives of, and the obligations imposed upon the Processor contained in and set out in this Agreement, all applicable Guidelines, and the Plan;<br>
(w) provide notice to OTS of any fines or regulatory orders relating to the Processor’s business made against it in the previous five years; and<br>
(x) provide notice to OTS within 24 hours after any fine or regulatory order relating to the substance of this Agreement made against it after the date hereof.<br>
2.2 <strong><u>Conflict Between Plan, Agreement, and Guidelines</u></strong>. To the extent any provision of the Plan may conflict with a term or terms of this Agreement or any Guideline, other than those dealing with the amount or payment of any incentive, then the Plan shall prevail. For greater clarity, in the event of any conflict between the Plan and any Guideline dealing with the amount or payment of any incentive, such Guideline shall prevail.<br>
2.3 <strong><u>Subcontractors</u></strong>. The Processor agrees that if any third party (a <strong>“Subcontractor”</strong>) performs all or part of any of Processor’s obligations hereunder (<strong>“Subcontracted Obligations”</strong>) at any time, while this Agreement is in effect:<br>
(a) Processor shall be solely responsible for ensuring that a Subcontractor complies fully with all requirements set out in this Agreement, the Plan, and any Guidelines which are applicable to Subcontractor or any Subcontracted Obligations;<br>
(b) Any act or omission by Subcontractor which would constitute a default if performed by the Processor shall constitute an actual default by the Processor; and<br>
(c) Processor may not engage as a Subcontractor any person having any unresolved default(s) of which Processor is aware under any agreement with OTS; if Collector does engage such a person as a Subcontractor, the Collector shall become jointly and severally liable with such person for all amounts owed to OTS with respect to the default(s). For greater clarity, in addition to the actual knowledge of Processor from time to time, Processor shall be deemed to have awareness of any unresolved default included in any list published by OTS from time to time of persons in default of obligations to OTS.<br>
2.4 <strong><u>Performance Bond</u></strong>. The Processor shall be required to post a performance bond in favour of OTS (the <strong>“Performance Bond”</strong>), and maintain the Performance Bond in effect at all times during the term of this Agreement. The Performance Bond must be in such form and amount as may be specified by OTS from time to time, and must be obtained from a surety acceptable to OTS in its sole discretion.<br>
2.5 <strong><u>Culled Tires</u></strong>.<br>
(a) Processor may transfer, divert or use Used Tires for lawful purposes other than processing them into TDP as set out herein (such Used Tires referred to as <strong>“Culled Used Tires”</strong>), subject to the requirements set out in this Section 2.5.<br>
(b) Processor agrees that all dealings in connection with Culled Used Tires, including without limitation the selection, use, or sale of Culled Used Tires, shall be at Processor’s own risk absolutely.<br>
(c) Processor shall provide detailed information to OTS regarding the quantities, final destinations and end-uses of all Culled Used Tires transferred, diverted, used, or otherwise dealt by the Processor, in the form required by OTS from time to time.<br>
(d) Processor shall have no entitlement to Incentives of any kind in respect of any Culled Used Tires, and Processor agrees that it shall not misrepresent any Culled Used Tires in any Report as being eligible for the payment of Incentives, or include any Culled Used Tires in any request for the payment of Incentives, and that submission of such a Report or request for payment with respect to Culled Used Tires shall constitute a False Statement.<br>
(e) Processor shall not sell, transfer or dispose of Culled Used Tires for any final purpose which is not an Approved Purpose, and which the Processor can demonstrate as such to OTS’s satisfaction, acting reasonably.<br>
2.6 <strong><u>Non-Eligible Material</u></strong>.<br>
(a) Except where OTS has issued an Exemption Order with respect thereto, no Incentives of any kind shall be paid in respect of any Non-Eligible Material, and Processor agrees that it shall not accept Processor Delivery of any Non-Eligible Material, or represent to any person including any Registered Collector or Registered Hauler that Used Tire Pickup or Processor Delivery is available with respect to any Non-eligible Material, or misrepresent any Non-Eligible Material in any Report as being eligible for or actually having been subject to Used Tire Pickup or Processor Delivery. Processor agrees that submission of such a Report with respect to Non-Eligible Material shall constitute a False Statement.<br>
(b) Processor agrees that all its dealings in connection with Non-Eligible Material, including without limitation the selection, use, or sale of Non-Eligible Material, shall be at Processor’s own risk absolutely.<br>
(c) Upon request by OTS, Processor shall provide detailed information to OTS regarding the final destinations and end-uses of Non-Eligible Material transferred, diverted, used, or otherwise dealt by the Processor, in the form required by OTS from time to time.<br>
2.7 <strong><u>Non-approved Sales</u></strong>. Processor shall not sell, transfer or dispose of TDP for any purpose which is not an Approved Purpose, and which the Processor can demonstrate as such to OTS’s satisfaction, acting reasonably (any sale for a purpose which is not an Approved Purpose is a <strong>“Non-approved Sale”</strong>). Processor agrees that it shall have no entitlement to Incentives of any kind in respect of any Non-approved Sale.<br>
ARTICLE 3<br>
<strong>PROCESSOR’S REPRESENTATIONS AND WARRANTIES</strong><br>
3.1 <strong><u>Representations and Warranties</u></strong>. The Processor hereby represents and warrants to OTS that:<br>
(a) The Processor is duly constituted and is validly existing and in good standing under the laws of its home jurisdiction, and has the necessary corporate or other powers, authority and capacity to own its property and assets and to carry on the business as presently conducted and contemplated by the terms of this Agreement;<br>
(b) The Processor is not a non-resident of Canada within the meaning of Section 116 of the Income Tax Act;<br>
(c) The Processor holds all permits, licences, consents and authorities issued by all government or governmental authorities of Canada or any province of Canada, or any municipal, regional or other authority, including, without limitation, any governmental department, commission, bureau, board or administrative agency which are necessary and/or desirable in connection with the conduct and operation of the Processor’s business and is not in breach of or in default of any term or condition thereof; and<br>
(d) All Reports, documentation and other instruments provided to OTS by the Processor are complete and correct.<br>
(e) The registration of the Processor with OTS as an approved Processor, the provision of all required information to OTS, and the entering into of this Agreement by Processor and the performance of its obligations hereunder have been duly authorized by all necessary corporate action.<br>
3.2 <strong><u>Statements</u></strong>. All statements contained in any Reports, documents or other instruments delivered by or on behalf of the Processor to OTS shall be deemed to be representations and warranties of the Processor of the facts therein contained.<br>
3.3 <strong><u>Reliance</u></strong>. The Processor acknowledges and agrees that OTS has entered into this Agreement relying on the warranties, representations and other terms and conditions set out in this Agreement notwithstanding independent searches or investigations that may have been undertaken by or on behalf of OTS and that no information which is now known or should be known or which may hereinafter become known to OTS or its officers, directors or professional advisors shall limit or extinguish any right of indemnification contained herein or otherwise limit, restrict, negate or constitute a waiver of any of the rights or remedies of OTS hereunder.<br>
<strong>ARTICLE 4 AUDITS AND INSPECTION</strong><br>
4.1 <strong><u>Inspection</u></strong>. The Processor agrees to permit OTS or its agents to inspect the Processor’s business site upon reasonable notice, during normal business hours, from time to time.<br>
4.2 <strong><u>Audit</u></strong>. The Processor agrees that OTS may, from time to time, audit any records of the Processor maintained in support of the Processor’s claims, and further, may examine and review, and audit records relating to the Processor’s compliance with the terms of this Agreement, the Plan and all Applicable Laws, and in the course of doing so may review or inspect the Processor’s operations to determine the Processor’s compliance (hereinafter referred to as the <strong>“Audit”</strong>).<br>
4.3 <strong><u>Spot Count</u></strong>. The Processor agrees that OTS or its designated representative(s) may, from time to time and without advance notice to the Processor, attend at the Processor’s premises to perform a spot audit count of all Used Tires in the Processor’s possession or on the Processor’s premises, including Used Tires located in any vehicles which are at Processor’s premises, including without limitation the vehicles of a Registered Collector or Registered Hauler or any other person, and may also at this time review any records which OTS may review in the course of an Audit as described in Section 4.2.<br>
4.4 <strong><u>Provision of Records</u></strong>. The Processor shall provide OTS’s auditor with any and all records requested and shall cooperate with the Audit at no expense to OTS. It is agreed that OTS shall pay the costs of the Audit except in circumstances where the auditor determines that the Processor has not complied with the terms of this Agreement and the Plan in which case OTS’ reasonable costs of the Audit, including any follow-up review or inspection undertaken as a result of a finding of non-compliance, shall be paid by the Processor within 30 days of demand therefor being transmitted from OTS to the Processor. In the event the Processor fails to pay OTS’ reasonable costs of the Audit as aforesaid, such costs of the Audit shall be treated as Repayment Amounts and subject to set-off in accordance with Section 7.3.<br>
<strong>ARTICLE 5 OBLIGATIONS OF OTS</strong><br>
5.1 <strong><u>Processor Delivery</u></strong>. OTS shall allow and/or facilitate Processor Delivery of Used Tires by Registered Haulers to Processor.<br>
5.2 <strong><u>Information and Support</u></strong>. OTS shall provide promotional and informational material and telephone support to Processor, as OTS deems necessary.<br>
5.3 <strong><u>Incentives</u></strong>. OTS shall pay such Incentives to the Processor at such rate and upon such schedule as may be determined by OTS from time to time in its sole discretion, all as may be published by OTS in one or more Guidelines from time to time, within 35 days of receipt by OTS of the completed Claims documentation as specified by OTS from time to time.<br>
<strong>ARTICLE 6 OPERATIONAL MATTERS</strong><br>
6.1 <strong><u>Incentives</u></strong>. Incentives contemplated by this Agreement for payment to the Processor shall be based on the Passenger / Light Truck/ Medium Truck/ Off-the-Road Tire Incentives as posted on the OTS website and as amended from time to time.<br>
6.2 <strong><u>Agreements Between Processor, Registered Collectors, and/or Registered Haulers</u></strong>. Notwithstanding any other provision of this Agreement, Processor may in its own right enter into any number of contracts with Registered Collectors and/or Registered Haulers:<br>
(a) regarding the pickup from such Registered Collector(s) and/or delivery to Processor by such Registered Hauler(s) of tires which are not Used Tires as defined in the Plan, or delivery schedules which are different from those contemplated hereunder; such contracts may provide for fees charged by or to Processor with respect to the services described in such contracts; and/or<br>
(b) containing additional terms as between Processor and such Registered Collector(s) and/or Registered Hauler(s) regarding Used Tire Pickup and/or Processor Delivery; such contracts may provide for payments by or to the Processor.<br>
6.3 <strong><u>Third Party Monies</u></strong>. The Processor shall not collect monies on behalf of OTS from any other person but in the event that such does occur notwithstanding the requirements of this section, those monies shall be held for the benefit of, and remitted (without deduction or set-off), forthwith to OTS.<br>
6.4 <strong><u>Anti-dumping</u></strong>. OTS reserves the right at any time to reduce, refuse to pay or recover all or part of Processing Incentives in respect of TDPs sold or delivered to a person situated outside of the Province of Ontario (the <strong>“export jurisdiction”</strong>) which may, in the opinion of OTS, result in a violation of the laws of Canada or the export jurisdiction or which may result in the possibility of administrative or legal action against OTS, Waste Diversion Ontario or the Governments of Ontario or Canada or the possibility of trade sanctions against TDPs originating from Ontario. Trade problems could include sale at local prices which are lower than Ontario prices (“dumping”) or sale at any price of subsidized goods which could trigger under local laws the right to take administrative or legal action, including the imposition of countervailing duties or interprovincial trade sanctions.<br>
<strong>ARTICLE 7 PROCESSOR DEFAULT</strong><br>
7.1 <strong><u>Events of Default</u></strong>. The occurrence of any of the following while this Agreement is in effect shall constitute an <strong>“Act of Default”</strong> by the Processor under this Agreement:<br>
(a) If the Processor fails to make payment of any amount required in this Agreement, including any Repayment Amount, when such payment becomes due and payable, and fails to pay such amount in full within five days of written demand therefor being sent by OTS;<br>
(b) If Processor breaches or fails to perform, observe or comply with any provision of this Agreement, the Plan, or any Guideline, and does not rectify such breach or failure to OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure being sent by OTS;<br>
(c) If Processor defaults in the due observance or performance of any covenant, undertaking, obligation or agreement given to OTS at any time, whether contained in this Agreement, the Plan, or any Guideline, or otherwise, and Processor does not rectify such default to OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure being sent by OTS;<br>
(d) If Processor fails to post or maintain the bond required under Section 2.4;<br>
(e) If Processor fails to submit any document or Report required under this Agreement or any Guideline, or to maintain records as required under this Agreement or any Guideline;<br>
(f) If any Report, representation, warranty, certificate, submission, or statement made by Processor to OTS is in any respect untrue, erroneous, incomplete, inaccurate, misleading, inconsistent with any Yard Count or Audit or results of a spot count pursuant to Section<br>
4.3, or not able to be supported by Processor’s records in a Yard Count or Audit, all in<br>
OTS’s sole determination (each a <strong>“False Statement”</strong>);<br>
(g) If Processor commits any default or breach under any other agreement between Processor<br>
and OTS;<br>
(h) If Processor engages any Subcontractor who has an unresolved default contrary to Section 2.3(c);<br>
(i) If Processor conspires or colludes with or assists any other person in making any False Statement to OTS or obtain under false pretenses the payment of any amount from OTS;<br>
(j) If Processor fails to comply with any applicable law affecting the Processor’s operation;<br>
(k) If Processor is convicted of an offense under the <i>Environmental Protection Act</i> (Ontario);<br>
(l) If Processor becomes insolvent or bankrupt or subject to the <i>Bankruptcy and Insolvency Act</i> (Canada) or the <i>Companies Creditors Arrangement Act</i> (Canada), or goes into winding-up or liquidation, either voluntarily or under an order of a court of competent jurisdiction, or makes a general assignment for the benefit of its creditors or otherwise acknowledges itself insolvent;<br>
(m) If any execution, sequestration, extent, or any other process of any court becomes enforceable against Processor or if a distress or analogous process is levied on the property and assets of the Processor; or<br>
(n) any proceedings shall be commenced for the winding-up, dissolution or liquidation of the Processor or under which the Processor could lose its corporate status, such proceedings not being <i>bona fide</i> opposed by the Processor within five days of the date of commencement or service on the Processor.<br>
7.2 <strong><u>Repayment of Payments Based on False Statements or with Respect to Non-approved Sales</u></strong>. Processor acknowledges and agrees that:<br>
(a) if any Incentives are paid to Processor as a result of or in connection with any False Statement of the Processor or any other person, in OTS’s sole determination, Processor shall forthwith upon demand repay the full amount of such payments to OTS, together with an amount equal to 10 per cent thereof as a reasonable pre-estimate of OTS’s liquidated damages and administrative expenses arising therefrom;<br>
(b) if any payment or Incentive of any kind is paid by OTS to any third party as a result of or in connection with any False Statement of the Processor, in OTS’s sole determination, Processor shall forthwith upon demand reimburse OTS for the full amount of such payments, together with an amount equal to 10 per cent thereof as a reasonable pre- estimate of OTS’s liquidated damages and administrative expenses arising therefrom; and<br>
(c) if any Processing Incentives are paid to Processor in connection with any Non-approved Sales, Processor shall forthwith upon demand repay the full amount of such payments to OTS, together with an amount equal to 10 per cent thereof as a reasonable pre-estimate of OTS’s liquidated damages and administrative expenses arising therefrom.<br>
Amounts payable by Processor in accordance with this Article are collectively <strong>“Repayment Amounts”</strong>. OTS may, its sole discretion, waive all or a portion of any Repayment Amount.<br>
7.3 <strong><u>Set-off</u></strong>. Processor agrees that OTS may set off Repayment Amounts owing by Processor to OTS against any future payments of Incentives or any other amount of any kind to Processor under this Agreement or any other agreement between Processor and OTS. If such future payment amounts are insufficient to recoup Repayment Amounts owed by Processor to OTS, Processor agrees that such Repayment Amounts are recoverable from Processor as liquidated damages.<br>
7.4 <strong><u>Consequences of Default</u></strong>. Immediately following any Act of Default, OTS may, in its sole and absolute discretion, do any, some or all of the following by written notice to the Processor:<br>
(a) suspend payment to the Processor of any or all Incentives and other amounts hereunder, with immediate effect, until the Act of Default is resolved to OTS’s satisfaction;<br>
(b) require Processor to pay any Repayment Amount arising in connection with an Act of Default;<br>
(c) terminate this Agreement, with immediate effect;<br>
(d) include the Processor on a published list of persons having unresolved defaults under agreements with OTS, until the Act of Default is resolved to OTS’s satisfaction; or<br>
(e) exclude Processor from future participation in the Used Tires Program in any capacity, on a temporary or permanent basis.<br>
7.5 <strong><u>Claim against Performance Bond</u></strong>. In addition to any other remedy which OTS has under this Agreement or at law, and without limiting the ability of OTS to pursue any such other remedy at any time, OTS may make a claim against the Performance Bond, with 30 days’ written notice to the Processor, at any time following:<br>
(a) an Act of Default, whether or not curable or cured, which results in the payment by OTS of any Incentive which should not have been paid; or<br>
(b) any action, failure to act, or series of actions or failures to act by the Processor, which result, individually or cumulatively, in OTS incurring any costs which it would not otherwise have incurred,<br>
<strong>ARTICLE 8 TERM &amp;TERMINATION</strong><br>
8.1 <strong><u>Term</u></strong>. This Agreement shall commence on the Effective Date and continue thereafter until terminated as provided for herein.<br>
8.2 <strong><u>Termination by OTS</u></strong>. OTS may immediately terminate this Agreement by written notice to the Processor, in addition to any other remedies available at law or in equity, in any of the following events:<br>
(a) if the Processor commits an Act of Default;<br>
(b) if the Plan is terminated by the Minister or any other governmental authority, or the program agreement between OTS and WDO is terminated, provided that prior notice of such termination is communicated to the Processor as soon as it is available;<br>
(c) if the Processor transfers by sale, assignment, bequest, inheritance, by operation of law or other disposition, or shares issued by subscription or allotment, or shares cancelled or redeemed, so as to result in a change in the effective voting or other control of the Processor from the person or persons holding control on the date of execution of this Agreement without the written consent of OTS, such consent not to be unreasonably withheld; or<br>
(d) in the event any other legal proceeding involving the Processor is instituted that in the reasonable opinion of OTS materially impairs the ability of the Processor to discharge its obligations hereunder.<br>
8.3 <strong><u>Termination for Convenience</u></strong>. Either Party may terminate this Agreement for convenience upon 365 days’ written notice to the other Party.<br>
8.4 <strong><u>Reports Following Termination</u></strong>. Following Termination, Processor agrees that it will submit any Reports required hereunder with respect to any activity involving Used Tires or TDPs occurring before the Termination of this Agreement.<br>
8.5 <strong><u>Incentive Payment Following Termination</u></strong>.<br>
(a) In the event of termination by OTS where Processor has committed an Act of Default, OTS may in its sole and absolute discretion cancel or redirect all payments of Incentives to the Processor which are pending as of the date on which notice of termination is given or which may arise at any time thereafter.<br>
(b) Upon termination of this Agreement, provided that payment has not been cancelled by OTS in accordance with Section 8.5(a), OTS shall continue to pay Incentives to the Processor with respect to Used Tire transactions performed before the termination of this Agreement (notwithstanding that claims for such services may be submitted to OTS after the termination of this Agreement).<br>
8.6 <strong><u>Processing Following Termination</u></strong>. Within ninety days of termination of this Agreement, the Processor shall process all inventory of Used Tires in its possession at the time of delivery of the notice of termination into TDP on the terms and conditions as set out in this Agreement and, provided that payment has not been cancelled by OTS in accordance with Section 8.5(a), OTS shall continue to pay Incentives to the Processor with respect to such processed Used Tires, notwithstanding the termination of this Agreement.<br>
<strong>ARTICLE 9 INDEMNITY &amp;INSURANCE</strong><br>
9.1 <strong><u>Indemnity</u></strong>. The Processor covenants and agrees with OTS to indemnify and hold harmless OTS, its directors, officers, employees and agents against all costs, charges, expenses, legal fees and any other losses or claims which OTS may hereinafter suffer, sustain or may incur or be compelled to pay as a result of any performance or non-performance by Processor of its obligations hereunder, or any claim, action or proceeding which is brought, prosecuted or threatened against OTS, its directors, officers, employees and agents for any act, deed or omission of the Processor related in any way to Culled Used Tires or arising from the breach of this Agreement, the Plan, or any applicable law.<br>
9.2 <strong><u>Release</u></strong>. The Processor, for itself, its successors and assigns, agrees to release OTS and its officers, directors, employees and agents from all manners of action, causes of action, claims, demands, losses, damages, charges, expenses and the like, of any nature whatsoever which the Processor ever had, now has or hereafter can, shall or may have against OTS and its officers, directors, employees and agents related in any way to Culled Used Tires or arising out of or in connection with this Agreement provided that all acts, deeds or omissions or the alleged acts, deeds or omissions in respect of which any action, cause of action, claim, demand, loss, damage, charge, expense and the like is based or performed in good faith, and when not performed or omitted to be performed fraudulently or in bad faith by OTS, its directors, officers, employees or agents.<br>
9.3 <strong><u>Indemnity</u></strong>. Processor shall maintain comprehensive “occurrence” general liability insurance, including personal injury liability, property damage, contractual liability insurance and employer’s liability coverage, with minimum limits of liability of $5,000,000, containing a severability of interests and cross-liability clause, and deliver to OTS on request a certificate thereof with OTS named as an additional insured thereon.<br>
ARTICLE 10<br>
<strong>NO OTS LIABILITY FOR USED TIRES OR TDP</strong><br>
10.1 <strong><u>Exclusion of Liability</u></strong>. Processor acknowledges and agrees that at no time shall OTS take possession of any Used Tires or TDP and that OTS shall not, in any event, be liable under any theory of liability to Processor, the previous owner(s) or user(s) of any Used Tires or TDP or any other party or parties for any damages, losses, expenses, liabilities and/or other amounts of any nature or kind whatsoever, including without limitation, any direct, indirect, incidental, special, consequential, exemplary and/or punitive damages, arising out of or related to any loss, improper use, improper culling, improper transfer or sale, improper disposal or environmental degradation resulting, proceeding or connected in any way to Used Tires or TDP.<br>
<strong>ARTICLE 11 PUBLICATION OF INFORMATION</strong><br>
11.1 <strong><u>Publication of Information</u></strong>. The Processor understands that its name, main contact information, and the registration number assigned to it by OTS, as well as information regarding the Processor’s operation, may be published by OTS on OTS’s website or other publically-accessible websites. OTS will take commercially reasonable and appropriate precautions to maintain the confidentiality of information in its database, but will not be liable to the Processor, or anyone claiming by, through or under it, for any losses, claims and damages arising out of negligent disclosure of any confidential information.<br>
11.2 <strong><u>Release of Information Following Act of Default</u></strong>. The Processor agrees that, in the event the Processor commits an Act of Default, OTS may publish its name and registration number on a list of persons with unresolved defaults, as described in Section 7.4(d), and may release details of the Act of Default to any Program Participant who may be affected thereby.<br>
<strong>ARTICLE 12 MODIFICATIONS TO PLAN</strong><br>
12.1 <strong><u>Modifications to Plan</u></strong>. The parties agree and understand that the Plan may be revised from time to time without the input or consent of the Processor, and the Processor shall be bound by each revised version of the same as each revision may be issued, as though each was set out herein and formed a contractual obligation upon the Processor and the Processor covenants and agrees to abide by, comply with and satisfy such revised Plan.<br>
12.2 <strong><u>Notice</u></strong>. In the event of the Plan or any part of it being cancelled or altered, then OTS shall issue notice to that effect.<br>
12.3 <strong><u>Modifications to Incentives</u></strong>. The incentives payable and the payment schedule implemented by OTS may be modified from time to time. All changes will be posed on OTS’s internet web site no less than 60 days before the effective date of such change.<br>
<strong>ARTICLE 13 GENERAL</strong><br>
13.1 <strong><u>Assignment</u></strong>. The parties hereby agree that the Processor’s rights under this Agreement are not assignable or transferable, in any manner, without the prior written consent of OTS, which consent may not be unreasonably withheld.<br>
13.2 <strong><u>Agreement Binding</u></strong>. This Agreement shall enure to the benefit of and be binding on the parties, their heirs, legal personal representatives, successors and permitted assigns.<br>
13.3 <strong><u>Dispute Resolution</u></strong>. The parties agree that in the event of a dispute between them with respect to the terms or performance of this Agreement then such dispute shall first be subject to Mediation under Appendix 12 in the Plan, <strong>“Mediation Guidelines”</strong>, and if such dispute is not able to be resolved through mediation, then it shall be subject to arbitration under Appendix 13 in the Plan, <strong>“Arbitration Guidelines”</strong>.<br>
13.4 <strong><u>Notices</u></strong>. Any notice, determination, consent, request or other communication from one party to the other or others or other documents required or which may be given under this Agreement may be delivered or transmitted by means of electronic communication with confirmation of transmission, personal service, facsimile with confirmation of transmission or by prepaid first class postage to the party at the addresses, in the case of the Processor at the address on the registration form completed by the Processor and in the case of OTS at the address noted at the top of page 1 of this Agreement, to the attention of the “Executive Director”. Such notifications shall be deemed to have been received on the third day after posting and on the first day after the date of electronic or facsimile transmission, in each case which is not a Saturday, Sunday or public holiday in Ontario. In the event of a postal disruption, notices must be given by personal delivery, e-mail or by a signed back facsimile and all notices delivered by post within one week prior to the postal disruption must be confirmed by a signed back facsimile to be effective.<br>
13.5 <strong><u>Independent Contractors</u></strong>. The Parties hereto are and shall at all times in the performance of this Agreement be independent contractors and neither Party shall have the authority to assume or create any obligations expressed or implied, in the name of the other Party, nor to contractually bind the other Party in any manner whatsoever.<br>
13.6 <strong><u>Amendment</u></strong>. OTS retains the right to revise or amend this Agreement. OTS will give notice to the Processor of such change (the <strong>“Change Notice”</strong>). Unless the Processor gives notice to OTS (the <strong>“Rejection Notice”</strong>) within 45 days of receipt of the Change Notice that the Processor does not accept the revisions or amendments in the Change Notice, this Agreement, as amended, remains in effect and is binding. If the Processor gives a Rejection Notice to OTS, this Agreement shall be terminated 30 days after the delivery by the Processor of the Rejection Notice and the Approved Collection Site will forgo its approval status and not be compensated under the OTS program.<br>
13.7 <strong><u>Waiver</u></strong>. No failure by any of the parties to insist on strict performance of any covenant, agreement, term or condition (the <strong>“provision”</strong>) of this Agreement, or to exercise any right or remedy consequent on the breach of any provision, and no acceptance of partial payment during the continuance of any such breach, shall constitute a waiver of any such breach or provision. No waiver of any breach shall affect or alter this Agreement, but each and every provision of this Agreement shall continue in full force and effect with respect to any other then existing or subsequent breach of such provision.<br>
13.8 <strong><u>Severability</u></strong>. If any provision of this Agreement or the application of the provision to any circumstances shall be held to be invalid or unenforceable, then the remaining provisions of this Agreement or the application of them to other circumstances shall not be affected by the invalidity or unenforceability and shall be valid and enforceable to the fullest extent permitted by law.<br>
13.9 <strong><u>Entire Agreement</u></strong>. This Agreement constitutes the entire agreement among the parties with respect to its subject matter and supersedes all prior and contemporaneous agreements, understandings, negotiations and discussions, whether oral or written, of the parties. There are no warranties, representations or other agreements among the parties in connection with the subject matter of this Agreement, except as specifically set forth in it. Except as specifically provided herein, no supplement, modification, waiver or termination of this Agreement shall be binding unless executed in writing by the parties to be bound by it.<br>
13.10 <strong><u>Remedies</u></strong>. No remedy herein conferred upon or reserved in favour of any party hereto shall exclude any other remedy herein or existing at law or in equity or by statute, but each shall be cumulative and in addition to every other remedy given hereunder or now or hereinafter existing.<br>
13.11 Force Majeure. Neither party shall be liable for delay or failure in performance resulting from acts beyond the control of such party, including but not limited to Acts of God, acts of war, riot, fire, flood or other disaster, acts of government, strike, lockout or communication line or power failure.<br>
13.12 <strong><u>Governing Law</u></strong>. This Agreement shall be construed and enforced in accordance with, and the rights of the parties shall be governed by, the laws in force in the Province of Ontario.<br>
13.13 <strong><u>Headings</u></strong>. The headings used throughout this Agreement are solely for convenience of the parties and are not to be used as an aid in the interpretation of this Agreement.<br>
13.14 <strong><u>Time of Essence</u></strong>. Time shall be of the essence of this Agreement and every part of it.<br>
13.15 <strong><u>Survival</u></strong>. All provisions of this Agreement which are expressed or which by their nature are intended to survive termination of this Agreement shall survive termination, and continue to bind the parties.<br>
13.16 <strong><u>Electronic Commerce</u></strong>. Any execution or amendment of this agreement which is conducted electronically by any of the parties is made in accordance with and governed by the Electronic Commerce Act, 2000, (Ontario). If this Agreement is executed on behalf of any party electronically, the natural person who selects the “Agree” button at the bottom of the “Agreement Ratification” page on OTS’s web site on behalf of the executing party certifies that by selecting the “Agree” button, the natural person represents and warrants that he or she is at least eighteen years of age, and has been duly appointed with the authority to bind the executing party.<br>
</p>
	'),
	(5, '2017-09-19',@userID,	'2017-09-19','9999-12-31', '
<p>
<strong>WHEREAS:</strong><br>
<strong>A.</strong> OTS has been designated as the industry funding organization under the Act to be responsible for 
the collection and environmentally responsible recycling of Used Tires;<br>
<strong>B.</strong> The RPM wishes to operate as a recycled product manufacturer receiving Tire-Derived Product 
(TDP) from Ontario Processors and receive Manufacturing Incentives in accordance with the Plan;<br>
<strong>C.</strong> The RPM has been approved by OTS as an approved recycled product manufacturer in 
accordance with the Plan; and<br>
<strong>D.</strong> The purpose of this Agreement is to set out the terms and conditions under which the RPM will 
operate under the Plan.<br>
<strong>NOW THEREFORE</strong> the parties hereto agree as follows as of the Effective Date:<br>
<strong>ARTICLE 1</strong><br>
<strong>DEFINITIONS</strong><br>
1.1 <strong><u>Definitions.</u></strong> In addition to the words and phrases defined in the recitals or elsewhere in this 
Agreement, as used in this Agreement, in any schedule hereto, in any amendment hereof, and in 
any documents to be executed and delivered pursuant to this Agreement, the following words and 
phrases have the following meanings, respectively:<br>
(a) “<strong>Act</strong>” means the <u>Waste Diversion Act, 2002</u>, S.O. 2002 c.6 as may be amended from time 
to time;<br>
(b) “<strong>Act of Default</strong>” has the meaning given in Section 8.1;<br>
(c) “<strong>Agreement</strong>” means this Agreement, including the schedules to this Agreement, as it or 
they may be amended or supplemented from time to time, and the expressions “hereof”, 
“herein”, “hereto”, “hereunder”, “hereby” and similar expressions refer to this Agreement 
and not to any particular section of other portion of this Agreement;<br>
(d) “<strong>Approved Purpose”</strong> means a purpose found on the list of Approved Purposes for 
Recycled Tires maintained by OTS and published on OTS’s website;<br>
(e) “<strong>Arbitration Guidelines</strong>” has the meaning given in Section 14.3;<br>
(f) “<strong>Audit</strong>” has the meaning given in Section 7.2;<br>
(g) “<strong>Applicable Laws</strong>” has the meaning given in Section 2.1(j);<br>
(h) “<strong>Change Notice</strong>” has the meaning given in Section 14.6;<br>
(i) “<strong>Collector</strong>” means a for-profit, not-for-profit, or municipal corporation that has entered 
into an agreement with OTS for the collection of Used Tires;<br>
(j) “<strong>Effective Date</strong>” has the meaning given to that term in the listing of parties to this 
Agreement;<br>
(k) “<strong>Environmental Laws</strong>” means any and all applicable laws, statutes, regulations, treatise, 
orders, judgements, decrees, official directives and all authorizations of any department 
or body of any federal, provincial, regional or municipal government of any agency 
thereof relating to the protection of the environment, including in particular, but without 
limiting the generality of the foregoing, the manufacture, use, storage, disposal and 
transportation of any Hazardous Substance;<br>
(l) “<strong>False Statement</strong>” has the meaning given in Section 8.1(d);<br>
(m) “<strong>Guidelines</strong>” means any directives, forms, procedure manuals, administrative guidance, 
or other document regarding the implementation of the Plan published by OTS from time 
to time on its web site;<br>
(n) “<strong>Hauler</strong>” means a person or corporation engaged in the business of collecting and 
transporting Used Tires to Processors, and which has registered with OTS and entered 
into a Hauler Agreement with OTS;<br>
(o) “<strong>Hazardous Substance</strong>” includes any contaminant, pollutant, dangerous substance, 
liquid waste, industrial waste, hauled liquid waste, toxic substance, hazardous waste, 
hazardous material, or hazardous substance as defined in or pursuant to any law, 
judgement, decree, order, injunction, rule, statute or regulation of any court, arbitrator or 
federal, provincial, state, municipal, county or regional government or governmental 
authority, domestic or foreign, or any department, commission, bureau, board, 
administrative agency or regulatory body of any of the foregoing to which the RPM is 
subject;<br>
(p) “<strong>Manufactured Product</strong>” means products manufactured by molding, calendaring or 
extruding recycled rubber into a finished product;<br>
(q) “<strong>Manufacturing Incentives</strong>” means the financial incentives, from time to time 
determined and payable by OTS to the RPM to support the manufacturing of 
Manufactured Products;<br>
(r) “<strong>Mediation Guidelines</strong>” has the meaning given in Section 14.3;<br>
(s) “<strong>Minister</strong>” means the Minister of the Environment for Ontario;<br>
(t) “<strong>Non-approved Use</strong>” has the meaning given in section 3.1;<br>
(u) “<strong>OTS</strong>” has the meaning given to that term in the listing of parties to this Agreement;<br>
(v) “<strong>Party</strong>” means a party to this Agreement and any reference to a Party includes its 
successors and permitted assigns; and Parties means every Party;<br>
(w) “<strong>Plan</strong>” means the Used Tires Program Plan submitted by OTS on 27 February 2009 and 
approved by Waste Diversion Ontario and the Minister of Environment;<br>
(x) “<strong>Processor</strong>” means a business that processes Used Tires into material that can be further 
processed in order to recover specific components within the same organization or sent to 
downstream processors for use as a raw material in another process, and which has 
registered with OTS and entered into a Processor Agreement with OTS;<br>
(y) “<strong>Recycled Product Manufacturer</strong>” has the meaning given that that term in the listing of 
parties to this Agreement;<br>
(z) “<strong>Registration System</strong>” means the electronic database maintained by OTS in which 
registration and other information regarding RPMs is kept;<br>
(aa) “<strong>Regulations</strong>” means the regulations passed pursuant to the Act;<br>
(bb) “<strong>Rejection Notice</strong>” has the meaning given in Section 14.6;<br>
(cc) “<strong>Repayment Amounts</strong>” has the meaning given in Section 8.2;<br>
(dd) “<strong>TDP</strong>” means marketable products made or derived wholly or partly from the recycling 
of Used Tires;<br>
(ee) “<strong>Used Tires</strong>” means used tires or parts of tires that that have not been refurbished for 
road use, or that, for any reason, are not suitable for their intended purpose; and<br>
(ff) “<strong>WDO</strong>” means Waste Diversion Ontario.<br>
1.2 <u><strong>Interpretation</strong></u>. Unless otherwise specified, all references to currency herein shall be to lawful 
money of Canada. Headings, table of contents, and Article and Section names and divisions are 
for convenience of reference only and shall not affect the meaning or interpretation of the 
Agreement. Any accounting terms not specifically defined shall have the meanings ascribed to 
them in accordance with Canadian generally accepted accounting principles. Unless the context 
requires otherwise, words importing the singular include the plural and vice versa and words 
importing gender include all genders. The words “hereto”, “herein”, “hereof”, “hereunder”, “this 
Agreement” and similar expressions mean and refer to this Agreement. All references to laws or 
statutes include all related rules, regulations, interpretations, amendments and successor 
provisions. All references to any document, instrument or agreement include any amendments, 
waivers and other modifications, extensions or renewals. The words “includes” or “including” 
shall mean “includes without limitation” or “including without limitation”, respectively.<br>
1.3 <u><strong>Agreements as Covenants</strong></u>. Each agreement and obligation of any of the Parties hereto in this 
Agreement even though not expressed as a covenant, is considered for all purposes to be a 
covenant.<br>
<strong>ARTICLE 2</strong><br>
<strong>OBLIGATIONS OF THE RPM</strong><br>
2.1 <u><strong>RPM Obligations</strong></u>. The RPM shall:<br>
(a) abide by the requirements set out in this Agreement and its schedules, the Plan, and all 
Guidelines applicable to RPM;<br>
(b) use the system of Guidelines, manifests and documents created by OTS to accurately, 
correctly, and completely record and report all transactions involving TDPs, as such 
system is modified by OTS from time to time in its sole discretion;<br>
(c) incorporate TDPs received by it in the manufacture of new products, specifically by:<br>
(i) using the TDPs received by it to produce Manufactured Products; and<br>
(ii) selling such Manufactured Products on to end users;<br>
all in accordance with the Plan.<br>
(d) promptly complete and submit to OTS each month, or as otherwise directed by OTS from 
time to time, all documentation required under the Plan and any Guideline, including 
without limitation:<br>
(i) delivery receipt documentation for TDPs received by RPM;<br>
(ii) reports and other documents as directed by OTS from time to time regarding sale 
of Manufactured Products to end users; and<br>
(iii) such other documentation as may be required by OTS from time to time 
regarding the receipt of TDPs, the use of TDPs in manufacturing, the sale or 
other disposition of Manufactured Products, or the use or other disposition of any 
residue of manufacturing, or unused inventories of TDP in the possession of the 
RPM.<br>
(e) provide advance notice of, or advise OTS immediately upon, any material change in the 
operating status of the RPM, including notice of all operating shutdowns or slowdowns 
that are expected to exceed five working days;<br>
(f) conduct itself in a professional and business-like manner in dealings with registered 
Collectors, Haulers, Processors, members of the public and OTS;<br>
(g) not use any trade-mark, trade name, or logo owned by OTS in any way not specifically 
authorised by OTS in writing, to provide samples of any proposed use to OTS for written 
approval which must be received before use begins, and to comply in all respects with 
any Guideline in effect from time to time regarding the use of OTS’s trade-marks;<br>
(h) at all times maintain clean and tidy premises and where it is necessary to store or 
stockpile TDPs, such is to be undertaken in an organized and safe fashion. In particular, 
but not limited to the generality of the foregoing, the RPM shall take reasonable steps to 
protect any TDPs from fire and leaching;<br>
(i) sell, transfer or dispose of all Manufactured Products in accordance with the letter and 
spirit of all protocols, treaties or agreements affecting international or interprovincial 
trade;<br>
(j) comply at all times with all laws issued by any government or governmental authority of 
Canada or any province of Canada, or any municipal, regional or other authority, 
including, without limitation, any governmental department, commission, bureau, board 
or administrative agency (“<strong>Applicable Laws</strong>”), which affect or govern the conduct and 
operation of the RPM, its business, and its performance under this Agreement; for greater 
clarity, the Applicable Laws include, without limitation, all laws relating to taxation, 
employment standards and compensation of workers, and the Environmental Laws;<br>
(k) obtain and maintain all permits, certificates, licences and other qualifications required 
under any Applicable Law;<br>
(l) update any registration information provided to OTS in the Registration System as soon 
as possible after the information is changed;<br>
(m) use equipment, supplies and service provided by OTS only for their intended purposes 
and in an efficient manner; and<br>
(n) file all required documents and reports in the manner directed by OTS from time to time.<br>
<strong>ARTICLE 3</strong><br>
<strong>NON-APPROVED SALES</strong><br>
3.1 <u><strong>Non-approved Sales</strong></u>. RPM shall not sell, transfer, dispose of or otherwise utilize TDPs in any 
manner not specifically provided for in this Agreement or the Plan (a “<strong>Non-approved Use</strong>”). 
RPM agrees that it shall have no entitlement to Manufacturing Incentives in respect of any TDP 
which is utilized for any Non-approved Use.<br>
<strong>ARTICLE 4</strong><br>
<strong>RPM’S REPRESENTATIONS AND WARRANTIES</strong><br>
4.1 <u><strong>Representations and Warranties</strong></u>. The RPM hereby represents and warrants to OTS that:<br>
(a) The RPM is duly constituted and is validly existing and in good standing under the laws 
of <strong>[Ontario]</strong>, and has the necessary corporate or other powers, authority and capacity to 
own its property and assets and to carry on the business as presently conducted and 
contemplated by the terms of this Agreement;<br>
(b) The RPM is not a non-resident of Canada within the meaning of Section 116 of the<br>
<u>Income Tax Act</u>;<br>
(c) The RPM holds all permits, licences, consents and authorities issued by all government 
or governmental authorities of Canada or any province of Canada, or any municipal, 
regional or other authority, including, without limitation, any governmental department, 
commission, bureau, board or administrative agency which are necessary and/or desirable 
in connection with the conduct and operation of the RPM’s business and is not in breach 
of or in default of any term or condition thereof; and<br>
(d) All documentation and other instruments provided to OTS by the RPM are complete and 
correct.<br>
(e) The registration of the RPM with OTS as an approved RPM, the provision of all required 
information to OTS, and the entering into of this Agreement by RPM and the 
performance of its obligations hereunder have been duly authorized by all necessary 
corporate action.<br>
4.2 <u><strong>Statements</strong></u>. All statements contained in any documents or other instruments delivered by or on 
behalf of the RPM to OTS shall be deemed to be representations and warranties of the RPM of 
the facts therein contained.<br>
4.3 <u><strong>Reliance</strong></u>. The RPM acknowledges and agrees that OTS has entered into this Agreement relying 
on the warranties, representations and other terms and conditions set out in this Agreement 
notwithstanding independent searches or investigations that may have been undertaken by or on 
behalf of OTS and that no information which is now known or should be known or which may 
hereinafter become known to OTS or its officers, directors or professional advisors shall limit or 
extinguish any right of indemnification contained herein or otherwise limit, restrict, negate or 
constitute a waiver of any of the rights or remedies of OTS hereunder.<br>
<strong>ARTICLE 5</strong><br>
<strong>OBLIGATIONS OF OTS</strong><br>
5.1 <u><strong>OTS Obligations</strong></u>. OTS shall:<br>
(a) pay all applicable Manufacturing Incentives to the RPM within 35 days of receipt of 
proof of sale, in accordance with the Plan and all applicable schedules put forth by OTS;<br>
and<br>
(b) provide OTS promotional and educational materials to the RPM.<br>
<strong>ARTICLE 6</strong><br>
<strong>INCENTIVE PAYMENTS TO THE RPM</strong><br>
6.1 <u><strong>Manufacturing Incentives</strong></u>. Manufacturing Incentives contemplated by this Agreement for 
payment to the RPM shall be based on the Manufacturing Incentives included in the Plan, as 
amended from time to time.<br>
6.2 <u><strong>Limitations on Manufacturing Incentives</strong></u>.<br>
(a) No Manufacturing Incentives will be paid to RPM with respect to Manufactured Products 
which:<br>
(i) contain TDPs derived from tires processed by a Processor before 1 September<br>
2009; or<br>
(ii) contain TDPs purchased by RPM before 1 September 2009; or<br>
(iii) were manufactured before 1 September 2009; or<br>
(iv) were held in RPM’s inventory prior to September 1st, 2009.<br>
(b) RPM shall have no entitlement to Manufacturing Incentives in respect of any TDPs 
originating outside Ontario, obtained by ROM directly or indirectly from any source 
outside Ontario, or derived from Used Tires originating outside Ontario, and RPM agrees 
that it shall not include any such non-Ontario TDPs in any request for the payment of 
Manufacturing Incentives, and that submission of such a request for payment with respect 
to non-Ontario TDPs shall constitute a False Statement.<br>
6.3 <u><strong>Third Party Monies</strong></u>. The RPM shall not collect monies on behalf of OTS from any other person 
but in the event that such does occur notwithstanding the requirements of this section, those 
monies shall be held for the benefit of, and remitted (without deduction or set-off), forthwith to 
OTS.<br>
6.4 <u><strong>Anti-dumping</strong></u>. OTS reserves the right at any time to reduce, refuse to pay or recover all or part 
of Manufacturing Incentives in respect of Manufactured Products sold or delivered to a person 
situated outside of the Province of Ontario (the “<strong>export jurisdiction</strong>”) which may, in the opinion 
of OTS, result in a violation of the laws of Canada or the export jurisdiction or which may result 
in the possibility of administrative or legal action against OTS, Waste Diversion Ontario or the 
Governments of Ontario or Canada or the possibility of trade sanctions against Manufactured 
Products originating from Ontario. Trade problems could include sale at local prices which are 
lower than Ontario prices (“dumping”) or sale at any price of subsidized goods which could 
trigger under local laws the right to take administrative or legal action, including the imposition 
of countervailing duties or interprovincial trade sanctions.<br>
<strong>ARTICLE 7</strong><br>
<strong>AUDITS AND INSPECTION</strong><br>
7.1 <u><strong>Inspection</strong></u>. The RPM agrees to permit OTS or its agents to inspect the RPM’s business site upon 
reasonable notice, during normal business hours, from time to time.<br>
7.2 <u><strong>Audit</strong></u>. The RPM agrees that OTS may, from time to time, audit any records of the RPM 
maintained in support of the RPM’s claims, and further, may examine and review, and audit 
records relating to the RPM’s compliance with the terms of this Agreement and the Plan 
(hereinafter referred to as the “<strong>Audit</strong>”).<br>
7.3 <u><strong>Provision of Records</strong></u>. The RPM shall provide OTS’s auditor with any and all records requested 
and shall cooperate with the Audit at no expense to OTS. It is agreed that OTS shall pay the costs 
of the Audit except in circumstances where the auditor determines that the RPM has not complied 
with the terms of this Agreement and the Plan in which case OTS’ reasonable costs of the Audit 
shall be paid by the the RPM within 30 days of the Audit results being transmitted from OTS to 
the RPM. In the event the RPM fails to pay OTS’ reasonable costs of the Audit as aforesaid, such 
costs of the Audit shall be treated as Repayment Amounts and subject to set-off in accordance 
with Section 8.3.<br>
<strong>ARTICLE 8</strong><br>
<strong>RPM DEFAULT</strong><br>
8.1 <u><strong>Events of Default</strong></u>. The occurrence of any of the following while this Agreement is in effect shall 
constitute an “<strong>Act of Default</strong>” by the RPM under this Agreement:<br>
(a) If the RPM fails to make payment of any amount required in this Agreement, including<br>
any Repayment Amount, when such payment becomes due and payable, and fails to pay 
such amount in full within five days of written demand therefor being sent by OTS;<br>
(b) If RPM breaches or fails to perform, observe or comply with any provision of this 
Agreement, the Plan, or any Guideline, and does not rectify such breach or failure to 
OTS’s reasonable satisfaction within 15 days of written notice of the breach or failure 
being sent by OTS;<br>
(c) If RPM defaults in the due observance or performance of any covenant, undertaking, 
obligation or agreement given to OTS at any time, whether contained in this Agreement, 
the Plan, or any Guideline, or otherwise, and RPM does not rectify such default to OTS’s 
reasonable satisfaction within 15 days of written notice of the breach or failure being sent 
by OTS;<br>
(d) If any representation, warranty, certificate, submission, statement or report made by RPM 
to OTS is in any respect untrue, erroneous, incomplete, inaccurate, misleading, 
inconsistent with any Audit, or not able to be supported by RPM’s records in an Audit, all 
in OTS’s sole determination (each a “<strong>False Statement</strong>”);<br>
(e) If RPM commits any default or breach under any other agreement between RPM and 
OTS;<br>
(f) If RPM conspires or colludes with or assists any other person in making any False 
Statement to OTS or obtain under false pretenses the payment of any amount from OTS;<br>
(g) If RPM fails to comply with any applicable law affecting the RPM’s operation;<br>
(h) If RPM is convicted of an offense under the <em>Environmental Protection Act</em> (Ontario);<br>
(i) If RPM becomes insolvent or bankrupt or subject to the Bankruptcy and Insolvency Act<br>
(Canada) or the <em>Companies Creditors Arrangement Act</em> (Canada), or goes into windingup 
or liquidation, either voluntarily or under an order of a court of competent jurisdiction, 
or makes a general assignment for the benefit of its creditors or otherwise acknowledges 
itself insolvent;<br>
(j) If any execution, sequestration, extent, or any other process of any court becomes 
enforceable against RPM or if a distress or analogous process is levied on the property 
and assets of the RPM; or<br>
(k) any proceedings shall be commenced for the winding-up, dissolution or liquidation of the 
RPM or under which the RPM could lose its corporate status, such proceedings not being 
bona fide opposed by the RPM within five days of the date of commencement or service 
on the RPM.<br>
8.2 <u><strong>Repayment of Payments Based on False Statements or with Respect to Non-approved Sales</strong></u>.<br>
RPM acknowledges and agrees that:<br>
(a) if any Manufacturing Incentives are paid to RPM as a result of or in connection with any 
False Statement of the RPM or any other person, in OTS’s sole determination, RPM shall 
forthwith upon demand repay the full amount of such payments to OTS, together with an 
amount equal to 10% thereof as a reasonable pre-estimate of OTS’s liquidated damages 
and administrative expenses arising therefrom;<br>
(b) if any payment or incentive of any kind is paid by OTS to any third party as a result of or 
in connection with any False Statement of the RPM, in OTS’s sole determination, RPM 
shall forthwith upon demand reimburse OTS for the full amount of such payments, 
together with an amount equal to 10% thereof as a reasonable pre-estimate of OTS’s
liquidated damages and administrative expenses arising therefrom; and<br>
(c) if any Manufacturing Incentives are paid to RPM in connection with any Non-approved 
Use of TDP, RPM shall forthwith upon demand repay the full amount of such payments 
to OTS, together with an amount equal to 10% thereof as a reasonable pre-estimate of 
OTS’s liquidated damages and administrative expenses arising therefrom. 
Amounts payable by RPM in accordance with this Article are collectively “<strong>Repayment 
Amounts</strong>”.<br>
8.3 <u><strong>Set-off</strong></u>. RPM agrees that OTS may set off Repayment Amounts owing by RPM to OTS against 
any future payments of any kind to RPM under this Agreement or any other agreement between 
RPM and OTS. If such future payment amounts are insufficient to recoup Repayment Amounts 
owed by RPM to OTS, RPM agrees that such Repayment Amounts are recoverable from RPM as 
liquidated damages.<br>
8.4 <u><strong>Consequences of Default</strong></u>. Immediately following any Act of Default, OTS may, in its sole and 
absolute discretion, do any, some or all of the following by written notice to the RPM:<br>
(a) suspend payment to the RPM of all incentives and other amounts hereunder, with 
immediate effect, until the Act of Default is resolved to OTS’s satisfaction;<br>
(b) require RPM to pay any Repayment Amount arising in connection with an Act of 
Default;<br>
(c) terminate this Agreement, with immediate effect;<br>
(d) exclude RPM from future participation in the Used Tires Program in any capacity, on a 
temporary or permanent basis.<br>
<strong>ARTICLE 9</strong><br>
<strong>TERM &amp; TERMINATION</strong><br>
9.1 <u><strong>Term</strong></u>. This Agreement shall commence on the Effective Date and continue thereafter until 
terminated as provided for herein.<br>
9.2 <u><strong>Termination by OTS</strong></u>. OTS may immediately terminate this Agreement by written notice to the 
RPM, in addition to any other remedies available at law or in equity, in any of the following 
events:<br>
(a) if the RPM commits an Act of Default;<br>
(b) if the Plan is terminated by the Minister or any other governmental authority, or the 
program agreement between OTS and WDO is terminated, provided that prior notice of 
such termination is communicated to the RPM as soon as it is available;<br>
(c) if the RPM transfers by sale, assignment, bequest, inheritance, by operation of law or 
other disposition, or shares issued by subscription or allotment, or shares cancelled or 
redeemed, so as to result in a change in the effective voting or other control of the RPM 
from the person or persons holding control on the date of execution of this Agreement 
without the written consent of OTS, such consent not to be unreasonably withheld; or<br>
(d) in the event any other legal proceeding involving the RPM is instituted that in the 
reasonable opinion of OTS materially impairs the ability of the RPM to discharge its 
obligations hereunder.<br>
9.3 <u><strong>Termination for Convenience</strong></u>. Either Party may terminate this Agreement for convenience 
upon 356 days’ written notice to the other Party.<br>
9.4 <u><strong>Incentive Payment Following Termination</strong></u>.<br>
(a) In the event of termination by OTS where RPM has committed an Act of Default, OTS 
may in its sole and absolute discretion cancel all payments of Manufacturing Incentives 
to the RPM which are pending as of the date on which notice of termination is given or 
which may arise at any time thereafter.<br>
(b) Upon termination of this Agreement, provided that payment has not been cancelled by 
OTS in accordance with Section 9.4(a), OTS shall continue to pay Manufacturing 
Incentives to the RPM with respect to Manufactured Products sold up to the day of 
termination of this Agreement (notwithstanding that claims for such Manufactured 
Products sold may be submitted to OTS after the termination of this Agreement).<br>
9.5 <u><strong>Processing Following Termination</strong></u>. Within ninety days of termination of this Agreement, 
unless otherwise directed by OTS, RPM shall manufacture all Inventory in its possession at the 
time of delivery of the notice of termination into Manufactured Products on the terms and 
conditions as set out in this Agreement and, provided that payment has not been cancelled by 
OTS in accordance with Section 9.4(a), OTS shall pay the Manufacturing Incentives to the RPM 
with respect to the sale of such Inventory notwithstanding the termination of this Agreement.<br>
<strong>ARTICLE 10</strong><br>
<strong>INDEMNITY &amp; INSURANCE</strong><br>
10.1 <u><strong>Indemnity</strong></u>. The RPM covenants and agrees with OTS to indemnify and hold harmless OTS, its 
directors, officers, employees and agents against all costs, charges, expenses, legal fees and any 
other losses or claims which OTS may hereinafter suffer, sustain or may incur or be compelled to 
pay as a result of any performance or non-performance by RPM of its obligations hereunder, or 
any claim, action or proceeding which is brought, prosecuted or threatened against OTS, its 
directors, officers, employees and agents for any act, deed or omission of the RPM arising from 
the breach of this Agreement, the Plan, or any applicable law.<br>
10.2 <u><strong>Release</strong></u>. The RPM, for itself, its successors and assigns, agrees to release OTS and its officers, 
directors, employees and agents from all manners of action, causes of action, claims, demands, 
losses, damages, charges, expenses and the like, of any nature whatsoever which the RPM ever 
had, now has or hereafter can, shall or may have against OTS and its officers, directors, 
employees and agents arising out of or in connection with this Agreement provided that all acts, 
deeds or omissions or the alleged acts, deeds or omissions in respect of which any action, cause 
of action, claim, demand, loss, damage, charge, expense and the like is based or performed in 
good faith, and when not performed or omitted to be performed fraudulently or in bad faith by 
OTS, its directors, officers, employees or agents.<br>
10.3 <u><strong>Insurance</strong></u>. RPM shall maintain comprehensive “occurrence” general liability insurance, 
including personal injury liability, property damage, contractual liability insurance and 
employer’s liability coverage, with minimum limits of liability of $5,000,000, containing a 
severability of interests and cross-liability clause, and deliver to OTS on request a certificate 
thereof with OTS named as an additional insured thereon.<br>
<strong>ARTICLE 11</strong><br>
<strong>LIMITATION OF LIABILITY</strong><br>
11.1 <u><strong>Exclusion of Liability</strong></u>. RPM acknowledges and agrees that at no time shall OTS take possession 
of any TDPs or Manufactured Product and that OTS shall not, in any event, be liable under any 
theory of liability to RPM, the previous or future owner(s) or user(s) of any TDPs or 
Manufactured Product or any other party or parties for any damages, losses, expenses, liabilities 
and/or other amounts of any nature or kind whatsoever, including without limitation, any direct, 
indirect, incidental, special, consequential, exemplary and/or punitive damages, arising out of or 
related to any loss, improper use, improper culling, improper transfer or sale, improper disposal 
or environmental degradation resulting, proceeding or connected in any way to TDPs or 
Manufactured Products.<br>
<strong>ARTICLE 12</strong><br>
<strong>PUBLICATION OF INFORMATION</strong><br>
12.1 <u><strong>Publication of Information</strong></u>. The RPM understands that its name, main contact information, and 
the registration number assigned to it by OTS, as well as information regarding the RPM’s 
operation, may be published by OTS on OTS’s website or other publically-accessible websites. 
OTS will take commercially reasonable and appropriate precautions to maintain the 
confidentiality of information in its database, but will not be liable to the RPM, or anyone 
claiming by, through or under it, for any losses, claims and damages arising out of negligent 
disclosure of any confidential information.<br>
<strong>ARTICLE 13</strong><br>
<strong>MODIFICATIONS TO PLAN</strong><br>
13.1 <u><strong>Modification to Plan</strong></u>. The parties agree and understand that the Plan may be revised from time 
to time without the input or consent of the RPM, and the RPM shall be bound by each revised 
version of the same as each revision may be issued, as though each was set out herein and formed 
a contractual obligation upon the RPM and the RPM covenants and agrees to abide by, comply 
with and satisfy such revised Plan.<br>
13.2 <u><strong>Notice</strong></u>. If the Plan or any part of it being cancelled or altered, OTS shall issue notice to that 
effect.<br>
13.3 <u><strong>Modification to Incentives</strong></u>. The incentives payable and the payment schedule implemented by 
OTS may be modified from time to time in OTS’s sole and absolute discretion. All changes will 
be posed on OTS’s internet web site no less than 60 days before the effective date of such change.<br>
<strong>ARTICLE 14</strong><br>
<strong>GENERAL</strong><br>
14.1 <u><strong>Assignment</strong></u>. The parties hereby agree that the RPM’s rights under this Agreement are not 
assignable or transferable, in any manner, without the prior written consent of OTS, which 
consent may not be unreasonably withheld.<br>
14.2 <u><strong>Agreement Binding</strong></u>. This Agreement shall enure to the benefit of and be binding on the parties, 
their heirs, legal personal representatives, successors and permitted assigns.<br>
14.3 <u><strong>Dispute Resolution</strong></u>. The parties agree that in the event of a dispute between them with respect to 
the terms or performance of this Agreement then such dispute shall first be subject to Mediation 
under Appendix 12 in the Plan, “<strong>Mediation Guidelines</strong>”, and if such dispute is not able to be 
resolved through mediation, then it shall be subject to arbitration under Appendix 13 in the Plan, 
“<strong>Arbitration Guidelines</strong>”.<br>
14.4 <u><strong>Notices</strong></u>. Any notice, determination, consent, request or other communication from one party to 
the other or others or other documents required or which may be given under this Agreement may 
be delivered or transmitted by means of electronic communication with confirmation of 
transmission, personal service, facsimile with confirmation of transmission or by prepaid first 
class postage to the party at the addresses, in the case of the RPM at the address on the 
registration form completed by the RPM and in the case of OTS at the address noted at the top of 
page 1 of this Agreement, to the attention of the “Executive Director”. Such notifications shall be 
deemed to have been received on the third day after posting and on the first day after the date of 
electronic or facsimile transmission, in each case which is not a Saturday, Sunday or public 
holiday in Ontario. In the event of a postal disruption, notices must be given by personal delivery, 
e-mail or by a signed back facsimile and all notices delivered by post within one week prior to the 
postal disruption must be confirmed by a signed back facsimile to be effective.<br>
14.5 <u><strong>Independent Contractors</strong></u>. The Parties hereto are and shall at all times in the performance of 
this Agreement be independent contractors and neither Party shall have the authority to assume or 
create any obligations expressed or implied, in the name of the other Party, nor to contractually 
bind the other Party in any manner whatsoever.<br>
14.6 <u><strong>Amendment</strong></u>. OTS retains the right to revise or amend this Agreement. OTS will give notice to 
the RPM of such change (the “<strong>Change Notice</strong>”). Unless the RPM gives notice to OTS (the 
“<strong>Rejection Notice</strong>”) within 45 days of receipt of the Change Notice that the RPM does not accept 
the revisions or amendments in the Change Notice, this Agreement, as amended, remains in effect 
and is binding. If the RPM gives a Rejection Notice to OTS, this Agreement shall be terminated 
30 days after the delivery by the RPM of the Rejection Notice and the RPM will forgo its 
approval status and not be compensated under the OTS program.<br>
14.7 <u><strong>Waiver</strong></u>. No failure by any of the parties to insist on strict performance of any covenant, 
agreement, term or condition (the “<strong>provision</strong>”) of this Agreement, or to exercise any right or 
remedy consequent on the breach of any provision, and no acceptance of partial payment during 
the continuance of any such breach, shall constitute a waiver of any such breach or provision. No 
waiver of any breach shall affect or alter this Agreement, but each and every provision of this 
Agreement shall continue in full force and effect with respect to any other then existing or 
subsequent breach of such provision.<br>
14.8 <u><strong>Severability</strong></u>. If any provision of this Agreement or the application of the provision to any 
circumstances shall be held to be invalid or unenforceable, then the remaining provisions of this 
Agreement or the application of them to other circumstances shall not be affected by the 
invalidity or unenforceability and shall be valid and enforceable to the fullest extent permitted by 
law.<br>
14.9 <u><strong>Entire Agreement</strong></u>. This Agreement constitutes the entire agreement among the parties with 
respect to its subject matter and supersedes all prior and contemporaneous agreements, 
understandings, negotiations and discussions, whether oral or written, of the parties. There are no 
warranties, representations or other agreements among the parties in connection with the subject 
matter of this Agreement, except as specifically set forth in it. Except as specifically provided 
herein, no supplement, modification, waiver or termination of this Agreement shall be binding 
unless executed in writing by the parties to be bound by it.<br>
14.10 <u><strong>Remedies</strong></u>. No remedy herein conferred upon or reserved in favour of any party hereto shall 
exclude any other remedy herein or existing at law or in equity or by statute, but each shall be 
cumulative and in addition to every other remedy given hereunder or now or hereinafter existing.<br>
14.11 <u><strong>Force Majeure</strong></u>. Neither party shall be liable for delay or failure in performance resulting from 
acts beyond the control of such party, including but not limited to Acts of God, acts of war, riot, 
fire, flood or other disaster, acts of government, strike, lockout or communication line or power 
failure.<br>
14.12 <u><strong>Governing Law</strong></u>. This Agreement shall be construed and enforced in accordance with, and the 
rights of the parties shall be governed by, the laws in force in the Province of Ontario.<br>
14.13 <u><strong>Headings</strong></u>. The headings used throughout this Agreement are solely for convenience of the parties 
and are not to be used as an aid in the interpretation of this Agreement.<br>
14.14 <u><strong>Time of Essence</strong></u>. Time shall be of the essence of this Agreement and every part of it.<br>
14.15 <u><strong>Survival</strong></u>. All provisions of this Agreement which are expressed or which by their nature are 
intended to survive termination of this Agreement shall survive termination, and continue to bind 
the parties.<br>
14.16 <u><strong>Electronic Commerce</strong></u>. Any execution or amendment of this agreement which is conducted 
electronically by any of the parties is made in accordance with and governed by the <em>Electronic 
Commerce Act</em>, 2000, (Ontario). If this Agreement is executed on behalf of any party 
electronically, the natural person who selects the “Agree” button at the bottom of the 
“<strong>Agreement Ratification</strong>” page on OTS’s web site on behalf of the executing party certifies that 
by selecting the “Agree” button, the natural person represents and warrants that he or she is at 
least eighteen years of age, and has been duly appointed with the authority to bind the executing 
party.
</p>
	')
end

go

update a
set a.TermsAndConditionsID=t.ID
from [Application] a
    inner join ApplicationInvitation ai
	on ai.ApplicationID=a.ID
	inner join TermsAndConditions t
	on t.ApplicationTypeID=ai.ParticipantTypeID
go


if not exists (select * from sysobjects where name='AdminSectionNotes' And xtype='U')
begin
	CREATE TABLE [dbo].[AdminSectionNotes](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Category] [nvarchar](50) NOT NULL,
		[Note] [varchar](max) NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserID] [bigint] NOT NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_AdminNote] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[AdminSectionNotes]  WITH CHECK ADD  CONSTRAINT [FK_AdminSectionNote_User] FOREIGN KEY([UserID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[AdminSectionNotes] CHECK CONSTRAINT [FK_AdminSectionNote_User]
end
go

