﻿--delete existing [dbo].[sp_InitializeClaimPeriod]
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_InitializeClaimPeriod]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_InitializeClaimPeriod]
END
GO

--create new [dbo].[sp_InitializeClaimPeriod]
CREATE PROCEDURE [dbo].[sp_InitializeClaimPeriod] 
 @claimYear as int
AS
BEGIN
	declare @counter int;
	SET NOCOUNT ON;
	IF NOT EXISTS (select top 1 * from period where YEAR(startdate)=@claimYear)
    Begin
		--Insert Collector claim period
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Jan.-Mar.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-01-01' as datetime), cast(cast(@claimYear as varchar)+'-03-31' as datetime), cast(cast(@claimYear as varchar)+'-04-01' as datetime),cast(cast(@claimYear as varchar)+'-06-30' as datetime), 1,1, 0, 0);
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Apr.-Jun.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-04-01' as datetime), cast(cast(@claimYear as varchar)+'-06-30' as datetime), cast(cast(@claimYear as varchar)+'-07-01' as datetime),cast(cast(@claimYear as varchar)+'-09-30' as datetime),1,1, 0, 0);
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Jul.-Sep.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-07-01' as datetime), cast(cast(@claimYear as varchar)+'-09-30' as datetime), cast(cast(@claimYear as varchar)+'-10-01' as datetime),cast(cast(@claimYear as varchar)+'-12-31' as datetime),1,1, 0, 0);
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Oct.-Dec.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-10-01' as datetime), cast(cast(@claimYear as varchar)+'-12-31' as datetime), cast(cast(@claimYear+1 as varchar)+'-01-01' as datetime),cast(cast(@claimYear+1 as varchar)+'-03-31' as datetime),1,1, 0, 0);

		--Insert hauler, Processor, RPM, Steward cliam period
		set @counter=1;
		while @counter < 6 
		begin
		if  @counter != 2
			begin
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Jan.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-01-01' as datetime), cast(cast(@claimYear as varchar)+'-01-31' as datetime), cast(cast(@claimYear as varchar)+'-02-01' as datetime),cast(cast(@claimYear as varchar)+'-02-28' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Feb.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-02-01' as datetime), DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,cast(cast(@claimYear as varchar)+'-02-01' as datetime))+1,0)), cast(cast(@claimYear as varchar)+'-03-01' as datetime),cast(cast(@claimYear as varchar)+'-03-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Mar.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-03-01' as datetime), cast(cast(@claimYear as varchar)+'-03-31' as datetime), cast(cast(@claimYear as varchar)+'-04-01' as datetime),cast(cast(@claimYear as varchar)+'-04-30' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Apr.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-04-01' as datetime), cast(cast(@claimYear as varchar)+'-04-30' as datetime), cast(cast(@claimYear as varchar)+'-05-01' as datetime),cast(cast(@claimYear as varchar)+'-05-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'May.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-05-01' as datetime), cast(cast(@claimYear as varchar)+'-05-31' as datetime), cast(cast(@claimYear as varchar)+'-06-01' as datetime),cast(cast(@claimYear as varchar)+'-06-30' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Jun.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-06-01' as datetime), cast(cast(@claimYear as varchar)+'-06-30' as datetime), cast(cast(@claimYear as varchar)+'-07-01' as datetime),cast(cast(@claimYear as varchar)+'-07-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Jul.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-07-01' as datetime), cast(cast(@claimYear as varchar)+'-07-31' as datetime), cast(cast(@claimYear as varchar)+'-08-01' as datetime),cast(cast(@claimYear as varchar)+'-08-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Aug.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-08-01' as datetime), cast(cast(@claimYear as varchar)+'-08-31' as datetime), cast(cast(@claimYear as varchar)+'-09-01' as datetime),cast(cast(@claimYear as varchar)+'-09-30' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Sep.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-09-01' as datetime), cast(cast(@claimYear as varchar)+'-09-30' as datetime), cast(cast(@claimYear as varchar)+'-10-01' as datetime),cast(cast(@claimYear as varchar)+'-10-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Oct.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-10-01' as datetime), cast(cast(@claimYear as varchar)+'-10-31' as datetime), cast(cast(@claimYear as varchar)+'-11-01' as datetime),cast(cast(@claimYear as varchar)+'-11-30' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Nov.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-11-01' as datetime), cast(cast(@claimYear as varchar)+'-11-30' as datetime), cast(cast(@claimYear as varchar)+'-12-01' as datetime),cast(cast(@claimYear as varchar)+'-12-31' as datetime),1,1, 0, 0);
				insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
				values(@counter,'Dec.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-12-01' as datetime), cast(cast(@claimYear as varchar)+'-12-31' as datetime), cast(cast(@claimYear+1 as varchar)+'-01-01' as datetime),cast(cast(@claimYear as varchar)+'-01-31' as datetime),1,1, 0, 0);
			end
			set @counter = @counter+1;
		end 

		-- Update SubmitStart and SubmitEnd
		update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=3 and YEAR(startdate)=@claimYear; 

		-- Update SubmitStart and SubmitEnd for Collector
		update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,3,EndDate))+1,0)) where PeriodType=2 and YEAR(startdate)=@claimYear;

		-- Update SubmitStart and SubmitEnd for Steward
		update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=1 and YEAR(startdate)=@claimYear;

		-- Update SubmitStart and SubmitEnd for Processor and RPM
		update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,2,EndDate))+1,0)) where (PeriodType=4 or PeriodType=5) and YEAR(startdate)=@claimYear;
	End
END

GO

--update existing claim period for Processor and RPM

update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,2,EndDate))+1,0)) where (PeriodType=4 or PeriodType=5) and YEAR(startdate)=2016;
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,2,EndDate))+1,0)) where (PeriodType=4 or PeriodType=5) and YEAR(startdate)=2017;

--update existing claim period for Collector
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,3,EndDate))+1,0)) where PeriodType=2 and YEAR(startdate)=2016;
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,3,EndDate))+1,0)) where PeriodType=2 and YEAR(startdate)=2017;

--update existing claim period for Haulser
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=3 and YEAR(startdate)=2016;
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=3 and YEAR(startdate)=2017;

--update existing claim period for Steward
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=1 and YEAR(startdate)=2016;
update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,1,EndDate))+1,0)) where PeriodType=1 and YEAR(startdate)=2017;
GO