﻿USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_Rpt_TsfExtractInBatchOnlyReportGp]    Script Date: 11/11/2016 3:54:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_Rpt_TsfExtractInBatchOnlyReportGp] 
AS
BEGIN
	select 
		B.ID BatchNumber,
		concat('Batch#', B.ID, ' Steward# ', c.RegistrationNumber, ' Claim Period ', convert(varchar(12),p.EndDate)) as 'BatchDesc',
		claim.ID as 'TsfClaimSummaryId', 
		p.StartDate as 'ReportingPeriod', 
		c.RegistrationNumber, 
		claim.ReceiptDate,
		claim.DepositDate as 'DepositDate',   
		claim.ChequeReferenceNumber 'ChequeNumber',
		claim.PaymentAmount 'ChequeAmount',
		claim.TotalTSFDue as 'TSF Before HST',
		claim.TotalTSFDue - coalesce(claim.Credit, 0) as 'totalRemittancePayable',
		be.GpiStatusID
		--  G.gpistatus_title as 'gpi_status'
	from GpiBatch B inner join
	GpiBatchEntry be on be.GpiBatchID = b.ID inner join 
	TSFClaim claim on claim.ID = be.GpiBatchEntryDataKey inner join
	Customer c on c.RegistrationNumber = claim.RegistrationNumber inner join
	Period p on p.ID = claim.PeriodID
	where GpiTypeID = 'S' 
	--and (Credit is not null) and Credit != 0 -- for debug only
END
GO


