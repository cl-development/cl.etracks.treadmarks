﻿USE [tmdb]

--== OTSTM2-530 begin ==--
update [TSFClaim] set ReceiptDate=DATEADD(hh,4,ReceiptDate) where CONVERT (time, ReceiptDate) ='00:00:00.0000000'
update [TSFClaim] set DepositDate=DATEADD(hh,4,DepositDate) where CONVERT (time, DepositDate) ='00:00:00.0000000'
--== OTSTM2-530 end ==--


--== OTSTM2-15 begin ==--
IF COL_LENGTH('Transaction', 'RetailPrice') IS NULL
BEGIN
    ALTER TABLE [Transaction]
    ADD [RetailPrice] DECIMAL(18,3) NULL;
END
--== OTSTM2-15 end ==--

--== OTSTM2-370 begin ==--
IF COL_LENGTH('TSFClaim', 'CurrencyType') IS NULL
BEGIN
    ALTER TABLE [TSFClaim]
    ADD [CurrencyType]  [varchar](5) NOT NULL DEFAULT('CAD');
END
--== OTSTM2-370 end ==--

--== Notification and Activity Name Length change from 100 to 150 begin ==--
alter table Notification alter column AssigneeName nvarchar(150) not null
alter table Notification alter column AssignerName nvarchar(150) not null
alter table Activity alter column InitiatorName nvarchar(150) not null
alter table Activity alter column AssigneeName nvarchar(150) not null
--== Notification and Activity Name Length change from 100 to 150 end ==--

GO

--== OTSTM2-561 begin ==--
update application set ExpireDate='2016-11-08' where id in (
select id from application 
where id in (select ApplicationID from ApplicationInvitation where ApplicationID in (select id from application where SubmittedDate is null and Status='Open') and (InvitationDate is null or InvitationDate<'2016-11-01')));
--== OTSTM2-561 end ==--
GO