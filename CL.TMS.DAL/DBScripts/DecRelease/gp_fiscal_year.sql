UPDATE GpFiscalYear
SET EffectiveEndDate = '2017-01-30'
WHERE EffectiveEndDate = '2999-12-31';

INSERT INTO GpFiscalYear (MinimumDocDate, EffectiveStartDate, EffectiveEndDate)
VALUES('2017-01-01', '2017-01-31', '2999-12-31');