﻿/*OTSTM2-486*/
INSERT INTO VendorSupportingDocument (VendorId, TypeID, [Option])
SELECT vS.VendorID as VendorId, '9', 'optionmail'
from VendorSupportingDocument vS left join Vendor v on vS.VendorID = v.ID
where v.VendorType = 2
group by vS.VendorID 

/*OTSTM2-486*/
INSERT INTO ApplicationSupportingDocument (ApplicationId, TypeID, [Option])
SELECT aSup.ApplicationID as ApplicationId, '9', 'optionmail'
from ApplicationSupportingDocument aSup left join Application a on aSup.ApplicationId = a.ID
join Vendor V on V.ApplicationID = aSup.ApplicationID
where V.VendorType = 2
group by aSup.ApplicationId 