USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_HaulerCollectorComparisonReport]    Script Date: 5/26/2016 10:56:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_HaulerCollectorComparisonReport] 
	@startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
BEGIN
	select 
	RegistrationNumberH,
	ClaimPeriodStartH,
	StatusH,
	t.FriendlyId as FormNumberH,
	t.ProcessingStatus as 'TransactionStatusH',
	RegistrationNumberC as CollectorNumberH,
	coalesce(Plt,0) as pltH, 
	coalesce(Mt,0) as mtH, 
	coalesce(Agls,0) as aglsH, 
	coalesce(Ind,0) as indH, 
	coalesce(Sotr,0) as sotrH, 
	coalesce(Motr,0) as motrH, 
	coalesce(Lotr,0) as lotrH, 
	coalesce(Gotr,0) as gotrH,
	(coalesce(items.plt,0) + coalesce(items.mt,0) + coalesce(items.agls,0) + coalesce(items.ind,0) + coalesce(items.sotr,0) + coalesce(items.motr,0) + coalesce(items.lotr ,0)+ coalesce(items.gotr,0)) as 'TotalH',
	RegistrationNumberC,
	ClaimPeriodStartC,
	StatusC,
	case when RegistrationNumberC is null
		then null
		else t.FriendlyId 
	end  FormNumberC,
	t.ProcessingStatus as 'TransactionStatusC',
	case when RegistrationNumberC is null
		then null
		else coalesce(plt, 0)
	end  pltC, 
	case when RegistrationNumberC is null
		then null
		else coalesce(Mt, 0)
	end  mtC, 
	case when RegistrationNumberC is null
		then null
		else coalesce(Agls, 0)
	end aglsC,
	case when RegistrationNumberC is null
		then null
		else coalesce(Ind, 0)
	end indC,
	case when RegistrationNumberC is null
		then null
		else coalesce(Sotr, 0) 
	end sotrC, 
	case when RegistrationNumberC is null
		then null
		else coalesce(Motr, 0)
	end motrC,
	case when RegistrationNumberC is null
		then null
		else coalesce(Lotr, 0)
	end lotrC, 
	case when RegistrationNumberC is null
		then null
		else coalesce(Gotr, 0)
	end gotrC,
	case when RegistrationNumberC is null
		then null
		else 	(coalesce(items.plt,0) + coalesce(items.mt,0) + coalesce(items.agls,0) + coalesce(items.ind,0) + coalesce(items.sotr,0) + coalesce(items.motr,0) + coalesce(items.lotr ,0)+ coalesce(items.gotr,0)) 
	end 'TotalC',
	case when RegistrationNumberC is null
		then null
		else 0
	end 'Difference'
	from
	(
		select 
			hauler.RegistrationNumber as RegistrationNumberH,
			hauler.ClaimPeriodStart as ClaimPeriodStartH,
			hauler.[Status] as StatusH,
			collector.RegistrationNumber as RegistrationNumberC,
			collector.ClaimPeriodStart as ClaimPeriodStartC,
			collector.[Status] as StatusC,
			hauler.TransactionId
		from
		(
			select 
				v.Number as RegistrationNumber,
				convert(nvarchar(10),p.StartDate, 101) as ClaimPeriodStart,
				cd.TransactionId as TransactionId,
				-- t.FriendlyId as FormNumber,
				c.[Status] as 'Status'
			from 
			[Claim] c inner join
			[Period] p on p.ID = c.ClaimPeriodID inner join 
			[Vendor] v on v.ID = c.ParticipantID inner join 
			[ClaimDetail] cd on cd.ClaimId = c.ID 
			where v.VendorType in (2)
		) as collector full outer join 
			(
				select 
					v.Number as RegistrationNumber,
					convert(nvarchar(10),p.StartDate, 101) as ClaimPeriodStart,
					cd.TransactionId as TransactionId,
					-- t.FriendlyId as FormNumber,
					c.[Status] as 'Status'
				from
				[Claim] c inner join
				[Period] p on p.ID = c.ClaimPeriodID inner join 
				[Vendor] v on v.ID = c.ParticipantID inner join 
				[ClaimDetail] cd on cd.ClaimId = c.ID 
				where v.VendorType in (3)
			) as hauler on hauler.TransactionId = collector.TransactionId
	) as participantSummary inner join
	[Transaction] t on t.ID = participantSummary.TransactionId inner join
	(
		select TransactionID, 
		sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
		from 
		(
			select piv.TransactionID, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
			from
			(
				select ti.TransactionId as TransactionID, ti.quantity, i.*
				from 
				TransactionItem ti left join 
				TransactionAdjustment ta on ta.id = ti.TransactionAdjustmentId				
				inner join Item i on i.id = ti.ItemID
				where ta.[Status] = 'Accepted'

				union

				select ti.TransactionId as TransactionID, ti.Quantity, i.*
				from 
				TransactionItem ti left join 
				TransactionAdjustment ta on ta.id = ti.TransactionAdjustmentId				
				left join Item i on i.id = ti.ItemID
				where ta.[Status] is null
				and ti.transactionid not in (
					select ta.OriginalTransactionId
					from TransactionAdjustment ta
					where ta.[Status] = 'Accepted' 
				)
			) src
			PIVOT
			(
				sum(Quantity)
				for ShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
			) piv
		) as items
		group by TransactionID
	) as items on items.TransactionID = t.ID 
	where t.TransactionTypeCode in ('TCR', 'DOT')
	and t.[status] = 'Completed'
	and (@startDate <= ClaimPeriodStartH or @startDate is null)
	and (@endDate >= ClaimPeriodStartH or @endDate is null)
	and (@registrationNumber = RegistrationNumberH or @registrationNumber is null)
	ORDER BY RegistrationNumberH, ClaimPeriodStartH, FormNumberH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_ProcessorTIPIReport]    Script Date: 5/26/2016 10:56:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_ProcessorTIPIReport] 
	@startDate DateTime, @endDate DateTime
AS
BEGIN
	select 
	v.Number as 'Processor',
	convert(nvarchar(10),p.StartDate, 101) as 'ClaimPeriod',
	1 as 'SubmissionNumber',
	c.[Status] as 'RecordState',
	CAST(coalesce(cp.totalTiAmount, 0.00) as decimal(10,2)) as 'TIPI',
	CAST((coalesce(c.ClaimsAmountTotal,0) - coalesce(cp.totalTiAmount, 0.00) - coalesce(c.AdjustmentTotal,0)) as decimal(10,2)) as 'PI',
	coalesce(c.AdjustmentTotal,0) as 'Adjustments',	
	CAST(coalesce(c.ClaimsAmountTotal,0) as decimal(10, 2)) as 'Payment'
	from Claim c left join   
	(
		select ClaimID, SUM( CAST(c.Rate*c.Weight as decimal(10,2))) as totalTiAmount
		from ClaimPayment c
		where c.PaymentType = 6 -- PTR
		GROUP BY c.ClaimID
	) cp on cp.ClaimID = c.ID inner join 
	Period p on p.ID = c.ClaimPeriodID inner join Vendor v on v.ID = c.ParticipantID
	where v.VendorType = 4
	and (@startDate <= p.StartDate or @startDate is null)
	and (@endDate >= p.StartDate or @endDate is null)
	order by v.Number, p.StartDate;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_RegistrantWeeklyReport]    Script Date: 5/26/2016 10:56:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_RegistrantWeeklyReport] 
AS
BEGIN

	select *
	from
	(
		select 	
		v.ID, v.VendorType as RegistrantType, v.Number as RegistrationNumber, v.BusinessName, v.FranchiseName, v.OperatingName, v.BusinessNumber, v.TerritoryCode, v.GlobalDimension1Code, v.GlobalDimension2Code, v.CreditLimit, v.TelexAnswerBack, v.TaxRegistrationNo, v.TaxRegistrationNo2, v.HomePage, v.BusinessGroupCode, v.CustomerGroupCode, v.DepartmentCode, v.ProjectCode, v.PurchaserCode, v.SalesCampaignCode, v.SalesPersonCode, v.AuthSigComplete, v.ReceivedDate, v.GracePeriodStartDate, v.GracePeriodEndDate, v.CommercialLiabInsurerName, v.CommercialLiabInsurerExpDate, v.WorkerHealthSafetyCertNo, v.PermitsCertDescription, v.AmendedAgreement, v.AmendedDate, v.IsTaxExempt, v.UpdateToFinancialSoftware, v.PrimaryBusinessActivity, v.MailingAddressSameAsBusiness, v.BusinessActivityID, v.GSTNumber, v.AuthorizedSigComplete, v.LocationEmailAddress, 
		coalesce(v.DateReceived, (select top 1 SubmittedDate from [application] a where a.id = v.ApplicationID)) as DateReceived, 
		v.ActivationDate, 
		coalesce(v.ConfirmationMailedDate, v.CreatedDate) as 'ConfirmationMailedDate', 	
		v.PreferredCommunication, v.RegistrantTypeId, v.RegistrantSubTypeId, v.LastUpdatedBy, v.LastUpdatedDate, v.CheckoutNum, v.CreatedDate, v.InBatch, v.IsActive, v.ActiveStateChangeDate,
		businessAddress.*,
		mailingAddress.*,
		primaryContact.*,
		coalesce([PLT], 0) as PLT, coalesce([MT], 0) as MT, coalesce([AGLS], 0) as AGLS, coalesce([IND], 0) as IND, coalesce([SOTR], 0) as SOTR, coalesce([MOTR], 0) as MOTR, coalesce([LOTR], 0) as LOTR, coalesce([GOTR], 0) as GOTR
		from vendor v left join 
		(
			select va.VendorID as PrimaryContactVendorID, 
			c.[AddressID],c.[Name],c.[Position],c.[Email],c.[IsPrimary],c.[PhoneNumber],c.[AlternatePhoneNumber],c.[FaxNumber],c.[PreferredContactMethod],c.[ContactAddressSameAsBusiness],c.[Ext],c.[FirstName],c.[LastName],
			a.ID as PrimaryContactID, a.AddressType as PrimaryContactAddressType, a.Address1 as PrimaryContactAddress1, a.Address2 as PrimaryContactAddress2,a.Address3 as PrimaryContactAddress3, a.City as PrimaryContactCity, a.Province as PrimaryContactProvince,a.PostalCode as PrimaryContactPostalCode, a.Country as PrimaryContactCountry, a.Phone as PrimaryContactPhone, a.Fax as PrimaryContactFax, a.Ext as PrimaryContactExt, a.Email as PrimaryContactEmail
			from VendorAddress va inner join [Contact] c on va.AddressID = c.AddressID left join 
			[Address] a on a.ID = va.AddressID
			where c.IsPrimary = 1
		) as primaryContact on v.ID = primaryContact.PrimaryContactVendorID left join 
		(
			select va.VendorID as BusinessVendorID, 
			a.ID as BusinessID, a.AddressType as BusinessAddressType, a.Address1 as BusinessAddress1, a.Address2 as BusinessAddress2,a.Address3 as BusinessAddress3, a.City as BusinessCity, a.Province as BusinessProvince,a.PostalCode as BusinessPostalCode, a.Country as BusinessCountry, a.Phone as BusinessPhone, a.Fax as BusinessFax, a.Ext as BusinessExt, a.Email as BusinessEmail
			from VendorAddress va inner join [Address] a on va.AddressID = a.ID
			where a.AddressType = 1
		) as businessAddress on v.ID = businessAddress.BusinessVendorID left join
		(
			select va.VendorID as MailingAddressID, 
			a.ID as MailingID, a.AddressType as MailingAddressType, a.Address1 as MailingAddress1, a.Address2 as MailingAddress2,a.Address3 as MailingAddress3, a.City as MailingCity, a.Province as MailingProvince,a.PostalCode as MailingPostalCode, a.Country as MailingCountry, a.Phone as MailingPhone, a.Fax as MailingFax, a.Ext as MailingExt, a.Email as MailingEmail,a.RowVersion as MailingRowVersion
			from VendorAddress va inner join [Address] a on va.AddressID = a.ID
			where a.AddressType = 2
		) as mailingAddress on v.ID = mailingAddress.MailingAddressID left join 
		(
			select itemVendorId, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from 
			(
				select piv.ItemVendorID, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select vi.VendorID as ItemVendorID, i.*
					from VendorItem vi left join Item i on i.id = vi.ItemID
				) src
				PIVOT
				(
					count(ShortName)
					for ShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv
			) as items
			group by itemVendorID
		) as items on items.ItemVendorID = v.ID
		union
		select 	c.ID, c.CustomerType as RegistrantType, c.RegistrationNumber, c.BusinessName, c.FranchiseName, c.OperatingName, c.BusinessNumber, c.TerritoryCode, c.GlobalDimension1Code, c.GlobalDimension2Code, c.CreditLimit, c.TelexAnswerBack, c.TaxRegistrationNo, c.TaxRegistrationNo2, c.HomePage, c.BusinessGroupCode, c.CustomerGroupCode, c.DepartmentCode, c.ProjectCode, c.PurchaserCode, c.SalesCampaignCode, c.SalesPersonCode, c.AuthSigComplete, c.ReceivedDate, c.GracePeriodStartDate, c.GracePeriodEndDate, c.CommercialLiabInsurerName, c.CommercialLiabInsurerExpDate, c.WorkerHealthSafetyCertNo, c.PermitsCertDescription, c.AmendedAgreement, c.AmendedDate, c.IsTaxExempt, c.UpdateToFinancialSoftware, c.PrimaryBusinessActivity, c.MailingAddressSameAsBusiness, c.BusinessActivityID, c.GSTNumber, c.AuthorizedSigComplete, c.LocationEmailAddress, 
		coalesce(c.DateReceived, (select top 1 SubmittedDate from [application] a where a.id = c.ApplicationID)) as 'DateReceived', 	
		c.ActivationDate, 
		coalesce(c.ConfirmationMailedDate, c.CreatedDate) as 'ConfirmationMailedDate', 		
		c.PreferredCommunication, c.RegistrantTypeId, c.RegistrantSubTypeId, c.LastUpdatedBy, c.LastUpdatedDate, c.CheckoutNum, c.CreatedDate, c.InBatch, c.IsActive, c.ActiveStateChangeDate,
		businessAddress.*,
		mailingAddress.*,
		primaryContact.*,
		coalesce([PLT], 0) as PLT, coalesce([MT], 0) as MT, coalesce([AGLS], 0) as AGLS, coalesce([IND], 0) as IND, coalesce([SOTR], 0) as SOTR, coalesce([MOTR], 0) as MOTR, coalesce([LOTR], 0) as LOTR, coalesce([GOTR], 0) as GOTR
		from customer c left join 
		(
			select ca.CustomerID as PrimaryContactVendorID, 
			c.[AddressID],c.[Name],c.[Position],c.[Email],c.[IsPrimary],c.[PhoneNumber],c.[AlternatePhoneNumber],c.[FaxNumber],c.[PreferredContactMethod],c.[ContactAddressSameAsBusiness],c.[Ext],c.[FirstName],c.[LastName],
			a.ID as PrimaryContactID, a.AddressType as PrimaryContactAddressType, a.Address1 as PrimaryContactAddress1, a.Address2 as PrimaryContactAddress2,a.Address3 as PrimaryContactAddress3, a.City as PrimaryContactCity, a.Province as PrimaryContactProvince,a.PostalCode as PrimaryContactPostalCode, a.Country as PrimaryContactCountry, a.Phone as PrimaryContactPhone, a.Fax as PrimaryContactFax, a.Ext as PrimaryContactExt, a.Email as PrimaryContactEmail
			from CustomerAddress ca inner join [Contact] c on ca.AddressID = c.AddressID left join 
			[Address] a on a.ID = ca.AddressID
			where c.IsPrimary = 1
		) as primaryContact on c.ID = primaryContact.PrimaryContactVendorID left join 
		(
			select ca.CustomerID as BusinessCustomerID,
			a.ID as BusinessID, a.AddressType as BusinessAddressType, a.Address1 as BusinessAddress1, a.Address2 as BusinessAddress2,a.Address3 as BusinessAddress3, a.City as BusinessCity, a.Province as BusinessProvince,a.PostalCode as BusinessPostalCode, a.Country as BusinessCountry, a.Phone as BusinessPhone, a.Fax as BusinessFax, a.Ext as BusinessExt, a.Email as BusinessEmail
			from CustomerAddress ca inner join [Address] a on ca.AddressID = a.ID
			where a.AddressType = 1
		) as businessAddress on c.ID = businessAddress.BusinessCustomerID left join
		(
			select ca.CustomerID as MailingCustomerID, 
			a.ID as MailingID, a.AddressType as MailingAddressType, a.Address1 as MailingAddress1, a.Address2 as MailingAddress2,a.Address3 as MailingAddress3, a.City as MailingCity, a.Province as MailingProvince,a.PostalCode as MailingPostalCode, a.Country as MailingCountry, a.Phone as MailingPhone, a.Fax as MailingFax, a.Ext as MailingExt, a.Email as MailingEmail,a.RowVersion as MailingRowVersion
			from CustomerAddress ca inner join [Address] a on ca.AddressID = a.ID
			where a.AddressType = 2
		) as mailingAddress on c.ID = mailingAddress.MailingCustomerID left join
		(
			select itemCustomerId, 
			sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from 
			(
				select piv.ItemCustomerID, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select ci.CustomerID as ItemCustomerID, i.*
					from CustomerItem ci left join Item i on i.id = ci.ItemID
				) src
				PIVOT
				(
					count(ShortName)
					for ShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv
			) as items
			group by itemCustomerID
		) as items on items.ItemCustomerID = c.ID
	) main 
	order by CreatedDate asc, RegistrationNumber;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_TsfExtractInBatchOnlyReportGp]    Script Date: 5/26/2016 10:56:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_TsfExtractInBatchOnlyReportGp] 
AS
BEGIN
	select 
		B.ID BatchNumber,
		concat('Batch#', B.ID, ' Steward# ', c.RegistrationNumber, ' Claim Period ', convert(varchar(12),p.EndDate)) as 'BatchDesc',
		claim.ID as 'TsfClaimSummaryId', 
		p.StartDate as 'ReportingPeriod', 
		c.RegistrationNumber, 
		claim.ReceiptDate,
		claim.DepositDate as 'DepositDate',   
		claim.ChequeReferenceNumber 'ChequeNumber',
		claim.PaymentAmount 'ChequeAmount',
		claim.TotalTSFDue as 'TSF Before HST',
		claim.TotalTSFDue  as 'totalRemittancePayable',
		be.GpiStatusID
		--  G.gpistatus_title as 'gpi_status'
	from GpiBatch B inner join
	GpiBatchEntry be on be.GpiBatchID = b.ID inner join 
	TSFClaim claim on claim.ID = be.GpiBatchEntryDataKey inner join
	Customer c on c.RegistrationNumber = claim.RegistrationNumber inner join
	Period p on p.ID = claim.PeriodID
	where GpiTypeID = 'S' 
END

GO
