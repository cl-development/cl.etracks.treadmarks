USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRegistrantSpotlights]    Script Date: 7/14/2016 1:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetRegistrantSpotlightsPaginate] 
	@keyWords nvarchar(255), @start int, @pageLimit int
AS
BEGIN
	DECLARE @wordCount int = (SELECT count(*) FROM [dbo].[fnSplitSpotlight](@keyWords, ' '));
	DECLARE @moduleType nvarchar(50);

	DECLARE @tmp TABLE (
		ID int,
		[Number] nvarchar(50),
		LegalName nvarchar(65),
		OperatingName nvarchar(65),
		ModuleType int,
		Active bit
	);

	DECLARE @result TABLE (
		ID int,
		[Number] nvarchar(50),
		LegalName nvarchar(65),
		OperatingName nvarchar(65),
		ModuleType int,
		Active bit,
		TotalCount int
	);

	INSERT INTO @result ([ID], [Number], LegalName, OperatingName, ModuleType, Active)
	SELECT *
	FROM
	(
		SELECT 
			v.ID,
			v.Number 'Number',
			v.BusinessName 'LegalName',
			v.OperatingName	'OperatingName',
			v.VendorType 'ModuleType',
			v.IsActive 'Active'
		FROM [dbo].[Vendor] v 
		WHERE ((
			SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount and @wordCount > 0) OR
		v.Keywords like '%'+@keyWords+'%'

		UNION  

		SELECT 
			c.ID,
			c.RegistrationNumber 'Number',
			c.BusinessName 'LegalName',
			c.OperatingName	'OperatingName',
			c.CustomerType 'ModuleType',
			c.IsActive 'Active'
		FROM [dbo].[Customer] c
		WHERE ((
			SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount AND @wordCount > 0) OR
		c.Keywords like '%'+@keyWords+'%'
	) main
	ORDER BY main.Active desc, main.Number;

	UPDATE @result
	SET TotalCount = (SELECT count(*) FROM @result);
	
	SET @pageLimit = coalesce(@pageLimit, (SELECT count(*) FROM @result));
	SET @start = coalesce(@start, 0);

	SELECT *
	FROM @result r
	ORDER BY r.Active DESC, r.Number
	OFFSET @start ROW
	FETCH NEXT @pageLimit ROWS ONLY;

END
