USE [tmdb];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

-- Add two column to claimpaymentdetails table (Added to production db already for rpm extra report--
-- alter table claimpaymentdetail add ItemDescription nvarchar(4000) null;
-- alter table claimpaymentdetail add ItemCode nvarchar(100) null;

-- OTSTM2-25 --
alter table VendorUser add IsActive bit not null default 1;
alter table CustomerUser add IsActive bit not null default 1;

-- OTSTM2-56 --
alter table TSFClaim add IsCreatedByStaff bit not null default 0;

-- Added non-clustered index for performance improvement --
-- Claim --
/****** Object:  Index [FK_ClaimPeriod]    Script Date: 27/07/2016 9:15:04 AM ******/
CREATE NONCLUSTERED INDEX [FK_ClaimPeriod] ON [dbo].[Claim]
(
	[ClaimPeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_Participant]    Script Date: 27/07/2016 9:16:34 AM ******/
CREATE NONCLUSTERED INDEX [FK_Participant] ON [dbo].[Claim]
(
	[ParticipantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- Claim Detail --
/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:19:54 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimDetail]
(
	[ClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:20:45 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[ClaimDetail]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- ClaimPayment --
/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:21:53 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimPayment]
(
	[ClaimID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:28:34 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimPaymentSummary]
(
	[ClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:29:23 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimProcess]
(
	[ClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:30:09 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimSummary]
(
	[ClaimID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:35:48 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimAttachment]
(
	[ClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Claim]    Script Date: 27/07/2016 9:36:39 AM ******/
CREATE NONCLUSTERED INDEX [FK_Claim] ON [dbo].[ClaimNote]
(
	[ClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:31:55 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionAdjustment]
(
	[OriginalTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:43:05 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionItem]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_TransactionAdjustment]    Script Date: 27/07/2016 9:34:16 AM ******/
CREATE NONCLUSTERED INDEX [FK_TransactionAdjustment] ON [dbo].[TransactionItem]
(
	[TransactionAdjustmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:35:00 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionAttachment]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:38:48 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionNote]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:39:27 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionPhoto]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:40:03 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionEligibility]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [FK_Transaction]    Script Date: 27/07/2016 9:40:48 AM ******/
CREATE NONCLUSTERED INDEX [FK_Transaction] ON [dbo].[TransactionSupportingDocument]
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO












