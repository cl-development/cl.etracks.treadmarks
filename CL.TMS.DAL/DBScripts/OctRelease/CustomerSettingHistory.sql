﻿--OTSTM2-21--new table for tracking 2x/12x setting
CREATE TABLE [dbo].[CustomerSettingHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[CustomerId] [int] NOT NULL,
	[ColumnName] [nvarchar](30) NOT NULL DEFAULT 'RemitsPerYear',
	[OldValue] [nvarchar](30) NOT NULL, 
	[NewValue] [nvarchar](30) NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
	[ChangeBy] [int] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_CustomerSettingHistory_Customer] FOREIGN KEY (CustomerId) references [Customer](Id)
 )