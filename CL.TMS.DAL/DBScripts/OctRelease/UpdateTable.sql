﻿--== OTSTM2-230 begin ==--
IF NOT EXISTS(select * from Report where [ReportName] = 'TSF Online Payments Outstanding') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(23,'TSF Online Payments Outstanding','1');
end
--== OTSTM2-230 end ==--


--== OTSTM2-231 begin ==--
IF NOT EXISTS(select * from Report where [ReportName] = 'Collector Cumulative Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(24,'Collector Cumulative Report','2');
end
--== OTSTM2-231 end ==--


--== OTSTM2-223 224 225 begin ==--
IF NOT EXISTS(select * from Report where [ReportName] = 'Web Listing Steward Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(25,'Web Listing Steward Report','6');
end
IF NOT EXISTS(select * from Report where [ReportName] = 'Web Listing Processor Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(26,'Web Listing Processor Report','6');
end
IF NOT EXISTS(select * from Report where [ReportName] = 'Web Listing Recycle Product Manufacturers Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(27,'Web Listing Recycle Product Manufacturers Report','6');
end
--== OTSTM2-223 224 225 end ==--

/*--OTSTM2-33 --*/
update TypeDefinition
set Description = 'Tires removed from purchased vehicles/equipment recycling (i.e Auto Dismantlers)'
where id = 119;
go

/*--OTSTM2-33 --*/
alter table typedefinition add DisplayOrder int not null default 0;
go

update TypeDefinition set displayorder=DefinitionValue;

/*--OTSTM2-33 --*/
Update Typedefinition
set DisplayOrder = case 
                    when DefinitionValue = 8 then 1
                    when DefinitionValue = 9 then 2
                    when DefinitionValue = 4 then 3
                    when DefinitionValue = 2 then 4
                    when DefinitionValue = 5 then 5
                    when DefinitionValue = 6 then 6
                    when DefinitionValue = 7 then 7
                    when DefinitionValue = 1 then 8
                    when DefinitionValue = 3 then 9
                    when DefinitionValue = 10 then 10
                   end
where Category = 'TireOrigin'

--== OTSTM2-184 begin ==--
IF NOT EXISTS(select * from Report where [ReportName] = 'Hauler Tire Movement Report') 
begin 
    insert into Report (ID,ReportName,ReportCategoryID) values(28,'Hauler Tire Movement Report','3');
end
--== OTSTM2-184 end ==--

--create RPM Data Report for October Release
insert into Report (ID, ReportName, ReportCategoryID)
values (20, 'RPM Data Report', 5);

/*--OTSTM2-306 --*/
IF NOT EXISTS(select * from Report where [ReportName] = 'RPM Data Report') 
begin 
    insert into Report (ID, ReportName, ReportCategoryID)
    values (20, 'RPM Data Report', 5)
end


/*--OTSTM2-238--*/
IF NOT EXISTS(select * from Report where [ReportName] = 'RPM Cumulative Report') 
begin 
    insert into Report (ID, ReportName, ReportCategoryID)
    values (29, 'RPM Cumulative Report', 5)
end

--== OTSTM2-344 begin ==--
IF COL_LENGTH('ApplicationInvitation', 'InvitationDate') IS NULL
BEGIN
    ALTER TABLE ApplicationInvitation
    ADD [InvitationDate] datetime null
END
IF COL_LENGTH('Application', 'ExpireDate') IS NULL
BEGIN
    ALTER TABLE Application
    ADD [ExpireDate] datetime null
END
IF NOT EXISTS(select * from AppSetting where [Key] = 'Application.InvitationExpiryDays') 
begin 
	insert into AppSetting ([Key],Value) values('Application.InvitationExpiryDays','7');
end
--== OTSTM2-344 end ==--

/*--OTSTM2-132 --*/
insert into AppSetting ([Key],Value)
values('Email.BccApplicationPath', 'info@rethinktires.ca')

insert into AppSetting ([Key],Value)
values('Email.StewardApplicationApproveBCCEmailAddr', 'accounting@rethinktires.ca')

/*--OTSTM2-72 --*/
IF NOT EXISTS(select * from AppSetting where [Key] = 'Email.NotifyHaulersForCollectorInactive') 
begin 
	insert into AppSetting ([Key],Value)
	values('Email.NotifyHaulersForCollectorInactive', 'Templates\NotifyHaulersForCollectorInactive.html')
end

/*--OTSTM2-72 --*/
IF NOT EXISTS(select * from AppSetting where [Key] = 'Email.NotifyHaulersAndRpmsForProcessorInactive') 
begin 
	insert into AppSetting ([Key], Value)
	values ('Email.NotifyHaulersAndRpmsForProcessorInactive','Templates\NotifyHaulersAndRpmsForProcessorInactive.html')
end

/*--OTSTM2-72 --*/
IF NOT EXISTS(select * from AppSetting where [Key] = 'Email.NotifyProcessorsForRPMInactive') 
begin 
	insert into AppSetting ([Key], Value)
	values ('Email.NotifyProcessorsForRPMInactive','Templates\NotifyProcessorsForRPMInactive.html')
end

/*--OTSTM2-72 --*/
IF NOT EXISTS(select * from AppSetting where [Key] = 'Email.NotifyProcessorsForHaulerInactive') 
begin 
	insert into AppSetting ([Key], Value)
	values ('Email.NotifyProcessorsForHaulerInactive','Templates\NotifyProcessorsForHaulerInactive.html')
end

/*--OTSTM2-555 --*/
update [User] set PasswordExpirationDate = DATEADD(dd,180,PasswordExpirationDate)
