﻿/****** Object:  Table [dbo].[Notification]    Script Date: 15/09/2016 9:36:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Notification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](1024) NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[Assigner] [nvarchar](50) NOT NULL,
	[AssignerInitial] [nvarchar](3) NOT NULL,
	[AssignerName] [nvarchar](100) NOT NULL,
	[Assignee] [nvarchar](50) NOT NULL,
	[AssigneeName] [nvarchar](100) NOT NULL,
	[NotificationType] [int] NOT NULL,
	[NotificationArea] [nvarchar](30) NOT NULL,
	[ObjectId] [int] NOT NULL,
	[Status] [nvarchar](10) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


