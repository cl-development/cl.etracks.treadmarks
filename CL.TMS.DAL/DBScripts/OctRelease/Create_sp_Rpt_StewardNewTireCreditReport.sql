﻿
USE [tmdb]
GO

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_Rpt_StewardNewTireCreditReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_Rpt_StewardNewTireCreditReport]
END

/****** Object:  StoredProcedure [dbo].[sp_Rpt_StewardNewTireCreditReport]    Script Date:  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_StewardNewTireCreditReport] 
	@startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
DECLARE @TSFNegAdjSwitchDate datetime;
DECLARE @newTireDate datetime;
select @newTireDate = '4/1/2013 12:00:00 AM';  --public const string ReportEffectiveDateNewTireBegin = "04/01/2013";
select @TSFNegAdjSwitchDate = [Value] from AppSetting where [Key] = 'Settings.TSFNegAdjSwitchDate';
BEGIN
select * from
(SELECT  TSFClaim.ID[ClaimID], CAST( CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 100) + CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 120) AS varchar(10)) [NegativeAdjDate], SUM(TSFClaimDetail.NegativeAdjustment)+ SUM(TSFClaimDetail.CreditNegativeAdj)[tires], SUM(TSFClaimDetail.CreditTSFDue)[creditPayableVal], 
	class1_ve_adj=SUM(case when [Item].ShortName='C1' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class1Amount=SUM(case when [Item].ShortName='C1' then CreditTSFDue else 0 end),
	class2_ve_adj=SUM(case when [Item].ShortName='C2' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class2Amount=SUM(case when [Item].ShortName='C2' then CreditTSFDue else 0 end),
	class3_ve_adj=SUM(case when [Item].ShortName='C3' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class3Amount=SUM(case when [Item].ShortName='C3' then CreditTSFDue else 0 end),
	class4_ve_adj=SUM(case when [Item].ShortName='C4' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class4Amount=SUM(case when [Item].ShortName='C4' then CreditTSFDue else 0 end),
	class5_ve_adj=SUM(case when [Item].ShortName='C5' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class5Amount=SUM(case when [Item].ShortName='C5' then CreditTSFDue else 0 end),
	class6_ve_adj=SUM(case when [Item].ShortName='C6' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class6Amount=SUM(case when [Item].ShortName='C6' then CreditTSFDue else 0 end),
	class7_ve_adj=SUM(case when [Item].ShortName='C7' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class7Amount=SUM(case when [Item].ShortName='C7' then CreditTSFDue else 0 end),
	class8_ve_adj=SUM(case when [Item].ShortName='C8' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class8Amount=SUM(case when [Item].ShortName='C8' then CreditTSFDue else 0 end),
	class9_ve_adj=SUM(case when [Item].ShortName='C9' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class9Amount=SUM(case when [Item].ShortName='C9' then CreditTSFDue else 0 end),
	class10_ve_adj=SUM(case when [Item].ShortName='C10' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class10Amount=SUM(case when [Item].ShortName='C10' then CreditTSFDue else 0 end),
	class11_ve_adj=SUM(case when [Item].ShortName='C11' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class11Amount=SUM(case when [Item].ShortName='C11' then CreditTSFDue else 0 end),
	class12_ve_adj=SUM(case when [Item].ShortName='C12' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class12Amount=SUM(case when [Item].ShortName='C12' then CreditTSFDue else 0 end),
	class13_ve_adj=SUM(case when [Item].ShortName='C13' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class13Amount=SUM(case when [Item].ShortName='C13' then CreditTSFDue else 0 end),
	class14_ve_adj=SUM(case when [Item].ShortName='C14' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class14Amount=SUM(case when [Item].ShortName='C14' then CreditTSFDue else 0 end),
	class15_ve_adj=SUM(case when [Item].ShortName='C15' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class15Amount=SUM(case when [Item].ShortName='C15' then CreditTSFDue else 0 end),
	class16_ve_adj=SUM(case when [Item].ShortName='C16' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class16Amount=SUM(case when [Item].ShortName='C16' then CreditTSFDue else 0 end),
	class17_ve_adj=SUM(case when [Item].ShortName='C17' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class17Amount=SUM(case when [Item].ShortName='C17' then CreditTSFDue else 0 end),
	class18_ve_adj=SUM(case when [Item].ShortName='C18' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	class18Amount=SUM(case when [Item].ShortName='C18' then CreditTSFDue else 0 end),
	NegativeAdjustment = SUM([TSFClaimDetail].NegativeAdjustment),
	CreditNegativeAdjTotal = SUM([TSFClaimDetail].CreditNegativeAdj),
	creditPayable = SUM([TSFClaimDetail].CreditTSFDue)
    FROM    [dbo].[TSFClaimDetail]
    INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
    INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
    INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
    INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] > @newTireDate)
	AND ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ([Period].[StartDate] >= @TSFNegAdjSwitchDate)  AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)
	group by TSFClaim.ID, [NegativeAdjDate]
) as subQuery1 left join 
(select TSFClaim.ID[ClaimID], SUM([TSFClaimDetail].CreditTSFDue)[totalCredit] FROM    [dbo].[TSFClaimDetail]
    INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
    INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
    INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
    INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] > @newTireDate) --public const string ReportEffectiveDateNewTireBegin = "04/01/2013";
	AND ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ([Period].[StartDate] >= @TSFNegAdjSwitchDate)  AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)
	group by TSFClaim.ID)as subQuery3 on subQuery1.[ClaimID] = subQuery3.[ClaimID]
	 left join 
( select distinct TSFClaim.ID [ClaimID],[Period].[ShortName] [Period], CAST( CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 100) + CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 120) AS varchar(10)) [NegativeAdjDate],[Customer].RegistrationNumber [regNo],[Customer].BusinessName [legalName],[TSFClaim].SubmissionDate [webSubmissionDate],
  [TSFClaim].CreatedUser [submittedBy],
  stewardType = CASE 
				 WHEN [Customer].RegistrantSubTypeID  = 1 THEN 'Original Equipment Manufacture (OEM)'
				 WHEN [Customer].RegistrantSubTypeID  = 2 THEN 'Tire Manufacture/Brand Owner'
				 ELSE 'First Importer'
			  END,
  [OEM] = CASE 
			 WHEN [Customer].RegistrantSubTypeID  = 1 THEN 'Yes'
			 ELSE 'No'
		  END,
  [tsfStatus] = [TSFClaim].[RecordState],
  [receiptDate] = [TSFClaim].[ReceiptDate],
  [depositDate] = [TSFClaim].[DepositDate],
  [ChequeEFT] = [TSFClaim].ChequeReferenceNumber,
  [chequeAmountVal] = [TSFClaim].PaymentAmount,
  [taxesDue] = 'N/A',
  [interest] = 'N/A',
  [penaltiesVal] = CASE 
					 WHEN [TSFClaim].[PenaltiesManually] != null THEN [TSFClaim].[PenaltiesManually]
					 ELSE [TSFClaim].Penalties
				  END,
  [registrantStatus] = CASE 
						 WHEN [Customer].[IsActive] = 1 THEN 'Active'
						 ELSE 'Inactive'
					  END,
  [statusChangeDate] = [Customer].[ActiveStateChangeDate]
	FROM  [dbo].[TSFClaimDetail]
		INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
		INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
		INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
		INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] > @newTireDate)
	AND ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ([Period].[StartDate] >= @TSFNegAdjSwitchDate) AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)) as subQuery2 
	on subQuery1.[ClaimID] = subQuery2.[ClaimID] and subQuery1.[NegativeAdjDate] = subQuery2 .[NegativeAdjDate]
END
GO
