﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_TSFYearToDate_Unit]    Script Date: 5/01/18 4:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--  Name:        [sp_Rpt_TSFYearToDate_Unit]
--  Author:		 Hefen Zhou
--  Create date: 05-01-2018
--  Description: Store procedure for loading Steward TSF by Tire Type Units (Yearly)
--               There will be 3-year comparison always  sequence:  
--               Current Year.. Current year -1 and Current year - 2.  
-- =============================================
Create PROCEDURE [dbo].[sp_Rpt_TSFYearToDate_Unit]
	-- Add the parameters for the stored procedure here
	@startDate DateTime=null, 
	@endDate DateTime=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @firstYearEnd datetime,
			@secondYear datetime,@secondYearEnd datetime,
			@thirdYear datetime,@thirdYearEnd datetime
	
			set @firstYearEnd=DATEADD(year,-2,@endDate);
			set @secondYear=DATEADD(year,1,@startDate);
			set @secondYearEnd=DATEADD(year,-1,@endDate);
			set @thirdYear=DATEADD(year,2,@startDate);
	select piv.[Year],(coalesce(PLT,0)+coalesce([C1],0)) as PLT, coalesce([MT],0)+coalesce([C2],0) as MT, 
	 (coalesce([AGLS],0)+ coalesce([IND],0)+ coalesce([SOTR],0)+ coalesce([MOTR],0)+ coalesce([LOTR],0)+ coalesce([GOTR],0)+
 	  coalesce([C3],0)+ coalesce([C4],0)+ coalesce([C5],0)+coalesce([C6],0) + coalesce([C7],0)+ coalesce([C8],0) +
	  coalesce([C9],0)+ coalesce([C10],0) + coalesce([C11],0) + coalesce([C12],0) + coalesce([C13],0) + 
	  coalesce([C14],0) + coalesce([C15],0) + coalesce([C16],0) + coalesce([C17],0) + coalesce([C18],0)) as OTR
	from
	(
	select 
		Year(p.StartDate) as [Year], 
		i.ShortName, 	
		sum(convert(decimal(16,0),(tc.TireSupplied-tc.NegativeAdjustment))) as [Amount] --CountSupplied=TireSupplied-NegativeAdjustment
	from TSFClaimDetail tc
	join Item i on tc.ItemID=i.ID
	join TSFClaim t on tc.TSFClaimID=t.ID
	join Period p on t.PeriodID=p.ID
	where (p.StartDate>=@startDate and p.StartDate<=@firstYearEnd) or
		  (p.StartDate>=@secondYear and p.StartDate<=@secondYearEnd) or
		  (p.StartDate>=@thirdYear and p.StartDate<=@endDate)
	group by Year(p.StartDate),i.ShortName
	) src
	PIVOT
	(
		sum(Amount)
		for ShortName in ([PLT],[MT],[AGLS],[IND],[SOTR],[MOTR],[LOTR],[GOTR], [C1], [C2], [C3], [C4], [C5],[C6], [C7], [C8], [C9], [C10], [C11],[C12], [C13], [C14], [C15], [C16], [C17],[C18])
	) piv
END
