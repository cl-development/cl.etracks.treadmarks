﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[[sp_Rpt_TSFStatusOverview_TimeClass]]    Script Date: 5/07/18 2:01:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--  Name:        sp_Rpt_TSFStatusOverview_TimeClass
--  Author:		 Hefen Zhou
--  Create date: 05-08-2018
--  Description: Store procedure for loading Steward TSF Status Overview Time Class
-- =============================================
Create PROCEDURE [dbo].[sp_Rpt_TSFStatusOverview_TimeClass]
	@startDate DateTime=null, 
	@endDate DateTime=null
AS
BEGIN
	select coalesce([TimeClass],'Other') as TimeClass,sum(amount) as Amount 
	from 
		 (select 
			case 
				when t.ReceiptDate is null 
				then 'Pending'
				else 
					case 
						when DATEDIFF(DAY,t.ReceiptDate,p.SubmitEnd)<=4 
						then 'OnTime'
						else 'Later'
					end
			end TimeClass,
			tc.TSFDue-tc.CreditTSFDue as amount  
		 from TSFClaimDetail tc
		 join TSFClaim t on tc.TSFClaimID=t.ID
		 join Period p on t.PeriodID=p.ID
		 where (@startDate is null or p.StartDate>=@startDate) and (@endDate is null or p.EndDate<=@endDate)
		 ) as tempData
	 group by [TimeClass]
END
