﻿USE [tmdb]
GO

/****** Object:  Table [dbo].[SpecialItemRateList]    Script Date: 5/7/2018 12:25:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecialItemRateList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventNumber] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[OnRoadRate] [decimal](18, 3) NOT NULL,
	[OffRoadRate] [decimal](18, 3) NOT NULL,
	[TransactionID] [int] null,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,	
	[EffectiveStartDate] [datetime] NOT NULL,
	[EffectiveEndDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_SpecialItemRateList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SpecialItemRateList]  WITH CHECK ADD  CONSTRAINT [FK_SpecialItemRateList_CreatedByUser] FOREIGN KEY([CreatedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[SpecialItemRateList] CHECK CONSTRAINT [FK_SpecialItemRateList_CreatedByUser]
GO

ALTER TABLE [dbo].[SpecialItemRateList]  WITH CHECK ADD  CONSTRAINT [FK_SpecialItemRateList_ModifiedUser] FOREIGN KEY([ModifiedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[SpecialItemRateList] CHECK CONSTRAINT [FK_SpecialItemRateList_ModifiedUser]
GO

ALTER TABLE [dbo].[SpecialItemRateList]  WITH CHECK ADD  CONSTRAINT [FK_SpecialItemRateList_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO

ALTER TABLE [dbo].[SpecialItemRateList] CHECK CONSTRAINT [FK_SpecialItemRateList_Transaction]
GO

