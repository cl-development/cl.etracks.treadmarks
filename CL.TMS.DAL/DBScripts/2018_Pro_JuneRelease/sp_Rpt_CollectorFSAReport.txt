USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_Rpt_CollectorFSAReport]    Script Date: 6/4/2018 1:03:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[sp_Rpt_CollectorFSAReport] 
    @startDate DateTime, @endDate DateTime, @rateGroupIds nvarchar(1000)
AS
BEGIN

declare  @startDateVar DateTime, @endDateVar DateTime, @rateGroupIdsVar nvarchar(1000)
set @startDateVar = @startDate
set @endDateVar = @endDate
set @rateGroupIdsVar = @rateGroupIds

--exec sp_executesql

declare @query nvarchar(max)
set @query = 'select 
RateGroupName PickupRateGroup,
GroupName CollectorGroup,
sum(plt) PLT,
sum(mt) MT,
sum(agls) AGLS,
sum(ind) IND,
sum(sotr) SOTR,
sum(motr) MOTR,
sum(lotr) LOTR,
sum(gotr) GOTR ,
@startDateVar EffectiveStartDate,
@endDateVar EffectiveEndDate
 from ((select 
rg.RateGroupName,
g.GroupName,
sum(tires.plt) plt,
sum(tires.mt) mt,
sum(tires.agls) agls,
sum(tires.ind) ind,
sum(tires.sotr) sotr,
sum(tires.motr) motr,
sum(tires.lotr) lotr,
sum(tires.gotr) gotr			
from [Transaction] t 
inner join
(
	select 
	ti.TransactionId,
	SUM(case when itemid = 65 then quantity else 0 end)  "PLT",
	SUM(case when itemid = 66 then quantity else 0 end)  "MT",
	SUM(case when itemid = 67 then quantity else 0 end)  "AGLS",
	SUM(case when itemid = 68 then quantity else 0 end)  "IND",
	SUM(case when itemid = 69 then quantity else 0 end)  "SOTR",
	SUM(case when itemid = 70 then quantity else 0 end)  "MOTR",
	SUM(case when itemid = 71 then quantity else 0 end)  "LOTR",
	SUM(case when itemid = 72 then quantity else 0 end)  "GOTR"
	from TransactionItem ti	
	where ti.TransactionAdjustmentId is null
	and ti.TransactionId not in (select tii.TransactionId from TransactionItem tii where tii.TransactionAdjustmentId is not null)
	group by ti.TransactionId
) as tires on tires.TransactionId=t.ID
inner join ClaimDetail cd
on cd.TransactionId=t.ID
inner join Claim c
on cd.ClaimId=c.id
inner join Period p
on p.id=c.ClaimPeriodID
inner join RateGroup rg
on rg.Id IN (' + @rateGroupIdsVar + ' )
inner join VendorRateGroup vrg on
rg.id = vrg.RateGroupId and vrg.VendorId=t.OutgoingId
inner join [Group] g on
g.id=vrg.GroupId
inner join vendor vi on
vi.id=t.OutgoingId

where t.TransactionTypeCode IN (''TCR'',''DOT'')
and t.ProcessingStatus IN (''Approved'',''Unreviewed'')
and cd.Direction=1 and
(@startDateVar is null or @startDateVar <= p.StartDate or @startDateVar <= p.EndDate) 
AND (@endDateVar is null or p.StartDate  <= @endDateVar)  


group by 
rg.RateGroupName,
g.GroupName)

union all
(select 
rg.RateGroupName,
g.GroupName,
sum(tires.plt) plt,
sum(tires.mt) mt,
sum(tires.agls) agls,
sum(tires.ind) ind,
sum(tires.sotr) sotr,
sum(tires.motr) motr,
sum(tires.lotr) lotr,
sum(tires.gotr) gotr			
from [Transaction] t 
inner join
(
	select 
	ti.TransactionId,
	SUM(case when itemid = 65 then quantity else 0 end)  "PLT",
	SUM(case when itemid = 66 then quantity else 0 end)  "MT",
	SUM(case when itemid = 67 then quantity else 0 end)  "AGLS",
	SUM(case when itemid = 68 then quantity else 0 end)  "IND",
	SUM(case when itemid = 69 then quantity else 0 end)  "SOTR",
	SUM(case when itemid = 70 then quantity else 0 end)  "MOTR",
	SUM(case when itemid = 71 then quantity else 0 end)  "LOTR",
	SUM(case when itemid = 72 then quantity else 0 end)  "GOTR"
	from TransactionItem ti	
	inner join TransactionAdjustment ta
	on ti.TransactionAdjustmentId=ta.ID
	where ti.TransactionAdjustmentId is not null and ta.Status=''Accepted''
	group by ti.TransactionId
) as tires on tires.TransactionId=t.ID
inner join ClaimDetail cd
on cd.TransactionId=t.ID
inner join Claim c
on cd.ClaimId=c.id
inner join Period p
on p.id=c.ClaimPeriodID
inner join RateGroup rg
on rg.Id IN (' + @rateGroupIdsVar + ')
inner join VendorRateGroup vrg on
rg.id = vrg.RateGroupId and vrg.VendorId=t.OutgoingId
inner join [Group] g on
g.id=vrg.GroupId
inner join vendor vi on
vi.id=t.OutgoingId

where t.TransactionTypeCode IN (''TCR'',''DOT'')
and t.ProcessingStatus IN (''Approved'',''Unreviewed'')
and cd.Direction=1 and
(@startDateVar is null or @startDateVar <= p.StartDate or @startDateVar <= p.EndDate) 
AND (@endDateVar is null or p.StartDate  <= @endDateVar)  

--and c.ParticipantID=18989
--and c.ParticipantID=28670

group by rg.RateGroupName,
g.GroupName
)
)  t
group by RateGroupName,
GroupName'

exec sp_executesql @query , N'@startDateVar datetime, @endDateVar datetime', @startDateVar, @endDateVar;

END




GO


