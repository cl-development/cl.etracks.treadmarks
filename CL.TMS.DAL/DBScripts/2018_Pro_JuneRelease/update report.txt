ALTER TABLE Report ALTER COLUMN ReportName varchar(100);

update report 
set ReportName='Collection/Generation of tires based on Groups Report',
Description='This report provides users with the number of tires collected, by type, from Collector Groups within a Pickup Rate Group and within a specified date range. Clicking on a Collector Group in the results panel will display all the tire collection transactions with the Collectors in that Collector Group within the specified date range.' 
where id=31