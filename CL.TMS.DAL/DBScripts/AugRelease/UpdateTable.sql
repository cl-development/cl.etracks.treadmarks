﻿USE [tmdb]
GO
--== OTSTM2-358 begin ==--
IF COL_LENGTH('BankInformation', 'SubmittedOnDate') IS NULL
BEGIN
    ALTER TABLE BankInformation
    ADD [SubmittedOnDate] datetime null
END
GO
--== OTSTM2-358 end ==--

--== OTSTM2-361 begin ==--
BEGIN
   IF NOT EXISTS (SELECT * FROM AppSetting WHERE [Key] = 'Email.RejectBankinfoEmailTemplLocation')
   BEGIN
       INSERT INTO [AppSetting] ([Key], [Value])
       VALUES ('Email.RejectBankinfoEmailTemplLocation', 'Templates\BankInformationRejecetEmailTempl.html')
   END
END
GO
--== OTSTM2-361 end ==--

--== OTSTM2-544 begin ==--
IF COL_LENGTH('[Transaction]', 'TransactionAdjustmentID') IS NULL
BEGIN
    ALTER TABLE [Transaction]
    ADD [TransactionAdjustmentID] int null  CONSTRAINT FK_Transaction_TransactionAdjustment FOREIGN KEY (TransactionAdjustmentID) REFERENCES TransactionAdjustment(ID)
END
GO
update [Transaction] set [Transaction].[TransactionAdjustmentID] = adj.ID 
from [TransactionAdjustment] adj join [Transaction] t on adj.OriginalTransactionId=t.ID where adj.Status = 'Accepted'
GO
--== OTSTM2-544 end ==--