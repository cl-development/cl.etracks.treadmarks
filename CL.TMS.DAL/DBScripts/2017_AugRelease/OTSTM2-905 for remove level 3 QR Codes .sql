﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-905 for remove level 3 QR Codes
Purpose     :   OTSTM2-905 Role and Permission Matrix issues
			:   1. please remove level #3 QRCODE under QRCODE menu and page , keep it similar to All Transactions and All users page
			:   2. Remove custom options for this resource , follow All Transactions page
Created By  :   Joe Zhou Aug 3th, 2017
*****************************************************************/

use tmdb
go

delete from [tmdb].[dbo].[RolePermission]
where AppResourceId in (
	select id from  [tmdb].[dbo].[AppResource]
		where name like '%QR Codes%' and ResourceLevel=2
  )

delete from [tmdb].[dbo].[AppResource]
where name like '%QR Codes%' and ResourceLevel=2

--update [tmdb].[dbo].[AppResource]
--set name='QR Codes', ResourceScreen='QR Codes' where  name like '%QR Codes%' and ResourceLevel=1

go