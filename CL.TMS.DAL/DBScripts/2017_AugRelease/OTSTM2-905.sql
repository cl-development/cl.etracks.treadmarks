﻿use tmdb

delete from RolePermission where Id in (select rp.Id from AppResource res join RolePermission rp on rp.AppResourceId=res.Id where res.Name = 'iPad Fleet')

delete from AppResource where Name = 'iPad Fleet'

update AppResource set Name='QR Codes' , ResourceMenu='QR Codes' , ResourceScreen='QR Codes' 
where Name='iPads + QR Codes' and ResourceMenu='iPads + QR Codes' and ResourceScreen='iPads + QR Codes'--ID 137

update AppResource set Name='All QR Codes' , ResourceMenu='QR Codes' , ResourceScreen='All QR Codes'
where Name='All iPads + QR Codes' and ResourceMenu='iPads + QR Codes' and ResourceScreen='All iPads + QR Codes'--ID 138

update AppResource set Name='QR Codes' , ResourceMenu='QR Codes' , ResourceScreen='All QR Codes'
where Name='QR Codes' and ResourceMenu='iPads + QR Codes' and ResourceScreen='All iPads + QR Codes'--ID 140

go