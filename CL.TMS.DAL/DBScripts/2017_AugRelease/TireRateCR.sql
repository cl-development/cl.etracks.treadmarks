﻿USE [tmdb]
GO

--create new table

/****** Object:  Table [dbo].[RateTransaction]    Script Date: 7/18/2017 12:10:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

if not exists (select * from sysobjects where name='RateTransaction' And xtype='U')
begin
CREATE TABLE [dbo].[RateTransaction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NULL,

	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[Category] [nvarchar](50) null,

	[EffectiveStartDate] [datetime] NOT NULL,
	[EffectiveEndDate] [datetime] NOT NULL,

	[RowVersion] [timestamp] NOT NULL,
CONSTRAINT [PK_RateTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

	ALTER TABLE [dbo].[RateTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RateTransaction_CreatedByUser] FOREIGN KEY([CreatedByID])
	REFERENCES [dbo].[User] ([ID])
	ALTER TABLE [dbo].[RateTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RateTransaction_ModifiedUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])
end
GO

/****** Object:  Table [dbo].[RateTransactionNote]    Script Date: 7/13/2017 4:35:09 PM ******/
if not exists (select * from sysobjects where name='RateTransactionNote' And xtype='U')
begin
	CREATE TABLE [dbo].[RateTransactionNote](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[RateTransactionID] [int] NOT NULL,
		[Note] [varchar](max) NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserID] [bigint] NOT NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_RateNote] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[RateTransactionNote]  WITH CHECK ADD  CONSTRAINT [FK_RateTransactionNote_RateTransaction] FOREIGN KEY([RateTransactionID])
	REFERENCES [dbo].[RateTransaction] ([ID])

	ALTER TABLE [dbo].[RateTransactionNote] CHECK CONSTRAINT [FK_RateTransactionNote_RateTransaction]

	ALTER TABLE [dbo].[RateTransactionNote]  WITH CHECK ADD  CONSTRAINT [FK_RateTransactionNote_User] FOREIGN KEY([UserID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[RateTransactionNote] CHECK CONSTRAINT [FK_RateTransactionNote_User]
end
go

IF COL_LENGTH('Rate', 'CreateDate') IS NOT NULL ALTER TABLE Rate DROP COLUMN [CreateDate]
IF COL_LENGTH('Rate', 'CreateByID') IS NOT NULL ALTER TABLE Rate DROP COLUMN [CreateByID]

IF COL_LENGTH('Rate', 'ModifiedDate') IS NOT NULL ALTER TABLE Rate DROP COLUMN [ModifiedDate]
IF COL_LENGTH('Rate', 'ModifiedByID') IS NOT NULL ALTER TABLE Rate DROP COLUMN [ModifiedByID]
IF COL_LENGTH('Rate', 'ModifiedBy') IS NOT NULL ALTER TABLE Rate DROP COLUMN [ModifiedBy]
GO

IF COL_LENGTH('Rate', 'RateTransactionID') IS NULL ALTER TABLE Rate ADD [RateTransactionID] int NULL
GO

--ALTER TABLE
IF (OBJECT_ID('Rate_RateTransaction', 'F') IS NULL)
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [Rate_RateTransaction] FOREIGN KEY([RateTransactionID])
REFERENCES [dbo].[RateTransaction] ([ID])
GO

--insert data
--if not exists ( select top 1 * from RateTransaction)
--insert into RateTransaction (CreatedDate, CreateByID, ModifiedDate, ModifiedByID, Category, EffectiveStartDate, EffectiveEndDate) values
--(GETDATE(),null,null,null,'Transportation Incentive Rates',
--(select top 1 EffectiveStartDate from Rate group by EffectiveStartDate ,EffectiveEndDate order by EffectiveStartDate desc, EffectiveEndDate desc),
--(select top 1 EffectiveEndDate from Rate group by EffectiveStartDate ,EffectiveEndDate order by EffectiveStartDate desc, EffectiveEndDate desc))
--go


--update Rate set EffectiveEndDate='9999-12-31', RateTransactionID=(select top 1 ID from RateTransaction)
--where ID in (select ID from Rate where EffectiveStartDate = (select top 1 EffectiveStartDate from Rate group by EffectiveStartDate ,EffectiveEndDate order by EffectiveStartDate desc, EffectiveEndDate desc))
--go