﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;

namespace CL.TMS.DAL
{
    public class SystemBoundedContext : TMBaseContext<SystemBoundedContext>
    {
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<TypeDefinition> TypeDefinitions { get; set; }

        public DbSet<ParticipantType> ParticipantTypes { get; set; }

        public DbSet<VendorUser> VendorUsers { get; set; }

        public DbSet<CustomerUser> CustomerUsers { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Rate> Rates { get; set; }

        public DbSet<VendorRate> VendorRates { get; set; }

        public DbSet<ItemWeight> ItemWeights { get; set; }

        public DbSet<TransactionType> TransactionTypes { get; set; }

        public DbSet<PostalCode> PostalCodes { get; set; }

        public DbSet<Zone> Zones { get; set; }

        public DbSet<Region> Region { get; set; } //OTSTM2-215
        public DbSet<RegionDetail> RegionDetail { get; set; } //OTSTM2-215
        public DbSet<Country> Country { get; set; } //OTSTM2-1016

        public DbSet<SystemActivity> SystemActivities { get; set; }

        public DbSet<GpiAccount> GPIAccounts { get; set; }

        public DbSet<FIAccount> FIAccounts { get; set; } //OTSTM2-1124 for Successor only

        //OTSTM2-486
        public DbSet<BusinessActivity> BusinessActivities { get; set; }

        public DbSet<AppResource> AppResources { get; set; }
        public DbSet<AppPermission> AppPermissions { get; set; }

        public DbSet<RateGroupRate> RateGroupRates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);
            modelBuilder.Entity<RateGroupRate>().Property(c => c.Rate).HasPrecision(18, 3);

            modelBuilder.Entity<ItemWeight>().Property(c => c.StandardWeight).HasPrecision(18, 4);

            //For performance to ignore the following schema
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Claim>();

            base.OnModelCreating(modelBuilder);
        }
    }
}