﻿using CL.TMS.Framework.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Metric;

namespace CL.TMS.DAL
{
    public class ReportingBoundedContext : TMBaseContext<ReportingBoundedContext>
    {
        public ReportingBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<Setting> AppSettings { get; set; }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportCategory> ReportCategories { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionAdjustment> TransactionAdjustments { get; set; }

        public DbSet<TSFClaim> TSFClaims { get; set; }
        public DbSet<TSFClaimDetail> TSFClaimDetails { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Rate> Rates { get; set; }
        #region OTSTM2-1226
        public DbSet<Group> Groups { get; set; }
        public DbSet<RateGroup> RateGroups { get; set; }
        public DbSet<VendorRateGroup> VendorRateGroups { get; set; }
        public DbSet<RateTransaction> RateTransactions { get; set; }
        public DbSet<VendorRate> VendorRates { get; set; }
        #endregion
        public DbSet<User> Users { get; set; }
        public DbSet<TSFRemittanceNote> TSFRemittanceNotes { get; set; }
        public DbSet<ClaimPaymentDetail> ClaimPaymentDetail { get; set; }
        public DbSet<Claim> Claim { get; set; }
        public DbSet<ClaimDetail> ClaimDetail { get; set; }

        //OTSTM2-238
        public DbSet<ClaimSummary> ClaimSummary { get; set; }
        public DbSet<ClaimPaymentSummary> ClaimPaymentSummary { get; set; }
        public DbSet<TransactionItem> TransactionItems { get; set; }
        public DbSet<ClaimInventoryDetail> ClaimInventoryDetail { get; set; }
        public DbSet<ClaimInventoryAdjustment> ClaimInventoryAdjustments { get; set; }

        //OTSTM2-764
        public DbSet<Invitation> Invitations { get; set; }

        //OTSTM2-215
        public DbSet<RegionDetail> RegionDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //For performance to ignore the following schema - Update the list when start using this context 
            //modelBuilder.Ignore<Contact>();     

            modelBuilder.Entity<Customer>().HasMany<Address>(r => r.CustomerAddresses).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerAddress"); m.MapLeftKey("CustomerId"); m.MapRightKey("AddressId"); });
            modelBuilder.Entity<Customer>().HasMany<Item>(r => r.Items).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerItem"); m.MapLeftKey("CustomerId"); m.MapRightKey("ItemId"); });

            //TSFClaim
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalTSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesGst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePostHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePreHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Penalties).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PenaltiesManually).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Interest).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalRemittancePayable).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PaymentAmount).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.BalanceDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Credit).HasPrecision(18, 3);

            //TSFClaimDetail
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFDue).HasPrecision(18, 3);

            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);

            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.Rate).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.ActualWeight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.AverageWeight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountCredit).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.AdjustmentTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.TotalTax).HasPrecision(18, 3);


            modelBuilder.Entity<Transaction>().Property(c => c.OnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.OffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.EstimatedOnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.EstimatedOffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.RetailPrice).HasPrecision(18, 3);

            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.OnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.OffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.EstimatedOnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.EstimatedOffRoadWeight).HasPrecision(18, 4);

            modelBuilder.Entity<TransactionItem>().Property(c => c.AverageWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ActualWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.Rate).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.RecyclablePercentage).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledIn).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledOut).HasPrecision(18, 4);

            //For performance to ignore the following schema
            //modelBuilder.Ignore<Permission>();
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<VendorStorageSite>();
            modelBuilder.Ignore<InvitationRole>();

            //modelBuilder.Ignore<ClaimPaymentSummary>();
            modelBuilder.Entity<ClaimPaymentSummary>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimPaymentSummary)
                .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimProcess>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimProcess)
                .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimSummary>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimSummary)
                .Map(m => m.MapKey("ClaimID"));

            modelBuilder.Entity<Vendor>().HasMany<Address>(r => r.VendorAddresses).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorAddress"); m.MapLeftKey("VendorID"); m.MapRightKey("AddressID"); });
            modelBuilder.Entity<Vendor>().HasMany<Item>(r => r.Items).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorItem"); m.MapLeftKey("VendorID"); m.MapRightKey("ItemID"); });
            modelBuilder.Entity<Customer>().HasMany<Address>(r => r.CustomerAddresses).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerAddress"); m.MapLeftKey("CustomerId"); m.MapRightKey("AddressId"); });
            modelBuilder.Entity<Customer>().HasMany<Item>(r => r.Items).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerItem"); m.MapLeftKey("CustomerId"); m.MapRightKey("ItemId"); });
        }

        public virtual ObjectResult<SpRptRegistrantWeeklyReport> Sp_Rpt_RegistrantWeeklyReport()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptRegistrantWeeklyReport>("sp_Rpt_RegistrantWeeklyReport");
        }
        public virtual ObjectResult<SpRptHaulerVolumeReport> sp_Rpt_HaulerVolumeReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);

            var context = ((IObjectContextAdapter)this).ObjectContext;
            context.CommandTimeout = 600;
            return context.ExecuteStoreQuery<SpRptHaulerVolumeReport>("sp_Rpt_HaulerVolumeReport @startDate, @endDate", startDateParameter, endDateParameter);
        }

        public virtual ObjectResult<SpRptTsfExtractInBatchOnlyReportGp> Sp_Rpt_TsfExtractInBatchOnlyReportGp()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptTsfExtractInBatchOnlyReportGp>("sp_Rpt_TsfExtractInBatchOnlyReportGp");
        }
        public virtual ObjectResult<SpRptProcessorTIPIReport> Sp_Rpt_ProcessorTIPIReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptProcessorTIPIReport>("sp_Rpt_ProcessorTIPIReport @startDate, @endDate", startDateParameter, endDateParameter);
        }

        public virtual ObjectResult<SpRptHaulerCollectorComparisonReport> sp_Rpt_HaulerCollectorComparisonReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptHaulerCollectorComparisonReport>("sp_Rpt_HaulerCollectorComparisonReport @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }
        public virtual ObjectResult<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> Sp_Rpt_DetailHaulerVolumeReportBasedOnScaleWeight(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptDetailHaulerVolumeReportBasedOnScaleWeight>("sp_Rpt_DetailHaulerVolumeReportBasedOnScaleWeight @startDate, @endDate", startDateParameter, endDateParameter);
        }

        public virtual ObjectResult<StewardRptRevenueSupplyNewTireCreditVMsp> Sp_Rpt_StewardNewTireCredit(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<StewardRptRevenueSupplyNewTireCreditVMsp>("sp_Rpt_StewardNewTireCreditReport @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }
        public virtual ObjectResult<SpRptProcessorVolumeReport> Sp_Rpt_ProcessorVolumeReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpRptProcessorVolumeReport>("sp_Rpt_ProcessorVolumeReport @startDate, @endDate", startDateParameter, endDateParameter);
        }
        public virtual ObjectResult<HaulerTireMovementReport> Sp_Rpt_HaulerTireMovement(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<HaulerTireMovementReport>("sp_Rpt_HaulerTireMovement @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }

        public virtual ObjectResult<HaulerTireMovementReportVM> Sp_Rpt_HaulerTireMovementDataValidation(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<HaulerTireMovementReportVM>("sp_Rpt_HaulerTireMovement @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }

        public virtual ObjectResult<CollectorTireOriginReport> Sp_Rpt_CollectorTireOriginReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CollectorTireOriginReport>("sp_Rpt_CollectorTireOrigin @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }

        public virtual ObjectResult<HaulerTireCollectionReport> Sp_Rpt_HaulerTireCollectionReport(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<HaulerTireCollectionReport>("[sp_Rpt_HaulerTireCollectionReport] @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }

        public virtual ObjectResult<StewardRptRevenueSupplyOldTireCreditVMsp> Sp_Rpt_StewardOldTireCredit(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, string registrationNumber)
        {
            var startDateParameter = startDate.HasValue ?
                new SqlParameter("@startDate", startDate) :
                new SqlParameter("@startDate", new DateTime(1900, 1, 1));
            var endDateParameter = endDate.HasValue ?
                new SqlParameter("@endDate", endDate) :
                new SqlParameter("@endDate", DateTime.MaxValue);
            var registrationNumberParameter = new SqlParameter("@registrationNumber", string.IsNullOrEmpty(registrationNumber) ? DBNull.Value : (object)registrationNumber);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<StewardRptRevenueSupplyOldTireCreditVMsp>("sp_Rpt_StewardOldTireCreditReport @startDate, @endDate, @registrationNumber", startDateParameter, endDateParameter, registrationNumberParameter);
        }

        #region Metric
        public virtual ObjectResult<YearToDateVM> Sp_Rpt_YearToDateDollar()
        //Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate)
        {
            //There will be 3-year comparison always  sequence: 
            //Current Year.. Current year -1 and Current year - 2.            
            var now = DateTime.UtcNow;
            int skipYear = now.Month == 1 ? 3 : 2;
            var startDateParameter = new SqlParameter("@startDate", new DateTime((now.Year - skipYear), 1, 1).Date);
            var endDateParameter = new SqlParameter("@endDate", (new DateTime(now.Year, now.Month, 1)).AddDays(-1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<YearToDateVM>("[sp_Rpt_TSFYearToDate_Dollar] @startDate, @endDate", startDateParameter, endDateParameter);
        }
        public virtual ObjectResult<YearToDateVM> Sp_Rpt_YearToDateUnit()
        {
            //There will be 3-year comparison always  sequence: 
            //Current Year.. Current year -1 and Current year - 2.            
            var now = DateTime.UtcNow;
            int skipYear = now.Month == 1 ? 3 : 2;
            var startDateParameter = new SqlParameter("@startDate", new DateTime((now.Year - skipYear), 1, 1).Date);
            var endDateParameter = new SqlParameter("@endDate", (new DateTime(now.Year, now.Month, 1)).AddDays(-1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<YearToDateVM>("[sp_Rpt_TSFYearToDate_Unit] @startDate, @endDate", startDateParameter, endDateParameter);
        }

        public virtual ObjectResult<TSFStatusVM> Sp_Rpt_TSFStatusOverview()
        {
            var now = DateTime.UtcNow;
            var startDateParameter = new SqlParameter("@startDate", new DateTime((now.Year - 1), 1, 1).Date);
            var endDateParameter = new SqlParameter("@endDate", (new DateTime(now.Year, now.Month + 1, 1)).AddDays(-1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TSFStatusVM>("[sp_Rpt_TSFStatusOverview] @startDate, @endDate", startDateParameter, endDateParameter);
        }
        public virtual ObjectResult<TSFStatusTimeClassVM> Sp_Rpt_TSFStatusOverview_TimeClass()
        {
            var now = DateTime.UtcNow;
            var startDateParameter = new SqlParameter("@startDate", new DateTime((now.Year - 1), 1, 1).Date);
            var endDateParameter = new SqlParameter("@endDate", (new DateTime(now.Year, now.Month + 1, 1)).AddDays(-1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TSFStatusTimeClassVM>("[sp_Rpt_TSFStatusOverview_TimeClass] @startDate, @endDate", startDateParameter, endDateParameter);
        }
        public virtual ObjectResult<PaymentMethodVM> Sp_Rpt_PaymentMethod()
        {
            var now = DateTime.UtcNow;
            var startDateParameter = new SqlParameter("@startDate", new DateTime((now.Year - 1), 1, 1).Date);
            var endDateParameter = new SqlParameter("@endDate", (new DateTime(now.Year, now.Month + 1, 1)).AddDays(-1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<PaymentMethodVM>("[sp_Rpt_StewardPaymentMethod] @startDate, @endDate", startDateParameter, endDateParameter);
        }
        public virtual ObjectResult<TopTenStewardsVM> Sp_Rpt_TopTenStewards(DateTime? startDate)
        {
            var now = startDate.HasValue ? startDate.Value : DateTime.UtcNow;
            var startDateParameter = new SqlParameter("@startDate", new DateTime(now.Year, now.Month, 1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TopTenStewardsVM>("[sp_Rpt_TopTenStewards] @startDate", startDateParameter);
        }
        public virtual ObjectResult<TopTenStewardChartsVM> Sp_Rpt_TopTenStewardCharts(DateTime? startDate)
        {
            var now = startDate.HasValue ? startDate.Value : DateTime.UtcNow;
            var startDateParameter = new SqlParameter("@startDate", new DateTime(now.Year, now.Month, 1).Date);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TopTenStewardChartsVM>("[sp_Rpt_TopTenStewards_Charts] @startDate", startDateParameter);
        }
        public virtual ObjectResult<CollectionGenerationOfTiresBasedOnRateGroupVM> Sp_Rpt_CollectorFSAReport(DateTime fromDate, DateTime toDate, string rateGroupIds)
        {
            var now = DateTime.Now;
            var startDatePar = new SqlParameter("@startDate", fromDate);
            var endDatePar = new SqlParameter("@endDate", toDate);
            var rateGroupIdsPar = new SqlParameter("@rateGroupIds", rateGroupIds);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CollectionGenerationOfTiresBasedOnRateGroupVM>("[sp_Rpt_CollectorFSAReport] @startDate, @endDate, @rateGroupIds", startDatePar, endDatePar, rateGroupIdsPar);
        }

        #endregion


    }
}
