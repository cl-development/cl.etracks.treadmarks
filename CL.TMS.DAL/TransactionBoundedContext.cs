﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class TransactionBoundedContext : TMBaseContext<TransactionBoundedContext>
    {
        public TransactionBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<ScaleTicket> ScaleTickets { get; set; }
        public DbSet<TransactionNote> Notes { get; set; }
        public DbSet<TransactionItem> TransactionItems { get; set; }
        public DbSet<GpsLog> GpsLogs { get; set; }
        public DbSet<TransactionPhoto> TransactionPhotos { get; set; }
        public DbSet<TransactionAttachment> TransactionAttachments { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<TransactionAdjustment> TransactionAdjustments { get; set; }
        public DbSet<TransactionComment> Comments { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimDetail> ClaimDetails { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<CompanyInfo> CompanyInfos { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ClaimDetailAdjustment> ClaimDetailAdjustments { get; set; }

        public DbSet<SpecialItemRateList> SpecialItemRateList { get; set; } //OTSTM2-1214 Event Premium Implementation
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Zone> Zones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendor>().HasMany<Address>(r => r.VendorAddresses).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorAddress"); m.MapLeftKey("VendorId"); m.MapRightKey("AddressId"); });
            modelBuilder.Entity<Customer>().HasMany<Address>(r => r.CustomerAddresses).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerAddresses"); m.MapLeftKey("CustomerId"); m.MapRightKey("AddressId"); });            
            modelBuilder.Entity<Customer>().HasMany<Item>(r => r.Items).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerItem"); m.MapLeftKey("CustomerId"); m.MapRightKey("ItemId"); });
            modelBuilder.Entity<Vendor>().HasMany<Item>(r => r.Items).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorItem"); m.MapLeftKey("VendorID"); m.MapRightKey("ItemID"); });

            modelBuilder.Entity<Transaction>().Property(c => c.OnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.OffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.EstimatedOnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.EstimatedOffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(c => c.RetailPrice).HasPrecision(18, 3);

            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.OnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.OffRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.EstimatedOnRoadWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionAdjustment>().Property(c => c.EstimatedOffRoadWeight).HasPrecision(18, 4);
            
            modelBuilder.Entity<ScaleTicket>().Property(c => c.InboundWeight).HasPrecision(18, 4);
            modelBuilder.Entity<ScaleTicket>().Property(c => c.OutboundWeight).HasPrecision(18, 4);

            modelBuilder.Entity<TransactionItem>().Property(c => c.AverageWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ActualWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.Rate).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.RecyclablePercentage).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledIn).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledOut).HasPrecision(18, 4);

            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);

            //GpsLog
            modelBuilder.Entity<GpsLog>().Property(c => c.Latitude).HasPrecision(9, 6);
            modelBuilder.Entity<GpsLog>().Property(c => c.Longitude).HasPrecision(9, 6);

            modelBuilder.Entity<Photo>().Property(c => c.Latitude).HasPrecision(9, 6);
            modelBuilder.Entity<Photo>().Property(c => c.Longitude).HasPrecision(9, 6);

            //Claim
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountCredit).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.AdjustmentTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.TotalTax).HasPrecision(18, 3);

            //Address
            modelBuilder.Entity<Address>().Property(c => c.GPSCoordinate1).HasPrecision(9, 6);
            modelBuilder.Entity<Address>().Property(c => c.GPSCoordinate2).HasPrecision(9, 6);

            //Define one-to-one foreign key association
            modelBuilder.Entity<ClaimPaymentSummary>()
               .HasRequired(a => a.Claim)
               .WithOptional(u => u.ClaimPaymentSummary)
               .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimProcess>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimProcess)
                .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimSummary>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimSummary)
                .Map(m => m.MapKey("ClaimID"));


            modelBuilder.Ignore<Contact>();
            modelBuilder.Ignore<VendorStorageSite>();
            modelBuilder.Ignore<Asset>();
            modelBuilder.Ignore<VendorActiveHistory>();
            modelBuilder.Ignore<BankInformation>();
            modelBuilder.Ignore<AppUser>();
            modelBuilder.Ignore<BusinessActivity>();
            modelBuilder.Ignore<Application>();

            base.OnModelCreating(modelBuilder);
        }
    }
}
