﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using CL.TMS.DataContracts.GpStaging;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class GpStagingBoundedContext : GpStagingBaseContext<GpStagingBoundedContext>
    {
        public GpStagingBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<RR_OTS_PM_TRANS_DISTRIBUTIONS> RrOTsPmTransDistributions { get; set; }
        public DbSet<RR_OTS_PM_TRANSACTIONS> RrOTsPmTransactions { get; set; }
        public DbSet<RR_OTS_PM_VENDORS> RrOTsPmVendors { get; set; }
        public DbSet<RR_OTS_RM_APPLY> RrOTsRmApplies { get; set; }
        public DbSet<RR_OTS_RM_CASH_RECEIPT> RrOTsRmCashReceipts { get; set; }
        public DbSet<RR_OTS_RM_CUSTOMERS> RrOTsRmCustomers { get; set; }
        public DbSet<RR_OTS_RM_TRANS_DISTRIBUTIONS> RrOTsRmTransDistributions { get; set; }
        public DbSet<RR_OTS_RM_TRANSACTIONS> RrOTsRmTransactions { get; set; }
        public DbSet<RR_OTS_VW_Vendors_Error> RrOtsVwVendorsErrors { get; set; }
        public DbSet<RR_OTS_VW_Customers_Error> RrOtsVwCustomersErrors { get; set; }
        public DbSet<RR_OTS_VW_Receivables_Invoice_Error> RrOtsVwReceivablesInvoiceErrors { get; set; }
        public DbSet<RR_OTS_VW_Cash_Receipt_Error> RrOtsVwCashReceiptErrors { get; set; }
        public DbSet<RR_OTS_VW_Payables_Invoice_Error> RrOtsVwPayablesInvoiceErrors { get; set; }
        public DbSet<RR_OTS_VW_Cash_Receipt_Apply_Error> RrOtsVwCashReceiptApplyErrors { get; set; }
        public DbSet<RR_OTS_PM_APPLY> RrOtsPmApplies { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

    }
}
