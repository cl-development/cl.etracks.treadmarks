﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class ProductCompositionContext : TMBaseContext<ProductCompositionContext>
    {
        public ProductCompositionContext()
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<RecoverableMaterial> RecoverableMaterials { get; set; }
       // public DbSet<RecoverableMaterialNote> Notes { get; set; }
        public DbSet<ItemRecoverableMaterial> ItemRecoverableMaterials { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Activity> Activities { get; set; }
        #region Estimated Weights
        public DbSet<WeightTransactionNote> WeightNotes { get; set; }
        public DbSet<WeightTransaction> WeightTransactions { get; set; }
        public DbSet<ItemWeight> ItemWeights { get; set; }
        #endregion
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {          
            modelBuilder.Entity<ItemRecoverableMaterial>().Ignore(c=>c.Rowversion);
            modelBuilder.Entity<RecoverableMaterial>().Ignore(c => c.Rowversion);
            modelBuilder.Entity<Item>().Ignore(c=>c.Customers).Ignore(c=>c.ItemParticipantTypes).Ignore(c=>c.ItemWeights).Ignore(c=>c.Vendors);

            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<UserClaimsWorkflow>();
            modelBuilder.Ignore<UserApplicationsWorkflow>();
        }
    }
}
