﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;

namespace CL.TMS.DAL
{
    public class ApplicationBoundedContext : TMBaseContext<ApplicationBoundedContext>
    {
        public DbSet<Application> Applications { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<ApplicationInvitation> ApplicationInvitations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Setting> Settings { get; set; }

        public DbSet<ApplicationNote> ApplicationNotes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Application>().HasMany<Attachment>(r => r.Attachments).WithMany(u => u.Applications).Map(m => { m.ToTable("ApplicationAttachment"); m.MapLeftKey("ApplicationID"); m.MapRightKey("AttachmentID"); });
            modelBuilder.Ignore<Vendor>();
            modelBuilder.Ignore<Claim>();
            modelBuilder.Ignore<Inventory>();
            modelBuilder.Ignore<Item>();
            modelBuilder.Ignore<Transaction>();

            //Ignore rowversion for application - no concurrency check for application auto save
            modelBuilder.Entity<Application>().Ignore(c => c.Rowversion);
            modelBuilder.Entity<Attachment>().Ignore(c => c.Rowversion);

            base.OnModelCreating(modelBuilder);
        }

    }
}
