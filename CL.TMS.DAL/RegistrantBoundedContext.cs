﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Framework.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DAL
{
    public class RegistrantBoundedContext : TMBaseContext<RegistrantBoundedContext>
    {
        public RegistrantBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<VendorActiveHistory> VendorActiveHistories { get; set; }
        public DbSet<BankInformation> BanksInformations { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerActiveHistory> CustomerActiveHistories { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<ApplicationInvitation> ApplicationInvitations { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<VendorUser> VendorUsers { get; set; }
        public DbSet<CustomerUser> CustomerUsers { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<VendorAttachment> VendorAttachments { get; set; }
        public DbSet<CustomerAttachment> CustomerAttachments { get; set; }
        public DbSet<ItemParticipantType> ItemParticipantTypes { get; set; }
        public DbSet<RateTransaction> RateTransactions { get; set; }
        public DbSet<VendorServiceThreshold> VendorServiceThresholds { get; set; }
        public DbSet<VendorRateGroup> VendorRateGroups { get; set; }
        public virtual ObjectResult<SpGetRegistrantSpotlight> SpGetRegistrantSpotlight(string keyWords, string section, int? pageLimit = null)
        {
            var keyWordsParameter = new SqlParameter("@keyWords", keyWords);
            var sectionParameter = new SqlParameter("@section", section);
            var pageLimitParameter = new SqlParameter("@pageLimit", pageLimit ?? 0);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpGetRegistrantSpotlight>("sp_GetRegistrantSpotlights @keyWords, @section, @pageLimit", keyWordsParameter, sectionParameter, pageLimitParameter);
        }

        public virtual ObjectResult<SpGetRegistrantSpotlightsPaginate> SpGetRegistrantSpotlightsPaginate(string keyWords, string section, Nullable<int> start, Nullable<int> pageLimit)
        {
            var keyWordsParameter = new SqlParameter("@keyWords", keyWords);
            var sectionParameter = new SqlParameter("@section", section);
            var startParameter = new SqlParameter("@start", start);
            var pageLimitParameter = new SqlParameter("@pageLimit", pageLimit);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpGetRegistrantSpotlightsPaginate>("sp_GetRegistrantSpotlightsPaginate @keyWords, @section, @start, @pageLimit", keyWordsParameter, sectionParameter, startParameter, pageLimitParameter);
        }

        public virtual int SpGetMaxRegNumByVendorType(int vendorTypeID)
        {
            var sqlParaVendorTypeID = new SqlParameter("@vendorTypeID", vendorTypeID);
            var sqlParaMaxNum = new SqlParameter("@maxNum", SqlDbType.Int);
            sqlParaMaxNum.Direction = ParameterDirection.Output;
            //var sqlParaIncreaseRegNbr = new SqlParameter("@increaseRegNbr", increaseRegNbr);
            var returnVal = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpGetRegistrantSpotlight>("sp_getMaxRegNumByVendorType @vendorTypeID, @maxNum out", sqlParaVendorTypeID, sqlParaMaxNum);
            ((IObjectContextAdapter)this).ObjectContext.Connection.Close();
            return int.Parse(sqlParaMaxNum.Value.ToString());
        }

        public virtual int SpGetMaxRegNumForCustomer()
        {
            var sqlParaMaxNum = new SqlParameter("@maxNum", SqlDbType.Int);
            sqlParaMaxNum.Direction = ParameterDirection.Output;
            var returnVal = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SpGetRegistrantSpotlight>("sp_getMaxRegNumForCustomer @maxNum out", sqlParaMaxNum);
            ((IObjectContextAdapter)this).ObjectContext.Connection.Close();
            return int.Parse(sqlParaMaxNum.Value.ToString());            
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendor>().HasMany<Address>(r => r.VendorAddresses).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorAddress"); m.MapLeftKey("VendorID"); m.MapRightKey("AddressID"); });
            modelBuilder.Entity<Vendor>().HasMany<Item>(r => r.Items).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorItem"); m.MapLeftKey("VendorID"); m.MapRightKey("ItemID"); });                  
            modelBuilder.Entity<Customer>().HasMany<Address>(r => r.CustomerAddresses).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerAddress"); m.MapLeftKey("CustomerId"); m.MapRightKey("AddressId"); });
            modelBuilder.Entity<Customer>().HasMany<Item>(r => r.Items).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerItem"); m.MapLeftKey("CustomerId"); m.MapRightKey("ItemId"); });

            modelBuilder.Entity<Address>().Property(c => c.GPSCoordinate1).HasPrecision(9, 6);
            modelBuilder.Entity<Address>().Property(c => c.GPSCoordinate2).HasPrecision(9, 6);

            base.OnModelCreating(modelBuilder);

            //For performance to ignore the following schema
            modelBuilder.Ignore<InvitationRole>();
            modelBuilder.Ignore<Claim>();

        }

        //this method using at configurationRespository too.
        public virtual ObjectResult<TCRListViewModel> Sp_GetAllServiceThresholds()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TCRListViewModel>("sp_GetAllServiceThresholds");
        }
    }
}