﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Steward;

namespace CL.TMS.ServiceContracts.StewardServices
{
    public interface IStewardRegistrationService : IRegistrationService<StewardRegistrationModel>
    {
        //  IList<IItem> GetAllItems();
        Application GetSingleApplication(int appId);
        StewardRegistrationModel GetAllByFilter(int appId);
        StewardRegistrationModel ModelInitialization(StewardRegistrationModel model);
        void UpdateUserExistsinApplication(StewardRegistrationModel model, int appId, string updatedBy);
        IList<Item> GetAllItems();
        StewardRegistrationModel GetAllItemsList();
        #region Workflow
        void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0);
        ApplicationEmailModel GetApprovedApplicantInformation(int applicationId);
        void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel);
        StewardRegistrationModel GetAllItemsList(int PaticipantType);
        void SetRemitanceStatus(int vendorId, int ID,string PropName, int userId);
        IList<BusinessActivity> GetBusinessActivities(int type);
        #endregion



        
    }
}

