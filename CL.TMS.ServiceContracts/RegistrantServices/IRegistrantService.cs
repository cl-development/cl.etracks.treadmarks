﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;

namespace CL.TMS.ServiceContracts.RegistrantServices
{
    public interface IRegistrantService : IGPService, IGpBatchService
    {
        IReadOnlyList<AppUserModel> GetAppUserList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection);
        IReadOnlyList<AppUserModel> GetAppUserList(string searchValue);
        void ActivateAppUser(int qrCodeId);
        void DeActivateAppUser(int qrCodeId);
        void AddAppUser(int amountToAdd, int vendorId, string createdBy);
        IList<AppUser> GetUpdatedAppUserList(DateTime lastUpdated);

        IReadOnlyList<IPadModel> IPadList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection);
        IReadOnlyList<IPadModel> IPadList(string searchValue);
        IReadOnlyList<IPadModel> IPadList();
        void AddIPad(int amountToAdd, int? vendorId, int assetTypeID, string createdBy);
        void AssignIPad(int iPadId, int vendorId, string unAssignBy);
        void UnAssignIPad(int iPadId, string unAssignBy);
        void ActiveIPad(int iPadId, string activatedBy);
        void DeActiveIPad(int iPadId, string deActivatedBy);
        string CheckForUnassignedIPadSearchValue(string searchValue);
        IReadOnlyDictionary<int, string> GetVendorNameList();

        //-----------Added by Frank, begin: new function-------------
        IReadOnlyDictionary<int, string> GetListOfRegistrantsWithPageIndex(int pageIndex, int pageSize);
        //-----------Added by Frank, end: new function-------------

        //Add by Frank for new requirement
        String GetMatchedRegistrant(string regNumberEntered);

        Vendor GetSingleWithApplicationID(int appId);
        Vendor GetSingleVendorByID(int vendorId);

        void AddVendor(Vendor vendor);
        void UpdateVendor(Vendor vendor);
        void ImportVendorDetails(IEnumerable<Vendor> vendorList);
        void AddVendors(IReadOnlyList<Vendor> vendors);
        string GetVendorNumber(int vendorId);
        IReadOnlyList<Address> GetVendorAddressList(int vendorId);
        Address GetVendorAddressByType(int vendorId, int typeId);
        IList<Vendor> GetUpdatedVendorList(DateTime lastUpdated);
        long GetLastAddedVendorNumber();
        void CreateBankInformation(Vendor vendor, BankingInformationRegistrationModel bankInformationModel);
        Vendor UpdateBankInformation(BankInformation bankInformation);
        void DeleteBankInformation(int bankId, int vendorId);
        BankInformation GetBankInformation(int vendorId);
        BankInformation GetBankInformationById(int bankId);
        IList<Vendor> GetVendorsByApplicationId(int applicationId);
        Vendor FindVendorByID(int vendorId);
        VendorReference GetVendorByNumber(string number);

        VendorReference GetVendorById(int vendorId);

        IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlights(string keyWords, string section, int? pageLimit);
        IEnumerable<RegistrantSpotlightModel> GetRegistrantSpotlightsPaginate(string keyWords, string section, int start, int pageLimit);
        PaginationDTO<RegistrantSpotlightModel, int> LoadSpotlightViewMoreResult(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string keyWords, int pageLimit, string section);
        List<RegistrantSpotlightModel> LoadSpotlightResultExport(string searchText, string orderBy, string sortDirection, string keyWords, string section); //OTSTM2-960/963/964/965/966
        ApplicationEmailModel GetApprovedApplicantInformation(int applicationId);

        #region ApplicationNotes
        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize, string searchText,
          string orderBy, string sortDirection);

        void AddApplicationNote(ApplicationNote applicationNote);

        List<ApplicationNoteViewMode> LoadApplicationNotesForExport(int claimId, string searchText, string sortcolumn, string sortdirection);

        List<ApplicationNoteViewMode> LoadVendorNotesForExport(int vendorId, string searchText, string sortcolumn, string sortdirection);

        List<ApplicationNoteViewMode> LoadCustomerNotesForExport(int customerId, string searchText, string sortcolumn, string sortdirection);

        #endregion

        #region TSFRemittanceNotes
        PaginationDTO<TSFRemittanceNoteViewMode, int> LoadTSFRemittanceNotes(int claimId, int pageIndex, int pageSize, string searchText,
          string orderBy, string sortDirection);

        void AddTSFRemittanceNote(TSFRemittanceNote applicationNote);

        List<TSFRemittanceNoteViewMode> LoadTSFRemittanceNotesForExport(int claimId, string searchText, string sortcolumn, string sortdirection);
        #endregion
        void UpdateVendorGeneratorStatus(int vendorId, bool Status, DateTime date);

        void UpdateVendorSubCollectorStatus(int vendorId, bool Status, DateTime date); //OTSTM2-953

        ApplicationEmailModel GetApprovedRegistrantInformation(int vendorId);

        ApplicationEmailModel GetApprovedCustomerInformation(int customerId);

        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForVendor(int vendorId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForCustomer(int customerId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        void ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory);

        VendorActiveHistory LoadCurrenActiveHistoryForVendor(int vendorId);

        VendorActiveHistory LoadPreviousActiveHistoryForVendor(int vendorId, bool isActive);

        void ActiveInactiveCustomer(CustomerActiveHistory customerrActiveHistory);

        CustomerActiveHistory LoadCurrenActiveHistoryForCustomer(int customerId);

        CustomerActiveHistory LoadPreviousActiveHistoryForCustomer(int customerId, bool isActive);

        VendorReference GetVendorByNumber(string number, long userId, string userName);

        void UpdateRegistrantKeywords(int? registrantType = null);

        //void DeleteBankInformation(int bankId, int vendorId);
        void AutoSwitchForAllCustomers(int changedBy); //OTSTM2-449 backup (manually)

        #region //admin financial integration 
        IEnumerable<FinancialIntegrationVM> GetAdminFinancialIntegration();
        bool UpdateFIData(List<FinancialIntegrationVM> vm);
        #endregion
    }
}
