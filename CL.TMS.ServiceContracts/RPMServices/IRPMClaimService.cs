﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.ServiceContracts.RPMServices
{
    public interface IRPMClaimService : IGPService
    {
        RPMClaimSummaryModel LoadClaimSummaryData(int vendorId, int claimId);
        RPMPaymentViewModel LoadRPMPayment(int claimId);
        RPMSubmitClaimViewModel RPMClaimSubmitBusinessRule(RPMSubmitClaimViewModel submitClaimModel);        
    }
}
