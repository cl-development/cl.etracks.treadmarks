﻿using CL.TMS.DataContracts.RetailConnectionService;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Email;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Export;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.RPMServices
{
    public interface IRetailConnectionService
    {
        PaginationDTO<ParticipantProductsViewModel, int> LoadParticipantProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<StaffCategoryViewModel> LoadProductCategory();

        void AddProduct(ProductsFormViewModel productFormViewModel);

        void AddCategory(StaffCategoryFormViewModel categoryFormViewModel);
        void AddRetailer(RetailersFormViewModel retailersFormViewModel, bool isAddedByParticipant);
        bool CheckRetailerName(string retailerName);
        PaginationDTO<RetailerViewModel, int> LoadParticipantRetailer(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<RetailerReference> LoadParticipantRetailer();

        PaginationDTO<StaffCategoryViewModel, int> LoadProductCategory(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        PaginationDTO<StaffProductViewModel, int> LoadStaffProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        PaginationDTO<RetailerViewModel, int> LoadStaffRetailers(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        List<InternalNoteViewModel> LoadRetailerInternalNotes(int retailerId);

        InternalNoteViewModel AddRetailerNote(int retailerId, string note);

        ProductsFormViewModel GetProduct(int productId);

        void EditProduct(ProductsFormViewModel productViewModel);

        void EditStaffProduct(ProductsFormViewModel productViewModel);

        void ChangeProductStatus(int productId, string fromStatus, string toStatus);
        ProductRetailerApproveEmailModel GetEmailInforByProductId(int productId);
        ProductRetailerApproveEmailModel GetEmailInforByRetailerId(int retailerId);

        void ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus);

        void ChangeRetailerStatus(int retailerId, string fromStatus, string toStatus);

        RetailersFormViewModel LoadRetailerById(int retailerId);

        void AddRetailerNote(RetailerNoteViewModel retailerNoteViewModel);

        void EditRetailer(RetailersFormViewModel retailerFormViewModel);

        List<ParticipantRetailerExportModel> GetParticipantRetailerExport();

        List<StaffRetailerExportModel> GetStaffRetailerExport();

        List<ParticipantProductExportModel> GetParticipantProductExport();

        List<StaffProductExportModel> GetStaffProductExport();

        List<ProductCategoryExportModel> GetProductCategoryExport();

        StaffCategoryFormViewModel LoadCategoryById(int categoryId);

        List<CategoryModel> LoadActiveCategory();

        List<CollectorModel> LoadActiveCollectors();

        List<ProductListModel> LoadActiveProductList(int categoryId);

        ProductModel LoadActiveProductDetails(int productId);
    }
}
