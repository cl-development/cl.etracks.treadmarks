﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.ServiceContracts.RPMServices
{
    public interface IRPMRegistrationService : IRegistrationService<RPMRegistrationModel>,  IActivityMessageService<ActivityMessageModel>
    {
        void UpdateUserExistsinApplication(RPMRegistrationModel model, int appId, string updatedBy);
        Address GetAddressByID(int addressID);       
        RPMRegistrationModel GetAllItemsWithAllChecks();
        RPMRegistrationModel GetAllItemsList();
        RPMRegistrationModel GetAllItemsList(int v);
        RPMRegistrationModel GetAllProduct();

        #region Workflow
        void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0);
        ApplicationEmailModel GetApprovedRPMApplicantInformation(int applicationId);
        void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel);
       

        #endregion

        #region commented out sections
        //void Update(Application application);
        //RPMRegistrationModel GetAllByFilter(int appId);
        //void CreateEntryForApplicationFormObject(string formObjectData, int appId, string createdBy);
        //void UpdateUserExistsinApplication(string formObjectData, int appId, string updatedBy);
        //RPMRegistrationModel GetApplicantInfoByApplicationID(int applicationID);
        //RPMApplicationEMailModel GetApprovedRPMApplicantInformation(int applicationId);
        //IAttachment GetApplicationAttachment(int applicationId, string fileUniqueName);
        //IEnumerable<IAttachment> GetApplicationAttachments(int applicationId);
        //void RemoveApplicationAttachment(int applicationId, int attachmentId, string deletedBy);
        //void RemoveApplicationAttachment(int applicationId, string fileUniqueName, string deletedBy);
        //void AddApplicationAttachments(int applicationId, IEnumerable<IAttachment> attachments);
        //void LogActivity(long? applicationID, string entityName, string message, string updatedBy);
        //List<ActivityMessageModel> GetActivityMessages(long? applicationID);
        //List<ActivityMessageModel> GetAllActivityMessages(long? applicationID, string searchValue);
        //RPMRegistrationModel ModelInitialization(RPMRegistrationModel model);
        //IVendor GetVendorByApplicationID(int applicationID);
        //void CreateEntryForVendorActiveHistory(RegistrantStatusChangeModel RegistrantStatusChangeModel);
        //IVendorActiveHistory GetRecentVendorActiveHistoryByVendorID(int vendorID);
        //ICollection<IVendor> GetVendorsByApplicationId(int applicationId);
        #endregion
    }
}
