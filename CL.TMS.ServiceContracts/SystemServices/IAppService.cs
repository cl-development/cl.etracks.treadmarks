﻿namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IAppService
    {
        string GetSettingValue(string key); 
    }
}