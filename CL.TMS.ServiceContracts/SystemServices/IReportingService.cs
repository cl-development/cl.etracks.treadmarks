﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Metric;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IReportingService
    {
        IEnumerable<ReportCategory> GetAllReportCategories();
        IEnumerable<Report> GetReportsByReportCategory(int Id);
        IEnumerable<RegistrantWeeklyReport> GetRegistrantWeeklyReport();
        IEnumerable<SpRptTsfExtractInBatchOnlyReportGp> GetTsfExtrtactInBatchOnlyReportGp();
        IEnumerable<SpRptProcessorTIPIReport> GetProcessorTIPIReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptHaulerCollectorComparisonReport> GetHaulerCollectorComparisonReport(DateTime? startDate, DateTime? endDate, string registrationNumber);
        Report GetReport(int Id);
        IEnumerable<StewardRptRevenueSupplyNewTireVM> GetStewardRevenueSupplyNewTireTypes(ReportDetailsViewModel detail);
        IEnumerable<StewardRptRevenueSupplyNewTireCreditVM> GetStewardRevenueSupplyNewTireCreditTypes(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate);
        IEnumerable<WebListingCollectorReportPostalVM> GetWebListingCollectorReportPostal(int reportId);
        IEnumerable<WebListingHaulerReport> GetWebListingHaulerReport();
        IEnumerable<HaulerVolumeReport> GetHaulerVolumeReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> GetDetailHaulerVolumeReportBasedOnScaleWeight(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptProcessorVolumeReport> GetProcessorVolumeReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<RpmData> GetRpmData(ReportDetailsViewModel detail);
        IEnumerable<RpmCumulative> GetRpmCumulative(ReportDetailsViewModel detail);
        IEnumerable<ProcessorDispositionOfResidual> GetProcessorDispositionOfResidual();
        IEnumerable<StewardNonFilers> GetStewardNonFilers(DateTime? startDate, DateTime? endDate);
        IEnumerable<TSFRptOnlinePaymentsOutstandingVM> GetTSFOnlinePaymentsOutstanding(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate);
        IEnumerable<CollectorCumulativeReportVM> GetCollectorCumulativeReport(ReportDetailsViewModel model);
        IEnumerable<StewardRptRevenueSupplyNewTireCreditVMsp> GetStewardRevenueSupplyNewTireCreditTypes_SP(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate);

        IEnumerable<WebListingStewardReport> GetWebListingStewardReportPostal(int reportId);

        IEnumerable<WebListingProcessorReport> GetWebListingProcessorReportPostal(int reportId);

        IEnumerable<WebListingRPMReport> GetWebListingRPMReportPostal(int reportId);

        IEnumerable<HaulerTireMovementReport> GetHaulerTireMovementReport(ReportDetailsViewModel model);

        IEnumerable<HaulerTireMovementReportVM> GetHaulerTireMovementReportVM(ReportDetailsViewModel model);

        IEnumerable<CollectorTireOriginReport> GetCollectorTireOriginReport(ReportDetailsViewModel model);

        IEnumerable<HaulerTireCollectionReport> GetHaulerTireCollectionReport(ReportDetailsViewModel model);

        #region OTSTM2-215
        List<CollectionGenerationOfTiresBasedOnFSAReportVM> GetCollectionGenerationOfTiresBasedOnFSAReport(CollectionGenerationOfTiresBasedOnFSAReportSelectionVM selectionModel);
        PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string region, string zone, string fromDate, string toDate);
        #endregion
        #region OTSTM2-1226
        List<CollectionGenerationOfTiresBasedOnRateGroupVM> GetCollectionGenerationOfTiresBasedOnRateGroup(CollectionGenerationOfTiresBasedOnRateGroupSelectionVM selectionModel);
        PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailByRateGroupHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string pickupRateGroup, string collectorGroup, string fromDate, string toDate);
        #endregion

        List<StaffUserNameAndEmailViewModel> GetUserNameBySearchText(string searchText);
        List<UserPermissionExportViewModel> GetStaffUserRolePermissionMatrix(string userEmail);
        List<ReportCategory> GetPermittedReportCategories();
        IEnumerable<StewardRptRevenueSupplyOldTireCreditVMsp> GetStewardRevenueSupplyOldTireCreditTypes_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate);
        IEnumerable<StewardRptRevenueSupplyOldTireVM> GetStewardRevenueSupplyOldTireTypes(ReportDetailsViewModel detail);

        #region Metrics
        MetricVM LoadMetricDetailsByID(MetricParams paramModel);
        #endregion
    }
}
