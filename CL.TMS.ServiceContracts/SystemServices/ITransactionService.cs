﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using CL.TMS.DataContracts.ViewModel.Transaction.RPM;
using CL.TMS.DataContracts.ViewModel.Common;
using System.IO;
using CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface ITransactionService
    {
        void ImportMobileTransaction(Guid transactionId);

        void AcceptTransaction(int transactionId);
        void RejectTransaction(int transactionId);
        void VoidTransaction(int transactionId);
        void VoidSPSRTransaction(int transactionId);
        void InvalidTransaction(int transactionId);
        void AddTransactionRetailPrice(int transactionId, decimal retailPrice);

        void ApproveTransaction(int transactionId);
        bool UpdateTransactionProcessingStatus(int transactionId, TransactionProcessingStatus status);
        PaginationDTO<AllTransactionsViewModel, int> LoadAllTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string queryFilter, string sExtendSearchTxt = "");

        PaginationDTO<TransactionViewModel, Guid> LoadTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);

        List<AllTransactionsViewModel> GetTransactionDetailsExport(string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType, string queryFilter);

        Transaction GetTransactionByFriendlyID(long friendlyId);

        TransactionCreationNotificationDetailViewModel AddHaulerPapeFormTransaction(IHaulerTransactionViewModel model);
        TransactionCreationNotificationDetailViewModel AddCollectorPapeFormTransaction(ICollectorTransactionViewModel model);
        TransactionCreationNotificationDetailViewModel AddProcessorPapeFormTransaction(IProcessorTransactionViewModel model);
        TransactionCreationNotificationDetailViewModel AddRPMPapeFormTransaction(IRPMTransactionViewModel model);

        //OTSTM2-81
        int ImportSPSTransactions(StreamReader InputStream, int claimId);

        //OTSTM2-660
        TransactionReviewToolBar getUpdatedReviewToolBar(int transactionId, int? claimId);

        ITransactionAdjViewModel GetTransactionAdjustmentModel(int transactionId);
        void AdjustTransaction(ITransactionAdjViewModel model, Period claimPeriod);

        void SubmitAdjustmentStatusChange(int id, TransactionAdjustmentStatus reviewStatus, bool getUpdatedData, string notes);

        InternalNoteViewModel AddTransactionNote(int transactionId, string note);
        List<InternalNoteViewModel> GetAllTransactionNote(int transactionId); //OTSTM2-765

        ITransactionDetailViewModel GetTransactionHandlerData(int transactionId, string vendorType, bool adjRequested, int? transactionAdjustmentId = null, int? claimId = 0);

        bool IsVendorActive(int vendorId, DateTime on);
        List<TransactionDisabledDateRangeViewModel> GetVendorInactiveDateRanges(int vendorId);
        IEnumerable<VendorStatusDateRangeModel> GetVendorStatusDateRanges(int vendorId);

        List<RPMCommonTransactionListViewModel> GetRPMExportDetailsStaffTransactions(string search, string sortcolumn, string sortdirection, int p, string transactionType, int claimId, string dataSubType);

        List<RPMCommonTransactionListViewModel> GetRPMExportDetailsParticipantTransactions(string search, string sortcolumn, string sortdirection, int p, string transactionType, int claimId, string dataSubType);

        PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMStaffTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType = "");

        PaginationDTO<RPMCommonTransactionListViewModel, int> LoadRPMVendorTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, string transactionType, int claimId, string dataSubType = "");

        ProcessorPTRWeightDetailViewModel GetPTRWeightDetail(long number);

        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerCommonTransactionListViewModel, int> LoadHaulerRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffUCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffHITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, string dataSubType);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffSTCTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<HaulerStaffCommonTransactionListViewModel, int> LoadHaulerStaffRTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffDOTTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<CollectorCommonTransactionListViewModel, int> LoadCollectorStaffTCRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPTRTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffDORTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffPITTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, int vendorId, string dataSubType);
        PaginationDTO<ProcessorCommonTransactionListViewModel, int> LoadProcessorStaffSPSTransactions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);

        //OTSTM2-620
        bool IsTransAdjPendingAndUnreviewed(int claimId, int transactionId);
        string CheckTransactionVendorStatus(int transactionId, bool addLog);

        #region STC Premium Implementation
        STCEventNumberValidationStatus EventNumberValidationCheck(string eventNumber, DateTime transactionStartDate);
        #endregion
    }
}
