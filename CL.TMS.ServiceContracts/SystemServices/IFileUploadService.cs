﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IFileUploadService
    {
        #region Application Upload
        void AddApplicationAttachments(int applicationId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetApplicationAttachments(int applicationId, bool bBankInfo = false);
        AttachmentModel GetApplicationAttachment(int applicationId, string fileUniqueName);
        void RemoveApplicationAttachment(int applicationId, string fileUniqueName, string deletedBy);
        void UpdateAttachmentDescription(string fileUniqueName, string description);
        #endregion

        #region VendorAttachmentService
        void AddVendorAttachments(int vendorId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo = false);
        AttachmentModel GetVendorAttachment(int vendorId, string fileUniqueName);
        void RemoveVendorAttachment(int vendorId, string fileUniqueName, string deletedBy);
        #endregion

        #region CustomerAttachmentService
        void AddCustomerAttachments(int customerId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo = false);
        AttachmentModel GetCustomerAttachment(int customerId, string fileUniqueName);
        void RemoveCustomerAttachment(int customerId, string fileUniqueName, string deletedBy);
        #endregion


        #region Claim File Upload
        void AddClaimAttachments(int claimId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo = false);
        AttachmentModel GetClaimAttachment(int claimId, string fileUniqueName);
        void RemoveClaimAttachment(int claimId, string fileUniqueName, string deletedBy);
        #endregion


        #region TSFClaim File Upload
        void AddTSFClaimAttachments(int tsfclaimId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetTSFClaimAttachments(int tsfclaimId, bool bBankInfo = false);
        AttachmentModel GetTSFClaimAttachment(int tsfclaimId, string fileUniqueName);
        void RemoveTSFClaimAttachment(int tsfclaimId, string fileUniqueName, string deletedBy);
        #endregion


        #region Transaction File Upload 
        void AddTransactionAttachments(List<AttachmentModel> attachments);
        AttachmentModel GetTransactionAttachment(int attachmentId);
        void RemoveTemporaryTransactionAttachment(int attachmentId);
        void RemoveTemporaryTransactionAttachments(List<int> attachmentIdList);
        #endregion      
  
        #region Product File Upload
        void AddProductAttachment(AttachmentModel attachment);
        AttachmentModel GetProductAttachment(int attachmentId);
        void RemoveTemporaryProductAttachment(int attachmentId);
        #endregion
    }
}
