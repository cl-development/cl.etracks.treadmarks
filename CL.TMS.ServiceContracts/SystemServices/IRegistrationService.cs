﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IRegistrationService<T>
        where T : class
    {
        T GetFormObject(int id);
        T GetByTokenId(Guid id);
        T GetApprovedApplication(int vendorId);
        int ApproveApplication(T model, int appID);
        int UpdateApproveApplication(T model,int vendorId);

    }
}
