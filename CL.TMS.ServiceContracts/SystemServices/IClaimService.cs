﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Enum;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IClaimService
    {
        PaginationDTO<StaffAllClaimsModel, Guid> LoadAllClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);

        List<StaffAllClaimsModel> GetExportDetails(string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);

        void UpdateAssignToUserIdForClaim(int? userId, int claimId);

        StaffAllClaimsModel FilterClaimByType(int userId);

        PaginationDTO<ClaimViewModel, int> LoadVendorClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);

        List<ClaimViewModel> GetExportDetailsParticipantClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);

        PaginationDTO<ClaimViewModel, int> LoadStaffVendorClaims(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);

        List<ClaimViewModel> GetExportDetailsStaffClaims(string searchText, string orderBy, string sortDirection, int vendorId, Dictionary<string, string> columnSearchText);

        void SubmitClaim(int claimId, bool isAutoApproved);

        Period GetClaimPeriod(int claimId);

        IEnumerable<int> GetAllClaimsToReCalculate(int? vendorType);
        IEnumerable<int> GetAllClaimsToReCalculateByStatus(int? vendorType, string status = "");

        IEnumerable<int> GetAllUnderReviewClaimsIds(); //Notification Load test only
        IList GetClaimPeriodForBreadCrumb(int vendorId);

        bool AddInventoryAdjustment(int claimId, ClaimAdjustmentModalResult modalResult);

        void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId);
        #region TransactionAdjusment
        PaginationDTO<ClaimInternalAdjustment, int> LoadClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false);

        ClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId);

        void EditInternalAdjustment(int claimId, ClaimAdjustmentModalResult modalResult);

        void UpdatedClaimMailDate(int claimId, DateTime mailDate);

        void AddSystemClaimNotes(int claimId, List<string> messages);

        int? GetTransactionIdByFriendlyId(long friendlyId, int claimId);
        #endregion


        #region Internal Adjust
        PaginationDTO<TransactionAdjustmentViewModel, int> LoadClaimTransactionAdjustment(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        #endregion

        bool IsTransactionInSameClaim(int claimId, int transactionId);

        ClaimStatus GetClaimStatus(int claimId);
        bool IsClaimOpen(int claimId);
        bool IsClaimAllowAddNewTransaction(int claimId);
        TransactionClaimAssociationDetailViewModel GetTransactionClaimAssociationDetail(int transactionId);
        List<TransactionSearchListViewModel> GetTransactionSearchList(int claimId, string transactionFormat, string processingStatus, string transactionType, int direction, int vendorId);

        VendorReference GetVendorRefernce(string regNumber);

        DateTime? UpdateClaimOnhold(int claimId, bool isOnhold);
        DateTime? UpdateAuditOnhold(int claimId, bool isOnhold);

        ClaimWorkflowViewModel LoadClaimWorkflowViewModel(int claimId, decimal? claimsAmountTotal = null);
        void UpdateClaimWorkflowStatus(int claimId, long reviewedBy, int fromStatus, int toStatus);
        void UpdateHST(int claimId, decimal totalTax, bool isTaxApplicable = true);
        List<ClaimWorkflowUser> LoadClaimWorkflowUsers(string Workflow, string AccountName);
        void ClaimBackToParticipant(int claimId);
        string ApproveClaim(int claimId, int fromStatus, int toStatus);
        bool ConcurrentUsersValidationBeforeApproving(int claimId, int fromStatus); //OTSTM2-1084 
        List<InternalNoteViewModel> LoadClaimInternalNotes(int claimId);
        InternalNoteViewModel AddClaimNote(int claimId, string note);
        List<InternalNoteViewModel> LoadClaimNotesForExport(int claimId, string searchText, string sortcolumn, bool sortReverse);

        string ValidateWorkflowProcess(int claimId);

        List<int> GetAllApprovedClaimIdsForInventoryCal(int? vendorId = null);

        void RecalculateVendorInventory(int claimId);

        void RecalculateClaimInventory(int claimId);

        string checkOtherClaimStatus(int claimId, int transactionId);

        StaffAllClaimsModel FindClaimById(int claimId);
        Claim FindClaimByClaimId(int claimId); //OTSTM2-551

        void ApproveLateCollectorClaimForSupport(string registrationNumber, int claimPeriodId);

        List<int> GetMissingReportDataClaimIds();

        //OTSTM2-270 
        InternalNoteViewModel AddNotesHandlerInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string notes);

        //OTSTM2-270 
        List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId);
        //OTSTM2-270
        List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse);

        List<int> GetMissingCollectorClaimIds();

        List<int> LoadAllClaimIdsByVendorTypeAndStatus(int vendorType = 2, CL.TMS.Common.Enum.ClaimStatus claimStatus = CL.TMS.Common.Enum.ClaimStatus.Approved);

        VendorReference GetVendorReference(int claimId);
        VendorReference GetVendorById(int vendorId);
        string GetPrimaryContactByClaimId(int claimId);

        #region Internal diagnostic method
        List<ClaimTireOrigin> LoadClaimTireOrigins(int claimId);
        List<ClaimDetailViewModel> LoadClaimDetails(int claimId, List<Item> items);
        List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId);
        List<int> getAllVenderByType(int vType);
        List<int> LoadAllClaimIdsByVendorID(int vType);

        #endregion
    }
}
