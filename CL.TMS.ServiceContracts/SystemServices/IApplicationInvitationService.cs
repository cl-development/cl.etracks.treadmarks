﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IApplicationInvitationService
    {
        ApplicationInvitation GetApplicationInvitationByEmailID(string email, string participantType);
        ApplicationInvitation AddApplicationInvitation(ApplicationInvitation appInvitation);
        int GetParticipantTypeID(string participantName);
        ApplicationInvitation GetByTokenID(Guid guid);
        void Update(ApplicationInvitation appInvitation);
        Guid GetTokenByApplicationId(int? applicationId);
        string GetEmailByApplicationId(int applicationId);
        ApplicationInvitation UpdateInvitationDatetime(int applicationId);
    }
}
