﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IUserService
    {
        User FindUserByEmailAddress(string emailAddress);

        User FindUserByUserId(long userId);

        User FindUserByUserName(string username);

        void AddPasswordResetToken(long userId, string token, string secretKey);

        IReadOnlyList<string> GetPasswordResetToken(long userId);

        string FindUserNameByUserId(long userId);

        User ResetPassword(string userName, string password, int passwordExpirationInDays);

        bool EmailIsExisting(string emailAddress);

        void CreateUserInvitation(Invitation invitation);

        Invitation FindInvitation(Guid guid);

        bool ConfirmInvitaion(string password, string userName, DateTime passwordExpiationInDays, string redirectUrl, Guid GUID);

        void UpdateInvitation(Invitation invitation);

        void UpdateInvitationRedirectUrl(int VendorId, string Url);

        Invitation FindInvitationByUserName(string userName);

        void EditUserAndInvitation(Invitation invitation);

        void EditParticipantUserAndInvitation(Invitation invitation);

        List<UserInviteModel> LoadInvitations(string searchText, int vendorId, string orderBy, string sortDirection);

        IDictionary<long, string> GetUsersByGroup(string[] groupName);
        IDictionary<long, string> GetUsersByApplicationsWorkflow(string accountName);

        Dictionary<long, string> GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType);

        bool RemoveAfterLoginRedirectUrl(int UserId);

        void CreateParticipantDefaultUserInvitation(Invitation invitation);

        void ResetRedirectUrlForSecondInvitation(Invitation invitation, User user);

        CreateInvitationViewModel GetParticipantUserDetails(string email, int particiapntId, bool isVendor);

        CreateInvitationViewModel GetParticipantUserDetailsView(string email, int particiapntId, bool isVendor);
        void ActiveUserByEmail(string email, bool isActive);

        void UnlockStaffUserByEmail(string email); //OTSTM2-1013
        void UnlockParticipantUserById(long userId); //OTSTM2-1013

        PaginationDTO<UserInviteModel, long> LoadInvitations(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);

        List<User> GetClaimWorkflowUsers(string accountName, string workflowName);

        Invitation FindInvitationByApplicationId(int applicationId);

        Invitation FindInvitationByVendorId(int vendorId);

        void AddAdditonalUser(long userId, int participantId, bool isVendor, Invitation invitation);

        bool EmailIsExistingForParticipant(string emailAddress, int participantId, bool isVendor);

        List<Claim> LoadParticipantUserRoles(long userId, int participantId, bool isVendor);

        Invitation FindInvitation(int vendorId, string email, bool isVendor);

        Invitation FindInvitationByUserName(string userName, int vendorId, bool isVendor);

        List<Claim> LoadUserVendorClaims(long userId);

        void ActiveParticipantUser(long userId, int vendorId, bool isVendor, bool isActive);
        PaginationDTO<RoleListViewModel, int> GetAllRoles(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<string> GetUserNameByRoleName(string roleName);

        List<RolePermissionViewModel> GetRolePermissions(string roleName);

        List<RolePermissionViewModel> GetDefaultRolePermissions();

        void AddNewUserRole(RoleViewModel roleViewModel, string userName);

        void UpdateUserRole(RoleViewModel roleViewModel, string userName);
        bool DeleteUserRole(int roleID);
        bool RoleNameExists(string roleName);
        
        List<RoleSelectionViewModel> GetStaffUserRoles();

        void CreateStaffUserInvitation(Invitation invitation, StaffUserViewModel staffUserViewModel);

        StaffUserViewModel LoadStaffUserDetails(string email);

        void EditStaffUserDetail(StaffUserViewModel staffUserViewModel);
    }
}