﻿using CL.TMS.DataContracts.ViewModel.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IDashBoardService
    {
        PaginationDTO<ApplicationViewModel,int> GetAllApplicatoinBriefByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        PaginationDTO<RegistrantManageModel, long> GetAllRegistrantByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        List<ApplicationViewModel> LoadApplications(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText);
        List<RegistrantManageModel> LoadRegistrants(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText);

        ApplicationViewModel GetFirstSubmitApplication();      
    }
}
