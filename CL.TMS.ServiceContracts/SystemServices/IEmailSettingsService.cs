﻿using CL.TMS.DataContracts.ViewModel.EmailSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IEmailSettingsService
    {
        //Email Settings - General
        EmailSettingsGeneralVM LoadEmailSettingsGeneralInformation();
        bool UpdateEmailSettingsGeneralInformation(EmailSettingsGeneralVM emailSettingsGeneralVM);

        //Email Settings - Content
        Dictionary<int, string> GetEmailDisplayNamesList();
        EmailVM GetEmailByID(int emailID);
        EmailVM GetEmailByName(string emailName);
        string GetPreviewEmailByID(int emailID);
        string GetCompleteEmailByName(string emailName);
        bool SaveEmailContent(EmailVM emailContent);
        bool IsEmailEnabled(string emailName);        
    }
}
