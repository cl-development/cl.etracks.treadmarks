﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Collector;

namespace CL.TMS.ServiceContracts.CollectorServices
{
    public interface ICollectorRegistrationService : IRegistrationService<CollectorRegistrationModel>
    {
      //  IList<IItem> GetAllItems();
        Application GetSingleApplication(int appId);
        CollectorRegistrationModel GetAllByFilter(int appId);
        CollectorRegistrationModel ModelInitialization(CollectorRegistrationModel model);        
        void UpdateUserExistsinApplication(CollectorRegistrationModel model, int appId, string updatedBy);
        CollectorRegistrationModel GetAllItemsList();
        IList<BusinessActivity> GetBusinessActivities(int type);

        #region Workflow
        void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0);
        ApplicationEmailModel GetApprovedApplicantInformation(int applicationId);

        void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel);
        #endregion

    }
}
