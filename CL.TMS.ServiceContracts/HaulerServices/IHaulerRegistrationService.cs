﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.ServiceContracts.HaulerServices
{
    public interface IHaulerRegistrationService : IRegistrationService<HaulerRegistrationModel>,  IActivityMessageService<ActivityMessageModel>
    {
        void UpdateUserExistsinApplication(HaulerRegistrationModel model, int appId, string updatedBy);
        Address GetAddressByID(int addressID);       
        HaulerRegistrationModel GetAllItemsWithAllChecks();
        HaulerRegistrationModel GetAllItemsList();
               
        #region Workflow
        void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel);

        #endregion      

    }
}
