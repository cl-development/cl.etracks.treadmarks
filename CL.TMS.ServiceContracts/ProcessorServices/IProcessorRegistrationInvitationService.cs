﻿using CL.TMS.DataContracts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.ServiceContracts.ProcessorServices
{
    public interface IProcessorRegistrationInvitationService// : IRegistrationService<ProcessorRegistrationModel>
    {
        ApplicationInvitation GetApplicationInvitationByEmail(string emailID, string participantType);
        int GetParticipantTypeID(string participantName);
        bool ComposeAndSendEmail(ApplicationInvitation appInvitation);
        ApplicationInvitation GetByTokenID(Guid guid);
        void Update(ApplicationInvitation appInvitation);
        Guid GetTokenByApplicationId(int applicationId);
        string GetEmailByApplicationId(int applicationID);
        //IList<Vendor> GetVendorsByApplicationId(int applicationId);
    }
}
