﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.ExceptionHandling;
using CL.TMS.ProcessorBLL;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.SystemBLL;
using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.IRepository;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Registrant;
using CL.TMS.RegistrantBLL;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.TSFSteward;

namespace CL.TMS.ProcessorServices
{
    public class ProcessorRegistrationService : IProcessorRegistrationService
    {
        private ProcessorRegistrationBO ProcessorRegistrationBO;
        private FileUploadBO fileUploadBO;
        private RegistrantBO registrantBO;

        public ProcessorRegistrationService(IApplicationRepository applicationRepository, IItemRepository itemRepository, 
            IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository,
            IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationInvitationRepository appInvitationRepository, ITSFRemittanceRepository tsfRemittanceRepository, IClaimsRepository claimsRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository,IMessageRepository messageRepository, IConfigurationsRepository configurationsRepository)
        {
            this.ProcessorRegistrationBO = new ProcessorRegistrationBO(applicationRepository, itemRepository, vendorRepository, applicationInvitationRepository, claimsRepository, configurationsRepository);
            this.registrantBO = new RegistrantBO(vendorRepository, assetRepository, appUserRepository, applicationRepository, tsfRemittanceRepository, appInvitationRepository, gpRepository, settingRepository, claimsRepository,messageRepository);
        }

        #region ApplicationServices Methods
      
        public ProcessorRegistrationModel GetFormObject(int id)
        {
            ProcessorRegistrationModel result = new ProcessorRegistrationModel();
            try
            {
                result = ProcessorRegistrationBO.GetFormObject(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProcessorRegistrationModel GetByTokenId(Guid id)
        {
            ProcessorRegistrationModel result = new ProcessorRegistrationModel();
            try
            {
                result = ProcessorRegistrationBO.GetByTokenId(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;            
        }

        public int ApproveApplication(ProcessorRegistrationModel model, int appID)
        {
            try
            {
                return this.ProcessorRegistrationBO.ApproveApplication(model, appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }
        public int UpdateApproveApplication(ProcessorRegistrationModel model,int AppID)
        {
            try
            {
                return this.ProcessorRegistrationBO.UpdateApproveApplication(model, AppID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw; 
                }
                return -1;
            }
        }

        public ProcessorRegistrationModel GetApprovedApplication(int vendorId)
        {
            try
            {
                return this.ProcessorRegistrationBO.GetApprovedApplication(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }

        #endregion

        #region IProcessorRegistrationService Methods

        public void UpdateUserExistsinApplication(ProcessorRegistrationModel model, int appId, string updatedBy)
        {
            try
            {
                var application = ProcessorRegistrationBO.GetApplicationById(appId);
                if (application != null)
                {
                    IList<string> tmpInvalidFields = model.InvalidFormFields ?? new List<string>();
                    IList<ContactRegistrationModel> tmpContacts = model.ContactInformationList;
                    IList<ProcessorSortYardDetailsRegistrationModel> tmpSortYardDetails = model.SortYardDetails;

                    //remove empty contacts from lists
                    ProcessorRegistrationBO.RemoveEmptyContacts(ref tmpContacts, ref tmpInvalidFields);
                    ProcessorRegistrationBO.RemoveEmptySortYardDetails(ref tmpSortYardDetails, ref tmpInvalidFields);

                    //reassign model values to new tmp variables
                    model.ContactInformationList = tmpContacts as List<ContactRegistrationModel>;
                    model.SortYardDetails = tmpSortYardDetails as List<ProcessorSortYardDetailsRegistrationModel>;
                    model.InvalidFormFields = tmpInvalidFields ?? new List<string>();

                    //serialize object
                    string formObjectDataSerialized = JsonConvert.SerializeObject(model);
                    Application result = new Application()
                    {
                        FormObject = formObjectDataSerialized,
                        ID = appId,
                        AgreementAcceptedCheck = true,
                        UserIP = model.UserIPAddress
                    };

                    if (model.BusinessLocation != null)
                    {
                        result.Company = model.BusinessLocation.BusinessName;
                    }

                    if (model.ContactInformationList.Count > 0)
                    {
                        result.Contact = string.Format("{0} {1} {2}",
                        model.ContactInformationList[0].ContactInformation.FirstName,
                        model.ContactInformationList[0].ContactInformation.LastName,
                        model.ContactInformationList[0].ContactInformation.PhoneNumber);
                    }


                    ProcessorRegistrationBO.UpdateApplication(result);
                }
                else
                {
                    string formObjectData = JsonConvert.SerializeObject(model);
                    ProcessorRegistrationBO.UpdateFormObject(appId, formObjectData, updatedBy);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Address GetAddressByID(int addressID)
        {
            Address addr = new Address();
            return addr;
        }
        
        public ProcessorRegistrationModel GetAllItemsList()
        {
            ProcessorRegistrationModel result = new ProcessorRegistrationModel();
            try
            {
                result = ProcessorRegistrationBO.GetAllItemsList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public ProcessorRegistrationModel GetAllProcessorProduced()
        {
            ProcessorRegistrationModel result = new ProcessorRegistrationModel();
            try
            {
                result = ProcessorRegistrationBO.GetAllProcessorProduced();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public ProcessorRegistrationModel GetAllProduct(int ItemCategory)
        {
            ProcessorRegistrationModel result = new ProcessorRegistrationModel();
            try
            {
                result = ProcessorRegistrationBO.GetAllProduct(ItemCategory);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        

        public ProcessorRegistrationModel GetAllItemsWithAllChecks()
        {
            ProcessorRegistrationModel model = new ProcessorRegistrationModel();
            return model;
        }

        #endregion

        #region IActivityMessageService Methods

        public IList<ActivityMessageModel> GetActivityMessages(long? applicationId)
        {

            throw new NotImplementedException();

            //if (applicationId == null || applicationId <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = activityPageLength;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;


        }

        public IList<ActivityMessageModel> GetAllActivityMessages(long? applicationId, string searchValue)
        {
            throw new NotImplementedException();

            //if (applicationID == null || applicationID <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = 0;
            //actShow.PageNumber = 0;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;
        }


        #endregion

        //public ProcessorRegistrationModel GetApplicantInfoByApplicationID(int applicationID)
        //{
        //    ProcessorRegistrationBO.CheckIfApplicationExists(applicationID);

        //    ProcessorRegistrationModel model = new ProcessorRegistrationModel();
        //    return model;
        //}

        #region WorkFlow Services

        public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0)
        {
            try
            {
                this.registrantBO.SetStatus(applicationId, applicationStatus, denyReasons, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }

        public ApplicationEmailModel GetApprovedProcessorApplicantInformation(int applicationId)
        {
            try
            {
                return this.ProcessorRegistrationBO.GetApprovedProcessorApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }        

        public void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel)
        {
            try
            {
                this.registrantBO.SendEmailForBackToApplicantProcessor(applicationID, emailModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion        
    }
}
