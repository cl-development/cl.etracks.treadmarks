﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.PDFGenerator.Encryption;
using CL.TMS.PDFGenerator.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using ZXing;

namespace CL.TMS.PDFGenerator
{
    public class Signs
    {
        #region Fields
        byte[] Content;
        string TemplatePath;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates an object based on PDF tempalte file path
        /// </summary>     
        public Signs(string templatePath)
        {
            TemplatePath = templatePath;
        }
        public Signs(byte[] content)
        {
            this.Content = content;
        }
        #endregion


        #region Methods
        /// <summary>        
        /// Populates the form fields in template
        /// </summary>     
        public byte[] PopulateForm(Dictionary<string, string> values, bool flattenForm)
        {
            using (PdfReader reader = new PdfReader(TemplatePath))
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (PdfStamper stamper = new PdfStamper(reader, stream))
                    {
                        foreach (string key in values.Keys)
                        {
                            stamper.AcroFields.SetField(key, values[key]);
                        }
                        stamper.FormFlattening = flattenForm;
                    }
                    return stream.ToArray();
                }
            }
        }

        /// <summary>        
        /// Inserts QR Code into the stream provided
        /// </summary>     
        public byte[] InsertQRCode(byte[] byteStream, SignModel sign)
        {
            var data = new
            {
                r = sign.RegistrationNumber,
                w = sign.UserID
            };

            //JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string qrText = JsonConvert.SerializeObject(data);
            byte[] compressedBytes = CompressionUtitity.Compress(qrText);
            byte[] encryptedBytes = EncryptionUtility.Encrypt(compressedBytes);
            string encryptedString = Encoding.GetEncoding("iso-8859-1").GetString(encryptedBytes);

            using (PdfReader reader = new PdfReader(byteStream))
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (PdfStamper stamper = new PdfStamper(reader, stream))
                    {
                        PdfContentByte cb = stamper.GetOverContent(1);

                        BarcodeWriter writer = new BarcodeWriter();
                        writer.Format = BarcodeFormat.QR_CODE;

                        writer.Options.Width = 270;
                        writer.Options.Margin = 0;
                        writer.Options.Height = writer.Options.Width;

                        using (System.Drawing.Image imageDrawing = writer.Write(encryptedString))
                        {
                            Image image = Image.GetInstance(imageDrawing, ImageFormat.Png);
                            image.SetAbsolutePosition(251, 150);
                            cb.AddImage(image);
                        }
                    }
                    return stream.ToArray();
                }
            }
        }

        /// <summary>        
        /// Generates stream of signs based on list provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateSigns(List<SignModel> signList)
        {
            for (int index = 0; index < signList.Count; index++)
            {
                if (index == 0)
                    this.Content = CreateSignFromTemplate(signList[index]);
                else
                    Concat(CreateSignFromTemplate(signList[index]));
            }
        }

        public byte[] CreateSignFromTemplate(SignModel sign)
        {
            Dictionary<string, string> formValues = new Dictionary<string, string>();
            formValues.Add("Registration", sign.RegistrationNumber + "-" + sign.UserID);
            formValues.Add("CompanyName", sign.BusinessName + "\n" + sign.Address);

            return InsertQRCode(PopulateForm(formValues, true), sign); ;
        }


        /// <summary>        
        /// Generates sign based on model provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateSingleSign(SignModel sign)
        {
            this.Content = CreateSignFromTemplate(sign);
        }

        public void Save(Stream stream)
        {
            if (this.Content.Length > 0)
            {
                stream.Write(this.Content, 0, this.Content.Length);
            }
        }

        /// <summary>        
        /// Concat one page behind another 
        /// </summary>     
        public void Concat(byte[] secondContent)
        {
            int pageOffset = 0;
            ArrayList master = new ArrayList();
            int f = 0;
            Document document = null;
            PdfCopy writer = null;

            using (MemoryStream firstPDF = new MemoryStream(this.Content))
            {
                using (MemoryStream secondPDF = new MemoryStream(secondContent))
                {
                    Stream[] fileNames = new Stream[] { firstPDF, secondPDF };

                    using (MemoryStream outFile = new MemoryStream())
                    {
                        while (f < fileNames.Length)
                        {
                            // we create a reader for a certain document
                            PdfReader reader = new PdfReader(fileNames[f]);
                            reader.ConsolidateNamedDestinations();
                            // we retrieve the total number of pages
                            int n = reader.NumberOfPages;
                            pageOffset += n;
                            if (f == 0)
                            {
                                // step 1: creation of a document-object
                                document = new Document(reader.GetPageSizeWithRotation(1));
                                // step 2: we create a writer that listens to the document
                                writer = new PdfCopy(document, outFile);
                                // step 3: we open the document
                                document.Open();
                            }
                            // step 4: we add content
                            for (int i = 0; i < n;)
                            {
                                ++i;
                                if (writer != null)
                                {
                                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                                    writer.AddPage(page);
                                }
                            }

                            //Error: Additional information: Document iTextSharp.text.pdf.PdfReader has already been added.
                            //PRAcroForm form = reader.AcroForm;
                            //if ( form != null && writer != null )
                            //{
                            //    writer.AddDocument( reader );
                            //}

                            f++;
                        }
                        // step 5: we close the document
                        if (document != null)
                        {
                            document.Close();
                        }

                        this.Content = outFile.ToArray();
                    }
                }
            }
        }
        public byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                this.Save(stream);

                return stream.ToArray();
            }
        }

        public byte[] GetContent()
        {
            return this.Content;
        }
        #endregion

    }
}
