﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.PDFGenerator.Models
{
    public class BadgeModel
    {
        public string RegistrationNumber
        {
            get;
            set;
        }
        public string UserID
        {
            get;
            set;
        }

        public string BusinessName
        {
            get;
            set;
        }
        public BadgeModel Clone()
        {
            return (BadgeModel)this.MemberwiseClone();
        }
    }
}
