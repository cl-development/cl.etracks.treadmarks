﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.CollectorBLL;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.SystemBLL;
using Newtonsoft.Json;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.IRepository;
using CL.TMS.IRepository.Registrant;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.Framework.Common;
using CL.TMS.IRepository.Claims;

namespace CL.TMS.CollectorServices
{
    public class CollectorRegistrationService : ICollectorRegistrationService
    {
        private CollectorRegistrationBO collectorRegistrationBO;
        private FileUploadBO fileUploadBO;
        public CollectorRegistrationService(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository,
            IApplicationInvitationRepository applicationInvitationRepository, IClaimsRepository claimsRepository, IUserRepository userRepository, IInvitationRepository invitationRepository,
            IConfigurationsRepository configurationsRepository)
        {
            this.collectorRegistrationBO = new CollectorRegistrationBO(applicationRepository, itemRepository, vendorRepository,
                applicationInvitationRepository, claimsRepository, userRepository, invitationRepository, configurationsRepository);
        }

        public CollectorRegistrationModel GetFormObject(int id)
        {
            CollectorRegistrationModel result = new CollectorRegistrationModel();
            try
            {
                result = collectorRegistrationBO.GetFormObject(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public IList<BusinessActivity> GetBusinessActivities(int type)
        {
            try
            {
                return collectorRegistrationBO.GetBusinessActivities(type);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public CollectorRegistrationModel GetAllItemsList()
        {
            CollectorRegistrationModel result = new CollectorRegistrationModel();
            try
            {
                result = collectorRegistrationBO.GetAllItemsList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public CollectorRegistrationModel GetAllByFilter(int appId)
        {
            try
            {
                return collectorRegistrationBO.GetAllByFilter(appId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public CollectorRegistrationModel ModelInitialization(CollectorRegistrationModel model)
        {
            try
            {
                return collectorRegistrationBO.ModelInitialization(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Application GetSingleApplication(int appId)
        {
            try
            {
                return collectorRegistrationBO.GetSingleApplication(appId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public CollectorRegistrationModel GetByTokenId(Guid id)
        {
            CollectorRegistrationModel result = new CollectorRegistrationModel();
            try
            {
                result = collectorRegistrationBO.GetByTokenId(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateUserExistsinApplication(CollectorRegistrationModel model, int appId, string updatedBy)
        {
            try
            {
                var application = collectorRegistrationBO.GetApplicationById(appId);
                if (application != null)
                {
                    if (model.Status != ApplicationStatusEnum.None)
                    {
                        if (model.Status != EnumHelper.ToEnum<ApplicationStatusEnum>(application.Status))
                        {
                            throw new CLValidaitonException(string.Format("Unable to save data.  The application status has changed to {0}.", application.Status));
                        }
                    }

                    IList<string> tmpInvalidFields = model.InvalidFormFields ?? new List<string>();
                    IList<ContactRegistrationModel> tmpContacts = model.ContactInformationList;

                    //remove empty contacts from lists
                    collectorRegistrationBO.RemoveEmptyContacts(ref tmpContacts, ref tmpInvalidFields);

                    //reassign model values to new tmp variables
                    model.ContactInformationList = tmpContacts as List<ContactRegistrationModel>;
                    model.InvalidFormFields = tmpInvalidFields ?? new List<string>();

                    //serialize object
                    string formObjectDataSerialized = JsonConvert.SerializeObject(model);
                    Application result = new Application()
                    {
                        FormObject = formObjectDataSerialized,
                        ID = appId,
                        AgreementAcceptedCheck = true,
                        UserIP = model.UserIPAddress
                    };

                    if (model.BusinessLocation != null)
                    {
                        result.Company = model.BusinessLocation.BusinessName;
                    }

                    if (model.ContactInformationList.Count > 0)
                    {
                        result.Contact = string.Format("{0} {1} {2}",
                            model.ContactInformationList[0].ContactInformation.FirstName,
                            model.ContactInformationList[0].ContactInformation.LastName,
                            model.ContactInformationList[0].ContactInformation.PhoneNumber);
                    }

                    collectorRegistrationBO.UpdateApplication(result);
                }
                else
                {
                    string formObjectData = JsonConvert.SerializeObject(model);
                    collectorRegistrationBO.UpdateFormObject(appId, formObjectData, updatedBy);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public int ApproveApplication(CollectorRegistrationModel model, int appID)
        {
            try
            {
                return this.collectorRegistrationBO.ApproveApplication(model, appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }
        public int UpdateApproveApplication(CollectorRegistrationModel model, int vendorId)
        {
            try
            {
                return this.collectorRegistrationBO.UpdateApproveApplication(model, vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }

        public CollectorRegistrationModel GetApprovedApplication(int vendorId)
        {
            try
            {
                return this.collectorRegistrationBO.GetApprovedApplication(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }


        #region WorkFlow Services

        public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0)
        {
            try
            {
                this.collectorRegistrationBO.SetStatus(applicationId, applicationStatus, denyReasons, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            try
            {
                return this.collectorRegistrationBO.GetApprovedCollectorApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel)
        {
            try
            {
                this.collectorRegistrationBO.SendEmailForBackToApplicant(applicationID, emailModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion        

    }
}
