﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.StewardBLL;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.StewardServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.SystemBLL;
using Newtonsoft.Json;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.IRepository;
using CL.TMS.IRepository.Registrant;
using CL.TMS.RegistrantBLL;

namespace CL.TMS.StewardServices
{
    public class StewardRegistrationService : IStewardRegistrationService
    {
        private StewardRegistrationBO StewardRegistrationBO;
        private FileUploadBO fileUploadBO;
        private RegistrantBO registrantBO;

        public StewardRegistrationService(
            IApplicationRepository applicationRepository, 
            IItemRepository itemRepository, 
            IVendorRepository vendorRepository, 
            IApplicationInvitationRepository applicationInvitationRepository)
        {
            this.StewardRegistrationBO = new StewardRegistrationBO(applicationRepository, itemRepository, vendorRepository, applicationInvitationRepository);
            //this.fileUploadBO = new FileUploadBO(applicationRepository);
        }
        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            try
            {
                StewardRegistrationBO.UpdateFormObject(id, formObject, updatedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public StewardRegistrationModel GetFormObject(int id)
        {
            StewardRegistrationModel result = new StewardRegistrationModel();
            try
            {
                result = StewardRegistrationBO.GetFormObject(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public IList<Item> GetAllItems()
        {    
            try
            {
                return StewardRegistrationBO.GetAllItems();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public StewardRegistrationModel GetAllItemsList()
        {
            StewardRegistrationModel result = new StewardRegistrationModel();
            try
            {
                result = StewardRegistrationBO.GetAllItemsList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }

        public IList<BusinessActivity> GetBusinessActivities(int type)
        {
            try
            {
                return StewardRegistrationBO.GetBusinessActivities(type);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public StewardRegistrationModel GetAllItemsList(int ParticipantType)
        {
            StewardRegistrationModel result = new StewardRegistrationModel();
            try
            {
                result = StewardRegistrationBO.GetAllItemsList(ParticipantType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public StewardRegistrationModel GetAllByFilter(int appId)
        {
            try
            {
                return StewardRegistrationBO.GetAllByFilter(appId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public StewardRegistrationModel ModelInitialization(StewardRegistrationModel model)
        {
            try
            {
                return StewardRegistrationBO.ModelInitialization(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Application GetSingleApplication(int appId)
        {
            try
            {
                return StewardRegistrationBO.GetSingleApplication(appId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public StewardRegistrationModel GetByTokenId(Guid id)
        {
            StewardRegistrationModel result = new StewardRegistrationModel();
            try
            {
                result = StewardRegistrationBO.GetByTokenId(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;          
        }

        public void UpdateUserExistsinApplication(StewardRegistrationModel model, int appId, string updatedBy)
        {
            try
            {
                var application = StewardRegistrationBO.GetApplicationById(appId);

                if (application != null)
                {
                    IList<string> tmpInvalidFields = model.InvalidFormFields ?? new List<string>();
                    IList<ContactRegistrationModel> tmpContacts = model.ContactInformationList;

                    //remove empty contacts from lists
                    StewardRegistrationBO.RemoveEmptyContacts(ref tmpContacts, ref tmpInvalidFields);

                    //reassign model values to new tmp variables
                    model.ContactInformationList = tmpContacts as List<ContactRegistrationModel>;
                    model.InvalidFormFields = tmpInvalidFields ?? new List<string>();

                    //serialize object
                    string formObjectDataSerialized = JsonConvert.SerializeObject(model);
                    Application result = new Application()
                    {                       
                        FormObject = formObjectDataSerialized,
                        ID = appId,
                        AgreementAcceptedCheck = true,
                        UserIP = model.UserIPAddress,
                        Company = model.BusinessLocation.BusinessName,
                        Contact = string.Format("{0} {1} {2}",                                                
                        model.ContactInformationList[0].ContactInformation.FirstName,
                        model.ContactInformationList[0].ContactInformation.LastName, 
                        model.ContactInformationList[0].ContactInformation.PhoneNumber),
                       
                    };

                    StewardRegistrationBO.UpdateApplication(result);
                }
                else
                {
                    string formObjectData = JsonConvert.SerializeObject(model);
                    StewardRegistrationBO.UpdateFormObject(appId, formObjectData, updatedBy);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public int ApproveApplication(StewardRegistrationModel model, int appID)
        {
            try
            {
                return this.StewardRegistrationBO.ApproveApplication(model, appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }
        public int UpdateApproveApplication(StewardRegistrationModel model, int appID)
        {
            try
            {
                return this.StewardRegistrationBO.UpdateApproveApplication(model,appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }

        public StewardRegistrationModel GetApprovedApplication(int vendorId)
        {
            try
            {
                return this.StewardRegistrationBO.GetApprovedApplication(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }
        
        #region WorkFlow Services

        public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0)
        {
            try
            {
                this.StewardRegistrationBO.SetStatus(applicationId, applicationStatus, denyReasons, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }
        public void SetRemitanceStatus(int vendorId, int ID,string PropName, int userId)
        {
            try
            {
                this.StewardRegistrationBO.SetRemitanceStatus(vendorId,ID,PropName,userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            try
            {
                return this.StewardRegistrationBO.GetApprovedStewardApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel)
        {
            try
            {
                this.StewardRegistrationBO.SendEmailForBackToApplicantSteward(applicationID, emailModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion        

    }
}
