﻿using CL.TMS.ServiceContracts.StewardServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Framework.DTO;
using CL.TMS.ExceptionHandling;
using CL.TMS.StewardBLL;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.StewardServices
{
    public class TSFRemittanceService : ITSFRemittanceService
    {
        private readonly TSFRemittanceBO remittanceBO;

        public TSFRemittanceService(IVendorRepository vendorRepository, ITSFRemittanceRepository remittanceRepository, IGpRepository gpRepository, IMessageRepository messageRepository, IUserRepository userRepository, IEventAggregator eventAggregator)
        {
            this.remittanceBO = new TSFRemittanceBO(vendorRepository, remittanceRepository, gpRepository, messageRepository, userRepository, eventAggregator);
        }

        public void AddRemittanceDetail(TSFRemittanceItemModel model)
        {
            try
            {
                remittanceBO.AddRemittanceDetail(model);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void UpdatePenaltiesManually(int claimID, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false)
        {
            try
            {
                remittanceBO.UpdatePenaltiesManually(claimID, penaltiesManually, modifiedBy, IsPenaltyOverride);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true)
        {
            try
            {
                remittanceBO.UpdateHST(tsfClaimId, modifiedBy, IsTaxApplicable);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateSupportingDocumentValue(int id, string docOptionValue, string modifiedBy)
        {
            try
            {
                remittanceBO.UpdateSupportingDocumentValue(id, docOptionValue, modifiedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public int AddTSFClaim(TSFRemittanceModel model, ref bool isCreated)
        {
            try
            {
                return remittanceBO.AddTSFClaim(model, ref isCreated);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;

                }
                return 0;
            }

        }

        public PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return remittanceBO.GetAllITSFRemittanceBrief(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceByFilterByCustomer(string RegistertionNo, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return remittanceBO.GetAllITSFRemittanceBriefByCustomer(RegistertionNo, pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public TSFRemittanceModel GetTSFRemittanceById(int id, int customerID)
        {
            try
            {
                return remittanceBO.GetTSFRemittanceById(id, customerID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public TSFRemittanceModel GetTSFRemittanceByClaimId(int id)
        {
            try
            {
                return remittanceBO.GetTSFRemittanceByClaimId(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public TSFRemittanceBriefModel GetFirstSubmitRemittance()
        {
            try
            {
                return remittanceBO.GetFirstSubmitRemittance();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod)
        {
            try
            {
                return remittanceBO.GetRateList(reportingPeriod);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<PeriodListModel> GetTSFRateDateList(DateTime PeriodDate)
        {
            try
            {
                return remittanceBO.GetTSFRateDateList(PeriodDate);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod, int ItemID)
        {
            try
            {
                return remittanceBO.GetRateList(reportingPeriod, ItemID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-567
        public RateListModel GetRate(DateTime reportingPeriod, int ItemID)
        {
            try
            {
                return remittanceBO.GetRate(reportingPeriod, ItemID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public TSFRemittanceItemModel GetRemittanceDetail(int claimId, int detailId)
        {
            try
            {
                return remittanceBO.GetRemittanceDetail(claimId, detailId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void SetRemittanceStatus(int ClaimId, string RemittanceStatus, string updatedBy, long assignToUser = 0, bool isAssigningToMe = false)
        {
            try
            {
                this.remittanceBO.SetRemittanceStatus(ClaimId, RemittanceStatus, updatedBy, assignToUser, isAssigningToMe);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public decimal? SetRemittanceStatus(int ClaimId, string RemittanceStatus, string updatedBy, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignToUser = 0)
        {
            try
            {
                return this.remittanceBO.SetRemittanceStatus(ClaimId, RemittanceStatus, updatedBy, submitted, received, deposit, referenceNo, amount, PaymentType, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void SetRemittanceStatusAuto(int ClaimId, string RemittanceStatus, string updatedBy, string amount, long assignToUser = 0)
        {
            try
            {
                this.remittanceBO.SetRemittanceStatusAuto(ClaimId, RemittanceStatus, updatedBy, amount, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void DeleteRemittanceDetail(int claimId, int detailId)
        {
            try
            {
                remittanceBO.DeleteRemittanceDetail(claimId, detailId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public List<TSFRemittanceBriefModel> LoadRemittanceByRegNo(string searchText, string sortcolumn, string sortdirection, string regNo, Dictionary<string, string> columnSearchText)
        {
            try
            {
                return remittanceBO.LoadRemittanceApplications(searchText, sortcolumn, sortdirection, regNo, columnSearchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public GpResponseMsg CreateBatch()
        {
            try
            {
                return this.remittanceBO.CreateBatch();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;

                }
                return null;
            }
        }

        public GpResponseMsg UpdatePaymentType(int id, string paymentType, string modifiedBy)
        {
            try
            {
                return remittanceBO.UpdatePaymentType(id, paymentType, modifiedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public GpResponseMsg UpdateCurrencyType(int id, string currencyType, string modifiedBy)
        {
            try
            {
                return remittanceBO.UpdateCurrencyType(id, currencyType, modifiedBy);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddTSFRemittanceNote(TSFRemittanceNote claimNote)
        {
            try
            {
                remittanceBO.AddTSFRemittanceNote(claimNote);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public TSFClaim GetLastApprovedTSFRemittance(int customerID, int claimID)
        {
            return remittanceBO.GetLastApprovedTSFRemittance(customerID, claimID);
        }

        public bool IsAnyPastPenalty(int customerID, int claimID)
        {
            return remittanceBO.IsAnyPastPenalty(customerID, claimID);
        }

        //OTSTM2-21
        public Customer GetCustomerByCustomerId(int customerId)
        {
            return remittanceBO.GetCustomerByCustomerId(customerId);
        }

        //OTSTM2-485
        public PaginationDTO<TSFClaimInternalAdjustment, int> LoadTSFClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            try
            {
                return remittanceBO.LoadTSFClaimInternalAdjustments(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, isStaff);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool AddInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            try
            {
                return remittanceBO.AddInternalAdjustment(claimId, modalResult);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            try
            {
                remittanceBO.RemoveInternalAdjustment(claimId, adjustmentType, internalAdjustmentId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public TSFClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId)
        {
            try
            {
                return remittanceBO.GetClaimAdjustmentModalResult(internalAdjustType, internalAdjustId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void EditInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            try
            {
                remittanceBO.EditInternalAdjustment(claimId, modalResult);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int ClaimInternalPaymentAdjustId)
        {
            try
            {
                return remittanceBO.GetInternalNotesInternalAdjustment(ClaimInternalPaymentAdjustId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public InternalNoteViewModel AddNotesHandlerInternalAdjustment(int ClaimInternalPaymentAdjustId, string notes)
        {
            try
            {
                return remittanceBO.AddNotesHandlerInternalAdjustment(ClaimInternalPaymentAdjustId, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            try
            {
                return remittanceBO.ExportToExcelInternalAdjustmentNotes(ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool HasData(int TSFClaimId)
        {
            try
            {
                return remittanceBO.HasData(TSFClaimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return true;
        }
        public bool RemoveTSFClaim(int tsfClaimId)
        {
            try
            {
                return remittanceBO.RemoveTSFClaim(tsfClaimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public IList GetClaimPeriodForBreadCrumb(int customerId)
        {
            try
            {
                return remittanceBO.GetTSFClaimPeriodForBreadCrumb(customerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable GetClaimPeriods(int periodTypeID)
        {
            try
            {
                return remittanceBO.GetTSFClaimPeriods(periodTypeID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public VendorReference GetVendorReference(int tsfClaimId)
        {
            try
            {
                return remittanceBO.GetVendorReference(tsfClaimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        
    }
}
